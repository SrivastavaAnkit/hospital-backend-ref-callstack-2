package com.hospital.support;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.io.Serializable;
import java.time.LocalDateTime;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class JobContext implements Serializable {
	private static final long serialVersionUID = -1L;

    public LocalDateTime getSystemTimestamp() {
        return LocalDateTime.now();
    }

	public String getUser() {
		return "USERNAME";
	}
}
