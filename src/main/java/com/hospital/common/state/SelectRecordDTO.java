package com.hospital.common.state;

import org.springframework.data.domain.Page;

import com.hospital.common.utils.RestResponsePage;
import com.hospital.model.RecordSelectedEnum;



/**
 * Generic Dto for SELRCD
 */
public class SelectRecordDTO<T> extends BaseDTO {
	private static final long serialVersionUID = 4359661418457023962L;

	private RestResponsePage<T> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";
    private RecordSelectedEnum recordSelect = null;

	public SelectRecordDTO() {
	}

	public void setPageDto(RestResponsePage<T> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<T> getPageDto() {
		return pageDto;
	}

    public int getPage()
    {
        return page;
    }

    public void setPage(int page)
    {
        this.page = page;
    }

    public int getSize()
    {
        return size;
    }

    public void setSize(int size)
    {
        this.size = size;
    }

    public String getSortData()
    {
        return sortData;
    }

    public void setSortData(String sortData)
    {
        this.sortData = sortData;
    }

    public RecordSelectedEnum getRecordSelect()
    {
        return recordSelect;
    }

    public void setRecordSelect(RecordSelectedEnum recordSelect)
    {
        this.recordSelect = recordSelect;
    }
}