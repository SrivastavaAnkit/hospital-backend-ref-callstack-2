package com.hospital.common.state;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.hospital.model.CmdKeyEnum;
import com.hospital.model.DeferConfirmEnum;
import com.hospital.model.ProgramModeEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;

/**
 * Common fields of Data Transfer Object.
 *
 * @author Amit Arya
 * @since (2015-11-17.12:47:12)
 */
public class BaseDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private CmdKeyEnum _sysCmdKey = CmdKeyEnum._STA_NONE;
	private DeferConfirmEnum _sysDeferConfirm = DeferConfirmEnum._PROCEED_TO_CONFIRM;
 	private ProgramModeEnum  _sysProgramMode  = ProgramModeEnum._STA_CHANGE;
	private ReturnCodeEnum   _sysReturnCode   = ReturnCodeEnum._STA_NORMAL;
    private String _sysEntrySelected = "";
    private ReloadSubfileEnum reloadSubfile;

	public CmdKeyEnum get_SysCmdKey() {
		return _sysCmdKey;
	}

	public DeferConfirmEnum get_SysDeferConfirm() {
		return _sysDeferConfirm;
	}

 	public ProgramModeEnum get_SysProgramMode() {
		return _sysProgramMode;
	}

 	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String get_SysEntrySelected() {
		return _sysEntrySelected;
	}

	public void set_SysCmdKey(CmdKeyEnum cmdKey) {
		_sysCmdKey = cmdKey;
	}

	public void set_SysDeferConfirm(DeferConfirmEnum deferConfirm) {
		_sysDeferConfirm = deferConfirm;
	}

	public void set_SysProgramMode(ProgramModeEnum programMode) {
		_sysProgramMode = programMode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
    }

	public void set_SysEntrySelected(String entrySelected) {
		_sysEntrySelected = entrySelected;
	}

    /*
     * Properties to be review...
     */
	private List<MessageDTO> messages = new ArrayList<MessageDTO>();
	private String nextScreen;

	public List<MessageDTO> getMessages() {
		return messages;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public void setMessage(MessageDTO message) {
		this.messages.add(message);
	}

	public void setMessages(List<MessageDTO> messages) {
		this.messages = messages;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	  private Map<String,String> messageMap=new TreeMap<String,String>();

		public Map<String, String> getMessageMap() {
			return messageMap;
		}

		public void setMessageMap(Map<String, String> messageMap) {
			this.messageMap = messageMap;
		}
	    
		public void setMessage(String code,String message) {
			this.messageMap.put(code, message);
		}
		
	public ReloadSubfileEnum getReloadSubfile() {
		return reloadSubfile;
	}

	public void setReloadSubfile(ReloadSubfileEnum reloadSubfile) {
		this.reloadSubfile = reloadSubfile;
	}
}
