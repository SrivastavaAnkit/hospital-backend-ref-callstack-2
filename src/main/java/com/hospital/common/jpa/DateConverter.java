package com.hospital.common.jpa;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;
import java.time.LocalDate;

/** LocalDate to JPA 2.1 converter. */
@Converter(autoApply = true)
public class DateConverter implements AttributeConverter<LocalDate, Date> {

  public Date convertToDatabaseColumn(LocalDate localDate) {
    if (localDate == null) {
      return null;
    }

    return Date.valueOf(localDate);
  }

  public LocalDate convertToEntityAttribute(Date sqlDate) {
    if (sqlDate == null) {
      return null;
    }

    return sqlDate.toLocalDate();
  }
}
