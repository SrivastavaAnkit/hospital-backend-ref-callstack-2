package com.hospital.common.jpa;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/** LocalDate to JPA 2.1 converter. */
@Converter(autoApply = true)
public class NumericDateConverter implements AttributeConverter<LocalDate, String> {

  private static final DateTimeFormatter dateFormatter = DateTimeFormatter.BASIC_ISO_DATE;

  public String convertToDatabaseColumn(LocalDate date) {
    if (date == null) {
      return null;
    }

    String longDate = date.format(dateFormatter);
    String shortDate = (Integer.parseInt(longDate.substring(0, 2)) - 19) + longDate.substring(2);

    return shortDate;
  }

  public LocalDate convertToEntityAttribute(String date) {
    if ((date == null) || "0".equals(date)) {
      return null;
    }

    if (date.length() == 8) {
      // 20161206
      return LocalDate.parse(date, dateFormatter);
    }

    if (date.length() == 6) {
      date = "0" + date;
    }

    String longDate = (Integer.parseInt(date.substring(0, 1)) + 19) + date.substring(1);

    return LocalDate.parse(longDate, dateFormatter);
  }
}
