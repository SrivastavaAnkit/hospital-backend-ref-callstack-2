package com.hospital.common.jpa;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/** LocalTime to JPA 2.1 converter. */
@Converter(autoApply = true)
public class TimeConverter implements AttributeConverter<LocalTime, Integer> {

  private static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HHmmss");

  public Integer convertToDatabaseColumn(LocalTime time) {
    if (time == null) {
      return 0;
    }

    return Integer.parseInt(
        String.format("%02d", time.getHour())
            + String.format("%02d", time.getMinute())
            + String.format("%02d", time.getSecond()));
  }

  public LocalTime convertToEntityAttribute(Integer time) {
    if (time == null) {
      return null;
    }

    try {
      return LocalTime.parse(String.valueOf(time), timeFormatter);
    } catch (IllegalArgumentException e) {
      String timeStr = time.toString();

      for (int i = timeStr.length(); i < 6; i++) {
        timeStr = "0" + timeStr;
      }

      int hourOfDay = Integer.parseInt(timeStr.substring(0, 2));
      int minuteOfHour = Integer.parseInt(timeStr.substring(2, 4));
      int secondOfMinute = Integer.parseInt(timeStr.substring(4));

      return LocalTime.of(hourOfDay, minuteOfHour, secondOfMinute);
    }
  }
}
