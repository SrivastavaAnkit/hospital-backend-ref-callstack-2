package com.hospital.common.jpa;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/** LocalDateTime to JPA 2.1 converter. */
@Converter(autoApply = true)
public class DateTimeConverter implements AttributeConverter<LocalDateTime, String> {

  private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  private static final DateTimeFormatter dateTimeFormatter =
      DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

  public String convertToDatabaseColumn(LocalDateTime date) {
    if (date == null) {
      return null;
    }

    if (date.toString().length() == 10) {
      // 2016-04-29
      return date.format(dateFormatter);
    }

    return date.format(dateTimeFormatter);
  }

  public LocalDateTime convertToEntityAttribute(String date) {
    if (date == null) {
      return null;
    }

    if (date.length() == 10) {
      return LocalDateTime.parse(date, dateFormatter);
    }

    return LocalDateTime.parse(date, dateTimeFormatter);
  }
}
