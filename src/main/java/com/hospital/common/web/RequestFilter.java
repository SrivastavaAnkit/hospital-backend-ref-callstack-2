package com.hospital.common.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;

/**
 * Filters HTTP requests and performs required conversions on the request.
 *
 * @author Robin Rizvi
 * @since (2016-01-04.15:03:12)
 */
@Component(value = "requestFilter")
public class RequestFilter extends OncePerRequestFilter {

  private static final Logger logger = LoggerFactory.getLogger(RequestFilter.class);

  @Autowired private RequestMappingHandlerMapping handlerMapping;

  @Override
  public void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
      throws ServletException, IOException {
    String requestURI = req.getRequestURI();

    // Handle empty path parameters
    if (!requestURI.endsWith(req.getContextPath() + "/")
        && (requestURI.contains("//") || requestURI.endsWith("/"))) {
      boolean urlHandlerFound = false;

      try {
        urlHandlerFound = (handlerMapping.getHandler(req) != null);
      } catch (Exception e) {
        logger.error(e.getMessage(), e);
      }

      // Check if a path match is found on ignoring trailing null path
      // parameters
      if (!requestURI.matches(".*//[^/].*") && urlHandlerFound) {
        chain.doFilter(req, res);
      } else {
        // Append Uri with a trailing slash
        requestURI += "/";
        requestURI = requestURI.replace(req.getContextPath(), "");

        // Replace empty path parameters with null so that Spring
        // dispatcher can map the request
        requestURI = requestURI.replaceAll("/(?=/)", "/null");

        // Decode request url string since request url is not auto
        // decoded when server side redirect is performed
        requestURI = URLDecoder.decode(requestURI, "UTF-8");

        req.getRequestDispatcher(requestURI).forward(req, res);
      }
    } else {
      chain.doFilter(req, res);
    }
  }
}
