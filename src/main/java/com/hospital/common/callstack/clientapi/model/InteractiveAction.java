package com.hospital.common.callstack.clientapi.model;

/** Describe an interactive action that have to be performed by the client. */
public enum InteractiveAction {

  /** Show an interactive screen. */
  SHOW_SCREEN,

  /** The terminal session ended and should be closed. */
  CLOSE_SESSION
}
