package com.hospital.common.callstack.clientapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.callstack.action.CallScreen;
import com.hospital.common.callstack.action.CallService;
import com.hospital.common.callstack.action.ReturnFromService;
import com.hospital.common.callstack.action.Visitor;
import com.hospital.common.callstack.clientapi.model.InteractiveAction;
import com.hospital.common.callstack.clientapi.model.Reply;
import com.hospital.common.callstack.clientapi.model.Request;
import com.hospital.common.callstack.runtime.StateManager;
import com.hospital.common.callstack.runtime.TerminalSession;
import com.hospital.common.callstack.runtime.TerminalSessionInfo;
import com.hospital.common.callstack.runtime.errors.TerminalSessionIllegalStateException;
import com.hospital.common.callstack.runtime.errors.TerminalSessionNotFoundExeception;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

@RestController
@RequestMapping("/v1")
public class ClientApiController {

  public static final String ENV_CLIENT_API_STARTING_SERVICE = "clientapi.starting-service";
  public static final String PARAM_TSID = "tsId";

  // FixMe: Hardcoded guest
  public static final String PRINCIPAL = "guest";

  private final ObjectMapper objectMapper;
  private final StateManager stateManager;
  private final Environment env;

  @Autowired
  public ClientApiController(
      ObjectMapper objectMapper, StateManager stateManager, Environment env) {
    this.objectMapper = objectMapper;
    this.stateManager = stateManager;
    this.env = env;
  }

  /** @return The starting service class. */
  private Class getStartingServiceClass() {
    String className = env.getRequiredProperty(ENV_CLIENT_API_STARTING_SERVICE);
    try {
      return Class.forName(className);
    } catch (ClassNotFoundException e) {
      throw new IllegalStateException("Cannot create the starting service instance", e);
    }
  }

  @PostMapping("session")
  public TerminalSessionInfo createSession() throws Exception {
    TerminalSessionInfo result = stateManager.createSession(PRINCIPAL);

    // Start the initial screen.
    try (TerminalSession terminalSession =
        stateManager.openSessionForAccess(result.getTsId(), PRINCIPAL)) {
      terminalSession.start(getStartingServiceClass(), null);
    }

    return result;
  }

  @GetMapping("/session/{" + PARAM_TSID + "}")
  public TerminalSessionInfo getSession(@PathVariable(PARAM_TSID) String tsId) throws Exception {
    TerminalSessionInfo result;
    try (TerminalSession terminalSession = stateManager.openSessionForAccess(tsId, PRINCIPAL)) {
      result = terminalSession.getInfo();
    }
    return result;
  }

  @GetMapping("/session/{" + PARAM_TSID + "}/action")
  public Request getAction(@PathVariable(PARAM_TSID) String tsId) throws Exception {
    Request request = new Request();

    try (TerminalSession terminalSession = stateManager.openSessionForAccess(tsId, PRINCIPAL)) {

      Long requestId = terminalSession.getSequence();
      com.hospital.common.callstack.action.Action action = terminalSession.getAction();

      if (action == null) {
        // Should not happen but logout.
        action = ReturnFromService.RETURN_NULL;
      }

      request.setRequestId(requestId.toString());
      action.accept(new ActionToRequestVisitor(request));
    }
    return request;
  }

  @PostMapping("/session/{" + PARAM_TSID + "}/action")
  public void replyAction(@PathVariable(PARAM_TSID) String tsId, @RequestBody Reply cmd)
      throws Exception {
    try (TerminalSession terminalSession = stateManager.openSessionForAccess(tsId, PRINCIPAL)) {
      Serializable params =
          (Serializable)
              objectMapper.convertValue(cmd.getModel(), terminalSession.getParameterClass());

      terminalSession.execute(params);
    }
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(TerminalSessionNotFoundExeception.class)
  public void notFoundError(TerminalSessionNotFoundExeception e) {
    // Nothing to do
  }

  @ResponseStatus(HttpStatus.CONFLICT)
  @ExceptionHandler(TerminalSessionIllegalStateException.class)
  public void illegalState(TerminalSessionIllegalStateException e) {
    // Nothing to do
  }

  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(Exception.class)
  public void serverError(Exception e) {
    // Nothing to do
  }

  /** Visitor that maps action to requests. */
  public static class ActionToRequestVisitor implements Visitor<Void> {

    private final Request request;

    public ActionToRequestVisitor(Request request) {
      this.request = request;
    }

    @Override
    public Void visit(CallScreen action) {
      this.request.setScreenId(action.getScreenId());
      this.request.setModel(action.getParams());
      this.request.setAction(InteractiveAction.SHOW_SCREEN);
      return null;
    }

    @Override
    public Void visit(CallService action) {
      throw new IllegalStateException("CallService should not be handled here.");
    }

    @Override
    public Void visit(ReturnFromService action) {
      this.request.setModel(action.getResults());
      this.request.setAction(InteractiveAction.CLOSE_SESSION);
      return null;
    }
  }
}
