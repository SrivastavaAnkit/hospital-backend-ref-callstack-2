package com.hospital.common.callstack;

import com.hospital.common.callstack.runtime.StepMethod;

import java.io.Serializable;

/**
 * Define the basic contract to be able to run a service with the RunningService class.
 * <p>
 * <p>Services should not implement directly this interface but should derive from AbstractService
 * instead.
 *
 * @param <U> The service class
 * @param <T> The state class
 */
public interface Service<U extends Service, T extends Serializable> {

  /**
   * @return The service class. Usually used to get an instance of the class from a IoC framework
   * like Spring.
   */
  Class<U> getServiceClass();

  /**
   * @return The state class. Used to instantiate a state.
   */
  Class<T> getStateClass();

  /**
   * Trade a step for a step method. The method is a pointer to a specific service instance and
   * should not be kept between calls of web services.
   *
   * @param step The Step.
   * @return The StepMethod.
   */
  StepMethod lookup(Step step);

  /**
   * @return The initial step to start this service (entry point).
   */
  Step getInitialStep();
}
