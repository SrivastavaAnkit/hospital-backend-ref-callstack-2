package com.hospital.common.callstack.action;

import java.io.Serializable;
import java.util.Objects;

/**
 * Describe the interactive action of returning from a service. This action is handled in the TerminalSession and is
 * not meant to be sent to the client except for the completion of the root service.
 */
public class ReturnFromService extends Action {

  public static final ReturnFromService RETURN_NULL = new ReturnFromService(null);

  private final Serializable results;

  public ReturnFromService(Serializable results) {
    this.results = results;
  }

  public Serializable getResults() {
    return results;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ReturnFromService that = (ReturnFromService) o;
    return Objects.equals(results, that.results);
  }

  @Override
  public int hashCode() {
    return Objects.hash(results);
  }

  @Override
  public String toString() {
    return "ReturnFromService{" + "results=" + results + '}';
  }

  @Override
  public <T> T accept(Visitor<T> visitor) {
    return visitor.visit(this);
  }
}
