package com.hospital.common.callstack.action;

import java.io.Serializable;

/**
 * Describe the interactive action of calling a service. This action is handled in the TerminalSession and is not meant
 * to be sent to the client.
 */
public class CallService extends Action {

  private final Class service;
  private final Serializable params;

  public CallService(Class service, Serializable params) {
    this.service = service;
    this.params = params;
  }

  public Class getService() {
    return service;
  }

  public Serializable getParams() {
    return params;
  }

  @Override
  public <T> T accept(Visitor<T> visitor) {
    return visitor.visit(this);
  }
}
