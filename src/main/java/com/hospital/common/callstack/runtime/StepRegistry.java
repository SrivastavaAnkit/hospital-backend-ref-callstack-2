package com.hospital.common.callstack.runtime;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

/** Class to keep tracks of available steps for a specific service. */
public class StepRegistry<U, S extends Serializable> {

  private final Class<U> serviceClass;
  private final Class<S> stateClass;
  private final Map<Step, StepMethod> stepMap;

  public StepRegistry(Class<U> serviceClass, Class<S> stateClass) {
    Objects.requireNonNull(serviceClass);
    Objects.requireNonNull(stateClass);
    this.serviceClass = serviceClass;
    this.stateClass = stateClass;
    this.stepMap = new HashMap<>(4);
  }

  public Step define(String stepId, Function<S, StepResult> function) {
    return addToRegistry(new Step(serviceClass, stepId), StepMethod.create(stateClass, function));
  }

  public Step define(String stepId, Consumer<S> function) {
    return addToRegistry(new Step(serviceClass, stepId), StepMethod.create(stateClass, function));
  }

  public <Q> Step define(
      String stepId, Class<Q> parametersClass, BiFunction<S, Q, StepResult> function) {
    StepMethod stepMethod = StepMethod.create(stateClass, parametersClass, function);
    Step tag = new Step(serviceClass, stepId, parametersClass, null);
    return addToRegistry(tag, stepMethod);
  }

  public <Q> Step define(String stepId, Class<Q> parametersClass, BiConsumer<S, Q> function) {
    StepMethod stepMethod = StepMethod.create(stateClass, parametersClass, function);
    Step tag = new Step(serviceClass, stepId, parametersClass, null);
    return addToRegistry(tag, stepMethod);
  }

  private Step addToRegistry(Step step, StepMethod method) {
    if (stepMap.containsKey(step)) {
      throw new IllegalStateException("Duplicate step tag:" + step.toString());
    }
    stepMap.put(step, method);
    return step;
  }

  public Class<U> getServiceClass() {
    return serviceClass;
  }

  public Class<S> getStateClass() {
    return stateClass;
  }

  public StepMethod lookup(Step step) {
    StepMethod result = stepMap.get(step);
    if (result == null) {
      throw new IllegalStateException("Step not found.");
    }
    return result;
  }
}
