package com.hospital.common.callstack.runtime;

import com.hospital.common.callstack.Service;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.action.Action;
import com.hospital.common.callstack.action.ReturnFromService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;

/**
 * This class represent an instance of a running service and is a composition of a state and a service class.
 *
 * @param <U> The service class.
 * @param <T> The state class.
 */
public class RunningService<U extends Service<U, T>, T extends Serializable>
    implements Serializable {

  private static final Logger logger = LoggerFactory.getLogger(RunningService.class);

  private final Class<U> serviceClass;
  private final T state;
  private final Deque<Step> steps = new ArrayDeque<>();

  public RunningService(Service<U, T> service) {
    try {

      // Keep the service class for deserialization purpose.
      this.serviceClass = service.getServiceClass();

      // Build a state
      Constructor<T> c = service.getStateClass().getConstructor();
      this.state = c.newInstance();

      // Stack the first steps
      steps.add(service.getInitialStep());

    } catch (Exception e) {
      throw new IllegalStateException(e);
    }
  }

  public Class<U> getServiceClass() {
    return serviceClass;
  }

  public Class<?> getCurrentStepParameterClass() {
    if (steps.size() > 0) {
      return steps.peek().getParamsClass();
    } else {
      return Void.class;
    }
  }

  public T getState() {
    return state;
  }

  public Action execute(Service<U, T> service, Serializable params) {
    StepResult result;
    do {
      if (steps.size() > 0) {

        Step step = this.steps.pop();
        StepMethod method = service.lookup(step);
        logger.trace("Executing step {}.", step, service);
        if (params != null) {

          // Both parameters cannot be null
          if (step.getParams() != null) {
            throw new IllegalStateException();
          }

          result = method.call(state, params);
          params = null;
        } else {
          result = method.call(state, step.getParams());
        }

        // Add all additional steps to the stack.
        for (int i = result.getSteps().size() - 1; i >= 0; i--) {
          Step c = result.getSteps().get(i);
          steps.push(c);
        }

      } else {
        return ReturnFromService.RETURN_NULL;
      }
    } while (result.getAction() == null);
    return result.getAction();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    RunningService<?, ?> that = (RunningService<?, ?>) o;
    return Objects.equals(state, that.state)
        && Objects.equals(steps.size(), that.steps.size())
        && steps.containsAll(that.steps);
  }

  @Override
  public int hashCode() {
    return Objects.hash(state, steps);
  }

  @Override
  public String toString() {
    return "ServiceRunner{" + "state=" + state + ", steps=" + steps + '}';
  }
}
