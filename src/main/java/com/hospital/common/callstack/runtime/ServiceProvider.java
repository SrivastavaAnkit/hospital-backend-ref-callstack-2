package com.hospital.common.callstack.runtime;

import com.hospital.common.callstack.Service;

/**
 * Provides service instances to Terminal Sessions.
 *
 * @param <T> Service class.
 */
@FunctionalInterface
public interface ServiceProvider<T extends Service> {

  T get(Class<T> serviceClass);
}
