package com.hospital.common.callstack.runtime.errors;

/**
 * Terminal session has encountered an illegal state.
 */
public class TerminalSessionIllegalStateException extends TerminalSessionException {

  public TerminalSessionIllegalStateException() {
  }

  public TerminalSessionIllegalStateException(String message) {
    super(message);
  }

  public TerminalSessionIllegalStateException(String message, Throwable cause) {
    super(message, cause);
  }

  public TerminalSessionIllegalStateException(Throwable cause) {
    super(cause);
  }

  public TerminalSessionIllegalStateException(
          String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
