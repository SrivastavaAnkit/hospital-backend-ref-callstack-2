package com.hospital.common.callstack;

import java.io.Serializable;
import java.util.Objects;

/**
 * Define a step inside a service.
 *
 * <p>Note: Two steps with different parameters are considered equals.
 */
public class Step implements Serializable {

  private final Class<?> serviceClass;
  private final String stepId;
  private final Class<?> paramsClass;
  private final Serializable params;

  public Step(Class<?> serviceClass, String stepId, Class<?> paramsClass, Serializable params) {
    this.paramsClass = paramsClass;
    Objects.requireNonNull(serviceClass);
    Objects.requireNonNull(stepId);

    this.serviceClass = serviceClass;
    this.stepId = stepId;
    this.params = params;
  }

  public Step(Class<?> serviceClass, String stepId) {
    this(serviceClass, stepId, Void.class, null);
  }

  /**
   * @return The service class this step is attached to.
   */
  public Class<?> getServiceClass() {
    return serviceClass;
  }

  /**
   * @return A unique identifier within the service class for this step.
   */
  public String getStepId() {
    return stepId;
  }

  /**
   * @return The parameter class or Void.class for no parameters.
   */
  public Class<?> getParamsClass() {
    return paramsClass;
  }

  /**
   * @return The parameters attached to this step or null.
   */
  public Serializable getParams() {
    return params;
  }

  /** @return A new step object pointing to same entry but with the specified parameters. */
  public Step withParams(Serializable params) {
    return new Step(getServiceClass(), getStepId(), paramsClass, params);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Step step = (Step) o;
    return Objects.equals(serviceClass, step.serviceClass) && Objects.equals(stepId, step.stepId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(serviceClass, stepId);
  }

  @Override
  public String toString() {
    return "Step{"
        + "serviceClass="
        + serviceClass
        + ", stepId='"
        + stepId
        + '\''
        + ", params="
        + params
        + '}';
  }
}
