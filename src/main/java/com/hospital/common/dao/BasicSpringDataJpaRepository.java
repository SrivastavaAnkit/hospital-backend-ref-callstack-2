package com.hospital.common.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

/**
 * Spring Data JPA repository interface for model.
 *
 * @author Martin Paquin
 */
@NoRepositoryBean
public interface BasicSpringDataJpaRepository<T extends JpaBaseEntity<S>, S extends Serializable>
    extends JpaRepository<T, S>, JpaSpecificationExecutor<T> {}
