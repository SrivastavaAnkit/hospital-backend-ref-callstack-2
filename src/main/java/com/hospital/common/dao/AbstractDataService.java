package com.hospital.common.dao;

import com.hospital.common.exception.ServiceException;
import com.hospital.common.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * Service implementation for Creating, Changing and Deleting record.
 *
 * @author Martin Paquin
 */
@Service
public abstract class AbstractDataService<T extends JpaBaseEntity<S>, S extends Serializable>
    implements DataService<T, S> {

  /** Default page size. */
  private static final int DEFAULT_PAGE_SIZE = 10;

  private Logger log = LoggerFactory.getLogger(this.getClass());

  private BasicSpringDataJpaRepository<T, S> repository;

  @Autowired
  @SuppressWarnings({
    "SpringJavaInjectionPointsAutowiringInspection",
    "SpringJavaAutowiredFieldsWarningInspection"
  })
  private Environment environment;

  /** {@link DataService#count(Specification)} */
  @Override
  public Long count(Specification<T> spec) {
    return repository.count(spec);
  }

  /** {@link DataService#create(T)} */
  public Result<T> create(T entity) throws ServiceException {
    LastIO lastIO = new LastIO();

    if (exists(entity)) {
      lastIO.setError(true);
      lastIO.setFound(true);

      // REVIEW Why this exception compare to EntityExistsException ?
      throw new ServiceException("Object already exists");
    }

    try {
      // REVIEW Exception might not occurs within the boundary of this block so the setting of the
      // error flag is
      // not guaranteed.
      // REVIEW Catching Exception is too large and might hide other issues. What are the cases we
      // want to trap ?
      // REVIEW this is inconsistent with the first if(exists) bloc where an exception is thrown is
      // an error occurs.
      entity = repository.save(entity);
    } catch (Exception e) {
      log.debug("create", e);
      lastIO.setError(true);
    }

    return new Result<>(entity, lastIO);
  }

  /** {@link DataService#delete(Object)} */
  public LastIO delete(T entity) throws ServiceException {
    if (entity == null) {
      LastIO lastIO = new LastIO();
      lastIO.setError(true);
      lastIO.setFound(false);

      // REVIEW should be EntityNotFoundException ?
      throw new ServiceException("Object doesn't exist");
    }

    // REVIEW this is inconsistent with other methods, including the other delete method, where the
    // actual
    // operation is framed within a try/catch
    return delete(entity.getId());
  }

  /** {@link DataService#delete(Object)} */
  public LastIO delete(S id) throws ServiceException {
    LastIO lastIO = new LastIO();

    if ((id == null) || !exists(id)) {
      lastIO.setError(true);
      lastIO.setFound(false);

      // REVIEW should be EntityNotFoundException ?
      throw new ServiceException("Object doesn't exist");
    }

    try {
      repository.deleteById(id);
      // REVIEW Catching Exception is too large and might hide other issues. What are the cases we
      // want to trap ?
      // REVIEW this is inconsistent with the first if(exists) bloc where an exception is thrown is
      // an error occurs.
    } catch (Exception e) {
      log.debug("delete", e);
      lastIO.setError(true);
    }

    return lastIO;
  }

  /** {@link DataService#delete(Object)} */
  public LastIO delete(Specification<T> spec) {
    LastIO lastIO = new LastIO();

    if (spec == null) {
      // REVIEW this is inconsistent with other blocks where exception is thrown upon errors
      // REVIEW this code is duplicated everywhere.
      lastIO.setError(true);
      lastIO.setFound(false);
    } else {
      Optional<T> entity = repository.findOne(spec);

      if (!entity.isPresent()) {
        // REVIEW this is inconsistent with other blocks where exception is thrown upon errors
        // REVIEW this code is duplicated everywhere.
        lastIO.setError(true);
        lastIO.setFound(false);
      } else {
        try {
          repository.delete(entity.get());
          // REVIEW Exception might not occurs within the boundary of this block so the setting of
          // the
          // error flag is
          // not guaranteed.
          // REVIEW Catching Exception is too large and might hide other issues. What are the cases
          // we
          // want to trap ?
        } catch (Exception e) {
          log.debug("delete", e);
          // REVIEW this is inconsistent with other blocks where exception is thrown upon errors
          // REVIEW inconsistency where everywhere else we set found but not here.
          lastIO.setError(true);
        }
      }
    }

    return lastIO;
  }

  @Override
  public boolean exists(T entity) {
    return exists(entity.getId());
  }

  @Override
  public boolean exists(S id) {
    return repository.existsById(id);
  }

  /** {@link DataService#findAll()} */
  @Override
  public ResultPage<T, S> findAll() {
    return this.findAll(DEFAULT_PAGE_SIZE);
  }

  /** {@link DataService#findAll(int)} */
  @Override
  public ResultPage<T, S> findAll(int pageSize) {
    return findAll(pageSize, null);
  }

  /** {@link DataService#findAll(Sort)} */
  @Override
  public ResultPage<T, S> findAll(Sort sort) {
    return findAll(DEFAULT_PAGE_SIZE, sort);
  }

  /** {@link DataService#findAll(int, Sort)} */
  @Override
  @SuppressWarnings("ConstantConditions")
  public ResultPage<T, S> findAll(int pageSize, Sort sort) {
    LastIO lastIO = new LastIO();
    Pageable pageable = PageRequest.of(0, pageSize, sort);
    Page<T> page = null;

    try {
      page = repository.findAll(pageable);
      lastIO.setAtEOF(page.getContent().isEmpty());
      lastIO.setFound(!page.getContent().isEmpty());
      lastIO.setError(false);
      // REVIEW Exception might not occurs within the boundary of this block so the setting of the
      // error flag is
      // not guaranteed.
      // REVIEW Catching Exception is too large and might hide other issues. What are the cases we
      // want to trap ?
    } catch (Exception e) {
      log.debug("create", e);
      // REVIEW this is inconsistent with other blocks where exception is thrown upon errors
      // REVIEW inconsistency where everywhere else we set found but not here.
      lastIO.setError(true);
    }

    return new ResultPage<>(page, lastIO, this);
  }

  /** {@link DataService#findAll(Specification)} */
  @Override
  public ResultPage<T, S> findAll(Specification<T> spec) {

    // REVIEW What is this code used for ?
    // REVIEW depends on a loosely couple convention of string and is therefore fragile. Need a test
    // for this.
    if (Utils.getDBName(environment).startsWith("DB2")) {
      List<T> content = null;
      LastIO lastIO = new LastIO();

      try {
        content = repository.findAll(spec);
        lastIO.setAtEOF(content.isEmpty());
        lastIO.setFound(!content.isEmpty());

        // REVIEW, why is it an error to not find anything ?
        lastIO.setError(content.isEmpty());
        // REVIEW Exception might not occurs within the boundary of this block so the setting of the
        // error flag is
        // not guaranteed.
        // REVIEW Catching Exception is too large and might hide other issues. What are the cases we
        // want to trap ?
      } catch (Exception e) {
        log.debug("create", e);
        // REVIEW this is inconsistent with other blocks where exception is thrown upon errors
        // REVIEW inconsistency where everywhere else we set found but not here.
        lastIO.setError(true);
      }

      return new ResultPage<>(content, lastIO, this);
    }

    Pageable pageable = PageRequest.of(0, DEFAULT_PAGE_SIZE);

    return findAll(spec, pageable);
  }

  public ResultPage<T, S> findAll(Specification<T> spec, Sort sort) {
    return findAll(spec, PageRequest.of(0, DEFAULT_PAGE_SIZE, sort));
  }

  /** {@link DataService#findAll(Specification, Pageable)} */
  @Override
  public ResultPage<T, S> findAll(Specification<T> spec, Pageable pageable) {
    LastIO lastIO = new LastIO();
    Page<T> page = null;

    try {
      page = repository.findAll(spec, pageable);
      lastIO.setAtEOF(!page.hasContent());
      lastIO.setFound(!page.getContent().isEmpty());
      lastIO.setError(false);
      // REVIEW Exception might not occurs within the boundary of this block so the setting of the
      // error flag is
      // not guaranteed.
      // REVIEW Catching Exception is too large and might hide other issues. What are the cases we
      // want to trap ?
    } catch (Exception e) {
      log.debug("create", e);
      // REVIEW this is inconsistent with other blocks where exception is thrown upon errors
      // REVIEW inconsistency where everywhere else we set found but not here.
      lastIO.setError(true);
    }

    // REVIEW  use a constant for 10
    if (pageable.equals(PageRequest.of(0, 10))) {
      // Allow reading from next page
      pageable = null;
    }

    // Read from current page only
    return new ResultPage<>(spec, pageable, page, lastIO, this);
  }

  /** {@link DataService#findAll(Pageable)} */
  // REVIEW why do we have a second abstraction for findAll ? Page vs ResultPage
  @Override
  public Page<T> findAll(Pageable pageable) {
    return repository.findAll(pageable);
  }

  /** {@link DataService#findAll(Pageable)} */
  @Override
  // REVIEW why do we have a second abstraction for findAll ? Page vs ResultPage
  public Page<T> findAll(Pageable pageable, Specification<T> spec) {
    return repository.findAll(spec, pageable);
  }

  /** {@link DataService#findOne(Specification)} */
  @Override
  public Result<T> findOne(Specification<T> spec) {
    LastIO lastIO = new LastIO();
    Optional<T> entity;

    try {
      entity = repository.findOne(spec);
      lastIO.setAtEOF(!entity.isPresent());
      lastIO.setFound(entity.isPresent());
      lastIO.setError(!entity.isPresent());
      if (entity.isPresent()) {
        return new Result<>(entity.get(), lastIO);
      }
      // REVIEW Exception might not occurs within the boundary of this block so the setting of the
      // error flag is
      // not guaranteed.
      // REVIEW Catching Exception is too large and might hide other issues. What are the cases we
      // want to trap ?
    } catch (Exception e) {
      log.debug("create", e);
      // REVIEW this is inconsistent with other blocks where exception is thrown upon errors
      // REVIEW What eof should be set to ?
      lastIO.setError(true);
      lastIO.setFound(false);
    }

    return new Result<>(null, lastIO);
  }

  /** {@link DataService#findOne(Object)} */
  @Override
  public Result<T> findOne(S id) {
    LastIO lastIO = new LastIO();
    Optional<T> entity;

    try {
      entity = repository.findById(id);
      lastIO.setAtEOF(!entity.isPresent());
      lastIO.setFound(entity.isPresent());
      lastIO.setError(!entity.isPresent());
      if (entity.isPresent()) {
        return new Result<>(entity.get(), lastIO);
      }
      // REVIEW Exception might not occurs within the boundary of this block so the setting of the
      // error flag is
      // not guaranteed.
      // REVIEW Catching Exception is too large and might hide other issues. What are the cases we
      // want to trap ?
    } catch (Exception e) {
      // REVIEW this is inconsistent with other blocks where exception is thrown upon errors
      // REVIEW What eof should be set to ?
      log.debug("findOne(id)", e);
      lastIO.setError(true);
      lastIO.setFound(false);
    }

    return new Result<>(null, lastIO);
  }

  public void setRepository(BasicSpringDataJpaRepository<T, S> repo) {
    this.repository = repo;
  }

  /** {@link DataService#update(JpaBaseEntity)} */
  public Result<T> update(T entity) throws ServiceException {
    LastIO lastIO = new LastIO();

    if ((entity == null) || !exists(entity)) {
      lastIO.setError(true);
      lastIO.setFound(false);

      throw new ServiceException("Object doesn't exist");
    }

    try {
      entity = repository.save(entity);
      lastIO.setError(false);
      lastIO.setFound(true);
      // REVIEW Exception might not occurs within the boundary of this block so the setting of the
      // error flag is
      // not guaranteed.
      // REVIEW Catching Exception is too large and might hide other issues. What are the cases we
      // want to trap ?
    } catch (Exception e) {
      // REVIEW this is inconsistent with other blocks where exception is thrown upon errors
      // REVIEW What eof should be set to ?
      log.debug("update", e);
      lastIO.setError(true);
      lastIO.setFound(false);
    }

    return new Result<>(entity, lastIO);
  }
}
