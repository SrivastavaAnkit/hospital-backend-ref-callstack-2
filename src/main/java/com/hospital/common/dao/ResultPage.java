package com.hospital.common.dao;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.io.Serializable;
import java.util.List;

/**
 * This class manages result from DataService. It is an helper class to handle some RPG constructs
 * that we can't convert in first version of XM3.
 *
 * @author Martin Paquin
 */
public class ResultPage<T extends JpaBaseEntity<S>, S> implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonIgnore private DataService<T, S> dataService;

  private final LastIO lastIO;
  private Page<T> currentPage;
  private Pageable pageable;
  private Specification<T> spec;
  private int currentIndex = 0;
  private long currentRRN = 1;

  /**
   * Constructor.
   *
   * @param content
   * @param lastIO
   * @param dataService
   */
  public ResultPage(List<T> content, LastIO lastIO, DataService<T, S> dataService) {
    currentPage = new PageImpl<T>(content);
    this.lastIO = lastIO;
  }

  /**
   * Constructor.
   *
   * @param page
   * @param lastIO
   * @param dataService
   */
  public ResultPage(Page<T> page, LastIO lastIO, DataService<T, S> dataService) {
    currentPage = page;
    this.lastIO = lastIO;
    this.dataService = dataService;
  }

  /**
   * Constructor.
   *
   * @param spec
   * @param pageable
   * @param page
   * @param lastIO
   * @param dataService
   */
  public ResultPage(
      Specification<T> spec,
      Pageable pageable,
      Page<T> page,
      LastIO lastIO,
      DataService<T, S> dataService) {
    this.spec = spec;
    this.pageable = pageable;
    currentPage = page;
    this.lastIO = lastIO;
    this.dataService = dataService;
  }

  /**
   * Constructor.
   *
   * @param content
   * @param endOfFile
   * @param error
   * @param equal
   * @param found
   */
  @JsonCreator
  public ResultPage(
      @JsonProperty("content") List<T> content,
      @JsonProperty("endOfFile") Boolean endOfFile,
      @JsonProperty("error") Boolean error,
      @JsonProperty("equal") Boolean equal,
      @JsonProperty("found") Boolean found) {
    currentPage = new PageImpl<T>(content);
    this.lastIO = new LastIO();
    this.lastIO.setAtEOF(endOfFile);
    this.lastIO.setEqual(equal);
    this.lastIO.setError(error);
    this.lastIO.setFound(found);
  }

  /**
   * Returns Current index.
   *
   * @return Current index
   */
  private int geCurrentIndex() {
    int index =
        ((currentRRN % currentPage.getSize()) == 0)
            ? (int) (currentRRN - 1)
            : (int) ((currentRRN % currentPage.getSize()) - 1);

    return index;
  }

  /** @return The last IO result associated with this result page. */
  public LastIO getLastIO() {
    return lastIO;
  }

  /**
   * Returns list of elements for current page.
   *
   * @return list of elements for current page
   */
  public List<T> getContent() {
    return currentPage.getContent();
  }

  /**
   * Returns the total of elements according to the query.
   *
   * @return the total of elements according to the query
   */
  public long getCount() {
    return currentPage.getTotalElements();
  }

  /*
   * Gets the RRN according to spec.
   *
   * @param spec
   * @return record number
   */
  private int getRRN(Specification<T> spec) {
    T entity = (dataService.count(spec) > 0) ? dataService.findAll(spec).getContent().get(0) : null;

    int index = currentPage.getContent().indexOf(entity);
    Page<T> newCurrentPage = currentPage;

    while ((index == -1) && newCurrentPage.hasNext()) {
      Pageable pageable = newCurrentPage.nextPageable();

      try {
        newCurrentPage = dataService.findAll(pageable);
        lastIO.setEqual(true);
        lastIO.setAtEOF(true);
        lastIO.setError(false);
        lastIO.setFound(true);
      } catch (Exception e) {
        lastIO.setEqual(false);
        lastIO.setAtEOF(true);
        lastIO.setError(true);
        lastIO.setFound(false);
      }

      index =
          (newCurrentPage.getContent().indexOf(entity) == -1)
              ? newCurrentPage.getContent().indexOf(entity)
              : ((newCurrentPage.getSize() * newCurrentPage.getNumber())
                  + newCurrentPage.getContent().indexOf(entity));
    }

    return index + 1;
  }

  public boolean hasNext() {
    return (currentPage != null)
        && (currentPage.getTotalElements() >= currentRRN)
        && !lastIO.isEndOfFile();
  }

  public boolean hasPrevious() {
    return (currentPage != null) && (currentRRN > 0) && !lastIO.isEndOfFile();
  }

  /**
   * Returns true is we are at the end of file or collection resulting of a query.
   *
   * @return true is we are at the end of file or collection resulting of a query
   */
  public boolean isEndOfFile() {
    return lastIO.isEndOfFile();
  }

  public boolean isEqual() {
    return lastIO.isEqual();
  }

  /**
   * Returns true if an error occurs during call to the data service.
   *
   * @return true if an error occurs during call to the data service
   */
  public boolean isError() {
    return lastIO.isError();
  }

  /**
   * Returns true if the request returns results.
   *
   * @return true if the request returns results
   */
  public boolean isFound() {
    return lastIO.isFound();
  }

  /**
   * Reads data.
   *
   * @return the object at current position in the virtual array and increment counter by 1
   */
  public T read() {
    T entity = null;

    // We are at end of file
    if ((currentRRN > currentPage.getTotalElements())
        || ((pageable != null) && (currentRRN > currentPage.getSize()))) {
      lastIO.setEqual(false);
      lastIO.setAtEOF(true);
      lastIO.setError(true);
      lastIO.setFound(false);
    } else {
      int nextPage = currentPage.getNumber() + 1;

      if (currentRRN > (nextPage * currentPage.getSize())) {
        Pageable pageable = PageRequest.of(nextPage, currentPage.getSize(), currentPage.getSort());

        if (spec != null) {
          currentPage = dataService.findAll(pageable, spec);
        } else {
          currentPage = dataService.findAll(pageable);
        }

        currentIndex = 0;
      }

      entity = currentPage.getContent().get(currentIndex);

      if (currentIndex < currentPage.getSize()) {
        currentIndex++;
        currentRRN++;
      }
    }

    return entity;
  }

  /**
   * Reads previous data.
   *
   * @return the object at current position in the virtual array and decrement counter by 1
   */
  public T readPrevious() {
    T entity = null;

    // We are at end of file
    if ((currentRRN > currentPage.getTotalElements()) || (currentRRN < 2)) {
      lastIO.setEqual(false);
      lastIO.setAtEOF(true);
      lastIO.setError(true);
      lastIO.setFound(false);
    } else {
      if ((currentRRN > 1) && ((currentRRN % currentPage.getSize()) == 0)) {
        Pageable pageable =
            PageRequest.of(
                currentPage.getNumber() - 1, currentPage.getSize(), currentPage.getSort());
        currentPage = dataService.findAll(pageable);
        currentIndex = currentPage.getNumberOfElements() - 1;
      }

      entity = currentPage.getContent().get(currentIndex - 1);

      if (currentIndex < currentPage.getSize()) {
        currentIndex--;
        currentRRN--;
      }
    }

    return entity;
  }

  /**
   * Positions the current record according to spec. Replaces RPG SETLL and SETGT opcodes.
   *
   * @param spec
   */
  @JsonIgnore
  public void setAt(Specification<T> spec) {
    currentRRN = getRRN(spec);
    currentIndex = geCurrentIndex();

    int newCurrentPage = (int) currentRRN / currentPage.getSize();
    Pageable pageable =
        PageRequest.of(newCurrentPage, currentPage.getSize(), currentPage.getSort());

    try {
      currentPage = dataService.findAll(pageable);
      lastIO.setEqual(true);
      lastIO.setAtEOF(false);
      lastIO.setError(false);
      lastIO.setFound(true);
    } catch (Exception e) {
      lastIO.setEqual(false);
      lastIO.setAtEOF(true);
      lastIO.setError(true);
      lastIO.setFound(false);
    }
  }

  /**
   * Positions the current record according to RRN. Replaces RPG SETLL and SETGT opcodes.
   *
   * @param rrn
   */
  public void setAt(long rrn) {
    Long count = currentPage.getTotalElements();
    currentRRN = (rrn < count) ? rrn : count;
    currentIndex = geCurrentIndex();

    int newCurrentPage =
        (rrn <= currentPage.getTotalElements())
            ? (int) (rrn / currentPage.getSize())
            : (currentPage.getTotalPages() - 1);
    Pageable pageable =
        PageRequest.of(newCurrentPage, currentPage.getSize(), currentPage.getSort());

    try {
      currentPage = dataService.findAll(pageable);
      lastIO.setEqual(true);
      lastIO.setAtEOF(true);
      lastIO.setError(false);
      lastIO.setFound(true);
    } catch (Exception e) {
      lastIO.setEqual(false);
      lastIO.setAtEOF(true);
      lastIO.setError(true);
      lastIO.setFound(false);
    }
  }
}
