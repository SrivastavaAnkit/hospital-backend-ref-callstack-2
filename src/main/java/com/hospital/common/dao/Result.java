package com.hospital.common.dao;

/**
 * Result class.
 *
 * @author Martin Paquin
 */
public class Result<T> {
  private LastIO lastIO = new LastIO();
  private final T content;

  /**
   * Constructor.
   *
   * @param content
   * @param lastIO
   */
  public Result(T content, LastIO lastIO) {
    this.lastIO = lastIO;
    this.content = content;
  }

  /** Gets content. */
  public T get() {
    return content;
  }

  /** @return The last IO associated with this result. */
  public LastIO getLastIO() {
    return lastIO;
  }

  /**
   * Checks if at end of file.
   *
   * @return true if at end of file.
   */
  public boolean isEndOfFile() {
    return lastIO.isEndOfFile();
  }

  /**
   * Checks if the object we query for is equal to the result.
   *
   * @return true if the object we query for is equal to the result
   */
  public boolean isEqual() {
    return lastIO.isEqual();
  }

  /**
   * Checks if an error occurs during call to data service.
   *
   * @return true if an error occurs during call to data service
   */
  public boolean isError() {
    return lastIO.isError();
  }

  /**
   * Checks if the query returns the request object.
   *
   * @return true if the query returns the request object
   */
  public boolean isFound() {
    return lastIO.isFound();
  }
}
