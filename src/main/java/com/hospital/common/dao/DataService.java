package com.hospital.common.dao;

import com.hospital.common.exception.ServiceException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

/**
 * Encapsulate access to database.
 *
 * @author Martin Paquin
 */
public interface DataService<T extends JpaBaseEntity<S>, S> {

  /** Returns the number of record according to specification criteria. */
  Long count(Specification<T> spec);

  /**
   * Persists a new entity in database.
   *
   * @return the save entity
   */
  Result<T> create(T entity) throws ServiceException;

  /** Deletes an existing entity from the database. */
  LastIO delete(T entity) throws ServiceException;

  /** Deletes an existing entity from the database. */
  LastIO delete(S id) throws ServiceException;

  /**
   * Deletes an existing entity from the database.
   *
   * @param spec Specification to find object to delete
   */
  LastIO delete(Specification<T> spec);

  /**
   * Finds if the entity exists.
   *
   * @return true if the entity exists
   */
  boolean exists(T entity);

  boolean exists(S id);

  /** Finds all entities of a table with a page size of 10. */
  ResultPage<T, S> findAll();

  /** Finds all entities of a table. */
  ResultPage<T, S> findAll(int pageSize);

  /**
   * Finds all entities of a table.
   *
   * @param sort the sort criteria
   */
  ResultPage<T, S> findAll(Sort sort);

  /**
   * Finds all entities of a table.
   *
   * @param sort the sort criteria
   */
  ResultPage<T, S> findAll(int pageSize, Sort sort);

  /** Finds all entities according to specification argument. */
  ResultPage<T, S> findAll(Specification<T> spec);

  /** Finds all entities according to specification argument. */
  ResultPage<T, S> findAll(Specification<T> spec, Pageable pageable);

  /** Finds all entities by page. */
  Page<T> findAll(Pageable pageable);

  /** Finds all entities by page. */
  Page<T> findAll(Pageable pageable, Specification<T> spec);

  /** Finds one entity according to specification argument. */
  Result<T> findOne(Specification<T> spec);

  /** Finds one record by ID. */
  Result<T> findOne(S id);

  /** Updates an entity in database. */
  Result<T> update(T entity) throws ServiceException;
}
