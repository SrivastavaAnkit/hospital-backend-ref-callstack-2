package com.hospital.common.dao;

import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * Base class for JPA entity. K is the ID class type. Allows to add common columns, filter
 * definitions etc.
 *
 * @author Martin Paquin
 */
@MappedSuperclass
@FilterDef(
  name = "COMPANY_FILTER",
  parameters = {@ParamDef(name = "companyid", type = "int")},
  defaultCondition = "companyid=:companyid"
)
public abstract class JpaBaseEntity<K> implements Serializable {
  private static final long serialVersionUID = 1L;

  public abstract K getId();
}
