package com.hospital.common.exception;

import com.hospital.common.state.BaseDTO;

public class ServiceException extends RuntimeException {

  private static final long serialVersionUID = 1L;
  private BaseDTO dto;

  public ServiceException() {}

  public ServiceException(String message) {
    super(message);
  }

  public ServiceException(String message, BaseDTO dto) {
    super(message);
    this.dto = dto;
  }

  public ServiceException(String message, Throwable cause) {
    super(message, cause);
  }

  public ServiceException(String message, BaseDTO dto, Throwable cause) {
    super(message, cause);
    this.dto = dto;
  }

  public ServiceException(
      String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public BaseDTO getDto() {
    return dto;
  }

  public void setDto(BaseDTO dto) {
    this.dto = dto;
  }

  /** Gets the message and the message from the cause exceptions. */
  public String getCompleteMessage() {
    StringBuilder stringBuilder = new StringBuilder(512);
    stringBuilder.append(getMessage());
    Throwable cause = getCause();
    while (cause != null) {
      stringBuilder.append(" ; ").append(cause.getMessage());
      cause = cause.getCause();
    }
    return stringBuilder.toString();
  }
}
