package com.hospital.common.exception;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.hospital.common.exception.ExceptionDTO;
import com.hospital.common.exception.ServiceException;

/**
 * Handles exception and returns Restful responses to the client for indicating
 * the exception.
 *
 * @author Robin Rizvi
 * @since (2015-11-04.15:03:12)
 */
@ControllerAdvice
public class GlobalControllerExceptionHandler {
	private static final Logger logger = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);
	@Autowired
	MessageSource messageSource;

	/**
	 * Gets the request details for logging.
	 *
	 * @param req HttpServletRequest object
	 * @return Request details
	 */
	private String getRequestDetails(HttpServletRequest req) {
		
		Map<String, String> requestMap = new HashMap<String, String>();
		Enumeration<?> requestParamNames = req.getParameterNames();

		while (requestParamNames.hasMoreElements()) {
			String requestParamName = (String)requestParamNames.nextElement();
			String requestParamValue = req.getParameter(requestParamName);

			requestMap.put(requestParamName, requestParamValue);
		}

		StringBuilder requestDetails =
			new StringBuilder("Request Details:\n").append("HTTP METHOD: ")
												   .append(req.getMethod())
												   .append("\nPATH INFO: ")
												   .append(req.getRequestURI());
		requestDetails.append("\nREQUEST PARAMETERS: ").append(requestMap)
					  .append("\nREMOTE ADDRESS: ").append(req.getRemoteAddr());

		return requestDetails.toString();
	}

	/**
	 * Handles exceptions that are not handled by other exception handlers.
	 *
	 * @param req HttpServletRequest object
	 * @param ex Exception object
	 * @return {@link ExceptionDTO}
	 */
	@ExceptionHandler(value=Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ExceptionDTO handleException(HttpServletRequest req, Exception ex) {
		
		logger.debug(getRequestDetails(req));
		logger.debug("Exception occurred", ex);

		ExceptionDTO response = new ExceptionDTO();

		response.setCode(Integer.toString(
				HttpStatus.INTERNAL_SERVER_ERROR.value()));
		response.setMessage(ex.getMessage());

		return response;
	}

	/**
	 * Handles service exceptions and sends the corresponding exception message
	 * to the client.
	 *
	 * @param req HttpServletRequest object
	 * @param ex Exception object
	 *
	 * @return {@link ExceptionDTO}
	 */
	@ExceptionHandler(value=ServiceException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ExceptionDTO handleServiceException(HttpServletRequest req, ServiceException ex) {
		
		logger.debug(getRequestDetails(req));
		logger.debug("Service exception occurred", ex);

		ExceptionDTO response = new ExceptionDTO();
		
		String[] parts = ex.getMessage().split(Pattern.quote("."));
		if(parts[1].equals("nf")) {
			response.setCode(Integer.toString(HttpStatus.NOT_FOUND.value()));
		} else if(parts[1].equals("ex")) {
			response.setCode(Integer.toString(HttpStatus.CONFLICT.value()));
		} else {
			response.setCode(Integer.toString(HttpStatus.BAD_REQUEST.value()));
		}
		
		response.setMessage(messageSource.getMessage(ex.getMessage(), null,
				"Service Exception", null));

		return response;
	}

	/**
	 * Handles field validation exceptions and returns fields and their
	 * corresponding validation errors to the client.
	 *
	 * @param req HttpServletRequest object
	 * @param ex Exception object
	 * 
	 * @return {@link ExceptionDTO}
	 */
	@ExceptionHandler(value=MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
	@ResponseBody
	public ExceptionDTO handleValidationException(HttpServletRequest req,	MethodArgumentNotValidException ex) {
		
		logger.debug(getRequestDetails(req));
		logger.debug("Validation error occurred\n" + ex.getMessage());

		ExceptionDTO response = new ExceptionDTO();
		BindingResult bindingResult = ex.getBindingResult();

		if (bindingResult.hasFieldErrors()) {
			List<FieldError> fieldErrors = bindingResult.getFieldErrors();
			String fieldName;
			String fieldErrorMessage;

			for (FieldError fieldError : fieldErrors) {
				fieldName = fieldError.getField();
				fieldErrorMessage = messageSource.getMessage(
						fieldError.getCode(), null,
						fieldError.getDefaultMessage(), null);

				response.putFieldError(fieldName, fieldErrorMessage);
			}

			response.setMessage("Field validation error");
		}

		if (bindingResult.hasGlobalErrors()) {
			String errorMessage =
				messageSource.getMessage(
					bindingResult.getGlobalError().getCode(), null,
					bindingResult.getGlobalError().getDefaultMessage(), null);

			response.setMessage(errorMessage);
		}

		response.setCode(Integer.toString(HttpStatus.NOT_ACCEPTABLE.value()));

		return response;
	}
	
	/**
     * Handle MissingServletRequestParameterException. Triggered when a 'required' request parameter is missing.
     *
     * @param req HttpServletRequest
     * @param ex  MissingServletRequestParameterException
     * 
     * @return {@link ExceptionDTO}
     */
    @ExceptionHandler(value=MissingServletRequestParameterException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
    protected ExceptionDTO handleMissingServletRequestParameter(HttpServletRequest req, MissingServletRequestParameterException ex) {
        
		String error = ex.getParameterName() + " parameter is missing";
		return(buildResponseEntity(req, ex, HttpStatus.BAD_REQUEST, error));
    }

    /**
     * Handle HttpMediaTypeNotSupportedException. This one triggers when JSON is invalid as well.
     *
     * @param req HttpServletRequest
     * @param ex  HttpMediaTypeNotSupportedException
     * 
     * @return {@link ExceptionDTO}
     */
    @ExceptionHandler(value=HttpMediaTypeNotSupportedException.class)
	@ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
	@ResponseBody
    protected ExceptionDTO handleHttpMediaTypeNotSupported(HttpServletRequest req, HttpMediaTypeNotSupportedException ex) {

		StringBuilder builder = new StringBuilder();
        builder.append(ex.getContentType());
        builder.append(" media type is not supported. Supported media types are ");
        ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(", "));

        return(buildResponseEntity(req, ex, HttpStatus.UNSUPPORTED_MEDIA_TYPE, builder.toString()));
    }

    /**
     * Handles EntityNotFoundException. Created to encapsulate errors with more detail than javax.persistence.EntityNotFoundException.
     *
     * @param req HttpServletRequest
     * @param ex  EntityNotFoundException
     * 
     * @return {@link ExceptionDTO}
     */
    @ExceptionHandler(value=EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
    protected ExceptionDTO handleEntityNotFound(HttpServletRequest req, EntityNotFoundException ex) {
    	
		String error = "Entity Not Found";
		return(buildResponseEntity(req, ex, HttpStatus.NOT_FOUND, error));
    }

    /**
     * Handle HttpMessageNotReadableException. Happens when request JSON is malformed.
     *
     * @param req HttpServletRequest
     * @param ex  HttpMessageNotReadableException
     * 
     * @return {@link ExceptionDTO}
     */
    @ExceptionHandler(value=HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
    protected ExceptionDTO handleHttpMessageNotReadable(HttpServletRequest req, HttpMessageNotReadableException ex) {
        
		String error = "Malformed JSON request";
		return(buildResponseEntity(req, ex, HttpStatus.BAD_REQUEST, error));
    }

    /**
     * Handle HttpMessageNotWritableException. Thrown when @Validated fails.
     *
     * @param req HttpServletRequest
     * @param ex  HttpMessageNotWritableException
     * 
     * @return {@link ExceptionDTO}
     */
    @ExceptionHandler(value=HttpMessageNotWritableException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
    protected ExceptionDTO handleHttpMessageNotWritable(HttpServletRequest req, HttpMessageNotWritableException ex) {
        
		String error = "Error writing JSON output";
		return(buildResponseEntity(req, ex, HttpStatus.INTERNAL_SERVER_ERROR, error));
    }

    /**
     * Handle DataIntegrityViolationException, inspects the cause for different DB causes.
     *
     * @param req HttpServletRequest
     * @param ex  DataIntegrityViolationException
     * 
     * @return {@link ExceptionDTO}
     */
    @ExceptionHandler(value=DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
    protected ExceptionDTO handleDataIntegrityViolation(HttpServletRequest req, DataIntegrityViolationException ex) {
        
		if (ex.getCause() instanceof ConstraintViolationException) {
    			String error = "Database error";
    			return(buildResponseEntity(req, ex, HttpStatus.BAD_REQUEST, error));
        } else {
	        	String error = "Data Integrity Violation";
	        	return(buildResponseEntity(req, ex, HttpStatus.INTERNAL_SERVER_ERROR, error));
        }
    }

    /**
     * Handle MethodArgumentTypeMismatchException. Thrown when @Validated fails.
     *
     * @param req HttpServletRequest
     * @param ex  MethodArgumentTypeMismatchException
     * 
     * @return {@link ExceptionDTO}
     */
    @ExceptionHandler(value=MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
    protected ExceptionDTO handleMethodArgumentTypeMismatch(HttpServletRequest req, MethodArgumentTypeMismatchException ex) {
    	
		String error = String.format("The parameter '%s' of value '%s' could not be converted to type '%s'", ex.getName(), ex.getValue(), ex.getRequiredType().getSimpleName());
		return(buildResponseEntity(req, ex, HttpStatus.BAD_REQUEST, error));
    }

    /**
     * Handle NoHandlerFoundException. Thrown when @Validated fails.
     *
     * @param req HttpServletRequest
     * @param ex  NoHandlerFoundException
     * 
     * @return {@link ExceptionDTO}
     */
    @ExceptionHandler(value=NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
    protected ExceptionDTO handleNoHandlerFoundException(HttpServletRequest req, NoHandlerFoundException ex) {
    		
		String error = "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();
		return(buildResponseEntity(req, ex, HttpStatus.NOT_FOUND, error));
    }
    
    /**
     * Handle HttpRequestMethodNotSupportedException. Thrown when @Validated fails.
     *
     * @param req HttpServletRequest
     * @param ex  HttpRequestMethodNotSupportedException
     * 
     * @return {@link ExceptionDTO}
     */
    @ExceptionHandler(value=HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	@ResponseBody
    protected ExceptionDTO handleHttpRequestMethodNotSupported(HttpServletRequest req, HttpRequestMethodNotSupportedException ex) {
       
		StringBuilder builder = new StringBuilder();
        builder.append(ex.getMethod());
        builder.append(" method is not supported for this request. Supported methods are ");
        ex.getSupportedHttpMethods().forEach(t -> builder.append(t + " "));
        
        return(buildResponseEntity(req, ex, HttpStatus.METHOD_NOT_ALLOWED, builder.toString()));
    }
    
    /**
     * Handles javax.validation.ConstraintViolationException. Thrown when @Validated fails.
     *
     * @param req HttpServletRequest
     * @param ex  ConstraintViolationException
     * 
     * @return {@link ExceptionDTO}
     */
    @ExceptionHandler(value=ConstraintViolationException.class)
   	@ResponseStatus(HttpStatus.BAD_REQUEST)
   	@ResponseBody
    protected ExceptionDTO handleConstraintViolation(HttpServletRequest req, ConstraintViolationException ex) {
        
		String error = "Constraint Violation Error";
		return(buildResponseEntity(req, ex, HttpStatus.BAD_REQUEST, error));
    }

    /**
     * Handles ObjectOptimisticLockingFailureException. Thrown when @Validated fails.
     *
     * @param req HttpServletRequest
     * @param ex  ObjectOptimisticLockingFailureException
     * 
     * @return {@link ExceptionDTO}
     */
    @ExceptionHandler(value=ObjectOptimisticLockingFailureException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
	@ResponseBody
    protected ExceptionDTO handleConstraintViolation(HttpServletRequest req, ObjectOptimisticLockingFailureException ex) {
        
		String error = "Optimistic Locking Failure";
		return(buildResponseEntity(req, ex, HttpStatus.CONFLICT, error));
    }
    
    private ExceptionDTO buildResponseEntity(HttpServletRequest req, Exception ex, HttpStatus code, String error) {
    	
		logger.debug(getRequestDetails(req));
		logger.debug(ex.getClass().getName() + " occured", ex);

		ExceptionDTO response = new ExceptionDTO();

		response.setCode(Integer.toString(code.value()));
		response.setMessage(messageSource.getMessage(ex.getMessage(), null, error, null));

		return response;
    }
    
}
