package com.hospital.common.exception;

import com.hospital.common.state.BaseDTO;

import java.util.HashMap;
import java.util.Map;

/**
 * Transfer object for transferring exception information to the clients.
 *
 * @author Robin Rizvi
 * @since (2015-11-04.15:03:12)
 */
@SuppressWarnings("unused")
public class ExceptionDTO {

  private BaseDTO dto;
  private Map<String, String> fieldErrors;
  private String code;
  private String message;

  ExceptionDTO() {
    this.code = "";
    this.message = "";
    this.fieldErrors = new HashMap<>();
  }

  public String getCode() {
    return code;
  }

  public BaseDTO getDto() {
    return dto;
  }

  /** Gets map of fields and their errors. */
  public Map<String, String> getFieldErrors() {
    return fieldErrors;
  }

  /** Gets exception message. */
  public String getMessage() {
    return message;
  }

  /** Sets exception code. */
  public void setCode(String code) {
    this.code = code;
  }

  /** Sets DTO object. */
  public void setDto(BaseDTO dto) {
    this.dto = dto;
  }

  /**
   * Sets error corresponding to a field.
   *
   * @param fieldName name of field
   * @param fieldErrorMessage error message of field
   */
  public void putFieldError(String fieldName, String fieldErrorMessage) {
    this.fieldErrors.put(fieldName, fieldErrorMessage);
  }

  /**
   * Sets map of fields and their errors.
   *
   * @param fieldErrors map of fields and their errors
   */
  public void setFieldErrors(Map<String, String> fieldErrors) {
    this.fieldErrors = fieldErrors;
  }

  /**
   * Sets exception message.
   *
   * @param message exception message
   */
  public void setMessage(String message) {
    this.message = message;
  }
}
