package com.hospital.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.NoTransactionException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * Utility class for different X-Migrate classes.
 *
 * <p>It is not to be instantiated, only use its public static members outside.
 *
 * @author Amit Arya
 * @since (2016-03-09.17:06:02)
 */
@SuppressWarnings({"unused", "WeakerAccess", "ConstantConditions"})
public final class Utils {

  private static final int EQ = 1;
  private static final int GT = 2;
  private static final int GE = 3;
  private static final int LT = 4;
  private static final int LE = 5;

  public static int bool2Int(boolean qind) {
    return qind ? 1 : 0;
  }

  public static String bool2Str(boolean qind) {
    return qind ? "1" : "0";
  }

  public static boolean getBoolVal(Object var) {
    return getBoolVal(String.valueOf(var));
  }

  public static boolean getBoolVal(String var) {
    return (var.equals("1") || var.equalsIgnoreCase("true"));
  }

  /**
   * Retrieves the name of the database product.
   *
   * @param environment Spring environment that contains the DB url
   * @return database product name
   */
  public static String getDBName(Environment environment) {
    String dbName = null;
    String url = environment.getProperty("spring.datasource.url");

    if (url.contains("mysql")) {
      dbName = "MySQL";
    } else if (url.contains("oracle")) {
      dbName = "Oracle";
    } else if (url.contains("db2")) {
      dbName = "DB2/NT";
    } else if (url.contains("as400")) {
      dbName = "DB2 UDB for AS/400";
    }

    return dbName;
  }

  /**
   * Gets normalized program name.
   *
   * @return normalized program name
   */
  public static String getProgramName(String name) {
    return StringUtils.replacePattern(
        name, "(?i)(Controller|Service|ServiceImpl|DTO|ViewDto)$", "");
  }

  /**
   * Gets current logged in user name in Spring Security.
   *
   * @return current logged in user name
   */
  public static String getUserName() {
    return SecurityContextHolder.getContext().getAuthentication().getName();
  }

  /**
   * Converts data from Hex (EBCDIC) to String (ASCII).
   *
   * @param hex Hex data in 8-bit character encoding
   * @return String equivalent data in 7-bit character encoding
   */
  public static String hex2Str(String hex) {
    if ((hex == null) || (hex.length() == 0)) {
      return hex;
    }

    if (hex.startsWith("X'")) {
      // X'7D'
      hex = hex.substring(2, hex.length() - 1);
    }

    StringBuilder ascii = new StringBuilder();
    int len = hex.length();

    // 49204c6f7665204a617661 split into two characters 49, 20, 4c...
    for (int i = 0; i < len; i += 2) {
      // Grab the hex in pairs
      String output = hex.substring(i, (i + 2));

      // Convert hex to decimal
      int decimal = Integer.parseInt(output, 16);
      ascii.append(decimal);
    }

    return ascii.toString();
  }

  /**
   * Returns the length of a String.
   *
   * @param strSource string whose length is to be known
   * @return length of the String
   */
  public static int length(String strSource) {
    return (strSource == null) ? 0 : strSource.length();
  }

  /**
   * Returns the position of the value passed in the parameter, in the array given in the parameter,
   * starting the search from the given position.
   *
   * @param lookUpVal value whose position is to be searched
   * @param lookUpArr array in which the position of the value is to be searched
   * @param fromPos index from which to start the search
   * @return index of the value in the given source array.
   */
  public static int lookUp(char lookUpVal, char lookUpArr[], int fromPos) {
    int returnPos = -1;
    int len = (lookUpArr == null) ? 0 : lookUpArr.length;

    for (int i = fromPos; i < len; i++) {
      if (lookUpVal == lookUpArr[i]) {
        returnPos = i;

        break;
      }
    }

    return returnPos;
  }

  public static int lookUp(int opCode, int lookUpVal, int lookUpArr[], int fromPos) {
    if (lookUpArr == null) {
      return 0;
    }

    return lookUpVal(opCode, lookUpVal, lookUpArr, fromPos, lookUpArr.length);
  }

  public static int lookUp(int lookUpVal, int lookUpArr[]) {
    return lookUp(EQ, lookUpVal, lookUpArr, 1);
  }

  public static int lookUp(int opCode, Object lookupStr, Object lookupStrArr[], int fromPos) {
    if (lookupStrArr == null) {
      return 0;
    }

    return lookUp(opCode, lookupStr, lookupStrArr, fromPos, lookupStrArr.length);
  }

  public static int lookUp(
      int opCode, Object lookupStr, Object lookupStrArr[], int fromPos, int len) {
    if ((lookupStr == null) || (lookupStrArr == null)) {
      return 0;
    }

    return lookupStrInStrArr(opCode, lookupStr, lookupStrArr, fromPos, fromPos + len);
  }

  public static int lookUp(Object lookupStr, Object lookupStrArr[]) {
    return lookUp(lookupStr, lookupStrArr, 1);
  }

  public static int lookUp(Object lookupStr, Object lookupStrArr[], int fromPos) {
    if (lookupStrArr == null) {
      return 0;
    }

    return lookUp(EQ, lookupStr, lookupStrArr, fromPos);
  }

  /**
   * Returns the position of the source string in the array specified in the parameter, starting
   * from the source position to the destination position.
   *
   * @param opCode Operator code
   * @param lookupStr source string
   * @param lookupStrArr array in which to look up the given string
   * @param fromPos position from which to start the search
   * @param toPos position (exclusive) on which to end the search
   * @return position of the source string in the given array
   */
  public static int lookupStrInStrArr(
      int opCode, Object lookupStr, Object lookupStrArr[], int fromPos, int toPos) {
    int returnPos = -1;

    if ((lookupStr == null) || (lookupStrArr == null)) {
      return returnPos;
    }

    int len = Math.min(toPos, lookupStrArr.length);

    /*String strTolookUp;

    if (lookupStr instanceof CharSequence || lookupStr instanceof Number) {
    	strTolookUp = String.valueOf(lookupStr).trim();
    } else {
    	strTolookUp = objectToString(lookupStr).trim();
    }*/

    String strTolookUp = String.valueOf(lookupStr).trim();

    int diffFrmClosest = Integer.MAX_VALUE;

    for (int i = fromPos; i < len; i++) {
      boolean equals = false;

      switch (opCode) {
        case EQ:
          {
            equals = String.valueOf(lookupStrArr[i]).trim().equalsIgnoreCase(strTolookUp);

            break;
          }

        case GT:
          {
            equals = String.valueOf(lookupStrArr[i]).trim().compareToIgnoreCase(strTolookUp) > 0;

            break;
          }

        case GE:
          {
            equals = String.valueOf(lookupStrArr[i]).trim().compareToIgnoreCase(strTolookUp) >= 0;

            break;
          }

        case LT:
          {
            equals = String.valueOf(lookupStrArr[i]).trim().compareToIgnoreCase(strTolookUp) < 0;

            break;
          }

        case LE:
          {
            equals = String.valueOf(lookupStrArr[i]).trim().compareToIgnoreCase(strTolookUp) <= 0;

            break;
          }
      }

      if (equals) {
        int d =
            Math.abs(
                String.valueOf(lookupStr)
                    .compareToIgnoreCase(String.valueOf(lookupStrArr[i]).trim()));

        if (diffFrmClosest > d) {
          diffFrmClosest = d;
          returnPos = i;
        }

        if (opCode == EQ) {
          break;
        }
      }
    } // i

    return returnPos;
  }

  /**
   * Returns the position of the value passed in the parameter, in the array given in the parameter,
   * starting the search from the given position.
   *
   * @param opCode Operator code
   * @param lookUpVal value whose position is to be searched
   * @param lookUpArr array in which the position of the value is to be searched
   * @param fromPos index from which to start the search
   * @param toPos position (exclusive) on which to end the search
   * @return index of the value in the given source array.
   */
  public static int lookUpVal(int opCode, int lookUpVal, int lookUpArr[], int fromPos, int toPos) {
    int returnPos = -1;

    toPos = (lookUpArr == null) ? 0 : ((toPos > lookUpArr.length) ? lookUpArr.length : toPos);

    int diffFrmClosest = Integer.MAX_VALUE;

    for (int i = fromPos; i < toPos; i++) {
      boolean equals = false;

      switch (opCode) {
        case EQ:
          equals = lookUpArr[i] == lookUpVal;
          break;

        case GT:
          equals = lookUpArr[i] > lookUpVal;
          break;

        case GE:
          equals = lookUpArr[i] >= lookUpVal;
          break;

        case LT:
          equals = lookUpArr[i] < lookUpVal;
          break;

        case LE:
          equals = lookUpArr[i] <= lookUpVal;
          break;
      }

      if (equals) {
        int d = Math.abs(lookUpVal - lookUpArr[i]);

        if (diffFrmClosest > d) {
          diffFrmClosest = d;
          returnPos = i;
        }

        if (opCode == EQ) {
          break;
        }
      }
    } // i

    return returnPos;
  }

  /**
   * Marks a transaction for Rollback within a transaction context when using programmatic
   * transaction management.
   *
   * @param transactionManager the Spring transaction manager
   */
  public static void markTransactionForRollback(PlatformTransactionManager transactionManager) {
    try {
      String name = TransactionSynchronizationManager.getCurrentTransactionName();
      int behavior = TransactionDefinition.PROPAGATION_REQUIRED;

      DefaultTransactionDefinition def = new DefaultTransactionDefinition();
      def.setName(name);
      def.setPropagationBehavior(behavior);

      TransactionStatus status = transactionManager.getTransaction(def);

      transactionManager.rollback(status);
    } catch (NoTransactionException nte) {
      nte.printStackTrace();
    }
  }

  /**
   * Converts data from String (ASCII) to Hex (EBCDIC).
   *
   * @param str String data in 7-bit character encoding
   * @return Hex data in 8-bit character encoding
   */
  public static String str2Hex(String str) {
    if ((str == null) || (str.length() == 0)) {
      return str;
    }

    return Integer.toHexString(Integer.parseInt(str));
  }
}
