package com.hospital.common.utils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * This class contains methods to replace RPG build in functions. The reason to create this class is
 * that for some functions tying to convert to pure Java class will have to much impact on the
 * generated code. One such example is the fact that in RPG array index start at one and in Java
 * array index start at zero. <code>
 * ASSIGN NUM EQ %SCAN('OE022':@ORGP)
 * IF NUM EQ 0
 * </code> in Java if we translate %SCAN to indexOf directly <code>
 *  num = orgp.indexOf('OE022');
 *  if (num == 0)
 * </code> num will be equals to -1 if the substring is not found, but the test on num is done as we
 * expect 0. The translation will be good, but the program will not do what is expected.
 *
 * @author martin.paquin
 */
public class RPGFunctionUtils {

  /**
   * This method is a replacement for %check RPG built-in functions returns the first position of
   * the string base that contains a character that does not appear in string comparator. If all of
   * the characters in base also appear in comparator, the function returns 0.
   *
   * @param comparator
   * @param base
   * @return
   */
  public static int checkChar(String comparator, String base) {
    return checkChar(comparator, base, 1);
  }

  public static int checkChar(String comparator, String base, int startPosition) {
    int pos = 0;

    for (int y = startPosition - 1; y < base.length(); y++) {
      CharSequence d = String.valueOf(base.charAt(y));

      if (comparator.contains(d)) {
        continue;
      } else {
        pos = (y == 0) ? 0 : (y + 1);

        break;
      }
    }

    return pos;
  }

  /**
   * This method is a replacement for %CHECKR RPG built-in function.
   *
   * @param comparator
   * @param base
   * @return
   */
  public static int checkCharReverse(String comparator, String base) {
    return checkCharReverse(comparator, base, 1);
  }

  public static int checkCharReverse(String comparator, String base, int startPosition) {
    int pos = 0;

    for (int y = base.length() - 1; y > (startPosition - 1); y--) {
      CharSequence d = String.valueOf(base.charAt(y));

      if (comparator.contains(d)) {
        continue;
      } else {
        pos = (y == (base.length() - 1)) ? 0 : (y + 1);

        break;
      }
    }

    return pos;
  }

  /**
   * This method is a replacement for RPG %EDITC built-in function.
   *
   * @param fldVal
   * @param editcode
   * @return
   */
  public static String formatUsingEditCode(Object fldVal, String editcode) {
    String fldstr = String.valueOf(fldVal);
    int di = fldstr.indexOf('.');
    int scale = 0;
    int len = fldstr.length();

    if (di > -1) {
      scale = len - di - 1;
      len--;
    }

    return formatUsingEditCode(fldVal, editcode, len, scale);
  }

  public static String formatUsingEditCode(Object fldVal, String editcode, int precision) {
    return formatUsingEditCode(fldVal, editcode, "", precision);
  }

  public static String formatUsingEditCode(
      Object fldVal, String editcode, int precision, int scale) {
    return formatUsingEditCode(fldVal, editcode, "", precision, scale);
  }

  public static String formatUsingEditCode(Object fldVal, String editcode, String prefix) {
    return formatUsingEditCode(fldVal, editcode, prefix, 0);
  }

  public static String formatUsingEditCode(
      Object fldVal, String editcode, String prefix, int precision) {
    String fldstr = String.valueOf(fldVal);
    int di = fldstr.indexOf('.');
    int scale = 0;

    if (di > -1) {
      scale = precision - di - 1;
      precision--;
    }

    return formatUsingEditCode(fldVal, editcode, prefix, precision, scale);
  }

  public static String formatUsingEditCode(
      Object fldVal, String editcode, String prefix, int precision, int scale) {
    String str = "";
    String fmt = "";
    int length = precision;

    if (length == 0) {
      return String.valueOf(fldVal);
    }

    if (fldVal instanceof Float) {
      if (precision > 4) {
        length = 23;
      } else {
        length = 14;
      }

      scale = length - 1;
      fmt = "%1$" + length + "." + scale + "e";
    } else if (fldVal instanceof Integer || fldVal instanceof Long) {
      fmt = "%1$" + length + "d";
    } else if (fldVal instanceof Double) {
      length++;
      fmt = "%1$" + length + "." + scale + "f";
    } else {
      fmt = "%1$-" + length + "s";
    }

    switch (editcode.charAt(0)) {
      case '1':
      case '2':
      case '3':
      case '4':
        {
          if (fldVal != null) {
            str = String.format(Locale.US, fmt, fldVal);
          }

          if (length(prefix) > 0) {
            str = prefix + str;
          }

          break;
        }

      case 'A':
      case 'B':
      case 'C':
      case 'D':
        {
          if (fldVal != null) {
            if (String.valueOf(fldVal).startsWith("-")) {
              str = String.format(Locale.US, fmt, fldVal);
            }
          }

          str += "CR";

          if (length(prefix) > 0) {
            str = prefix + str;
          }

          return str.substring(1);
        }

      case 'J':
      case 'K':
      case 'L':
      case 'M':
        {
          if (fldVal != null) {
            str = String.format(Locale.US, fmt, fldVal);

            if (String.valueOf(fldVal).startsWith("-")) {
              str += "-";
            }
          }

          if (length(prefix) > 0) {
            str = prefix + str;
          }

          return str.substring(1);
        }

      case 'N':
      case 'O':
      case 'P':
      case 'Q':
        {
          if (fldVal != null) {
            str = String.format(Locale.US, fmt, fldVal);
          }

          if (length(prefix) > 0) {
            str = prefix + str;
          }

          break;
        }

      case 'X':
      case 'Z':
        {
          if (fldVal instanceof Number) {
            length = precision;
          }

          if (fldVal instanceof Float) {
            if (precision > 4) {
              length = 23;
            } else {
              length = 14;
            }

            scale = length - 1;
            fmt = "%1$0" + length + "." + scale + "e";
          } else if (fldVal instanceof Integer || fldVal instanceof Long) {
            fmt = "%1$0" + length + "d";
          } else if (fldVal instanceof Double) {
            // No Decimal point
            length++;
            fmt = "%1$0" + length + "d";

            String fldStr = String.valueOf(fldVal);

            if (fldStr.indexOf(".") != -1) {
              fldStr = fldStr.replace(".", "");
            }

            fldVal = toInt(fldStr);
          } else {
            fmt = "%1$-" + length + "s";
          }

          if (fldVal != null) {
            str = String.format(Locale.US, fmt, fldVal);
          }

          if (length(prefix) > 0) {
            str = prefix + str;
          }

          break;
        }

      case 'Y':
        if (fldVal instanceof Date) {
          Calendar cal = Calendar.getInstance();
          cal.setTime(((Date) fldVal));

          String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
          String day = String.valueOf(cal.get(Calendar.DATE));
          String year = String.valueOf(cal.get(Calendar.YEAR));
          String dateStr = month + "/" + day + "/" + year;

          return dateStr;
        }

        break;
    }

    return str;
  }

  /**
   * Returns a String result representing the numeric value edited according to the edit word.
   *
   * @param numVal numeric value
   * @param editWord edit word
   * @return edited numeric value
   */
  public static String formatUsingEditWord(Object numVal, String editWord) {
    if (numVal == null) {
      return "";
    }

    char numValArr[] = numVal.toString().toCharArray();
    int numPos = numValArr.length - 1;
    char editWordArr[] = editWord.toCharArray();
    int len = editWordArr.length;

    int pos = 0;
    StringBuffer sb = new StringBuffer();

    for (int i = len - 1; i >= 0; i--) {
      String edtwStr = Character.toString(editWordArr[i]);

      if (Character.isSpaceChar(editWordArr[i])) {
        if (numPos != -1) {
          sb.insert(pos++, numValArr[numPos--]);
        }
      } else {
        if ("&".equals(edtwStr)) {
          sb.insert(pos++, ' ');
        } else {
          sb.insert(pos++, editWordArr[i]);
        }
      }
    } // i

    return sb.reverse().toString();
  }

  /**
   * Returns the storage size, in bytes, of a long type.
   *
   * @param num long type whose length is to be known
   * @return length of the long type
   */
  public static int length(long num) {
    return 4;
  }

  /**
   * Returns the length of a String.
   *
   * @param strSource string whose length is to be known
   * @return length of the String
   */
  public static int length(String strSource) {
    return (strSource == null) ? 0 : strSource.length();
  }

  /**
   * Trims leading white spaces in a given source string.
   *
   * @param strToTrim source string
   * @return string with trimmed white spaces
   */
  public static String ltrim(String strToTrim) {
    return StringUtils.ltrim(strToTrim);
  }

  public static String replace(String source, int start, int length, String replacement) {
    return replace(replacement, source, start, length);
  }

  public static String replace(String replacement, String source) {
    StringBuilder str = new StringBuilder(source).replace(0, replacement.length(), replacement);

    return str.toString();
  }

  public static String replace(String replacement, String source, int startPosition) {
    StringBuilder str =
        new StringBuilder(source)
            .replace(startPosition - 1, (startPosition + replacement.length()) - 1, replacement);

    return str.toString();
  }

  public static String replace(
      String replacement, String source, int startPosition, int sourceLengthToReplace) {
    StringBuilder str =
        new StringBuilder(source)
            .replace(startPosition - 1, (startPosition + sourceLengthToReplace) - 1, replacement);

    return str.toString();
  }

  /**
   * Trims trailing white spaces in a given source string.
   *
   * @param strToTrim source string
   * @return string with trimmed white spaces
   */
  public static String rtrim(String strToTrim) {
    return StringUtils.rtrim(strToTrim);
  }

  /**
   * Scans for specified substring, starting at the 1 index.
   *
   * @param searchString The search string
   * @param stringToSearch The string we search for
   * @return the index of the first occurrence of the specified substring, or 0 if there is no such
   *     occurrence.
   */
  public static int search(int searchString, int stringToSearch) {
    return search(String.valueOf(searchString), String.valueOf(stringToSearch), 1);
  }

  /**
   * Scans for specified substring, starting at the 1 index.
   *
   * @param searchString The search string
   * @param stringToSearch The string we search for
   * @return the index of the first occurrence of the specified substring, or 0 if there is no such
   *     occurrence.
   */
  public static int search(String searchString, String stringToSearch) {
    return search(searchString, stringToSearch, 1);
  }

  /**
   * Scans for specified substring, starting at the specified index.
   *
   * @param searchString The search string
   * @param stringToSearch The string we search for
   * @param startPos
   * @return the index of the first occurrence of the specified substring, or 0 if there is no such
   *     occurrence. The index is based on the position of the first occurrence in the searchString.
   */
  public static int search(String searchString, String stringToSearch, int startPos) {
    if (isEmpty(searchString) || isEmpty(stringToSearch)) {
      throw new IllegalArgumentException(
          "Parameters searchString and stringToSearch may not be null or empty");
    }

    String subString = stringToSearch.substring((startPos < 1) ? 0 : (startPos - 1));
    int result = subString.indexOf(searchString);

    return (result == -1) ? 0 : (result + startPos);
  }

  /**
   * @param searchString The search string
   * @param stringToSearch The string we search for
   * @param startPos
   * @param length, the length of the substring
   * @return the index of the first occurrence of the specified substring, or 0 if there is no such
   *     occurrence. The index is based on the position of the first occurrence in the searchString.
   */
  public static int search(String searchString, String stringToSearch, int startPos, int length) {
    if (isEmpty(searchString) || isEmpty(stringToSearch)) {
      throw new IllegalArgumentException(
          "Parameters searchString and stringToSearch may not be null");
    }

    if (startPos < 1) {
      throw new IllegalArgumentException("startPos must be greater than 0");
    }

    String subString =
        stringToSearch.substring((startPos < 1) ? 0 : (startPos - 1), startPos - 1 + length);
    int result = search(searchString, subString);

    return (result == 0) ? 0 : ((result + startPos) - 1);
  }

  /**
   * RPG subst (substring) replacement with one based index.
   *
   * @param source
   * @param start
   * @return
   */
  public static String subst(String source, int start) {
    if (source == null) {
      throw new IllegalArgumentException("source must not be null");
    }

    if (start < 1) {
      throw new IllegalArgumentException("start must be 1 or greater");
    }

    if (start > source.length()) {
      throw new IllegalArgumentException("start must be less than source's length");
    }

    return source.substring(start - 1);
  }

  public static String subst(String source, int start, int length) {
    if (source == null) {
      throw new IllegalArgumentException("source must not be null");
    }

    if (start < 1) {
      throw new IllegalArgumentException("start must be 1 or greater");
    }

    if (start > source.length()) {
      throw new IllegalArgumentException("start must be less than source's length");
    }

    if (((start + length) - 1) > source.length()) {
      throw new IllegalArgumentException("start + length must be less than source's length");
    }

    return source.substring(start - 1, (start + length) - 1);
  }

  /**
   * Converts a LocalDate to String.
   *
   * @param date local date
   * @param rpgFmt rpg date format
   * @return converted date in String
   */
  public static String toChar(LocalDate date, String rpgFmt) {
    return DateTimeUtils.toString(date, rpgFmt);
  }

  /**
   * Converts a LocalDate to String.
   *
   * @param date local date
   * @param rpgFmt rpg date format
   * @param sep separator
   * @return converted date in String
   */
  public static String toChar(LocalDate date, String rpgFmt, String sep) {
    return DateTimeUtils.toString(date, rpgFmt).replaceAll("/|-|\\.", sep);
  }

  /**
   * Converts a LocalDateTime to String.
   *
   * @param date local date-time
   * @param rpgFmt rpg date format
   * @param sep separator
   * @return converted date in String
   */
  public static String toChar(LocalDateTime date, String rpgFmt, String sep) {
    return DateTimeUtils.toString(date, rpgFmt).replaceAll("/|-|\\.", sep);
  }

  /**
   * Converts a LocalTime to String.
   *
   * @param time local time
   * @param rpgFmt rpg date format
   * @return converted time in String
   */
  public static String toChar(LocalTime time, String rpgFmt) {
    return DateTimeUtils.toString(time, rpgFmt);
  }

  /**
   * Converts an Object to String.
   *
   * @param obj Object
   * @return converted object in String
   */
  public static String toChar(Object obj) {
    return StringUtils.toChar(obj);
  }

  /**
   * Converts an Object to String and pads with a specified character to match the specified size.
   *
   * @param obj Object
   * @param size Object
   * @return converted object in String
   */
  public static String toChar(Object obj, int size) {
    return StringUtils.toChar(obj, size);
  }

  /**
   * Converts a LocalDate to String.
   *
   * @param date local date
   * @param rpgFmt rpg date format
   * @return converted date in String
   */
  public static String toDecimal(LocalDate date, String rpgFmt) {
    return DateTimeUtils.toString(date, rpgFmt);
  }

  /**
   * Converts a LocalTime to int.
   *
   * @param time local time
   * @return converted time in integer
   */
  public static int toDecimal(LocalTime time) {
    return DateTimeUtils.toInt(time);
  }

  public static BigDecimal toDecimal(String expression) {
    return toDecimal(expression, 7, 2);
  }

  public static BigDecimal toDecimal(String expression, int precision, int decimalPlaces) {
    return NumberFormatter.toDecimal(expression, precision, decimalPlaces);
  }

  public static Integer toInt(Object objToConvert) {
    return NumberFormatter.toInt(objToConvert);
  }

  /**
   * Translate char in stringToTranslate with the char from to string which is at the same position
   * in from string.
   *
   * @param from String from which char will be translate
   * @param to
   * @param stringToTranslate
   * @return
   */
  public static String translate(String from, String to, String stringToTranslate) {
    return translate(from, to, stringToTranslate, 1);
  }

  /**
   * Translate char in stringToTranslate with the char from to string which is at the same position
   * in from string.
   *
   * @param from String from which char will be translate
   * @param to
   * @param stringToTranslate
   * @param startPos
   * @return
   */
  public static String translate(String from, String to, String stringToTranslate, int startPos) {
    StringBuilder strb = new StringBuilder();

    if (isEmpty(from)) {
      throw new IllegalArgumentException("From string should not be null or empty");
    }

    if (isEmpty(to)) {
      throw new IllegalArgumentException("To string should not be null or empty");
    }

    if (isEmpty(stringToTranslate)) {
      throw new IllegalArgumentException("StringToTranslate string should not be null or empty");
    }

    if (startPos < 1) {
      throw new IllegalArgumentException("StartPos must be greater than 0");
    }

    if (startPos > stringToTranslate.length()) {
      throw new IllegalArgumentException("StartPos must be less than stringToTranslate length");
    }

    if (startPos != 1) {
      strb.append(subst(stringToTranslate, 1, startPos - 1));
    }

    for (int x = startPos - 1; x < stringToTranslate.length(); x++) {
      CharSequence d = String.valueOf(stringToTranslate.charAt(x));

      if (from.contains(d)) {
        strb.append(to.charAt(from.indexOf(stringToTranslate.charAt(x))));
      } else {
        strb.append(stringToTranslate.charAt(x));
      }
    }

    return strb.toString();
  }

  /**
   * Trims leading and trailing white spaces in a given source string.
   *
   * @param strToTrim source string
   * @return string with trimmed white spaces
   */
  public static String trim(String strToTrim) {
    return StringUtils.trim(strToTrim);
  }
}
