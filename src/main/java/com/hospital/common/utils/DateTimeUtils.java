package com.hospital.common.utils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.WeekFields;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Utility class for Date/Time operations.
 *
 * @author Amit Arya
 * @since (2016-07-18.18:01:02)
 */
public final class DateTimeUtils {

  public static final Map<String, String> rpg2JavaDateFormat = new HashMap<String, String>();

  static {
    rpg2JavaDateFormat.put("*ISO", "yyyy-MM-dd");
    rpg2JavaDateFormat.put("*USA", "MM/dd/yyyy");
    rpg2JavaDateFormat.put("*EUR", "dd.MM.yyyy");
    rpg2JavaDateFormat.put("*JIS", "dd.MM.yyyy");
    rpg2JavaDateFormat.put("*JUL", "yy/D");
    rpg2JavaDateFormat.put("*YMD", "yy/MM/dd");
    rpg2JavaDateFormat.put("*YYMD", "yyyy/MM/dd");
    rpg2JavaDateFormat.put("*MDY", "MM/dd/yy");
    rpg2JavaDateFormat.put("*MDYY", "MM/dd/yyyy");
    rpg2JavaDateFormat.put("*DMY", "dd/MM/yy");
    rpg2JavaDateFormat.put("*DMYY", "dd/MM/yyyy");

    // new format created for separator -
    rpg2JavaDateFormat.put("*MDY1", "MM-dd-yy");
    rpg2JavaDateFormat.put("*MDYY1", "MM-dd-yyyy");
    rpg2JavaDateFormat.put("*DMY1", "dd-MM-yy");
    rpg2JavaDateFormat.put("*DMYY1", "dd-MM-yyyy");
  }

  public static final Map<String, String> rpg2JavaDateTimeFormat = new HashMap<String, String>();

  static {
    rpg2JavaDateTimeFormat.put("*ISO", "yyyy-MM-dd HH:mm:ss");
    rpg2JavaDateTimeFormat.put("*USA", "MM/dd/yyyy hh:mm a");
    rpg2JavaDateTimeFormat.put("*EUR", "dd.MM.yyyy HH:mm:ss");
    rpg2JavaDateTimeFormat.put("*JIS", "yyyy-MM-dd HH:mm:ss");
  }

  public static Map<String, String> rpg2JavaTimeFormat = new HashMap<String, String>();

  static {
    rpg2JavaTimeFormat.put("*ISO", "HH.mm.ss");
    rpg2JavaTimeFormat.put("*USA", "HH.mm a");
    rpg2JavaTimeFormat.put("*EUR", "HH.mm.ss");
    rpg2JavaTimeFormat.put("*JIS", "HH:mm:ss");
    rpg2JavaTimeFormat.put("*HMS", "HH:mm:ss");
  }

  /**
   * Gets difference between the two dates. Replacement for RPG %DIFF built-in function.
   *
   * @param dt1
   * @param dt2
   * @param part
   * @return difference between the two dates
   */
  public static int difference(LocalDate dt1, LocalDate dt2, String part) {
    // TODO
    int diff = 0;

    return diff;
  }

  /**
   * Gets difference between the two times. Replacement for RPG %DIFF built-in function.
   *
   * @param dt1
   * @param dt2
   * @param part
   * @return difference between the two times
   */
  public static int difference(LocalTime dt1, LocalTime dt2, String part) {
    // TODO
    int diff = 0;

    return diff;
  }

  /**
   * Obtains the current date from the system clock in the default time-zone. Replacement for RPG
   * %DATE built-in function.
   *
   * @return local date
   */
  public static LocalDate getDate() {
    return LocalDate.now();
  }

  /**
   * Gets the current day of month. Replacement for RPG %DAY built-in function.
   *
   * @return the day-of-month, from 1 to 31
   */
  public static int getDay() {
    return LocalDate.now().getDayOfMonth();
  }

  /**
   * Gets the current month of year from 1 to 12. Replacement for RPG %MONTH built-in function.
   *
   * @return the month-of-year, from 1 to 12
   */
  public static int getMonth() {
    return LocalDate.now().getMonthValue();
  }

  /**
   * Obtains the current time from the system clock in the default time-zone. Replacement for RPG
   * %TIME built-in function.
   *
   * @return local time
   */
  public static LocalTime getTime() {
    return LocalTime.now();
  }

  /**
   * Obtains the current date-time from the system clock in the default time-zone. Replacement for
   * RPG %TIMESTAMP built-in function.
   *
   * @return local date-time
   */
  public static LocalDateTime getTimestamp() {
    return LocalDateTime.now();
  }

  /**
   * Returns the Week Number.
   *
   * @param date local date
   * @return week of a week-based-year
   */
  public static int getWeekNumber(LocalDate date) {
    WeekFields weekFields = WeekFields.of(Locale.getDefault());

    return date.get(weekFields.weekOfWeekBasedYear());
  }

  /**
   * Gets the current year. Replacement for RPG %YEAR built-in function.
   *
   * @return the year, from MIN_YEAR to MAX_YEAR
   */
  public static int getYear() {
    return LocalDate.now().getYear();
  }

  /**
   * Returns a copy of this date with the specified constant subtracted.
   *
   * @param date local date
   * @param constToSubtract the constant to subtract
   * @return a LocalDate based on this date with the subtraction made
   */
  public static LocalDate minusConst(LocalDate date, long constToSubtract) {
    if (constToSubtract == 10000) {
      return date.minusYears(1);
    } else if (constToSubtract == 100) {
      return date.minusMonths(1);
    }

    return date;
  }

  /**
   * Returns a copy of this date with the specified constant added.
   *
   * @param date local date
   * @param constToAdd the constant to add
   * @return a LocalDate based on this date with the addition made
   */
  public static LocalDate plusConst(LocalDate date, long constToAdd) {
    if (constToAdd == 10000) {
      return date.plusYears(1);
    } else if (constToAdd == 100) {
      return date.plusMonths(1);
    }

    return date;
  }

  /**
   * Converts the date to Database format i.e. yyyy-MM-dd format.
   *
   * @param date local date
   * @return converted date in Database format
   */
  public static String toDbFormat(LocalDate date) {
    return date.format(DateTimeFormatter.ISO_LOCAL_DATE);
  }

  /**
   * Converts the date to integer.
   *
   * @param date local date
   * @return converted date in integer
   */
  public static int toInt(LocalDate date) {
    return Integer.parseInt(
        date.getYear()
            + String.format("%02d", date.getMonthValue())
            + String.format("%02d", date.getDayOfMonth()));
  }

  /**
   * Converts the date-time to integer.
   *
   * @param date local date-time
   * @return converted date-time in integer
   */
  public static int toInt(LocalDateTime date) {
    return Integer.parseInt(
        date.getYear()
            + String.format("%02d", date.getMonthValue())
            + String.format("%02d", date.getDayOfMonth())
            + String.format("%02d", date.getHour())
            + String.format("%02d", date.getMinute())
            + String.format("%02d", date.getSecond()));
  }

  /**
   * Converts the time to integer.
   *
   * @param time local time
   * @return converted time in integer
   */
  public static int toInt(LocalTime time) {
    return Integer.parseInt(time.toString().replaceAll(":|\\.", ""));
  }

  /**
   * Obtains an instance of LocalDate in yyyyMMdd format from a text string.
   *
   * @param date the text to parse
   * @return the parsed local date
   */
  public static LocalDate toIntFormat(String date) {
    String longDate = date;

    if (date.length() == 6) {
      date = "0" + date;
    }

    if (date.length() == 7) {
      longDate = (Integer.parseInt(date.substring(0, 1)) + 19) + date.substring(1);
    }

    return LocalDate.parse(longDate, DateTimeFormatter.BASIC_ISO_DATE);
  }

  /**
   * Converts integer to date.
   *
   * @param date the date to set
   * @return local date
   */
  public static LocalDate toLocalDate(int date) {
    String dateStr = String.valueOf(date);

    if (date == 0) {
      return null;
    } else if ((dateStr.length() == 2) || (dateStr.length() == 4)) {
      int year = date;

      if (dateStr.length() == 2) {
        year += 1900;
      }

      return LocalDate.of(year, 1, 1);
    } else {
      return toIntFormat(dateStr);
    }
  }

  /**
   * Converts from an Instant on the time-line to a LocalDate.
   *
   * @param epochMilli the number of milliseconds from 1970-01-01T00:00:00Z
   * @return local date
   */
  public static LocalDate toLocalDate(long epochMilli) {
    try {
      return toIntFormat(String.valueOf(epochMilli));
    } catch (DateTimeParseException dtpe) {
    }

    return Instant.ofEpochMilli(epochMilli).atZone(ZoneId.systemDefault()).toLocalDate();
  }

  /**
   * Converts String to date.
   *
   * @param date date string
   * @return converted local date
   */
  public static LocalDate toLocalDate(String date) {
    return LocalDate.parse(date);
  }

  /**
   * Converts numeric date to date-time.
   *
   * @param date numeric date
   * @return converted local date-time
   */
  public static LocalDateTime toLocalDateTime(int date) {
    return LocalDateTime.parse(String.valueOf(date), DateTimeFormatter.BASIC_ISO_DATE);
  }

  /**
   * Converts String to time.
   *
   * @param time time string
   * @return converted local time
   */
  public static LocalTime toLocalTime(String time) {
    return LocalTime.parse(time);
  }

  /**
   * Converts date to String.
   *
   * @param date local date
   * @param rpgFmt the pattern for describing the format
   * @return converted date in String
   */
  public static String toString(LocalDate date, String rpgFmt) {
    String pattern = rpg2JavaDateFormat.get(rpgFmt);

    if (pattern == null) {
      pattern = "yyyy-MM-dd";
    }

    return date.format(DateTimeFormatter.ofPattern(pattern));
  }

  /**
   * Converts date-time to String.
   *
   * @param date local date-time
   * @param rpgFmt the pattern for describing the format
   * @return converted date-time in String
   */
  public static String toString(LocalDateTime date, String rpgFmt) {
    String pattern = rpg2JavaDateTimeFormat.get(rpgFmt);

    if (pattern == null) {
      pattern = "yyyy-MM-dd HH:mm:ss";
    }

    return date.format(DateTimeFormatter.ofPattern(pattern));
  }

  /**
   * Converts time to String.
   *
   * @param time local time
   * @param rpgFmt the pattern for describing the format
   * @return converted time in String
   */
  public static String toString(LocalTime time, String rpgFmt) {
    String pattern = rpg2JavaTimeFormat.get(rpgFmt);

    if (pattern == null) {
      pattern = "HH.mm.ss";
    }

    return time.format(DateTimeFormatter.ofPattern(pattern));
  }
}
