package com.hospital.common.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

/*
 * Class uses instead of PagImpl to enable deseralization
 * src: https://www.codesd.com/item/spring-resttemplate-with-paged-api.html
 */
public class RestResponsePage<T> extends PageImpl<T> implements Serializable{

  private static final long serialVersionUID = 3248189030448292002L;

  public RestResponsePage(List<T> content, Pageable pageable, long total) {
    super(content, pageable, total);
  }

  public RestResponsePage(List<T> content) {
    super(content);
  }

  /* RestResponsePage does not have an empty constructor and this was causing an issue for RestTemplate to cast the Rest API response
   * back to Page.
   */
  public RestResponsePage() {
    super(new ArrayList<T>());
  }

}
