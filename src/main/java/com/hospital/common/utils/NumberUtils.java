package com.hospital.common.utils;

import java.math.BigDecimal;

/**
 * The NumberUtils class contains various methods for manipulating numbers.
 *
 * @author Amit Arya
 * @since (2016-10-25.15:59:16)
 */
public class NumberUtils {

  /**
   * Replacement for *HIVAL RPG literal.
   *
   * @param len
   * @return max value
   */
  public static Integer hiInt(int len) {
    return hiLoInt(len, false);
  }

  private static Integer hiLoInt(int len, boolean isLoval) {
    Integer hiIn = 0;

    if (len >= 10) {
      hiIn = Integer.MAX_VALUE;
    } else {
      for (int i = 0; i < len; i++) {
        hiIn = (10 * hiIn) + 9;
      }
    }

    hiIn *= (isLoval ? (-1) : 1);

    return hiIn;
  }

  public static boolean isNumber(String os) {
    return (os != null) && os.matches("[\\+\\-]?\\d+([\\,\\.]\\d+)?");
  }

  /**
   * Sums the elements of an array.
   *
   * @param BigDecimal array to sum
   * @return sum of the elements of an array
   */
  public static BigDecimal sumArray(BigDecimal array[]) {
    if (array == null) {
      return BigDecimal.ZERO;
    }

    BigDecimal sum = BigDecimal.ZERO;

    int len = array.length;

    for (int i = 0; i < len; i++) {
      if (array[i] != null) {
        sum = sum.add(array[i]);
      }
    }

    return sum;
  }

  /**
   * Sums the elements of an array.
   *
   * @param int array to sum
   * @return sum of the elements of an array
   */
  public static int sumArray(int array[]) {
    if (array == null) {
      return 0;
    }

    int sum = 0;

    int len = array.length;

    for (int i = 0; i < len; i++) {
      sum += array[i];
    }

    return sum;
  }
}
