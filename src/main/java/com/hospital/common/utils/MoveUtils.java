package com.hospital.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/** @author Zhen Qiang He */
public final class MoveUtils implements Serializable {
  private static final String CONVERSION_TABLE_PATH = "ibm037-utf16.bin";
  private static boolean convertionTablesInitialized;
  private static Map<String, DateTimeFormatter> dateFormatterMap;
  private static final Logger logger = LoggerFactory.getLogger(MoveUtils.class);
  private static final long serialVersionUID = 4647325005525095411L;
  private static char tableIBM037ToUTF16[];
  private static byte tableUTF16ToIBM037[];
  private static Map<String, DateTimeFormatter> timeFormatterMap;
  private static DateTimeFormatter timestampFormatterWithoutSeparator;
  private static DateTimeFormatter timestampFormatterWithSeparator;

  /**
   * This method is base on JTOpen method com.ibm.as400.access.AS400ZonedDecimal.toBytes(Object,
   * byte[], int)
   *
   * @param source
   * @param length
   * @param decimal
   * @return
   */
  protected static byte[] convertBigDecimalToZoned(BigDecimal source, int length, int decimal) {
    byte result[] = new byte[length];
    int offset = 0;

    if (source.scale() > decimal) {
      throw new IllegalArgumentException(
          "Invalid decimal point <" + source.scale() + "> to expected <" + decimal + ">");
    }

    int sign = source.signum();

    // Don't use unscaledValue() here for specified decimal might be
    // different to scale of BigDecimal
    char inChars[] = source.abs().movePointRight(decimal).toBigInteger().toString().toCharArray();

    if (inChars.length > length) {
      throw new IllegalArgumentException(
          "Invalid length <" + inChars.length + "> to expected <" + length + ">");
    }

    int inPosition = 0; // position in char[]
    // write correct number of leading zero's

    for (int i = 0; i < (length - inChars.length); ++i) {
      result[offset++] = (byte) 0xF0;
    }

    // place all the digits except the last one
    while (inPosition < (inChars.length - 1)) {
      result[offset++] = (byte) ((inChars[inPosition++] & 0x000F) | 0x00F0);
    }

    // place the sign and last digit
    if (sign != -1) {
      result[offset] = (byte) ((inChars[inPosition] & 0x000F) | 0x00F0);
    } else {
      result[offset] = (byte) ((inChars[inPosition] & 0x000F) | 0x00D0);
    }

    return result;
  }

  /**
   * @param data
   * @param itemLength
   * @return
   */
  protected static String[] convertCharArrayToStringArray(char data[], int itemLength) {
    String result[] = null;

    if (data != null) {
      result = new String[data.length / itemLength];

      char itemData[] = new char[itemLength];

      for (int i = 0; i < result.length; i++) {
        System.arraycopy(data, itemLength * i, itemData, 0, itemLength);
        result[i] = String.valueOf(itemData);
      }
    }

    return result;
  }

  /**
   * @param source
   * @param dateFormat
   * @param separator
   * @return
   */
  protected static String convertDateToString(
      LocalDate source, String dateFormat, String separator) {
    StringBuilder result = new StringBuilder();

    if ((source == null)) {
      throw new IllegalArgumentException("Date cannot be null!");
    }

    dateFormat = (dateFormat == null) ? "" : dateFormat.trim().toUpperCase();
    separator =
        (separator == null)
            ? getDefaultDateSeparator(dateFormat)
            : ((separator.length() > 1) ? separator.substring(0, 1) : separator);

    switch (dateFormat) {
      case "*CDMY":
      case "*CMDY":
      case "*CYMD":
        String century = String.valueOf((source.getYear() / 100) - 19);
        result.append(century.charAt(century.length() - 1));

      case "*DMY":
      case "*MDY":
      case "*YMD":
      case "*DMYY":
      case "*EUR":
      case "*ISO":
      case "*JIS":
      case "*YYMD":
      case "*LONGJUL":
      case "*JUL":
      case "*MDYY":
      case "*USA":
        break;

      default:
        throw new IllegalArgumentException("Invalid date format: <" + dateFormat + ">");
    }

    result.append(source.format(getDateFormatter(dateFormat, separator)));

    return result.toString();
  }

  /**
   * Converts IBM EBCDIC bytes into UTF-16 encoding.
   *
   * @param IBM037Data the bytes to convert
   * @return Characters in UTF-16 encoding
   */
  private static char[] convertIBM037ToUTF16(byte IBM037Data[]) {
    int len = IBM037Data.length;
    char result[] = new char[len];

    for (int i = 0; i < len; i++) {
      result[i] = getIBM0372UTF16Table()[IBM037Data[i] & 0xFF];
    }

    return result;
  }

  /**
   * @param data
   * @param itemLength
   * @return
   */
  protected static char[] convertStringArrayToCharArray(String data[], int itemLength) {
    char result[] = null;

    if (data != null) {
      result = new char[data.length * itemLength];

      for (int i = 0; i < data.length; i++) {
        if (data[i].length() > itemLength) {
          throw new IllegalArgumentException(
              "Invalid target length <" + data[i].length() + "> to expected <" + itemLength + ">");
        }

        System.arraycopy(
            MoveUtils.padRight(data[i], itemLength, ' ').toCharArray(),
            0,
            result,
            itemLength * i,
            itemLength);
      }
    }

    return result;
  }

  /**
   * @param source
   * @param dateFormat
   * @param separator
   * @return
   */
  protected static LocalDate convertStringToDate(
      String source, String dateFormat, String separator) {
    LocalDate result = null;

    if ((source == null)) {
      throw new IllegalArgumentException("Source cannot be null!");
    }

    dateFormat = (dateFormat == null) ? "" : dateFormat.trim().toUpperCase();
    separator =
        (separator == null)
            ? getDefaultDateSeparator(dateFormat)
            : ((separator.length() > 1) ? separator.substring(0, 1) : separator);

    if (!isValidDateFormatName(dateFormat)) {
      throw new IllegalArgumentException("Invalid date format: <" + dateFormat + ">");
    }

    if (source.length() != getDateLength(dateFormat, separator)) {
      throw new IllegalArgumentException(
          "Invalid character length <" + source.length() + "> for a complete date.");
    }

    int century = 0;
    String yearStr = "";

    switch (dateFormat) {
      case "*CDMY":
      case "*CMDY":
      case "*CYMD":
        if ((source.charAt(0) >= '0') && (source.charAt(0) <= '9')) {
          century = (19 + Integer.valueOf(source.substring(0, 1))) * 100;
        } else {
          throw new IllegalArgumentException(
              "Invalid century <" + source.charAt(0) + "> for date format <" + dateFormat + ">.");
        }

        source = source.substring(1);

        break;

      case "*DMY":
      case "*MDY":
        if (yearStr.isEmpty()) {
          if (separator.isEmpty()) {
            yearStr = source.substring(4, 6);
          } else {
            yearStr = source.substring(6, 8);
          }
        }

      case "*YMD":
      case "*JUL":
        if (yearStr.isEmpty()) {
          yearStr = source.substring(0, 2);
        }

        if (!yearStr.isEmpty()) {
          try {
            century = Integer.parseInt(yearStr);
          } catch (NumberFormatException e) {
            throw new IllegalArgumentException(
                "Invalid year <" + yearStr + "> for date format <" + dateFormat + ">.");
          }

          century = (century < 40) ? 2000 : 1900;
        }

        break;

      case "*DMYY":
      case "*EUR":
      case "*ISO":
      case "*JIS":
      case "*YYMD":
      case "*LONGJUL":
      case "*MDYY":
      case "*USA":
        break;

      default:
        throw new IllegalArgumentException("Invalid date format: <" + dateFormat + ">");
    }

    try {
      result = LocalDate.parse(source, getDateFormatter(dateFormat, separator));
    } catch (DateTimeParseException e) {
      throw new IllegalArgumentException(
          "Date, Time or Timestamp value is not valid: <" + source + ">");
    }

    if (century != 0) {
      result =
          LocalDate.of(
              (result.getYear() % 100) + century, result.getMonthValue(), result.getDayOfMonth());
    }

    return result;
  }

  /**
   * @param source
   * @param timeFormat
   * @param separator
   * @return
   */
  protected static LocalTime convertStringToTime(
      String source, String timeFormat, String separator) {
    if ((source == null)) {
      throw new IllegalArgumentException("Source cannot be null!");
    }

    timeFormat = (timeFormat == null) ? "" : timeFormat.trim().toUpperCase();
    separator =
        (separator == null)
            ? getDefaultTimeSeparator(timeFormat)
            : ((separator.length() > 1) ? separator.substring(0, 1) : separator);

    if (!isValidTimeFormatName(timeFormat)) {
      throw new IllegalArgumentException("Invalid time format: <" + timeFormat + ">");
    }

    if (source.length() != getTimeLength(timeFormat, separator)) {
      throw new IllegalArgumentException(
          "Invalid character length <" + source.length() + "> for a complete time.");
    }

    try {
      return LocalTime.parse(source, getTimeFormatter(timeFormat, separator));
    } catch (DateTimeParseException e) {
      throw new IllegalArgumentException(
          "Date, Time or Timestamp value is not valid: <" + source + ">");
    }
  }

  /**
   * @param source
   * @param includeSeparator
   * @return
   */
  protected static LocalDateTime convertStringToTimestamp(String source, boolean includeSeparator) {
    if ((source == null)) {
      throw new IllegalArgumentException("Source cannot be null!");
    }

    int length = includeSeparator ? 29 : 23;

    if ((includeSeparator && (source.length() < 19))
        || (!includeSeparator && (source.length() < 14))) {
      throw new IllegalArgumentException(
          "Length <" + source.length() + "> is too small to contain a complete timestamp.");
    }

    if ((includeSeparator && (source.length() == 20))
        || (!includeSeparator && (source.length() == 15))) {
      throw new IllegalArgumentException("Invalid timestamp length: <" + source.length() + ">.");
    }

    if ((includeSeparator && (source.length() > 32))
        || (!includeSeparator && (source.length() > 26))) {
      throw new IllegalArgumentException(
          "Length <" + source.length() + "> is over precision of timestamp.");
    }

    if (includeSeparator && (source.length() == 19)) {
      source += '.';
    }

    source =
        (source.length() < length) ? padRight(source, length, '0') : source.substring(0, length);

    try {
      return LocalDateTime.parse(source, getTimestampFormatter(includeSeparator));
    } catch (DateTimeParseException e) {
      throw new IllegalArgumentException(
          "Date, Time or Timestamp value is not valid: <" + source + ">");
    }
  }

  /**
   * @param source
   * @param timeFormat
   * @param separator
   * @return
   */
  protected static String convertTimeToString(
      LocalTime source, String timeFormat, String separator) {
    if ((source == null)) {
      throw new IllegalArgumentException("Time cannot be null!");
    }

    timeFormat = (timeFormat == null) ? "" : timeFormat.trim().toUpperCase();
    separator =
        (separator == null)
            ? getDefaultTimeSeparator(timeFormat)
            : ((separator.length() > 1) ? separator.substring(0, 1) : separator);

    if (!isValidTimeFormatName(timeFormat)) {
      throw new IllegalArgumentException("Invalid time format: <" + timeFormat + ">");
    }

    return source.format(getTimeFormatter(timeFormat, separator));
  }

  /**
   * @param source
   * @param length
   * @param includeSeparator
   * @return
   */
  protected static String convertTimestampToString(
      LocalDateTime source, int length, boolean includeSeparator) {
    if ((source == null)) {
      throw new IllegalArgumentException("Timestamp cannot be null!");
    }

    if (length < 19) {
      throw new IllegalArgumentException(
          "Source length <" + length + "> is too small to contain a complete timestamp.");
    }

    if (length == 20) {
      throw new IllegalArgumentException("Invalid source length: <" + length + ">.");
    }

    if (length > 32) {
      throw new IllegalArgumentException(
          "Source length <" + length + "> is over precision of timestamp.");
    }

    return padRight(
            source.format(getTimestampFormatter(includeSeparator)),
            (includeSeparator ? 32 : 26),
            '0')
        .substring(0, (includeSeparator ? length : ((length == 19) ? (length - 5) : (length - 6))));
  }

  /**
   * Converts UTF-16 characters into IBM EBCDIC encoding.
   *
   * @param UTF16Data the characters to convert
   * @return Bytes in IBM EBCDIC encoding.
   */
  private static byte[] convertUTF16ToIBM037(char UTF16Data[]) {
    int len = UTF16Data.length;
    byte result[] = new byte[len];

    for (int i = 0; i < len; i++) {
      result[i] = getUTF162IBM037Table()[(UTF16Data[i])];
    }

    return result;
  }

  /**
   * This method is base on JTOpen method com.ibm.as400.access.AS400ZonedDecimal.toObject(byte[],
   * int)
   *
   * @param data
   * @param decimal
   * @return
   */
  protected static BigDecimal convertZonedToBigDecimal(byte data[], int decimal) {
    int offset = 0;
    int outputPosition = 0; // position in char[]
    char outputData[] = null;

    // read the sign bit, allow ArrayIndexException to be thrown
    byte nibble = (byte) ((data[data.length - 1] & 0xFF) >>> 4);

    switch (nibble) {
      case (byte) 0x0B: // valid negative sign bits
      case (byte) 0x0D:
        outputData = new char[data.length + 1];
        outputData[outputPosition++] = '-';

        break;

      case (byte) 0x0A: // valid positive sign bits
      case (byte) 0x0C:
      case (byte) 0x0E:
      case (byte) 0x0F:
        outputData = new char[data.length];

        break;

      default: // others invalid
        throw new IllegalArgumentException(
            "Invalid last character <" + (byte) (data[data.length - 1] & 0xFF) + ">");
    }

    // place the digits
    while (outputPosition < outputData.length) {
      nibble = (byte) (data[offset++] & 0x0F);

      if (nibble > 0x09) {
        throw new IllegalArgumentException(
            "Invalid character <"
                + (byte) (data[offset - 1] & 0xFF)
                + "> at position <"
                + offset
                + ">");
      }

      outputData[outputPosition++] = (char) (nibble | 0x0030);
    }

    // construct New BigDecimal object
    return new BigDecimal(new BigInteger(new String(outputData)), decimal);
  }

  /**
   * @param dateFormat
   * @param separator
   * @return
   */
  private static synchronized DateTimeFormatter getDateFormatter(
      String dateFormat, String separator) {
    DateTimeFormatter result = null;
    StringBuilder pattern = new StringBuilder();
    separator =
        (separator == null)
            ? getDefaultDateSeparator(dateFormat)
            : ((separator.length() > 1) ? separator.substring(0, 1) : separator);
    dateFormat = (dateFormat == null) ? "" : dateFormat.trim().toUpperCase();

    String key = dateFormat + separator;

    if (getDateFormatterMap().containsKey(key) && (getDateFormatterMap().get(key) != null)) {
      result = getDateFormatterMap().get(key);
    } else {
      switch (dateFormat) {
        case "*CDMY":
        case "*DMY":
          pattern.append("dd").append(separator).append("MM").append(separator).append("uu");

          break;

        case "*CMDY":
        case "*MDY":
          pattern.append("MM").append(separator).append("dd").append(separator).append("uu");

          break;

        case "*CYMD":
        case "*YMD":
          pattern.append("uu").append(separator).append("MM").append(separator).append("dd");

          break;

        case "*DMYY":
        case "*EUR":
          pattern.append("dd").append(separator).append("MM").append(separator).append("uuuu");

          break;

        case "*ISO":
        case "*JIS":
        case "*YYMD":
          pattern.append("uuuu").append(separator).append("MM").append(separator).append("dd");

          break;

        case "*LONGJUL":
          pattern.append("uuuu").append(separator).append("DDD");

          break;

        case "*JUL":
          pattern.append("uu").append(separator).append("DDD");

          break;

        case "*MDYY":
        case "*USA":
          pattern.append("MM").append(separator).append("dd").append(separator).append("uuuu");

          break;

        default:
          throw new IllegalArgumentException("Invalid date format: <" + dateFormat + ">");
      }

      result = DateTimeFormatter.ofPattern(pattern.toString());
      getDateFormatterMap().put(key, result);
    }

    return result;
  }

  /** @return the dateFormatterMap */
  private static Map<String, DateTimeFormatter> getDateFormatterMap() {
    if (dateFormatterMap == null) {
      dateFormatterMap = new ConcurrentHashMap<>();
    }

    return dateFormatterMap;
  }

  /**
   * @param dateFormat
   * @param separator
   * @return
   */
  protected static int getDateLength(String dateFormat, String separator) {
    int result = 0;
    separator =
        (separator == null)
            ? getDefaultDateSeparator(dateFormat)
            : ((separator.length() > 1) ? separator.substring(0, 1) : separator);
    dateFormat = (dateFormat == null) ? "" : dateFormat.trim().toUpperCase();

    switch (dateFormat) {
      case "*CDMY":
      case "*CMDY":
      case "*CYMD":
        result = separator.isEmpty() ? 7 : 9;

        break;

      case "*DMY":
      case "*MDY":
      case "*YMD":
        result = separator.isEmpty() ? 6 : 8;

        break;

      case "*DMYY":
      case "*EUR":
      case "*ISO":
      case "*JIS":
      case "*MDYY":
      case "*YYMD":
      case "*USA":
        result = separator.isEmpty() ? 8 : 10;

        break;

      case "*LONGJUL":
        result = separator.isEmpty() ? 7 : 8;

        break;

      case "*JUL":
        result = separator.isEmpty() ? 5 : 6;

        break;

      default:
        throw new IllegalArgumentException("Invalid date format: <" + dateFormat + ">");
    }

    return result;
  }

  /**
   * @param dateFormat
   * @return
   */
  protected static String getDefaultDateSeparator(String dateFormat) {
    String result = "";
    dateFormat = (dateFormat == null) ? "" : dateFormat.trim().toUpperCase();

    switch (dateFormat) {
      case "*CDMY":
      case "*CMDY":
      case "*CYMD":
      case "*DMY":
      case "*DMYY":
      case "*JUL":
      case "*LONGJUL":
      case "*MDY":
      case "*MDYY":
      case "*USA":
      case "*YMD":
      case "*YYMD":
        result = "/";

        break;

      case "*EUR":
        result = ".";

        break;

      case "*ISO":
      case "*JIS":
        result = "-";

        break;

      default:
        throw new IllegalArgumentException("Invalid date format: <" + dateFormat + ">");
    }

    return result;
  }

  /**
   * @param timeFormat
   * @return
   */
  protected static String getDefaultTimeSeparator(String timeFormat) {
    String result = "";
    timeFormat = (timeFormat == null) ? "" : timeFormat.trim().toUpperCase();

    switch (timeFormat) {
      case "*EUR":
      case "*ISO":
        result = ".";

        break;

      case "*HMS":
      case "*JIS":
      case "*USA":
        result = ":";

        break;

      default:
        throw new IllegalArgumentException("Invalid time format: <" + timeFormat + ">");
    }

    return result;
  }

  /** @return */
  private static char[] getIBM0372UTF16Table() {
    if ((tableIBM037ToUTF16 == null) && !convertionTablesInitialized) {
      initializeConversionTable();
    }

    return tableIBM037ToUTF16;
  }

  /**
   * @param timeFormat
   * @param separator
   * @return
   */
  private static synchronized DateTimeFormatter getTimeFormatter(
      String timeFormat, String separator) {
    DateTimeFormatter result = null;
    StringBuilder pattern = new StringBuilder();
    separator =
        (separator == null)
            ? getDefaultTimeSeparator(timeFormat)
            : ((separator.length() > 1) ? separator.substring(0, 1) : separator);
    timeFormat = (timeFormat == null) ? "" : timeFormat.trim().toUpperCase();

    String key = timeFormat + separator;

    if (getTimeFormatterMap().containsKey(key) && (getTimeFormatterMap().get(key) != null)) {
      result = getTimeFormatterMap().get(key);
    } else {
      switch (timeFormat) {
        case "*EUR":
        case "*HMS":
        case "*ISO":
        case "*JIS":
          pattern.append("HH").append(separator).append("mm").append(separator).append("ss");

          break;

        case "*USA":
          pattern
              .append("hh")
              .append(separator)
              .append("mm")
              .append(separator.isEmpty() ? "" : ' ')
              .append("a");

          break;

        default:
          throw new IllegalArgumentException("Invalid time format: <" + timeFormat + ">");
      }

      result = DateTimeFormatter.ofPattern(pattern.toString());
      getTimeFormatterMap().put(key, result);
    }

    return result;
  }

  /** @return the timeFormatterMap */
  private static Map<String, DateTimeFormatter> getTimeFormatterMap() {
    if (timeFormatterMap == null) {
      timeFormatterMap = new ConcurrentHashMap<>();
    }

    return timeFormatterMap;
  }

  /**
   * @param timeFormat
   * @param separator
   * @return
   */
  protected static int getTimeLength(String timeFormat, String separator) {
    int result = 0;
    timeFormat = (timeFormat == null) ? "" : timeFormat.trim().toUpperCase();
    separator =
        (separator == null)
            ? getDefaultTimeSeparator(timeFormat)
            : ((separator.length() > 1) ? separator.substring(0, 1) : separator);

    switch (timeFormat) {
      case "*EUR":
      case "*HMS":
      case "*ISO":
      case "*JIS":
      case "*USA":
        result = separator.isEmpty() ? 6 : 8;

        break;

      default:
        throw new IllegalArgumentException("Invalid time format: <" + timeFormat + ">");
    }

    return result;
  }

  /**
   * Java's DateTimeFormatter only supports nanoseconds, picosecond supported by IBM i cannot be
   * implemented here.
   */
  private static synchronized DateTimeFormatter getTimestampFormatter(boolean includeSeparator) {
    DateTimeFormatter result = null;

    if (includeSeparator) {
      if (timestampFormatterWithSeparator == null) {
        timestampFormatterWithSeparator =
            DateTimeFormatter.ofPattern("uuuu-MM-dd-HH.mm.ss.nnnnnnnnn");
      }

      result = timestampFormatterWithSeparator;
    } else {
      if (timestampFormatterWithoutSeparator == null) {
        timestampFormatterWithoutSeparator = DateTimeFormatter.ofPattern("uuuuMMddHHmmssnnnnnnnnn");
      }

      result = timestampFormatterWithoutSeparator;
    }

    return result;
  }

  /** @return */
  private static byte[] getUTF162IBM037Table() {
    if ((tableUTF16ToIBM037 == null) && !convertionTablesInitialized) {
      initializeConversionTable();
    }

    return tableUTF16ToIBM037;
  }

  /** Initialize conversion tables: IBM037 to UTF-16 UTF-16 to IBM037. */
  private static synchronized void initializeConversionTable() {
    List<Character> ibm2unicode = new ArrayList<>();
    byte inputBuffer[] = new byte[2];
    int count = 0;
    int maxUnicode = 0x00;
    char data = 0x00;

    if (!convertionTablesInitialized) {
      convertionTablesInitialized = true;

      // Locate the file and create input stream
      InputStream inputStream = MoveUtils.class.getResourceAsStream(CONVERSION_TABLE_PATH);

      if (inputStream != null) {
        BufferedInputStream bufferedStream = new BufferedInputStream(inputStream);

        /*
         * Load the conversion table
         *
         * The structure of the Unicode conversion file is identical
         * with the structure of files produced by Linux ICONV program.
         *
         * First two bytes are always FF FE, followed by pairs of bytes
         * containing UTF-16 values, one for each IBM037 character.
         * Total 256 unicode values (512 bytes). Total length of the
         * conversion table file is 514 bytes. The Unicode words are
         * encoded as little-endian.
         */
        try {
          // Read and check the first two bytes - must be 0xFEFF
          count = bufferedStream.read(inputBuffer);

          if (count == 2) {
            data = (char) ((inputBuffer[0] & 0x00FF) | ((inputBuffer[1] & 0x00FF) << 8));

            if (data == 0xFEFF) {
              // File validated, read the content and store it.
              // Total 256 words expected
              count = bufferedStream.read(inputBuffer);

              while (count > 0) {
                if (count == 2) {
                  data = (char) ((inputBuffer[0] & 0x00FF) | ((inputBuffer[1] & 0x00FF) << 8));
                  ibm2unicode.add(data);
                } else {
                  ibm2unicode = null;
                  logger.error(
                      "Premature end of conversion file <{}> detected", CONVERSION_TABLE_PATH);

                  break;
                }

                count = bufferedStream.read(inputBuffer);
              }

              if (ibm2unicode != null) {
                /*
                 * Should have the table now. Run check for
                 * duplicate mappings (i.e. two different IBMxxx
                 * characters mapped to the same Unicode
                 * character.
                 */
                if (!ibm2unicode.isEmpty()) {
                  // List of processed Unicode values, for
                  // checking only
                  List<Character> encounteredChars = new ArrayList<>();

                  for (int i = 0; i < ibm2unicode.size(); i++) {
                    if (encounteredChars.contains(ibm2unicode.get(i))) {
                      ibm2unicode = null;
                      logger.error(
                          "Ambiguous IBM->UTF-16 mapping detected in <{}>", CONVERSION_TABLE_PATH);

                      break;
                    }

                    encounteredChars.add(ibm2unicode.get(i));

                    if (maxUnicode < ibm2unicode.get(i)) {
                      maxUnicode = ibm2unicode.get(i);
                    }
                  }
                } else {
                  ibm2unicode = null;
                  logger.error("Empty conversion table file <{}>", CONVERSION_TABLE_PATH);
                }
              }
            } else {
              ibm2unicode = null;
              logger.error(
                  "Invalid format of conversion file <{}> detected", CONVERSION_TABLE_PATH);
            }
          } else {
            ibm2unicode = null;
            logger.error("Invalid format of conversion file <{}> detected", CONVERSION_TABLE_PATH);
          }
        } catch (FileNotFoundException e) {
          ibm2unicode = null;
          logger.error("Failed to load conversion table <{}>", CONVERSION_TABLE_PATH);
        } catch (IOException e) {
          ibm2unicode = null;
          logger.error("Failed to load conversion table <{}>", CONVERSION_TABLE_PATH);
        } finally {
          try {
            bufferedStream.close();
          } catch (IOException e) {
          }
        }
      } else {
        logger.error("Can not find conversion table file <{}>", CONVERSION_TABLE_PATH);
      }

      if ((ibm2unicode != null) && !ibm2unicode.isEmpty()) {
        tableUTF16ToIBM037 = new byte[maxUnicode + 1];
        tableIBM037ToUTF16 = new char[ibm2unicode.size()];

        for (int i = 0; i < ibm2unicode.size(); i++) {
          tableIBM037ToUTF16[i] = ibm2unicode.get(i);
          tableUTF16ToIBM037[ibm2unicode.get(i)] = (byte) i;
        }
      }
    }
  }

  /**
   * @param dateFormat
   * @return
   */
  protected static boolean isValidDateFormatName(String dateFormat) {
    boolean result = false;
    dateFormat = (dateFormat == null) ? "" : dateFormat.trim().toUpperCase();

    switch (dateFormat) {
      case "*CDMY":
      case "*CMDY":
      case "*CYMD":
      case "*DMY":
      case "*DMYY":
      case "*EUR":
      case "*ISO":
      case "*JIS":
      case "*LONGJUL":
      case "*JUL":
      case "*MDY":
      case "*MDYY":
      case "*USA":
      case "*YMD":
      case "*YYMD":
        result = true;

        break;
    }

    return result;
  }

  /**
   * @param timeFormat
   * @return
   */
  protected static boolean isValidTimeFormatName(String timeFormat) {
    boolean result = false;
    timeFormat = (timeFormat == null) ? "" : timeFormat.trim().toUpperCase();

    switch (timeFormat) {
      case "*EUR":
      case "*HMS":
      case "*ISO":
      case "*JIS":
      case "*USA":
        result = true;

        break;
    }

    return result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal move(
      BigDecimal source,
      BigDecimal target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVE operation must be positive!");
    }

    if ((sourceDecimal < 0) || (targetDecimal < 0)) {
      throw new IllegalArgumentException(
          "Operands' decimal positions of MOVE operation cannot be negative!");
    }

    // get byte arrays of source and target in IBM37
    byte sourceZoned[] = convertBigDecimalToZoned(source, sourceLength, sourceDecimal);
    byte targetZoned[] = null;

    if (sourceLength >= targetLength) {
      targetZoned = new byte[targetLength];
    } else {
      targetZoned = convertBigDecimalToZoned(target, targetLength, targetDecimal);
    }

    // set value to target byte array
    int top = Math.min(sourceLength, targetLength);
    System.arraycopy(sourceZoned, sourceLength - top, targetZoned, targetLength - top, top);

    // Boolean switch for pad zeros from left
    if (pad) {
      for (int i = 0; i < (targetLength - sourceLength); i++) {
        targetZoned[i] = (byte) 0xF0;
      }
    }

    return convertZonedToBigDecimal(targetZoned, targetDecimal);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      BigDecimal source,
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] =
          move(source, target[i], sourceLength, targetLength, sourceDecimal, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger move(
      BigDecimal source,
      BigInteger target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean unsigned,
      boolean pad) {
    BigInteger result = target;

    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    result =
        move(source, new BigDecimal(target), sourceLength, targetLength, sourceDecimal, 0, pad)
            .toBigInteger();

    return unsigned ? result.abs() : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      BigDecimal source,
      BigInteger target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, targetLength, sourceDecimal, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @return
   */
  public static Boolean move(
      BigDecimal source, Boolean target, int sourceLength, int sourceDecimal) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    String resultStr = move(source, target ? "1" : "0", sourceLength, 1, sourceDecimal, false);

    if (!"1".equals(resultStr) && !"0".equals(resultStr)) {
      throw new IllegalArgumentException(
          "Value <"
              + resultStr
              + "> in source <"
              + source.toPlainString()
              + "> is invalid for boolean!");
    }

    return "1".equals(resultStr);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @return
   */
  public static Boolean[] move(
      BigDecimal source, Boolean target[], int sourceLength, int sourceDecimal) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, sourceDecimal);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int move(
      BigDecimal source,
      int target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    return move(
            source,
            BigDecimal.valueOf(target),
            sourceLength,
            targetLength,
            sourceDecimal,
            targetDecimal,
            pad)
        .intValue();
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param dateFormat
   * @return
   */
  public static LocalDate move(
      BigDecimal source, LocalDate target, int sourceLength, int sourceDecimal, String dateFormat) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if (sourceLength <= 0) {
      throw new IllegalArgumentException("Operands' lengths of MOVE operation must be positive!");
    }

    if (sourceDecimal < 0) {
      throw new IllegalArgumentException(
          "Operands' decimal positions of MOVE operation cannot be negative!");
    }

    if (!isValidDateFormatName(dateFormat)) {
      throw new IllegalArgumentException("Invalid date format: <" + dateFormat + ">");
    }

    int length = getDateLength(dateFormat, "");

    if (sourceLength < length) {
      throw new IllegalArgumentException(
          "Source numeric length <" + sourceLength + "> is too small to contain a complete date.");
    }

    String str = padRight("", length, '0');
    str = move(source, str, sourceLength, length, sourceDecimal, false);

    return convertStringToDate(str, dateFormat, "");
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param dateFormat
   * @return
   */
  public static LocalDate[] move(
      BigDecimal source,
      LocalDate target[],
      int sourceLength,
      int sourceDecimal,
      String dateFormat) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, sourceDecimal, dateFormat);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param targetLength
   * @return
   */
  public static LocalDateTime move(
      BigDecimal source,
      LocalDateTime target,
      int sourceLength,
      int targetLength,
      int sourceDecimal) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVE operation must be positive!");
    }

    if (sourceDecimal < 0) {
      throw new IllegalArgumentException(
          "Operand's decimal position of MOVE operation cannot be negative!");
    }

    if (targetLength < 19) {
      throw new IllegalArgumentException(
          "Target length <" + targetLength + "> is too small to contain a complete timestamp.");
    }

    if (targetLength == 20) {
      throw new IllegalArgumentException("Invalid target length: <" + targetLength + ">.");
    }

    if (targetLength > 32) {
      throw new IllegalArgumentException(
          "Target length <" + targetLength + "> is over precision of timestamp.");
    }

    targetLength = (targetLength == 19) ? (targetLength - 5) : (targetLength - 6);

    if (sourceLength < targetLength) {
      throw new IllegalArgumentException(
          "Source numeric length <"
              + sourceLength
              + "> is too small to contain a complete timestamp.");
    }

    String str = padRight("", targetLength, '0');
    str = move(source, str, sourceLength, targetLength, sourceDecimal, false);

    return convertStringToTimestamp(str, false);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param targetLength
   * @return
   */
  public static LocalDateTime[] move(
      BigDecimal source,
      LocalDateTime target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, targetLength, sourceDecimal);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param timeFormat
   * @return
   */
  public static LocalTime move(
      BigDecimal source, LocalTime target, int sourceLength, int sourceDecimal, String timeFormat) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if (sourceLength <= 0) {
      throw new IllegalArgumentException("Operands' lengths of MOVE operation must be positive!");
    }

    if (sourceDecimal < 0) {
      throw new IllegalArgumentException(
          "Operands' decimal positions of MOVE operation cannot be negative!");
    }

    timeFormat = (timeFormat == null) ? "" : timeFormat.trim().toUpperCase();

    if (!isValidTimeFormatName(timeFormat) || "*USA".equalsIgnoreCase(timeFormat)) {
      throw new IllegalArgumentException("Invalid time format: <" + timeFormat + ">");
    }

    int length = getTimeLength(timeFormat, "");

    if (sourceLength < length) {
      throw new IllegalArgumentException(
          "Source numeric length <" + sourceLength + "> is too small to contain a complete time.");
    }

    String str = padRight("", length, '0');
    str = move(source, str, sourceLength, length, sourceDecimal, false);

    return convertStringToTime(str, timeFormat, "");
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param timeFormat
   * @return
   */
  public static LocalTime[] move(
      BigDecimal source,
      LocalTime target[],
      int sourceLength,
      int sourceDecimal,
      String timeFormat) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, sourceDecimal, timeFormat);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static long move(
      BigDecimal source,
      long target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    return move(
            source,
            BigDecimal.valueOf(target),
            sourceLength,
            targetLength,
            sourceDecimal,
            targetDecimal,
            pad)
        .longValue();
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long move(
      BigDecimal source,
      Long target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean unsigned,
      boolean pad) {
    Long result = target;

    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    result =
        move(source, BigDecimal.valueOf(target), sourceLength, targetLength, sourceDecimal, 0, pad)
            .longValue();

    return unsigned ? Math.abs(result) : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   */
  public static Long[] move(
      BigDecimal source,
      Long target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, targetLength, sourceDecimal, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   * @author Amit Arya
   */
  public static String move(BigDecimal source, String target) {
    return source.toString();
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   */
  public static String move(
      BigDecimal source,
      String target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVE operation must be positive!");
    }

    if (sourceDecimal < 0) {
      throw new IllegalArgumentException(
          "Operand's decimal position of MOVE operation cannot be negative!");
    }

    byte sourceZoned[] = convertBigDecimalToZoned(source, sourceLength, sourceDecimal);
    byte targetBytes[] = null;

    if (sourceLength >= targetLength) {
      targetBytes = new byte[targetLength];
    } else {
      if (target.length() > targetLength) {
        throw new IllegalArgumentException(
            "Invalid target length <" + target.length() + "> to expected <" + targetLength + ">");
      }

      targetBytes = convertUTF16ToIBM037(padRight(target, targetLength, ' ').toCharArray());
    }

    int top = Math.min(sourceLength, targetLength);
    System.arraycopy(sourceZoned, sourceLength - top, targetBytes, targetLength - top, top);

    /*
     * Pad (in documentation "P") specified in the operation extender
     * position causes the result field to be padded on the left after the
     * move occurs.
     */
    if (pad) {
      for (int i = 0; i < (targetLength - sourceLength); i++) {
        targetBytes[i] = (byte) 0x40;
      }
    }

    return String.valueOf(convertIBM037ToUTF16(targetBytes));
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   */
  public static String[] move(
      BigDecimal source,
      String target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, targetLength, sourceDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      BigDecimal source[],
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          move(source[i], target[i], sourceLength, targetLength, sourceDecimal, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      BigDecimal source[],
      BigInteger target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          move(source[i], target[i], sourceLength, targetLength, sourceDecimal, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @return
   */
  public static Boolean[] move(
      BigDecimal source[], Boolean target[], int sourceLength, int sourceDecimal) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, sourceDecimal);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param dateFormat
   * @return
   */
  public static LocalDate[] move(
      BigDecimal source[],
      LocalDate target[],
      int sourceLength,
      int sourceDecimal,
      String dateFormat) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, sourceDecimal, dateFormat);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param timeFormat
   * @return
   */
  public static LocalTime[] move(
      BigDecimal source[],
      LocalTime target[],
      int sourceLength,
      int sourceDecimal,
      String timeFormat) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, sourceDecimal, timeFormat);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] move(
      BigDecimal source[],
      Long target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          move(source[i], target[i], sourceLength, targetLength, sourceDecimal, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   */
  public static String[] move(
      BigDecimal source[],
      String target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, targetLength, sourceDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal move(
      BigInteger source,
      BigDecimal target,
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        new BigDecimal(unsigned ? source.abs() : source),
        target,
        sourceLength,
        targetLength,
        0,
        targetDecimal,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      BigInteger source,
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        new BigDecimal(unsigned ? source.abs() : source),
        target,
        sourceLength,
        targetLength,
        0,
        targetDecimal,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      BigInteger source[],
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          move(source[i], target[i], sourceLength, targetLength, targetDecimal, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger move(
      BigInteger source,
      BigInteger target,
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    BigInteger result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    result =
        move(
                new BigDecimal(sourceUnsigned ? source.abs() : source),
                new BigDecimal(target),
                sourceLength,
                targetLength,
                0,
                0,
                pad)
            .toBigInteger();

    return targetUnsigned ? result.abs() : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      BigInteger source,
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] =
          movel(source, target[i], sourceLength, targetLength, sourceUnsigned, targetUnsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      BigInteger source[],
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          move(
              source[i],
              target[i],
              sourceLength,
              targetLength,
              sourceUnsigned,
              targetUnsigned,
              pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param unsigned
   * @return
   */
  public static Boolean move(
      BigInteger source, Boolean target, int sourceLength, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, 0);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param unsigned
   * @return
   */
  public static Boolean[] move(
      BigInteger source, Boolean target[], int sourceLength, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, 0);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param unsigned
   * @return
   */
  public static Boolean[] move(
      BigInteger source[], Boolean target[], int sourceLength, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, unsigned);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param unsigned
   * @return
   */
  public static LocalDate move(
      BigInteger source, LocalDate target, int sourceLength, String dateFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, 0, dateFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param unsigned
   * @return
   */
  public static LocalDate[] move(
      BigInteger source,
      LocalDate target[],
      int sourceLength,
      String dateFormat,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, 0, dateFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param unsigned
   * @return
   */
  public static LocalDate[] move(
      BigInteger source[],
      LocalDate target[],
      int sourceLength,
      String dateFormat,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, dateFormat, unsigned);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @return
   */
  public static LocalDateTime move(
      BigInteger source,
      LocalDateTime target,
      int sourceLength,
      int targetLength,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, targetLength, 0);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @return
   */
  public static LocalDateTime[] move(
      BigInteger source,
      LocalDateTime target[],
      int sourceLength,
      int targetLength,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, targetLength, 0);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @return
   */
  public static LocalDateTime[] move(
      BigInteger source[],
      LocalDateTime target[],
      int sourceLength,
      int targetLength,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, targetLength, unsigned);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param unsigned
   * @return
   */
  public static LocalTime move(
      BigInteger source, LocalTime target, int sourceLength, String timeFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, 0, timeFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param unsigned
   * @return
   */
  public static LocalTime[] move(
      BigInteger source,
      LocalTime target[],
      int sourceLength,
      String timeFormat,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, 0, timeFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param unsigned
   * @return
   */
  public static LocalTime[] move(
      BigInteger source[],
      LocalTime target[],
      int sourceLength,
      String timeFormat,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, timeFormat, unsigned);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long move(
      BigInteger source,
      Long target,
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    Long result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    result =
        move(
                new BigDecimal(sourceUnsigned ? source.abs() : source),
                BigDecimal.valueOf(target),
                sourceLength,
                targetLength,
                0,
                0,
                pad)
            .longValue();

    return targetUnsigned ? Math.abs(result) : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long[] move(
      BigInteger source,
      Long target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] =
          move(source, target[i], sourceLength, targetLength, sourceUnsigned, targetUnsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long[] move(
      BigInteger source[],
      Long target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          move(
              source[i],
              target[i],
              sourceLength,
              targetLength,
              sourceUnsigned,
              targetUnsigned,
              pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static String move(
      BigInteger source,
      String target,
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        new BigDecimal(unsigned ? source.abs() : source),
        target,
        sourceLength,
        targetLength,
        0,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static String[] move(
      BigInteger source,
      String target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        new BigDecimal(unsigned ? source.abs() : source),
        target,
        sourceLength,
        targetLength,
        0,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static String[] move(
      BigInteger source[],
      String target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int move(
      boolean source, int target, int sourceLength, int targetLength, boolean pad) {
    return move(String.valueOf(source), target, sourceLength, targetLength, pad);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static long move(
      boolean source,
      long target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    return source ? 1 : 0;
  }

  /**
   * @param source
   * @param target
   * @return
   * @author Amit Arya
   */
  public static String move(boolean source, String target) {
    return source ? "1" : "0";
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static String move(
      boolean source,
      String target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean pad) {
    return StringUtils.toChar(source, sourceLength).substring(targetLength);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal move(
      Boolean source, BigDecimal target, int targetLength, int targetDecimal, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(source ? "1" : "0", target, 1, targetLength, targetDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      Boolean source, BigDecimal target[], int targetLength, int targetDecimal, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source ? "1" : "0", target[i], 1, targetLength, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      Boolean source[], BigDecimal target[], int targetLength, int targetDecimal, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i] ? "1" : "0", target[i], 1, targetLength, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger move(
      Boolean source, BigInteger target, int targetLength, boolean unsigned, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(source ? "1" : "0", target, 1, targetLength, unsigned, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      Boolean source, BigInteger target[], int targetLength, boolean unsigned, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source ? "1" : "0", target[i], 1, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      Boolean source[], BigInteger target[], int targetLength, boolean unsigned, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i] ? "1" : "0", target[i], 1, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static Boolean move(Boolean source, Boolean target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return Boolean.valueOf(source.booleanValue());
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static Boolean[] move(Boolean source, Boolean target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = Boolean.valueOf(source.booleanValue());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static Boolean[] move(Boolean source[], Boolean target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = Boolean.valueOf(source[i].booleanValue());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long move(
      Boolean source, Long target, int targetLength, boolean unsigned, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(source ? "1" : "0", target, 1, targetLength, unsigned, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] move(
      Boolean source, Long target[], int targetLength, boolean unsigned, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source ? "1" : "0", target[i], 1, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] move(
      Boolean source[], Long target[], int targetLength, boolean unsigned, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i] ? "1" : "0", target[i], 1, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param pad
   * @return
   */
  public static String move(Boolean source, String target, int targetLength, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(source ? "1" : "0", target, 1, targetLength, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param pad
   * @return
   */
  public static String[] move(Boolean source, String target[], int targetLength, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source ? "1" : "0", target[i], 1, targetLength, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param pad
   * @return
   */
  public static String[] move(Boolean source[], String target[], int targetLength, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i] ? "1" : "0", target[i], 1, targetLength, pad);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param unsigned
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static BigDecimal move(
      int source,
      BigDecimal target,
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean unsigned,
      boolean pad) {
    return move(
        BigInteger.valueOf(source),
        target,
        sourceLength,
        targetLength,
        targetDecimal,
        unsigned,
        pad);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static BigDecimal move(
      int source,
      BigDecimal target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    return move(
        BigDecimal.valueOf(source),
        target,
        sourceLength,
        targetLength,
        sourceDecimal,
        targetDecimal,
        pad);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static boolean move(
      int source, boolean target, int sourceLength, int targetLength, boolean pad) {
    return (source == 1);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param unsigned
   * @return
   * @author Amit Arya
   */
  public static Boolean move(int source, Boolean target, int sourceLength, boolean unsigned) {
    return move(BigInteger.valueOf(source), target, sourceLength, unsigned);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int move(
      int source, int target, int sourceLength, int targetLength, int sourceDecimal, boolean pad) {
    return NumberFormatter.toInt(
        move(
            BigDecimal.valueOf(source),
            String.valueOf(target),
            sourceLength,
            targetLength,
            sourceDecimal,
            pad));
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param unsigned
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int move(
      int source,
      int target,
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean unsigned,
      boolean pad) {
    return move(
            BigInteger.valueOf(source),
            BigDecimal.valueOf(target),
            sourceLength,
            targetLength,
            targetDecimal,
            unsigned,
            pad)
        .intValue();
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int move(
      int source,
      int target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    return move(
            BigDecimal.valueOf(source),
            BigDecimal.valueOf(target),
            sourceLength,
            targetLength,
            sourceDecimal,
            targetDecimal,
            pad)
        .intValue();
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param dateFormat
   * @return
   * @author Amit Arya
   */
  public static LocalDate move(
      int source, LocalDate target, int sourceLength, int sourceDecimal, String dateFormat) {
    return move(BigDecimal.valueOf(source), target, sourceLength, sourceDecimal, dateFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param unsigned
   * @return
   * @author Amit Arya
   */
  public static LocalDate move(
      int source, LocalDate target, int sourceLength, String dateFormat, boolean unsigned) {
    // Validate parameters
    if (target == null) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        new BigDecimal(unsigned ? Math.abs(source) : source), target, sourceLength, 0, dateFormat);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param timeFormat
   * @return
   * @author Amit Arya
   */
  public static LocalTime move(
      int source, LocalTime target, int sourceLength, int sourceDecimal, String timeFormat) {
    return move(BigDecimal.valueOf(source), target, sourceLength, sourceDecimal, timeFormat);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static long move(
      int source,
      long target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    return source;
  }

  /**
   * @param source
   * @param target
   * @return
   * @author Amit Arya
   */
  public static String move(int source, String target) {
    return String.valueOf(source);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static String move(
      int source,
      String target,
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    return move(BigInteger.valueOf(source), target, sourceLength, targetLength, unsigned, pad);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static String move(
      int source,
      String target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean pad) {
    return move(BigDecimal.valueOf(source), target, sourceLength, targetLength, sourceDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param dateFormat
   * @param pad
   * @return
   */
  public static BigDecimal move(
      LocalDate source,
      BigDecimal target,
      int targetLength,
      int targetDecimal,
      String dateFormat,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if (targetLength <= 0) {
      throw new IllegalArgumentException("Operand's length of MOVE operation must be positive!");
    }

    if (targetDecimal < 0) {
      throw new IllegalArgumentException(
          "Operand's decimal position of MOVE operation cannot be negative!");
    }

    if (!isValidDateFormatName(dateFormat)) {
      throw new IllegalArgumentException("Invalid date format: <" + dateFormat + ">");
    }

    if (targetLength < getDateLength(dateFormat, "")) {
      throw new IllegalArgumentException(
          "Target numeric length <" + targetLength + "> is too small to contain a complete date.");
    }

    String sourceStr = convertDateToString(source, dateFormat, "");

    return move(sourceStr, target, sourceStr.length(), targetLength, targetDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param dateFormat
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      LocalDate source,
      BigDecimal target[],
      int targetLength,
      int targetDecimal,
      String dateFormat,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], targetLength, targetDecimal, dateFormat, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param dateFormat
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      LocalDate source[],
      BigDecimal target[],
      int targetLength,
      int targetDecimal,
      String dateFormat,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], targetLength, targetDecimal, dateFormat, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger move(
      LocalDate source,
      BigInteger target,
      int targetLength,
      String dateFormat,
      boolean unsigned,
      boolean pad) {
    BigInteger result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    result = move(source, new BigDecimal(target), targetLength, 0, dateFormat, pad).toBigInteger();

    return unsigned ? result.abs() : result;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      LocalDate source,
      BigInteger target[],
      int targetLength,
      String dateFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], targetLength, dateFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      LocalDate source[],
      BigInteger target[],
      int targetLength,
      String dateFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], targetLength, dateFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int move(
      LocalDate source,
      int target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    return move(
            DateTimeUtils.toInt(source),
            BigDecimal.valueOf(target),
            sourceLength,
            targetLength,
            sourceDecimal,
            targetDecimal,
            pad)
        .intValue();
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param dateFormat
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int move(
      LocalDate source,
      int target,
      int targetLength,
      int targetDecimal,
      String dateFormat,
      boolean pad) {
    return move(source, BigDecimal.valueOf(target), targetLength, targetDecimal, dateFormat, pad)
        .intValue();
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDate move(LocalDate source, LocalDate target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return LocalDate.of(source.getYear(), source.getMonthValue(), source.getDayOfMonth());
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDate[] move(LocalDate source, LocalDate target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = LocalDate.of(source.getYear(), source.getMonthValue(), source.getDayOfMonth());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDate[] move(LocalDate source[], LocalDate target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          LocalDate.of(source[i].getYear(), source[i].getMonthValue(), source[i].getDayOfMonth());
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param dateFormat
   * @return
   * @author Amit Arya
   */
  public static LocalDate move(
      LocalDate source, LocalDate target, int targetLength, int targetDecimal, String dateFormat) {
    return source;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime move(LocalDate source, LocalDateTime target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return LocalDateTime.of(
        source.getYear(),
        source.getMonthValue(),
        source.getDayOfMonth(),
        target.getHour(),
        target.getMinute(),
        target.getSecond(),
        target.getNano());
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime[] move(LocalDate source, LocalDateTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] =
          LocalDateTime.of(
              source.getYear(),
              source.getMonthValue(),
              source.getDayOfMonth(),
              target[i].getHour(),
              target[i].getMinute(),
              target[i].getSecond(),
              target[i].getNano());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime[] move(LocalDate source[], LocalDateTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          LocalDateTime.of(
              source[i].getYear(),
              source[i].getMonthValue(),
              source[i].getDayOfMonth(),
              target[i].getHour(),
              target[i].getMinute(),
              target[i].getSecond(),
              target[i].getNano());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long move(
      LocalDate source,
      Long target,
      int targetLength,
      String dateFormat,
      boolean unsigned,
      boolean pad) {
    Long result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    result = move(source, BigDecimal.valueOf(target), targetLength, 0, dateFormat, pad).longValue();

    return unsigned ? Math.abs(result) : result;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] move(
      LocalDate source,
      Long target[],
      int targetLength,
      String dateFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], targetLength, dateFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @author Amit Arya
   * @return
   */
  public static String move(
      LocalDate source,
      String target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean pad) {
    return move(
        DateTimeUtils.toInt(source), target, sourceLength, targetLength, sourceDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] move(
      LocalDate source[],
      Long target[],
      int targetLength,
      String dateFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], targetLength, dateFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param dateFormat
   * @return
   * @author Amit Arya
   */
  public static String move(
      LocalDate source, String target, int targetLength, int targetDecimal, String dateFormat) {
    return DateTimeUtils.toString(source, "yyyy-MM-dd");
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param pad
   * @return
   */
  public static String move(
      LocalDate source,
      String target,
      int targetLength,
      String dateFormat,
      String separator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if (targetLength <= 0) {
      throw new IllegalArgumentException("Operand's length of MOVE operation must be positive!");
    }

    if (!isValidDateFormatName(dateFormat)) {
      throw new IllegalArgumentException("Invalid date format: <" + dateFormat + ">");
    }

    if (targetLength < getDateLength(dateFormat, separator)) {
      throw new IllegalArgumentException(
          "Target character length <"
              + targetLength
              + "> is too small to contain a complete date.");
    }

    String sourceStr = convertDateToString(source, dateFormat, separator);

    return move(sourceStr, target, sourceStr.length(), targetLength, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param pad
   * @return
   */
  public static String[] move(
      LocalDate source,
      String target[],
      int targetLength,
      String dateFormat,
      String separator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], targetLength, dateFormat, separator, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param separator
   * @param pad
   * @return
   */
  public static String[] move(
      LocalDate source[],
      String target[],
      int targetLength,
      String dateFormat,
      String separator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], targetLength, dateFormat, separator, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal move(
      LocalDateTime source,
      BigDecimal target,
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVE operation must be positive!");
    }

    if (sourceLength < 19) {
      throw new IllegalArgumentException(
          "Source length <" + sourceLength + "> is too small to contain a complete timestamp.");
    }

    if (sourceLength == 20) {
      throw new IllegalArgumentException("Invalid source length: <" + sourceLength + ">.");
    }

    if (sourceLength > 32) {
      throw new IllegalArgumentException(
          "Source length <" + sourceLength + "> is over precision of timestamp.");
    }

    if (((sourceLength == 19) ? (sourceLength - 5) : (sourceLength - 6)) > targetLength) {
      throw new IllegalArgumentException(
          "Target numeric length <"
              + targetLength
              + "> is too small to contain a complete timestamp.");
    }

    String sourceStr = convertTimestampToString(source, sourceLength, false);

    return move(sourceStr, target, sourceStr.length(), targetLength, targetDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      LocalDateTime source,
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, targetLength, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      LocalDateTime source[],
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, targetLength, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param pad
   * @return
   */
  public static BigInteger move(
      LocalDateTime source,
      BigInteger target,
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    BigInteger result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    result =
        move(source, new BigDecimal(target), sourceLength, targetLength, 0, pad).toBigInteger();

    return unsigned ? result.abs() : result;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      LocalDateTime source,
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      LocalDateTime source[],
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDate move(LocalDateTime source, LocalDate target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return LocalDate.of(source.getYear(), source.getMonthValue(), source.getDayOfMonth());
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDate[] move(LocalDateTime source, LocalDate target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = LocalDate.of(source.getYear(), source.getMonthValue(), source.getDayOfMonth());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDate[] move(LocalDateTime source[], LocalDate target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          LocalDate.of(source[i].getYear(), source[i].getMonthValue(), source[i].getDayOfMonth());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime move(LocalDateTime source, LocalDateTime target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return LocalDateTime.of(
        source.getYear(),
        source.getMonthValue(),
        source.getDayOfMonth(),
        source.getHour(),
        source.getMinute(),
        source.getSecond(),
        source.getNano());
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime[] move(LocalDateTime source, LocalDateTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] =
          LocalDateTime.of(
              source.getYear(),
              source.getMonthValue(),
              source.getDayOfMonth(),
              source.getHour(),
              source.getMinute(),
              source.getSecond(),
              source.getNano());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime[] move(LocalDateTime source[], LocalDateTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          LocalDateTime.of(
              source[i].getYear(),
              source[i].getMonthValue(),
              source[i].getDayOfMonth(),
              source[i].getHour(),
              source[i].getMinute(),
              source[i].getSecond(),
              source[i].getNano());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalTime move(LocalDateTime source, LocalTime target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return LocalTime.of(source.getHour(), source.getMinute(), source.getSecond(), 0);
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalTime[] move(LocalDateTime source, LocalTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = LocalTime.of(source.getHour(), source.getMinute(), source.getSecond(), 0);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalTime[] move(LocalDateTime source[], LocalTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          LocalTime.of(source[i].getHour(), source[i].getMinute(), source[i].getSecond(), 0);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param pad
   * @return
   */
  public static String move(
      LocalDateTime source,
      String target,
      int sourceLength,
      int targetLength,
      boolean includeSeparator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVE operation must be positive!");
    }

    if (sourceLength < 19) {
      throw new IllegalArgumentException(
          "Source length <" + sourceLength + "> is too small to contain a complete timestamp.");
    }

    if (sourceLength == 20) {
      throw new IllegalArgumentException("Invalid source length: <" + sourceLength + ">.");
    }

    if (sourceLength > 32) {
      throw new IllegalArgumentException(
          "Source length <" + sourceLength + "> is over precision of timestamp.");
    }

    if ((includeSeparator
            ? sourceLength
            : ((sourceLength == 19) ? (sourceLength - 5) : (sourceLength - 6)))
        > targetLength) {
      throw new IllegalArgumentException(
          "Target character length <"
              + targetLength
              + "> is too small to contain a complete timestamp.");
    }

    String sourceStr = convertTimestampToString(source, sourceLength, includeSeparator);

    return move(sourceStr, target, sourceStr.length(), targetLength, pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param includeSeparator
   * @param pad
   * @return
   */
  public static String[] move(
      LocalDateTime source,
      String target[],
      int sourceLength,
      int targetLength,
      boolean includeSeparator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, targetLength, includeSeparator, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param includeSeparator
   * @param pad
   * @return
   */
  public static String[] move(
      LocalDateTime source[],
      String target[],
      int sourceLength,
      int targetLength,
      boolean includeSeparator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, targetLength, includeSeparator, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param timeFormat
   * @param pad
   * @return
   */
  public static BigDecimal move(
      LocalTime source,
      BigDecimal target,
      int targetLength,
      int targetDecimal,
      String timeFormat,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if (targetLength <= 0) {
      throw new IllegalArgumentException("Operand's length of MOVE operation must be positive!");
    }

    timeFormat = (timeFormat == null) ? "" : timeFormat.trim().toUpperCase();

    if (!isValidTimeFormatName(timeFormat) || "*USA".equalsIgnoreCase(timeFormat)) {
      throw new IllegalArgumentException("Invalid time format: <" + timeFormat + ">");
    }

    if (targetLength < getTimeLength(timeFormat, "")) {
      throw new IllegalArgumentException(
          "Target numeric length <" + targetLength + "> is too small to contain a complete time.");
    }

    String sourceStr = convertTimeToString(source, timeFormat, "");

    return move(sourceStr, target, sourceStr.length(), targetLength, targetDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param timeFormat
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      LocalTime source,
      BigDecimal target[],
      int targetLength,
      int targetDecimal,
      String timeFormat,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], targetLength, targetDecimal, timeFormat, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param timeFormat
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      LocalTime source[],
      BigDecimal target[],
      int targetLength,
      int targetDecimal,
      String timeFormat,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], targetLength, targetDecimal, timeFormat, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger move(
      LocalTime source,
      BigInteger target,
      int targetLength,
      String timeFormat,
      boolean unsigned,
      boolean pad) {
    BigInteger result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    result = move(source, new BigDecimal(target), targetLength, 0, timeFormat, pad).toBigInteger();

    return unsigned ? result.abs() : result;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      LocalTime source,
      BigInteger target[],
      int targetLength,
      String timeFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], targetLength, timeFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      LocalTime source[],
      BigInteger target[],
      int targetLength,
      String timeFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], targetLength, timeFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime move(LocalTime source, LocalDateTime target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return LocalDateTime.of(
        target.getYear(),
        target.getMonthValue(),
        target.getDayOfMonth(),
        source.getHour(),
        source.getMinute(),
        source.getSecond(),
        target.getNano());
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime[] move(LocalTime source, LocalDateTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] =
          LocalDateTime.of(
              target[i].getYear(),
              target[i].getMonthValue(),
              target[i].getDayOfMonth(),
              source.getHour(),
              source.getMinute(),
              source.getSecond(),
              target[i].getNano());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime[] move(LocalTime source[], LocalDateTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          LocalDateTime.of(
              target[i].getYear(),
              target[i].getMonthValue(),
              target[i].getDayOfMonth(),
              source[i].getHour(),
              source[i].getMinute(),
              source[i].getSecond(),
              target[i].getNano());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalTime move(LocalTime source, LocalTime target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return LocalTime.of(source.getHour(), source.getMinute(), source.getSecond(), 0);
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalTime[] move(LocalTime source, LocalTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = LocalTime.of(source.getHour(), source.getMinute(), source.getSecond(), 0);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalTime[] move(LocalTime source[], LocalTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          LocalTime.of(source[i].getHour(), source[i].getMinute(), source[i].getSecond(), 0);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long move(
      LocalTime source,
      Long target,
      int targetLength,
      String timeFormat,
      boolean unsigned,
      boolean pad) {
    Long result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    result = move(source, BigDecimal.valueOf(target), targetLength, 0, timeFormat, pad).longValue();

    return unsigned ? Math.abs(result) : result;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param pad
   * @return
   */
  public static Long[] move(
      LocalTime source,
      Long target[],
      int targetLength,
      String timeFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], targetLength, timeFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] move(
      LocalTime source[],
      Long target[],
      int targetLength,
      String timeFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], targetLength, timeFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param pad
   * @return
   */
  public static String move(
      LocalTime source,
      String target,
      int targetLength,
      String timeFormat,
      String separator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if (targetLength <= 0) {
      throw new IllegalArgumentException("Operand's length of MOVE operation must be positive!");
    }

    if (!isValidTimeFormatName(timeFormat)) {
      throw new IllegalArgumentException("Invalid time format: <" + timeFormat + ">");
    }

    if (targetLength < getTimeLength(timeFormat, separator)) {
      throw new IllegalArgumentException(
          "Target character length <"
              + targetLength
              + "> is too small to contain a complete time.");
    }

    String sourceStr = convertTimeToString(source, timeFormat, separator);

    return move(sourceStr, target, sourceStr.length(), targetLength, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param separator
   * @param pad
   * @return
   */
  public static String[] move(
      LocalTime source,
      String target[],
      int targetLength,
      String timeFormat,
      String separator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], targetLength, timeFormat, separator, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param separator
   * @param pad
   * @return
   */
  public static String[] move(
      LocalTime source[],
      String target[],
      int targetLength,
      String timeFormat,
      String separator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], targetLength, timeFormat, separator, pad);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int move(
      long source, int target, int sourceLength, int targetLength, int sourceDecimal, boolean pad) {
    return NumberFormatter.toInt(
        move(
            BigDecimal.valueOf(source),
            String.valueOf(target),
            sourceLength,
            targetLength,
            sourceDecimal,
            pad));
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int move(
      long source,
      int target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    return (int) source;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static long move(
      long source,
      long target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    return source;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static String move(
      long source,
      String target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean pad) {
    return move(BigDecimal.valueOf(source), target, sourceLength, targetLength, sourceDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal move(
      Long source,
      BigDecimal target,
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        targetLength,
        0,
        targetDecimal,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      Long source,
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, targetLength, targetDecimal, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      Long source[],
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          move(source[i], target[i], sourceLength, targetLength, targetDecimal, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger move(
      Long source,
      BigInteger target,
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    BigInteger result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    result =
        move(
                BigDecimal.valueOf(sourceUnsigned ? Math.abs(source) : source),
                new BigDecimal(target),
                sourceLength,
                targetLength,
                0,
                0,
                pad)
            .toBigInteger();

    return targetUnsigned ? result.abs() : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      Long source,
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] =
          move(source, target[i], sourceLength, targetLength, sourceUnsigned, targetUnsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      Long source[],
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          move(
              source[i],
              target[i],
              sourceLength,
              targetLength,
              sourceUnsigned,
              targetUnsigned,
              pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param unsigned
   * @return
   */
  public static Boolean move(Long source, Boolean target, int sourceLength, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(BigDecimal.valueOf(unsigned ? Math.abs(source) : source), target, sourceLength, 0);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param unsigned
   * @return
   */
  public static Boolean[] move(Long source, Boolean target[], int sourceLength, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(BigDecimal.valueOf(unsigned ? Math.abs(source) : source), target, sourceLength, 0);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param unsigned
   * @return
   */
  public static Boolean[] move(
      Long source[], Boolean target[], int sourceLength, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, unsigned);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param unsigned
   * @return
   */
  public static LocalDate move(
      Long source, LocalDate target, int sourceLength, String dateFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        0,
        dateFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param unsigned
   * @return
   */
  public static LocalDate[] move(
      Long source, LocalDate target[], int sourceLength, String dateFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        0,
        dateFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param unsigned
   * @return
   */
  public static LocalDate[] move(
      Long source[], LocalDate target[], int sourceLength, String dateFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, dateFormat, unsigned);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param unsigned
   * @return
   */
  public static LocalTime move(
      Long source, LocalTime target, int sourceLength, String timeFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        0,
        timeFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param unsigned
   * @return
   */
  public static LocalTime[] move(
      Long source, LocalTime target[], int sourceLength, String timeFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        0,
        timeFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param unsigned
   * @return
   */
  public static LocalTime[] move(
      Long source[], LocalTime target[], int sourceLength, String timeFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, timeFormat, unsigned);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long move(
      Long source,
      Long target,
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    Long result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    result =
        MoveUtils.move(
                BigDecimal.valueOf(sourceUnsigned ? Math.abs(source) : source),
                BigDecimal.valueOf(target),
                sourceLength,
                targetLength,
                0,
                0,
                pad)
            .longValue();

    return targetUnsigned ? Math.abs(result) : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long[] move(
      Long source,
      Long target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] =
          move(source, target[i], sourceLength, targetLength, sourceUnsigned, targetUnsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long[] move(
      Long source[],
      Long target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          move(
              source[i],
              target[i],
              sourceLength,
              targetLength,
              sourceUnsigned,
              targetUnsigned,
              pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static String move(
      Long source,
      String target,
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        targetLength,
        0,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static String[] move(
      Long source,
      String target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    return move(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        targetLength,
        0,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static String[] move(
      Long source[],
      String target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal move(
      String source,
      BigDecimal target,
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVE operation must be positive!");
    }

    if (source.length() > sourceLength) {
      throw new IllegalArgumentException(
          "Invalid source length <" + source.length() + "> to expected <" + sourceLength + ">");
    }

    if (targetDecimal < 0) {
      throw new IllegalArgumentException(
          "Operand's decimal position of MOVE operation cannot be negative!");
    }

    // Get byte arrays of source and target in IBM37
    byte sourceBytes[] = convertUTF16ToIBM037(padRight(source, sourceLength, ' ').toCharArray());
    byte targetZoned[];

    if (sourceLength >= targetLength) {
      targetZoned = new byte[targetLength];
    } else {
      targetZoned = convertBigDecimalToZoned(target, targetLength, targetDecimal);
    }

    int top = Math.min(sourceLength, targetLength);

    for (int i = 0; i < top; i++) {
      if (((byte) (sourceBytes[sourceBytes.length - 1 - i] & 0x0F)) > ((byte) 0x09)) {
        targetZoned[targetZoned.length - 1 - i] =
            (byte) (sourceBytes[sourceBytes.length - 1 - i] & 0xF0);
      } else {
        if ((sourceBytes[sourceBytes.length - 1 - i]) == ((byte) 0x40)) {
          // Fix numeric for space
          sourceBytes[sourceBytes.length - 1 - i] = (byte) 0xF0;
        }

        targetZoned[targetZoned.length - 1 - i] = sourceBytes[sourceBytes.length - 1 - i];
      }
    }

    /*
     * Pad (in documentation "P") specified in the operation extender
     * position causes the result field to be padded on the left after the
     * move occurs.
     */
    if (pad) {
      for (int i = 0; i < (targetLength - sourceLength); i++) {
        targetZoned[i] = (byte) 0xF0;
      }
    }

    return convertZonedToBigDecimal(targetZoned, targetDecimal);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      String source,
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, targetLength, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger move(
      String source,
      BigInteger target,
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    BigInteger result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    result =
        move(source, new BigDecimal(target), sourceLength, targetLength, 0, pad).toBigInteger();

    return unsigned ? result.abs() : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      String source,
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static boolean move(
      String source,
      boolean target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean pad) {
    return move(source, target, sourceLength);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @return
   * @author Amit Arya
   */
  public static Boolean move(String source, Boolean target) {
    return move(source, target, 1);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @return
   */
  public static Boolean move(String source, Boolean target, int sourceLength) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if (sourceLength <= 0) {
      throw new IllegalArgumentException("Operand's length of MOVE operation must be positive!");
    }

    if (source.length() > sourceLength) {
      throw new IllegalArgumentException(
          "Invalid source length <" + source.length() + "> to expected <" + sourceLength + ">");
    }

    source = padRight(source, sourceLength, ' ');

    char ch = source.charAt(sourceLength - 1);

    if ((ch != '1') && (ch != '0')) {
      throw new IllegalArgumentException(
          "Value <" + ch + "> in source <" + source + "> is invalid for boolean!");
    }

    return ch == '1';
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @return
   */
  public static Boolean[] move(String source, Boolean target[], int sourceLength) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int move(
      String source, int target, int sourceLength, int targetLength, boolean pad) {
    return NumberFormatter.toInt(
        move(source, String.valueOf(target), sourceLength, targetLength, pad));
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int move(
      String source,
      int target,
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    return NumberFormatter.toInt(
        move(source, String.valueOf(target), sourceLength, targetLength, pad));
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int move(
      String source,
      int target,
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    return move(source, BigDecimal.valueOf(target), sourceLength, targetLength, targetDecimal, pad)
        .intValue();
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param separator
   * @return
   */
  public static LocalDate move(
      String source, LocalDate target, int sourceLength, String dateFormat, String separator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if (sourceLength <= 0) {
      throw new IllegalArgumentException("Operands' lengths of MOVE operation must be positive!");
    }

    if (source.length() > sourceLength) {
      throw new IllegalArgumentException(
          "Invalid source length <" + source.length() + "> to expected <" + sourceLength + ">");
    }

    if (!isValidDateFormatName(dateFormat)) {
      throw new IllegalArgumentException("Invalid date format: <" + dateFormat + ">");
    }

    int length = getDateLength(dateFormat, separator);

    if (sourceLength < length) {
      throw new IllegalArgumentException(
          "Source character length <"
              + sourceLength
              + "> is too small to contain a complete date.");
    }

    source = padRight(source, sourceLength, ' ');

    return convertStringToDate(
        source.substring(sourceLength - length, sourceLength), dateFormat, separator);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param separator
   * @return
   */
  public static LocalDate[] move(
      String source, LocalDate target[], int sourceLength, String dateFormat, String separator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, dateFormat, separator);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param includeSeparator
   * @return
   */
  public static LocalDateTime move(
      String source,
      LocalDateTime target,
      int sourceLength,
      int targetLength,
      boolean includeSeparator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVE operation must be positive!");
    }

    if (targetLength <= 0) {
      throw new IllegalArgumentException("Operands' lengths of MOVE operation must be positive!");
    }

    if (targetLength < 19) {
      throw new IllegalArgumentException(
          "Target length <" + targetLength + "> is too small to contain a complete timestamp.");
    }

    if (targetLength == 20) {
      throw new IllegalArgumentException("Invalid target length: <" + targetLength + ">.");
    }

    if (targetLength > 32) {
      throw new IllegalArgumentException(
          "Target length <" + targetLength + "> is over precision of timestamp.");
    }

    targetLength =
        includeSeparator
            ? targetLength
            : ((targetLength == 19) ? (targetLength - 5) : (targetLength - 6));

    if (source.length() > sourceLength) {
      throw new IllegalArgumentException(
          "Invalid source length <" + source.length() + "> to expected <" + sourceLength + ">");
    }

    if (sourceLength < targetLength) {
      throw new IllegalArgumentException(
          "Source character length <"
              + sourceLength
              + "> is too small to contain a complete timestamp.");
    }

    source = padRight(source, sourceLength, ' ');

    return convertStringToTimestamp(
        source.substring(source.length() - targetLength, source.length()), includeSeparator);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param includeSeparator
   * @return
   */
  public static LocalDateTime[] move(
      String source,
      LocalDateTime target[],
      int sourceLength,
      int targetLength,
      boolean includeSeparator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, targetLength, includeSeparator);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param separator
   * @return
   */
  public static LocalTime move(
      String source, LocalTime target, int sourceLength, String timeFormat, String separator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if (sourceLength <= 0) {
      throw new IllegalArgumentException("Operands' lengths of MOVE operation must be positive!");
    }

    if (source.length() > sourceLength) {
      throw new IllegalArgumentException(
          "Invalid source length <" + source.length() + "> to expected <" + sourceLength + ">");
    }

    if (!isValidTimeFormatName(timeFormat)) {
      throw new IllegalArgumentException("Invalid time format: <" + timeFormat + ">");
    }

    int length = getTimeLength(timeFormat, separator);

    if (sourceLength < length) {
      throw new IllegalArgumentException(
          "Source character length <"
              + sourceLength
              + "> is too small to contain a complete time.");
    }

    source = padRight(source, sourceLength, ' ');

    return convertStringToTime(
        source.substring(sourceLength - length, sourceLength), timeFormat, separator);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param separator
   * @return
   */
  public static LocalTime[] move(
      String source, LocalTime target[], int sourceLength, String timeFormat, String separator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, timeFormat, separator);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static long move(
      String source,
      long target,
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    return move(source, BigDecimal.valueOf(target), sourceLength, targetLength, targetDecimal, pad)
        .longValue();
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long move(
      String source,
      Long target,
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    Long result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    result =
        move(source, BigDecimal.valueOf(target), sourceLength, targetLength, 0, pad).longValue();

    return unsigned ? Math.abs(result) : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] move(
      String source,
      Long target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   * @author Amit Arya
   */
  public static String move(String source, String target) {
    return source;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   */
  public static String move(
      String source, String target, int sourceLength, int targetLength, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVE operation must be positive!");
    }

    if (source.length() > sourceLength) {
      throw new IllegalArgumentException(
          "Invalid source length <" + source.length() + "> to expected <" + sourceLength + ">");
    }

    // get char arrays of source and target
    char sourceChars[] = padRight(source, sourceLength, ' ').toCharArray();
    char targetChars[] = null;

    if (sourceLength >= targetLength) {
      targetChars = new char[targetLength];
    } else {
      if (target.length() > targetLength) {
        throw new IllegalArgumentException(
            "Invalid target length <" + target.length() + "> to expected <" + targetLength + ">");
      }

      targetChars = padRight(target, targetLength, ' ').toCharArray();
    }

    // set value to target char array
    int top = Math.min(sourceLength, targetLength);
    System.arraycopy(sourceChars, sourceLength - top, targetChars, targetLength - top, top);

    // Boolean switch for pad space from left
    if (pad) {
      for (int i = 0; i < (targetLength - sourceLength); i++) {
        targetChars[i] = ' ';
      }
    }

    return String.valueOf(targetChars);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static String move(
      String source,
      String target,
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    return move(source, target, sourceLength, targetLength, targetDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   */
  public static String[] move(
      String source, String target[], int sourceLength, int targetLength, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = move(source, target[i], sourceLength, targetLength, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] move(
      String source[],
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, targetLength, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] move(
      String source[],
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @return
   */
  public static Boolean[] move(String source[], Boolean target[], int sourceLength) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param separator
   * @return
   */
  public static LocalDate[] move(
      String source[], LocalDate target[], int sourceLength, String dateFormat, String separator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, dateFormat, separator);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param includeSeparator
   * @return
   */
  public static LocalDateTime[] move(
      String source[],
      LocalDateTime target[],
      int sourceLength,
      int targetLength,
      boolean includeSeparator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, targetLength, includeSeparator);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param separator
   * @return
   */
  public static LocalTime[] move(
      String source[], LocalTime target[], int sourceLength, String timeFormat, String separator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, timeFormat, separator);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] move(
      String source[],
      Long target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   */
  public static String[] move(
      String source[], String target[], int sourceLength, int targetLength, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = move(source[i], target[i], sourceLength, targetLength, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceDecimal
   * @param targetDecimal
   * @param targetStartIndex 1 base array index
   * @param pad
   * @return
   */
  public static BigDecimal[] movea(
      BigDecimal source,
      BigDecimal target[],
      int length,
      int sourceDecimal,
      int targetDecimal,
      int targetStartIndex,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    target[targetStartIndex - 1] =
        move(
            source,
            target[targetStartIndex - 1],
            length,
            length,
            sourceDecimal,
            targetDecimal,
            false);

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = targetStartIndex; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, sourceDecimal, targetDecimal, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceDecimal
   * @param targetStartIndex 1 base array index
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movea(
      BigDecimal source,
      BigInteger target[],
      int length,
      int sourceDecimal,
      int targetStartIndex,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    target[targetStartIndex - 1] =
        move(source, target[targetStartIndex - 1], length, length, sourceDecimal, unsigned, false);

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = targetStartIndex; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, sourceDecimal, unsigned, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceDecimal
   * @param targetStartIndex 1 base array index
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] movea(
      BigDecimal source,
      Long target[],
      int length,
      int sourceDecimal,
      int targetStartIndex,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    target[targetStartIndex - 1] =
        move(source, target[targetStartIndex - 1], length, length, sourceDecimal, unsigned, false);

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = targetStartIndex; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, sourceDecimal, unsigned, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceDecimal
   * @param targetDecimal
   * @param sourceStartIndex 1 base array index
   * @return
   */
  public static BigDecimal movea(
      BigDecimal source[],
      BigDecimal target,
      int length,
      int sourceDecimal,
      int targetDecimal,
      int sourceStartIndex) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    return move(
        source[sourceStartIndex - 1], target, length, length, sourceDecimal, targetDecimal, false);
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceDecimal
   * @param targetDecimal
   * @param sourceStartIndex 1 base array index
   * @param targetStartIndex 1 base array index
   * @param pad
   * @return
   */
  public static BigDecimal[] movea(
      BigDecimal source[],
      BigDecimal target[],
      int length,
      int sourceDecimal,
      int targetDecimal,
      int sourceStartIndex,
      int targetStartIndex,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    int count =
        Math.min((source.length - sourceStartIndex) + 1, (target.length - targetStartIndex) + 1);

    for (int i = 0; i < count; i++) {
      target[(targetStartIndex - 1) + i] =
          move(
              source[(sourceStartIndex - 1) + i],
              target[(targetStartIndex - 1) + i],
              length,
              length,
              sourceDecimal,
              targetDecimal,
              false);
    }

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = (targetStartIndex - 1) + count; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, sourceDecimal, targetDecimal, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceDecimal
   * @param sourceStartIndex 1 base array index
   * @param unsigned
   * @return
   */
  public static BigInteger movea(
      BigDecimal source[],
      BigInteger target,
      int length,
      int sourceDecimal,
      int sourceStartIndex,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    return move(
        source[sourceStartIndex - 1], target, length, length, sourceDecimal, unsigned, false);
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceDecimal
   * @param sourceStartIndex 1 base array index
   * @param targetStartIndex 1 base array index
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movea(
      BigDecimal source[],
      BigInteger target[],
      int length,
      int sourceDecimal,
      int sourceStartIndex,
      int targetStartIndex,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    int count =
        Math.min((source.length - sourceStartIndex) + 1, (target.length - targetStartIndex) + 1);

    for (int i = 0; i < count; i++) {
      target[(targetStartIndex - 1) + i] =
          move(
              source[(sourceStartIndex - 1) + i],
              target[(targetStartIndex - 1) + i],
              length,
              length,
              sourceDecimal,
              unsigned,
              false);
    }

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = (targetStartIndex - 1) + count; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, sourceDecimal, unsigned, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceDecimal
   * @param sourceStartIndex 1 base array index
   * @param unsigned
   * @return
   */
  public static Long movea(
      BigDecimal source[],
      Long target,
      int length,
      int sourceDecimal,
      int sourceStartIndex,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    return move(
        source[sourceStartIndex - 1], target, length, length, sourceDecimal, unsigned, false);
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceDecimal
   * @param sourceStartIndex 1 base array index
   * @param targetStartIndex 1 base array index
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] movea(
      BigDecimal source[],
      Long target[],
      int length,
      int sourceDecimal,
      int sourceStartIndex,
      int targetStartIndex,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    int count =
        Math.min((source.length - sourceStartIndex) + 1, (target.length - targetStartIndex) + 1);

    for (int i = 0; i < count; i++) {
      target[(targetStartIndex - 1) + i] =
          move(
              source[(sourceStartIndex - 1) + i],
              target[(targetStartIndex - 1) + i],
              length,
              length,
              sourceDecimal,
              unsigned,
              false);
    }

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = (targetStartIndex - 1) + count; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, sourceDecimal, unsigned, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param targetDecimal
   * @param sourceStartIndex 1 base array index
   * @param unsigned
   * @return
   */
  public static BigDecimal movea(
      BigInteger source[],
      BigDecimal target,
      int length,
      int targetDecimal,
      int sourceStartIndex,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    return move(
        source[sourceStartIndex - 1], target, length, length, targetDecimal, unsigned, false);
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param targetDecimal
   * @param targetStartIndex 1 base array index
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal[] movea(
      BigInteger source,
      BigDecimal target[],
      int length,
      int targetDecimal,
      int targetStartIndex,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    target[targetStartIndex - 1] =
        move(source, target[targetStartIndex - 1], length, length, targetDecimal, unsigned, false);

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = targetStartIndex; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, targetDecimal, targetDecimal, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param targetDecimal
   * @param sourceStartIndex 1 base array index
   * @param targetStartIndex 1 base array index
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal[] movea(
      BigInteger source[],
      BigDecimal target[],
      int length,
      int targetDecimal,
      int sourceStartIndex,
      int targetStartIndex,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    int count =
        Math.min((source.length - sourceStartIndex) + 1, (target.length - targetStartIndex) + 1);

    for (int i = 0; i < count; i++) {
      target[(targetStartIndex - 1) + i] =
          move(
              source[(sourceStartIndex - 1) + i],
              target[(targetStartIndex - 1) + i],
              length,
              length,
              targetDecimal,
              unsigned,
              false);
    }

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = (targetStartIndex - 1) + count; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, targetDecimal, targetDecimal, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceStartIndex 1 base array index
   * @param sourceUnsigned
   * @param targetUnsigned
   * @return
   */
  public static BigInteger movea(
      BigInteger source[],
      BigInteger target,
      int length,
      int sourceStartIndex,
      boolean sourceUnsigned,
      boolean targetUnsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    return move(
        source[sourceStartIndex - 1],
        target,
        length,
        length,
        sourceUnsigned,
        targetUnsigned,
        false);
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param targetStartIndex 1 base array index
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movea(
      BigInteger source,
      BigInteger target[],
      int length,
      int targetStartIndex,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    target[targetStartIndex - 1] =
        move(
            source,
            target[targetStartIndex - 1],
            length,
            length,
            sourceUnsigned,
            targetUnsigned,
            false);

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = targetStartIndex; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, 0, targetUnsigned, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceStartIndex 1 base array index
   * @param targetStartIndex 1 base array index
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movea(
      BigInteger source[],
      BigInteger target[],
      int length,
      int sourceStartIndex,
      int targetStartIndex,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    int count =
        Math.min((source.length - sourceStartIndex) + 1, (target.length - targetStartIndex) + 1);

    for (int i = 0; i < count; i++) {
      target[(targetStartIndex - 1) + i] =
          move(
              source[(sourceStartIndex - 1) + i],
              target[(targetStartIndex - 1) + i],
              length,
              length,
              sourceUnsigned,
              targetUnsigned,
              false);
    }

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = (targetStartIndex - 1) + count; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, 0, targetUnsigned, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceStartIndex 1 base array index
   * @param sourceUnsigned
   * @param targetUnsigned
   * @return
   */
  public static Long movea(
      BigInteger source[],
      Long target,
      int length,
      int sourceStartIndex,
      boolean sourceUnsigned,
      boolean targetUnsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    return move(
        source[sourceStartIndex - 1],
        target,
        length,
        length,
        sourceUnsigned,
        targetUnsigned,
        false);
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param targetStartIndex 1 base array index
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long[] movea(
      BigInteger source,
      Long target[],
      int length,
      int targetStartIndex,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    target[targetStartIndex - 1] =
        move(
            source,
            target[targetStartIndex - 1],
            length,
            length,
            sourceUnsigned,
            targetUnsigned,
            false);

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = targetStartIndex; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, 0, targetUnsigned, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceStartIndex 1 base array index
   * @param targetStartIndex 1 base array index
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long[] movea(
      BigInteger source[],
      Long target[],
      int length,
      int sourceStartIndex,
      int targetStartIndex,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    int count =
        Math.min((source.length - sourceStartIndex) + 1, (target.length - targetStartIndex) + 1);

    for (int i = 0; i < count; i++) {
      target[(targetStartIndex - 1) + i] =
          move(
              source[(sourceStartIndex - 1) + i],
              target[(targetStartIndex - 1) + i],
              length,
              length,
              sourceUnsigned,
              targetUnsigned,
              false);
    }

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = (targetStartIndex - 1) + count; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, 0, targetUnsigned, false);
      }
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param targetLength
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static String movea(boolean source[], String target, int targetLength, boolean pad) {
    return String.valueOf(source);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param targetLength
   * @param sourceStartIndex 1 base array index
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static String movea(
      boolean source[], String target, int targetLength, int sourceStartIndex, boolean pad) {
    int size = source.length;
    Boolean src[] = new Boolean[size];

    for (int i = 0; i < size; i++) {
      src[i] = source[i];
    }

    return movea(src, target, targetLength, sourceStartIndex, pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceStartIndex 1 base array index
   * @return
   */
  public static Boolean movea(Boolean source[], Boolean target, int sourceStartIndex) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    return Boolean.valueOf(source[sourceStartIndex - 1].booleanValue());
  }

  /**
   * @param source
   * @param target
   * @param targetStartIndex 1 base array index
   * @param pad
   * @return
   */
  public static Boolean[] movea(
      Boolean source, Boolean target[], int targetStartIndex, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    target[targetStartIndex - 1] = Boolean.valueOf(source.booleanValue());

    if (pad) {
      for (int i = targetStartIndex; i < target.length; i++) {
        target[i] = false;
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceStartIndex 1 base array index
   * @param targetStartIndex 1 base array index
   * @param pad
   * @return
   */
  public static Boolean[] movea(
      Boolean source[], Boolean target[], int sourceStartIndex, int targetStartIndex, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    int count =
        Math.min((source.length - sourceStartIndex) + 1, (target.length - targetStartIndex) + 1);

    for (int i = 0; i < count; i++) {
      target[(targetStartIndex - 1) + i] =
          Boolean.valueOf(source[(sourceStartIndex - 1) + i].booleanValue());
    }

    if (pad) {
      for (int i = (targetStartIndex - 1) + count; i < target.length; i++) {
        target[i] = false;
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetStartIndex 1 base array index
   * @param pad
   * @return
   */
  public static String[] movea(
      Boolean source, String target[], int targetLength, int targetStartIndex, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > (target.length / targetLength))) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    return movea(source ? "1" : "0", target, 1, targetLength, targetStartIndex, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param sourceStartIndex 1 base array index
   * @param pad
   * @return
   */
  public static String movea(
      Boolean source[], String target, int targetLength, int sourceStartIndex, boolean pad) {
    int i = 0;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    String result[] = new String[source.length];

    for (i = 0; i < source.length; i++) {
      result[i] = source[i] ? "1" : "0";
    }

    return movea(result, target, 1, targetLength, sourceStartIndex, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param sourceStartIndex 1 base array index
   * @param targetStartIndex 1 base array index
   * @param pad
   * @return
   */
  public static String[] movea(
      Boolean source[],
      String target[],
      int targetLength,
      int sourceStartIndex,
      int targetStartIndex,
      boolean pad) {
    int i = 0;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    String result[] = new String[source.length];

    for (i = 0; i < source.length; i++) {
      result[i] = source[i] ? "1" : "0";
    }

    return movea(result, target, 1, targetLength, sourceStartIndex, targetStartIndex, pad);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param length
   * @param sourceDecimal
   * @param targetDecimal
   * @param sourceStartIndex 1 base array index
   * @return
   * @author Amit Arya
   */
  public static int movea(
      int source[],
      int target,
      int length,
      int sourceDecimal,
      int targetDecimal,
      int sourceStartIndex) {
    return source[sourceStartIndex];
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceDecimal
   * @param targetDecimal
   * @param sourceStartIndex 1 base array index
   * @param targetStartIndex 1 base array index
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int[] movea(
      int source[],
      int target[],
      int length,
      int sourceDecimal,
      int targetDecimal,
      int sourceStartIndex,
      int targetStartIndex,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    int count =
        Math.min((source.length - sourceStartIndex) + 1, (target.length - targetStartIndex) + 1);

    for (int i = 0; i < count; i++) {
      target[(targetStartIndex - 1) + i] =
          move(
              source[(sourceStartIndex - 1) + i],
              target[(targetStartIndex - 1) + i],
              length,
              length,
              sourceDecimal,
              targetDecimal,
              false);
    }

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = (targetStartIndex - 1) + count; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, sourceDecimal, targetDecimal, false);
      }
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param length
   * @param sourceDecimal
   * @param targetDecimal
   * @param sourceStartIndex 1 base array index
   * @return
   * @author Amit Arya
   */
  public static long movea(
      int source[],
      long target,
      int length,
      int sourceDecimal,
      int targetDecimal,
      int sourceStartIndex) {
    return source[sourceStartIndex];
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param targetDecimal
   * @param sourceStartIndex 1 base array index
   * @param unsigned
   * @return
   */
  public static BigDecimal movea(
      Long source[],
      BigDecimal target,
      int length,
      int targetDecimal,
      int sourceStartIndex,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    return move(
        source[sourceStartIndex - 1], target, length, length, targetDecimal, unsigned, false);
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param targetDecimal
   * @param targetStartIndex 1 base array index
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal[] movea(
      Long source,
      BigDecimal target[],
      int length,
      int targetDecimal,
      int targetStartIndex,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    target[targetStartIndex - 1] =
        move(source, target[targetStartIndex - 1], length, length, targetDecimal, unsigned, false);

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = targetStartIndex; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, targetDecimal, targetDecimal, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param targetDecimal
   * @param sourceStartIndex 1 base array index
   * @param targetStartIndex 1 base array index
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal[] movea(
      Long source[],
      BigDecimal target[],
      int length,
      int targetDecimal,
      int sourceStartIndex,
      int targetStartIndex,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    int count =
        Math.min((source.length - sourceStartIndex) + 1, (target.length - targetStartIndex) + 1);

    for (int i = 0; i < count; i++) {
      target[(targetStartIndex - 1) + i] =
          move(
              source[(sourceStartIndex - 1) + i],
              target[(targetStartIndex - 1) + i],
              length,
              length,
              targetDecimal,
              unsigned,
              false);
    }

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = (targetStartIndex - 1) + count; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, targetDecimal, targetDecimal, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceStartIndex 1 base array index
   * @param sourceUnsigned
   * @param targetUnsigned
   * @return
   */
  public static BigInteger movea(
      Long source[],
      BigInteger target,
      int length,
      int sourceStartIndex,
      boolean sourceUnsigned,
      boolean targetUnsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    return move(
        source[sourceStartIndex - 1],
        target,
        length,
        length,
        sourceUnsigned,
        targetUnsigned,
        false);
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param targetStartIndex 1 base array index
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movea(
      Long source,
      BigInteger target[],
      int length,
      int targetStartIndex,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    target[targetStartIndex - 1] =
        move(
            source,
            target[targetStartIndex - 1],
            length,
            length,
            sourceUnsigned,
            targetUnsigned,
            false);

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = targetStartIndex; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, 0, targetUnsigned, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceStartIndex 1 base array index
   * @param targetStartIndex 1 base array index
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movea(
      Long source[],
      BigInteger target[],
      int length,
      int sourceStartIndex,
      int targetStartIndex,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    int count =
        Math.min((source.length - sourceStartIndex) + 1, (target.length - targetStartIndex) + 1);

    for (int i = 0; i < count; i++) {
      target[(targetStartIndex - 1) + i] =
          move(
              source[(sourceStartIndex - 1) + i],
              target[(targetStartIndex - 1) + i],
              length,
              length,
              sourceUnsigned,
              targetUnsigned,
              false);
    }

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = (targetStartIndex - 1) + count; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, 0, targetUnsigned, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceStartIndex 1 base array index
   * @param sourceUnsigned
   * @param targetUnsigned
   * @return
   */
  public static Long movea(
      Long source[],
      Long target,
      int length,
      int sourceStartIndex,
      boolean sourceUnsigned,
      boolean targetUnsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    return move(
        source[sourceStartIndex - 1],
        target,
        length,
        length,
        sourceUnsigned,
        targetUnsigned,
        false);
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param targetStartIndex 1 base array index
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long[] movea(
      Long source,
      Long target[],
      int length,
      int targetStartIndex,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    target[targetStartIndex - 1] =
        move(
            source,
            target[targetStartIndex - 1],
            length,
            length,
            sourceUnsigned,
            targetUnsigned,
            false);

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = targetStartIndex; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, 0, targetUnsigned, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param length
   * @param sourceStartIndex 1 base array index
   * @param targetStartIndex 1 base array index
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long[] movea(
      Long source[],
      Long target[],
      int length,
      int sourceStartIndex,
      int targetStartIndex,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    int count =
        Math.min((source.length - sourceStartIndex) + 1, (target.length - targetStartIndex) + 1);

    for (int i = 0; i < count; i++) {
      target[(targetStartIndex - 1) + i] =
          move(
              source[(sourceStartIndex - 1) + i],
              target[(targetStartIndex - 1) + i],
              length,
              length,
              sourceUnsigned,
              targetUnsigned,
              false);
    }

    if (pad) {
      BigDecimal padValue = new BigDecimal(0);

      for (int i = (targetStartIndex - 1) + count; i < target.length; i++) {
        target[i] = move(padValue, target[i], length, length, 0, targetUnsigned, false);
      }
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceStartIndex 1 base array index
   * @return
   */
  public static Boolean movea(
      String source[], Boolean target, int sourceLength, int sourceStartIndex) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    Boolean tmp[] = new Boolean[] {target};
    tmp = movea(source, tmp, sourceLength, sourceStartIndex, 1, false);

    return tmp[0];
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetStartIndex 1 base array index
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static boolean[] movea(
      String source, boolean target[], int sourceLength, int targetStartIndex, boolean pad) {
    return movea(source, target, sourceLength, targetStartIndex, pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetStartIndex 1 base array index
   * @param pad
   * @return
   */
  public static Boolean[] movea(
      String source, Boolean target[], int sourceLength, int targetStartIndex, boolean pad) {
    int i = 0;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    String result[] = new String[target.length];

    for (i = 0; i < target.length; i++) {
      result[i] = target[i] ? "1" : "0";
    }

    result = movea(source, result, sourceLength, 1, targetStartIndex, pad);

    for (i = 0; i < target.length; i++) {
      target[i] = result[i].charAt(0) == '1';
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceStartIndex 1 base array index
   * @param targetStartIndex 1 base array index
   * @param pad
   * @return
   */
  public static Boolean[] movea(
      String source[],
      Boolean target[],
      int sourceLength,
      int sourceStartIndex,
      int targetStartIndex,
      boolean pad) {
    int i = 0;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    String result[] = new String[target.length];

    for (i = 0; i < target.length; i++) {
      result[i] = target[i] ? "1" : "0";
    }

    result = movea(source, result, sourceLength, 1, sourceStartIndex, targetStartIndex, pad);

    for (i = 0; i < result.length; i++) {
      target[i] = result[i].charAt(0) == '1';
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static String movea(
      String source[], String target, int sourceLength, int targetLength, boolean pad) {
    return String.valueOf(source);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetStartIndex 1 base array index
   * @param pad
   * @return
   */
  public static String[] movea(
      String source,
      String target[],
      int sourceLength,
      int targetLength,
      int targetStartIndex,
      boolean pad) {
    int i = 0;
    String result[] = null;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    if (source.length() > sourceLength) {
      throw new IllegalArgumentException(
          "Invalid source length <" + source.length() + "> to expected <" + sourceLength + ">");
    }

    char sourceChars[] = padRight(source, sourceLength, ' ').toCharArray();
    char targetChars[] = convertStringArrayToCharArray(target, targetLength);
    int top = Math.min(sourceLength, targetLength * ((target.length - targetStartIndex) + 1));
    System.arraycopy(sourceChars, 0, targetChars, targetLength * (targetStartIndex - 1), top);

    if (pad) {
      for (i = 0; i < (targetChars.length - ((targetLength * (targetStartIndex - 1)) + top)); i++) {
        targetChars[targetChars.length - i - 1] = ' ';
      }
    }

    result = convertCharArrayToStringArray(targetChars, targetLength);

    for (i = 0; i < result.length; i++) {
      target[i] = result[i];
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceStartIndex 1 base array index
   * @param pad
   * @return
   */
  public static String movea(
      String source[],
      String target,
      int sourceLength,
      int targetLength,
      int sourceStartIndex,
      boolean pad) {
    int i = 0;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if (target.length() > targetLength) {
      throw new IllegalArgumentException(
          "Invalid target length <" + target.length() + "> to expected <" + targetLength + ">");
    }

    char sourceChars[] = convertStringArrayToCharArray(source, sourceLength);
    char targetChars[] = padRight(target, targetLength, ' ').toCharArray();
    int top = Math.min(sourceLength * ((source.length - sourceStartIndex) + 1), targetLength);
    System.arraycopy(sourceChars, sourceLength * (sourceStartIndex - 1), targetChars, 0, top);

    if (pad) {
      for (i = 0; i < (targetChars.length - sourceChars.length); i++) {
        targetChars[targetChars.length - i - 1] = ' ';
      }
    }

    return String.valueOf(targetChars);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceStartIndex 1 base array index
   * @param targetStartIndex 1 base array index
   * @param pad
   * @return
   */
  public static String[] movea(
      String source[],
      String target[],
      int sourceLength,
      int targetLength,
      int sourceStartIndex,
      int targetStartIndex,
      boolean pad) {
    int i = 0;
    String result[] = null;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEA operation cannot be null!");
    }

    if ((sourceStartIndex < 1) || (sourceStartIndex > source.length)) {
      throw new IllegalArgumentException("Invalid source start index <" + sourceStartIndex + ">");
    }

    if ((targetStartIndex < 1) || (targetStartIndex > target.length)) {
      throw new IllegalArgumentException("Invalid target start index <" + targetStartIndex + ">");
    }

    char sourceChars[] = convertStringArrayToCharArray(source, sourceLength);
    char targetChars[] = convertStringArrayToCharArray(target, targetLength);
    int top =
        Math.min(
            sourceLength * ((source.length - sourceStartIndex) + 1),
            targetLength * ((target.length - targetStartIndex) + 1));
    System.arraycopy(
        sourceChars,
        sourceLength * (sourceStartIndex - 1),
        targetChars,
        targetLength * (targetStartIndex - 1),
        top);

    if (pad) {
      for (i = 0; i < (targetChars.length - ((targetLength * (targetStartIndex - 1)) + top)); i++) {
        targetChars[targetChars.length - i - 1] = ' ';
      }
    }

    result = convertCharArrayToStringArray(targetChars, targetLength);

    for (i = 0; i < result.length; i++) {
      target[i] = result[i];
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal movel(
      BigDecimal source,
      BigDecimal target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVEL operation must be positive!");
    }

    if ((sourceDecimal < 0) || (targetDecimal < 0)) {
      throw new IllegalArgumentException(
          "Operands' decimal positions of MOVEL operation cannot be negative!");
    }

    byte sourceZoned[] = convertBigDecimalToZoned(source, sourceLength, sourceDecimal);
    byte targetZoned[] = null;

    if (sourceLength >= targetLength) {
      targetZoned = new byte[targetLength];
    } else {
      targetZoned = convertBigDecimalToZoned(target, targetLength, targetDecimal);
    }

    int top = Math.min(sourceLength, targetLength);
    System.arraycopy(sourceZoned, 0, targetZoned, 0, top);

    /*
     * When data is moved to a numeric field, the sign (+ or -) of the result
     * field is retained except when Factor2 is along as or longer than the
     * result field.
     * https://www.ibm.com/support/knowledgecenter/en/ssw_ibm_i_73/rzasd/zzmovel.htm
     */
    if (sourceLength >= targetLength) {
      targetZoned[targetLength - 1] =
          (byte) ((targetZoned[targetLength - 1] & 0x0F) | (sourceZoned[sourceLength - 1] & 0xF0));
    } else if (pad) {
      for (int i = 0; i < (targetLength - sourceLength); i++) {
        targetZoned[targetLength - i - 1] = (byte) 0xF0;
      }
    }

    return convertZonedToBigDecimal(targetZoned, targetDecimal);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      BigDecimal source,
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] =
          movel(source, target[i], sourceLength, targetLength, sourceDecimal, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      BigDecimal source[],
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          movel(
              source[i], target[i], sourceLength, targetLength, sourceDecimal, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger movel(
      BigDecimal source,
      BigInteger target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean unsigned,
      boolean pad) {
    BigInteger result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    result =
        movel(source, new BigDecimal(target), sourceLength, targetLength, sourceDecimal, 0, pad)
            .toBigInteger();

    return unsigned ? result.abs() : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      BigDecimal source,
      BigInteger target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] =
          movel(source, target[i], sourceLength, targetLength, sourceDecimal, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      BigDecimal source[],
      BigInteger target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          movel(source[i], target[i], sourceLength, targetLength, sourceDecimal, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @return
   */
  public static Boolean movel(
      BigDecimal source, Boolean target, int sourceLength, int sourceDecimal) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    String resultStr = movel(source, target ? "1" : "0", sourceLength, 1, sourceDecimal, false);

    if (!"1".equals(resultStr) && !"0".equals(resultStr)) {
      throw new IllegalArgumentException(
          "Value <"
              + resultStr
              + "> in source <"
              + source.toPlainString()
              + "> is invalid for boolean!");
    }

    return "1".equals(resultStr);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @return
   */
  public static Boolean[] movel(
      BigDecimal source, Boolean target[], int sourceLength, int sourceDecimal) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, sourceDecimal);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @return
   */
  public static Boolean[] movel(
      BigDecimal source[], Boolean target[], int sourceLength, int sourceDecimal) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, sourceDecimal);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int movel(
      BigDecimal source,
      int target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    return movel(
            source,
            BigDecimal.valueOf(target),
            sourceLength,
            targetLength,
            sourceDecimal,
            targetDecimal,
            pad)
        .intValue();
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param dateFormat
   * @return
   */
  public static LocalDate movel(
      BigDecimal source, LocalDate target, int sourceLength, int sourceDecimal, String dateFormat) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if (sourceLength <= 0) {
      throw new IllegalArgumentException("Operands' lengths of MOVEL operation must be positive!");
    }

    if (sourceDecimal < 0) {
      throw new IllegalArgumentException(
          "Operands' decimal positions of MOVEL operation cannot be negative!");
    }

    if (!isValidDateFormatName(dateFormat)) {
      throw new IllegalArgumentException("Invalid date format: <" + dateFormat + ">");
    }

    int length = getDateLength(dateFormat, "");

    if (sourceLength < length) {
      throw new IllegalArgumentException(
          "Source numeric length <" + sourceLength + "> is too small to contain a complete date.");
    }

    String str = padRight("", length, '0');
    str = movel(source, str, sourceLength, length, sourceDecimal, false);

    return convertStringToDate(str, dateFormat, "");
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param dateFormat
   * @return
   */
  public static LocalDate[] movel(
      BigDecimal source,
      LocalDate target[],
      int sourceLength,
      int sourceDecimal,
      String dateFormat) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, sourceDecimal, dateFormat);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param dateFormat
   * @return
   */
  public static LocalDate[] movel(
      BigDecimal source[],
      LocalDate target[],
      int sourceLength,
      int sourceDecimal,
      String dateFormat) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, sourceDecimal, dateFormat);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param targetLength
   * @return
   */
  public static LocalDateTime movel(
      BigDecimal source,
      LocalDateTime target,
      int sourceLength,
      int targetLength,
      int sourceDecimal) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVEL operation must be positive!");
    }

    if (sourceDecimal < 0) {
      throw new IllegalArgumentException(
          "Operand's decimal position of MOVEL operation cannot be negative!");
    }

    if (targetLength < 19) {
      throw new IllegalArgumentException(
          "Target length <" + targetLength + "> is too small to contain a complete timestamp.");
    }

    if (targetLength == 20) {
      throw new IllegalArgumentException("Invalid target length: <" + targetLength + ">.");
    }

    if (targetLength > 32) {
      throw new IllegalArgumentException(
          "Target length <" + targetLength + "> is over precision of timestamp.");
    }

    targetLength -= ((targetLength == 19) ? 5 : 6);

    if (sourceLength < targetLength) {
      throw new IllegalArgumentException(
          "Source numeric length <"
              + sourceLength
              + "> is too small to contain a complete timestamp.");
    }

    String str = padRight("", targetLength, '0');
    str = movel(source, str, sourceLength, targetLength, sourceDecimal, false);

    return convertStringToTimestamp(str, false);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param targetLength
   * @return
   */
  public static LocalDateTime[] movel(
      BigDecimal source,
      LocalDateTime target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, targetLength, sourceDecimal);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param timeFormat
   * @return
   */
  public static LocalTime movel(
      BigDecimal source, LocalTime target, int sourceLength, int sourceDecimal, String timeFormat) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if (sourceLength <= 0) {
      throw new IllegalArgumentException("Operands' lengths of MOVEL operation must be positive!");
    }

    if (sourceDecimal < 0) {
      throw new IllegalArgumentException(
          "Operands' decimal positions of MOVEL operation cannot be negative!");
    }

    timeFormat = (timeFormat == null) ? "" : timeFormat.trim().toUpperCase();

    if (!isValidTimeFormatName(timeFormat) || "*USA".equalsIgnoreCase(timeFormat)) {
      throw new IllegalArgumentException("Invalid time format: <" + timeFormat + ">");
    }

    int length = getTimeLength(timeFormat, "");

    if (sourceLength < length) {
      throw new IllegalArgumentException(
          "Source numeric length <" + sourceLength + "> is too small to contain a complete time.");
    }

    String str = padRight("", length, '0');
    str = movel(source, str, sourceLength, length, sourceDecimal, false);

    return convertStringToTime(str, timeFormat, "");
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param timeFormat
   * @return
   */
  public static LocalTime[] movel(
      BigDecimal source,
      LocalTime target[],
      int sourceLength,
      int sourceDecimal,
      String timeFormat) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, sourceDecimal, timeFormat);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param timeFormat
   * @return
   */
  public static LocalTime[] movel(
      BigDecimal source[],
      LocalTime target[],
      int sourceLength,
      int sourceDecimal,
      String timeFormat) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, sourceDecimal, timeFormat);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long movel(
      BigDecimal source,
      Long target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean unsigned,
      boolean pad) {
    Long result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    result =
        movel(source, BigDecimal.valueOf(target), sourceLength, targetLength, sourceDecimal, 0, pad)
            .longValue();

    return unsigned ? Math.abs(result) : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] movel(
      BigDecimal source,
      Long target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] =
          movel(source, target[i], sourceLength, targetLength, sourceDecimal, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] movel(
      BigDecimal source[],
      Long target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          movel(source[i], target[i], sourceLength, targetLength, sourceDecimal, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   */
  public static String movel(
      BigDecimal source,
      String target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVEL operation must be positive!");
    }

    if (sourceDecimal < 0) {
      throw new IllegalArgumentException(
          "Operand's decimal position of MOVEL operation cannot be negative!");
    }

    byte sourceZoned[] = convertBigDecimalToZoned(source, sourceLength, sourceDecimal);
    byte targetBytes[] = null;

    if (sourceLength >= targetLength) {
      targetBytes = new byte[targetLength];
    } else {
      if (target.length() > targetLength) {
        throw new IllegalArgumentException(
            "Invalid target length <" + target.length() + "> to expected <" + targetLength + ">");
      }

      targetBytes = convertUTF16ToIBM037(padRight(target, targetLength, ' ').toCharArray());
    }

    int top = Math.min(sourceZoned.length, targetBytes.length);
    System.arraycopy(sourceZoned, 0, targetBytes, 0, top);

    /*
     * Pad (in documentation "P") specified in the operation extender position
     * causes the result field to be padded on the right after the move occurs.
     * The result field is padded from the right.
     */
    if (pad) {
      for (int i = 0; i < (targetBytes.length - sourceZoned.length); i++) {
        targetBytes[targetBytes.length - i - 1] = (byte) 0x40;
      }
    }

    return String.valueOf(convertIBM037ToUTF16(targetBytes));
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   */
  public static String[] movel(
      BigDecimal source,
      String target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, targetLength, sourceDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   */
  public static String[] movel(
      BigDecimal source[],
      String target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, targetLength, sourceDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @return
   */
  public static LocalDateTime[] movel(
      BigDecimal source[],
      LocalDateTime target[],
      int sourceLength,
      int targetLength,
      int sourceDecimal) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, targetLength, sourceDecimal);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal movel(
      BigInteger source,
      BigDecimal target,
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        new BigDecimal(unsigned ? source.abs() : source),
        target,
        sourceLength,
        targetLength,
        0,
        targetDecimal,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      BigInteger source,
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        new BigDecimal(unsigned ? source.abs() : source),
        target,
        sourceLength,
        targetLength,
        0,
        targetDecimal,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      BigInteger source[],
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          movel(source[i], target[i], sourceLength, targetLength, targetDecimal, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger movel(
      BigInteger source,
      BigInteger target,
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    BigInteger result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    result =
        movel(
                new BigDecimal(sourceUnsigned ? source.abs() : source),
                new BigDecimal(target),
                sourceLength,
                targetLength,
                0,
                0,
                pad)
            .toBigInteger();

    return targetUnsigned ? result.abs() : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      BigInteger source,
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        new BigDecimal(sourceUnsigned ? source.abs() : source),
        target,
        sourceLength,
        targetLength,
        0,
        targetUnsigned,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      BigInteger source[],
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          movel(
              source[i],
              target[i],
              sourceLength,
              targetLength,
              sourceUnsigned,
              targetUnsigned,
              pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param unsigned
   * @return
   */
  public static Boolean movel(
      BigInteger source, Boolean target, int sourceLength, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, 0);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param unsigned
   * @return
   */
  public static Boolean[] movel(
      BigInteger source, Boolean target[], int sourceLength, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, 0);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param unsigned
   * @return
   */
  public static Boolean[] movel(
      BigInteger source[], Boolean target[], int sourceLength, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, unsigned);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param unsigned
   * @return
   */
  public static LocalDate movel(
      BigInteger source, LocalDate target, int sourceLength, String dateFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, 0, dateFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param unsigned
   * @return
   */
  public static LocalDate[] movel(
      BigInteger source,
      LocalDate target[],
      int sourceLength,
      String dateFormat,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, 0, dateFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param unsigned
   * @return
   */
  public static LocalDate[] movel(
      BigInteger source[],
      LocalDate target[],
      int sourceLength,
      String dateFormat,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, dateFormat, unsigned);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @return
   */
  public static LocalDateTime movel(
      BigInteger source,
      LocalDateTime target,
      int sourceLength,
      int targetLength,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, targetLength, 0);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @return
   */
  public static LocalDateTime[] movel(
      BigInteger source,
      LocalDateTime target[],
      int sourceLength,
      int targetLength,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, targetLength, 0);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @return
   */
  public static LocalDateTime[] movel(
      BigInteger source[],
      LocalDateTime target[],
      int sourceLength,
      int targetLength,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, targetLength, unsigned);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param unsigned
   * @return
   */
  public static LocalTime movel(
      BigInteger source, LocalTime target, int sourceLength, String timeFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, 0, timeFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param unsigned
   * @return
   */
  public static LocalTime[] movel(
      BigInteger source,
      LocalTime target[],
      int sourceLength,
      String timeFormat,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        new BigDecimal(unsigned ? source.abs() : source), target, sourceLength, 0, timeFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param unsigned
   * @return
   */
  public static LocalTime[] movel(
      BigInteger source[],
      LocalTime target[],
      int sourceLength,
      String timeFormat,
      boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, timeFormat, unsigned);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long movel(
      BigInteger source,
      Long target,
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    Long result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    result =
        movel(
                new BigDecimal(sourceUnsigned ? source.abs() : source),
                BigDecimal.valueOf(target),
                sourceLength,
                targetLength,
                0,
                0,
                pad)
            .longValue();

    return targetUnsigned ? Math.abs(result) : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long[] movel(
      BigInteger source,
      Long target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        new BigDecimal(sourceUnsigned ? source.abs() : source),
        target,
        sourceLength,
        targetLength,
        0,
        targetUnsigned,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long[] movel(
      BigInteger source[],
      Long target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          movel(
              source[i],
              target[i],
              sourceLength,
              targetLength,
              sourceUnsigned,
              targetUnsigned,
              pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static String movel(
      BigInteger source,
      String target,
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        new BigDecimal(unsigned ? source.abs() : source),
        target,
        sourceLength,
        targetLength,
        0,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static String[] movel(
      BigInteger source,
      String target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        new BigDecimal(unsigned ? source.abs() : source),
        target,
        sourceLength,
        targetLength,
        0,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static String[] movel(
      BigInteger source[],
      String target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal movel(
      Boolean source, BigDecimal target, int targetLength, int targetDecimal, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(source ? "1" : "0", target, 1, targetLength, targetDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      Boolean source, BigDecimal target[], int targetLength, int targetDecimal, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(source ? "1" : "0", target, 1, targetLength, targetDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      Boolean source[], BigDecimal target[], int targetLength, int targetDecimal, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], targetLength, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger movel(
      Boolean source, BigInteger target, int targetLength, boolean unsigned, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(source ? "1" : "0", target, 1, targetLength, unsigned, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      Boolean source, BigInteger target[], int targetLength, boolean unsigned, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(source ? "1" : "0", target, 1, targetLength, unsigned, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      Boolean source[], BigInteger target[], int targetLength, boolean unsigned, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static Boolean movel(Boolean source, Boolean target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return Boolean.valueOf(source.booleanValue());
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static Boolean[] movel(Boolean source, Boolean target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = Boolean.valueOf(source.booleanValue());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static Boolean[] movel(Boolean source[], Boolean target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = Boolean.valueOf(source[i].booleanValue());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param pad
   * @return
   */
  public static Long movel(
      Boolean source, Long target, int targetLength, boolean unsigned, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(source ? "1" : "0", target, 1, targetLength, unsigned, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] movel(
      Boolean source, Long target[], int targetLength, boolean unsigned, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(source ? "1" : "0", target, 1, targetLength, unsigned, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] movel(
      Boolean source[], Long target[], int targetLength, boolean unsigned, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param pad
   * @return
   */
  public static String movel(Boolean source, String target, int targetLength, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(source ? "1" : "0", target, 1, targetLength, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param pad
   * @return
   */
  public static String[] movel(Boolean source, String target[], int targetLength, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(source ? "1" : "0", target, 1, targetLength, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param pad
   * @return
   */
  public static String[] movel(Boolean source[], String target[], int targetLength, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], targetLength, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static BigDecimal movel(
      int source,
      BigDecimal target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    return movel(source, target, sourceLength, targetLength, sourceDecimal, targetDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @return
   * @author Amit Arya
   */
  public static int movel(int source, int target) {
    return source;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int movel(
      int source, int target, int sourceLength, int targetLength, int sourceDecimal, boolean pad) {
    return movel(source, target);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param unsigned
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int movel(
      int source,
      int target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean unsigned,
      boolean pad) {
    return movel(source, target);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int movel(
      int source,
      int target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    return movel(
            BigDecimal.valueOf(source),
            BigDecimal.valueOf(target),
            sourceLength,
            targetLength,
            sourceDecimal,
            targetDecimal,
            pad)
        .intValue();
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param sourceDecimal
   * @param dateFormat
   * @return
   * @author Amit Arya
   */
  public static LocalDate movel(
      int source, LocalDate target, int sourceLength, int sourceDecimal, String dateFormat) {
    return movel(BigDecimal.valueOf(source), target, sourceLength, sourceDecimal, dateFormat);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static String movel(
      int source,
      String target,
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    return movel(source, target, sourceLength, targetLength, unsigned, pad);
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static String movel(
      int source,
      String target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean pad) {
    return movel(
        BigDecimal.valueOf(source), target, sourceLength, targetLength, sourceDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param dateFormat
   * @param pad
   * @return
   */
  public static BigDecimal movel(
      LocalDate source,
      BigDecimal target,
      int targetLength,
      int targetDecimal,
      String dateFormat,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if (targetLength <= 0) {
      throw new IllegalArgumentException("Operand's length of MOVEL operation must be positive!");
    }

    if (targetDecimal < 0) {
      throw new IllegalArgumentException(
          "Operand's decimal position of MOVEL operation cannot be negative!");
    }

    if (!isValidDateFormatName(dateFormat)) {
      throw new IllegalArgumentException("Invalid date format: <" + dateFormat + ">");
    }

    if (targetLength < getDateLength(dateFormat, "")) {
      throw new IllegalArgumentException(
          "Target numeric length <" + targetLength + "> is too small to contain a complete date.");
    }

    String sourceStr = convertDateToString(source, dateFormat, "");

    return movel(sourceStr, target, sourceStr.length(), targetLength, targetDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param dateFormat
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      LocalDate source,
      BigDecimal target[],
      int targetLength,
      int targetDecimal,
      String dateFormat,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], targetLength, targetDecimal, dateFormat, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param dateFormat
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      LocalDate source[],
      BigDecimal target[],
      int targetLength,
      int targetDecimal,
      String dateFormat,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], targetLength, targetDecimal, dateFormat, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger movel(
      LocalDate source,
      BigInteger target,
      int targetLength,
      String dateFormat,
      boolean unsigned,
      boolean pad) {
    BigInteger result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    result = movel(source, new BigDecimal(target), targetLength, 0, dateFormat, pad).toBigInteger();

    return unsigned ? result.abs() : result;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      LocalDate source,
      BigInteger target[],
      int targetLength,
      String dateFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], targetLength, dateFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      LocalDate source[],
      BigInteger target[],
      int targetLength,
      String dateFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], targetLength, dateFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int movel(
      LocalDate source,
      int target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    return movel(
            DateTimeUtils.toInt(source),
            BigDecimal.valueOf(target),
            sourceLength,
            targetLength,
            sourceDecimal,
            targetDecimal,
            pad)
        .intValue();
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param dateFormat
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int movel(
      LocalDate source,
      int target,
      int targetLength,
      int targetDecimal,
      String dateFormat,
      boolean pad) {
    return movel(source, BigDecimal.valueOf(target), targetLength, targetDecimal, dateFormat, pad)
        .intValue();
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDate movel(LocalDate source, LocalDate target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return LocalDate.of(source.getYear(), source.getMonthValue(), source.getDayOfMonth());
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param dateFormat
   * @return
   * @author Amit Arya
   */
  public static LocalDate movel(
      LocalDate source, LocalDate target, int targetLength, int targetDecimal, String dateFormat) {
    return movel(source, target);
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDate[] movel(LocalDate source, LocalDate target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = LocalDate.of(source.getYear(), source.getMonthValue(), source.getDayOfMonth());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDate[] movel(LocalDate source[], LocalDate target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          LocalDate.of(source[i].getYear(), source[i].getMonthValue(), source[i].getDayOfMonth());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime movel(LocalDate source, LocalDateTime target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return LocalDateTime.of(
        source.getYear(),
        source.getMonthValue(),
        source.getDayOfMonth(),
        target.getHour(),
        target.getMinute(),
        target.getSecond(),
        target.getNano());
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime[] movel(LocalDate source, LocalDateTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] =
          LocalDateTime.of(
              source.getYear(),
              source.getMonthValue(),
              source.getDayOfMonth(),
              target[i].getHour(),
              target[i].getMinute(),
              target[i].getSecond(),
              target[i].getNano());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime[] movel(LocalDate source[], LocalDateTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          LocalDateTime.of(
              source[i].getYear(),
              source[i].getMonthValue(),
              source[i].getDayOfMonth(),
              target[i].getHour(),
              target[i].getMinute(),
              target[i].getSecond(),
              target[i].getNano());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long movel(
      LocalDate source,
      Long target,
      int targetLength,
      String dateFormat,
      boolean unsigned,
      boolean pad) {
    Long result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    result =
        movel(source, BigDecimal.valueOf(target), targetLength, 0, dateFormat, pad).longValue();

    return unsigned ? Math.abs(result) : result;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] movel(
      LocalDate source,
      Long target[],
      int targetLength,
      String dateFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], targetLength, dateFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] movel(
      LocalDate source[],
      Long target[],
      int targetLength,
      String dateFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], targetLength, dateFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param pad
   * @return
   */
  public static String movel(
      LocalDate source,
      String target,
      int targetLength,
      String dateFormat,
      String separator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if (targetLength <= 0) {
      throw new IllegalArgumentException("Operand's length of MOVEL operation must be positive!");
    }

    if (!isValidDateFormatName(dateFormat)) {
      throw new IllegalArgumentException("Invalid date format: <" + dateFormat + ">");
    }

    if (targetLength < getDateLength(dateFormat, separator)) {
      throw new IllegalArgumentException(
          "Target character length <"
              + targetLength
              + "> is too small to contain a complete date.");
    }

    String sourceStr = convertDateToString(source, dateFormat, separator);

    return movel(sourceStr, target, sourceStr.length(), targetLength, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param pad
   * @return
   */
  public static String[] movel(
      LocalDate source,
      String target[],
      int targetLength,
      String dateFormat,
      String separator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], targetLength, dateFormat, separator, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param dateFormat
   * @param separator
   * @param pad
   * @return
   */
  public static String[] movel(
      LocalDate source[],
      String target[],
      int targetLength,
      String dateFormat,
      String separator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], targetLength, dateFormat, separator, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal movel(
      LocalDateTime source,
      BigDecimal target,
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVEL operation must be positive!");
    }

    if (sourceLength < 19) {
      throw new IllegalArgumentException(
          "Source length <" + sourceLength + "> is too small to contain a complete timestamp.");
    }

    if (sourceLength == 20) {
      throw new IllegalArgumentException("Invalid source length: <" + sourceLength + ">.");
    }

    if (sourceLength > 32) {
      throw new IllegalArgumentException(
          "Source length <" + sourceLength + "> is over precision of timestamp.");
    }

    if (((sourceLength == 19) ? (sourceLength - 5) : (sourceLength - 6)) > targetLength) {
      throw new IllegalArgumentException(
          "Target numeric length <"
              + targetLength
              + "> is too small to contain a complete timestamp.");
    }

    String sourceStr = convertTimestampToString(source, sourceLength, false);

    return movel(sourceStr, target, sourceStr.length(), targetLength, targetDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      LocalDateTime source,
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, targetLength, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      LocalDateTime source[],
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, targetLength, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param pad
   * @return
   */
  public static BigInteger movel(
      LocalDateTime source,
      BigInteger target,
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    BigInteger result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    result =
        movel(source, new BigDecimal(target), sourceLength, targetLength, 0, pad).toBigInteger();

    return unsigned ? result.abs() : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      LocalDateTime source,
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      LocalDateTime source[],
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int movel(
      LocalDateTime source,
      int target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean pad) {
    return DateTimeUtils.toInt(source);
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDate movel(LocalDateTime source, LocalDate target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return LocalDate.of(source.getYear(), source.getMonthValue(), source.getDayOfMonth());
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDate[] movel(LocalDateTime source, LocalDate target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = LocalDate.of(source.getYear(), source.getMonthValue(), source.getDayOfMonth());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDate[] movel(LocalDateTime source[], LocalDate target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          LocalDate.of(source[i].getYear(), source[i].getMonthValue(), source[i].getDayOfMonth());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime movel(LocalDateTime source, LocalDateTime target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return LocalDateTime.of(
        source.getYear(),
        source.getMonthValue(),
        source.getDayOfMonth(),
        source.getHour(),
        source.getMinute(),
        source.getSecond(),
        source.getNano());
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime[] movel(LocalDateTime source, LocalDateTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] =
          LocalDateTime.of(
              source.getYear(),
              source.getMonthValue(),
              source.getDayOfMonth(),
              source.getHour(),
              source.getMinute(),
              source.getSecond(),
              source.getNano());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime[] movel(LocalDateTime source[], LocalDateTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          LocalDateTime.of(
              source[i].getYear(),
              source[i].getMonthValue(),
              source[i].getDayOfMonth(),
              source[i].getHour(),
              source[i].getMinute(),
              source[i].getSecond(),
              source[i].getNano());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalTime movel(LocalDateTime source, LocalTime target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return LocalTime.of(source.getHour(), source.getMinute(), source.getSecond(), 0);
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalTime[] movel(LocalDateTime source, LocalTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = LocalTime.of(source.getHour(), source.getMinute(), source.getSecond(), 0);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalTime[] movel(LocalDateTime source[], LocalTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          LocalTime.of(source[i].getHour(), source[i].getMinute(), source[i].getSecond(), 0);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param pad
   * @return
   */
  public static String movel(
      LocalDateTime source,
      String target,
      int sourceLength,
      int targetLength,
      boolean includeSeparator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVEL operation must be positive!");
    }

    if (sourceLength < 19) {
      throw new IllegalArgumentException(
          "Source length <" + sourceLength + "> is too small to contain a complete timestamp.");
    }

    if (sourceLength == 20) {
      throw new IllegalArgumentException("Invalid source length: <" + sourceLength + ">.");
    }

    if (sourceLength > 32) {
      throw new IllegalArgumentException(
          "Source length <" + sourceLength + "> is over precision of timestamp.");
    }

    if ((includeSeparator
            ? sourceLength
            : ((sourceLength == 19) ? (sourceLength - 5) : (sourceLength - 6)))
        > targetLength) {
      throw new IllegalArgumentException(
          "Target character length <"
              + targetLength
              + "> is too small to contain a complete timestamp.");
    }

    String sourceStr = convertTimestampToString(source, sourceLength, includeSeparator);

    return movel(sourceStr, target, sourceStr.length(), targetLength, pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param includeSeparator
   * @param pad
   * @return
   */
  public static String[] movel(
      LocalDateTime source,
      String target[],
      int sourceLength,
      int targetLength,
      boolean includeSeparator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, targetLength, includeSeparator, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param includeSeparator
   * @param pad
   * @return
   */
  public static String[] movel(
      LocalDateTime source[],
      String target[],
      int sourceLength,
      int targetLength,
      boolean includeSeparator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, targetLength, includeSeparator, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param timeFormat
   * @param pad
   * @return
   */
  public static BigDecimal movel(
      LocalTime source,
      BigDecimal target,
      int targetLength,
      int targetDecimal,
      String timeFormat,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if (targetLength <= 0) {
      throw new IllegalArgumentException("Operand's length of MOVEL operation must be positive!");
    }

    timeFormat = (timeFormat == null) ? "" : timeFormat.trim().toUpperCase();

    if (!isValidTimeFormatName(timeFormat) || "*USA".equalsIgnoreCase(timeFormat)) {
      throw new IllegalArgumentException("Invalid time format: <" + timeFormat + ">");
    }

    if (targetLength < getTimeLength(timeFormat, "")) {
      throw new IllegalArgumentException(
          "Target numeric length <" + targetLength + "> is too small to contain a complete time.");
    }

    String sourceStr = convertTimeToString(source, timeFormat, "");

    return movel(sourceStr, target, sourceStr.length(), targetLength, targetDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param timeFormat
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      LocalTime source,
      BigDecimal target[],
      int targetLength,
      int targetDecimal,
      String timeFormat,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], targetLength, targetDecimal, timeFormat, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param targetDecimal
   * @param timeFormat
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      LocalTime source[],
      BigDecimal target[],
      int targetLength,
      int targetDecimal,
      String timeFormat,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], targetLength, targetDecimal, timeFormat, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger movel(
      LocalTime source,
      BigInteger target,
      int targetLength,
      String timeFormat,
      boolean unsigned,
      boolean pad) {
    BigInteger result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    result = movel(source, new BigDecimal(target), targetLength, 0, timeFormat, pad).toBigInteger();

    return unsigned ? result.abs() : result;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      LocalTime source,
      BigInteger target[],
      int targetLength,
      String timeFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], targetLength, timeFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      LocalTime source[],
      BigInteger target[],
      int targetLength,
      String timeFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], targetLength, timeFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime movel(LocalTime source, LocalDateTime target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return LocalDateTime.of(
        target.getYear(),
        target.getMonthValue(),
        target.getDayOfMonth(),
        source.getHour(),
        source.getMinute(),
        source.getSecond(),
        target.getNano());
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime[] movel(LocalTime source, LocalDateTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] =
          LocalDateTime.of(
              target[i].getYear(),
              target[i].getMonthValue(),
              target[i].getDayOfMonth(),
              source.getHour(),
              source.getMinute(),
              source.getSecond(),
              target[i].getNano());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalDateTime[] movel(LocalTime source[], LocalDateTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          LocalDateTime.of(
              target[i].getYear(),
              target[i].getMonthValue(),
              target[i].getDayOfMonth(),
              source[i].getHour(),
              source[i].getMinute(),
              source[i].getSecond(),
              target[i].getNano());
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalTime movel(LocalTime source, LocalTime target) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return LocalTime.of(source.getHour(), source.getMinute(), source.getSecond(), 0);
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalTime[] movel(LocalTime source, LocalTime target[]) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = LocalTime.of(source.getHour(), source.getMinute(), source.getSecond(), 0);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @return
   */
  public static LocalTime[] movel(LocalTime source[], LocalTime target[]) {
    // validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          LocalTime.of(source[i].getHour(), source[i].getMinute(), source[i].getSecond(), 0);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long movel(
      LocalTime source,
      Long target,
      int targetLength,
      String timeFormat,
      boolean unsigned,
      boolean pad) {
    Long result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    result =
        movel(source, BigDecimal.valueOf(target), targetLength, 0, timeFormat, pad).longValue();

    return unsigned ? Math.abs(result) : result;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] movel(
      LocalTime source,
      Long target[],
      int targetLength,
      String timeFormat,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], targetLength, timeFormat, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param pad
   * @return
   */
  public static String movel(
      LocalTime source,
      String target,
      int targetLength,
      String timeFormat,
      String separator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if (targetLength <= 0) {
      throw new IllegalArgumentException("Operand's length of MOVEL operation must be positive!");
    }

    if (!isValidTimeFormatName(timeFormat)) {
      throw new IllegalArgumentException("Invalid time format: <" + timeFormat + ">");
    }

    if (targetLength < getTimeLength(timeFormat, separator)) {
      throw new IllegalArgumentException(
          "Target character length <"
              + targetLength
              + "> is too small to contain a complete time.");
    }

    String sourceStr = convertTimeToString(source, timeFormat, separator);

    return movel(sourceStr, target, sourceStr.length(), targetLength, pad);
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param pad
   * @return
   */
  public static String[] movel(
      LocalTime source,
      String target[],
      int targetLength,
      String timeFormat,
      String separator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], targetLength, timeFormat, separator, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param targetLength
   * @param timeFormat
   * @param separator
   * @param pad
   * @return
   */
  public static String[] movel(
      LocalTime source[],
      String target[],
      int targetLength,
      String timeFormat,
      String separator,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], targetLength, timeFormat, separator, pad);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int movel(
      long source,
      int target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      int targetDecimal,
      boolean pad) {
    return movel(source, target, sourceLength, targetLength, sourceDecimal, targetDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static String movel(
      long source,
      String target,
      int sourceLength,
      int targetLength,
      int sourceDecimal,
      boolean pad) {
    return movel(source, target, sourceLength, targetLength, sourceDecimal, pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal movel(
      Long source,
      BigDecimal target,
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        targetLength,
        0,
        targetDecimal,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      Long source,
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        targetLength,
        0,
        targetDecimal,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      Long source[],
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          movel(source[i], target[i], sourceLength, targetLength, targetDecimal, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger movel(
      Long source,
      BigInteger target,
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    BigInteger result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    result =
        movel(
                BigDecimal.valueOf(sourceUnsigned ? Math.abs(source) : source),
                new BigDecimal(target),
                sourceLength,
                targetLength,
                0,
                0,
                pad)
            .toBigInteger();

    return targetUnsigned ? result.abs() : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      Long source,
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        BigDecimal.valueOf(sourceUnsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        targetLength,
        0,
        targetUnsigned,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      Long source[],
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          movel(
              source[i],
              target[i],
              sourceLength,
              targetLength,
              sourceUnsigned,
              targetUnsigned,
              pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param unsigned
   * @return
   */
  public static Boolean movel(Long source, Boolean target, int sourceLength, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(BigDecimal.valueOf(unsigned ? Math.abs(source) : source), target, sourceLength, 0);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param unsigned
   * @return
   */
  public static Boolean[] movel(Long source, Boolean target[], int sourceLength, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(BigDecimal.valueOf(unsigned ? Math.abs(source) : source), target, sourceLength, 0);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param unsigned
   * @return
   */
  public static Boolean[] movel(
      Long source[], Boolean target[], int sourceLength, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, unsigned);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param unsigned
   * @return
   */
  public static LocalDate movel(
      Long source, LocalDate target, int sourceLength, String dateFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        0,
        dateFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param unsigned
   * @return
   */
  public static LocalDate[] movel(
      Long source, LocalDate target[], int sourceLength, String dateFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        0,
        dateFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param unsigned
   * @return
   */
  public static LocalDate[] movel(
      Long source[], LocalDate target[], int sourceLength, String dateFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, dateFormat, unsigned);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param unsigned
   * @return
   */
  public static LocalTime movel(
      Long source, LocalTime target, int sourceLength, String timeFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        0,
        timeFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param unsigned
   * @return
   */
  public static LocalTime[] movel(
      Long source, LocalTime target[], int sourceLength, String timeFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        0,
        timeFormat);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param unsigned
   * @return
   */
  public static LocalTime[] movel(
      Long source[], LocalTime target[], int sourceLength, String timeFormat, boolean unsigned) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, timeFormat, unsigned);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long movel(
      Long source,
      Long target,
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    Long result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    result =
        MoveUtils.movel(
                BigDecimal.valueOf(sourceUnsigned ? Math.abs(source) : source),
                BigDecimal.valueOf(target),
                sourceLength,
                targetLength,
                0,
                0,
                pad)
            .longValue();

    return targetUnsigned ? Math.abs(result) : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   */
  public static Long[] movel(
      Long source,
      Long target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        BigDecimal.valueOf(sourceUnsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        targetLength,
        0,
        targetUnsigned,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param sourceUnsigned
   * @param targetUnsigned
   * @param pad
   * @return
   */
  public static Long[] movel(
      Long source[],
      Long target[],
      int sourceLength,
      int targetLength,
      boolean sourceUnsigned,
      boolean targetUnsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] =
          movel(
              source[i],
              target[i],
              sourceLength,
              targetLength,
              sourceUnsigned,
              targetUnsigned,
              pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static String movel(
      Long source,
      String target,
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        targetLength,
        0,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static String[] movel(
      Long source,
      String target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    return movel(
        BigDecimal.valueOf(unsigned ? Math.abs(source) : source),
        target,
        sourceLength,
        targetLength,
        0,
        pad);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static String[] movel(
      Long source[],
      String target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal movel(
      String source,
      BigDecimal target,
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    BigDecimal result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVEL operation must be positive!");
    }

    if (source.length() > sourceLength) {
      throw new IllegalArgumentException(
          "Invalid source length <" + source.length() + "> to expected <" + sourceLength + ">");
    }

    if (targetDecimal < 0) {
      throw new IllegalArgumentException(
          "Operand's decimal position of MOVEL operation cannot be negative!");
    }

    // Get byte arrays of source and target in IBM37
    byte sourceBytes[] = convertUTF16ToIBM037(padRight(source, sourceLength, ' ').toCharArray());
    byte targetZoned[];

    if (sourceLength >= targetLength) {
      targetZoned = new byte[targetLength];
    } else {
      targetZoned = convertBigDecimalToZoned(target, targetLength, targetDecimal);
    }

    int top = Math.min(sourceLength, targetLength);

    for (int i = 0; i < top; i++) {
      if (((byte) (sourceBytes[i] & 0x0F)) > ((byte) 0x09)) {
        targetZoned[i] = (byte) (sourceBytes[i] & 0xF0);
      } else {
        if ((sourceBytes[i]) == ((byte) 0x40)) {
          // Fix numeric for space
          sourceBytes[i] = (byte) 0xF0;
        }

        targetZoned[i] = sourceBytes[i];
      }
    }

    /*
     * When data is moved to a numeric field, the sign (+ or -) of the
     * result field is retained except when Factor2 is a long as or longer
     * than the result field.
     * https://www.ibm.com/support/knowledgecenter/en/ssw_ibm_i_73/rzasd/zzmovel.htm
     */
    if (sourceLength >= targetLength) {
      if ((sourceBytes[sourceLength - 1]) == ((byte) 0x40)) {
        // Fix numeric for space
        sourceBytes[sourceLength - 1] = (byte) 0xF0;
      }

      targetZoned[targetLength - 1] =
          (byte) ((targetZoned[targetLength - 1] & 0x0F) | (sourceBytes[sourceLength - 1] & 0xF0));
    } else if (pad) {
      /*
       * Pad (in documentation "P") specified in the operation extender
       * position causes the result field to be padded on the left after
       * the move occurs.
       */
      for (int i = 0; i < (targetLength - sourceLength); i++) {
        targetZoned[targetLength - i - 1] = (byte) 0xF0;
      }
    }

    result = convertZonedToBigDecimal(targetZoned, targetDecimal);

    return result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      String source,
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, targetLength, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   */
  public static BigDecimal[] movel(
      String source[],
      BigDecimal target[],
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, targetLength, targetDecimal, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger movel(
      String source,
      BigInteger target,
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    BigInteger result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    result =
        movel(source, new BigDecimal(target), sourceLength, targetLength, 0, pad).toBigInteger();

    return unsigned ? result.abs() : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      String source,
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static BigInteger[] movel(
      String source[],
      BigInteger target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @return
   */
  public static Boolean movel(String source, Boolean target, int sourceLength) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if (sourceLength <= 0) {
      throw new IllegalArgumentException("Operand's length of MOVE operation must be positive!");
    }

    if (source.length() > sourceLength) {
      throw new IllegalArgumentException(
          "Invalid source length <" + source.length() + "> to expected <" + sourceLength + ">");
    }

    source = padRight(source, sourceLength, ' ');

    char ch = source.charAt(0);

    if ((ch != '1') && (ch != '0')) {
      throw new IllegalArgumentException(
          "Value <" + ch + "> in source <" + source + "> is invalid for boolean!");
    }

    return ch == '1';
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @return
   */
  public static Boolean[] movel(String source, Boolean target[], int sourceLength) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @return
   */
  public static Boolean[] movel(String source[], Boolean target[], int sourceLength) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength);
    }

    return target;
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int movel(
      String source, int target, int sourceLength, int targetLength, boolean pad) {
    return NumberFormatter.toInt(
        movel(source, String.valueOf(target), sourceLength, targetLength, pad));
  }

  /**
   * Test and remove the Deprecated annotation.
   *
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param targetDecimal
   * @param pad
   * @return
   * @author Amit Arya
   */
  public static int movel(
      String source,
      int target,
      int sourceLength,
      int targetLength,
      int targetDecimal,
      boolean pad) {
    return movel(source, BigDecimal.valueOf(target), sourceLength, targetLength, targetDecimal, pad)
        .intValue();
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @return
   */
  public static LocalDate movel(
      String source, LocalDate target, int sourceLength, String dateFormat, String separator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVE operation cannot be null!");
    }

    if (sourceLength <= 0) {
      throw new IllegalArgumentException("Operands' lengths of MOVE operation must be positive!");
    }

    if (!isValidDateFormatName(dateFormat)) {
      throw new IllegalArgumentException("Invalid date format: <" + dateFormat + ">");
    }

    int length = getDateLength(dateFormat, separator);

    if (sourceLength < length) {
      throw new IllegalArgumentException(
          "Source character length <"
              + sourceLength
              + "> is too small to contain a complete date.");
    }

    source = padRight(source, sourceLength, ' ');

    return convertStringToDate(source.substring(0, length), dateFormat, separator);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @return
   */
  public static LocalDate[] movel(
      String source, LocalDate target[], int sourceLength, String dateFormat, String separator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, dateFormat, separator);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param dateFormat
   * @param separator
   * @return
   */
  public static LocalDate[] movel(
      String source[], LocalDate target[], int sourceLength, String dateFormat, String separator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, dateFormat, separator);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param includeSeparator
   * @return
   */
  public static LocalDateTime movel(
      String source,
      LocalDateTime target,
      int sourceLength,
      int targetLength,
      boolean includeSeparator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVEL operation must be positive!");
    }

    if (targetLength < 19) {
      throw new IllegalArgumentException(
          "Target length <" + targetLength + "> is too small to contain a complete timestamp.");
    }

    if (targetLength == 20) {
      throw new IllegalArgumentException("Invalid target length: <" + targetLength + ">.");
    }

    if (targetLength > 32) {
      throw new IllegalArgumentException(
          "Target length <" + targetLength + "> is over precision of timestamp.");
    }

    targetLength =
        includeSeparator
            ? targetLength
            : ((targetLength == 19) ? (targetLength - 5) : (targetLength - 6));

    if (source.length() > sourceLength) {
      throw new IllegalArgumentException(
          "Invalid source length <" + source.length() + "> to expected <" + sourceLength + ">");
    }

    if (sourceLength < targetLength) {
      throw new IllegalArgumentException(
          "Source character length <"
              + sourceLength
              + "> is too small to contain a complete timestamp.");
    }

    return convertStringToTimestamp(
        padRight(source, sourceLength, ' ').substring(0, targetLength), includeSeparator);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @return
   */
  public static LocalDateTime[] movel(
      String source,
      LocalDateTime target[],
      int sourceLength,
      int targetLength,
      boolean includeSeparator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, targetLength, includeSeparator);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param includeSeparator
   * @return
   */
  public static LocalDateTime[] movel(
      String source[],
      LocalDateTime target[],
      int sourceLength,
      int targetLength,
      boolean includeSeparator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, targetLength, includeSeparator);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param separator
   * @return
   */
  public static LocalTime movel(
      String source, LocalTime target, int sourceLength, String timeFormat, String separator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if (sourceLength <= 0) {
      throw new IllegalArgumentException("Operands' lengths of MOVEL operation must be positive!");
    }

    if (source.length() > sourceLength) {
      throw new IllegalArgumentException(
          "Invalid source length <" + source.length() + "> to expected <" + sourceLength + ">");
    }

    if (!isValidTimeFormatName(timeFormat)) {
      throw new IllegalArgumentException("Invalid time format: <" + timeFormat + ">");
    }

    int length = getTimeLength(timeFormat, separator);

    if (sourceLength < length) {
      throw new IllegalArgumentException(
          "Source character length <"
              + sourceLength
              + "> is too small to contain a complete time.");
    }

    source = padRight(source, sourceLength, ' ');

    return convertStringToTime(source.substring(0, length), timeFormat, separator);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param separator
   * @return
   */
  public static LocalTime[] movel(
      String source, LocalTime target[], int sourceLength, String timeFormat, String separator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, timeFormat, separator);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param timeFormat
   * @param separator
   * @return
   */
  public static LocalTime[] movel(
      String source[], LocalTime target[], int sourceLength, String timeFormat, String separator) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, timeFormat, separator);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   */
  public static Long movel(
      String source,
      Long target,
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    Long result = target;

    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    result =
        movel(source, BigDecimal.valueOf(target), sourceLength, targetLength, 0, pad).longValue();

    return unsigned ? Math.abs(result) : result;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] movel(
      String source,
      Long target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param unsigned
   * @param pad
   * @return
   */
  public static Long[] movel(
      String source[],
      Long target[],
      int sourceLength,
      int targetLength,
      boolean unsigned,
      boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, targetLength, unsigned, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   */
  public static String movel(
      String source, String target, int sourceLength, int targetLength, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    if ((sourceLength <= 0) || (targetLength <= 0)) {
      throw new IllegalArgumentException("Operands' lengths of MOVEL operation must be positive!");
    }

    if (source.length() > sourceLength) {
      throw new IllegalArgumentException(
          "Invalid source length <" + source.length() + "> to expected <" + sourceLength + ">");
    }

    if (target.length() > targetLength) {
      throw new IllegalArgumentException(
          "Invalid target length <" + target.length() + "> to expected <" + targetLength + ">");
    }

    // get char arrays of source and target
    char sourceChars[] = padRight(source, sourceLength, ' ').toCharArray();
    char targetChars[] = null;

    if (sourceLength >= targetLength) {
      targetChars = new char[targetLength];
    } else {
      targetChars = padRight(target, targetLength, ' ').toCharArray();
    }

    int top = Math.min(sourceLength, targetLength);
    System.arraycopy(sourceChars, 0, targetChars, 0, top);

    /*
     * Pad (in documentation "P") specified in the operation extender
     * position causes the result field to be padded on the right after the
     * move occurs.
     * The result field is padded from the right.
     */
    if (pad) {
      for (int i = 0; i < (targetLength - sourceLength); i++) {
        targetChars[targetLength - i - 1] = ' ';
      }
    }

    return String.valueOf(targetChars);
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   */
  public static String[] movel(
      String source, String target[], int sourceLength, int targetLength, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = 0; i < target.length; i++) {
      target[i] = movel(source, target[i], sourceLength, targetLength, pad);
    }

    return target;
  }

  /**
   * @param source
   * @param target
   * @param sourceLength
   * @param targetLength
   * @param pad
   * @return
   */
  public static String[] movel(
      String source[], String target[], int sourceLength, int targetLength, boolean pad) {
    // Validate parameters
    if ((source == null) || (target == null)) {
      throw new IllegalArgumentException("Operands of MOVEL operation cannot be null!");
    }

    for (int i = Math.min(source.length, target.length) - 1; i >= 0; i--) {
      target[i] = movel(source[i], target[i], sourceLength, targetLength, pad);
    }

    return target;
  }

  /**
   * @param originalString
   * @param size
   * @param padChar
   * @return
   */
  protected static String padLeft(String originalString, int size, char padChar) {
    StringBuilder result = new StringBuilder(size);

    if (originalString != null) {
      result.append(originalString);

      while (result.length() < size) {
        result.insert(0, padChar);
      }
    }

    return result.toString();
  }

  /**
   * @param originalString
   * @param size
   * @param padChar
   * @return
   */
  protected static String padRight(String originalString, int size, char padChar) {
    StringBuilder result = new StringBuilder(size);

    if (originalString != null) {
      result.append(originalString);

      while (result.length() < size) {
        result.append(padChar);
      }
    }

    return result.toString();
  }
}
