package com.hospital.common.utils;

/**
 * Data-related utilities.
 */
public class DataUtils {

  /**
   * Make sure string is not null and does not have trailing spaces.
   */
  public static String normalize(String s) {
    if ( s == null ){
      return "";
    } else {
      return StringUtils.rtrim(s);
    }
  }

}
