package com.hospital.common.utils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

/**
 * Provides the interface for formatting and parsing numbers.
 *
 * @author Amit Arya
 * @since (2016-10-25.15:48:16)
 */
public class NumberFormatter {

  private static String getValidStr(Object objToConvert) {
    String strToConvert = String.valueOf(objToConvert);

    if (objToConvert instanceof Number || NumberUtils.isNumber(strToConvert.trim())) {
      return strToConvert.trim();
    }

    // Replace slash, colon, minus, space
    StringBuilder regDtRpl = new StringBuilder("[\\/\\:\\-\\ ]");

    if (objToConvert instanceof Date) {
      regDtRpl.insert(4, "\\.");
    }

    strToConvert = strToConvert.replaceAll(regDtRpl.toString(), "");

    return strToConvert;
  }

  /**
   * Converts a String to BigDecimal.
   *
   * @param strToConvert String to convert
   * @param precision number of digits
   * @param decimalPlaces decimal places of the strToConvert value to be returned
   * @return a BigDecimal value
   */
  public static BigDecimal toDecimal(String strToConvert, int precision, int decimalPlaces) {
    BigDecimal bd;

    if (strToConvert.trim().isEmpty()) {
      bd = new BigDecimal(0);
    } else {
      bd = new BigDecimal(strToConvert, new MathContext(precision, RoundingMode.HALF_UP));
      bd = bd.setScale(decimalPlaces, RoundingMode.HALF_UP);
    }

    return bd;
  }

  public static Integer toInt(Object objToConvert) {
    if (objToConvert instanceof LocalDate) {
      return DateTimeUtils.toInt((LocalDate) objToConvert);
    } else if (objToConvert instanceof LocalTime) {
      return DateTimeUtils.toInt((LocalTime) objToConvert);
    } else if (objToConvert instanceof LocalDateTime) {
      return DateTimeUtils.toInt((LocalDateTime) objToConvert);
    }

    return toInt(objToConvert, 10, 0);
  }

  /**
   * Converts to signed integer.
   *
   * @param objToConvert Object to convert
   * @param inScale Input scale
   * @param nbrNfe Number to set in case of NumberFormatException
   * @return an integer after converting
   */
  public static Integer toInt(Object objToConvert, int inScale, int nbrNfe) {
    if (objToConvert instanceof String) {
      // 115 3 -> 11503
      String str = objToConvert.toString().trim();

      if (str.length() != 0) {
        objToConvert = str.replaceAll("[ ]", "0");
      }
    }

    String strToConvert = getValidStr(objToConvert);

    if ((strToConvert == null) || (strToConvert.length() == 0)) {
      return nbrNfe;
    }

    if (!NumberUtils.isNumber(strToConvert)) {
      return nbrNfe;
    }

    try {
      return new Integer(strToConvert);
    } catch (NumberFormatException nfe1) {
      try {
        return new Double(strToConvert).intValue();
      } catch (NumberFormatException nfe2) {
        return nbrNfe;
      }
    }
  }
}
