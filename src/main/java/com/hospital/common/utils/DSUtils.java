package com.hospital.common.utils;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

/**
 * Utility class for data structure operations.
 *
 * @author Robin Rizvi
 * @since (2017-08-20.17:06:02)
 */
public final class DSUtils {
  private static final Logger logger = LoggerFactory.getLogger(DSUtils.class);
  private static final String CHARSET = "ISO-8859-1";

  /** Disable initialization of this class. */
  private DSUtils() {}

  /**
   * Converts number into bytes and optionally pack those bytes.
   *
   * @param number number to be converted
   * @param precision number of significant digits
   * @param scale number of digits to the right of decimal point
   * @param packed if bytes should be packed
   * @return array of bytes representing the number
   */
  private static byte[] getBytes(BigDecimal number, int precision, int scale, boolean packed) {
    if ((number.scale() > scale) || (number.precision() > precision)) {
      IllegalArgumentException ex =
          new IllegalArgumentException(
              "Precision or scale of number is greater than the specified precision and scale");
      logger.debug(ex.getMessage());
      throw ex;
    }

    if (packed) {
      return getPackedBytes(number, precision, scale);
    } else {
      String numberString = numberToString(number, precision, scale);
      byte bytes[] = numberString.getBytes();

      if (number.signum() < 0) {
        bytes = ArrayUtils.remove(numberString.getBytes(), numberString.length() - 1);

        byte lastDigitAndSign =
            getPackedBytes(
                new BigDecimal(getSignChar(number) + StringUtils.right(numberString, 1)), 1, 0)[0];
        bytes = ArrayUtils.add(bytes, lastDigitAndSign);
      }

      return bytes;
    }
  }

  /**
   * Returns bytes encoded in {@link DSUtils#CHARSET} that represents the specified string .
   *
   * @param str string to converted into bytes encoded in {@link DSUtils#CHARSET}
   * @return bytes encoded in {@link DSUtils#CHARSET}
   */
  private static byte[] getEncodedBytes(String str) {
    try {
      return str.getBytes(CHARSET);
    } catch (UnsupportedEncodingException e) {
      logger.error(e.getMessage(), e);

      return new byte[str.length()];
    }
  }

  /**
   * Returns string encoded in {@link DSUtils#CHARSET} that represents the specified bytes.
   *
   * @param bytes bytes to be returned as string encoded in {@link DSUtils#CHARSET}
   * @return string represented by the specified bytes encoded in {@link DSUtils#CHARSET}
   */
  private static String getEncodedString(byte bytes[]) {
    try {
      return new String(bytes, CHARSET);
    } catch (UnsupportedEncodingException e) {
      logger.error(e.getMessage(), e);

      return getString("0", bytes.length);
    }
  }

  /**
   * Converts bytes representing a non decimal number into a number.
   *
   * @param number bytes representing the number
   * @param size size of the number
   * @return number represented by the bytes
   */
  public static int getNumber(byte number[], int size) {
    return getNumber(number, size, 0, false).intValue();
  }

  /**
   * Converts packed/free bytes representing a number, into a number with length as dictated by the
   * precision argument and scale as dictated by the scale argument.
   *
   * @param number bytes to be converted
   * @param precision number of significant digits
   * @param scale number of digits to the right of decimal point
   * @param packed if bytes are packed
   * @return decimal number represented by the bytes
   */
  public static BigDecimal getNumber(byte number[], int precision, int scale, boolean packed) {
    BigDecimal decNumber;

    if (!isPackedCorrectly(number)) {
      String numberString = getString(getEncodedString(number), precision, '0');
      StringBuilder numberStringDot =
          new StringBuilder(numberString).insert(numberString.length() - scale, '.');

      try {
        decNumber = new BigDecimal(numberStringDot.toString());
      } catch (NumberFormatException e) {
        logger.error(
            "Number could not be formed correctly because the value "
                + "\"{}\" cannot be formatted to a numeric type. The most "
                + "probable cause of this could be that the value must have "
                + "been set from another overlapping string field.",
            numberString,
            e);

        // Return 0 since no valid value could be formed
        return BigDecimal.ZERO;
      }
    } else if (packed) {
      decNumber = getUnpackedNumber(number, precision, scale);
    } else {
      byte unPackedBytes[] = ArrayUtils.remove(number, number.length - 1);
      byte lastDigitAndSignByte[] = ArrayUtils.subarray(number, number.length - 1, number.length);
      BigDecimal lastDigitAndSign = getUnpackedNumber(lastDigitAndSignByte, 1, 0);
      String unPackedNumberString =
          getSignChar(lastDigitAndSign)
              + getEncodedString(unPackedBytes)
              + numberToString(lastDigitAndSign);

      decNumber = new BigDecimal(new BigInteger(unPackedNumberString), scale);
    }

    return decNumber;
  }

  /**
   * Converts string representing a non decimal number into a number.
   *
   * @param number string representing the number
   * @param size size of the number
   * @return number represented by the string
   */
  public static int getNumber(String number, int size) {
    return getNumber(number, size, false);
  }

  /**
   * Converts string representing a non decimal number into a number.
   *
   * @param number string representing the number
   * @param size size of the number
   * @param packed if bytes are packed
   * @return number represented by the string
   */
  public static int getNumber(String number, int size, boolean packed) {
    return getNumber(number, size, 0, packed).intValue();
  }

  /**
   * Converts a string representing a number, into a number with length as dictated by the precision
   * argument.
   *
   * @param number bytes to be converted
   * @param precision number of significant digits
   * @param scale number of digits to the right of decimal point
   * @param packed if bytes are packed
   * @return decimal number represented by the string
   */
  public static BigDecimal getNumber(String number, int precision, int scale, boolean packed) {
    return getNumber(getEncodedBytes(number), precision, scale, packed);
  }

  /**
   * Converts number into bytes and packs those bytes.
   *
   * @param number number to be converted
   * @param precision number of significant digits
   * @param scale number of digits to the right of decimal point
   * @return array of packed bytes representing the number
   */
  private static byte[] getPackedBytes(BigDecimal number, int precision, int scale) {
    int bitMask = 0xF;
    int outputLength = (precision / 2) + 1;
    byte input[] = numberToString(number, precision, scale).getBytes();
    byte output[] = new byte[outputLength];
    byte nibble[] = new byte[2];
    int inpos = 0;
    int outpos = 0;

    while (inpos < (precision - 1)) {
      nibble[0] = (byte) ((input[inpos++] & bitMask) << 4);
      nibble[1] = (byte) (input[inpos++] & bitMask);
      output[outpos++] = (byte) (nibble[0] | nibble[1]);
    }

    // If precision is odd, last digit will be stored in the last byte
    // together with sign
    if ((precision % 2) != 0) {
      nibble[0] = (byte) ((input[inpos] & bitMask) << 4);
    } else {
      nibble[0] = 0;
    }

    // Store sign in the last nibble
    nibble[1] = getSignByte(number.signum());
    output[outpos] = (byte) (nibble[0] | nibble[1]);

    return output;
  }

  /**
   * Gets the binary representation of sign (positive/negative).
   *
   * @param sign sign represented by -1/0/+1
   * @return byte representing the positive/negative sign
   */
  private static byte getSignByte(int sign) {
    return (byte) ((sign >= 0) ? 0xE : 0xF);
  }

  /**
   * Gets the sign (positive/negative) of the number as a character literal.
   *
   * @param number number whose sign value has to be retrieved
   * @return sign of the number as character literal
   */
  private static char getSignChar(Number number) {
    BigDecimal decNumber = new BigDecimal(number.toString());

    return (decNumber.signum() >= 0) ? '+' : '-';
  }

  /**
   * Converts number into bytes (optionally pack those bytes) and returns those bytes as string.
   *
   * @param number number to be converted
   * @param precision number of significant digits
   * @param scale number of digits to the right of decimal point
   * @param packed if bytes should be packed
   * @return string representing the number
   */
  public static String getString(BigDecimal number, int precision, int scale, boolean packed) {
    return getEncodedString(getBytes(number, precision, scale, packed));
  }

  /**
   * Converts decimal number with no scale into bytes and return those bytes as string.
   *
   * @param number number to be converted
   * @param size size of string to be returned
   * @param packed if bytes should be packed
   * @return string representing the number
   */
  public static String getString(Number number, int size, boolean packed) {
    return getString(new BigDecimal(number.toString()), size, 0, packed);
  }

  /**
   * Converts non decimal number into bytes and return those bytes as string.
   *
   * @param number number to be converted
   * @param size size of string to be returned
   * @return string representing the number
   */
  public static String getString(Number number, int size) {
    return getString(number, size, false);
  }

  /**
   * Converts boolean to String.
   *
   * @param boolVal boolean to be converted
   * @param size size of string to be returned
   * @return string of appropriate size
   */
  public static String getString(boolean boolVal, int size) {
    return getString(String.valueOf(boolVal), size, '0');
  }

  /**
   * Converts LocalDate to String.
   *
   * @param date LocalDate to be converted
   * @param size size of string to be returned
   * @return string of appropriate size
   */
  public static String getString(LocalDate date, int size) {
    return getString(DateTimeUtils.toString(date, "*ISO"), size, '0');
  }

  /**
   * Returns a string of appropriate size.
   *
   * @param str string
   * @param size size of the string
   * @return string of appropriate size
   */
  public static String getString(String str, int size) {
    return getString(str, size, ' ');
  }

  /**
   * Converts decimal number with no scale into bytes and return those bytes as string.
   *
   * @param number number to be converted
   * @param size size of string to be returned
   * @param packed if bytes should be packed
   * @return string representing the number
   */
  public static String getString(String number, int size, boolean packed) {
    return getString(new BigDecimal(number), size, 0, packed);
  }

  /**
   * Returns a string of appropriate size padded by padChar if required.
   *
   * @param str string
   * @param size size of the string
   * @param padChar character to pad the string with
   * @return string of appropriate size
   */
  private static String getString(String str, int size, char padChar) {
    return getEncodedString(
        StringUtils.substring(StringUtils.leftPad(str, size, padChar), 0, size).getBytes());
  }

  /**
   * Unpacks packed bytes and converts them into number.
   *
   * @param packedNumber bytes representing the number
   * @param precision number of significant digits
   * @param scale number of digits to the right of decimal point
   * @return number that is represented by the specified bytes
   */
  private static BigDecimal getUnpackedNumber(byte packedNumber[], int precision, int scale) {
    int bitMask = 0xF;
    int inputLength = packedNumber.length;
    int outputLength = (2 * packedNumber.length) - 1;
    StringBuilder output = new StringBuilder(outputLength);
    int inpos = 0;

    // Get the sign from the last nibble
    output.append(((packedNumber[inputLength - 1] & bitMask) == getSignByte(-1)) ? '-' : '+');

    while (inpos < (packedNumber.length - 1)) {
      output.append(StringUtils.right(Integer.toHexString(packedNumber[inpos] >> 4), 1));
      output.append(StringUtils.right(Integer.toHexString(packedNumber[inpos++] & bitMask), 1));
    }

    // If precision is odd, get last digit from the first nibble of the last
    // byte
    if ((precision % 2) != 0) {
      output.append(StringUtils.right(Integer.toHexString(packedNumber[inpos] >> 4), 1));
    }

    return new BigDecimal(new BigInteger(output.toString()), scale);
  }

  /**
   * Check if the specified bytes are packed correctly and are not altered externally.
   *
   * @param number bytes representing the number
   * @return true if the bytes were packed by this utility and were not altered externally
   */
  private static boolean isPackedCorrectly(byte number[]) {
    boolean isPackedCorrectly = false;

    if (((number[number.length - 1] & 0xF) == getSignByte(1))
        || ((number[number.length - 1] & 0xF) == getSignByte(-1))) {
      isPackedCorrectly = true;
    }

    return isPackedCorrectly;
  }

  /**
   * Removes decimal point and sign from the number and converts it to string.
   *
   * @param number number to be converted
   * @return number converted to string
   */
  private static String numberToString(Number number) {
    BigDecimal decNumber = new BigDecimal(number.toString());

    return decNumber.abs().movePointRight(decNumber.scale()).toBigInteger().toString();
  }

  /**
   * Converts number to a string of specified length, by adding trailing zeros as per the scale and
   * leading zeros as per the precision.
   *
   * @param number number to be converted to string
   * @param length length of the number
   * @param scale scale of the number
   * @return string representing the number of the specified length and scale
   */
  private static String numberToString(Number number, int length, int scale) {
    String numberString = numberToString(number);

    if (scale > 0) {
      BigDecimal decNumber = new BigDecimal(number.toString());
      numberString =
          StringUtils.rightPad(
              numberString, scale - decNumber.scale() + numberString.length(), '0');
    }

    return StringUtils.leftPad(numberString, length, '0');
  }
}
