package com.hospital.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Enable and scan Spring Data repositories.
 *
 */
@Configuration
@EnableJpaRepositories("com.hospital.file")
public class RepositoryConfig {
    //
}
