package com.hospital.config;

import java.io.IOException;

import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * Defines a Custom Serializer to serialize a LocalDate object with Jackson.
 *
 * @author Amit Arya
 * @since (2016-07-13.13:08:22)
 */
final public class LocalDateSerializer extends JsonSerializer<LocalDate> {

	@Override
	public void serialize(LocalDate value, JsonGenerator gen,
		SerializerProvider provider) throws IOException {
		gen.writeString(value.toString());
	}
}
