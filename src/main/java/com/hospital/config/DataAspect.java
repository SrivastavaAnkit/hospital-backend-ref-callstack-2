package com.hospital.config;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hospital.common.dao.JpaBaseEntity;


/**
 * Handles concerns related to data, like global filters, default resultset ordering etc.
 *
 * @author Robin Rizvi
 * @since (2017-09-25.17:57:12)
 */
@Aspect
@Component
public class DataAspect {

  private static final Logger logger = LoggerFactory.getLogger(DataAspect.class);

  @Autowired
  @SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
  private ApplicationContext applicationContext;

  @PersistenceContext private EntityManager entityManager;

  /** Enable database filter defined in {@link JpaBaseEntity} to support multitenancy. */
  @Before("publicTransactionalMethod()")
  public void enableDatabaseFilter() {
    Session session = entityManager.unwrap(Session.class);

    if (session.getEnabledFilter("COMPANY_FILTER") != null) {
      return;
    }

  /*  session
        .enableFilter("COMPANY_FILTER")
        .setParameter("companyid", getGlobalContext().getCompanyid());*/

    logger.info(
        "Enabled global database filter defined in {}", JpaBaseEntity.class.getSimpleName());
  }

  /**
   * Check if the application is running in batch then return the active batch context else return
   * the session scoped bean representing the global context.
   *
   * @return global context
   */
  /*private GlobalContext getGlobalContext() {
    return applicationContext.getBean(GlobalContext.class);
  }*/

  /** Pointcut that identifies public methods that are marked with {@link Transactional}. */
  @Pointcut(
      "@annotation(org.springframework.transaction.annotation.Transactional) "
          + "&& execution(public * *(..))")
  private void publicTransactionalMethod() {
    // Placeholder for public transactional method pointcut
  }
}
