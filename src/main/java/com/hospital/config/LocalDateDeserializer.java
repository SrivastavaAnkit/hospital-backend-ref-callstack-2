package com.hospital.config;

import java.io.IOException;

import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * Defines a Custom Deserializer to deserialize a LocalDate object with Jackson.
 *
 * @author Amit Arya
 * @since (2016-07-13.16:08:22)
 */
final public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {

	@Override
	public LocalDate deserialize(JsonParser parser,
		DeserializationContext context) throws IOException {
		return LocalDate.parse(parser.getText());
	}
}
