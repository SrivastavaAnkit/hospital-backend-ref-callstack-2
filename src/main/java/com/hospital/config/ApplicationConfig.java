package com.hospital.config;

import java.time.Clock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

  /** @return The system clock. Defined for testability purpose. */
  @Bean
  public Clock systemClock() {
    return Clock.systemDefaultZone();
  }
}
