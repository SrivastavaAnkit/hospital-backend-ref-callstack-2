package com.hospital.config;

import java.time.LocalTime;

import org.springframework.core.convert.converter.Converter;

/**
 * Converts String to LocalTime for Spring Type Conversion.
 *
 * @author Amit Arya
 * @since (2016-07-13.18:07:39)
 */
final public class LocalTimeConverter implements Converter<String, LocalTime> {

	public LocalTime convert(String source) {
		return LocalTime.parse(source);
	}
}
