package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for RepeatControl.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum RepeatControlEnum {

	_STA_DO_NOT_REPEAT("N", "*Do not repeat"),
	_STA_RESET("R", "*Reset"),
	_STA_REPEAT("Y", "*Repeat");
    
	private final String code;
	private final String description;

	RepeatControlEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static RepeatControlEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<RepeatControlEnum> getAllValues() {
        return Arrays.asList(RepeatControlEnum._STA_DO_NOT_REPEAT, RepeatControlEnum._STA_REPEAT, RepeatControlEnum._STA_RESET);
    }

    public static List<RepeatControlEnum> getContinue_() {
        return Arrays.asList(RepeatControlEnum._STA_REPEAT, RepeatControlEnum._STA_RESET);
    }

    /**
     * Check
     */

    public static boolean isAllValues(RepeatControlEnum repeatControl) {
        return RepeatControlEnum.getAllValues().contains(repeatControl);
    }

    public static boolean isContinue_(RepeatControlEnum repeatControl) {
        return RepeatControlEnum.getContinue_().contains(repeatControl);
    }
}