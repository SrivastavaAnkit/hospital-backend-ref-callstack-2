package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for RepeatControlEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class RepeatControlConverter implements AttributeConverter<RepeatControlEnum, String> {

	@Override
	public String convertToDatabaseColumn(RepeatControlEnum repeatControl) {
		if (repeatControl == null) {
			return "";
		}

		return repeatControl.getCode();
	}

	@Override
	public RepeatControlEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return RepeatControlEnum.fromCode("");
		}

		return RepeatControlEnum.fromCode(StringUtils.strip(dbData));
	}
}
