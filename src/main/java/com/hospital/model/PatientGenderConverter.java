package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for PatientGenderEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class PatientGenderConverter implements AttributeConverter<PatientGenderEnum, String> {

	@Override
	public String convertToDatabaseColumn(PatientGenderEnum patientGender) {
		if (patientGender == null) {
			return "";
		}

		return patientGender.getCode();
	}

	@Override
	public PatientGenderEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return PatientGenderEnum.fromCode("");
		}

		return PatientGenderEnum.fromCode(StringUtils.strip(dbData));
	}
}
