package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ContextEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ContextConverter implements AttributeConverter<ContextEnum, String> {

	@Override
	public String convertToDatabaseColumn(ContextEnum context) {
		if (context == null) {
			return "";
		}

		return context.getCode();
	}

	@Override
	public ContextEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ContextEnum.fromCode("");
		}

		return ContextEnum.fromCode(StringUtils.strip(dbData));
	}
}
