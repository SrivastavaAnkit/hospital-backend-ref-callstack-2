package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for PrintFormat.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum PrintFormatEnum {

	_DO_NOT_PRINT_FORMAT("N", "Do not print format"),
	_PRINT_FORMAT("Y", "Print format");
    
	private final String code;
	private final String description;

	PrintFormatEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static PrintFormatEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<PrintFormatEnum> getAllValues() {
        return Arrays.asList(PrintFormatEnum._DO_NOT_PRINT_FORMAT, PrintFormatEnum._PRINT_FORMAT);
    }

    /**
     * Check
     */

    public static boolean isAllValues(PrintFormatEnum printFormat) {
        return PrintFormatEnum.getAllValues().contains(printFormat);
    }
}