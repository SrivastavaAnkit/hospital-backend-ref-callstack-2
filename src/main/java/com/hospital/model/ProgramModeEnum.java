package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ProgramMode.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ProgramModeEnum {

	_STA_ADD("ADD", "*ADD"),
	_STA_AUTO("AUT", "*AUTO"),
	_STA_CHANGE("CHG", "*CHANGE"),
	_STA_DISPLAY("DSP", "*DISPLAY"),
	_STA_ENTER("ENT", "*ENTER"),
	_STA_NEW("NEW", "*NEW"),
	_STA_OPEN("OPN", "*OPEN"),
	_STA_SELECT("SEL", "*SELECT"),
	_STA_DELETE("DEL","*DELETE");
    
	private final String code;
	private final String description;

	ProgramModeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ProgramModeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ProgramModeEnum> getAllValues() {
        return Arrays.asList(ProgramModeEnum._STA_ADD, ProgramModeEnum._STA_AUTO, ProgramModeEnum._STA_CHANGE, ProgramModeEnum._STA_DISPLAY, ProgramModeEnum._STA_ENTER, ProgramModeEnum._STA_NEW, ProgramModeEnum._STA_OPEN, ProgramModeEnum._STA_SELECT);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ProgramModeEnum programMode) {
        return ProgramModeEnum.getAllValues().contains(programMode);
    }
}