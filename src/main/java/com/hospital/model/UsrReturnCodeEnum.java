package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for UsrReturnCode.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum UsrReturnCodeEnum {

	_ERROR("E", "Error"),
	_RECORD_FOUND("F", "Record found"),
	_RECORD_NOT_FOUND("N", "Record Not found");
    
	private final String code;
	private final String description;

	UsrReturnCodeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static UsrReturnCodeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<UsrReturnCodeEnum> getAllValues() {
        return Arrays.asList(UsrReturnCodeEnum._ERROR, UsrReturnCodeEnum._RECORD_FOUND, UsrReturnCodeEnum._RECORD_NOT_FOUND);
    }

    /**
     * Check
     */

    public static boolean isAllValues(UsrReturnCodeEnum usrReturnCode) {
        return UsrReturnCodeEnum.getAllValues().contains(usrReturnCode);
    }
}