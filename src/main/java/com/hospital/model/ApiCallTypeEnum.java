package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ApiCallType.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ApiCallTypeEnum {

	_STA_ASYNCHRONOUS_CALL("A", "*Asynchronous Call"),
	_STA_SYNCHRONOUS_CALL("S", "*Synchronous Call");
    
	private final String code;
	private final String description;

	ApiCallTypeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ApiCallTypeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ApiCallTypeEnum> getAllValues() {
        return Arrays.asList(ApiCallTypeEnum._STA_ASYNCHRONOUS_CALL, ApiCallTypeEnum._STA_SYNCHRONOUS_CALL);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ApiCallTypeEnum apiCallType) {
        return ApiCallTypeEnum.getAllValues().contains(apiCallType);
    }
}