package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ApiWindowStyle.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ApiWindowStyleEnum {

	_STA_HIDDEN("HIDDEN", "*Hidden"),
	_STA_MINIMIZED("MINIMIZED", "*Minimized"),
	_STA_SHOWNORMAL("NORMAL", "*ShowNormal");
    
	private final String code;
	private final String description;

	ApiWindowStyleEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ApiWindowStyleEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ApiWindowStyleEnum> getAllValues() {
        return Arrays.asList(ApiWindowStyleEnum._STA_HIDDEN, ApiWindowStyleEnum._STA_MINIMIZED, ApiWindowStyleEnum._STA_SHOWNORMAL);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ApiWindowStyleEnum apiWindowStyle) {
        return ApiWindowStyleEnum.getAllValues().contains(apiWindowStyle);
    }
}