package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for PatientGender.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum PatientGenderEnum {

	_FEMALE("F", "Female"),
	_MALE("M", "Male");
    
	private final String code;
	private final String description;

	PatientGenderEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static PatientGenderEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<PatientGenderEnum> getAllValues() {
        return Arrays.asList(PatientGenderEnum._FEMALE, PatientGenderEnum._MALE);
    }

    /**
     * Check
     */

    public static boolean isAllValues(PatientGenderEnum patientGender) {
        return PatientGenderEnum.getAllValues().contains(patientGender);
    }
}