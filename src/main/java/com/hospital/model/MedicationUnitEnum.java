package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for MedicationUnit.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum MedicationUnitEnum {

	_ZERO_USED_ERROR("0", "Zero Used Error"),
	_CAPSULES("C", "Capsules"),
	_LITER("L", "Liter"),
	_MILLILITER("M", "Milliliter"),
	_NUMBER("N", "Number"),
	_PILLS("P", "Pills"),
	_TEASPOON("S", "Teaspoon"),
	_TABLETS("T", "Tablets");
    
	private final String code;
	private final String description;

	MedicationUnitEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static MedicationUnitEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<MedicationUnitEnum> getAllValues() {
        return Arrays.asList(MedicationUnitEnum._CAPSULES, MedicationUnitEnum._LITER, MedicationUnitEnum._MILLILITER, MedicationUnitEnum._NUMBER, MedicationUnitEnum._PILLS, MedicationUnitEnum._TABLETS, MedicationUnitEnum._TEASPOON, MedicationUnitEnum._ZERO_USED_ERROR);
    }

    /**
     * Check
     */

    public static boolean isAllValues(MedicationUnitEnum medicationUnit) {
        return MedicationUnitEnum.getAllValues().contains(medicationUnit);
    }
}