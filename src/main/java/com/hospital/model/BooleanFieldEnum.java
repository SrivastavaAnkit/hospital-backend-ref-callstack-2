package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for BooleanField.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum BooleanFieldEnum {

	_STA_NOT_SET("", "*Not set"),
	_STA_STA_NO("N", "**No"),
	_STA_STA_YES("Y", "**Yes");
    
	private final String code;
	private final String description;

	BooleanFieldEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static BooleanFieldEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<BooleanFieldEnum> getAllValues() {
        return Arrays.asList(BooleanFieldEnum._STA_STA_NO, BooleanFieldEnum._STA_NOT_SET, BooleanFieldEnum._STA_STA_YES);
    }

    public static List<BooleanFieldEnum> getYesNoValues() {
        return Arrays.asList(BooleanFieldEnum._STA_STA_NO, BooleanFieldEnum._STA_STA_YES);
    }

    /**
     * Check
     */

    public static boolean isAllValues(BooleanFieldEnum booleanField) {
        return BooleanFieldEnum.getAllValues().contains(booleanField);
    }

    public static boolean isYesNoValues(BooleanFieldEnum booleanField) {
        return BooleanFieldEnum.getYesNoValues().contains(booleanField);
    }
}