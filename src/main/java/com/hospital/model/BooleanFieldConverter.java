package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for BooleanFieldEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class BooleanFieldConverter implements AttributeConverter<BooleanFieldEnum, String> {

	@Override
	public String convertToDatabaseColumn(BooleanFieldEnum booleanField) {
		if (booleanField == null) {
			return "";
		}

		return booleanField.getCode();
	}

	@Override
	public BooleanFieldEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return BooleanFieldEnum.fromCode("");
		}

		return BooleanFieldEnum.fromCode(StringUtils.strip(dbData));
	}
}
