package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for SpecialityLevel.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum SpecialityLevelEnum {

	_GENERAL_DOCTOR("1", "General Doctor"),
	_GENERAL_DENTIST("2", "General Dentist"),
	_PROFESSOR_DENTISTRYS("D", "Professor Dentistrys"),
	_PROFESSOR_GENERAL("G", "Professor General"),
	_PROFESSOR_ORTHOPEDICS("O", "Professor Orthopedics");
    
	private final String code;
	private final String description;

	SpecialityLevelEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static SpecialityLevelEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<SpecialityLevelEnum> getAllValues() {
        return Arrays.asList(SpecialityLevelEnum._GENERAL_DENTIST, SpecialityLevelEnum._GENERAL_DOCTOR, SpecialityLevelEnum._PROFESSOR_DENTISTRYS, SpecialityLevelEnum._PROFESSOR_GENERAL, SpecialityLevelEnum._PROFESSOR_ORTHOPEDICS);
    }

    /**
     * Check
     */

    public static boolean isAllValues(SpecialityLevelEnum specialityLevel) {
        return SpecialityLevelEnum.getAllValues().contains(specialityLevel);
    }
}