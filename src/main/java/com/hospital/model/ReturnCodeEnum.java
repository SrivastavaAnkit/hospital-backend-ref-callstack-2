package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ReturnCode.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ReturnCodeEnum {

	_STA_NORMAL("", "*Normal"),
	_E("E", "E"),
	_EXIT_REQUESTED("USR0091", "Exit requested"),
	_STA_RECORD_ALREADY_EXISTS("Y2U0003", "*Record already exists"),
	_STA_DATA_UPDATE_ERROR("Y2U0004", "*Data update error"),
	_STA_RECORD_DOES_NOT_EXIST("Y2U0005", "*Record does not exist"),
	_STA_UNABLE_TO_INCREMENT_DATE("Y2U0059", "*Unable to increment date"),
	_STA_UNABLE_TO_CALC_DOT_DURATION("Y2U0060", "*Unable to calc. duration"),
	_STA_ARRAY_INDEX_ERROR("Y2U0068", "*Array index error"),
	_STA_SUBSTRING_ERROR("Y2U0510", "*Substring error"),
	_STA_USER_QUIT_REQUESTED("Y2U9999", "*User QUIT requested");
    
	private final String code;
	private final String description;

	ReturnCodeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ReturnCodeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ReturnCodeEnum> getAllValues() {
        return Arrays.asList(ReturnCodeEnum._STA_ARRAY_INDEX_ERROR, ReturnCodeEnum._STA_DATA_UPDATE_ERROR, ReturnCodeEnum._E, ReturnCodeEnum._EXIT_REQUESTED, ReturnCodeEnum._STA_NORMAL, ReturnCodeEnum._STA_RECORD_ALREADY_EXISTS, ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST, ReturnCodeEnum._STA_SUBSTRING_ERROR, ReturnCodeEnum._STA_UNABLE_TO_CALC_DOT_DURATION, ReturnCodeEnum._STA_UNABLE_TO_INCREMENT_DATE, ReturnCodeEnum._STA_USER_QUIT_REQUESTED);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ReturnCodeEnum returnCode) {
        return ReturnCodeEnum.getAllValues().contains(returnCode);
    }
}