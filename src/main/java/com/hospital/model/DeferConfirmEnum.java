package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for DeferConfirm.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum DeferConfirmEnum {

	_PROCEED_TO_CONFIRM("N", "Proceed to confirm"),
	_DEFER_CONFIRM("Y", "Defer confirm");
    
	private final String code;
	private final String description;

	DeferConfirmEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static DeferConfirmEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<DeferConfirmEnum> getAllValues() {
        return Arrays.asList(DeferConfirmEnum._DEFER_CONFIRM, DeferConfirmEnum._PROCEED_TO_CONFIRM);
    }

    /**
     * Check
     */

    public static boolean isAllValues(DeferConfirmEnum deferConfirm) {
        return DeferConfirmEnum.getAllValues().contains(deferConfirm);
    }
}