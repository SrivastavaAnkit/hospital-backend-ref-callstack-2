package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for CmdKeyEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class CmdKeyConverter implements AttributeConverter<CmdKeyEnum, String> {

	@Override
	public String convertToDatabaseColumn(CmdKeyEnum cmdKey) {
		if (cmdKey == null) {
			return "";
		}

		return cmdKey.getCode();
	}

	@Override
	public CmdKeyEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return CmdKeyEnum.fromCode("");
		}

		return CmdKeyEnum.fromCode(StringUtils.strip(dbData));
	}
}
