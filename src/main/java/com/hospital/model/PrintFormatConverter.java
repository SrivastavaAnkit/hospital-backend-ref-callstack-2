package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for PrintFormatEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class PrintFormatConverter implements AttributeConverter<PrintFormatEnum, String> {

	@Override
	public String convertToDatabaseColumn(PrintFormatEnum printFormat) {
		if (printFormat == null) {
			return "";
		}

		return printFormat.getCode();
	}

	@Override
	public PrintFormatEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return PrintFormatEnum.fromCode("");
		}

		return PrintFormatEnum.fromCode(StringUtils.strip(dbData));
	}
}
