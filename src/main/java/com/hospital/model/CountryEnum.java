package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for Country.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum CountryEnum {

	_BLANK("", "Blank"),
	_AUSTRALIA("AUS", "Australia"),
	_CANADA("CAN", "Canada"),
	_FRANCE("FRA", "France"),
	_GERMANY("GER", "Germany"),
	_SOUTH_AFRICA("RSA", "South Africa"),
	_UNITED_KINGDOM("UK", "United Kingdom"),
	_UNITED_STATES_OF_AMERICA("USA", "United States of America");
    
	private final String code;
	private final String description;

	CountryEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static CountryEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<CountryEnum> getAllValues() {
        return Arrays.asList(CountryEnum._AUSTRALIA, CountryEnum._BLANK, CountryEnum._CANADA, CountryEnum._FRANCE, CountryEnum._GERMANY, CountryEnum._SOUTH_AFRICA, CountryEnum._UNITED_KINGDOM, CountryEnum._UNITED_STATES_OF_AMERICA);
    }

    /**
     * Check
     */

    public static boolean isAllValues(CountryEnum country) {
        return CountryEnum.getAllValues().contains(country);
    }
}