package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ApiCallTypeEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ApiCallTypeConverter implements AttributeConverter<ApiCallTypeEnum, String> {

	@Override
	public String convertToDatabaseColumn(ApiCallTypeEnum apiCallType) {
		if (apiCallType == null) {
			return "";
		}

		return apiCallType.getCode();
	}

	@Override
	public ApiCallTypeEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ApiCallTypeEnum.fromCode("");
		}

		return ApiCallTypeEnum.fromCode(StringUtils.strip(dbData));
	}
}
