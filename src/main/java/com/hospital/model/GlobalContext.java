package com.hospital.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
//@Scope(value="session", proxyMode=ScopedProxyMode.TARGET_CLASS)
public class GlobalContext implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GlobalContext(){
		
	}
	
	private Map<String, String> fields = new HashMap<>();

	public BigDecimal getBigDecimal(String name) {
		return (fields.containsKey(name))
				? new BigDecimal(fields.get(name))
				: BigDecimal.ZERO;
	}

	public LocalDate getLocalDate(String name) {
		return (fields.containsKey(name))
				? LocalDate.parse(fields.get(name), DateTimeFormatter.BASIC_ISO_DATE)
				: LocalDate.of(1801, 1, 1);
	}

	public long getLong(String name) {
		return (fields.containsKey(name))
				? Long.parseLong(fields.get(name))
				: 0;
	}

	public String getString(String name) {
		return (fields.containsKey(name))
				? fields.get(name)
				: "";
	}

	public void setBigDecimal(String name, BigDecimal value) {
		fields.put(name, value.toString());
	}

	public void setLocalDate(String name, LocalDate value) {
		fields.put(name, value.format(DateTimeFormatter.BASIC_ISO_DATE));
	}

	public void setLong(String name, long value) {
		fields.put(name, String.valueOf(value));
	}

	public void setString(String name, String value) {
		fields.put(name, value);
	}
}
