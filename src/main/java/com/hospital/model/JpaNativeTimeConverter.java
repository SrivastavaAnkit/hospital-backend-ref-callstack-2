package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.LocalTime;

@Converter(autoApply = true)
public class JpaNativeTimeConverter implements AttributeConverter<LocalTime, Long> {

    @Override
    public Long convertToDatabaseColumn(LocalTime time) {
        if (time == null)
            return 0L;

        return Long.valueOf(
                String.format("%02d%02d%02d",
                        time.getHour(),
                        time.getMinute(),
                        time.getSecond()));
    }

    @Override
    public LocalTime convertToEntityAttribute(Long time) {
        if (time == null || time == 0)
            return LocalTime.of(0, 0);

        String timeAsString = String.valueOf(time);
        return LocalTime.of(
                Integer.parseInt(timeAsString.substring(0, 2)),
                Integer.parseInt(timeAsString.substring(2, 4)),
                Integer.parseInt(timeAsString.substring(4, 6)));
    }
}
