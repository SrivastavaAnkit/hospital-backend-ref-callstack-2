package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for Context.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ContextEnum {

	_2ND("2ND", "2ND"),
	_3RD("3RD", "3RD"),
	_CND("CND", "CND"),
	_CON("CON", "CON"),
	_CTL("CTL", "CTL"),
	_CUR("CUR", "CUR"),
	_DB1("DB1", "DB1"),
	_DB2("DB2", "DB2"),
	_DTL("DTL", "DTL"),
	_JOB("JOB", "JOB"),
	_KEY("KEY", "KEY"),
	_NXT("NXT", "NXT"),
	_PAR("PAR", "PAR"),
	_PGM("PGM", "PGM"),
	_RCD("RCD", "RCD"),
	_WRK("WRK", "WRK");
    
	private final String code;
	private final String description;

	ContextEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ContextEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ContextEnum> getAllValues() {
        return Arrays.asList(ContextEnum._CND, ContextEnum._CON, ContextEnum._CTL, ContextEnum._CUR, ContextEnum._DB1, ContextEnum._DB2, ContextEnum._DTL, ContextEnum._JOB, ContextEnum._KEY, ContextEnum._NXT, ContextEnum._PAR, ContextEnum._PGM, ContextEnum._RCD, ContextEnum._WRK, ContextEnum._2ND, ContextEnum._3RD);
    }

    public static List<ContextEnum> getValidFieldContexts() {
        return Arrays.asList(ContextEnum._CTL, ContextEnum._CUR, ContextEnum._DB1, ContextEnum._DB2, ContextEnum._DTL, ContextEnum._JOB, ContextEnum._KEY, ContextEnum._NXT, ContextEnum._PAR, ContextEnum._PGM, ContextEnum._RCD, ContextEnum._WRK, ContextEnum._2ND, ContextEnum._3RD);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ContextEnum context) {
        return ContextEnum.getAllValues().contains(context);
    }

    public static boolean isValidFieldContexts(ContextEnum context) {
        return ContextEnum.getValidFieldContexts().contains(context);
    }
}