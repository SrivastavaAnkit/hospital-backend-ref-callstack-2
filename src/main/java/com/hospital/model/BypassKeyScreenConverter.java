package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for BypassKeyScreenEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class BypassKeyScreenConverter implements AttributeConverter<BypassKeyScreenEnum, String> {

	@Override
	public String convertToDatabaseColumn(BypassKeyScreenEnum bypassKeyScreen) {
		if (bypassKeyScreen == null) {
			return "";
		}

		return bypassKeyScreen.getCode();
	}

	@Override
	public BypassKeyScreenEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return BypassKeyScreenEnum.fromCode("");
		}

		return BypassKeyScreenEnum.fromCode(StringUtils.strip(dbData));
	}
}
