package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for PrescriptionStatusEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class PrescriptionStatusConverter implements AttributeConverter<PrescriptionStatusEnum, String> {

	@Override
	public String convertToDatabaseColumn(PrescriptionStatusEnum prescriptionStatus) {
		if (prescriptionStatus == null) {
			return "";
		}

		return prescriptionStatus.getCode();
	}

	@Override
	public PrescriptionStatusEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return PrescriptionStatusEnum.fromCode("");
		}

		return PrescriptionStatusEnum.fromCode(StringUtils.strip(dbData));
	}
}
