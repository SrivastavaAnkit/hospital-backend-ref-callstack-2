package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for PatientStatusEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class PatientStatusConverter implements AttributeConverter<PatientStatusEnum, String> {

	@Override
	public String convertToDatabaseColumn(PatientStatusEnum patientStatus) {
		if (patientStatus == null) {
			return "";
		}

		return patientStatus.getCode();
	}

	@Override
	public PatientStatusEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return PatientStatusEnum.fromCode("");
		}

		return PatientStatusEnum.fromCode(StringUtils.strip(dbData));
	}
}
