package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for MedicationUnitEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class MedicationUnitConverter implements AttributeConverter<MedicationUnitEnum, String> {

	@Override
	public String convertToDatabaseColumn(MedicationUnitEnum medicationUnit) {
		if (medicationUnit == null) {
			return "";
		}

		return medicationUnit.getCode();
	}

	@Override
	public MedicationUnitEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return MedicationUnitEnum.fromCode("");
		}

		return MedicationUnitEnum.fromCode(StringUtils.strip(dbData));
	}
}
