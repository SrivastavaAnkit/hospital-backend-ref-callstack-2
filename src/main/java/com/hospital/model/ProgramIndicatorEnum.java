package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ProgramIndicator.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ProgramIndicatorEnum {

	_STA_VALID_COMMAND_KEY("29", "*Valid command key"),
	_STA_CLEAR_SUBFILE("80", "*Clear subfile"),
	_STA_DISPLAY_SUBFILE("81", "*Display subfile"),
	_STA_END_OF_SUBFILE("82", "*End of subfile"),
	_STA_ENABLE_INVITE_TIME_OUT("83", "*Enable INVITE time out"),
	_STA_SUBFILE_NEXT_CHANGE("84", "*Subfile next change"),
	_STA_ENABLE_PUTOVR("86", "*Enable PUTOVR"),
	_STA_ENABLE_KEY_SCREEN("87", "*Enable key screen"),
	_STA_PROTECT_KEY_FIELDS("88", "*Protect key fields"),
	_STA_APO_ADD_APO_MODE("89", "*'ADD' mode"),
	_STA_RECORD_DOES_NOT_EXIST("90", "*Record does not exist"),
	_STA_RECORD_LOCKED("91", "*Record locked"),
	_STA_SUBFILE_RECORD_NOT_FOUND("92", "*Subfile record not found"),
	_STA_DISPLAY_MODE("93", "*Display mode"),
	_STA_ENABLE_CSRLOC("94", "*Enable CSRLOC"),
	_STA_INVALID_CONFIRM_VALUE("96", "*Invalid confirm value"),
	_STA_PAGE_OVERFLOW("97", "*Page overflow"),
	_STA_INVALID_SUBFILE_RECORD("98", "*Invalid subfile record"),
	_STA_GLOBAL_ERROR_FLAG("99", "*Global error flag");
    
	private final String code;
	private final String description;

	ProgramIndicatorEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ProgramIndicatorEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ProgramIndicatorEnum> getAllValues() {
        return Arrays.asList(ProgramIndicatorEnum._STA_APO_ADD_APO_MODE, ProgramIndicatorEnum._STA_CLEAR_SUBFILE, ProgramIndicatorEnum._STA_DISPLAY_MODE, ProgramIndicatorEnum._STA_DISPLAY_SUBFILE, ProgramIndicatorEnum._STA_ENABLE_CSRLOC, ProgramIndicatorEnum._STA_ENABLE_INVITE_TIME_OUT, ProgramIndicatorEnum._STA_ENABLE_KEY_SCREEN, ProgramIndicatorEnum._STA_ENABLE_PUTOVR, ProgramIndicatorEnum._STA_END_OF_SUBFILE, ProgramIndicatorEnum._STA_GLOBAL_ERROR_FLAG, ProgramIndicatorEnum._STA_INVALID_CONFIRM_VALUE, ProgramIndicatorEnum._STA_INVALID_SUBFILE_RECORD, ProgramIndicatorEnum._STA_PAGE_OVERFLOW, ProgramIndicatorEnum._STA_PROTECT_KEY_FIELDS, ProgramIndicatorEnum._STA_RECORD_DOES_NOT_EXIST, ProgramIndicatorEnum._STA_RECORD_LOCKED, ProgramIndicatorEnum._STA_SUBFILE_NEXT_CHANGE, ProgramIndicatorEnum._STA_SUBFILE_RECORD_NOT_FOUND, ProgramIndicatorEnum._STA_VALID_COMMAND_KEY);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ProgramIndicatorEnum programIndicator) {
        return ProgramIndicatorEnum.getAllValues().contains(programIndicator);
    }
}