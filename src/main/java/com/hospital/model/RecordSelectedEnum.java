package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for RecordSelected.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum RecordSelectedEnum {

	_STA_NO("N", "*NO"),
	_STA_YES("Y", "*YES");
    
	private final String code;
	private final String description;

	RecordSelectedEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static RecordSelectedEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<RecordSelectedEnum> getAllValues() {
        return Arrays.asList(RecordSelectedEnum._STA_NO, RecordSelectedEnum._STA_YES);
    }

    /**
     * Check
     */

    public static boolean isAllValues(RecordSelectedEnum recordSelected) {
        return RecordSelectedEnum.getAllValues().contains(recordSelected);
    }
}