package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for BypassKeyScreen.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum BypassKeyScreenEnum {

	_STA_DO_NOT_BYPASS_KEY_SCREEN("N", "*Do not bypass key screen"),
	_STA_BYPASS_KEY_SCREEN("Y", "*Bypass key screen");
    
	private final String code;
	private final String description;

	BypassKeyScreenEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static BypassKeyScreenEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<BypassKeyScreenEnum> getAllValues() {
        return Arrays.asList(BypassKeyScreenEnum._STA_BYPASS_KEY_SCREEN, BypassKeyScreenEnum._STA_DO_NOT_BYPASS_KEY_SCREEN);
    }

    /**
     * Check
     */

    public static boolean isAllValues(BypassKeyScreenEnum bypassKeyScreen) {
        return BypassKeyScreenEnum.getAllValues().contains(bypassKeyScreen);
    }
}