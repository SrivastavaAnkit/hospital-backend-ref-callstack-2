package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for SpecialityLevelEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class SpecialityLevelConverter implements AttributeConverter<SpecialityLevelEnum, String> {

	@Override
	public String convertToDatabaseColumn(SpecialityLevelEnum specialityLevel) {
		if (specialityLevel == null) {
			return "";
		}

		return specialityLevel.getCode();
	}

	@Override
	public SpecialityLevelEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return SpecialityLevelEnum.fromCode("");
		}

		return SpecialityLevelEnum.fromCode(StringUtils.strip(dbData));
	}
}
