package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for CmdKey.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum CmdKeyEnum {  // 217 STS CKY *CMD key

	_STA_NONE("00", "*NONE") /* 731 */ ,
	_CF01("01", "CF01") /* 407 */ ,
	_CF02("02", "CF02") /* 409 */ ,
	_CF03("03", "CF03") /* 410 */ ,
	_CF04("04", "CF04") /* 411 */ ,
	_CF05("05", "CF05") /* 412 */ ,
	_CF06("06", "CF06") /* 413 */ ,
	_CF07("07", "CF07") /* 414 */ ,
	_CF08("08", "CF08") /* 415 */ ,
	_CF09("09", "CF09") /* 416 */ ,
	_CF10("10", "CF10") /* 417 */ ,
	_CF11("11", "CF11") /* 418 */ ,
	_CF12("12", "CF12") /* 419 */ ,
	_CF13("13", "CF13") /* 420 */ ,
	_CF14("14", "CF14") /* 421 */ ,
	_CF15("15", "CF15") /* 422 */ ,
	_CF16("16", "CF16") /* 423 */ ,
	_CF17("17", "CF17") /* 424 */ ,
	_CF18("18", "CF18") /* 425 */ ,
	_CF19("19", "CF19") /* 426 */ ,
	_CF20("20", "CF20") /* 427 */ ,
	_CF21("21", "CF21") /* 428 */ ,
	_CF22("22", "CF22") /* 429 */ ,
	_CF23("23", "CF23") /* 430 */ ,
	_CF24("24", "CF24") /* 431 */ ,
	_HELP("25", "HELP") /* 480 */ ,
	_CLEAR("26", "CLEAR") /* 739 */ ,
	_ROLLUP("27", "ROLLUP") /* 477 */ ,
	_ROLLDOWN("28", "ROLLDOWN") /* 478 */ ,
	_HOME("30", "HOME") /* 479 */ ;
    
	private final String code;
	private final String description;

	CmdKeyEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static CmdKeyEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<CmdKeyEnum> getActions() { /* 526 *Actions */
        return Arrays.asList(CmdKeyEnum._CF10);
    }

    public static List<CmdKeyEnum> getAdd() { /* 483 *Add */
        return Arrays.asList(CmdKeyEnum._CF09);
    }

    public static List<CmdKeyEnum> getAllValues() { /* 408 *ALL values */
        return Arrays.asList(CmdKeyEnum._CF01, CmdKeyEnum._CF02, CmdKeyEnum._CF03, CmdKeyEnum._CF04, CmdKeyEnum._CF05, CmdKeyEnum._CF06, CmdKeyEnum._CF07, CmdKeyEnum._CF08, CmdKeyEnum._CF09, CmdKeyEnum._CF10, CmdKeyEnum._CF11, CmdKeyEnum._CF12, CmdKeyEnum._CF13, CmdKeyEnum._CF14, CmdKeyEnum._CF15, CmdKeyEnum._CF16, CmdKeyEnum._CF17, CmdKeyEnum._CF18, CmdKeyEnum._CF19, CmdKeyEnum._CF20, CmdKeyEnum._CF21, CmdKeyEnum._CF22, CmdKeyEnum._CF23, CmdKeyEnum._CF24, CmdKeyEnum._CLEAR, CmdKeyEnum._HELP, CmdKeyEnum._HOME, CmdKeyEnum._STA_NONE, CmdKeyEnum._ROLLDOWN, CmdKeyEnum._ROLLUP);
    }

    public static List<CmdKeyEnum> getCancel() { /* 530 *Cancel */
        return Arrays.asList(CmdKeyEnum._CF12);
    }

    public static List<CmdKeyEnum> getChange() { /* 484 *Change */
        return Arrays.asList(CmdKeyEnum._CF09);
    }

    public static List<CmdKeyEnum> getChangeMode() { /* 473 *Change mode */
        return Arrays.asList(CmdKeyEnum._CF09);
    }

    public static List<CmdKeyEnum> getChangeRdb() { /* 808 *Change RDB */
        return Arrays.asList(CmdKeyEnum._CF22);
    }

    public static List<CmdKeyEnum> getClearSelects() { /* 735 *Clear selects */
        return Arrays.asList(CmdKeyEnum._STA_NONE);
    }

    public static List<CmdKeyEnum> getDelete() { /* 474 *Delete */
        return Arrays.asList(CmdKeyEnum._CF11);
    }

    public static List<CmdKeyEnum> getExit() { /* 471 *Exit */
        return Arrays.asList(CmdKeyEnum._CF03);
    }

    public static List<CmdKeyEnum> getExpand() { /* 527 *Expand */
        return Arrays.asList(CmdKeyEnum._CF20);
    }

    public static List<CmdKeyEnum> getHelp() { /* 523 *Help */
        return Arrays.asList(CmdKeyEnum._CF01, CmdKeyEnum._HELP);
    }

    public static List<CmdKeyEnum> getHelpForHelp() { /* 738 *Help for help */
        return Arrays.asList(CmdKeyEnum._STA_NONE);
    }

    public static List<CmdKeyEnum> getIdeographicConversion() { /* 524 *Ideographic conversion */
        return Arrays.asList(CmdKeyEnum._CF18);
    }

    public static List<CmdKeyEnum> getKeyScreen() { /* 472 *Key screen */
        return Arrays.asList(CmdKeyEnum._CF12);
    }

    public static List<CmdKeyEnum> getMessages() { /* 736 *Messages */
        return Arrays.asList(CmdKeyEnum._STA_NONE);
    }

    public static List<CmdKeyEnum> getNext() { /* 732 *Next */
        return Arrays.asList(CmdKeyEnum._ROLLUP);
    }

    public static List<CmdKeyEnum> getNextPage() { /* 481 *Next page */
        return Arrays.asList(CmdKeyEnum._CF08, CmdKeyEnum._ROLLUP);
    }

    public static List<CmdKeyEnum> getOk() { /* 737 *OK */
        return Arrays.asList(CmdKeyEnum._STA_NONE);
    }

    public static List<CmdKeyEnum> getPosition() { /* 528 *Position */
        return Arrays.asList(CmdKeyEnum._CF17);
    }

    public static List<CmdKeyEnum> getPrevious() { /* 733 *Previous */
        return Arrays.asList(CmdKeyEnum._ROLLDOWN);
    }

    public static List<CmdKeyEnum> getPreviousPage() { /* 482 *Previous page */
        return Arrays.asList(CmdKeyEnum._CF07, CmdKeyEnum._ROLLDOWN);
    }

    public static List<CmdKeyEnum> getPrompt() { /* 525 *Prompt */
        return Arrays.asList(CmdKeyEnum._CF04);
    }

    public static List<CmdKeyEnum> getRepositionWindow() { /* 740 *Reposition window */
        return Arrays.asList(CmdKeyEnum._CLEAR);
    }

    public static List<CmdKeyEnum> getReset() { /* 522 *Reset */
        return Arrays.asList(CmdKeyEnum._CF05);
    }

    public static List<CmdKeyEnum> getResize() { /* 531 *Resize */
        return Arrays.asList(CmdKeyEnum._CF19);
    }

    public static List<CmdKeyEnum> getSelectAll() { /* 734 *Select all */
        return Arrays.asList(CmdKeyEnum._STA_NONE);
    }

    public static List<CmdKeyEnum> getSubset() { /* 529 *Subset */
        return Arrays.asList(CmdKeyEnum._CF17);
    }

    /**
     * Check
     */

    public static boolean isActions(CmdKeyEnum cmdKey) {  /* 526 *Actions */
        return CmdKeyEnum.getActions().contains(cmdKey);
    }

    public static boolean isAdd(CmdKeyEnum cmdKey) {  /* 483 *Add */
        return CmdKeyEnum.getAdd().contains(cmdKey);
    }

    public static boolean isAllValues(CmdKeyEnum cmdKey) {  /* 408 *ALL values */
        return CmdKeyEnum.getAllValues().contains(cmdKey);
    }

    public static boolean isCancel(CmdKeyEnum cmdKey) {  /* 530 *Cancel */
        return CmdKeyEnum.getCancel().contains(cmdKey);
    }

    public static boolean isChange(CmdKeyEnum cmdKey) {  /* 484 *Change */
        return CmdKeyEnum.getChange().contains(cmdKey);
    }

    public static boolean isChangeMode(CmdKeyEnum cmdKey) {  /* 473 *Change mode */
        return CmdKeyEnum.getChangeMode().contains(cmdKey);
    }

    public static boolean isChangeRdb(CmdKeyEnum cmdKey) {  /* 808 *Change RDB */
        return CmdKeyEnum.getChangeRdb().contains(cmdKey);
    }

    public static boolean isClearSelects(CmdKeyEnum cmdKey) {  /* 735 *Clear selects */
        return CmdKeyEnum.getClearSelects().contains(cmdKey);
    }

    public static boolean isDelete(CmdKeyEnum cmdKey) {  /* 474 *Delete */
        return CmdKeyEnum.getDelete().contains(cmdKey);
    }

    public static boolean isExit(CmdKeyEnum cmdKey) {  /* 471 *Exit */
        return CmdKeyEnum.getExit().contains(cmdKey);
    }

    public static boolean isExpand(CmdKeyEnum cmdKey) {  /* 527 *Expand */
        return CmdKeyEnum.getExpand().contains(cmdKey);
    }

    public static boolean isHelp(CmdKeyEnum cmdKey) {  /* 523 *Help */
        return CmdKeyEnum.getHelp().contains(cmdKey);
    }

    public static boolean isHelpForHelp(CmdKeyEnum cmdKey) {  /* 738 *Help for help */
        return CmdKeyEnum.getHelpForHelp().contains(cmdKey);
    }

    public static boolean isIdeographicConversion(CmdKeyEnum cmdKey) {  /* 524 *Ideographic conversion */
        return CmdKeyEnum.getIdeographicConversion().contains(cmdKey);
    }

    public static boolean isKeyScreen(CmdKeyEnum cmdKey) {  /* 472 *Key screen */
        return CmdKeyEnum.getKeyScreen().contains(cmdKey);
    }

    public static boolean isMessages(CmdKeyEnum cmdKey) {  /* 736 *Messages */
        return CmdKeyEnum.getMessages().contains(cmdKey);
    }

    public static boolean isNext(CmdKeyEnum cmdKey) {  /* 732 *Next */
        return CmdKeyEnum.getNext().contains(cmdKey);
    }

    public static boolean isNextPage(CmdKeyEnum cmdKey) {  /* 481 *Next page */
        return CmdKeyEnum.getNextPage().contains(cmdKey);
    }

    public static boolean isOk(CmdKeyEnum cmdKey) {  /* 737 *OK */
        return CmdKeyEnum.getOk().contains(cmdKey);
    }

    public static boolean isPosition(CmdKeyEnum cmdKey) {  /* 528 *Position */
        return CmdKeyEnum.getPosition().contains(cmdKey);
    }

    public static boolean isPrevious(CmdKeyEnum cmdKey) {  /* 733 *Previous */
        return CmdKeyEnum.getPrevious().contains(cmdKey);
    }

    public static boolean isPreviousPage(CmdKeyEnum cmdKey) {  /* 482 *Previous page */
        return CmdKeyEnum.getPreviousPage().contains(cmdKey);
    }

    public static boolean isPrompt(CmdKeyEnum cmdKey) {  /* 525 *Prompt */
        return CmdKeyEnum.getPrompt().contains(cmdKey);
    }

    public static boolean isRepositionWindow(CmdKeyEnum cmdKey) {  /* 740 *Reposition window */
        return CmdKeyEnum.getRepositionWindow().contains(cmdKey);
    }

    public static boolean isReset(CmdKeyEnum cmdKey) {  /* 522 *Reset */
        return CmdKeyEnum.getReset().contains(cmdKey);
    }

    public static boolean isResize(CmdKeyEnum cmdKey) {  /* 531 *Resize */
        return CmdKeyEnum.getResize().contains(cmdKey);
    }

    public static boolean isSelectAll(CmdKeyEnum cmdKey) {  /* 734 *Select all */
        return CmdKeyEnum.getSelectAll().contains(cmdKey);
    }

    public static boolean isSubset(CmdKeyEnum cmdKey) {  /* 529 *Subset */
        return CmdKeyEnum.getSubset().contains(cmdKey);
    }
}