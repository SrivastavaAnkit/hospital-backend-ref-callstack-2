package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for UsrReturnCodeEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class UsrReturnCodeConverter implements AttributeConverter<UsrReturnCodeEnum, String> {

	@Override
	public String convertToDatabaseColumn(UsrReturnCodeEnum usrReturnCode) {
		if (usrReturnCode == null) {
			return "";
		}

		return usrReturnCode.getCode();
	}

	@Override
	public UsrReturnCodeEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return UsrReturnCodeEnum.fromCode("");
		}

		return UsrReturnCodeEnum.fromCode(StringUtils.strip(dbData));
	}
}
