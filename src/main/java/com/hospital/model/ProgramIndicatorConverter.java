package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ProgramIndicatorEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ProgramIndicatorConverter implements AttributeConverter<ProgramIndicatorEnum, String> {

	@Override
	public String convertToDatabaseColumn(ProgramIndicatorEnum programIndicator) {
		if (programIndicator == null) {
			return "";
		}

		return programIndicator.getCode();
	}

	@Override
	public ProgramIndicatorEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ProgramIndicatorEnum.fromCode("");
		}

		return ProgramIndicatorEnum.fromCode(StringUtils.strip(dbData));
	}
}
