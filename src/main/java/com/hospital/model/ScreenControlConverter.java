package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ScreenControlEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ScreenControlConverter implements AttributeConverter<ScreenControlEnum, String> {

	@Override
	public String convertToDatabaseColumn(ScreenControlEnum screenControl) {
		if (screenControl == null) {
			return "";
		}

		return screenControl.getCode();
	}

	@Override
	public ScreenControlEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ScreenControlEnum.fromCode("");
		}

		return ScreenControlEnum.fromCode(StringUtils.strip(dbData));
	}
}
