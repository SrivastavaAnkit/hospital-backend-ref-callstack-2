package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Converter(autoApply = true)
public class JpaNativeDateConverter implements AttributeConverter<LocalDate, Long> {

    private static final LocalDate DATE_MIN = LocalDate.of(1801, 1, 1);
    private static final LocalDate DATE_MAX = LocalDate.of(2199, 12, 31);

    @Override
    public Long convertToDatabaseColumn(LocalDate date) {
        if (date == null)
            return 0L;

        if (date.isBefore(DATE_MIN) || date.isAfter(DATE_MAX))
            throw new RuntimeException("Invalid 2E Date");

        if (date.isBefore(LocalDate.of(1900, 1, 1)))
            return -(1000000 - Long.valueOf(
                    String.format("%d%02d%02d",
                            date.getYear() - 1800,
                            date.getMonthValue(),
                            date.getDayOfMonth())));
        else
            return Long.valueOf(
                    String.format("%d%02d%02d",
                            date.getYear() - 1900,
                            date.getMonthValue(),
                            date.getDayOfMonth()));
    }

    @Override
    public LocalDate convertToEntityAttribute(Long date) {
        if (date == null || date == 0)
            return DATE_MIN;

        String dateAsString = (date > 0)
                ? String.valueOf(date)
                : String.valueOf(1000000 - -date);

        Pattern pattern = Pattern.compile("^(.*?)([0-9]{2})([0-9]{2})$");
        Matcher matcher = pattern.matcher(dateAsString);
        if (matcher.matches()) {
            return (date > 0)
                    ? LocalDate.of(
                    Integer.valueOf(matcher.group(1)) + 1900,
                    Integer.valueOf(matcher.group(2)),
                    Integer.valueOf(matcher.group(3)))
                    : LocalDate.of(
                    Integer.valueOf(matcher.group(1)) + 1800,
                    Integer.valueOf(matcher.group(2)),
                    Integer.valueOf(matcher.group(3)));
        } else
            throw new IllegalArgumentException("Invalid 2E date format");
    }
}
