package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ApiWindowStyleEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ApiWindowStyleConverter implements AttributeConverter<ApiWindowStyleEnum, String> {

	@Override
	public String convertToDatabaseColumn(ApiWindowStyleEnum apiWindowStyle) {
		if (apiWindowStyle == null) {
			return "";
		}

		return apiWindowStyle.getCode();
	}

	@Override
	public ApiWindowStyleEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ApiWindowStyleEnum.fromCode("");
		}

		return ApiWindowStyleEnum.fromCode(StringUtils.strip(dbData));
	}
}
