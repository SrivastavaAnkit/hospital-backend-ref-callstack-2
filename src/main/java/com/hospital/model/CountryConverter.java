package com.hospital.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for CountryEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class CountryConverter implements AttributeConverter<CountryEnum, String> {

	@Override
	public String convertToDatabaseColumn(CountryEnum country) {
		if (country == null) {
			return "";
		}

		return country.getCode();
	}

	@Override
	public CountryEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return CountryEnum.fromCode("");
		}

		return CountryEnum.fromCode(StringUtils.strip(dbData));
	}
}
