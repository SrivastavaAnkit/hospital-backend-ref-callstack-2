package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ScreenControl.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ScreenControlEnum {

	_STA_KEY_SCREEN("0", "*Key screen"),
	_STA_FIRST_DETAILS_SCREEN("1", "*First details screen"),
	_STA_SECOND_DETAILS_SCREEN("2", "*Second details screen"),
	_STA_THIRD_DETAILS_SCREEN("3", "*Third details screen"),
	_STA_HEADER_AND_DETAILS("D", "*Header and details"),
	_STA_HEADER_ONLY("H", "*Header only");
    
	private final String code;
	private final String description;

	ScreenControlEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ScreenControlEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ScreenControlEnum> getAllValues() {
        return Arrays.asList(ScreenControlEnum._STA_FIRST_DETAILS_SCREEN, ScreenControlEnum._STA_HEADER_AND_DETAILS, ScreenControlEnum._STA_HEADER_ONLY, ScreenControlEnum._STA_KEY_SCREEN, ScreenControlEnum._STA_SECOND_DETAILS_SCREEN, ScreenControlEnum._STA_THIRD_DETAILS_SCREEN);
    }

    public static List<ScreenControlEnum> getDetailScreen() {
        return Arrays.asList(ScreenControlEnum._STA_FIRST_DETAILS_SCREEN, ScreenControlEnum._STA_SECOND_DETAILS_SCREEN, ScreenControlEnum._STA_THIRD_DETAILS_SCREEN);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ScreenControlEnum screenControl) {
        return ScreenControlEnum.getAllValues().contains(screenControl);
    }

    public static boolean isDetailScreen(ScreenControlEnum screenControl) {
        return ScreenControlEnum.getDetailScreen().contains(screenControl);
    }
}