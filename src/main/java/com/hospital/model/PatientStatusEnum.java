package com.hospital.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for PatientStatus.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum PatientStatusEnum {

	_WITH_FULL_MEDICAL_AID("F", "With Full Medical Aid"),
	_HOSPITAL_PLAN_ONLY("H", "Hospital Plan only"),
	_RECURRING_MONTHLY("M", "Recurring Monthly"),
	_NEW("N", "New"),
	_PRIVATE_PAID_UPFRONT("P", "Private Paid Upfront"),
	_TO_BE_TRANSFERRED("T", "To be Transferred"),
	_NO_MEDICAL_AID("Z", "No Medical Aid");
    
	private final String code;
	private final String description;

	PatientStatusEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static PatientStatusEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<PatientStatusEnum> getAllValues() {
        return Arrays.asList(PatientStatusEnum._HOSPITAL_PLAN_ONLY, PatientStatusEnum._NEW, PatientStatusEnum._NO_MEDICAL_AID, PatientStatusEnum._PRIVATE_PAID_UPFRONT, PatientStatusEnum._RECURRING_MONTHLY, PatientStatusEnum._TO_BE_TRANSFERRED, PatientStatusEnum._WITH_FULL_MEDICAL_AID);
    }

    public static List<PatientStatusEnum> getPaymentType() {
        return Arrays.asList(PatientStatusEnum._HOSPITAL_PLAN_ONLY, PatientStatusEnum._NO_MEDICAL_AID, PatientStatusEnum._PRIVATE_PAID_UPFRONT, PatientStatusEnum._WITH_FULL_MEDICAL_AID);
    }

    /**
     * Check
     */

    public static boolean isAllValues(PatientStatusEnum patientStatus) {
        return PatientStatusEnum.getAllValues().contains(patientStatus);
    }

    public static boolean isPaymentType(PatientStatusEnum patientStatus) {
        return PatientStatusEnum.getPaymentType().contains(patientStatus);
    }
}