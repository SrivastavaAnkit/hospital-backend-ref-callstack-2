package com.hospital.file.ward.dspwardsperhospital;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.ReloadSubfileEnum;

/**
 * State for file 'Ward' (TSADREP) and function 'DSP Wards per Hospital' (TSAMDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class DspWardsPerHospitalState extends DspWardsPerHospitalDTO {
	private static final long serialVersionUID = -2342845616758503061L;

	// Local fields
	private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");

	// System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;


    public DspWardsPerHospitalState() {

    }

	public void setLclReturnCode(ReturnCodeEnum returnCode) {
    	this.lclReturnCode = returnCode;
    }

    public ReturnCodeEnum getLclReturnCode() {
    	return lclReturnCode;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }
    }