package com.hospital.file.ward.dspwardsperhospital;

import java.io.Serializable;

import com.hospital.model.ReturnCodeEnum;

/**
 * Params for resource: DspWardsPerHospital (TSAMDFR).
 *
 * @author X2EGenerator
 */
public class DspWardsPerHospitalParams implements Serializable {
	private static final long serialVersionUID = -4589994293355327643L;
 
	private String hospitalCode = "";
	private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");

		
	public String getHospitalCode() {
		return hospitalCode;
	}
	
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	
	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
    }

    public ReturnCodeEnum getReturnCode() {
    	return returnCode;
    }

 
}