package com.hospital.file.ward.dspwardsperhospital;
 
import java.math.BigDecimal;
  
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.common.utils.RestResponsePage;

import com.hospital.file.ward.Ward;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.common.state.BaseDTO;

/**
 * Dto for file 'Ward' (TSADREP) and function 'DSP Wards per Hospital' (TSAMDFR).
 */
public class DspWardsPerHospitalDTO extends BaseDTO {
	private static final long serialVersionUID = -3289204317249321192L;

    private RestResponsePage<DspWardsPerHospitalGDO> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";

	private String hospitalCode = "";
	private String hospitalName = "";
	private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");


	private DspWardsPerHospitalGDO gdo;

    public DspWardsPerHospitalDTO() {

    }

	public DspWardsPerHospitalDTO(String hospitalCode, String hospitalName) {
		this.hospitalCode = hospitalCode;
		this.hospitalName = hospitalName;
	}

    public void setPageDto(RestResponsePage<DspWardsPerHospitalGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<DspWardsPerHospitalGDO> getPageDto() {
		return pageDto;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
    }

    public String getHospitalCode() {
    	return hospitalCode;
    }

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
    	return hospitalName;
    }

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
    }

    public ReturnCodeEnum getReturnCode() {
    	return returnCode;
    }

	public void setGdo(DspWardsPerHospitalGDO gdo) {
		this.gdo = gdo;
	}

	public DspWardsPerHospitalGDO getGdo() {
		return gdo;
	}

}