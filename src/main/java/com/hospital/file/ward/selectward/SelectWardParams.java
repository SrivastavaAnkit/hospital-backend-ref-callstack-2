package com.hospital.file.ward.selectward;

import java.io.Serializable;


/**
 * Params for resource: SelectWard (TSAKSRR).
 *
 * @author X2EGenerator
 */
public class SelectWardParams implements Serializable {
    private static final long serialVersionUID = -974208337012834163L;

	private String hospitalCode = "";
	private String wardCode = "";

	public String getHospitalCode() {
		return hospitalCode;
	}
	
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	
	public String getWardCode() {
		return wardCode;
	}
	
	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}
}

