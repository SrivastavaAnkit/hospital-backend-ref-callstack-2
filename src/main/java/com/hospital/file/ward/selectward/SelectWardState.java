package com.hospital.file.ward.selectward;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.file.ward.Ward;
import com.hospital.model.ReloadSubfileEnum;

/**
 * State for file 'Ward' (TSADREP) and function 'Select Ward' (TSAKSRR).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectWardState extends SelectWardDTO {
    private static final long serialVersionUID = 1674225332886128310L;


    // System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    public SelectWardState() {
    }

    public SelectWardState(Ward ward) {
        setDtoFields(ward);
    }

   public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
       _sysReloadSubfile = reloadSubfile;
   }

   public ReloadSubfileEnum get_SysReloadSubfile() {
       return _sysReloadSubfile;
   }

}
