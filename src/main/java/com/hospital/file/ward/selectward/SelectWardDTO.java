package com.hospital.file.ward.selectward;



import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;

import com.hospital.common.state.BaseDTO;
import com.hospital.common.state.SelectRecordDTO;
import com.hospital.file.ward.Ward;
import com.hospital.model.RecordSelectedEnum;


/**
 * Dto for file 'Ward' (TSADREP) and function 'Select Ward' (TSAKSRR).
 */
public class SelectWardDTO extends SelectRecordDTO<SelectWardGDO> {
	private static final long serialVersionUID = -6695822519174740525L;

	private String hospitalCode = "";
	private String wardCode = "";

	public SelectWardDTO() {
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getWardCode() {
		return wardCode;
	}

    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param ward Ward Entity bean
     */
    public void setDtoFields(Ward ward) {
        BeanUtils.copyProperties(ward, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param ward Ward Entity bean
     */
    public void setEntityFields(Ward ward) {
        BeanUtils.copyProperties(this, ward);
    }
}
