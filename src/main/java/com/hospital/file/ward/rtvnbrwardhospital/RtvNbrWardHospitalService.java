package com.hospital.file.ward.rtvnbrwardhospital;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.ward.Ward;
import com.hospital.file.ward.WardId;
import com.hospital.file.ward.WardRepository;
import com.hospital.model.ReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvNbrWardHospitalService extends AbstractService<RtvNbrWardHospitalService, RtvNbrWardHospitalDTO>
{
    private final Step execute = define("execute", RtvNbrWardHospitalDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private WardRepository wardRepository;
	
    @Autowired
    public RtvNbrWardHospitalService()
    {
        super(RtvNbrWardHospitalService.class, RtvNbrWardHospitalDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvNbrWardHospitalDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvNbrWardHospitalDTO dto, RtvNbrWardHospitalDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Ward> wardList = wardRepository.findAllByIdHospitalCode(dto.getHospitalCode());
		if (!wardList.isEmpty()) {
			for (Ward ward : wardList) {
				processDataRecord(dto, ward);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvNbrWardHospitalDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000009 BLK ACT
		//functionCall 1000010 ACT PAR.USR Nbr of Ward = CON.*ZERO
		dto.setUsrNbrOfWard(0);
		//switchBLK 1000003 BLK ACT
		//functionCall 1000004 ACT LCL.USR Nbr of Ward = CON.*ZERO
		dto.setLclUsrNbrOfWard(0);
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvNbrWardHospitalDTO dto, Ward ward) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		//switchSUB 41 SUB    
		//switchBLK 1000021 BLK ACT
		//functionCall 1000022 ACT LCL.USR Nbr of Ward = LCL.USR Nbr of Ward + CON.1
		dto.setLclUsrNbrOfWard(dto.getLclUsrNbrOfWard() + 1);
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvNbrWardHospitalDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		//switchSUB 52 SUB    
		//switchBLK 1000015 BLK ACT
		//functionCall 1000016 ACT PAR.USR Nbr of Ward = CON.*ZERO
		dto.setUsrNbrOfWard(0);
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvNbrWardHospitalDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		//switchSUB 61 SUB    
		//switchBLK 1000026 BLK ACT
		//functionCall 1000027 ACT PAR.USR Nbr of Ward = LCL.USR Nbr of Ward
		dto.setUsrNbrOfWard(dto.getLclUsrNbrOfWard());
        return NO_ACTION;
    }
}
