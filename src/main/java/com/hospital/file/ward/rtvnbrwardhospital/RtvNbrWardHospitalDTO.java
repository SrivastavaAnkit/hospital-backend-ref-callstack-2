package com.hospital.file.ward.rtvnbrwardhospital;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvNbrWardHospitalDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private ReturnCodeEnum returnCode;
	private String hospitalCode;
	private long usrNbrOfWard;
	private String nextScreen;
	private long lclUsrNbrOfWard;

	public String getHospitalCode() {
		return hospitalCode;
	}

	public long getUsrNbrOfWard() {
		return usrNbrOfWard;
	}

	public long getLclUsrNbrOfWard() {
		return lclUsrNbrOfWard;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public void setUsrNbrOfWard(long usrNbrOfWard) {
		this.usrNbrOfWard = usrNbrOfWard;
	}

	public void setLclUsrNbrOfWard(long lclUsrNbrOfWard) {
		this.lclUsrNbrOfWard = lclUsrNbrOfWard;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
