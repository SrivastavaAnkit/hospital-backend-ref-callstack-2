package com.hospital.file.ward;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;

import com.hospital.model.CountryEnum;

import com.hospital.file.ward.Ward;
import com.hospital.file.ward.selectward.SelectWardGDO;
import com.hospital.file.ward.editward.EditWardGDO;
import com.hospital.file.ward.dspwardsperhospital.DspWardsPerHospitalGDO;
import com.hospital.file.ward.dspwardsperhospital2.DspWardsPerHospital2GDO;

/**
 * Custom Spring Data JPA repository implementation for model: Ward (TSADREP).
 *
 * @author X2EGenerator
 */
@Repository
public class WardRepositoryImpl implements WardRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see com.hospital.file.ward.WardService#deleteWard(Ward)
	 */
	@Override
	public void deleteWard(
		String hospitalCode, String wardCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " ward.id.hospitalCode = :hospitalCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(wardCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " ward.id.wardCode = :wardCode";
			isParamSet = true;
		}

		String sqlString = "DELETE FROM Ward ward";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			query.setParameter("hospitalCode", hospitalCode);
		}

		if (StringUtils.isNotEmpty(wardCode)) {
			query.setParameter("wardCode", wardCode);
		}

		em.createNativeQuery("SET foreign_key_checks = 0").executeUpdate();
		query.executeUpdate();
		em.createNativeQuery("SET foreign_key_checks = 1").executeUpdate();
	}

	@Override
	public RestResponsePage<SelectWardGDO> selectWard(
		String hospitalCode, String wardCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " (ward.id.hospitalCode >= :hospitalCode OR ward.id.hospitalCode like CONCAT('%', :hospitalCode, '%'))";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(wardCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " ward.id.wardCode like CONCAT('%', :wardCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.ward.selectward.SelectWardGDO(ward.id.hospitalCode, ward.id.wardCode, ward.wardName) from Ward ward";
		String countQueryString = "SELECT COUNT(ward.id.hospitalCode) FROM Ward ward";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Ward ward = new Ward();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(ward.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"ward.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"ward." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"ward.id.hospitalCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"ward.id.wardCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		if (StringUtils.isNotEmpty(wardCode)) {
			countQuery.setParameter("wardCode", wardCode);
			query.setParameter("wardCode", wardCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<SelectWardGDO> content = query.getResultList();
		RestResponsePage<SelectWardGDO> pageDto = new RestResponsePage<SelectWardGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<EditWardGDO> editWard(
		String hospitalCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " ward.id.hospitalCode like CONCAT('%', :hospitalCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.ward.editward.EditWardGDO(ward.id.hospitalCode, ward.id.wardCode, ward.wardName) from Ward ward";
		String countQueryString = "SELECT COUNT(ward.id.hospitalCode) FROM Ward ward";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Ward ward = new Ward();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(ward.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"ward.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"ward." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"ward.id.hospitalCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"ward.id.wardCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<EditWardGDO> content = query.getResultList();
		RestResponsePage<EditWardGDO> pageDto = new RestResponsePage<EditWardGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<DspWardsPerHospitalGDO> dspWardsPerHospital(
		String hospitalCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " ward.id.hospitalCode like CONCAT('%', :hospitalCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.ward.dspwardsperhospital.DspWardsPerHospitalGDO(ward.id.hospitalCode, ward.id.wardCode, ward.wardName) from Ward ward";
		String countQueryString = "SELECT COUNT(ward.id.hospitalCode) FROM Ward ward";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Ward ward = new Ward();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(ward.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"ward.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"ward." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"ward.id.hospitalCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"ward.id.wardCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<DspWardsPerHospitalGDO> content = query.getResultList();
		RestResponsePage<DspWardsPerHospitalGDO> pageDto = new RestResponsePage<DspWardsPerHospitalGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.ward.WardService#rtvWardDetail(String, String)
	 */
	@Override
	public Ward rtvWardDetail(
		String hospitalCode, String wardCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " ward.id.hospitalCode = :hospitalCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(wardCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " ward.id.wardCode = :wardCode";
			isParamSet = true;
		}

		String sqlString = "SELECT ward FROM Ward ward";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			query.setParameter("hospitalCode", hospitalCode);
		}

		if (StringUtils.isNotEmpty(wardCode)) {
			query.setParameter("wardCode", wardCode);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		Ward dto = (Ward)query.getSingleResult();

		return dto;
	}

	/**
	 * @see com.hospital.file.ward.WardService#rtvNoOfWardsHospital(String)
	 */
	@Override
	public RestResponsePage<Ward> rtvNoOfWardsHospital(
		String hospitalCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " ward.id.hospitalCode = :hospitalCode";
			isParamSet = true;
		}

		String sqlString = "SELECT ward FROM Ward ward";
		String countQueryString = "SELECT COUNT(ward.id.hospitalCode) FROM Ward ward";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<Ward> content = query.getResultList();
		RestResponsePage<Ward> pageDto = new RestResponsePage<Ward>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<DspWardsPerHospital2GDO> dspWardsPerHospital2(
		String hospitalCode, CountryEnum country, String countryName, long telephoneNumber, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " ward.id.hospitalCode like CONCAT('%', :hospitalCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.ward.dspwardsperhospital2.DspWardsPerHospital2GDO(ward.id.hospitalCode, ward.id.wardCode, ward.wardName) from Ward ward";
		String countQueryString = "SELECT COUNT(ward.id.hospitalCode) FROM Ward ward";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Ward ward = new Ward();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(ward.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"ward.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"ward." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"ward.id.hospitalCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"ward.id.wardCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<DspWardsPerHospital2GDO> content = query.getResultList();
		RestResponsePage<DspWardsPerHospital2GDO> pageDto = new RestResponsePage<DspWardsPerHospital2GDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.ward.WardService#rtvWardForHospital(String)
	 */
	@Override
	public RestResponsePage<Ward> rtvWardForHospital(
		String hospitalCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " ward.id.hospitalCode = :hospitalCode";
			isParamSet = true;
		}

		String sqlString = "SELECT ward FROM Ward ward";
		String countQueryString = "SELECT COUNT(ward.id.hospitalCode) FROM Ward ward";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<Ward> content = query.getResultList();
		RestResponsePage<Ward> pageDto = new RestResponsePage<Ward>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.ward.WardService#rtvNbrWardHospital(String)
	 */
	@Override
	public RestResponsePage<Ward> rtvNbrWardHospital(
		String hospitalCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " ward.id.hospitalCode = :hospitalCode";
			isParamSet = true;
		}

		String sqlString = "SELECT ward FROM Ward ward";
		String countQueryString = "SELECT COUNT(ward.id.hospitalCode) FROM Ward ward";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<Ward> content = query.getResultList();
		RestResponsePage<Ward> pageDto = new RestResponsePage<Ward>(
				content, pageable, count.longValue());

		return pageDto;
	}

}
