package com.hospital.file.ward.editward;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.common.state.BaseDTO;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.model.ReturnCodeEnum;



/**
 * Dto for file 'Ward' (TSADREP) and function 'Edit Ward' (TSALEFR).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 *
 */

public class EditWardDTO extends BaseDTO {
	private static final long serialVersionUID = 6173800692173392392L;
	private long version = 0;

    private RestResponsePage<EditWardGDO> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";
    private boolean confirm = false;

	private String hospitalCode = "";
	private ReturnCodeEnum returnCode = null;
	private String hospitalName = "";
	private long usrNbrOfWard = 0L;


	private EditWardGDO gdo;

    public EditWardDTO() {

    }

	public EditWardDTO(long version, String hospitalCode, String hospitalName) {
		this.version = version;
		this.hospitalCode = hospitalCode;
		this.hospitalName = hospitalName;
	}

	public void setVersion(long version) {
		this.version = version;
    }

    public long getVersion() {
    	return version;
    }

    public void setPageDto(RestResponsePage<EditWardGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<EditWardGDO> getPageDto() {
		return pageDto;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

    public boolean isConfirm() {
		return confirm;
	}

	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
    }

    public String getHospitalCode() {
    	return hospitalCode;
    }

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
    }

    public ReturnCodeEnum getReturnCode() {
    	return returnCode;
    }

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
    	return hospitalName;
    }

	public void setUsrNbrOfWard(long usrNbrOfWard) {
		this.usrNbrOfWard = usrNbrOfWard;
    }

    public long getUsrNbrOfWard() {
    	return usrNbrOfWard;
    }

	public void setGdo(EditWardGDO gdo) {
		this.gdo = gdo;
	}

	public EditWardGDO getGdo() {
		return gdo;
	}

}