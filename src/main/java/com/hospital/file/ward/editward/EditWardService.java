package com.hospital.file.ward.editward;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.exception.ServiceException;
import com.hospital.common.utils.RestResponsePage;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callService;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsParams;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsService;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.medication.editmedication.EditMedicationGDO;
import com.hospital.file.patient.dspfdisplaypatientswrd.DspfDisplayPatientsWrdParams;
import com.hospital.file.patient.dspfdisplaypatientswrd.DspfDisplayPatientsWrdService;
import com.hospital.file.ward.WardId;
import com.hospital.file.ward.WardRepository;
import com.hospital.file.ward.changeward.ChangeWardDTO;
import com.hospital.file.ward.changeward.ChangeWardService;
import com.hospital.file.ward.createward.CreateWardDTO;
import com.hospital.file.ward.createward.CreateWardService;
import com.hospital.file.ward.deleteward.DeleteWardDTO;
import com.hospital.file.ward.deleteward.DeleteWardService;
import com.hospital.file.ward.dspwardsperhospital2.DspWardsPerHospital2Params;
import com.hospital.file.ward.rtvnbrwardhospital.RtvNbrWardHospitalDTO;
import com.hospital.file.ward.rtvnbrwardhospital.RtvNbrWardHospitalService;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.DeferConfirmEnum;
import com.hospital.model.ProgramModeEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.support.JobContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.stereotype.Service;


/**
 * EDTFIL controller for 'Edit Ward' (TSALEFR) of file 'Ward' (TSADREP)
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 */
@Service
public class EditWardService extends AbstractService<EditWardService, EditWardState> {
    
	@Autowired
	private JobContext job;

    @Autowired
    private WardRepository wardRepository;	
	@Autowired
	private ChangeWardService changeWardService;
	
	@Autowired
	private CreateLogFileService createLogFileService;
	
	@Autowired
	private CreateWardService createWardService;
	
	@Autowired
	private DeleteWardService deleteWardService;
	
	@Autowired
	private RtvNbrWardHospitalService rtvNbrWardHospitalService;
	

    @Autowired
    private EditWardService editWardService;

    @Autowired
    private MessageSource messageSource;

//    @Autowired
//    private EditWardValidator editWardValidator;


    
	public static final String SCREEN_CTL = "editWard";
    public static final String SCREEN_RCD = "EditWard.rcd";
    public static final String SCREEN_CFM = "EditWard.confirm";

    private final Step execute = define("execute", EditWardParams.class, this::execute);
    private final Step response = define("ctlScreen", EditWardState.class, this::processResponse);
	private final Step confirmScreenResponse = define("cfmscreen", EditWardDTO.class, this::processConfirmScreenResponse);
	private final Step serviceDspfDisplayPatientsWrd = define("serviceDspfDisplayPatientsWrd",DspfDisplayPatientsWrdParams.class, this::processServiceDspfDisplayPatientsWrd);
	private final Step serviceDspAllHospitals = define("serviceDspAllHospitals",DspAllHospitalsParams.class, this::processServiceDspAllHospitals);
	//private final Step serviceCreateWard = define("serviceCreateWard",CreateWardParams.class, this::processServiceCreateWard);
	//private final Step serviceDeleteWard = define("serviceDeleteWard",DeleteWardParams.class, this::processServiceDeleteWard);
	//private final Step serviceChangeWard = define("serviceChangeWard",ChangeWardParams.class, this::processServiceChangeWard);
	//private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
	//private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
	//private final Step serviceQuit = define("serviceQuit",QuitParams.class, this::processServiceQuit);
	//private final Step serviceRtvNbrWardHospital = define("serviceRtvNbrWardHospital",RtvNbrWardHospitalParams.class, this::processServiceRtvNbrWardHospital);
	//private final Step serviceCreateLogFile = define("serviceCreateLogFile",CreateLogFileParams.class, this::processServiceCreateLogFile);
	//private final Step serviceConcat = define("serviceConcat",ConcatParams.class, this::processServiceConcat);
	
    
    @Autowired
    public EditWardService() {
        super(EditWardService.class, EditWardState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * EDTFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(EditWardState state, EditWardParams params) {
		StepResult result = NO_ACTION;

		BeanUtils.copyProperties(params, state);
		usrInitializeProgram(state);

		result = mainLoop(state);

		return result;
	}

	/**
	 * SCREEN_KEY initial processing loop method.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult mainLoop(EditWardState state) {
		StepResult result = NO_ACTION;

		result = loadFirstSubfilePage(state);

		state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
		result = conductScreenConversation(state);

		return result;
	}

	/**
	 * SCREEN  initial processing loop method.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult loadFirstSubfilePage(EditWardState state) {
		StepResult result = NO_ACTION;

		result = usrInitializeSubfileHeader(state);

		dbfReadFirstDataRecord(state);
		if (state.getPageDto() != null && state.getPageDto().getSize() > 0) {
			state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
			result = loadNextSubfilePage(state);
		} else {
			state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
		}

		return result;
	}

	/**
	 * Iterate on data loaded to do stuff on each record.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult loadNextSubfilePage(EditWardState state) {
		StepResult result = NO_ACTION;

		for (EditWardGDO gdo : state.getPageDto().getContent()) {
			if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
				if (gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
//             TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
					result = usrInitializeSubfileRecordExistingRecord(state, gdo);
				}
			} else {
//             TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
				result = usrInitializeSubfileRecordNewRecord(state, gdo);
			}
			validateSubfileRecord(state, gdo);
			result = dbfUpdateDataRecord(state, gdo);
		}

		return result;
	}

	/**
	 * SCREEN  validate subfile record.
	 *
	 * @param gdo - subfile record class.
	 * @return result
	 */
	private StepResult validateSubfileRecord(EditWardState state, EditWardGDO gdo) {
		StepResult result = NO_ACTION;

		return result;
	}

	/**
	 * SCREEN  initial processing loop method.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult conductScreenConversation(EditWardState state) {
		StepResult result = NO_ACTION;

		if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
			EditWardDTO dto = new EditWardDTO();
			BeanUtils.copyProperties(state, dto);
			result = StepResult.callScreen(SCREEN_CTL, dto).thenCall(response);
		}

		return result;
	}

	/**
	 * SCREEN_KEY returned response processing method.
	 *
	 * @param state      - Service state class.
	 * @param fromScreen - returned screen model.
	 * @return result
	 */
	private StepResult processResponse(EditWardState state, EditWardDTO fromScreen) {
		StepResult result = NO_ACTION;

		// update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
		BeanUtils.copyProperties(fromScreen, state);

		if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
			result = closedown(state);
		}
		else if (CmdKeyEnum.isChangeMode(state.get_SysCmdKey())) {
			   if(state.get_SysProgramMode()==ProgramModeEnum._STA_CHANGE){
			    state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
			    List<EditWardGDO> content=new ArrayList<EditWardGDO>();
			    for(int idx=0;idx<10;idx++){
			     content.add(new EditWardGDO());
			    }
			    RestResponsePage<EditWardGDO> pageDto = new RestResponsePage<>(content);
			          state.setPageDto(pageDto);
			   return conductScreenConversation(state);
			   }
			   else{
			    state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
			    return mainLoop(state);
			   }
		}else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
			state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
		} else if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
			result = processHelpRequest(state); //synon built-in function
		} else if (CmdKeyEnum.isNextPage(state.get_SysCmdKey())) {
			dbfReadNextPageRecord(state);
			result = loadNextSubfilePage(state);
		} else {
			result = processScreen(state);
		}

		return result;
	}

	private StepResult processHelpRequest(EditWardState state) {
		return NO_ACTION;
	}

	/**
	 * SCREEN process screen.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult processScreen(EditWardState state) {
		StepResult result = NO_ACTION;

		result = validateSubfileControl(state);
		if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
			return result;
		} else {
			for (EditWardGDO gdo : state.getPageDto().getContent()) {
				//if (gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
					result = usrValidateSubfileRecordFields(state, gdo);
					if(result!=NO_ACTION) {
						return result;
					}
					if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
						return result;
					}
					result = checkRelations(gdo);
					if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
						return result;
					}
					result = usrSubfileRecordFunctionFields(state, gdo);
					result = usrValidateSubfileRecordRelations(state, gdo);
					if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
						return result;
					}
					result = dbfUpdateDataRecord(state, gdo);
				//}
			}
		}

		EditWardDTO dto = new EditWardDTO();
		BeanUtils.copyProperties(state, dto);
		//TODO: Fix Me CFM Screen
		//result = StepResult.callScreen(SCREEN_CFM, dto).thenCall(confirmScreenResponse);

		return result;
	}

	/**
	 * SCREEN  check the relations of GDO.
	 *
	 * @param gdo -  Service subfile record class.
	 * @return result
	 */
	private StepResult checkRelations(EditWardGDO gdo) {
		StepResult result = NO_ACTION;

		return result;
	}

	/**
	 * SCREEN  validate subfile control.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult validateSubfileControl(EditWardState state) {
		StepResult result = NO_ACTION;

		state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
		validateSubfileControlField(state);
		if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
			return result;
		}
		usrSubfileControlFunctionFields(state);
		result = usrValidateSubfileControl(state);

		return result;
	}

	/**
	 * SCREEN  validate subfile control field.
	 *
	 * @param state -  Service state class.
	 * @return result
	 */
	private StepResult validateSubfileControlField(EditWardState state) {
		StepResult result = NO_ACTION;

		return result;
	}

	/**
	 * SCREEN_CONFIRM returned response processing method.
	 *
	 * @param state - Service state class.
	 * @param model - returned screen model.
	 * @return result
	 */
	private StepResult processConfirmScreenResponse(EditWardState state, EditWardDTO model) {
		StepResult result = NO_ACTION;

		if (state.isConfirm()) {
			for (EditWardGDO gdo : state.getPageDto().getContent()) {
				if (gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
					if ("DELETE".equals(gdo.get_SysSelected())) {
						result = dbfDeleteDataRecord(state, gdo);
					} else {
						if ("CHANGE".equals(gdo.get_SysSelected())) {
							result = dbfUpdateDataRecord(state, gdo);
						} else {
							result = dbfCreateDataRecord(state, gdo);
						}
					}
				}
				result = usrExtraProcessingAfterDBFUpdate(state);
				state.setConfirm(false);
			}
		}

		if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
			return result;
		} else {
			if (state.get_SysReloadSubfile() == ReloadSubfileEnum._STA_YES) {
				// request subfile reload if necessary
			}
			if (CmdKeyEnum.isChangeMode(state.get_SysCmdKey())) {
				if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
					state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
				} else {
					state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
				}
			} else {
				result = usrProcessCommandKeys(state);
				if (result != StepResult.NO_ACTION) {
					return result;
				}
			}
		}

		result = conductScreenConversation(state);

		return result;
	}

	/**
	 * Terminate this program
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult closedown(EditWardState state) {
		StepResult result = NO_ACTION;

		state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		result = usrExitProgramProcessing(state);

		return result;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(EditWardState state) {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadNextPageRecord(EditWardState state) {
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    private void dbfReadDataRecord(EditWardState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = new PageRequest(state.getPage(), state.getSize());
        }
        else {
            pageable = new PageRequest(state.getPage(), state.getSize(), new Sort(sortOrders));
        }

        RestResponsePage<EditWardGDO> pageDto = wardRepository.editWard(state.getHospitalCode(), pageable);
        state.setPageDto(pageDto);
    }


    /**
     * Create record data
     * @param dto - Service state class.
     * @return
     */
    private StepResult dbfCreateDataRecord(EditWardState dto, EditWardGDO gdo) {
		StepResult result = NO_ACTION;


//        val dbfOBJ: X2EFunction = getFunctionForType(x2EFile, "CRTOBJ")
//        val dtoOBJ = dbfOBJ.asServiceDtoClassName()
//        val dtoVAR = dbfOBJ.asServiceDtoVariableName()
        try {
        	/**
			 * USER: Create Object (Generated:1387)
			 */
			CreateWardDTO createWardDTO;
			//switchBLK 1387 BLK ACT
			// DEBUG genFunctionCall 1388 ACT Create Ward - Ward  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createWardDTO = new CreateWardDTO();
			// DEBUG genFunctionCall Parameters IN
			createWardDTO.setHospitalCode(gdo.getHospitalCode());
			createWardDTO.setWardCode(gdo.getWardCode());
			createWardDTO.setWardName(gdo.getWardName());
			// DEBUG genFunctionCall Service call
			createWardService.execute(createWardDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setLclUsrReturnCode(createWardDTO.getUsrReturnCode());
			// DEBUG genFunctionCall Parameters DONE
	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
	}


    /**
     * Delete record data
     * @param dto - Service state class.
     * @return
     */
    private StepResult dbfDeleteDataRecord(EditWardState dto, EditWardGDO gdo) {
        StepResult result = NO_ACTION;


//        val functions = x2EFile.functions.filter { it.functionType == "DLTOBJ" }

//        val dbfOBJ: X2EFunction = if (functions.isNotEmpty()) getFunctionForType(x2EFile, "DLTOBJ") else function
//        val dtoOBJ = dbfOBJ.asServiceDtoClassName()
//        val dtoVAR = dbfOBJ.asServiceDtoVariableName()
        try {
        	/**
			 * USER: Delete Object (Generated:1389)
			 */
			DeleteWardDTO deleteWardDTO;
			//switchBLK 1389 BLK ACT
			// DEBUG genFunctionCall 1390 ACT Delete Ward - Ward  *
			// DEBUG genFunctionCall ServiceDtoVariable
			deleteWardDTO = new DeleteWardDTO();
			// DEBUG genFunctionCall Parameters IN
			deleteWardDTO.setHospitalCode(gdo.getHospitalCode());
			deleteWardDTO.setWardCode(gdo.getWardCode());
			// DEBUG genFunctionCall Service call
			deleteWardService.execute(deleteWardDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
	}


    /**
     * Update record data
     * @param dto - Service state class.
     * @return
     */
    private StepResult dbfUpdateDataRecord(EditWardState dto, EditWardGDO gdo) {
        StepResult result = NO_ACTION;


//        val dbfOBJ: X2EFunction = getFunctionForType(x2EFile, "CHGOBJ")
//        val dtoOBJ = dbfOBJ.asServiceDtoClassName()
//        val dtoVAR = dbfOBJ.asServiceDtoVariableName()
        try {
			WardId wardId = new WardId(gdo.getHospitalCode(), gdo.getWardCode());
			if (wardRepository.existsById(wardId)) {
			
        		/**
				 * USER: Change Object (Generated:1391)
				 */
				ChangeWardDTO changeWardDTO;
				//switchBLK 1391 BLK ACT
				// DEBUG genFunctionCall 1392 ACT Change Ward - Ward  *
				// DEBUG genFunctionCall ServiceDtoVariable
				changeWardDTO = new ChangeWardDTO();
				// DEBUG genFunctionCall Parameters IN
				changeWardDTO.setHospitalCode(gdo.getHospitalCode());
				changeWardDTO.setWardCode(gdo.getWardCode());
				changeWardDTO.setWardName(gdo.getWardName());
				changeWardDTO.setAddedUser(gdo.getAddedUser());
				changeWardDTO.setAddedDate(gdo.getAddedDate());
				changeWardDTO.setAddedTime(gdo.getAddedTime());
				changeWardDTO.setChangedUser(gdo.getChangedUser());
				changeWardDTO.setChangedDate(gdo.getChangedDate());
				changeWardDTO.setChangedTime(gdo.getChangedTime());
				// DEBUG genFunctionCall Service call
				changeWardService.execute(changeWardDTO);
				// DEBUG genFunctionCall Parameters OUT
				dto.setLclUsrReturnCode(changeWardDTO.getUsrReturnCode());
				// DEBUG genFunctionCall Parameters DONE
		
            }
            else {
                throw new ServiceException("diagnosis.nf");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
	}


    
	/**
	 * USER: Initialize Program (Generated:15)
	 */
    private StepResult usrInitializeProgram(EditWardState dto) {
        StepResult result = NO_ACTION;

        try {
        		CreateLogFileDTO createLogFileDTO;
				//switchSUB 15 SUB    
				//switchBLK 1000077 BLK ACT
				// DEBUG genFunctionCall 1000078 ACT PGM.*Scan limit = CND.High Value
				dto.set_SysScanLimit(9999999);
				//switchBLK 1000209 BLK TXT
				// 
				//switchBLK 1000210 BLK ACT
				// DEBUG genFunctionCall 1000211 ACT LCL.Log Function Type = CON.EDTFIL
				dto.setLclLogFunctionType("EDTFIL");
				//switchBLK 1000214 BLK ACT
				// DEBUG genFunctionCall 1000215 ACT LCL.Log User Point = CON.Initialize program
				dto.setLclLogUserPoint("Initialize program");
				//switchBLK 1000224 BLK ACT
				// DEBUG genFunctionCall 1000225 ACT Create Log File - Log File  *
				// DEBUG genFunctionCall ServiceDtoVariable
				createLogFileDTO = new CreateLogFileDTO();
				// DEBUG genFunctionCall Parameters IN
				createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
				createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
				// DEBUG genFunctionCall Service call
				createLogFileService.execute(createLogFileDTO);
				// DEBUG genFunctionCall Parameters OUT
				// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Header (Generated:1488)
	 */
    private StepResult usrInitializeSubfileHeader(EditWardState dto) {
        StepResult result = NO_ACTION;

        try {
        		CreateLogFileDTO createLogFileDTO;
				RtvNbrWardHospitalDTO rtvNbrWardHospitalDTO;
				//switchSUB 1488 SUB    
				//switchBLK 1000085 BLK ACT
				// DEBUG genFunctionCall 1000086 ACT RTV Nbr Ward /Hospital - Ward  *
				// DEBUG genFunctionCall ServiceDtoVariable
				rtvNbrWardHospitalDTO = new RtvNbrWardHospitalDTO();
				// DEBUG genFunctionCall Parameters IN
				rtvNbrWardHospitalDTO.setHospitalCode(dto.getHospitalCode());
				// DEBUG genFunctionCall Service call
				rtvNbrWardHospitalService.execute(rtvNbrWardHospitalDTO);
				// DEBUG genFunctionCall Parameters OUT
				dto.setUsrNbrOfWard(rtvNbrWardHospitalDTO.getUsrNbrOfWard());
				// DEBUG genFunctionCall Parameters DONE
				//switchBLK 1000233 BLK TXT
				// 
				//switchBLK 1000234 BLK ACT
				// DEBUG genFunctionCall 1000235 ACT LCL.Log Function Type = CON.EDTFIL
				dto.setLclLogFunctionType("EDTFIL");
				//switchBLK 1000238 BLK ACT
				// DEBUG genFunctionCall 1000239 ACT LCL.Log User Point = CON.Initialize subfile header
				dto.setLclLogUserPoint("Initialize subfile header");
				//switchBLK 1000248 BLK ACT
				// DEBUG genFunctionCall 1000249 ACT Create Log File - Log File  *
				// DEBUG genFunctionCall ServiceDtoVariable
				createLogFileDTO = new CreateLogFileDTO();
				// DEBUG genFunctionCall Parameters IN
				createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
				createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
				// DEBUG genFunctionCall Service call
				createLogFileService.execute(createLogFileDTO);
				// DEBUG genFunctionCall Parameters OUT
				// DEBUG genFunctionCall Parameters DONE
        
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Record (New Record) (Generated:1027)
	 */
    private StepResult usrInitializeSubfileRecordNewRecord(EditWardState dto, EditWardGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        		CreateLogFileDTO createLogFileDTO;
				//switchSUB 1027 SUB    
				//switchBLK 1000108 BLK ACT
				// DEBUG genFunctionCall 1000109 ACT RCD.Added User = JOB.*USER
				gdo.setAddedUser(job.getUser());
				//switchBLK 1000112 BLK ACT
				// DEBUG genFunctionCall 1000113 ACT RCD.Added Date = JOB.*Job date
				gdo.setAddedDate(LocalDate.now());
				//switchBLK 1000116 BLK ACT
				// DEBUG genFunctionCall 1000117 ACT RCD.Added Time = JOB.*Job time
				gdo.setAddedTime(LocalTime.now());
				//switchBLK 1000309 BLK TXT
				// 
				//switchBLK 1000310 BLK ACT
				// DEBUG genFunctionCall 1000311 ACT LCL.Log Function Type = CON.EDTFIL
				dto.setLclLogFunctionType("EDTFIL");
				//switchBLK 1000314 BLK ACT
				// DEBUG genFunctionCall 1000315 ACT LCL.Log User Point = CON.Initialize subfile record
				dto.setLclLogUserPoint("Initialize subfile record");
				//switchBLK 1000318 BLK ACT
				// DEBUG genFunctionCall 1000319 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.(new record),CND.*One)
				dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "(new record)"));
				//switchBLK 1000324 BLK ACT
				// DEBUG genFunctionCall 1000325 ACT Create Log File - Log File  *
				// DEBUG genFunctionCall ServiceDtoVariable
				createLogFileDTO = new CreateLogFileDTO();
				// DEBUG genFunctionCall Parameters IN
				createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
				createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
				// DEBUG genFunctionCall Service call
				createLogFileService.execute(createLogFileDTO);
				// DEBUG genFunctionCall Parameters OUT
				// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Record (Existing Record) (Generated:1023)
	 */
    private StepResult usrInitializeSubfileRecordExistingRecord(EditWardState dto, EditWardGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        		CreateLogFileDTO createLogFileDTO;
				//switchSUB 1023 SUB    
				//switchBLK 1000095 BLK ACT
				// DEBUG genFunctionCall 1000096 ACT RCD.Changed User = JOB.*USER
				gdo.setChangedUser(job.getUser());
				//switchBLK 1000102 BLK ACT
				// DEBUG genFunctionCall 1000103 ACT RCD.Changed Date = JOB.*Job date
				gdo.setChangedDate(LocalDate.now());
				//switchBLK 1000089 BLK ACT
				// DEBUG genFunctionCall 1000090 ACT RCD.Changed Time = JOB.*Job time
				gdo.setChangedTime(LocalTime.now());
				//switchBLK 1000257 BLK TXT
				// 
				//switchBLK 1000258 BLK ACT
				// DEBUG genFunctionCall 1000259 ACT LCL.Log Function Type = CON.EDTFIL
				dto.setLclLogFunctionType("EDTFIL");
				//switchBLK 1000262 BLK ACT
				// DEBUG genFunctionCall 1000263 ACT LCL.Log User Point = CON.Initialize subfile record
				dto.setLclLogUserPoint("Initialize subfile record");
				//switchBLK 1000266 BLK ACT
				// DEBUG genFunctionCall 1000267 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.(existing record),CND.*One)
				dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "(existing record)"));
				//switchBLK 1000272 BLK ACT
				// DEBUG genFunctionCall 1000273 ACT Create Log File - Log File  *
				// DEBUG genFunctionCall ServiceDtoVariable
				createLogFileDTO = new CreateLogFileDTO();
				// DEBUG genFunctionCall Parameters IN
				createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
				createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
				// DEBUG genFunctionCall Service call
				createLogFileService.execute(createLogFileDTO);
				// DEBUG genFunctionCall Parameters OUT
				// DEBUG genFunctionCall Parameters DONE

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:1463)
	 */
    private StepResult usrSubfileControlFunctionFields(EditWardState dto) {
        StepResult result = NO_ACTION;

        try {
        		CreateLogFileDTO createLogFileDTO;
				RtvNbrWardHospitalDTO rtvNbrWardHospitalDTO;
				//switchSUB 1463 SUB    
				//switchBLK 1000130 BLK ACT
				// DEBUG genFunctionCall 1000131 ACT RTV Nbr Ward /Hospital - Ward  *
				// DEBUG genFunctionCall ServiceDtoVariable
				rtvNbrWardHospitalDTO = new RtvNbrWardHospitalDTO();
				// DEBUG genFunctionCall Parameters IN
				rtvNbrWardHospitalDTO.setHospitalCode(dto.getHospitalCode());
				// DEBUG genFunctionCall Service call
				rtvNbrWardHospitalService.execute(rtvNbrWardHospitalDTO);
				// DEBUG genFunctionCall Parameters OUT
				dto.setUsrNbrOfWard(rtvNbrWardHospitalDTO.getUsrNbrOfWard());
				// DEBUG genFunctionCall Parameters DONE
				//switchBLK 1000333 BLK TXT
				// 
				//switchBLK 1000334 BLK ACT
				// DEBUG genFunctionCall 1000335 ACT LCL.Log Function Type = CON.EDTFIL
				dto.setLclLogFunctionType("EDTFIL");
				//switchBLK 1000338 BLK ACT
				// DEBUG genFunctionCall 1000339 ACT LCL.Log User Point = CON.Subfile control function
				dto.setLclLogUserPoint("Subfile control function");
				//switchBLK 1000342 BLK ACT
				// DEBUG genFunctionCall 1000343 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.fields,CND.*One)
				dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "fields"));
				//switchBLK 1000348 BLK ACT
				// DEBUG genFunctionCall 1000349 ACT Create Log File - Log File  *
				// DEBUG genFunctionCall ServiceDtoVariable
				createLogFileDTO = new CreateLogFileDTO();
				// DEBUG genFunctionCall Parameters IN
				createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
				createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
				// DEBUG genFunctionCall Service call
				createLogFileService.execute(createLogFileDTO);
				// DEBUG genFunctionCall Parameters OUT
				// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Subfile Control (Generated:37)
	 */
    private StepResult usrValidateSubfileControl(EditWardState dto) {
        StepResult result = NO_ACTION;

        try {
        		CreateLogFileDTO createLogFileDTO;
				//switchSUB 37 SUB    
				//switchBLK 1000014 BLK CAS
				//switchSUB 1000014 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
					// CTL.*CMD key is *Cancel
					//switchBLK 1000056 BLK ACT
					// DEBUG genFunctionCall 1000057 ACT PAR.*Return code = CND.E
					dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
					//switchBLK 1000017 BLK ACT
					// DEBUG genFunctionCall 1000018 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
				//switchBLK 1000359 BLK TXT
				// 
				//switchBLK 1000360 BLK ACT
				// DEBUG genFunctionCall 1000361 ACT LCL.Log Function Type = CON.EDTFIL
				dto.setLclLogFunctionType("EDTFIL");
				//switchBLK 1000364 BLK ACT
				// DEBUG genFunctionCall 1000365 ACT LCL.Log User Point = CON.Validate subfile control
				dto.setLclLogUserPoint("Validate subfile control");
				//switchBLK 1000374 BLK ACT
				// DEBUG genFunctionCall 1000375 ACT Create Log File - Log File  *
				// DEBUG genFunctionCall ServiceDtoVariable
				createLogFileDTO = new CreateLogFileDTO();
				// DEBUG genFunctionCall Parameters IN
				createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
				createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
				// DEBUG genFunctionCall Service call
				createLogFileService.execute(createLogFileDTO);
				// DEBUG genFunctionCall Parameters OUT
				// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Subfile Record Fields (Generated:1496)
	 */
    private StepResult usrValidateSubfileRecordFields(EditWardState dto, EditWardGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        		CreateLogFileDTO createLogFileDTO;
				//switchSUB 1496 SUB    
				//switchBLK 1000001 BLK CAS
				//switchSUB 1000001 BLK CAS
				if (gdo.get_SysSelected().equals("Display Patients")) {
					// RCD.*SFLSEL is Display Patients
					//switchBLK 1000004 BLK ACT
					// DEBUG genFunctionCall 1000005 ACT DSPF Display Patients/Wrd - Patient  *
					// TODO: XEKDM-779 quick fix
					// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
					// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
					// TODO: XEKDM-779 better solution required
					{
					//TODO: split
					DspfDisplayPatientsWrdParams dspfDisplayPatientsWrdParams = new DspfDisplayPatientsWrdParams();
					BeanUtils.copyProperties(dto, dspfDisplayPatientsWrdParams);
					result = callService(DspfDisplayPatientsWrdService.class, dspfDisplayPatientsWrdParams).thenCall(serviceDspfDisplayPatientsWrd);
					}
					//switchBLK 1000030 BLK CAS
					//switchSUB 1000030 BLK CAS
					if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
						// LCL.*Return code is E
						//switchBLK 1000065 BLK ACT
						// DEBUG genFunctionCall 1000066 ACT PGM.*Defer confirm = CND.Proceed to confirm
						dto.set_SysDeferConfirm(DeferConfirmEnum.fromCode("N"));
						//switchBLK 1000033 BLK ACT
						// DEBUG genFunctionCall 1000034 ACT PGM.*Reload subfile = CND.*YES
						dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
						//switchBLK 1000063 BLK ACT
						// DEBUG genFunctionCall 1000064 ACT <-- *QUIT
						// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
					}//switchSUB 1000039 SUB    
					 else {
						// *OTHERWISE
						//switchBLK 1000041 BLK ACT
						// DEBUG genFunctionCall 1000042 ACT Exit program - return code CND.*Normal
						// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
					}
					//switchBLK 1000008 BLK ACT
					// DEBUG genFunctionCall 1000009 ACT PGM.*Defer confirm = CND.Defer confirm
					dto.set_SysDeferConfirm(DeferConfirmEnum.fromCode("Y"));
				}
				//switchBLK 1000381 BLK TXT
				// 
				//switchBLK 1000382 BLK ACT
				// DEBUG genFunctionCall 1000383 ACT LCL.Log Function Type = CON.EDTFIL
				dto.setLclLogFunctionType("EDTFIL");
				//switchBLK 1000386 BLK ACT
				// DEBUG genFunctionCall 1000387 ACT LCL.Log User Point = CON.Validate subfile record
				dto.setLclLogUserPoint("Validate subfile record");
				//switchBLK 1000390 BLK ACT
				// DEBUG genFunctionCall 1000391 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.fields,CND.*One)
				dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "fields"));
				//switchBLK 1000396 BLK ACT
				// DEBUG genFunctionCall 1000397 ACT Create Log File - Log File  *
				// DEBUG genFunctionCall ServiceDtoVariable
				createLogFileDTO = new CreateLogFileDTO();
				// DEBUG genFunctionCall Parameters IN
				createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
				createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
				// DEBUG genFunctionCall Service call
				try {
				    createLogFileService.execute(createLogFileDTO);
				} catch (ServiceException se) {
					//Todo Commented code 
				    //e.reject("ServiceException");
				}
				// DEBUG genFunctionCall Parameters OUT
				// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:1449)
	 */
    private StepResult usrSubfileRecordFunctionFields(EditWardState dto, EditWardGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        		CreateLogFileDTO createLogFileDTO;
				//switchSUB 1449 SUB    
				//switchBLK 1000150 BLK ACT
				// DEBUG genFunctionCall 1000151 ACT RCD.Changed User = JOB.*USER
				gdo.setChangedUser(job.getUser());
				//switchBLK 1000154 BLK ACT
				// DEBUG genFunctionCall 1000155 ACT RCD.Changed Date = JOB.*Job date
				gdo.setChangedDate(LocalDate.now());
				//switchBLK 1000158 BLK ACT
				// DEBUG genFunctionCall 1000159 ACT RCD.Changed Time = JOB.*Job time
				gdo.setChangedTime(LocalTime.now());
				//switchBLK 1000407 BLK TXT
				// 
				//switchBLK 1000408 BLK ACT
				// DEBUG genFunctionCall 1000409 ACT LCL.Log Function Type = CON.EDTFIL
				dto.setLclLogFunctionType("EDTFIL");
				//switchBLK 1000412 BLK ACT
				// DEBUG genFunctionCall 1000413 ACT LCL.Log User Point = CON.Subfile record function
				dto.setLclLogUserPoint("Subfile record function");
				//switchBLK 1000416 BLK ACT
				// DEBUG genFunctionCall 1000417 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.fields,CND.*One)
				dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "fields"));
				//switchBLK 1000422 BLK ACT
				// DEBUG genFunctionCall 1000423 ACT Create Log File - Log File  *
				// DEBUG genFunctionCall ServiceDtoVariable
				createLogFileDTO = new CreateLogFileDTO();
				// DEBUG genFunctionCall Parameters IN
				createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
				createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
				// DEBUG genFunctionCall Service call
				createLogFileService.execute(createLogFileDTO);
				// DEBUG genFunctionCall Parameters OUT
				// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Subfile Record Relations (Generated:1171)
	 */
    private StepResult usrValidateSubfileRecordRelations(EditWardState dto, EditWardGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        		CreateLogFileDTO createLogFileDTO;
				DeleteWardDTO deleteWardDTO;
				//switchSUB 1171 SUB    
				//switchBLK 1000164 BLK CAS
				//switchSUB 1000164 BLK CAS
				if ((gdo.get_SysSelected().equals("*Delete#1")) || (gdo.get_SysSelected().equals("*Delete#2"))) {
					// RCD.*SFLSEL is *Delete
					//switchBLK 1000194 BLK ACT
					// DEBUG genFunctionCall 1000195 ACT Delete Ward - Ward  *
					// DEBUG genFunctionCall ServiceDtoVariable
					deleteWardDTO = new DeleteWardDTO();
					// DEBUG genFunctionCall Parameters IN
					deleteWardDTO.setHospitalCode(gdo.getHospitalCode());
					deleteWardDTO.setWardCode(gdo.getWardCode());
					// DEBUG genFunctionCall Service call
					deleteWardService.execute(deleteWardDTO);
					// DEBUG genFunctionCall Parameters OUT
					// DEBUG genFunctionCall Parameters DONE
				}
				//switchBLK 1000433 BLK TXT
				// 
				//switchBLK 1000434 BLK ACT
				// DEBUG genFunctionCall 1000435 ACT LCL.Log Function Type = CON.EDTFIL
				dto.setLclLogFunctionType("EDTFIL");
				//switchBLK 1000438 BLK ACT
				// DEBUG genFunctionCall 1000439 ACT LCL.Log User Point = CON.Validate subfile record
				dto.setLclLogUserPoint("Validate subfile record");
				//switchBLK 1000442 BLK ACT
				// DEBUG genFunctionCall 1000443 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.relations,CND.*One)
				dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "relations"));
				//switchBLK 1000448 BLK ACT
				// DEBUG genFunctionCall 1000449 ACT Create Log File - Log File  *
				// DEBUG genFunctionCall ServiceDtoVariable
				createLogFileDTO = new CreateLogFileDTO();
				// DEBUG genFunctionCall Parameters IN
				createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
				createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
				// DEBUG genFunctionCall Service call
				createLogFileService.execute(createLogFileDTO);
				// DEBUG genFunctionCall Parameters OUT
				// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Extra Processing After DBF Update (Generated:1442)
	 */
    private StepResult usrExtraProcessingAfterDBFUpdate(EditWardState dto) {
        StepResult result = NO_ACTION;

        try {
        		CreateLogFileDTO createLogFileDTO;
				//switchSUB 1442 SUB    
				//switchBLK 1000198 BLK ACT
				// DEBUG genFunctionCall 1000199 ACT PAR.*Return code = CND.*Normal
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000459 BLK TXT
				// 
				//switchBLK 1000460 BLK ACT
				// DEBUG genFunctionCall 1000461 ACT LCL.Log Function Type = CON.EDTFIL
				dto.setLclLogFunctionType("EDTFIL");
				//switchBLK 1000464 BLK ACT
				// DEBUG genFunctionCall 1000465 ACT LCL.Log User Point = CON.Extra processing after
				dto.setLclLogUserPoint("Extra processing after");
				//switchBLK 1000468 BLK ACT
				// DEBUG genFunctionCall 1000469 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.DBF update,CND.*One)
				dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "DBF update"));
				//switchBLK 1000474 BLK ACT
				// DEBUG genFunctionCall 1000475 ACT Create Log File - Log File  *
				// DEBUG genFunctionCall ServiceDtoVariable
				createLogFileDTO = new CreateLogFileDTO();
				// DEBUG genFunctionCall Parameters IN
				createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
				createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
				// DEBUG genFunctionCall Service call
				createLogFileService.execute(createLogFileDTO);
				// DEBUG genFunctionCall Parameters OUT
				// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Generated:1420)
	 */
    private StepResult usrProcessCommandKeys(EditWardState dto) {
        StepResult result = NO_ACTION;

        try {
        		CreateLogFileDTO createLogFileDTO;
				//switchSUB 1420 SUB    
				//switchBLK 1000204 BLK CAS
				//switchSUB 1000204 BLK CAS
				if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("15")) {
					// CTL.*CMD key is CF15
					//switchBLK 1000207 BLK ACT
					// DEBUG genFunctionCall 1000208 ACT DSP All Hospitals - Hospital  *
					// TODO: XEKDM-779 quick fix
					// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
					// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
					// TODO: XEKDM-779 better solution required
					{
					//TODO: split
					DspAllHospitalsParams dspAllHospitalsParams = new DspAllHospitalsParams();
					BeanUtils.copyProperties(dto, dspAllHospitalsParams);
					result = StepResult.callService(DspAllHospitalsService.class, dspAllHospitalsParams).thenCall(serviceDspAllHospitals);
					}
				}
				//switchBLK 1000485 BLK TXT
				// 
				//switchBLK 1000486 BLK ACT
				// DEBUG genFunctionCall 1000487 ACT LCL.Log Function Type = CON.EDTFIL
				dto.setLclLogFunctionType("EDTFIL");
				//switchBLK 1000490 BLK ACT
				// DEBUG genFunctionCall 1000491 ACT LCL.Log User Point = CON.Process command keys
				dto.setLclLogUserPoint("Process command keys");
				//switchBLK 1000500 BLK ACT
				// DEBUG genFunctionCall 1000501 ACT Create Log File - Log File  *
				// DEBUG genFunctionCall ServiceDtoVariable
				createLogFileDTO = new CreateLogFileDTO();
				// DEBUG genFunctionCall Parameters IN
				createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
				createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
				// DEBUG genFunctionCall Service call
				createLogFileService.execute(createLogFileDTO);
				// DEBUG genFunctionCall Parameters OUT
				// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Generated:1262)
	 */
    private StepResult usrExitProgramProcessing(EditWardState dto) {
        StepResult result = NO_ACTION;
        
        try {
        		CreateLogFileDTO createLogFileDTO;
				//switchSUB 1262 SUB    
				//switchBLK 1000044 BLK CAS
				//switchSUB 1000044 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
					// CTL.*CMD key is *Exit
					//switchBLK 1000047 BLK ACT
					// DEBUG genFunctionCall 1000048 ACT PAR.*Return code = CND.*Normal
					dto.setReturnCode(ReturnCodeEnum.fromCode(""));
					//switchBLK 1000053 BLK ACT
					// DEBUG genFunctionCall 1000054 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
				//switchBLK 1000507 BLK TXT
				// 
				//switchBLK 1000508 BLK ACT
				// DEBUG genFunctionCall 1000509 ACT LCL.Log Function Type = CON.EDTFIL
				dto.setLclLogFunctionType("EDTFIL");
				//switchBLK 1000512 BLK ACT
				// DEBUG genFunctionCall 1000513 ACT LCL.Log User Point = CON.Exit program processing
				dto.setLclLogUserPoint("Exit program processing");
				//switchBLK 1000522 BLK ACT
				// DEBUG genFunctionCall 1000523 ACT Create Log File - Log File  *
				// DEBUG genFunctionCall ServiceDtoVariable
				createLogFileDTO = new CreateLogFileDTO();
				// DEBUG genFunctionCall Parameters IN
				createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
				createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
				// DEBUG genFunctionCall Service call
				createLogFileService.execute(createLogFileDTO);
				// DEBUG genFunctionCall Parameters OUT
				// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */

    /**
     * DspfDisplayPatientsWrdService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspfDisplayPatientsWrd(EditWardState state, DspfDisplayPatientsWrdParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        EditWardParams params=new EditWardParams();
        BeanUtils.copyProperties(state, params);
        result=execute(state, params);
        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }

    /**
     * DspAllHospitalsService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspAllHospitals(EditWardState state, DspAllHospitalsParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }
//
//    /**
//     * CreateWardService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateWard(EditWardState state, CreateWardParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * DeleteWardService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceDeleteWard(EditWardState state, DeleteWardParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ChangeWardService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChangeWard(EditWardState state, ChangeWardParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(EditWardState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(EditWardState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * QuitService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceQuit(EditWardState state, QuitParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvNbrWardHospitalService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvNbrWardHospital(EditWardState state, RtvNbrWardHospitalParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * CreateLogFileService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateLogFile(EditWardState state, CreateLogFileParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ConcatService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceConcat(EditWardState state, ConcatParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//


}
