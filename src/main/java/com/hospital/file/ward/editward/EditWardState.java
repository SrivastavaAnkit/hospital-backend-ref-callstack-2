package com.hospital.file.ward.editward;

import com.hospital.model.DeferConfirmEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;


/**
 * State for file 'Ward' (TSADREP) and function 'Edit Ward' (TSALEFR).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 *
 */

public class EditWardState extends EditWardDTO {
	private static final long serialVersionUID = -7246534147819680450L;

	// Local fields
	private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");
	private UsrReturnCodeEnum lclUsrReturnCode = null;
	private String lclLogFunctionType = "";
	private String lclLogUserPoint = "";

	// System fields
	private long _sysScanLimit = 0L;
	private DeferConfirmEnum _sysDeferConfirm = null;
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;


    public EditWardState() {

    }

	public void setLclLogFunctionType(String logFunctionType) {
    	this.lclLogFunctionType = logFunctionType;
    }

    public String getLclLogFunctionType() {
    	return lclLogFunctionType;
    }
	public void setLclLogUserPoint(String logUserPoint) {
    	this.lclLogUserPoint = logUserPoint;
    }

    public String getLclLogUserPoint() {
    	return lclLogUserPoint;
    }
	public void setLclReturnCode(ReturnCodeEnum returnCode) {
    	this.lclReturnCode = returnCode;
    }

    public ReturnCodeEnum getLclReturnCode() {
    	return lclReturnCode;
    }
	public void setLclUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
    	this.lclUsrReturnCode = usrReturnCode;
    }

    public UsrReturnCodeEnum getLclUsrReturnCode() {
    	return lclUsrReturnCode;
    }

	public void set_SysDeferConfirm(DeferConfirmEnum deferConfirm) {
    	_sysDeferConfirm = deferConfirm;
    }

    public DeferConfirmEnum get_SysDeferConfirm() {
    	return _sysDeferConfirm;
    }

	public void set_SysScanLimit(long scanLimit) {
    	_sysScanLimit = scanLimit;
    }

    public long get_SysScanLimit() {
    	return _sysScanLimit;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }
    }