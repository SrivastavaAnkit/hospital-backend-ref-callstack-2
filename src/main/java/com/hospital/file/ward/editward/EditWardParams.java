package com.hospital.file.ward.editward;

import java.io.Serializable;


/**
 * Params for resource: EditWard (TSALEFR).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 */
public class EditWardParams implements Serializable {
	private static final long serialVersionUID = 7497835678623026421L;

	private String hospitalCode = "";


	public String getHospitalCode() {
		return hospitalCode;
	}
	
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

}