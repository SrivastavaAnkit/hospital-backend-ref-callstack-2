package com.hospital.file.ward.dspwardsperhospital2;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;



/**
 * Gdo for file 'Ward' (TSADREP) and function 'DSP Wards per Hospital 2' (TSBADFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */
public class DspWardsPerHospital2GDO implements Serializable {
	private static final long serialVersionUID = 8436929918114940500L;

	private long version = 0;
    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private String hospitalCode = "";
	private String wardCode = "";
	private String wardName = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;
	private long patientCount = 0L;
	private String addedUser = "";

	public DspWardsPerHospital2GDO() {

	}
  //Todo Parameters are not available in repo.
   	public DspWardsPerHospital2GDO(/*long version,*/ String hospitalCode, String wardCode, String wardName/*, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime, String addedUser*/) {
		this.version = version;
		this.hospitalCode = hospitalCode;
		this.wardCode = wardCode;
		this.wardName = wardName;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
		this.addedUser = addedUser;
	}


	public void setVersion(long version) {
		this.version = version;
	}

    public long getVersion() {
		return version;
    }

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setHospitalCode(String hospitalCode) {
    	this.hospitalCode = hospitalCode;
    }

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setWardCode(String wardCode) {
    	this.wardCode = wardCode;
    }

	public String getWardCode() {
		return wardCode;
	}

	public void setWardName(String wardName) {
    	this.wardName = wardName;
    }

	public String getWardName() {
		return wardName;
	}

	public void setAddedDate(LocalDate addedDate) {
    	this.addedDate = addedDate;
    }

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
    	this.addedTime = addedTime;
    }

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
    	this.changedUser = changedUser;
    }

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
    	this.changedDate = changedDate;
    }

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
    	this.changedTime = changedTime;
    }

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void setPatientCount(long patientCount) {
    	this.patientCount = patientCount;
    }

	public long getPatientCount() {
		return patientCount;
	}

	public void setAddedUser(String addedUser) {
    	this.addedUser = addedUser;
    }

	public String getAddedUser() {
		return addedUser;
	}

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}