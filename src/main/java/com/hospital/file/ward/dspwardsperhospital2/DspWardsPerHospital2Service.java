package com.hospital.file.ward.dspwardsperhospital2;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
 
import com.hospital.support.JobContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.hospital.file.ward.Ward;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.file.ward.WardRepository;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsService;
import com.hospital.file.patient.dspfdisplaypatientwrd2.DspfDisplayPatientWrd2Service;
import com.hospital.file.patient.rtvpatientsperward.RtvPatientsPerWardService;
import com.hospital.file.ward.editward.EditWardService;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsDTO;
import com.hospital.file.patient.dspfdisplaypatientwrd2.DspfDisplayPatientWrd2DTO;
import com.hospital.file.patient.rtvpatientsperward.RtvPatientsPerWardDTO;
import com.hospital.file.ward.editward.EditWardDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsParams;
import com.hospital.file.patient.dspfdisplaypatientwrd2.DspfDisplayPatientWrd2Params;
import com.hospital.file.ward.editward.EditWardParams;

import com.hospital.common.exception.ServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

	
/**
 * Service implementation for resource: DspWardsPerHospital2 (TSBADFR).
 *
 * @author X2EGenerator
 */
@Service
public class DspWardsPerHospital2Service extends AbstractService<DspWardsPerHospital2Service, DspWardsPerHospital2State> {
    
	@Autowired
	private JobContext job;

    @Autowired
    private WardRepository wardRepository;
    
    @Autowired
    private RtvPatientsPerWardService rtvPatientsPerWardService;
    

    @Autowired
    private MessageSource messageSource;
    
	public static final String SCREEN_CTL = "dspWardsPerHospital2";
    public static final String SCREEN_RCD = "DspWardsPerHospital2.rcd";

    private final Step execute = define("execute", DspWardsPerHospital2Params.class, this::execute);
    private final Step response = define("response", DspWardsPerHospital2DTO.class, this::processResponse);
	private final Step serviceDspfDisplayPatientWrd2 = define("serviceDspfDisplayPatientWrd2",DspfDisplayPatientWrd2Params.class, this::processServiceDspfDisplayPatientWrd2);
	private final Step serviceDspAllHospitals = define("serviceDspAllHospitals",DspAllHospitalsParams.class, this::processServiceDspAllHospitals);
	//private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
	private final Step serviceEditWard = define("serviceEditWard",EditWardParams.class, this::processServiceEditWard);
	//private final Step serviceRtvPatientsPerWard = define("serviceRtvPatientsPerWard",RtvPatientsPerWardParams.class, this::processServiceRtvPatientsPerWard);
	//private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
	//private final Step serviceQuit = define("serviceQuit",QuitParams.class, this::processServiceQuit);
	//private final Step serviceRtvcnd = define("serviceRtvcnd",RtvcndParams.class, this::processServiceRtvcnd);
	
    
    @Autowired
    public DspWardsPerHospital2Service() {
        super(DspWardsPerHospital2Service.class, DspWardsPerHospital2State.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * DSPFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(DspWardsPerHospital2State state, DspWardsPerHospital2Params params) {
    	StepResult result = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        result = usrInitializeProgram(state);

        result = mainLoop(state, params);

        return result;
    }

    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult mainLoop(DspWardsPerHospital2State state, DspWardsPerHospital2Params params) {
        StepResult result = NO_ACTION;

        result = loadFirstSubfilePage(state, params);

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        result = conductScreenConversation(state);

        return result;
    }

    /**
     * SCREEN  initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult loadFirstSubfilePage(DspWardsPerHospital2State state, DspWardsPerHospital2Params params) {
    	StepResult result = NO_ACTION;

    	result = usrInitializeSubfileControl(state, params);

		dbfReadFirstDataRecord(state);
		if (state.getPageDto() != null && state.getPageDto().getSize() > 0) {
		    result = loadNextSubfilePage(state);
		}

        return result;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     * @return
     */
    private StepResult loadNextSubfilePage(DspWardsPerHospital2State state) {
    	StepResult result = NO_ACTION;

    	List<DspWardsPerHospital2GDO> list = state.getPageDto().getContent();
        for (DspWardsPerHospital2GDO gdo : list) {
            gdo.set_SysRecordSelected(RecordSelectedEnum._STA_YES);
            result = usrInitializeSubfileRecordFromDBFRecord(state, gdo);
//             TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
            if (gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
//                TODO:writeSubfileRecord(state);  // synon built-in function
            }
        }

        return result;
    }

    /**
     * SCREEN  initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductScreenConversation(DspWardsPerHospital2State state) {
        StepResult result = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            DspWardsPerHospital2DTO model = new DspWardsPerHospital2DTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_CTL, model).thenCall(response);
        }

        return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processResponse(DspWardsPerHospital2State state, DspWardsPerHospital2DTO model) {
    	StepResult result = NO_ACTION;

        // update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            result = closedown(state);
        } else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
        } else if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            //TODO:processHelpRequest(state);//synon built-in function
        }
        else if (CmdKeyEnum.isNextPage(state.get_SysCmdKey())) {
        	DspWardsPerHospital2Params params=new DspWardsPerHospital2Params();
        	   
        	dbfReadNextPageRecord(state);
        	BeanUtils.copyProperties(state, model);
        	result = callScreen(SCREEN_CTL, model).thenCall(response);
        	//result = execute(state, params);            
        } else {
            result = processScreen(state);
        }

        return result;
    }

    /**
     * SCREEN process screen.
     * @param state - Service state class.
     * @return
     */
    private StepResult processScreen(DspWardsPerHospital2State state) {
    	StepResult result = NO_ACTION;

        result = usrProcessSubfilePreConfirm(state);
		if(result!=StepResult.NO_ACTION) {        	
        	return result;
        }
        if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
        	return closedown(state);
        } else {
        	if(!state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
        		return closedown(state);
	        } else {
//	        	if(!state.get_SysProgramConfirm().getCode().equals(ProgramConfirmEnum._STA_YES.getCode())) {
//	        		return closedown(state);
//		        } else {
//		        	result = usrProcessCommandKeys(state);
//		        }

	        	result = usrProcessSubfileControlPostConfirm(state);
	        	for (DspWardsPerHospital2GDO gdo : state.getPageDto().getContent()) {
	                if(gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
	                	result = usrProcessSubfileRecordPostConfirm(state, gdo);
//	                  TODO:writeSubfileRecord(state);   // synon built-in function
	                }
	            }
	        	result = usrFinalProcessingPostConfirm(state);
	        	result = usrProcessCommandKeys(state);
	        }
        }

        result = conductScreenConversation(state);

        return result;
    }

    /**
     * SCREEN Process subfile Pre Confirm.
     * @param state - Service state class.
     * @return
     */
    private StepResult usrProcessSubfilePreConfirm(DspWardsPerHospital2State state) {
    	StepResult result = NO_ACTION;

    	result = usrSubfileControlFunctionFields(state);
    	result = usrProcessSubfileControlPreConfirm(state);
		//Todo comment Fix Me.
    	//if(state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
    		for (DspWardsPerHospital2GDO gdo : state.getPageDto().getContent()) {
                if(gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
                	result = usrSubfileRecordFunctionFields(state, gdo);
                    result = usrProcessSubfileRecordPreConfirm(state, gdo);
//                  TODO:writeSubfileRecord(state);   // synon built-in function
                }
            }
    	//}
    		//TODO: fIXME 
    		if(result!=StepResult.NO_ACTION) {        	
            	return result;
            }
        result = usrFinalProcessingPreConfirm(state);

        return result;
    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(DspWardsPerHospital2State state) {
        StepResult result = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        result = usrExitProgramProcessing(state);

        return result;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(DspWardsPerHospital2State state) {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadNextPageRecord(DspWardsPerHospital2State state)
	{
		//Todo Please FixMe why page size increment ?
		//state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    private void dbfReadDataRecord(DspWardsPerHospital2State state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = new PageRequest(state.getPage(), state.getSize());
        }
        else {
            pageable = new PageRequest(state.getPage(), state.getSize(), new Sort(sortOrders));
        }

        RestResponsePage<DspWardsPerHospital2GDO> pageDto = wardRepository.dspWardsPerHospital2(state.getHospitalCode(), state.getCountry(), state.getCountryName(), state.getTelephoneNumber(), pageable);
        state.setPageDto(pageDto);
    }
    
	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(DspWardsPerHospital2State dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 20 SUB    
			//switchBLK 1000189 BLK ACT
			// DEBUG genFunctionCall 1000190 ACT PGM.*Scan limit = CND.High Value
			dto.set_SysScanLimit(9999999);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Control (Generated:182)
	 */
    private StepResult usrInitializeSubfileControl(DspWardsPerHospital2State dto, DspWardsPerHospital2Params params) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 182 SUB    
			//switchBLK 1000017 BLK ACT
			// DEBUG genFunctionCall 1000018 ACT CTL.Country = PAR.Country
			dto.setCountry(dto.getCountry());
			//switchBLK 1000011 BLK ACT
			// DEBUG genFunctionCall 1000012 ACT CTL.Country Name = PAR.Country Name
			dto.setCountryName(dto.getCountryName());
			//switchBLK 1000023 BLK ACT
			// DEBUG genFunctionCall 1000024 ACT CTL.Telephone Number = PAR.Telephone Number
			dto.setTelephoneNumber(dto.getTelephoneNumber());
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrInitializeSubfileRecordFromDBFRecord(DspWardsPerHospital2State dto, DspWardsPerHospital2GDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	RtvPatientsPerWardDTO rtvPatientsPerWardDTO;
			//switchSUB 41 SUB    
			//switchBLK 1000044 BLK ACT
			// DEBUG genFunctionCall 1000045 ACT RTV Patients per ward - Patient  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvPatientsPerWardDTO = new RtvPatientsPerWardDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvPatientsPerWardDTO.setWardCode(gdo.getWardCode());
			// DEBUG genFunctionCall Service call
			rtvPatientsPerWardService.execute(rtvPatientsPerWardDTO);
			// DEBUG genFunctionCall Parameters OUT
			gdo.setPatientCount(rtvPatientsPerWardDTO.getCount());
			// DEBUG genFunctionCall Parameters DONE
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:175)
	 */
    private StepResult usrSubfileControlFunctionFields(DspWardsPerHospital2State dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 175 SUB    
			//switchBLK 1000195 BLK ACT
			// DEBUG genFunctionCall 1000196 ACT CTL.Country Name = Condition name of CTL.Country
			dto.setCountryName(dto.getCountry().getDescription()); // Retrieve condition
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Control (Pre-Confirm) (Generated:72)
	 */
    private StepResult usrProcessSubfileControlPreConfirm(DspWardsPerHospital2State dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 72 SUB    
			//switchBLK 1000060 BLK CAS
			//switchSUB 1000060 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("09"))) {
				// CTL.*CMD key is *Add
				//switchBLK 1000051 BLK ACT
				// DEBUG genFunctionCall 1000052 ACT Edit Ward - Ward  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				EditWardParams editWardParams = new EditWardParams();
				BeanUtils.copyProperties(dto, editWardParams);
				//result = StepResult.callService(EditWardService.class, editWardParams);//.thenCall(serviceEditWard)
				}
				//switchBLK 1000121 BLK CAS
				//switchSUB 1000121 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000124 BLK ACT
					// DEBUG genFunctionCall 1000125 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000183 BLK ACT
					// DEBUG genFunctionCall 1000184 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000128 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000130 BLK ACT
					// DEBUG genFunctionCall 1000131 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
			//switchBLK 1000172 BLK TXT
			// 
			//switchBLK 1000169 BLK CAS
			//switchSUB 1000169 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// CTL.*CMD key is *Cancel
				//switchBLK 1000151 BLK ACT
				// DEBUG genFunctionCall 1000152 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000148 BLK ACT
				// DEBUG genFunctionCall 1000149 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:170)
	 */
    private StepResult usrSubfileRecordFunctionFields(DspWardsPerHospital2State dto, DspWardsPerHospital2GDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 170 SUB    
			//switchBLK 1000201 BLK ACT
			// DEBUG genFunctionCall 1000202 ACT RCD.Added User = JOB.*USER
			gdo.setAddedUser(job.getUser());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Record (Pre-Confirm) (Generated:101)
	 */
    private StepResult usrProcessSubfileRecordPreConfirm(DspWardsPerHospital2State dto, DspWardsPerHospital2GDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 101 SUB    
			//switchBLK 1000031 BLK CAS
			//switchSUB 1000031 BLK CAS
			if (gdo.get_SysSelected().equals("Medication")) {
				// RCD.*SFLSEL is Medication
				//switchBLK 1000034 BLK ACT
				// DEBUG genFunctionCall 1000035 ACT DSPF Display Patient/Wrd2 - Patient  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				//Todo extra curly braces ..
				//{
				//TODO: split
				DspfDisplayPatientWrd2Params dspfDisplayPatientWrd2Params = new DspfDisplayPatientWrd2Params();
				BeanUtils.copyProperties(dto, dspfDisplayPatientWrd2Params);
				result = StepResult.callService(DspfDisplayPatientWrd2Service.class, dspfDisplayPatientWrd2Params).thenCall(serviceDspfDisplayPatientWrd2);
				//}
				//switchBLK 1000082 BLK CAS
				//switchSUB 1000082 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000085 BLK ACT
					// DEBUG genFunctionCall 1000086 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000185 BLK ACT
					// DEBUG genFunctionCall 1000186 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000091 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000093 BLK ACT
					// DEBUG genFunctionCall 1000094 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
			//switchBLK 1000038 BLK CAS
			//switchSUB 1000038 BLK CAS
			if (gdo.get_SysSelected().equals("Change record")) {
				// RCD.*SFLSEL is Change record
				//switchBLK 1000041 BLK ACT
				// DEBUG genFunctionCall 1000042 ACT Edit Ward - Ward  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				//Todo extra curly braces ..
				//{
				//TODO: split
				EditWardParams editWardParams = new EditWardParams();
				BeanUtils.copyProperties(dto, editWardParams);
				//Todo uncomment this line.
				result = StepResult.callService(EditWardService.class, editWardParams).thenCall(serviceEditWard);
				//}
				//switchBLK 1000097 BLK CAS
				//switchSUB 1000097 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000100 BLK ACT
					// DEBUG genFunctionCall 1000101 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000187 BLK ACT
					// DEBUG genFunctionCall 1000188 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000104 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000106 BLK ACT
					// DEBUG genFunctionCall 1000107 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Final processing (Pre-confirm) (Generated:222)
	 */
    private StepResult usrFinalProcessingPreConfirm(DspWardsPerHospital2State dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 222 SUB    
			//switchBLK 1000207 BLK CAS
			//switchSUB 1000207 BLK CAS
			if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("15")) {
				// CTL.*CMD key is CF15
				//switchBLK 1000210 BLK ACT
				// DEBUG genFunctionCall 1000211 ACT DSP All Hospitals - Hospital  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				DspAllHospitalsParams dspAllHospitalsParams = new DspAllHospitalsParams();
				BeanUtils.copyProperties(dto, dspAllHospitalsParams);
				result = StepResult.callService(DspAllHospitalsService.class, dspAllHospitalsParams).thenCall(serviceDspAllHospitals);
				}
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process subfile control (Post-confirm) (Generated:225)
	 */
    private StepResult usrProcessSubfileControlPostConfirm(DspWardsPerHospital2State dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 225 SUB    
			// Unprocessed SUB 225 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Record (Post-Confirm Pass) (Generated:209)
	 */
    private StepResult usrProcessSubfileRecordPostConfirm(DspWardsPerHospital2State dto, DspWardsPerHospital2GDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 209 SUB    
			// Unprocessed SUB 209 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Final processing (Post-confirm) (Generated:228)
	 */
    private StepResult usrFinalProcessingPostConfirm(DspWardsPerHospital2State dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 228 SUB    
			// Unprocessed SUB 228 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Generated:140)
	 */
    private StepResult usrProcessCommandKeys(DspWardsPerHospital2State dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 140 SUB    
			//switchBLK 1000140 BLK CAS
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(DspWardsPerHospital2State dto) {
        StepResult result = NO_ACTION;
        
        try {
        	//switchSUB 132 SUB    
			//switchBLK 1000048 BLK CAS
			//switchSUB 1000048 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// CTL.*CMD key is *Exit
				//switchBLK 1000176 BLK ACT
				// DEBUG genFunctionCall 1000177 ACT PAR.*Return code = CND.*Normal
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000076 BLK ACT
				// DEBUG genFunctionCall 1000077 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */

    /**
     * DspfDisplayPatientWrd2Service returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspfDisplayPatientWrd2(DspWardsPerHospital2State state, DspfDisplayPatientWrd2Params serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }

    /**
     * DspAllHospitalsService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspAllHospitals(DspWardsPerHospital2State state, DspAllHospitalsParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }
//
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(DspWardsPerHospital2State state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * EditWardService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
    //Todo uncomment 
//    private StepResult processServiceEditWard(DspWardsPerHospital2State state, EditWardParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(DspWardsPerHospital2State state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * EditWardService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
    //Todo uncomment 
    private StepResult processServiceEditWard(DspWardsPerHospital2State state, EditWardParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;
        DspWardsPerHospital2Params params=new DspWardsPerHospital2Params();
        BeanUtils.copyProperties(state, params);
        result=execute(state, params);
        return result;
    }
////
//    /**
//     * RtvPatientsPerWardService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvPatientsPerWard(DspWardsPerHospital2State state, RtvPatientsPerWardParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(DspWardsPerHospital2State state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * QuitService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceQuit(DspWardsPerHospital2State state, QuitParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvcndService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvcnd(DspWardsPerHospital2State state, RtvcndParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//


}
