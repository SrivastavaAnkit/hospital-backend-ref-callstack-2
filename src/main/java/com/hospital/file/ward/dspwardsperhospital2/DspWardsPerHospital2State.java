package com.hospital.file.ward.dspwardsperhospital2;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;


/**
 * State for file 'Ward' (TSADREP) and function 'DSP Wards per Hospital 2' (TSBADFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class DspWardsPerHospital2State extends DspWardsPerHospital2DTO {
	private static final long serialVersionUID = -3485254582979575501L;

	// Local fields
	private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");

	// System fields
	private long _sysScanLimit = 0L;
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;


    public DspWardsPerHospital2State() {

    }

	public void setLclReturnCode(ReturnCodeEnum returnCode) {
    	this.lclReturnCode = returnCode;
    }

    public ReturnCodeEnum getLclReturnCode() {
    	return lclReturnCode;
    }

	public void set_SysScanLimit(long scanLimit) {
    	_sysScanLimit = scanLimit;
    }

    public long get_SysScanLimit() {
    	return _sysScanLimit;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }
    }