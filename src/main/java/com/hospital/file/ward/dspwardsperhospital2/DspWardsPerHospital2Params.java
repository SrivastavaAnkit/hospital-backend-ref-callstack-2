package com.hospital.file.ward.dspwardsperhospital2;

import java.io.Serializable;

import com.hospital.model.CountryEnum;



/**
 * Params for resource: DspWardsPerHospital2 (TSBADFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
public class DspWardsPerHospital2Params implements Serializable {
	private static final long serialVersionUID = 4183970881911279603L;

	private String hospitalCode = "";
	private CountryEnum country = null;
	private String countryName = "";
	private long telephoneNumber = 0L;


	public String getHospitalCode() {
		return hospitalCode;
	}
	
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	
	public CountryEnum getCountry() {
		return country;
	}
	
	public void setCountry(CountryEnum country) {
		this.country = country;
	}
	
	public void setCountry(String country) {
		setCountry(CountryEnum.valueOf(country));
	}
	
	public String getCountryName() {
		return countryName;
	}
	
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
	public long getTelephoneNumber() {
		return telephoneNumber;
	}
	
	public void setTelephoneNumber(long telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	
	public void setTelephoneNumber(String telephoneNumber) {
		setTelephoneNumber(Long.parseLong(telephoneNumber));
	}

}