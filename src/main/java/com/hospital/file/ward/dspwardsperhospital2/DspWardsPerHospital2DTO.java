package com.hospital.file.ward.dspwardsperhospital2;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.common.state.BaseDTO;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;

/**
 * Dto for file 'Ward' (TSADREP) and function 'DSP Wards per Hospital 2' (TSBADFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class DspWardsPerHospital2DTO extends BaseDTO {
	private static final long serialVersionUID = -5603206300384673126L;
	private long version = 0;

    private RestResponsePage<DspWardsPerHospital2GDO> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";

	private String hospitalCode = "";
	private CountryEnum country = null;
	private String countryName = "";
	private String hospitalName = "";
	private long telephoneNumber = 0L;
	private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");


	private DspWardsPerHospital2GDO gdo;

    public DspWardsPerHospital2DTO() {

    }

	public DspWardsPerHospital2DTO(long version, String hospitalCode, String hospitalName) {
		this.version = version;
		this.hospitalCode = hospitalCode;
		this.hospitalName = hospitalName;
	}

	public void setVersion(long version) {
		this.version = version;
    }

    public long getVersion() {
    	return version;
    }

    public void setPageDto(RestResponsePage<DspWardsPerHospital2GDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<DspWardsPerHospital2GDO> getPageDto() {
		return pageDto;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
    }

    public String getHospitalCode() {
    	return hospitalCode;
    }

	public void setCountry(CountryEnum country) {
		this.country = country;
    }

    public CountryEnum getCountry() {
    	return country;
    }

	public void setCountryName(String countryName) {
		this.countryName = countryName;
    }

    public String getCountryName() {
    	return countryName;
    }

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
    	return hospitalName;
    }

	public void setTelephoneNumber(long telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
    }

    public long getTelephoneNumber() {
    	return telephoneNumber;
    }

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
    }

    public ReturnCodeEnum getReturnCode() {
    	return returnCode;
    }

	public void setGdo(DspWardsPerHospital2GDO gdo) {
		this.gdo = gdo;
	}

	public DspWardsPerHospital2GDO getGdo() {
		return gdo;
	}

}