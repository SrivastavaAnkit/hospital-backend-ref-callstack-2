package com.hospital.file.ward.rtvwardforhospital;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.ward.Ward;
import com.hospital.file.ward.WardId;
import com.hospital.file.ward.WardRepository;
import com.hospital.file.patient.rtvpatientsforhospital.RtvPatientsForHospitalService;
import com.hospital.file.patient.rtvpatientsforhospital.RtvPatientsForHospitalDTO;
import com.hospital.model.ReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvWardForHospitalService extends AbstractService<RtvWardForHospitalService, RtvWardForHospitalDTO>
{
    private final Step execute = define("execute", RtvWardForHospitalDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private WardRepository wardRepository;
	
	@Autowired
	private RtvPatientsForHospitalService rtvPatientsForHospitalService;
	
    @Autowired
    public RtvWardForHospitalService()
    {
        super(RtvWardForHospitalService.class, RtvWardForHospitalDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvWardForHospitalDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvWardForHospitalDTO dto, RtvWardForHospitalDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Ward> wardList = wardRepository.findAllByIdHospitalCode(dto.getHospitalCode());
		if (!wardList.isEmpty()) {
			for (Ward ward : wardList) {
				processDataRecord(dto, ward);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvWardForHospitalDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Empty:48)
		 */
		//switchSUB 48 SUB    
		// Unprocessed SUB 48 -
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvWardForHospitalDTO dto, Ward ward) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		RtvPatientsForHospitalDTO rtvPatientsForHospitalDTO;
		//switchSUB 41 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT RTV Patients for Hospital - Patient  *
		rtvPatientsForHospitalDTO = new RtvPatientsForHospitalDTO();
		rtvPatientsForHospitalDTO.setHospitalCode(dto.getHospitalCode());
		rtvPatientsForHospitalDTO.setWardCode(ward.getWardCode());
		rtvPatientsForHospitalService.execute(rtvPatientsForHospitalDTO);
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvWardForHospitalDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Empty:52)
		 */
		//switchSUB 52 SUB    
		// Unprocessed SUB 52 -
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvWardForHospitalDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Empty:61)
		 */
		//switchSUB 61 SUB    
		// Unprocessed SUB 61 -
        return NO_ACTION;
    }
}
