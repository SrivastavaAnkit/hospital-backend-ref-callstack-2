package com.hospital.file.ward.changeward;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.ward.Ward;
import com.hospital.file.ward.WardId;
import com.hospital.file.ward.WardRepository;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class ChangeWardService extends AbstractService<ChangeWardService, ChangeWardDTO>
{
    private final Step execute = define("execute", ChangeWardDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private WardRepository wardRepository;
	

    @Autowired
    public ChangeWardService() {
        super(ChangeWardService.class, ChangeWardDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(ChangeWardDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(ChangeWardDTO dto, ChangeWardDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);
		WardId wardId = new WardId();
		wardId.setHospitalCode(dto.getHospitalCode());
		wardId.setWardCode(dto.getWardCode());
		Ward ward = wardRepository.findById(wardId).get();
		if (ward == null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0005"));
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, ward);
			ward.setHospitalCode(dto.getHospitalCode());
			ward.setWardCode(dto.getWardCode());
			ward.setWardName(dto.getWardName());
			/*ward.setAddedUser(dto.getAddedUser());
			ward.setAddedDate(dto.getAddedDate());
			ward.setAddedTime(dto.getAddedTime());
			ward.setChangedUser(dto.getChangedUser());
			ward.setChangedDate(dto.getChangedDate());
			ward.setChangedTime(dto.getChangedTime());*/
			processingBeforeDataUpdate(dto, ward);
			try {
				wardRepository.saveAndFlush(ward);
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
			}
		}

       return result;
	}

	private StepResult processingBeforeDataRead(ChangeWardDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000025 BLK CAS
		//switchSUB 1000025 BLK CAS
		if (dto.getHospitalCode().equals("PUBLIC")) {
			// PAR.Hospital Code is Equal Public
			//switchBLK 1000028 BLK ACT
			//functionCall 1000029 ACT LCL.Prescription Code = CND.Two pills by day
			dto.setLclPrescriptionCode("TWOPLD");
		}
       return NO_ACTION;
	}

	private StepResult processingIfDataRecordNotFound(ChangeWardDTO dto) throws ServiceException {
		/**
		 * USER: Processing if Data Record Not Found (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
	}

	private StepResult processingAfterDataRead(ChangeWardDTO dto, Ward ward) throws ServiceException {
		/**
		 * USER: Processing After Data Read (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000034 BLK CAS
		//switchSUB 1000034 BLK CAS
		if (ward.getHospitalCode().equals("PUBLIC")) {
			// DB1.Hospital Code is Equal Public
			//switchBLK 1000037 BLK ACT
			//functionCall 1000038 ACT LCL.Prescription Code = CND.Two pills by day
			dto.setLclPrescriptionCode("TWOPLD");
		}
       return NO_ACTION;
	}

	private StepResult processingBeforeDataUpdate(ChangeWardDTO dto, Ward ward) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Generated:10)
		 */
		//switchSUB 10 SUB    
		//switchBLK 1000013 BLK ACT
		//functionCall 1000014 ACT DB1.Changed User = JOB.*USER
		/*ward.setChangedUser(job.getUser());
		//switchBLK 1000017 BLK ACT
		//functionCall 1000018 ACT DB1.Changed Date = JOB.*Job date
		ward.setChangedDate(LocalDate.now());
		//switchBLK 1000021 BLK ACT
		//functionCall 1000022 ACT DB1.Changed Time = JOB.*Job time
		ward.setChangedTime(LocalTime.now());*/
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(ChangeWardDTO dto) throws ServiceException {
		/**
		 * USER: Processing After Data Update (Generated:23)
		 */
		//switchSUB 23 SUB    
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
	}
}
