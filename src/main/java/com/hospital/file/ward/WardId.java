package com.hospital.file.ward;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.io.Serializable;
import javax.persistence.Column;

public class WardId implements Serializable {
	private static final long serialVersionUID = -1L;

	@Column(name = "HospitalCode")
	private String hospitalCode = "";
	
	@Column(name = "WardCode")
	private String wardCode = "";

	public WardId() {
	
	}

	public WardId(String hospitalCode, String wardCode) {
		this.hospitalCode = hospitalCode;
		this.wardCode = wardCode;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}
	
	public String getWardCode() {
		return wardCode;
	}
	
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	
	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
