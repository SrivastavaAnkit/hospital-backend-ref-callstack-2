package com.hospital.file.ward;

import com.hospital.file.hospital.Hospital;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name="Ward", schema="HospitalMgmt")
public class Ward implements Serializable {
	private static final long serialVersionUID = -1L;

	@Version
	@Column(name="version")
	private long version;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns ({
		@JoinColumn(name="HospitalCode", referencedColumnName="HospitalCode", insertable=false, updatable=false)
	})
	private Hospital hospitalObj;

	@EmbeddedId
	private WardId id = new WardId();

	@Column(name = "WardName")
	private String wardName = "";

	public WardId getId() {
		return id;
	}

	public String getHospitalCode() {
		return id.getHospitalCode();
	}
	
	public String getWardCode() {
		return id.getWardCode();
	}

	public String getWardName() {
		return wardName;
	}

	public String getHospitalName() {
		return hospitalObj.getHospitalName();
	}

	public long getVersion() {
		return version;
	}

	public void setHospitalCode(String hospitalCode) {
		this.id.setHospitalCode(hospitalCode);
	}
	
	public void setWardCode(String wardCode) {
		this.id.setWardCode(wardCode);
	}

	public void setWardName(String wardName) {
		this.wardName = wardName;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
