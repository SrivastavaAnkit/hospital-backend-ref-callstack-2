package com.hospital.file.ward.deleteward;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.ward.Ward;
import com.hospital.file.ward.WardId;
import com.hospital.file.ward.WardRepository;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class DeleteWardService  extends AbstractService<DeleteWardService, DeleteWardDTO>
{
    private final Step execute = define("execute", DeleteWardDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private WardRepository wardRepository;
	

    @Autowired
    public DeleteWardService() {
        super(DeleteWardService.class, DeleteWardDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(DeleteWardDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(DeleteWardDTO dto, DeleteWardDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataUpdate(dto);
		WardId wardId = new WardId();
		wardId.setHospitalCode(dto.getHospitalCode());
		wardId.setWardCode(dto.getWardCode());
		try {
			wardRepository.deleteById(wardId);
			wardRepository.flush();
			dto.setReturnCode(ReturnCodeEnum.fromCode(""));
			processingAfterDataUpdate(dto);
		} catch (Exception e) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
		}

       return result;
	}

	private StepResult processingBeforeDataUpdate(DeleteWardDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Empty:9)
		 */
		
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(DeleteWardDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Empty:17)
		 */
		
       return NO_ACTION;
	}
}
