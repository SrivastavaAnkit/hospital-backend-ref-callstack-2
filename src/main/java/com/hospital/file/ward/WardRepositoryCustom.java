package com.hospital.file.ward;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.hospital.model.CountryEnum;

import com.hospital.file.ward.selectward.SelectWardGDO;
import com.hospital.file.ward.editward.EditWardGDO;
import com.hospital.file.ward.dspwardsperhospital.DspWardsPerHospitalGDO;
import com.hospital.file.ward.dspwardsperhospital2.DspWardsPerHospital2GDO;

/**
 * Custom Spring Data JPA repository interface for model: Ward (TSADREP).
 *
 * @author X2EGenerator
 */
@Repository
public interface WardRepositoryCustom {

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param wardCode Ward Code
	 */
	void deleteWard(String hospitalCode, String wardCode);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param wardCode Ward Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of SelectWardGDO
	 */
	RestResponsePage<SelectWardGDO> selectWard(String hospitalCode, String wardCode, Pageable pageable);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of EditWardGDO
	 */
	RestResponsePage<EditWardGDO> editWard(String hospitalCode, Pageable pageable);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of DspWardsPerHospitalGDO
	 */
	RestResponsePage<DspWardsPerHospitalGDO> dspWardsPerHospital(String hospitalCode, Pageable pageable);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param wardCode Ward Code
	 * @return Ward
	 */
	Ward rtvWardDetail(String hospitalCode, String wardCode);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of Ward
	 */
	RestResponsePage<Ward> rtvNoOfWardsHospital(String hospitalCode, Pageable pageable);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param country Country
	 * @param countryName Country Name
	 * @param telephoneNumber Telephone Number
	 * @param pageable a Pageable object used for pagination
	 * @return Page of DspWardsPerHospital2GDO
	 */
	RestResponsePage<DspWardsPerHospital2GDO> dspWardsPerHospital2(String hospitalCode, CountryEnum country, String countryName, long telephoneNumber, Pageable pageable);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of Ward
	 */
	RestResponsePage<Ward> rtvWardForHospital(String hospitalCode, Pageable pageable);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of Ward
	 */
	RestResponsePage<Ward> rtvNbrWardHospital(String hospitalCode, Pageable pageable);
}
