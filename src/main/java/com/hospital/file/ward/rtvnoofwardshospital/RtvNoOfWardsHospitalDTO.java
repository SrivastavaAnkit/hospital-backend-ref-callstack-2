package com.hospital.file.ward.rtvnoofwardshospital;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvNoOfWardsHospitalDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private ReturnCodeEnum returnCode;
	private String hospitalCode;
	private long count;
	private long numberOfWards;
	private String nextScreen;

	public long getCount() {
		return count;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public long getNumberOfWards() {
		return numberOfWards;
	}

	public long getWfWardCount() {
		return global.getLong("wardCount");
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public void setNumberOfWards(long numberOfWards) {
		this.numberOfWards = numberOfWards;
	}

	public void setWfWardCount(long wfWardCount) {
		global.setLong("wardCount", wfWardCount);
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
