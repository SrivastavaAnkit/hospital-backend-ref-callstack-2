package com.hospital.file.ward.rtvnoofwardshospital;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.ward.Ward;
import com.hospital.file.ward.WardId;
import com.hospital.file.ward.WardRepository;
import com.hospital.model.ReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvNoOfWardsHospitalService extends AbstractService<RtvNoOfWardsHospitalService, RtvNoOfWardsHospitalDTO>
{
    private final Step execute = define("execute", RtvNoOfWardsHospitalDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private WardRepository wardRepository;
	
    @Autowired
    public RtvNoOfWardsHospitalService()
    {
        super(RtvNoOfWardsHospitalService.class, RtvNoOfWardsHospitalDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvNoOfWardsHospitalDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvNoOfWardsHospitalDTO dto, RtvNoOfWardsHospitalDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;
        //TODO: Commented below wrk variables as globalContext null
        //BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Ward> wardList = wardRepository.findAllByIdHospitalCode(dto.getHospitalCode());
//		if (!wardList.isEmpty()) {
//			for (Ward ward : wardList) {
//				processDataRecord(dto, ward);
//			}
//			exitProcessing(dto);
//		}
//		else {
//			processingIfDataRecordNotFound(dto);
//		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvNoOfWardsHospitalDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT WRK.Ward Count = CON.*ZERO
		//dto.setWfWardCount(0);
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvNoOfWardsHospitalDTO dto, Ward ward) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		//switchSUB 41 SUB    
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT WRK.Ward Count = WRK.Ward Count + CON.1
		dto.setWfWardCount(dto.getWfWardCount() + 1);
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvNoOfWardsHospitalDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		//switchSUB 52 SUB    
		//switchBLK 1000037 BLK ACT
		//functionCall 1000038 ACT WRK.Ward Count = CON.*ZERO
		//dto.setWfWardCount(0);
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvNoOfWardsHospitalDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		//switchSUB 61 SUB    
		//switchBLK 1000033 BLK ACT
		//functionCall 1000034 ACT PAR.Number of wards = WRK.Ward Count
		dto.setNumberOfWards(dto.getWfWardCount());
        return NO_ACTION;
    }
}
