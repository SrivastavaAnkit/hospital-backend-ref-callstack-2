package com.hospital.file.ward.rtvwarddetail;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvWardDetailDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private ReturnCodeEnum returnCode;
	private String hospitalCode;
	private String wardCode;
	private String wardName;
	private String nextScreen;

	public String getHospitalCode() {
		return hospitalCode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public String getWardName() {
		return wardName;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public void setWardName(String wardName) {
		this.wardName = wardName;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
