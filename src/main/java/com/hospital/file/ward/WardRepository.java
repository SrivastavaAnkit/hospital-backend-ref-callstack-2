package com.hospital.file.ward;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository interface for model: Ward (TSADREP).
 *
 * @author X2EGenerator
 */
@Repository
public interface WardRepository extends WardRepositoryCustom, JpaRepository<Ward, WardId> {

	List<Ward> findAllByIdHospitalCode(String hospitalCode);

	List<Ward> findAllByIdHospitalCodeAndIdWardCode(String hospitalCode, String wardCode);
}
