package com.hospital.file.ward.createward;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.ward.Ward;
import com.hospital.file.ward.WardId;
import com.hospital.file.ward.WardRepository;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class CreateWardService  extends AbstractService<CreateWardService, CreateWardDTO>
{
    private final Step execute = define("execute", CreateWardDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private WardRepository wardRepository;
	

    @Autowired
    public CreateWardService() {
        super(CreateWardService.class, CreateWardDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(CreateWardDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(CreateWardDTO dto, CreateWardDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		Ward ward = new Ward();
		ward.setHospitalCode(dto.getHospitalCode());
		ward.setWardCode(dto.getWardCode());
		ward.setWardName(dto.getWardName());
		/*ward.setAddedUser(dto.getAddedUser());
		ward.setAddedDate(dto.getAddedDate());
		ward.setAddedTime(dto.getAddedTime());
		ward.setChangedUser(dto.getChangedUser());
		ward.setChangedDate(dto.getChangedDate());
		ward.setChangedTime(dto.getChangedTime());*/

		processingBeforeDataUpdate(dto, ward);

		Ward ward2 = wardRepository.findById(ward.getId()).get();
		if (ward2 != null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0003"));
			processingIfDataRecordAlreadyExists(dto, ward2);
		}
		else {
            try {
				wardRepository.save(ward);
                dto.setReturnCode(ReturnCodeEnum.fromCode(""));
                processingAfterDataUpdate(dto, ward);
            } catch (Exception e) {
                dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
                processingIfDataUpdateError(dto, ward);
            }
        }

        return result;
	}

    private StepResult processingBeforeDataUpdate(CreateWardDTO dto, Ward ward) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT DB1.Added User = JOB.*USER
		/*ward.setAddedUser(job.getUser());
		//switchBLK 1000005 BLK ACT
		//functionCall 1000006 ACT DB1.Added Date = JOB.*Job date
		ward.setAddedDate(LocalDate.now());
		//switchBLK 1000009 BLK ACT
		//functionCall 1000010 ACT DB1.Added Time = JOB.*Job time
		ward.setAddedTime(LocalTime.now());*/
       return NO_ACTION;
    }

    private StepResult processingIfDataRecordAlreadyExists(CreateWardDTO dto, Ward ward) throws ServiceException {
		/**
		 * USER: Processing if Data Record Already Exists (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000013 BLK ACT
		//functionCall 1000014 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
    }

    private StepResult processingAfterDataUpdate(CreateWardDTO dto, Ward ward) throws ServiceException {
		/**
		 * USER: Processing after Data Update (Generated:11)
		 */
		//switchSUB 11 SUB    
		//switchBLK 1000025 BLK ACT
		//functionCall 1000026 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
    }

    private StepResult processingIfDataUpdateError(CreateWardDTO dto, Ward ward) throws ServiceException {
		/**
		 * Processing if Data Update Error (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000019 BLK ACT
		//functionCall 1000020 ACT PAR.USR Return Code = CND.Error
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("E"));
       return NO_ACTION;
    }
}
