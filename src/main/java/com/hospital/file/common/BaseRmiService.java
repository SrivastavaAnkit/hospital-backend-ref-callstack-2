package com.hospital.file.common;

import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.runtime.TerminalSession;
import com.hospital.model.GlobalContext;
import java.io.Serializable;
import org.springframework.beans.BeanUtils;
import org.springframework.core.env.Environment;

/**
 * This is a sample base services to support RPG-translated services that contains access to a LDA
 * datastructure.
 */
public abstract class BaseRmiService<U extends BaseRmiService<U, T>, T extends Serializable>
    extends AbstractService<U, T> {

  private static final String GLOBAL_CONTEXT_KEY = "GLOBAL_CONTEXT";
  private final Environment environment;

  public BaseRmiService(Class<U> serviceClass, Class<T> stateClass, Environment environment) {
    super(serviceClass, stateClass);
    this.environment = environment;
  }

//  @Override
//  public void onActivate(TerminalSession session, ServiceInstance<U, T> state) {
//    super.onActivate(session, state);
//
//    // Copy the Global Context to the app state.
//    GlobalContext globalContext =
//        session.readSessionVariable(GLOBAL_CONTEXT_KEY, GlobalContext.class);
//    if (globalContext == null) {
//      globalContext = new GlobalContext();
//
//      globalContext.setCompanyid(
//          environment.getRequiredProperty("rmi.default.company.id", Integer.class));
//      globalContext.setCompanyNumber(environment.getRequiredProperty("rmi.default.company.number"));
//      globalContext.setDistrictNumber(
//          environment.getRequiredProperty("rmi.default.district.number"));
//      globalContext.setUser(environment.getRequiredProperty("rmi.default.username"));
//    }
//    BeanUtils.copyProperties(globalContext, state.getState());
//  }
//
//  @Override
//  public void onDeactivate(TerminalSession session, ServiceInstance<U, T> state) {
//    super.onDeactivate(session, state);
//
//    GlobalContext globalContext = new GlobalContext();
//    BeanUtils.copyProperties(state.getState(), globalContext);
//    session.writeSessionVariable(GLOBAL_CONTEXT_KEY, globalContext);
//  }
}
