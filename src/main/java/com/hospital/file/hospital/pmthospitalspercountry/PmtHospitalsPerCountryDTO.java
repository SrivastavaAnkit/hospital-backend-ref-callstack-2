package com.hospital.file.hospital.pmthospitalspercountry;

import java.time.LocalDate;
import java.time.LocalTime;


import org.springframework.beans.BeanUtils;

import com.hospital.file.hospital.Hospital;
import com.hospital.model.CountryEnum;

import com.hospital.common.state.BaseDTO;

/**
 * Dto for file 'Hospital' (TSACREP) and function 'PMT Hospitals per Country' (TSA7PVR).
 */
public class PmtHospitalsPerCountryDTO extends BaseDTO {
	private static final long serialVersionUID = 739476672657769795L;

	private String hospitalCode = "";
	private String hospitalName = "";
	private String addressStreet = "";
	private String addressTown = "";
	private String addressProvince = "";
	private String addressPostZip = "";
	private long telephoneNumber = 0L;
	private long faxNumber = 0L;
	private CountryEnum country = null;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;
	private String countryName = "";

	public PmtHospitalsPerCountryDTO() {

	}

	public PmtHospitalsPerCountryDTO(Hospital hospital) {
		BeanUtils.copyProperties(hospital, this);
	}

	public PmtHospitalsPerCountryDTO(String hospitalCode, String hospitalName, String addressStreet, String addressTown, String addressProvince, String addressPostZip, long telephoneNumber, long faxNumber, CountryEnum country, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime) {
		this.hospitalCode = hospitalCode;
		this.hospitalName = hospitalName;
		this.addressStreet = addressStreet;
		this.addressTown = addressTown;
		this.addressProvince = addressProvince;
		this.addressPostZip = addressPostZip;
		this.telephoneNumber = telephoneNumber;
		this.faxNumber = faxNumber;
		this.country = country;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}

	public String getAddressStreet() {
		return addressStreet;
	}

	public void setAddressTown(String addressTown) {
		this.addressTown = addressTown;
	}

	public String getAddressTown() {
		return addressTown;
	}

	public void setAddressProvince(String addressProvince) {
		this.addressProvince = addressProvince;
	}

	public String getAddressProvince() {
		return addressProvince;
	}

	public void setAddressPostZip(String addressPostZip) {
		this.addressPostZip = addressPostZip;
	}

	public String getAddressPostZip() {
		return addressPostZip;
	}

	public void setTelephoneNumber(long telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public long getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setFaxNumber(long faxNumber) {
		this.faxNumber = faxNumber;
	}

	public long getFaxNumber() {
		return faxNumber;
	}

	public void setCountry(CountryEnum country) {
		this.country = country;
	}

	public CountryEnum getCountry() {
		return country;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryName() {
		return countryName;
	}

}
