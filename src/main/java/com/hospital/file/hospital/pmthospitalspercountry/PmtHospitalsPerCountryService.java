package com.hospital.file.hospital.pmthospitalspercountry;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;

import com.hospital.file.hospital.HospitalRepository;

import com.hospital.file.hospital.dspfdisplayhospitals.DspfDisplayHospitalsService;
import com.hospital.file.hospital.dspfdisplayhospitals.DspfDisplayHospitalsDTO;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.file.hospital.dspfdisplayhospitals.DspfDisplayHospitalsParams;

import com.hospital.model.CmdKeyEnum;
import com.hospital.model.DeferConfirmEnum;


/**
 * Service for file 'Hospital' (TSACREP) and function 'PMT Hospitals per Country' (TSA7PVR).
 */
@Service
public class PmtHospitalsPerCountryService extends AbstractService<PmtHospitalsPerCountryService,PmtHospitalsPerCountryDTO> {

    public static final String SCREEN_KEY = "com.hospital.file.hospital.pmthospitalspercountry.key";
    public static final String SCREEN_CONFIRM_PROMPT = "com.hospital.file.hospital.pmthospitalspercountry.key2";

    private final Step execute = define("execute",PmtHospitalsPerCountryParams.class, this::execute);
    private final Step response = define("response",PmtHospitalsPerCountryDTO.class, this::processResponse);
    private final Step displayConfirmPrompt = define("displayConfirmPrompt",PmtHospitalsPerCountryDTO.class, this::processDisplayConfirmPrompt);
    //private final Step serviceDspfDisplayHospitals = define("serviceDspfDisplayHospitals",DspfDisplayHospitalsParams.class, this::processServiceDspfDisplayHospitals);

	@Autowired
	private HospitalRepository hospitalRepository;

    	

    @Autowired
    public PmtHospitalsPerCountryService()
    {
        super(PmtHospitalsPerCountryService.class, PmtHospitalsPerCountryDTO.class);
    }

    @Override
    public Step getInitialStep()
    {
        return execute;
    }

    /**
     * PmtHospitalsPerCountry service starting point.
     * @param dto   - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(PmtHospitalsPerCountryDTO dto, PmtHospitalsPerCountryParams params)  {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);
        initialize(dto);
        result = conductScreenConversation(dto);

        return result;
    }

    /**
     * PmtHospitalsPerCountry service initialization.
     * @param dto   - Service state class.
     * @return
     */
    private void initialize(PmtHospitalsPerCountryDTO dto) {
        usrInitializeProgram(dto);
        usrScreenFunctionFields(dto);
        usrLoadScreen(dto);
    }

    /**
     * SCREEN_KEY display processing loop method.
     * @param dto   - Service state class.
     * @return
     */
    private StepResult conductScreenConversation(PmtHospitalsPerCountryDTO dto)
    {
        StepResult result = NO_ACTION;

        result = callScreen(SCREEN_KEY, dto).thenCall(response);

        return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param dto - Service state class.
     * @param screenDto - returned screen model.
     * @return
     */
    private StepResult processResponse(PmtHospitalsPerCountryDTO dto, PmtHospitalsPerCountryDTO screenDto)
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(screenDto, dto);
        if (CmdKeyEnum.isExit(dto.get_SysCmdKey()))
        {
            result = closedown(dto);
            return result;
        }
        else if (CmdKeyEnum.isReset(dto.get_SysCmdKey()))
        {
            //TODO: processResetRequest(dto);//synon built-in function
        }
        else if (CmdKeyEnum.isHelp(dto.get_SysCmdKey()))
        {
            //TODO:processHelpRequest(dto);//synon built-in function
        }
        else
        {
            validateScreenInput(dto);
            if (!dto.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_USER_QUIT_REQUESTED.getCode()))
            {
                result = callScreen(SCREEN_CONFIRM_PROMPT, dto).thenCall(displayConfirmPrompt);
                return result;
            }
        }
        result = conductScreenConversation(dto);

        return result;
    }

    /**
     * SCREEN_CONFIRM_PROMPT returned response processing method.
     * @param dto - Service state class.
     * @param screenDto - returned screen model.
     * @return
     */
    private StepResult processDisplayConfirmPrompt(PmtHospitalsPerCountryDTO dto, PmtHospitalsPerCountryDTO screenDto) {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(screenDto, dto);
        if (!dto.get_SysDeferConfirm().getCode().equals(DeferConfirmEnum._PROCEED_TO_CONFIRM.getCode()))
        {
            usrUserDefinedAction(dto);
        }
        result = conductScreenConversation(dto);

        return result;
    }

    /**
     * Validate input in SCREEN_KEY.
     * @param dto - Service state class.
     * @return
     */
    private void validateScreenInput(PmtHospitalsPerCountryDTO dto)
    {
        usrProcessCommandKeys(dto);
        //TODO: checkFields(dto); //synon built-in function
        if (dto.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode()))
        {
            usrValidateFields(dto);
            //TODO: checkRelations(dto); //synon built-in function
            if (dto.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode()))
            {
                usrScreenFunctionFields(dto);
                usrValidateRelations(dto);
            }
        }
    }

    /**
     * Terminate this program
     * @param dto - Service state class.
     * @return
     */
    private StepResult closedown(PmtHospitalsPerCountryDTO dto)
    {
        StepResult result = NO_ACTION;

        usrExitProgramProcessing(dto);
        dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        //exitProgram

        return result;
    }

    /**
     * ---------------------- Programmatic user-point --------------------------
     */

	/**
	 * USER: Initialize Program (Empty:10)
	 */
    private StepResult usrInitializeProgram(PmtHospitalsPerCountryDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 10 SUB    
			// Unprocessed SUB 10 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Screen Function Fields (Empty:1052)
	 */
    private StepResult usrScreenFunctionFields(PmtHospitalsPerCountryDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1052 SUB    
			// Unprocessed SUB 1052 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Load Screen (Generated:1058)
	 */
    private StepResult usrLoadScreen(PmtHospitalsPerCountryDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1058 SUB    
			//switchBLK 1000001 BLK ACT
			//functionCall 1000002 ACT DTL.Country Name = CVTVAR(DTL.Country)
			if (dto.getCountry() != null) {
				dto.setCountryName(dto.getCountry().getDescription());
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Empty:1035)
	 */
    private StepResult usrProcessCommandKeys(PmtHospitalsPerCountryDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1035 SUB    
			// Unprocessed SUB 1035 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Fields (Empty:1065)
	 */
    private StepResult usrValidateFields(PmtHospitalsPerCountryDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1065 SUB    
			// Unprocessed SUB 1065 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Relations (Empty:29)
	 */
    private StepResult usrValidateRelations(PmtHospitalsPerCountryDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 29 SUB    
			// Unprocessed SUB 29 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: User Defined Action (Generated:41)
	 */
    private StepResult usrUserDefinedAction(PmtHospitalsPerCountryDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 41 SUB    
			//switchBLK 1000007 BLK CAS
			//switchSUB 1000007 BLK CAS
			if (dto.getCountry() == CountryEnum.fromCode("*BLANK")) {
				// DTL.Country is Blank
			}//switchSUB 1000010 SUB    
			 else {
				// *OTHERWISE
				//switchBLK 1000012 BLK ACT
				//functionCall 1000013 ACT DSPF Display Hospitals - Hospital  *
				//TODO: split
				DspfDisplayHospitalsParams dspfDisplayHospitalsParams = new DspfDisplayHospitalsParams();
				BeanUtils.copyProperties(dto, dspfDisplayHospitalsParams);
				//result = StepResult.callService(DspfDisplayHospitalsService.class, dspfDisplayHospitalsParams);//.thenCall(serviceDspfDisplayHospitalsDisplay)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Empty:1021)
	 */
    private StepResult usrExitProgramProcessing(PmtHospitalsPerCountryDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1021 SUB    
			// Unprocessed SUB 1021 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * DspfDisplayHospitalsService returned response processing method.
//     * @param dto - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceDspfDisplayHospitals(PmtHospitalsPerCountryDTO dto, DspfDisplayHospitalsParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        return result;
//    }
//
}
