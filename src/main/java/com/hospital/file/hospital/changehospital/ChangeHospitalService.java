package com.hospital.file.hospital.changehospital;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.hospital.Hospital;
import com.hospital.file.hospital.HospitalId;
import com.hospital.file.hospital.HospitalRepository;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class ChangeHospitalService extends AbstractService<ChangeHospitalService, ChangeHospitalDTO>
{
    private final Step execute = define("execute", ChangeHospitalDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private HospitalRepository hospitalRepository;
	

    @Autowired
    public ChangeHospitalService() {
        super(ChangeHospitalService.class, ChangeHospitalDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(ChangeHospitalDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(ChangeHospitalDTO dto, ChangeHospitalDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);
		HospitalId hospitalId = new HospitalId();
		hospitalId.setHospitalCode(dto.getHospitalCode());
		Hospital hospital = hospitalRepository.findById(hospitalId).get();
		if (hospital == null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0005"));
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, hospital);
			hospital.setHospitalCode(dto.getHospitalCode());
			hospital.setHospitalName(dto.getHospitalName());
			hospital.setAddressStreet(dto.getAddressStreet());
			hospital.setAddressTown(dto.getAddressTown());
			hospital.setAddressProvince(dto.getAddressProvince());
			hospital.setAddressPostZip(dto.getAddressPostZip());
			hospital.setTelephoneNumber(dto.getTelephoneNumber());
			hospital.setFaxNumber(dto.getFaxNumber());
			hospital.setCountry(dto.getCountry());
			/*hospital.setAddedUser(dto.getAddedUser());
			hospital.setAddedDate(dto.getAddedDate());
			hospital.setAddedTime(dto.getAddedTime());
			hospital.setChangedUser(dto.getChangedUser());
			hospital.setChangedDate(dto.getChangedDate());
			hospital.setChangedTime(dto.getChangedTime());*/
			processingBeforeDataUpdate(dto, hospital);
			try {
				hospitalRepository.saveAndFlush(hospital);
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
			}
		}

       return result;
	}

	private StepResult processingBeforeDataRead(ChangeHospitalDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000033 BLK CAS
		//switchSUB 1000033 BLK CAS
		if (dto.getTelephoneNumber() != 0) {
			// PAR.Telephone Number is not zero
			//switchBLK 1000036 BLK ACT
			//functionCall 1000037 ACT LCL.Telephone Number = PAR.Telephone Number
		}
       return NO_ACTION;
	}

	private StepResult processingIfDataRecordNotFound(ChangeHospitalDTO dto) throws ServiceException {
		/**
		 * USER: Processing if Data Record Not Found (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000003 BLK ACT
		//functionCall 1000004 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
	}

	private StepResult processingAfterDataRead(ChangeHospitalDTO dto, Hospital hospital) throws ServiceException {
		/**
		 * USER: Processing After Data Read (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000040 BLK CAS
		//switchSUB 1000040 BLK CAS
		if (dto.getTelephoneNumber() == 0) {
			// PAR.Telephone Number is Equal zero
			//switchBLK 1000043 BLK ACT
			//functionCall 1000044 ACT DB1.Telephone Number = CON.9999999999
			hospital.setTelephoneNumber(9999999999L);
		}
       return NO_ACTION;
	}

	private StepResult processingBeforeDataUpdate(ChangeHospitalDTO dto, Hospital hospital) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Generated:10)
		 */
		//switchSUB 10 SUB    
		//switchBLK 1000021 BLK ACT
		//functionCall 1000022 ACT DB1.Changed User = JOB.*USER
		/*hospital.setChangedUser(job.getUser());
		//switchBLK 1000027 BLK ACT
		//functionCall 1000028 ACT DB1.Changed Date = JOB.*Job date
		hospital.setChangedDate(LocalDate.now());
		//switchBLK 1000015 BLK ACT
		//functionCall 1000016 ACT DB1.Changed Time = JOB.*Job time
		hospital.setChangedTime(LocalTime.now());*/
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(ChangeHospitalDTO dto) throws ServiceException {
		/**
		 * USER: Processing After Data Update (Generated:23)
		 */
		//switchSUB 23 SUB    
		//switchBLK 1000009 BLK ACT
		//functionCall 1000010 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
	}
}
