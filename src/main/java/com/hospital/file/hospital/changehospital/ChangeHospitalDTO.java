package com.hospital.file.hospital.changehospital;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ChangeHospitalDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private CountryEnum country;
	private LocalDate addedDate;
	private LocalDate changedDate;
	private LocalTime addedTime;
	private LocalTime changedTime;
	private ReturnCodeEnum returnCode;
	private String addedUser;
	private String addressPostZip;
	private String addressProvince;
	private String addressStreet;
	private String addressTown;
	private String changedUser;
	private String hospitalCode;
	private String hospitalName;
	private UsrReturnCodeEnum usrReturnCode;
	private long faxNumber;
	private long telephoneNumber;
	private String nextScreen;
	private long lclTelephoneNumber;

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public String getAddressPostZip() {
		return addressPostZip;
	}

	public String getAddressProvince() {
		return addressProvince;
	}

	public String getAddressStreet() {
		return addressStreet;
	}

	public String getAddressTown() {
		return addressTown;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public CountryEnum getCountry() {
		return country;
	}

	public long getFaxNumber() {
		return faxNumber;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public long getTelephoneNumber() {
		return telephoneNumber;
	}

	public UsrReturnCodeEnum getUsrReturnCode() {
		return usrReturnCode;
	}

	public long getLclTelephoneNumber() {
		return lclTelephoneNumber;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public void setAddressPostZip(String addressPostZip) {
		this.addressPostZip = addressPostZip;
	}

	public void setAddressProvince(String addressProvince) {
		this.addressProvince = addressProvince;
	}

	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}

	public void setAddressTown(String addressTown) {
		this.addressTown = addressTown;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public void setCountry(CountryEnum country) {
		this.country = country;
	}

	public void setFaxNumber(long faxNumber) {
		this.faxNumber = faxNumber;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public void setTelephoneNumber(long telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public void setUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
		this.usrReturnCode = usrReturnCode;
	}

	public void setLclTelephoneNumber(long lclTelephoneNumber) {
		this.lclTelephoneNumber = lclTelephoneNumber;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
