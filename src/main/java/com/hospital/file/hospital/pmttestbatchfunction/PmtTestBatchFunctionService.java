package com.hospital.file.hospital.pmttestbatchfunction;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;

import com.hospital.file.hospital.HospitalRepository;

import com.hospital.file.hospital.xfupdateworkfileandrpt.XfUpdateWorkFileAndRptService;
import com.hospital.file.workfile.xfbatchjob.XfBatchJobService;
import com.hospital.file.hospital.xfupdateworkfileandrpt.XfUpdateWorkFileAndRptDTO;
import com.hospital.file.workfile.xfbatchjob.XfBatchJobDTO;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.model.CmdKeyEnum;
import com.hospital.model.DeferConfirmEnum;


/**
 * Service for file 'Hospital' (TSACREP) and function 'PMT Test Batch Function' (TSBJPVR).
 */
@Service
public class PmtTestBatchFunctionService extends AbstractService<PmtTestBatchFunctionService,PmtTestBatchFunctionDTO> {

    public static final String SCREEN_KEY = "com.hospital.file.hospital.pmttestbatchfunction.key";
    public static final String SCREEN_CONFIRM_PROMPT = "com.hospital.file.hospital.pmttestbatchfunction.key2";

    private final Step execute = define("execute",PmtTestBatchFunctionParams.class, this::execute);
    private final Step response = define("response",PmtTestBatchFunctionDTO.class, this::processResponse);
    private final Step displayConfirmPrompt = define("displayConfirmPrompt",PmtTestBatchFunctionDTO.class, this::processDisplayConfirmPrompt);

	@Autowired
	private HospitalRepository hospitalRepository;

    	
	@Autowired
	private XfBatchJobService xfBatchJobService;
	
	@Autowired
	private XfUpdateWorkFileAndRptService xfUpdateWorkFileAndRptService;
	

    @Autowired
    public PmtTestBatchFunctionService()
    {
        super(PmtTestBatchFunctionService.class, PmtTestBatchFunctionDTO.class);
    }

    @Override
    public Step getInitialStep()
    {
        return execute;
    }

    /**
     * PmtTestBatchFunction service starting point.
     * @param dto   - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(PmtTestBatchFunctionDTO dto, PmtTestBatchFunctionParams params)  {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);
        initialize(dto);
        result = conductScreenConversation(dto);

        return result;
    }

    /**
     * PmtTestBatchFunction service initialization.
     * @param dto   - Service state class.
     * @return
     */
    private void initialize(PmtTestBatchFunctionDTO dto) {
        usrInitializeProgram(dto);
        usrScreenFunctionFields(dto);
        usrLoadScreen(dto);
    }

    /**
     * SCREEN_KEY display processing loop method.
     * @param dto   - Service state class.
     * @return
     */
    private StepResult conductScreenConversation(PmtTestBatchFunctionDTO dto)
    {
        StepResult result = NO_ACTION;

        result = callScreen(SCREEN_KEY, dto).thenCall(response);

        return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param dto - Service state class.
     * @param screenDto - returned screen model.
     * @return
     */
    private StepResult processResponse(PmtTestBatchFunctionDTO dto, PmtTestBatchFunctionDTO screenDto)
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(screenDto, dto);
        if (CmdKeyEnum.isExit(dto.get_SysCmdKey()))
        {
            result = closedown(dto);
            return result;
        }
        else if (CmdKeyEnum.isReset(dto.get_SysCmdKey()))
        {
            //TODO: processResetRequest(dto);//synon built-in function
        }
        else if (CmdKeyEnum.isHelp(dto.get_SysCmdKey()))
        {
            //TODO:processHelpRequest(dto);//synon built-in function
        }
        else
        {
            validateScreenInput(dto);
            if (!dto.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_USER_QUIT_REQUESTED.getCode()))
            {
                result = callScreen(SCREEN_CONFIRM_PROMPT, dto).thenCall(displayConfirmPrompt);
                return result;
            }
        }
        result = conductScreenConversation(dto);

        return result;
    }

    /**
     * SCREEN_CONFIRM_PROMPT returned response processing method.
     * @param dto - Service state class.
     * @param screenDto - returned screen model.
     * @return
     */
    private StepResult processDisplayConfirmPrompt(PmtTestBatchFunctionDTO dto, PmtTestBatchFunctionDTO screenDto) {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(screenDto, dto);
        if (!dto.get_SysDeferConfirm().getCode().equals(DeferConfirmEnum._PROCEED_TO_CONFIRM.getCode()))
        {
            usrUserDefinedAction(dto);
        }
        result = conductScreenConversation(dto);

        return result;
    }

    /**
     * Validate input in SCREEN_KEY.
     * @param dto - Service state class.
     * @return
     */
    private void validateScreenInput(PmtTestBatchFunctionDTO dto)
    {
        usrProcessCommandKeys(dto);
        //TODO: checkFields(dto); //synon built-in function
        if (dto.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode()))
        {
            usrValidateFields(dto);
            //TODO: checkRelations(dto); //synon built-in function
            if (dto.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode()))
            {
                usrScreenFunctionFields(dto);
                usrValidateRelations(dto);
            }
        }
    }

    /**
     * Terminate this program
     * @param dto - Service state class.
     * @return
     */
    private StepResult closedown(PmtTestBatchFunctionDTO dto)
    {
        StepResult result = NO_ACTION;

        usrExitProgramProcessing(dto);
        dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        //exitProgram

        return result;
    }

    /**
     * ---------------------- Programmatic user-point --------------------------
     */

	/**
	 * USER: Initialize Program (Empty:10)
	 */
    private StepResult usrInitializeProgram(PmtTestBatchFunctionDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 10 SUB    
			// Unprocessed SUB 10 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Screen Function Fields (Empty:1052)
	 */
    private StepResult usrScreenFunctionFields(PmtTestBatchFunctionDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1052 SUB    
			// Unprocessed SUB 1052 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Load Screen (Empty:1058)
	 */
    private StepResult usrLoadScreen(PmtTestBatchFunctionDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1058 SUB    
			// Unprocessed SUB 1058 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Empty:1035)
	 */
    private StepResult usrProcessCommandKeys(PmtTestBatchFunctionDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1035 SUB    
			// Unprocessed SUB 1035 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Fields (Empty:1065)
	 */
    private StepResult usrValidateFields(PmtTestBatchFunctionDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1065 SUB    
			// Unprocessed SUB 1065 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Relations (Empty:29)
	 */
    private StepResult usrValidateRelations(PmtTestBatchFunctionDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 29 SUB    
			// Unprocessed SUB 29 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: User Defined Action (Generated:41)
	 */
    private StepResult usrUserDefinedAction(PmtTestBatchFunctionDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    XfBatchJobDTO xfBatchJobDTO;
			XfUpdateWorkFileAndRptDTO xfUpdateWorkFileAndRptDTO;
			//switchSUB 41 SUB    
			//switchBLK 1000001 BLK CAS
			//switchSUB 1000001 BLK CAS
			if (!dto.getHospitalCode().equals("")) {
				// DTL.Hospital Code is Not Blank
				//switchBLK 1000004 BLK ACT
				//functionCall 1000005 ACT XF Batch job - Work File  * Test
				xfBatchJobDTO = new XfBatchJobDTO();
				xfBatchJobService.execute(xfBatchJobDTO);
				//switchBLK 1000007 BLK ACT
				//functionCall 1000008 ACT XF Update Work File & Rpt - Hospital  *
				//TODO: split
				XfUpdateWorkFileAndRptDTO xfUpdateWorkFileAndRptParams = new XfUpdateWorkFileAndRptDTO();
				BeanUtils.copyProperties(dto, xfUpdateWorkFileAndRptParams);
				//result = StepResult.callService(XfUpdateWorkFileAndRptService.class, xfUpdateWorkFileAndRptParams);//.thenCall(serviceXfUpdateWorkFileAndRptDisplay)
				xfUpdateWorkFileAndRptDTO = new XfUpdateWorkFileAndRptDTO();
				xfUpdateWorkFileAndRptDTO.setHospitalCode(dto.getHospitalCode());
				xfUpdateWorkFileAndRptService.execute(xfUpdateWorkFileAndRptDTO);
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Empty:1021)
	 */
    private StepResult usrExitProgramProcessing(PmtTestBatchFunctionDTO dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1021 SUB    
			// Unprocessed SUB 1021 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
}
