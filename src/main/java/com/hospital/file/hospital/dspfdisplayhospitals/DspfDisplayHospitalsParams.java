package com.hospital.file.hospital.dspfdisplayhospitals;

import com.hospital.model.CountryEnum;

	
/**
 * Params for resource: DspfDisplayHospitals (TSA8DFR).
 *
 * @author X2EGenerator
 */
public class DspfDisplayHospitalsParams {
	private static final long serialVersionUID = -182507391244678053L;
 
	private CountryEnum country = null;

		
	public CountryEnum getCountry() {
		return country;
	}
	
	public void setCountry(CountryEnum country) {
		this.country = country;
	}
	
	public void setCountry(String country) {
		setCountry(CountryEnum.valueOf(country));
	}
 
}