package com.hospital.file.hospital.dspfdisplayhospitals;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.file.hospital.Hospital;
import com.hospital.model.CountryEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;

/**
 * Gdo for file 'Hospital' (TSACREP) and function 'DSPF Display Hospitals' (TSA8DFR).
 */
public class DspfDisplayHospitalsGDO implements Serializable {
	private static final long serialVersionUID = -1123883649310374111L;

	private RecordSelectedEnum recordSelected = RecordSelectedEnum._STA_NO;
	private RecordDataChangedEnum recordDataChanged = RecordDataChangedEnum._STA_NO;
	private String gdo_selected = "";
	private CountryEnum country = null;
	private String hospitalCode = "";
	private String hospitalName = "";
	private String addressStreet = "";
	private String addressTown = "";
	private String addressProvince = "";
	private String addressPostZip = "";
	private long telephoneNumber = 0L;
	private long faxNumber = 0L;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;

	public DspfDisplayHospitalsGDO() {
 
	}
 
   	public DspfDisplayHospitalsGDO(CountryEnum country, String hospitalCode, String hospitalName, String addressStreet, String addressTown, String addressProvince, String addressPostZip, long telephoneNumber, long faxNumber, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime) {
		this.country = country;
		this.hospitalCode = hospitalCode;
		this.hospitalName = hospitalName;
		this.addressStreet = addressStreet;
		this.addressTown = addressTown;
		this.addressProvince = addressProvince;
		this.addressPostZip = addressPostZip;
		this.telephoneNumber = telephoneNumber;
		this.faxNumber = faxNumber;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}

	public void set_Selected(String gdo_selected) {
		this.gdo_selected = gdo_selected;
	}

	public String get_Selected() {
		return gdo_selected;
	}

	public void setCountry(CountryEnum country) {
    	this.country = country;
    }

	public CountryEnum getCountry() {
		return country;
	}

	public void setHospitalCode(String hospitalCode) {
    	this.hospitalCode = hospitalCode;
    }

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalName(String hospitalName) {
    	this.hospitalName = hospitalName;
    }

	public String getHospitalName() {
		return hospitalName;
	}

	public void setAddressStreet(String addressStreet) {
    	this.addressStreet = addressStreet;
    }

	public String getAddressStreet() {
		return addressStreet;
	}

	public void setAddressTown(String addressTown) {
    	this.addressTown = addressTown;
    }

	public String getAddressTown() {
		return addressTown;
	}

	public void setAddressProvince(String addressProvince) {
    	this.addressProvince = addressProvince;
    }

	public String getAddressProvince() {
		return addressProvince;
	}

	public void setAddressPostZip(String addressPostZip) {
    	this.addressPostZip = addressPostZip;
    }

	public String getAddressPostZip() {
		return addressPostZip;
	}

	public void setTelephoneNumber(long telephoneNumber) {
    	this.telephoneNumber = telephoneNumber;
    }

	public long getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setFaxNumber(long faxNumber) {
    	this.faxNumber = faxNumber;
    }

	public long getFaxNumber() {
		return faxNumber;
	}

	public void setAddedUser(String addedUser) {
    	this.addedUser = addedUser;
    }

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
    	this.addedDate = addedDate;
    }

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
    	this.addedTime = addedTime;
    }

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
    	this.changedUser = changedUser;
    }

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
    	this.changedDate = changedDate;
    }

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
    	this.changedTime = changedTime;
    }

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void setRecordSelected(RecordSelectedEnum recordSelected) {
		this.recordSelected = recordSelected;
	}

	public RecordSelectedEnum getRecordSelected() {
		return recordSelected;
	}
 
	public void setRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		this.recordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum getRecordDataChanged() {
		return recordDataChanged;
	}

}