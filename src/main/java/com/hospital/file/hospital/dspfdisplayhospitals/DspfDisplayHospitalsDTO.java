package com.hospital.file.hospital.dspfdisplayhospitals;
 
import java.math.BigDecimal;
  
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.common.utils.RestResponsePage;

import com.hospital.file.hospital.Hospital;
import com.hospital.model.CountryEnum;
import com.hospital.common.state.BaseDTO;

/**
 * Dto for file 'Hospital' (TSACREP) and function 'DSPF Display Hospitals' (TSA8DFR).
 */
public class DspfDisplayHospitalsDTO extends BaseDTO {
	private static final long serialVersionUID = -425598135059984971L;
	private String selector = "";
	private RestResponsePage<DspfDisplayHospitalsGDO> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";
 
	private CountryEnum country = null;
	private String countryName = "";
	private String hospitalCode = "";


	private DspfDisplayHospitalsGDO gdo;

    public DspfDisplayHospitalsDTO() {
 
    }
			
	public DspfDisplayHospitalsDTO(CountryEnum country) {
		this.country = country;
	}

	public void setSelector(String doctorCode) {
		this.selector = doctorCode;
	}

	public String getSelector() {
		return selector;
	}

	public void setPageDto(RestResponsePage<DspfDisplayHospitalsGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<DspfDisplayHospitalsGDO> getPageDto() {
		return pageDto;
	}
 
	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

   public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }
 
    public String getSortData() {
        return sortData;
    }
 
	public void setCountry(CountryEnum country) {
		this.country = country;
    }

    public CountryEnum getCountry() {
    	return country;
    }
 
	public void setCountryName(String countryName) {
		this.countryName = countryName;
    }

    public String getCountryName() {
    	return countryName;
    }
 
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
    }

    public String getHospitalCode() {
    	return hospitalCode;
    }
 
	public void setGdo(DspfDisplayHospitalsGDO gdo) {
		this.gdo = gdo;
	}

	public DspfDisplayHospitalsGDO getGdo() {
		return gdo;
	}
 
}