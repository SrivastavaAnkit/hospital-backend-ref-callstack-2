package com.hospital.file.hospital.eredithospital;

import java.io.IOException;
import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.redisson.codec.MsgPackJacksonCodec;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DefaultMessageCodesResolver;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ValidationUtils;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;


import com.hospital.file.hospital.Hospital;
import com.hospital.file.hospital.HospitalId;
import com.hospital.file.hospital.HospitalRepository;
import com.hospital.file.hospital.changehospital.ChangeHospitalService;
import com.hospital.file.hospital.changehospital.ChangeHospitalDTO;
import com.hospital.file.hospital.createhospital.CreateHospitalService;
import com.hospital.file.hospital.createhospital.CreateHospitalDTO;
import com.hospital.file.hospital.deletehospital.DeleteHospitalService;
import com.hospital.file.hospital.selecthospital.SelectHospitalDTO;
import com.hospital.file.hospital.selecthospital.SelectHospitalParams;
import com.hospital.file.hospital.selecthospital.SelectHospitalService;
import com.hospital.file.hospital.deletehospital.DeleteHospitalDTO;
import com.hospital.file.doctor.dspfdoctorsforhospital.DspfDoctorsForHospitalService;
import com.hospital.file.patient.dsppatientsperhospital.DspPatientsPerHospitalService;
import com.hospital.file.ward.dspwardsperhospital2.DspWardsPerHospital2Service;
import com.hospital.file.ward.editward.EditWardService;
import com.hospital.file.doctor.dspfdoctorsforhospital.DspfDoctorsForHospitalDTO;
import com.hospital.file.patient.dsppatientsperhospital.DspPatientsPerHospitalDTO;
import com.hospital.file.ward.dspwardsperhospital2.DspWardsPerHospital2DTO;
import com.hospital.file.ward.editward.EditWardDTO;
import com.hospital.file.doctor.dspfdoctorsforhospital.DspfDoctorsForHospitalParams;
import com.hospital.file.patient.dsppatientsperhospital.DspPatientsPerHospitalParams;
import com.hospital.file.ward.dspwardsperhospital2.DspWardsPerHospital2Params;
import com.hospital.file.ward.editward.EditWardParams;


import com.hospital.common.exception.ServiceException;

import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ProgramModeEnum;
import com.hospital.model.CountryEnum;
import com.hospital.model.DeferConfirmEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;
import com.hospital.support.JobContext;
/**
 * Service controller for 'ER Edit Hospital' (TSAJE1R) of file 'Hospital' (TSACREP)
 *
 * @author X2EGenerator
 */
@Service
public class ErEditHospitalService extends AbstractService<ErEditHospitalService, ErEditHospitalState>
{
    
    @Autowired
    private JobContext job;
    
    @Autowired
    private HospitalRepository hospitalRepository;
    
    @Autowired
    private ChangeHospitalService changeHospitalService;
    
    @Autowired
    private CreateHospitalService createHospitalService;
    
    @Autowired
    private DeleteHospitalService deleteHospitalService;
    
    @Autowired
	private MessageSource messageSource;
        
    public static final String SCREEN_KEY = "erEditHospitalEntryPanel";
    public static final String SCREEN_DTL = "erEditHospitalPanel";
    public static final String SCREEN_CFM = "ErEditHospital.confirm";
    
    private final Step execute = define("execute", ErEditHospitalParams.class, this::execute);
    private final Step keyScreenResponse = define("keyScreen", ErEditHospitalDTO.class, this::processKeyScreenResponse);
    private final Step detailScreenResponse = define("dtlScreen", ErEditHospitalDTO.class, this::processDetailScreenResponse);
    private final Step confirmScreenResponse = define("cfmScreen", ErEditHospitalDTO.class, this::processConfirmScreenResponse);
    private final Step promptSelectHospital = define("promptSelectHospital",SelectHospitalParams.class, this::processPromptSelectHospital);
    private final Step serviceDspWardsPerHospital2 = define("serviceDspWardsPerHospital2",DspWardsPerHospital2Params.class, this::processServiceDspWardsPerHospital2);
    private final Step serviceDspPatientsPerHospital = define("serviceDspPatientsPerHospital",DspPatientsPerHospitalParams.class, this::processServiceDspPatientsPerHospital);
    private final Step serviceDspfDoctorsForHospital = define("serviceDspfDoctorsForHospital",DspfDoctorsForHospitalParams.class, this::processServiceDspfDoctorsForHospital);
    //private final Step serviceDeleteHospital = define("serviceDeleteHospital",DeleteHospitalParams.class, this::processServiceDeleteHospital);
    //private final Step serviceCreateHospital = define("serviceCreateHospital",CreateHospitalParams.class, this::processServiceCreateHospital);
    //private final Step serviceChangeHospital = define("serviceChangeHospital",ChangeHospitalParams.class, this::processServiceChangeHospital);
    //private final Step serviceEditWard = define("serviceEditWard",EditWardParams.class, this::processServiceEditWard);
    //private final Step serviceRsaMustBe4DigitsCode = define("serviceRsaMustBe4DigitsCode",RsaMustBe4DigitsCodeParams.class, this::processServiceRsaMustBe4DigitsCode);
    //private final Step serviceInvalidUkPostalCode = define("serviceInvalidUkPostalCode",InvalidUkPostalCodeParams.class, this::processServiceInvalidUkPostalCode);
    //private final Step serviceInvalidUsaPostalCode = define("serviceInvalidUsaPostalCode",InvalidUsaPostalCodeParams.class, this::processServiceInvalidUsaPostalCode);
    //private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
    //private final Step serviceInvalidCanPostalCode = define("serviceInvalidCanPostalCode",InvalidCanPostalCodeParams.class, this::processServiceInvalidCanPostalCode);
    //private final Step serviceRtvcnd = define("serviceRtvcnd",RtvcndParams.class, this::processServiceRtvcnd);
    //private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
    //private final Step serviceQuit = define("serviceQuit",QuitParams.class, this::processServiceQuit);
    
        @Autowired
    public ErEditHospitalService()
    {
        super(ErEditHospitalService.class, ErEditHospitalState.class);
    }
    
    @Override
    public Step getInitialStep()
    {
        return execute;
    }
    
    /**
     * EDTRCD controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(ErEditHospitalState state, ErEditHospitalParams params)
    {
        StepResult result = NO_ACTION;
    
        if(params != null) {
            BeanUtils.copyProperties(params, state);
        }
    
        state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
    
        result = conductKeyScreenConversation(state);
    
        return result;
    }
    
    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductKeyScreenConversation(ErEditHospitalState state) 
    {
        StepResult result = NO_ACTION;
    
        result = displayKeyScreenConversation(state);
    
        return result;
    }
    
    /**
     * SCREEN_KEY display processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult displayKeyScreenConversation(ErEditHospitalState state)
    {
        StepResult result = NO_ACTION;

        if (state.get_SysConductKeyScreenConversation()) {
            ErEditHospitalDTO model = new ErEditHospitalDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_KEY, model).thenCall(keyScreenResponse);
        }
    
        return result;
    }
    
    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processKeyScreenResponse(ErEditHospitalState state, ErEditHospitalDTO model)
    {
        StepResult result = NO_ACTION;
        state.setMessageMap(null);

        BeanUtils.copyProperties(model, state);  
        state.getMessageMap().clear();
        
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(state, "model");
        
        if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {

            result = displayKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            result = conductKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
           result = closedown(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
    		switch (state.get_SysEntrySelected())
    		    {
    		        case "hospitalCode":
    		            SelectHospitalParams selectHospitalParams = new SelectHospitalParams();
    		            BeanUtils.copyProperties(state, selectHospitalParams);
    		            result = StepResult.callService(SelectHospitalService.class, selectHospitalParams).thenCall(promptSelectHospital);
    		            break;
    		        default:
    		            System.out.println("Field "+ state.get_SysEntrySelected() + " is not promptable");
    		            result = displayKeyScreenConversation(state);
    		            break;
    		    }
        }
        else if(CmdKeyEnum.isChangeMode(state.get_SysCmdKey())) {
            if(state.get_SysProgramMode() == ProgramModeEnum._STA_ADD) {
                state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            }
            else {
                state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
            }
            result = conductKeyScreenConversation(state);
        }
        else {
        	checkKeyFields(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = displayKeyScreenConversation(state);
                return result;
            }
            dbfReadDataRecord(state);
            if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
                usrInitializeExistingScreen(state);
            }
            else {
                usrInitializeNewScreen(state);
            }
    
            result = conductDetailScreenConversation(state);
        }
    
        return result;
    }
    
    /**
     * Check key fields set in SCREEN_KEY.
     * For now it's just if fields are null or strings are empty
     * @param state - Service state class.
     * @return
     */
    private void checkKeyFields(ErEditHospitalState state) {
       if(state.getHospitalCode() == null || state.getHospitalCode().isEmpty())
       {
           state.set_SysErrorFound(true);
           state.getMessageMap().put("hospitalCode", 
    		   messageSource.getMessage("value.required", null, null, null));
           state.setMessageMap(state.getMessageMap());
       }
    }
    
    /**
     * SCREEN_DTL display processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductDetailScreenConversation(ErEditHospitalState state)
    {
        StepResult result = NO_ACTION;
    
        if (state.get_SysConductDetailScreenConversation()) {
            ErEditHospitalDTO model = new ErEditHospitalDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_DTL, model).thenCall(detailScreenResponse);
        }
    
        return result;
    }
    
    /**
     * SCREEN_DETAIL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processDetailScreenResponse(ErEditHospitalState state, ErEditHospitalDTO model)
    {
        StepResult result = NO_ACTION;
        model.getMessageMap().clear();
        BeanUtils.copyProperties(model, state);
    
        if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            result = conductDetailScreenConversation(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            result = conductDetailScreenConversation(state);
        }
        else if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            result = closedown(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
    
        }
        else if (CmdKeyEnum.isKeyScreen(state.get_SysCmdKey())) {
            result = conductKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isDelete(state.get_SysCmdKey())) {
            //delete action confirmed in client side
            dbfDeleteDataRecord(state);
            result = conductDetailScreenConversation(state);
        }
        else {
            checkFields(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = conductDetailScreenConversation(state);
                return result;
            }
            usrValidateDetailScreenFields(state);
            checkRelations(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = conductDetailScreenConversation(state);
                return result;
            }
            usrValidateDetailScreenRelations(state);
            //TODO: make confirm screen
            result = processConfirmScreenResponse(state, null);//callScreen(SCREEN_CFM, state).thenCall(confirmScreenResponse);
        }
    
        return result;
    }
    /**
     * SCREEN_CONFIRM returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processConfirmScreenResponse(ErEditHospitalState state, ErEditHospitalDTO model)
    {
        StepResult result = NO_ACTION;
    
        if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
            dbfUpdateDataRecord(state);
        }
        else {
            dbfCreateDataRecord(state);
        }
    
        result = usrProcessCommandKeys(state);
        if (result != StepResult.NO_ACTION) {
            return result;
        }
        result = conductKeyScreenConversation(state);
    
        return result;
    }
    
    /*
     * Check fields set in SCREEN_DETAIL.
     * @param state - Service state class.
     * @return
     */
    private void checkFields(ErEditHospitalState state) {
    
    }
    
    /**
     * Check relations set in SCREEN_DETAIL.
     * @param state - Service state class.
     * @return
     */
    private void checkRelations(ErEditHospitalState state) {
    
    }
    
    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(ErEditHospitalState state)
    {
        StepResult result = NO_ACTION;
    
        result = usrExitCommandProcessing(state);
    
        ErEditHospitalParams params = new ErEditHospitalParams();
        BeanUtils.copyProperties(state, params);
        result = StepResult.returnFromService(params);
    
        return result;
    }
        
    /**
     * ------------------------ Generated DBF methods ----------------------
     */
    
    private StepResult dbfReadDataRecord(ErEditHospitalState dto)
    {
        StepResult result = NO_ACTION;
    
        HospitalId hospitalId = new HospitalId(dto.getHospitalCode());
        Hospital hospital = hospitalRepository.findById(hospitalId).orElse(null);
    
        if (hospital == null) {
            dto.set_SysProgramMode(ProgramModeEnum._STA_ADD);
        }
        else {
            dto.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            BeanUtils.copyProperties(hospital, dto);
        }
        return result;
    }
    
    private StepResult dbfCreateDataRecord(ErEditHospitalState dto)
    {
        StepResult result = NO_ACTION;
        try {
            /**
    		 * USER: Create Object (Generated:425)
    		 */
    		CreateHospitalDTO createHospitalDTO;
    		//switchBLK 425 BLK ACT
    		// DEBUG genFunctionCall 426 ACT Create Hospital - Hospital  *
    		// DEBUG genFunctionCall ServiceDtoVariable
    		createHospitalDTO = new CreateHospitalDTO();
    		// DEBUG genFunctionCall Parameters IN
    		createHospitalDTO.setHospitalCode(dto.getHospitalCode());
    		createHospitalDTO.setHospitalName(dto.getHospitalName());
    		createHospitalDTO.setAddressStreet(dto.getAddressStreet());
    		createHospitalDTO.setAddressTown(dto.getAddressTown());
    		createHospitalDTO.setAddressProvince(dto.getAddressProvince());
    		createHospitalDTO.setAddressPostZip(dto.getAddressPostZip());
    		createHospitalDTO.setTelephoneNumber(dto.getTelephoneNumber());
    		createHospitalDTO.setFaxNumber(dto.getFaxNumber());
    		createHospitalDTO.setCountry(dto.getCountry());
    		// DEBUG genFunctionCall Service call
    		createHospitalService.execute(createHospitalDTO);
    		// DEBUG genFunctionCall Parameters OUT
    		dto.setLclUsrReturnCode(createHospitalDTO.getUsrReturnCode());
    		// DEBUG genFunctionCall Parameters DONE
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }
    private StepResult dbfDeleteDataRecord(ErEditHospitalState dto)
    {
        StepResult result = NO_ACTION;
        try {
            /**
    		 * USER: Delete Object (Generated:383)
    		 */
    		DeleteHospitalDTO deleteHospitalDTO;
    		//switchBLK 383 BLK ACT
    		// DEBUG genFunctionCall 384 ACT Delete Hospital - Hospital  *
    		// DEBUG genFunctionCall ServiceDtoVariable
    		deleteHospitalDTO = new DeleteHospitalDTO();
    		// DEBUG genFunctionCall Parameters IN
    		deleteHospitalDTO.setHospitalCode(dto.getHospitalCode());
    		// DEBUG genFunctionCall Service call
    		deleteHospitalService.execute(deleteHospitalDTO);
    		// DEBUG genFunctionCall Parameters OUT
    		// DEBUG genFunctionCall Parameters DONE
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    
        return result;
    }
    
    private StepResult dbfUpdateDataRecord(ErEditHospitalState dto)
    {
        StepResult result = NO_ACTION;
        try {
    		HospitalId hospitalId = new HospitalId(dto.getHospitalCode());
    		if (hospitalRepository.existsById(hospitalId)) {
    
            	/**
    			 * USER: Change Object (Generated:427)
    			 */
    			ChangeHospitalDTO changeHospitalDTO;
    			//switchBLK 427 BLK ACT
    			// DEBUG genFunctionCall 428 ACT Change Hospital - Hospital  *
    			// DEBUG genFunctionCall ServiceDtoVariable
    			changeHospitalDTO = new ChangeHospitalDTO();
    			// DEBUG genFunctionCall Parameters IN
    			changeHospitalDTO.setHospitalCode(dto.getHospitalCode());
    			changeHospitalDTO.setHospitalName(dto.getHospitalName());
    			changeHospitalDTO.setAddressStreet(dto.getAddressStreet());
    			changeHospitalDTO.setAddressTown(dto.getAddressTown());
    			changeHospitalDTO.setAddressProvince(dto.getAddressProvince());
    			changeHospitalDTO.setAddressPostZip(dto.getAddressPostZip());
    			changeHospitalDTO.setTelephoneNumber(dto.getTelephoneNumber());
    			changeHospitalDTO.setFaxNumber(dto.getFaxNumber());
    			changeHospitalDTO.setCountry(dto.getCountry());
    			// DEBUG genFunctionCall Service call
    			changeHospitalService.execute(changeHospitalDTO);
    			// DEBUG genFunctionCall Parameters OUT
    			dto.setLclUsrReturnCode(changeHospitalDTO.getUsrReturnCode());
    			// DEBUG genFunctionCall Parameters DONE
    	        }
            else {
                throw new ServiceException("diagnosis.nf");
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }
    
    
/**
 * ------------------ Generated ActionDiagram UserPoint -----------------
 */

    private StepResult usrInitializeNewScreen(ErEditHospitalState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Initialize Detail Screen (New Record) (Generated:239)
			 */
			//switchSUB 239 SUB    
			//switchBLK 1000204 BLK ACT
			// DEBUG genFunctionCall 1000205 ACT WRK.Number 4 = CON.*ZERO
			dto.setWfNumber4(0);
			//switchBLK 1000208 BLK ACT
			// DEBUG genFunctionCall 1000209 ACT WRK.Number 5 = CON.*ZERO
			dto.setWfNumber5(0);
			//switchBLK 1000214 BLK ACT
			// DEBUG genFunctionCall 1000215 ACT WRK.Number 7 = CON.*ZERO
			dto.setWfNumber7(0);
			//switchBLK 1000218 BLK ACT
			// DEBUG genFunctionCall 1000219 ACT WRK.Alpha 6 = CON.*BLANK
			dto.setWfAlpha6("");
			//switchBLK 1000278 BLK ACT
			// DEBUG genFunctionCall 1000279 ACT DTL.Hospital Country = Condition name of DTL.Country Name
			dto.setCountry(CountryEnum.fromCode(dto.getCountryName())); // Retrieve condition

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrInitializeExistingScreen(ErEditHospitalState dto)
    {
        StepResult result = NO_ACTION;
        //Hello
        try
        {
           /**
			 * Initialize Detail Screen (Existing Record) (Generated:242)
			 */
			//switchSUB 242 SUB    
			//switchBLK 1000222 BLK ACT
			// DEBUG genFunctionCall 1000223 ACT WRK.Number 4 = CON.*ZERO
			dto.setWfNumber4(0); 
			//switchBLK 1000226 BLK ACT
			// DEBUG genFunctionCall 1000227 ACT WRK.Number 5 = CON.*ZERO
			dto.setWfNumber5(0);
			//switchBLK 1000230 BLK ACT
			// DEBUG genFunctionCall 1000231 ACT WRK.Number 7 = CON.*ZERO
			dto.setWfNumber7(0);
			//switchBLK 1000234 BLK ACT
			// DEBUG genFunctionCall 1000235 ACT WRK.Alpha 6 = CON.*BLANK
			dto.setWfAlpha6("");

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrValidateDetailScreenFields(ErEditHospitalState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Validate Detail Screen Fields (Generated:502)
			 */
			if (dto.getCountry() == CountryEnum.fromCode("RSA")) {
				// DTL.Hospital Country is South Africa
				if (dto.getAddressPostZip().trim().compareTo("1") >= 0 && dto.getAddressPostZip().trim().compareTo("9999") <= 0 ) {
					// DTL.Hospital Address Post/Zip is Range 1 to 9999
				} else {
					// *OTHERWISE
					dto.getMessageMap().put("addressPostZip", messageSource.getMessage("rsa.must.be.4.digits.code", null, null, null)); //TODO: Process error in Controller (ActionDiagramGenrator::generateSendErrorMsg).
				}
				// 
			} else if (dto.getCountry() == CountryEnum.fromCode("UK")) {
				// DTL.Hospital Country is United Kingdom
				if (dto.getAddressPostZip().trim().compareTo("1") >= 0 && dto.getAddressPostZip().trim().compareTo("9999999") <= 0 ) {
					// DTL.Hospital Address Post/Zip is Range 1 to 9999999
				} else {
					// *OTHERWISE
					dto.getMessageMap().put("addressPostZip", messageSource.getMessage("invalid.uk.postal.code", null, null, null)); //TODO: Process error in Controller (ActionDiagramGenrator::generateSendErrorMsg).
				}
				// 
			} else if (dto.getCountry() == CountryEnum.fromCode("USA")) {
				// DTL.Hospital Country is United States of America
				if (dto.getAddressPostZip().trim().compareTo("1") >= 0 && dto.getAddressPostZip().trim().compareTo("99999") <= 0 ) {
					// DTL.Hospital Address Post/Zip is Range 1 to 99999
				} else {
					// *OTHERWISE
					dto.getMessageMap().put("addressPostZip", messageSource.getMessage("invalid.usa.postal.code", null, null, null)); //TODO: Process error in Controller (ActionDiagramGenrator::generateSendErrorMsg).
				}
				// 
			} else if (dto.getCountry() == CountryEnum.fromCode("CAN")) {
				// DTL.Hospital Country is Canada
				dto.setWfAlpha6(dto.getAddressPostZip());
				if (!dto.getWfAlpha6().equals(dto.getAddressPostZip())) {
					// WRK.Alpha 6 NE DTL.Hospital Address Post/Zip
					dto.getMessageMap().put("addressPostZip", messageSource.getMessage("invalid.can.postal.code", null, null, null)); //TODO: Process error in Controller (ActionDiagramGenrator::generateSendErrorMsg).
				}
			}
			// 
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			} else {
				// *OTHERWISE
				dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
			}

            if (dto.getMessageMap().size() > 0) {
                dto.set_SysErrorFound(true);
                dto.setMessageMap(dto.getMessageMap());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrValidateDetailScreenRelations(ErEditHospitalState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Validate Detail Screen Relations (Generated:268)
			 */
			//switchSUB 268 SUB    
			//switchBLK 1000371 BLK CAS
			//switchSUB 1000371 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				//switchBLK 1000405 BLK ACT
				// DEBUG genFunctionCall 1000406 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000392 BLK ACT
				// DEBUG genFunctionCall 1000393 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrProcessCommandKeys(ErEditHospitalState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Process Command Keys (Generated:446)
			 */
			//switchSUB 446 SUB    
			//switchBLK 1000293 BLK CAS
			//switchSUB 1000293 BLK CAS
			if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("06")) {
				// DTL.*CMD key is CF06
				//switchBLK 1000296 BLK ACT
				// DEBUG genFunctionCall 1000297 ACT DSP Wards per Hospital 2 - Ward  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				DspWardsPerHospital2Params dspWardsPerHospital2Params = new DspWardsPerHospital2Params();
				BeanUtils.copyProperties(dto, dspWardsPerHospital2Params);
				result = StepResult.callService(DspWardsPerHospital2Service.class, dspWardsPerHospital2Params).thenCall(serviceDspWardsPerHospital2);
				}
				//switchBLK 1000331 BLK CAS
				//switchSUB 1000331 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000339 BLK ACT
					// DEBUG genFunctionCall 1000340 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000420 BLK ACT
					// DEBUG genFunctionCall 1000421 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000337 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000334 BLK ACT
					// DEBUG genFunctionCall 1000335 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
			//switchBLK 1000398 BLK TXT
			// 
			//switchBLK 1000302 BLK CAS
			//switchSUB 1000302 BLK CAS
			if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("09")) {
				// DTL.*CMD key is CF09
				//switchBLK 1000305 BLK ACT
				// DEBUG genFunctionCall 1000306 ACT DSP Patients per Hospital - Patient  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				DspPatientsPerHospitalParams dspPatientsPerHospitalParams = new DspPatientsPerHospitalParams();
				BeanUtils.copyProperties(dto, dspPatientsPerHospitalParams);
				result = StepResult.callService(DspPatientsPerHospitalService.class, dspPatientsPerHospitalParams).thenCall(serviceDspPatientsPerHospital);
				}
				//switchBLK 1000345 BLK CAS
				//switchSUB 1000345 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000348 BLK ACT
					// DEBUG genFunctionCall 1000349 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000400 BLK ACT
					// DEBUG genFunctionCall 1000401 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000352 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000354 BLK ACT
					// DEBUG genFunctionCall 1000355 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
			//switchBLK 1000399 BLK TXT
			// 
			//switchBLK 1000284 BLK CAS
			//switchSUB 1000284 BLK CAS
			if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("10")) {
				// DTL.*CMD key is CF10
				//switchBLK 1000287 BLK ACT
				// DEBUG genFunctionCall 1000288 ACT DSPF Doctors for Hospital - Doctor  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				DspfDoctorsForHospitalParams dspfDoctorsForHospitalParams = new DspfDoctorsForHospitalParams();
				BeanUtils.copyProperties(dto, dspfDoctorsForHospitalParams);
				result = StepResult.callService(DspfDoctorsForHospitalService.class, dspfDoctorsForHospitalParams).thenCall(serviceDspfDoctorsForHospital);
				}
				//switchBLK 1000357 BLK CAS
				//switchSUB 1000357 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000360 BLK ACT
					// DEBUG genFunctionCall 1000361 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000402 BLK ACT
					// DEBUG genFunctionCall 1000403 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000364 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000366 BLK ACT
					// DEBUG genFunctionCall 1000367 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
			//switchBLK 1000404 BLK TXT
			// 
			//switchBLK 1000001 BLK CAS

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrExitCommandProcessing(ErEditHospitalState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Exit Program Processing (Generated:64)
			 */
			//switchSUB 64 SUB    
			//switchBLK 1000264 BLK CAS
			//switchSUB 1000264 BLK CAS
			if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03")) {
				// KEY.*CMD key is CF03
				//switchBLK 1000414 BLK ACT
				// DEBUG genFunctionCall 1000415 ACT PAR.*Return code = CND.*Normal
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000411 BLK ACT
				// DEBUG genFunctionCall 1000412 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}//switchSUB 1000267 SUB    
			 else {
				// *OTHERWISE
				//switchBLK 1000269 BLK ACT
				// DEBUG genFunctionCall 1000270 ACT Edit Ward - Ward  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				EditWardParams editWardParams = new EditWardParams();
				BeanUtils.copyProperties(dto, editWardParams);
				//result = StepResult.callService(EditWardService.class, editWardParams);//.thenCall(serviceEditWard)
				}
				//switchBLK 1000423 BLK CAS
				//switchSUB 1000423 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000426 BLK ACT
					// DEBUG genFunctionCall 1000427 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000432 BLK ACT
					// DEBUG genFunctionCall 1000433 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000434 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000438 BLK ACT
					// DEBUG genFunctionCall 1000439 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }


    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */

    /**
     * DspWardsPerHospital2Service returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspWardsPerHospital2(ErEditHospitalState state, DspWardsPerHospital2Params serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        result = conductKeyScreenConversation(state);
        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }

    /**
     * DspPatientsPerHospitalService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspPatientsPerHospital(ErEditHospitalState state, DspPatientsPerHospitalParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }
        result =conductKeyScreenConversation(state);

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }

    /**
     * DspfDoctorsForHospitalService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspfDoctorsForHospital(ErEditHospitalState state, DspfDoctorsForHospitalParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }
        result = conductKeyScreenConversation(state);
        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }
//
//    /**
//     * DeleteHospitalService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceDeleteHospital(ErEditHospitalState state, DeleteHospitalParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * CreateHospitalService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateHospital(ErEditHospitalState state, CreateHospitalParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ChangeHospitalService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChangeHospital(ErEditHospitalState state, ChangeHospitalParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * EditWardService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceEditWard(ErEditHospitalState state, EditWardParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RsaMustBe4DigitsCodeService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRsaMustBe4DigitsCode(ErEditHospitalState state, RsaMustBe4DigitsCodeParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * InvalidUkPostalCodeService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceInvalidUkPostalCode(ErEditHospitalState state, InvalidUkPostalCodeParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * InvalidUsaPostalCodeService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceInvalidUsaPostalCode(ErEditHospitalState state, InvalidUsaPostalCodeParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(ErEditHospitalState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * InvalidCanPostalCodeService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceInvalidCanPostalCode(ErEditHospitalState state, InvalidCanPostalCodeParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvcndService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvcnd(ErEditHospitalState state, RtvcndParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(ErEditHospitalState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * QuitService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceQuit(ErEditHospitalState state, QuitParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//

    /**
     * SelectPatientService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processPromptSelectHospital(ErEditHospitalState state, SelectHospitalParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        result = displayKeyScreenConversation(state);

        return result;
    }

}

//public class ErEditHospitalService extends AbstractService<ErEditHospitalService, ErEditHospitalState>
//{
//    
//    	@Autowired
//    	private HospitalRepository hospitalRepository;
//    
//    	@Autowired
//    	private ChangeHospitalService changeHospitalService;
//    
//    	@Autowired
//    	private CreateHospitalService createHospitalService;
//    
//    	@Autowired
//    	private DeleteHospitalService deleteHospitalService;
//    
//        
//    public static final String SCREEN_KEY = "erEditHospitalEntryPanel";
//    public static final String SCREEN_DTL = "erEditHospitalPanel";
//    public static final String SCREEN_CFM = "ErEditHospital.confirm";
//    
//    private final Step execute = define("execute", ErEditHospitalParams.class, this::execute);
//    private final Step keyScreenResponse = define("keyscreen", ErEditHospitalDTO.class, this::processKeyScreenResponse);
//    private final Step detailScreenResponse = define("dtlscreen", ErEditHospitalDTO.class, this::processDetailScreenResponse);
//    private final Step confirmScreenResponse = define("cfmscreen", ErEditHospitalDTO.class, this::processConfirmScreenResponse);
//    private final Step postCallSelectHospitalService = define("postCallSelectHospitalService", SelectHospitalDTO.class, this::postCallSelectHospitalService);
//
//    //private final Step serviceEditWard = define("serviceEditWard",EditWardParams.class, this::processServiceEditWard);
//    private final Step serviceDspWardsPerHospital2 = define("serviceDspWardsPerHospital2",DspWardsPerHospital2Params.class, this::processServiceDspWardsPerHospital2);
//    private final Step serviceDspPatientsPerHospital = define("serviceDspPatientsPerHospital",DspPatientsPerHospitalParams.class, this::processServiceDspPatientsPerHospital);
//    private final Step serviceDspfDoctorsForHospital = define("serviceDspfDoctorsForHospital",DspfDoctorsForHospitalParams.class, this::processServiceDspfDoctorsForHospital);
//        
//    @Autowired
//    public ErEditHospitalService()
//    {
//        super(ErEditHospitalService.class, ErEditHospitalState.class);
//    }
//    
//    @Override
//    public Step getInitialStep()
//    {
//        return execute;
//    }
//    
//    /**
//     * EDTRCD controller starting point.
//     * @param dto - Service state class.
//     * @param params - Service input/output parameters class.
//     * @return
//     */
//    private StepResult execute(ErEditHospitalState state, ErEditHospitalParams params)
//    {
//        StepResult result = NO_ACTION;
//    
//        BeanUtils.copyProperties(params, state);
//    
//        result = conductKeyScreenConversation(state);
//    
//        return result;
//    }
//    
//    /**
//     * SCREEN_KEY initial processing loop method.
//     * @param dto - Service state class.
//     * @return
//     */
//    private StepResult conductKeyScreenConversation(ErEditHospitalState state) 
//    {
//        StepResult result = NO_ACTION;
//    
//        state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
//    
//        result = displayKeyScreenConversation(state);
//    
//        return result;
//    }
//    
//    /**
//     * SCREEN_KEY display processing loop method.
//     * @param dto - Service state class.
//     * @return
//     */
//    private StepResult displayKeyScreenConversation(ErEditHospitalState state)
//    {
//        StepResult result = NO_ACTION;
//    
//        if (state.getConductKeyScreenConversation()) {
//        	 ErEditHospitalDTO model = new ErEditHospitalDTO();
//             BeanUtils.copyProperties(state, model);
//            result = callScreen(SCREEN_KEY, state).thenCall(keyScreenResponse);
//        }
//    
//        return result;
//    }
//    
//    /**
//     * SCREEN_KEY returned response processing method.
//     * @param dto - Service state class.
//     * @param model - returned screen model.
//     * @return
//     */
//    private StepResult processKeyScreenResponse(ErEditHospitalState state, ErEditHospitalDTO model)
//    {
//        StepResult result = NO_ACTION;
//      
//        BeanUtils.copyProperties(model, state);
//        if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
//          return  result = StepResult.callService(SelectHospitalService.class, new SelectHospitalParams()).thenCall(postCallSelectHospitalService);
//        }
//        else if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
//            result = displayKeyScreenConversation(state);
//        }
//        else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
//            result = conductKeyScreenConversation(state);
//        }
//        else if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
//            result = usrExitCommandProcessing(state);
//        }
//        else {
//            dbfReadDataRecord(state);
//            if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
//                usrInitializeExistingScreen(state);
//            }
//            else {
//                usrInitializeNewScreen(state);
//            }
//    
//            result = conductDetailScreenConversation(state);
//        }
//    
//        return result;
//    }
//    
//    /**
//     * SCREEN_DTL display processing loop method.
//     * @param dto - Service state class.
//     * @return
//     */
//    private StepResult conductDetailScreenConversation(ErEditHospitalState state)
//    {
//        StepResult result = NO_ACTION;
//    
//        if (state.getConductDetailScreenConversation()) {
//        	ErEditHospitalDTO model = new ErEditHospitalDTO();
//            BeanUtils.copyProperties(state, model);
//            result = callScreen(SCREEN_DTL, model).thenCall(detailScreenResponse);
//        }
//    
//        return result;
//    }
//    
//    /**
//     * SCREEN_DETAIL returned response processing method.
//     * @param dto - Service state class.
//     * @param model - returned screen model.
//     * @return
//     */
//    private StepResult processDetailScreenResponse(ErEditHospitalState state, ErEditHospitalDTO model)
//    {
//        StepResult result = NO_ACTION;
//    
//        BeanUtils.copyProperties(model, state);
//    
//        if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
//            return usrExitCommandProcessing(state);
//        }
//        else if (state.get_SysCmdKey() == CmdKeyEnum._CF12) {
//            BeanUtils.copyProperties(state, model);
//        	return result = callScreen(SCREEN_KEY, model).thenCall(keyScreenResponse);
//        }
//        if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
//            result = conductDetailScreenConversation(state);
//        }
//        else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
//            result = conductDetailScreenConversation(state);
//        }             
//        else {
//    //        if (CmdKeyEnum.isDelete(dto.get_SysCmdKey())) {
//    //            dto.setProgramMode(ProgramModeEnum._STA_DELETE);
//    //        }
//        	return processConfirmScreenResponse(state, model);
//            //result = callScreen(SCREEN_KEY, dto).thenCall(keyScreenResponse);
//        }
//    
//        return result;
//    }
//    
//    /*private StepResult USERPOINT_exitCommandProcessing(ErEditHospitalState state)
//    {
//        return StepResult.returnFromService(state);
//    }*/
//    
//    /**
//     * SCREEN_CONFIRM returned response processing method.
//     * @param dto - Service state class.
//     * @param model - returned screen model.
//     * @return
//     */
//    private StepResult processConfirmScreenResponse(ErEditHospitalState state, ErEditHospitalDTO model)
//    {
//        StepResult result = NO_ACTION;
//        if (CmdKeyEnum.isDelete(state.get_SysCmdKey())) {
//        	if (state.get_SysProgramMode() == ProgramModeEnum._STA_DELETE) {
//        		return dbfDeleteDataRecord(state);
//        	}
//        	return NO_ACTION;
//         }
//      else if (state.get_SysCmdKey() == CmdKeyEnum._CF06) {
//    	  DspWardsPerHospital2Params param= new DspWardsPerHospital2Params();
//    	  BeanUtils.copyProperties(state, param);
//    	return  result = StepResult.callService(DspWardsPerHospital2Service.class, param).thenCall(serviceDspWardsPerHospital2);  
//       }
//       else if (state.get_SysCmdKey() == CmdKeyEnum._CF09) {
//    	   DspPatientsPerHospitalParams param=new DspPatientsPerHospitalParams();
//    	   BeanUtils.copyProperties(state, param);
//    	   return  result = StepResult.callService(DspPatientsPerHospitalService.class, param).thenCall(serviceDspPatientsPerHospital)  ;
//       }else if (state.get_SysCmdKey() == CmdKeyEnum._CF10) {
//    	   DspfDoctorsForHospitalParams param=new DspfDoctorsForHospitalParams();
//    	   BeanUtils.copyProperties(state, param);
//    	   return  result = StepResult.callService(DspfDoctorsForHospitalService.class, param).thenCall(serviceDspfDoctorsForHospital);  
//       }              
//        else
//        {
//            usrValidateDetailScreenFields(state);
//            usrValidateDetailScreenRelations(state);            
//            if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
//                dbfUpdateDataRecord(state);
//            }
//            else {
//                dbfCreateDataRecord(state);
//            }
//        }        
//        result = conductKeyScreenConversation(state);
//    
//        return result;
//    }
//        
//    /**
//     * ------------------------ Generated DBF methods ----------------------
//     */
//    
//    private StepResult dbfReadDataRecord(ErEditHospitalState dto)
//    {
//        StepResult result = NO_ACTION;
//      
//        HospitalId hospitalId = new HospitalId(dto.getHospitalCode());
//        Optional<Hospital> hospital = hospitalRepository.findById(hospitalId);
//    
//        if (!hospital.isPresent()) {
//            dto.set_SysProgramMode(ProgramModeEnum._STA_ADD);
//        }
//        else {
//            dto.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
//            BeanUtils.copyProperties(hospital.get(), dto);
//        }
//        return result;
//    }
//    
//    private StepResult dbfCreateDataRecord(ErEditHospitalState dto)
//    {
//        StepResult result = NO_ACTION;
//        try {
//            CreateHospitalDTO createHospitalDTO = new CreateHospitalDTO();
//            createHospitalDTO.setHospitalCode(dto.getHospitalCode());
//            createHospitalDTO.setHospitalName(dto.getHospitalName());
//            createHospitalDTO.setAddressStreet(dto.getAddressStreet());
//            createHospitalDTO.setAddressTown(dto.getAddressTown());
//            createHospitalDTO.setAddressProvince(dto.getAddressProvince());
//            createHospitalDTO.setAddressPostZip(dto.getAddressPostZip());
//            createHospitalDTO.setTelephoneNumber(dto.getTelephoneNumber());
//            createHospitalDTO.setFaxNumber(dto.getFaxNumber());
//            createHospitalDTO.setCountry(dto.getCountry());
//    	    createHospitalService.execute(createHospitalDTO);
//             //dto.setUsrReturnCode(createHospitalDTO.getUsrReturnCode());
//        }
//        catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//        return result;
//    }
//    private StepResult dbfDeleteDataRecord(ErEditHospitalState dto)
//    {
//        StepResult result = NO_ACTION;
//        try {
//            DeleteHospitalDTO deleteHospitalDTO = new DeleteHospitalDTO();
//            deleteHospitalDTO.setHospitalCode(dto.getHospitalCode());
//    	    deleteHospitalService.execute(deleteHospitalDTO);
//        }
//        catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//        result = callScreen(SCREEN_KEY, dto).thenCall(keyScreenResponse);
//        return result;
//    }
//    
//    private StepResult dbfUpdateDataRecord(ErEditHospitalState dto)
//    {
//        StepResult result = NO_ACTION;
//        try {
//    	HospitalId hospitalId = new HospitalId(dto.getHospitalCode());
//    	if (hospitalRepository.existsById(hospitalId)) {
//                ChangeHospitalDTO changeHospitalDTO = new ChangeHospitalDTO();
//                changeHospitalDTO.setHospitalCode(dto.getHospitalCode());
//                changeHospitalDTO.setHospitalName(dto.getHospitalName());
//                changeHospitalDTO.setAddressStreet(dto.getAddressStreet());
//                changeHospitalDTO.setAddressTown(dto.getAddressTown());
//                changeHospitalDTO.setAddressProvince(dto.getAddressProvince());
//                changeHospitalDTO.setAddressPostZip(dto.getAddressPostZip());
//                changeHospitalDTO.setTelephoneNumber(dto.getTelephoneNumber());
//                changeHospitalDTO.setFaxNumber(dto.getFaxNumber());
//                changeHospitalDTO.setCountry(dto.getCountry());
//    	        changeHospitalService.execute(changeHospitalDTO);
//                // dto.setUsrReturnCode(changeHospitalDTO.getUsrReturnCode());
//            }
//            else {
//                throw new ServiceException("diagnosis.nf");
//            }
//        }
//        catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//        return result;
//    }
//    
//    
///**
// * ------------------ Generated ActionDiagram UserPoint -----------------
// */
//
//    private StepResult usrInitializeNewScreen(ErEditHospitalState dto)
//    {
//        try
//        {
//           /**
//			 * USER: Initialize Detail Screen (New Record) (Generated:239)
//			 */
//			//switchSUB 239 SUB    
//			//switchBLK 1000204 BLK ACT
//			//functionCall 1000205 ACT WRK.Number 4 = CON.*ZERO
//			//dto.setWfNumber4(0);
//			//switchBLK 1000208 BLK ACT
//			//functionCall 1000209 ACT WRK.Number 5 = CON.*ZERO
//			//dto.setWfNumber5(0);
//			//switchBLK 1000214 BLK ACT
//			//functionCall 1000215 ACT WRK.Number 7 = CON.*ZERO
//			//dto.setWfNumber7(0);
//			//switchBLK 1000218 BLK ACT
//			//functionCall 1000219 ACT WRK.Alpha 6 = CON.*BLANK
//			//dto.setWfAlpha6("");
//			//switchBLK 1000278 BLK ACT
//			//functionCall 1000279 ACT DTL.Hospital Country = Condition name of DTL.Country Name
//			dto.setCountry(CountryEnum.fromCode(dto.getCountryName())); // Retrieve condition
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//        return NO_ACTION;
//    }
//
//    private StepResult usrInitializeExistingScreen(ErEditHospitalState dto)
//    {
//        try
//        {
//           /**
//			 * Initialize Detail Screen (Existing Record) (Generated:242)
//			 */
//			//switchSUB 242 SUB    
//			//switchBLK 1000222 BLK ACT
//			//functionCall 1000223 ACT WRK.Number 4 = CON.*ZERO
//			//dto.setWfNumber4(0);
//			//switchBLK 1000226 BLK ACT
//			//functionCall 1000227 ACT WRK.Number 5 = CON.*ZERO
//			//dto.setWfNumber5(0);
//			//switchBLK 1000230 BLK ACT
//			//functionCall 1000231 ACT WRK.Number 7 = CON.*ZERO
//			//dto.setWfNumber7(0);
//			//switchBLK 1000234 BLK ACT
//			//functionCall 1000235 ACT WRK.Alpha 6 = CON.*BLANK
//			//dto.setWfAlpha6("");
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//        return NO_ACTION;
//    }
//
//    private StepResult usrValidateDetailScreenFields(ErEditHospitalState dto)
//    {
//        try
//        {
//           /**
//			 * USER: Validate Detail Screen Fields (Generated:502)
//			 */
//			//switchSUB 502 SUB    
//			//switchBLK 1000145 BLK CAS
//			//switchSUB 1000145 BLK CAS
//			if (dto.getCountry() == CountryEnum.fromCode("RSA")) {
//				// DTL.Hospital Country is South Africa
//				//switchBLK 1000243 BLK CAS
//				//switchSUB 1000243 BLK CAS
//				if (dto.getAddressPostZip().trim().compareTo("1") >= 0 && dto.getAddressPostZip().trim().compareTo("9999") <= 0 ) {
//					// DTL.Hospital Address Post/Zip is Range 1 to 9999
//				}//switchSUB 1000246 SUB    
//				 else {
//					// *OTHERWISE
//					//switchBLK 1000157 BLK ACT
//					//functionCall 1000158 ACT Send error message - 'RSA must be 4 digits code'
//			//		e.rejectValue("addressPostZip", "rsa.must.be.4.digits.code");
//				}
//				//switchBLK 1000395 BLK TXT
//				// 
//			} else //switchSUB 1000160 SUB    
//			if (dto.getCountry() == CountryEnum.fromCode("UK")) {
//				// DTL.Hospital Country is United Kingdom
//				//switchBLK 1000248 BLK CAS
//				//switchSUB 1000248 BLK CAS
//				if (dto.getAddressPostZip().trim().compareTo("1") >= 0 && dto.getAddressPostZip().trim().compareTo("9999999") <= 0 ) {
//					// DTL.Hospital Address Post/Zip is Range 1 to 9999999
//				}//switchSUB 1000256 SUB    
//				 else {
//					// *OTHERWISE
//					//switchBLK 1000171 BLK ACT
//					//functionCall 1000172 ACT Send error message - 'Invalid UK Postal code'
//				//	e.rejectValue("addressPostZip", "invalid.uk.postal.code");
//				}
//				//switchBLK 1000396 BLK TXT
//				// 
//			} else //switchSUB 1000174 SUB    
//			if (dto.getCountry() == CountryEnum.fromCode("USA")) {
//				// DTL.Hospital Country is United States of America
//				//switchBLK 1000258 BLK CAS
//				//switchSUB 1000258 BLK CAS
//				if (dto.getAddressPostZip().trim().compareTo("1") >= 0 && dto.getAddressPostZip().trim().compareTo("99999") <= 0 ) {
//					// DTL.Hospital Address Post/Zip is Range 1 to 99999
//				}//switchSUB 1000261 SUB    
//				 else {
//					// *OTHERWISE
//					//switchBLK 1000185 BLK ACT
//					//functionCall 1000186 ACT Send error message - 'Invalid USA Postal Code'
//				//	e.rejectValue("addressPostZip", "invalid.usa.postal.code");
//				}
//				//switchBLK 1000397 BLK TXT
//				// 
//			} else //switchSUB 1000188 SUB    
//			if (dto.getCountry() == CountryEnum.fromCode("CAN")) {
//				// DTL.Hospital Country is Canada
//				//switchBLK 1000190 BLK ACT
//				//functionCall 1000191 ACT WRK.Alpha 6 = DTL.Hospital Address Post/Zip
//				//dto.setWfAlpha6(dto.getAddressPostZip());
//				//switchBLK 1000194 BLK CAS
//				//switchSUB 1000194 BLK CAS
//				//if (!dto.getWfAlpha6().equals(dto.getAddressPostZip())) {
//					// WRK.Alpha 6 NE DTL.Hospital Address Post/Zip
//					//switchBLK 1000198 BLK ACT
//					//functionCall 1000199 ACT Send error message - 'Invalid CAN Postal code'
//					//e.rejectValue("addressPostZip", "invalid.can.postal.code");
//				//}
//			}
//			//switchBLK 1000444 BLK TXT
//			// 
//			//switchBLK 1000380 BLK CAS
//			//switchSUB 1000380 BLK CAS
//			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
//				// DTL.*CMD key is *Cancel
//				//switchBLK 1000385 BLK ACT
//				//functionCall 1000386 ACT Exit program - return code CND.E
//				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
//				//switchBLK 1000441 BLK ACT
//				//functionCall 1000442 ACT Exit program - return code CND.*Normal
//				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
//			}//switchSUB 1000383 SUB    
//			 else {
//				// *OTHERWISE
//				//switchBLK 1000200 BLK ACT
//				//functionCall 1000201 ACT PGM.*Reload subfile = CND.*YES
//				//dto.set_sysR(ReloadSubfileEnum.fromCode("Y"));
//			}
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//        return NO_ACTION;
//    }
//
//    private StepResult usrValidateDetailScreenRelations(ErEditHospitalState dto)
//    {
//        try
//        {
//           /**
//			 * USER: Validate Detail Screen Relations (Generated:268)
//			 */
//			//switchSUB 268 SUB    
//			//switchBLK 1000371 BLK CAS
//			//switchSUB 1000371 BLK CAS
//			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
//				// DTL.*CMD key is *Cancel
//				//switchBLK 1000405 BLK ACT
//				//functionCall 1000406 ACT PAR.*Return code = CND.E
//				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
//				//switchBLK 1000392 BLK ACT
//				//functionCall 1000393 ACT Exit program - return code CND.*Normal
//				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
//			}
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//        return NO_ACTION;
//    }
//
//    private StepResult usrExitCommandProcessing(ErEditHospitalState dto)
//    {
//        try
//        {
//           /**
//			 * USER: Exit Program Processing (Generated:64)
//			 */
//			//switchSUB 64 SUB    
//			//switchBLK 1000264 BLK CAS
//			//switchSUB 1000264 BLK CAS
//			if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03")) {
//				// KEY.*CMD key is CF03
//				//switchBLK 1000414 BLK ACT
//				//functionCall 1000415 ACT PAR.*Return code = CND.*Normal
//				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
//				//switchBLK 1000411 BLK ACT
//				//functionCall 1000412 ACT Exit program - return code CND.*Normal
//				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
//			}//switchSUB 1000267 SUB    
//			 else {
//				// *OTHERWISE
//				//switchBLK 1000269 BLK ACT
//				//functionCall 1000270 ACT Edit Ward - Ward  *
//				//TODO: split
//				EditWardParams editWardParams = new EditWardParams();
//				BeanUtils.copyProperties(dto, editWardParams);
//				//result = StepResult.callService(EditWardService.class, editWardParams);//.thenCall(serviceEditWardDisplay)
//				//switchBLK 1000423 BLK CAS
//				//switchSUB 1000423 BLK CAS
//				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
//					// LCL.*Return code is E
//					//switchBLK 1000426 BLK ACT
//					//functionCall 1000427 ACT PGM.*Reload subfile = CND.*YES
//					//dto.setReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
//					//switchBLK 1000432 BLK ACT
//					//functionCall 1000433 ACT <-- *QUIT
//					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
//				}//switchSUB 1000434 SUB    
//				 else {
//					// *OTHERWISE
//					//switchBLK 1000438 BLK ACT
//					//functionCall 1000439 ACT Exit program - return code CND.*Normal
//					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
//				}
//			}
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//        return StepResult.returnFromService(dto);
//    }
//
//
//    /**
//     * ---------------------- Programmatic user-point: process call service --------------------------
//     */
////
////    /**
////     * EditWardService returned response processing method.
////     * @param dto - Service state class.
////     * @param serviceResult - returned service model.
////     * @return
////     */
////    private StepResult processServiceEditWard(ErEditHospitalDTO dto, EditWardParams serviceResult)
////    {
////        StepResult result = NO_ACTION;
////
////        if(serviceResult != null) {
////            BeanUtils.copyProperties(serviceResult, dto);
////        }
////
////        return result;
////    }
////
////    /**
////     * DspWardsPerHospital2Service returned response processing method.
////     * @param dto - Service state class.
////     * @param serviceResult - returned service model.
////     * @return
////     */
//    private StepResult processServiceDspWardsPerHospital2(ErEditHospitalDTO dto, DspWardsPerHospital2Params serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        return result;
//    }
////
////    /**
////     * DspPatientsPerHospitalService returned response processing method.
////     * @param dto - Service state class.
////     * @param serviceResult - returned service model.
////     * @return
////     */
//    private StepResult processServiceDspPatientsPerHospital(ErEditHospitalDTO dto, DspPatientsPerHospitalParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//        ErEditHospitalState state=new ErEditHospitalState();
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//        
//        result=conductKeyScreenConversation(state);
//        return result;
//    }
//
//    /**
//     * DspfDoctorsForHospitalService returned response processing method.
//     * @param dto - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceDspfDoctorsForHospital(ErEditHospitalDTO dto, DspfDoctorsForHospitalParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        return result;
//    }
//
//    private StepResult postCallSelectHospitalService(ErEditHospitalState dto, SelectHospitalDTO serviceResult) {
//    	dto=new ErEditHospitalState();
//    	BeanUtils.copyProperties(serviceResult, dto);
//    	return displayKeyScreenConversation(dto);
//    }
//    
//}
