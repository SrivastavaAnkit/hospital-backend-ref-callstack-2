package com.hospital.file.hospital.eredithospital;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;

import com.hospital.common.state.MessageDTO;
import com.hospital.file.hospital.changehospital.ChangeHospitalService;
import com.hospital.file.hospital.createhospital.CreateHospitalService;
import com.hospital.file.hospital.deletehospital.DeleteHospitalService;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.support.JobContext;

/**
 * Spring Validator for file 'Hospital' (TSACREP) and function 'ER Edit Hospital' (TSAJE1R).
 *
 * @author X2EGenerator
 */
@Component
public class ErEditHospitalValidator/* implements Validator*/ {
	
	
@Autowired
private ChangeHospitalService changeHospitalService;
	
@Autowired
private CreateHospitalService createHospitalService;
	
@Autowired
private DeleteHospitalService deleteHospitalService;
	

	@Autowired
	private JobContext job;
	
	@Autowired
	private MessageSource messageSource;

	public boolean supports(Class<?> clazz) {
		return ErEditHospitalState.class.equals(clazz);
	}

	public void validate(Object object, Errors e) {
		if (!(object instanceof ErEditHospitalState)) {
			e.reject("Not a valid ErEditHospitalState");
		} else {
			ErEditHospitalState dto = (ErEditHospitalState)object;

			/**
			 * Validate Key Fields
			 *
			 */
			if ("".equals(dto.getHospitalCode())) {
				MessageDTO messageDTO =  new MessageDTO();
				messageDTO.setCode("hospitalCode");
				messageDTO.setField("value.required");
				messageDTO.setMessage("Value required");
				dto.setMessage(messageDTO.getCode(), messageDTO.getMessage());
				dto.setMessage(messageDTO.getMessage(), messageDTO.getCode());
				//e.rejectValue("hospitalCode", "value.required");
				//dto.setMessage(messageDTO);
				//String errorMessage = messageSource.getMessage(messageDTO.getField(), null, messageDTO.getField(), Locale.ENGLISH);
			}
			try {
				/**
				 * USER: Validate Detail Screen Fields (Generated:502)
				 */
				//switchSUB 502 SUB    
				//switchBLK 1000145 BLK CAS
				//switchSUB 1000145 BLK CAS
				if (dto.getCountry() == CountryEnum.fromCode("RSA")) {
					// DTL.Hospital Country is South Africa
					//switchBLK 1000243 BLK CAS
					//switchSUB 1000243 BLK CAS
					if (dto.getAddressPostZip().trim().compareTo("1") >= 0 && dto.getAddressPostZip().trim().compareTo("9999") <= 0 ) {
						// DTL.Hospital Address Post/Zip is Range 1 to 9999
					}//switchSUB 1000246 SUB    
					 else {
						// *OTHERWISE
						//switchBLK 1000157 BLK ACT
						// DEBUG genFunctionCall 1000158 ACT Send error message - 'RSA must be 4 digits code'
						//e.rejectValue("addressPostZip", "rsa.must.be.4.digits.code"); //TODO: Process error in Controller (ActionDiagramGenrator::generateSendErrorMsg).
					}
					//switchBLK 1000395 BLK TXT
					// 
				} else //switchSUB 1000160 SUB    
				if (dto.getCountry() == CountryEnum.fromCode("UK")) {
					// DTL.Hospital Country is United Kingdom
					//switchBLK 1000248 BLK CAS
					//switchSUB 1000248 BLK CAS
					if (dto.getAddressPostZip().trim().compareTo("1") >= 0 && dto.getAddressPostZip().trim().compareTo("9999999") <= 0 ) {
						// DTL.Hospital Address Post/Zip is Range 1 to 9999999
					}//switchSUB 1000256 SUB    
					 else {
						// *OTHERWISE
						//switchBLK 1000171 BLK ACT
						// DEBUG genFunctionCall 1000172 ACT Send error message - 'Invalid UK Postal code'
						//e.rejectValue("addressPostZip", "invalid.uk.postal.code"); //TODO: Process error in Controller (ActionDiagramGenrator::generateSendErrorMsg).
					}
					//switchBLK 1000396 BLK TXT
					// 
				} else //switchSUB 1000174 SUB    
				if (dto.getCountry() == CountryEnum.fromCode("USA")) {
					// DTL.Hospital Country is United States of America
					//switchBLK 1000258 BLK CAS
					//switchSUB 1000258 BLK CAS
					if (dto.getAddressPostZip().trim().compareTo("1") >= 0 && dto.getAddressPostZip().trim().compareTo("99999") <= 0 ) {
						// DTL.Hospital Address Post/Zip is Range 1 to 99999
					}//switchSUB 1000261 SUB    
					 else {
						// *OTHERWISE
						//switchBLK 1000185 BLK ACT
						// DEBUG genFunctionCall 1000186 ACT Send error message - 'Invalid USA Postal Code'
						//e.rejectValue("addressPostZip", "invalid.usa.postal.code"); //TODO: Process error in Controller (ActionDiagramGenrator::generateSendErrorMsg).
					}
					//switchBLK 1000397 BLK TXT
					// 
				} else //switchSUB 1000188 SUB    
				if (dto.getCountry() == CountryEnum.fromCode("CAN")) {
					// DTL.Hospital Country is Canada
					//switchBLK 1000190 BLK ACT
					// DEBUG genFunctionCall 1000191 ACT WRK.Alpha 6 = DTL.Hospital Address Post/Zip
					/*dto.setWfAlpha6(dto.getAddressPostZip());
					//switchBLK 1000194 BLK CAS
					//switchSUB 1000194 BLK CAS
					if (!dto.getWfAlpha6().equals(dto.getAddressPostZip())) {
						// WRK.Alpha 6 NE DTL.Hospital Address Post/Zip
						//switchBLK 1000198 BLK ACT
						// DEBUG genFunctionCall 1000199 ACT Send error message - 'Invalid CAN Postal code'
						//e.rejectValue("addressPostZip", "invalid.can.postal.code"); //TODO: Process error in Controller (ActionDiagramGenrator::generateSendErrorMsg).
					}*/
				}
				//switchBLK 1000444 BLK TXT
				// 
				//switchBLK 1000380 BLK CAS
				//switchSUB 1000380 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
					// DTL.*CMD key is *Cancel
					//switchBLK 1000385 BLK ACT
					// DEBUG genFunctionCall 1000386 ACT Exit program - return code CND.E
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
					//switchBLK 1000441 BLK ACT
					// DEBUG genFunctionCall 1000442 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}//switchSUB 1000383 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000200 BLK ACT
					// DEBUG genFunctionCall 1000201 ACT PGM.*Reload subfile = CND.*YES
					//dto.setReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
				}
			} catch (Exception se) {
				se.printStackTrace();
			}
			try {
				/**
				 * USER: Validate Detail Screen Relations (Generated:268)
				 */
				//switchSUB 268 SUB    
				//switchBLK 1000371 BLK CAS
				//switchSUB 1000371 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
					// DTL.*CMD key is *Cancel
					//switchBLK 1000405 BLK ACT
					// DEBUG genFunctionCall 1000406 ACT PAR.*Return code = CND.E
					dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
					//switchBLK 1000392 BLK ACT
					// DEBUG genFunctionCall 1000393 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			} catch (Exception se) {
				se.printStackTrace();
			}
		}
	}
}
