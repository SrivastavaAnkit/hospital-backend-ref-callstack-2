package com.hospital.file.hospital.eredithospital;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.MessageSource;

import com.hospital.file.hospital.Hospital;
import com.hospital.model.GlobalContext;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

/**
 * State for file 'Hospital' (TSACREP) and function 'ER Edit Hospital' (TSAJE1R).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
@Configurable
public class ErEditHospitalState extends ErEditHospitalDTO {
    private static final long serialVersionUID = 2584109543969866551L;


   // @Autowired
    private GlobalContext globalCtx=new GlobalContext();


    // Local fields
    private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");
    private UsrReturnCodeEnum lclUsrReturnCode = null;

    // System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysConductKeyScreenConversation = true;
    private boolean _sysConductDetailScreenConversation = true;
    private boolean _sysErrorFound = false;

    public ErEditHospitalState() {
    }

    public ErEditHospitalState(Hospital hospital) {
        setDtoFields(hospital);
    }

    public void setWfAlpha6(String alpha6) {
        globalCtx.setString("alpha6", alpha6);
    }

    public String getWfAlpha6() {
        return globalCtx.getString("alpha6");
    }

    public void setWfNumber4(long number4) {
        globalCtx.setLong("number4", number4);
    }

    public long getWfNumber4() {
        return globalCtx.getLong("number4");
    }

    public void setWfNumber5(long number5) {
        globalCtx.setLong("number5", number5);
    }

    public long getWfNumber5() {
        return globalCtx.getLong("number5");
    }

    public void setWfNumber7(long number7) {
        globalCtx.setLong("number7", number7);
    }

    public long getWfNumber7() {
        return globalCtx.getLong("number7");
    }

    public void setLclReturnCode(ReturnCodeEnum returnCode) {
        this.lclReturnCode = returnCode;
    }

    public ReturnCodeEnum getLclReturnCode() {
        return lclReturnCode;
    }

    public void setLclUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
        this.lclUsrReturnCode = usrReturnCode;
    }

    public UsrReturnCodeEnum getLclUsrReturnCode() {
        return lclUsrReturnCode;
    }

   public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
       _sysReloadSubfile = reloadSubfile;
   }

   public ReloadSubfileEnum get_SysReloadSubfileEnum() {
       return _sysReloadSubfile;
   }

    public boolean get_SysConductKeyScreenConversation() {
        return _sysConductKeyScreenConversation;
    }

    public void set_SysConductKeyScreenConversation(boolean conductKeyScreenConversation) {
        _sysConductKeyScreenConversation = conductKeyScreenConversation;
    }

    public boolean get_SysConductDetailScreenConversation() {
        return _sysConductDetailScreenConversation;
    }

    public void set_SysConductDetailScreenConversation(boolean conductDetailScreenConversation) {
        _sysConductDetailScreenConversation = conductDetailScreenConversation;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }
  
    
}
