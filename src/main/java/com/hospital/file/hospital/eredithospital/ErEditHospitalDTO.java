package com.hospital.file.hospital.eredithospital;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.common.state.BaseDTO;
import com.hospital.file.hospital.Hospital;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;



/**
 * DTO for file 'Hospital' (TSACREP) and function 'ER Edit Hospital' (TSAJE1R).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
public class ErEditHospitalDTO extends BaseDTO {
    private static final long serialVersionUID = 3904550329873548099L;

    private long version = 0;
    private String hospitalCode = "";
    private String sflselPromptText = "";
    private String hospitalName = "";
    private String addressStreet = "";
    private String addressTown = "";
    private String addressProvince = "";
    private String hospitalAddressCountry = "";
    private String addressPostZip = "";
    private CountryEnum country = CountryEnum.fromCode("");
    private String addedUser = "";
    private LocalDate addedDate = null;
    private LocalTime addedTime = null;
    private String changedUser = "";
    private LocalDate changedDate = null;
    private LocalTime changedTime = null;
    private String countryName = "";
    private long telephoneNumber = 0L;
    private long faxNumber = 0L;
    private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");

    public ErEditHospitalDTO() {
    }

    public ErEditHospitalDTO(Hospital hospital) {
        setDtoFields(hospital);
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public long getVersion() {
        return version;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getSflselPromptText() {
        return sflselPromptText;
    }

    public void setSflselPromptText(String sflselPromptText) {
        this.sflselPromptText = sflselPromptText;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public String getAddressTown() {
        return addressTown;
    }

    public void setAddressTown(String addressTown) {
        this.addressTown = addressTown;
    }

    public String getAddressProvince() {
        return addressProvince;
    }

    public void setAddressProvince(String addressProvince) {
        this.addressProvince = addressProvince;
    }

    public String getHospitalAddressCountry() {
        return hospitalAddressCountry;
    }

    public void setHospitalAddressCountry(String hospitalAddressCountry) {
        this.hospitalAddressCountry = hospitalAddressCountry;
    }

    public String getAddressPostZip() {
        return addressPostZip;
    }

    public void setAddressPostZip(String addressPostZip) {
        this.addressPostZip = addressPostZip;
    }

    public CountryEnum getCountry() {
        return country;
    }

    public void setCountry(CountryEnum country) {
        this.country = country;
    }

    public String getAddedUser() {
        return addedUser;
    }

    public void setAddedUser(String addedUser) {
        this.addedUser = addedUser;
    }

    public LocalDate getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(LocalDate addedDate) {
        this.addedDate = addedDate;
    }

    public LocalTime getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(LocalTime addedTime) {
        this.addedTime = addedTime;
    }

    public String getChangedUser() {
        return changedUser;
    }

    public void setChangedUser(String changedUser) {
        this.changedUser = changedUser;
    }

    public LocalDate getChangedDate() {
        return changedDate;
    }

    public void setChangedDate(LocalDate changedDate) {
        this.changedDate = changedDate;
    }

    public LocalTime getChangedTime() {
        return changedTime;
    }

    public void setChangedTime(LocalTime changedTime) {
        this.changedTime = changedTime;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public long getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(long telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public long getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(long faxNumber) {
        this.faxNumber = faxNumber;
    }

    public ReturnCodeEnum getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(ReturnCodeEnum returnCode) {
        this.returnCode = returnCode;
    }

    /**
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param hospital Hospital Entity bean
     */
    public void setDtoFields(Hospital hospital) {
        this.version = hospital.getVersion();
        BeanUtils.copyProperties(hospital, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param hospital Hospital Entity bean
     */
    public void setEntityFields(Hospital hospital) {
        hospital.setVersion(this.version);
        BeanUtils.copyProperties(this, hospital);
    }
}
