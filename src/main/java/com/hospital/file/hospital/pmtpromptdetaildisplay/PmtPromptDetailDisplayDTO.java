package com.hospital.file.hospital.pmtpromptdetaildisplay;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;

import com.hospital.file.hospital.Hospital;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.CountryEnum;
import com.hospital.model.DeferConfirmEnum;
import com.hospital.model.ProgramModeEnum;
import com.hospital.model.UsrReturnCodeEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.common.state.BaseDTO;
import com.hospital.common.state.MessageDTO;

/**
 * Dto for file 'Hospital' (TSACREP) and function 'PMT Prompt detail display' (TSAQPVR).
 */
public class PmtPromptDetailDisplayDTO extends BaseDTO {
	private static final long serialVersionUID = 2810948952295753433L;

	private String hospitalCode = "";
	private String hospitalName = "";
	private String addressStreet = "";
	private String addressTown = "";
	private String addressProvince = "";
	private String addressPostZip = "";
	private long telephoneNumber = 0L;
	private long faxNumber = 0L;
	private CountryEnum country = null;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;
	private UsrReturnCodeEnum usrReturnCode = null;

	// Local fields
	private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");
	private String lclHospitalCode = "";
	private String lclLogFunctionType = "";
	private String lclLogUserPoint = "";

	private ReloadSubfileEnum reloadSubfile = null;

	public PmtPromptDetailDisplayDTO() {

	}

	public PmtPromptDetailDisplayDTO(Hospital hospital) {
		BeanUtils.copyProperties(hospital, this);
	}

	public PmtPromptDetailDisplayDTO(String hospitalCode, String hospitalName, String addressStreet, String addressTown, String addressProvince, String addressPostZip, long telephoneNumber, long faxNumber, CountryEnum country, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime) {
		this.hospitalCode = hospitalCode;
		this.hospitalName = hospitalName;
		this.addressStreet = addressStreet;
		this.addressTown = addressTown;
		this.addressProvince = addressProvince;
		this.addressPostZip = addressPostZip;
		this.telephoneNumber = telephoneNumber;
		this.faxNumber = faxNumber;
		this.country = country;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}

	public String getAddressStreet() {
		return addressStreet;
	}

	public void setAddressTown(String addressTown) {
		this.addressTown = addressTown;
	}

	public String getAddressTown() {
		return addressTown;
	}

	public void setAddressProvince(String addressProvince) {
		this.addressProvince = addressProvince;
	}

	public String getAddressProvince() {
		return addressProvince;
	}

	public void setAddressPostZip(String addressPostZip) {
		this.addressPostZip = addressPostZip;
	}

	public String getAddressPostZip() {
		return addressPostZip;
	}

	public void setTelephoneNumber(long telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public long getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setFaxNumber(long faxNumber) {
		this.faxNumber = faxNumber;
	}

	public long getFaxNumber() {
		return faxNumber;
	}

	public void setCountry(CountryEnum country) {
		this.country = country;
	}

	public CountryEnum getCountry() {
		return country;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void setUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
		this.usrReturnCode = usrReturnCode;
	}

	public UsrReturnCodeEnum getUsrReturnCode() {
		return usrReturnCode;
	}

	public void setLclReturnCode(ReturnCodeEnum returnCode) {
		this.lclReturnCode = returnCode;
	}

	public ReturnCodeEnum getLclReturnCode() {
		return lclReturnCode;
	}

	public void setLclHospitalCode(String hospitalCode) {
		this.lclHospitalCode = hospitalCode;
	}

	public String getLclHospitalCode() {
		return lclHospitalCode;
	}

	public void setLclLogFunctionType(String logFunctionType) {
		this.lclLogFunctionType = logFunctionType;
	}

	public String getLclLogFunctionType() {
		return lclLogFunctionType;
	}

	public void setLclLogUserPoint(String logUserPoint) {
		this.lclLogUserPoint = logUserPoint;
	}

	public String getLclLogUserPoint() {
		return lclLogUserPoint;
	}

	public void setReloadSubfile(ReloadSubfileEnum reloadSubfile) {
		this.reloadSubfile = reloadSubfile;
	}

	public ReloadSubfileEnum getReloadSubfile() {
		return reloadSubfile;
	}
    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param hospital Hospital Entity bean
     */
    public void setDtoFields(Hospital hospital) {
        BeanUtils.copyProperties(hospital, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param hospital Hospital Entity bean
     */
    public void setEntityFields(Hospital hospital) {
        BeanUtils.copyProperties(this, hospital);
    }
}
