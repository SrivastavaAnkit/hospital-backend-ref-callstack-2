package com.hospital.file.hospital.pmtpromptdetaildisplay;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;

import com.hospital.file.hospital.HospitalRepository;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailService;
import com.hospital.file.hospital.selecthospital.SelectHospitalService;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.ward.dspwardsperhospital.DspWardsPerHospitalService;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailDTO;
import com.hospital.file.hospital.selecthospital.SelectHospitalDTO;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.support.JobContext;
import com.hospital.file.hospital.selecthospital.SelectHospitalParams;
import com.hospital.file.ward.dspwardsperhospital.DspWardsPerHospitalParams;

import com.hospital.model.DeferConfirmEnum;


/**
 * Service for file 'Hospital' (TSACREP) and function 'PMT Prompt detail display' (TSAQPVR).
 */
@Service
public class PmtPromptDetailDisplayService extends AbstractService<PmtPromptDetailDisplayService,PmtPromptDetailDisplayState> {
    
    @Autowired
    private JobContext job;

    @Autowired
    private MessageSource messageSource;

	@Autowired
	private HospitalRepository hospitalRepository;

    
    public static final String SCREEN_KEY = "pmtPromptDetailDisplay";
    public static final String SCREEN_CONFIRM_PROMPT = "PmtPromptDetailDisplay.key2";

    private final Step execute = define("execute",PmtPromptDetailDisplayParams.class, this::execute);
    private final Step response = define("response",PmtPromptDetailDisplayDTO.class, this::processResponse);
//    private final Step displayConfirmPrompt = define("displayConfirmPrompt",PmtPromptDetailDisplayDTO.class, this::processDisplayConfirmPrompt);

	private final Step promptSelectHospital = define("promptSelectHospital",SelectHospitalParams.class, this::processPromptSelectHospital);
	
	private final Step serviceSelectHospital = define("serviceSelectHospital",SelectHospitalParams.class, this::processServiceSelectHospital);
	private final Step serviceDspWardsPerHospital = define("serviceDspWardsPerHospital",DspWardsPerHospitalParams.class, this::processServiceDspWardsPerHospital);
	//private final Step serviceRtvHospitalDetail = define("serviceRtvHospitalDetail",RtvHospitalDetailParams.class, this::processServiceRtvHospitalDetail);
	//private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
	//private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
	//private final Step serviceQuit = define("serviceQuit",QuitParams.class, this::processServiceQuit);
	//private final Step serviceFieldRequired = define("serviceFieldRequired",FieldRequiredParams.class, this::processServiceFieldRequired);
	//private final Step serviceCreateLogFile = define("serviceCreateLogFile",CreateLogFileParams.class, this::processServiceCreateLogFile);
	
    	
@Autowired
private CreateLogFileService createLogFileService;
	
@Autowired
private RtvHospitalDetailService rtvHospitalDetailService;
	
    @Autowired
    public PmtPromptDetailDisplayService()
    {
        super(PmtPromptDetailDisplayService.class, PmtPromptDetailDisplayState.class);
    }

    @Override
    public Step getInitialStep()
    {
        return execute;
    }

    /**
     * PmtPromptDetailDisplay service starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(PmtPromptDetailDisplayState state, PmtPromptDetailDisplayParams params)  {
        StepResult result = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        initialize(state);
        result = conductScreenConversation(state);

        return result;
    }

    /**
     * PmtPromptDetailDisplay service initialization.
     * @param state   - Service state class.
     * @return
     */
    private void initialize(PmtPromptDetailDisplayState state) {
        usrInitializeProgram(state);
        usrScreenFunctionFields(state);
        usrLoadScreen(state);
    }

    /**
     * SCREEN_KEY display processing loop method.
     * @param state   - Service state class.
     * @return
     */
    private StepResult conductScreenConversation(PmtPromptDetailDisplayState state)
    {
        StepResult result = NO_ACTION;

        PmtPromptDetailDisplayDTO dto = new PmtPromptDetailDisplayDTO();
        BeanUtils.copyProperties(state, dto);
        result = callScreen(SCREEN_KEY, dto).thenCall(response);

        return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param dto - returned screen model.
     * @return
     */
    private StepResult processResponse(PmtPromptDetailDisplayState state, PmtPromptDetailDisplayDTO dto)
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(dto, state);
        if (CmdKeyEnum.isExit(state.get_SysCmdKey()))
        {
            result = closedown(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey()))
        {
            //TODO: processResetRequest(state);//synon built-in function
            result = conductScreenConversation(state);
        }
        else if (CmdKeyEnum.isHelp(state.get_SysCmdKey()))
        {
            //TODO:processHelpRequest(state);//synon built-in function
            result = conductScreenConversation(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
			switch (state.get_SysEntrySelected())
			    {
			        case "hospitalCode":
			            SelectHospitalParams selectHospitalParams = new SelectHospitalParams();
			            BeanUtils.copyProperties(state, selectHospitalParams);
			            result = StepResult.callService(SelectHospitalService.class, selectHospitalParams).thenCall(promptSelectHospital);
			            break;
			        default:
			            System.out.println("Field "+ state.get_SysEntrySelected() + " is not promptable");
			            result = conductScreenConversation(state);
			            break;
			    }
        }
        else
        {
            validateScreenInput(state);
            if (!state.get_SysErrorFound()){
                //BeanUtils.copyProperties(state, dto);
                //TODO: for now, confirm prompt is skipped
                result = usrUserDefinedAction(state);//callScreen(SCREEN_CONFIRM_PROMPT, dto).thenCall(displayConfirmPrompt);
                if(result != NO_ACTION) {
                    return result;
                }
            }
            result = conductScreenConversation(state);
        }

        return result;
    }

//    /**
//     * SCREEN_CONFIRM_PROMPT returned response processing method.
//     * @param state - Service state class.
//     * @param dto - returned screen model.
//     * @return
//     */
//    private StepResult processDisplayConfirmPrompt(PmtPromptDetailDisplayState state, PmtPromptDetailDisplayDTO dto) {
//        StepResult result = NO_ACTION;
//
//        BeanUtils.copyProperties(dto, state);
//        if (!state.getDeferConfirm().getCode().equals(DeferConfirmEnum._PROCEED_TO_CONFIRM.getCode()))
//        {
//            usrUserDefinedAction(state);
//        }
//        result = conductScreenConversation(state);
//
//        return result;
//    }

    /**
     * Validate input in SCREEN_KEY.
     * @param state - Service state class.
     * @return
     */
    private void validateScreenInput(PmtPromptDetailDisplayState state)
    {
        usrProcessCommandKeys(state);
        checkFields(state);
        if (!state.get_SysErrorFound())
        {
            usrValidateFields(state);
            checkRelations(state);
            if (!state.get_SysErrorFound())
            {
                usrScreenFunctionFields(state);
                usrValidateRelations(state);
            }
        }
    }

    /**
     * Check fields set in SCREEN_KEY.
     * @param state - Service state class.
     * @return
     */
    private void checkFields(PmtPromptDetailDisplayState state) {

    }

    /**
     * Check relations set in SCREEN_KEY.
     * @param state - Service state class.
     * @return
     */
    private void checkRelations(PmtPromptDetailDisplayState state) {

    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(PmtPromptDetailDisplayState state)
    {
        StepResult result = NO_ACTION;

        result = usrExitProgramProcessing(state);
        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);

        PmtPromptDetailDisplayParams params = new PmtPromptDetailDisplayParams();
        BeanUtils.copyProperties(state, params);
        result = StepResult.returnFromService(params);

        return result;
    }


    
/**
 * ---------------------- Programmatic user-point --------------------------
 */
	/**
	 * USER: Initialize Program (Generated:10)
	 */
    private StepResult usrInitializeProgram(PmtPromptDetailDisplayState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			//switchSUB 10 SUB    
			//switchBLK 1000069 BLK ACT
			// DEBUG genFunctionCall 1000070 ACT LCL.Hospital Code = CON.*BLANK
			dto.setLclHospitalCode("");
			//switchBLK 1000101 BLK TXT
			// 
			//switchBLK 1000102 BLK ACT
			// DEBUG genFunctionCall 1000103 ACT LCL.Log Function Type = CON.PMTRCD
			dto.setLclLogFunctionType("PMTRCD");
			//switchBLK 1000106 BLK ACT
			// DEBUG genFunctionCall 1000107 ACT LCL.Log User Point = CON.Initialize program
			dto.setLclLogUserPoint("Initialize program");
			//switchBLK 1000116 BLK ACT
			// DEBUG genFunctionCall 1000117 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Screen Function Fields (Generated:1052)
	 */
    private StepResult usrScreenFunctionFields(PmtPromptDetailDisplayState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			RtvHospitalDetailDTO rtvHospitalDetailDTO;
			//switchSUB 1052 SUB    
			//switchBLK 1000082 BLK ACT
			// DEBUG genFunctionCall 1000083 ACT RTV Hospital detail - Hospital  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvHospitalDetailDTO = new RtvHospitalDetailDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvHospitalDetailDTO.setHospitalCode(dto.getHospitalCode());
			// DEBUG genFunctionCall Service call
			rtvHospitalDetailService.execute(rtvHospitalDetailDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setHospitalName(rtvHospitalDetailDTO.getHospitalName());
			dto.setAddressStreet(rtvHospitalDetailDTO.getAddressStreet());
			dto.setAddressTown(rtvHospitalDetailDTO.getAddressTown());
			dto.setAddressProvince(rtvHospitalDetailDTO.getAddressProvince());
			dto.setAddressPostZip(rtvHospitalDetailDTO.getAddressPostZip());
			dto.setTelephoneNumber(rtvHospitalDetailDTO.getTelephoneNumber());
			dto.setFaxNumber(rtvHospitalDetailDTO.getFaxNumber());
			dto.setCountry(rtvHospitalDetailDTO.getCountry());
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000211 BLK TXT
			// 
			//switchBLK 1000212 BLK ACT
			// DEBUG genFunctionCall 1000213 ACT LCL.Log Function Type = CON.PMTRCD
			dto.setLclLogFunctionType("PMTRCD");
			//switchBLK 1000216 BLK ACT
			// DEBUG genFunctionCall 1000217 ACT LCL.Log User Point = CON.Screen function fields
			dto.setLclLogUserPoint("Screen function fields");
			//switchBLK 1000226 BLK ACT
			// DEBUG genFunctionCall 1000227 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Load Screen (Generated:1058)
	 */
    private StepResult usrLoadScreen(PmtPromptDetailDisplayState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			RtvHospitalDetailDTO rtvHospitalDetailDTO;
			//switchSUB 1058 SUB    
			//switchBLK 1000007 BLK ACT
			// DEBUG genFunctionCall 1000008 ACT RTV Hospital detail - Hospital  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvHospitalDetailDTO = new RtvHospitalDetailDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvHospitalDetailDTO.setHospitalCode(dto.getHospitalCode());
			// DEBUG genFunctionCall Service call
			rtvHospitalDetailService.execute(rtvHospitalDetailDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setHospitalName(rtvHospitalDetailDTO.getHospitalName());
			dto.setAddressStreet(rtvHospitalDetailDTO.getAddressStreet());
			dto.setAddressTown(rtvHospitalDetailDTO.getAddressTown());
			dto.setAddressProvince(rtvHospitalDetailDTO.getAddressProvince());
			dto.setAddressPostZip(rtvHospitalDetailDTO.getAddressPostZip());
			dto.setTelephoneNumber(rtvHospitalDetailDTO.getTelephoneNumber());
			dto.setFaxNumber(rtvHospitalDetailDTO.getFaxNumber());
			dto.setCountry(rtvHospitalDetailDTO.getCountry());
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000145 BLK TXT
			// 
			//switchBLK 1000146 BLK ACT
			// DEBUG genFunctionCall 1000147 ACT LCL.Log Function Type = CON.PMTRCD
			dto.setLclLogFunctionType("PMTRCD");
			//switchBLK 1000150 BLK ACT
			// DEBUG genFunctionCall 1000151 ACT LCL.Log User Point = CON.Load screen
			dto.setLclLogUserPoint("Load screen");
			//switchBLK 1000160 BLK ACT
			// DEBUG genFunctionCall 1000161 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Generated:1035)
	 */
    private StepResult usrProcessCommandKeys(PmtPromptDetailDisplayState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			//switchSUB 1035 SUB    
			//switchBLK 1000001 BLK CAS
			//switchSUB 1000001 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("04"))) {
				// DTL.*CMD key is *Prompt
				//switchBLK 1000004 BLK ACT
				// DEBUG genFunctionCall 1000005 ACT Select Hospital - Hospital  *
				//TODO: commented below line
				//dto.setNextScreen("SelectHospital");
			}
			//switchBLK 1000167 BLK TXT
			// 
			//switchBLK 1000168 BLK ACT
			// DEBUG genFunctionCall 1000169 ACT LCL.Log Function Type = CON.PMTRCD
			dto.setLclLogFunctionType("PMTRCD");
			//switchBLK 1000172 BLK ACT
			// DEBUG genFunctionCall 1000173 ACT LCL.Log User Point = CON.Process command keys
			dto.setLclLogUserPoint("Process command keys");
			//switchBLK 1000182 BLK ACT
			// DEBUG genFunctionCall 1000183 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Fields (Generated:1065)
	 */
    private StepResult usrValidateFields(PmtPromptDetailDisplayState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			//switchSUB 1065 SUB    
			//switchBLK 1000075 BLK CAS
			//switchSUB 1000075 BLK CAS
			if (dto.getHospitalCode() != null && dto.getHospitalCode().isEmpty()) {
				// DTL.Hospital Code is Blank
				//switchBLK 1000080 BLK ACT
				// DEBUG genFunctionCall 1000081 ACT Send error message - 'Field required'
				//e.rejectValue("", "field.required");
			}
			//switchBLK 1000189 BLK TXT
			// 
			//switchBLK 1000190 BLK ACT
			// DEBUG genFunctionCall 1000191 ACT LCL.Log Function Type = CON.PMTRCD
			dto.setLclLogFunctionType("PMTRCD");
			//switchBLK 1000194 BLK ACT
			// DEBUG genFunctionCall 1000195 ACT LCL.Log User Point = CON.Validate fields
			dto.setLclLogUserPoint("Validate fields");
			//switchBLK 1000204 BLK ACT
			// DEBUG genFunctionCall 1000205 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Relations (Generated:29)
	 */
    private StepResult usrValidateRelations(PmtPromptDetailDisplayState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			//switchSUB 29 SUB    
			//switchBLK 1000093 BLK CAS
			//switchSUB 1000093 BLK CAS
			if (dto.getHospitalCode().equals("PUBLIC")) {
				// DTL.Hospital Code is Equal Public
				//switchBLK 1000096 BLK CAS
				//switchSUB 1000096 BLK CAS
				if (dto.getAddressProvince().equals("QC")) {
					// DTL.Address Province is Quebec
					//switchBLK 1000099 BLK ACT
					// DEBUG genFunctionCall 1000100 ACT Send error message - 'Field required'
					//e.rejectValue("", "field.required");
				}
			}
			//switchBLK 1000233 BLK TXT
			// 
			//switchBLK 1000234 BLK ACT
			// DEBUG genFunctionCall 1000235 ACT LCL.Log Function Type = CON.PMTRCD
			dto.setLclLogFunctionType("PMTRCD");
			//switchBLK 1000238 BLK ACT
			// DEBUG genFunctionCall 1000239 ACT LCL.Log User Point = CON.Validate relations
			dto.setLclLogUserPoint("Validate relations");
			//switchBLK 1000248 BLK ACT
			// DEBUG genFunctionCall 1000249 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: User Defined Action (Generated:41)
	 */
    private StepResult usrUserDefinedAction(PmtPromptDetailDisplayState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			//switchSUB 41 SUB    
			//switchBLK 1000018 BLK CAS
			//switchSUB 1000018 BLK CAS
			if (!dto.getHospitalCode().equals("")) {
				// DTL.Hospital Code is Not Blank
				//switchBLK 1000021 BLK ACT
				// DEBUG genFunctionCall 1000022 ACT DSP Wards per Hospital - Ward  *
				//TODO: commented below line and added respective service invocation
				//dto.setNextScreen("DspWardsPerHospital");
				DspWardsPerHospitalParams dspWardsPerHospitalParams = new DspWardsPerHospitalParams();
				BeanUtils.copyProperties(dto, dspWardsPerHospitalParams);
				result = StepResult.callService(DspWardsPerHospitalService.class, dspWardsPerHospitalParams).thenCall(serviceDspWardsPerHospital);
				//switchBLK 1000024 BLK CAS
				//switchSUB 1000024 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000027 BLK ACT
					// DEBUG genFunctionCall 1000028 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000065 BLK ACT
					// DEBUG genFunctionCall 1000066 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000038 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000040 BLK ACT
					// DEBUG genFunctionCall 1000041 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
			//switchBLK 1000255 BLK TXT
			// 
			//switchBLK 1000256 BLK ACT
			// DEBUG genFunctionCall 1000257 ACT LCL.Log Function Type = CON.PMTRCD
			dto.setLclLogFunctionType("PMTRCD");
			//switchBLK 1000260 BLK ACT
			// DEBUG genFunctionCall 1000261 ACT LCL.Log User Point = CON.User defined action
			dto.setLclLogUserPoint("User defined action");
			//switchBLK 1000270 BLK ACT
			// DEBUG genFunctionCall 1000271 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Generated:1021)
	 */
    private StepResult usrExitProgramProcessing(PmtPromptDetailDisplayState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			//switchSUB 1021 SUB    
			//switchBLK 1000052 BLK CAS
			//switchSUB 1000052 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// DTL.*CMD key is *Exit
				//switchBLK 1000055 BLK ACT
				// DEBUG genFunctionCall 1000056 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
			//switchBLK 1000277 BLK TXT
			// 
			//switchBLK 1000278 BLK ACT
			// DEBUG genFunctionCall 1000279 ACT LCL.Log Function Type = CON.PMTRCD
			dto.setLclLogFunctionType("PMTRCD");
			//switchBLK 1000282 BLK ACT
			// DEBUG genFunctionCall 1000283 ACT LCL.Log User Point = CON.Exit program processing
			dto.setLclLogUserPoint("Exit program processing");
			//switchBLK 1000292 BLK ACT
			// DEBUG genFunctionCall 1000293 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */

    /**
     * SelectHospitalService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceSelectHospital(PmtPromptDetailDisplayState state, SelectHospitalParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }

    /**
     * DspWardsPerHospitalService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspWardsPerHospital(PmtPromptDetailDisplayState state, DspWardsPerHospitalParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }
//
//    /**
//     * RtvHospitalDetailService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvHospitalDetail(PmtPromptDetailDisplayState state, RtvHospitalDetailParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(PmtPromptDetailDisplayState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(PmtPromptDetailDisplayState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * QuitService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceQuit(PmtPromptDetailDisplayState state, QuitParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * FieldRequiredService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceFieldRequired(PmtPromptDetailDisplayState state, FieldRequiredParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * CreateLogFileService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateLogFile(PmtPromptDetailDisplayState state, CreateLogFileParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//

    /**
     * SelectPatientService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processPromptSelectHospital(PmtPromptDetailDisplayState state, SelectHospitalParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        result = conductScreenConversation(state);

        return result;
    }


}
