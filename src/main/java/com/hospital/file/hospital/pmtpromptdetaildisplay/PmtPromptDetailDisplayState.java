package com.hospital.file.hospital.pmtpromptdetaildisplay;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.file.hospital.Hospital;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;

/**
 * State for file 'Hospital' (TSACREP) and function 'PMT Prompt detail display' (TSAQPVR).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class PmtPromptDetailDisplayState extends PmtPromptDetailDisplayDTO {
    private static final long serialVersionUID = -3698769149132390059L;

    // Local fields
    private String lclHospitalCode = "";
    private String lclLogFunctionType = "";
    private String lclLogUserPoint = "";
    private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");

    // System fields
    private ReloadSubfileEnum _sysReloadSubfile = null;
    private ReturnCodeEnum _sysReturnCode = ReturnCodeEnum._STA_NORMAL;
    private boolean _sysErrorFound = false;

    public PmtPromptDetailDisplayState() {
    }

    public PmtPromptDetailDisplayState(Hospital hospital) {
        setDtoFields(hospital);
    }

    public void setLclHospitalCode(String hospitalCode) {
        this.lclHospitalCode = hospitalCode;
    }

    public String getLclHospitalCode() {
        return lclHospitalCode;
    }
    public void setLclLogFunctionType(String logFunctionType) {
        this.lclLogFunctionType = logFunctionType;
    }

    public String getLclLogFunctionType() {
        return lclLogFunctionType;
    }
    public void setLclLogUserPoint(String logUserPoint) {
        this.lclLogUserPoint = logUserPoint;
    }

    public String getLclLogUserPoint() {
        return lclLogUserPoint;
    }
    public void setLclReturnCode(ReturnCodeEnum returnCode) {
        this.lclReturnCode = returnCode;
    }

    public ReturnCodeEnum getLclReturnCode() {
        return lclReturnCode;
    }

   public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
       _sysReloadSubfile = reloadSubfile;
   }

   public ReloadSubfileEnum get_SysReloadSubfile() {
       return _sysReloadSubfile;
   }

   public void set_SysReturnCode(ReturnCodeEnum returnCode) {
       _sysReturnCode = returnCode;
   }

   public ReturnCodeEnum get_SysReturnCode() {
       return _sysReturnCode;
   }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
