package com.hospital.file.hospital.pmtpromptdetaildisplay;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


import com.hospital.common.exception.ServiceException;

import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailService;
import com.hospital.file.hospital.selecthospital.SelectHospitalService;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.ward.dspwardsperhospital.DspWardsPerHospitalService;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailDTO;
import com.hospital.file.hospital.selecthospital.SelectHospitalDTO;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.file.ward.dspwardsperhospital.DspWardsPerHospitalDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.file.hospital.selecthospital.SelectHospitalParams;
import com.hospital.file.ward.dspwardsperhospital.DspWardsPerHospitalParams;


/**
 * Spring Validator for file 'Hospital' (TSACREP) and function 'PMT Prompt detail display' (TSAQPVR).
 *
 * @author X2EGenerator
 */
@Component
public class PmtPromptDetailDisplayValidator implements Validator {
	
	
	@Autowired
	private CreateLogFileService createLogFileService;
	
	@Autowired
	private RtvHospitalDetailService rtvHospitalDetailService;
	

	@Override
	public boolean supports(Class<?> clazz) {
		return PmtPromptDetailDisplayDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors e) {
		if (!(object instanceof PmtPromptDetailDisplayDTO)) {
			e.reject("Not a valid PmtPromptDetailDisplayDTO");
		} else {
			PmtPromptDetailDisplayDTO dto = (PmtPromptDetailDisplayDTO)object;
			PmtPromptDetailDisplayParams params = new PmtPromptDetailDisplayParams();
			BeanUtils.copyProperties(dto, params);
			try {
				/**
				 * USER: Validate Fields (Generated:1065)
				 */
				CreateLogFileDTO createLogFileDTO;
				//switchSUB 1065 SUB    
				//switchBLK 1000075 BLK CAS
				//switchSUB 1000075 BLK CAS
				if (dto.getHospitalCode() != null && dto.getHospitalCode().isEmpty()) {
					// DTL.Hospital Code is Blank
					//switchBLK 1000080 BLK ACT
					//functionCall 1000081 ACT Send error message - 'Field required'
					//e.rejectValue("", "field.required");
				}
				//switchBLK 1000189 BLK TXT
				// 
				//switchBLK 1000190 BLK ACT
				//functionCall 1000191 ACT LCL.Log Function Type = CON.PMTRCD
				dto.setLclLogFunctionType("PMTRCD");
				//switchBLK 1000194 BLK ACT
				//functionCall 1000195 ACT LCL.Log User Point = CON.Validate fields
				dto.setLclLogUserPoint("Validate fields");
				//switchBLK 1000204 BLK ACT
				//functionCall 1000205 ACT Create Log File - Log File  *
				createLogFileDTO = new CreateLogFileDTO();
				//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				// TODO: Unsupported Action Diagram Context: LCL
				// TODO: Unsupported Action Diagram Context: LCL
				createLogFileService.execute(createLogFileDTO);
			} catch (Exception se) {
				se.printStackTrace();
			}
			try {
				/**
				 * USER: Validate Relations (Generated:29)
				 */
				CreateLogFileDTO createLogFileDTO;
				//switchSUB 29 SUB    
				//switchBLK 1000093 BLK CAS
				//switchSUB 1000093 BLK CAS
				if (dto.getHospitalCode().equals("PUBLIC")) {
					// DTL.Hospital Code is Equal Public
					//switchBLK 1000096 BLK CAS
					//switchSUB 1000096 BLK CAS
					if (dto.getAddressProvince().equals("QC")) {
						// DTL.Address Province is Quebec
						//switchBLK 1000099 BLK ACT
						//functionCall 1000100 ACT Send error message - 'Field required'
						//e.rejectValue("", "field.required");
					}
				}
				//switchBLK 1000233 BLK TXT
				// 
				//switchBLK 1000234 BLK ACT
				//functionCall 1000235 ACT LCL.Log Function Type = CON.PMTRCD
				dto.setLclLogFunctionType("PMTRCD");
				//switchBLK 1000238 BLK ACT
				//functionCall 1000239 ACT LCL.Log User Point = CON.Validate relations
				dto.setLclLogUserPoint("Validate relations");
				//switchBLK 1000248 BLK ACT
				//functionCall 1000249 ACT Create Log File - Log File  *
				createLogFileDTO = new CreateLogFileDTO();
				//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				// TODO: Unsupported Action Diagram Context: LCL
				// TODO: Unsupported Action Diagram Context: LCL
				createLogFileService.execute(createLogFileDTO);
			} catch (Exception se) {
				se.printStackTrace();
			}
		}
	}
}
