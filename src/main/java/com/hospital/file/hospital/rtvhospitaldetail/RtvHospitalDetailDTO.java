package com.hospital.file.hospital.rtvhospitaldetail;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvHospitalDetailDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private CountryEnum country;
	private ReturnCodeEnum returnCode;
	private String addressPostZip;
	private String addressProvince;
	private String addressStreet;
	private String addressTown;
	private String hospitalCode;
	private String hospitalName;
	private long faxNumber;
	private long telephoneNumber;
	private String nextScreen;

	public String getAddressPostZip() {
		return addressPostZip;
	}

	public String getAddressProvince() {
		return addressProvince;
	}

	public String getAddressStreet() {
		return addressStreet;
	}

	public String getAddressTown() {
		return addressTown;
	}

	public CountryEnum getCountry() {
		return country;
	}

	public long getFaxNumber() {
		return faxNumber;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public long getTelephoneNumber() {
		return telephoneNumber;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setAddressPostZip(String addressPostZip) {
		this.addressPostZip = addressPostZip;
	}

	public void setAddressProvince(String addressProvince) {
		this.addressProvince = addressProvince;
	}

	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}

	public void setAddressTown(String addressTown) {
		this.addressTown = addressTown;
	}

	public void setCountry(CountryEnum country) {
		this.country = country;
	}

	public void setFaxNumber(long faxNumber) {
		this.faxNumber = faxNumber;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public void setTelephoneNumber(long telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
