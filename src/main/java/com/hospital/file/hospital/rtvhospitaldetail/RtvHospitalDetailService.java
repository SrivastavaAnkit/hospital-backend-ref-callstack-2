package com.hospital.file.hospital.rtvhospitaldetail;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.hospital.Hospital;
import com.hospital.file.hospital.HospitalId;
import com.hospital.file.hospital.HospitalRepository;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvHospitalDetailService extends AbstractService<RtvHospitalDetailService, RtvHospitalDetailDTO>
{
    private final Step execute = define("execute", RtvHospitalDetailDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private HospitalRepository hospitalRepository;
	
    @Autowired
    public RtvHospitalDetailService()
    {
        super(RtvHospitalDetailService.class, RtvHospitalDetailDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvHospitalDetailDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvHospitalDetailDTO dto, RtvHospitalDetailDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Hospital> hospitalList = hospitalRepository.findAllByIdHospitalCode(dto.getHospitalCode());
		if (!hospitalList.isEmpty()) {
			for (Hospital hospital : hospitalList) {
				processDataRecord(dto, hospital);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvHospitalDetailDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Empty:48)
		 */
		//switchSUB 48 SUB    
		// Unprocessed SUB 48 -
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvHospitalDetailDTO dto, Hospital hospital) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		//switchSUB 41 SUB    
		//switchBLK 1000010 BLK ACT
		//functionCall 1000011 ACT PAR = DB1 By name
		dto.setHospitalName(hospital.getHospitalName()); dto.setAddressStreet(hospital.getAddressStreet()); dto.setAddressTown(hospital.getAddressTown()); dto.setAddressProvince(hospital.getAddressProvince()); dto.setAddressPostZip(hospital.getAddressPostZip()); dto.setTelephoneNumber(hospital.getTelephoneNumber()); dto.setFaxNumber(hospital.getFaxNumber()); dto.setCountry(hospital.getCountry());
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvHospitalDetailDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		//switchSUB 52 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT PAR = CON By name
		dto.setHospitalName(""); dto.setAddressStreet(""); dto.setAddressTown(""); dto.setAddressProvince(""); dto.setAddressPostZip(""); dto.setTelephoneNumber(0L); dto.setFaxNumber(0L); dto.setCountry(CountryEnum.fromCode(""));
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvHospitalDetailDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Empty:61)
		 */
		//switchSUB 61 SUB    
		// Unprocessed SUB 61 -
        return NO_ACTION;
    }
}
