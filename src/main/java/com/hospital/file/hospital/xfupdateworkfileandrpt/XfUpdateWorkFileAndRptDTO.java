package com.hospital.file.hospital.xfupdateworkfileandrpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class XfUpdateWorkFileAndRptDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private ReturnCodeEnum returnCode;
	private String hospitalCode;
	private String nextScreen;
	private String lclLogFunctionType;
	private String lclLogUserPoint;

	public String getHospitalCode() {
		return hospitalCode;
	}

	public String getLclLogFunctionType() {
		return lclLogFunctionType;
	}

	public String getLclLogUserPoint() {
		return lclLogUserPoint;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public void setLclLogFunctionType(String lclLogFunctionType) {
		this.lclLogFunctionType = lclLogFunctionType;
	}

	public void setLclLogUserPoint(String lclLogUserPoint) {
		this.lclLogUserPoint = lclLogUserPoint;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
