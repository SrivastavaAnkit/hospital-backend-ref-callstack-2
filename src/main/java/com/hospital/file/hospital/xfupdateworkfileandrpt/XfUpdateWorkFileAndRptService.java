package com.hospital.file.hospital.xfupdateworkfileandrpt;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.hospital.Hospital;
import com.hospital.file.hospital.HospitalId;
import com.hospital.file.hospital.HospitalRepository;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.ward.rtvwardforhospital.RtvWardForHospitalService;
import com.hospital.file.workfile.prtfilreportperhosp.PrtfilReportPerHospService;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.file.ward.rtvwardforhospital.RtvWardForHospitalDTO;
import com.hospital.file.workfile.prtfilreportperhosp.PrtfilReportPerHospDTO;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;



@Service
public class XfUpdateWorkFileAndRptService {

	
	@Autowired
	private JobContext job;
	
	@Autowired
	private HospitalRepository hospitalRepository;
	
	@Autowired
	private CreateLogFileService createLogFileService;
	
	@Autowired
	private PrtfilReportPerHospService prtfilReportPerHospService;
	
	@Autowired
	private RtvWardForHospitalService rtvWardForHospitalService;
	

	public void execute(XfUpdateWorkFileAndRptDTO dto) throws ServiceException {
		/**
		 *  (Generated:1)
		 */
		CreateLogFileDTO createLogFileDTO;
		RtvWardForHospitalDTO rtvWardForHospitalDTO;
		//switchSUB 1 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT RTV Ward for Hospital - Ward  *
		rtvWardForHospitalDTO = new RtvWardForHospitalDTO();
		rtvWardForHospitalDTO.setHospitalCode(dto.getHospitalCode());
		rtvWardForHospitalService.execute(rtvWardForHospitalDTO);
		//switchBLK 1000004 BLK ACT
		//functionCall 1000005 ACT SBMJOB: PRTFIL Report per Hosp - Work File  *
		// TODO: Unsupported Function Type 'Print File' (message surrogate = 1101559)
		//switchBLK 1000006 BLK TXT
		// 
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT LCL.Log Function Type = CON.EXCEXTFUN
		dto.setLclLogFunctionType("EXCEXTFUN");
		//switchBLK 1000011 BLK ACT
		//functionCall 1000012 ACT LCL.Log User Point = CON.Execute External Function
		dto.setLclLogUserPoint("Execute External Function");
		//switchBLK 1000021 BLK ACT
		//functionCall 1000022 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
	}
}
