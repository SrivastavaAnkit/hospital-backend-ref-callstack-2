package com.hospital.file.hospital;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository interface for model: Hospital (TSACREP).
 *
 * @author X2EGenerator
 */
@Repository
public interface HospitalRepository extends HospitalRepositoryCustom, JpaRepository<Hospital, HospitalId> {

	List<Hospital> findAllByIdHospitalCode(String hospitalCode);
}
