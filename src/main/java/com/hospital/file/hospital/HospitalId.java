package com.hospital.file.hospital;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.io.Serializable;
import javax.persistence.Column;

public class HospitalId implements Serializable {
	private static final long serialVersionUID = -1L;

	@Column(name = "HospitalCode")
	private String hospitalCode = "";

	public HospitalId() {
	
	}

	public HospitalId(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}
	
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
