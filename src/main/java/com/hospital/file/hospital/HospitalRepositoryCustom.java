package com.hospital.file.hospital;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.hospital.model.CountryEnum;
import com.hospital.model.CountryEnum;

import com.hospital.file.hospital.selecthospital.SelectHospitalGDO;
import com.hospital.file.hospital.dspfdisplayhospitals.DspfDisplayHospitalsGDO;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsGDO;
import com.hospital.file.hospital.pmtpromptdetaildisplay.PmtPromptDetailDisplayDTO;
import com.hospital.file.hospital.pmthospitalspercountry.PmtHospitalsPerCountryDTO;
import com.hospital.file.hospital.pmttestbatchfunction.PmtTestBatchFunctionDTO;

/**
 * Custom Spring Data JPA repository interface for model: Hospital (TSACREP).
 *
 * @author X2EGenerator
 */
@Repository
public interface HospitalRepositoryCustom {

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 */
	void deleteHospital(String hospitalCode);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of SelectHospitalGDO
	 */
	RestResponsePage<SelectHospitalGDO> selectHospital(String hospitalCode, Pageable pageable);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @return Hospital
	 */
	Hospital rtvHospitalDetail(String hospitalCode);

	/**
	 * 
	 * @param country Country
	 * @param pageable a Pageable object used for pagination
	 * @return Page of DspfDisplayHospitalsGDO
	 */
	RestResponsePage<DspfDisplayHospitalsGDO> dspfDisplayHospitals(CountryEnum country, Pageable pageable);

	/**
	 * 
	 * @param country Country
	 * @param hospitalName Hospital Name
	 * @param pageable a Pageable object used for pagination
	 * @return Page of DspAllHospitalsGDO
	 */
	RestResponsePage<DspAllHospitalsGDO> dspAllHospitals(CountryEnum country, String hospitalName, Pageable pageable);

	/**
	 * 
	 * @param pageable a Pageable object used for pagination
	 * @return Page of PmtPromptDetailDisplayDTO
	 */
	RestResponsePage<PmtPromptDetailDisplayDTO> pmtPromptDetailDisplay(Pageable pageable);

	/**
	 * 
	 * @param pageable a Pageable object used for pagination
	 * @return Page of PmtHospitalsPerCountryDTO
	 */
	RestResponsePage<PmtHospitalsPerCountryDTO> pmtHospitalsPerCountry(Pageable pageable);

	/**
	 * 
	 * @param pageable a Pageable object used for pagination
	 * @return Page of PmtTestBatchFunctionDTO
	 */
	RestResponsePage<PmtTestBatchFunctionDTO> pmtTestBatchFunction(Pageable pageable);
}
