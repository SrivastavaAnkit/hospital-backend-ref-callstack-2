package com.hospital.file.hospital;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.hospital.model.CountryConverter;
import com.hospital.model.CountryEnum;

@Entity
@Table(name="Hospital", schema="HospitalMgmt")
public class Hospital implements Serializable {
	private static final long serialVersionUID = -1L;

	@Version
	@Column(name="version")
	private long version;
	
	

	@EmbeddedId
	private HospitalId id = new HospitalId();

	@Column(name = "AddressPostZip")
	private String addressPostZip = "";
	
	@Column(name = "AddressProvince")
	private String addressProvince = "";
	
	@Column(name = "AddressStreet")
	private String addressStreet = "";
	
	@Column(name = "AddressTown")
	private String addressTown = "";
	
	@Column(name = "Country")
	@Convert(converter=CountryConverter.class)
	private CountryEnum country;
	
	@Column(name = "FaxNumber")
	private long faxNumber = 0L;
	
	@Column(name = "HospitalName")
	private String hospitalName = "";
	
	@Column(name = "TelephoneNumber")
	private long telephoneNumber = 0L;

	public HospitalId getId() {
		return id;
	}

	public String getHospitalCode() {
		return id.getHospitalCode();
	}

	public String getHospitalName() {
		return hospitalName;
	}
	
	public String getAddressStreet() {
		return addressStreet;
	}
	
	public String getAddressTown() {
		return addressTown;
	}
	
	public String getAddressProvince() {
		return addressProvince;
	}
	
	public String getAddressPostZip() {
		return addressPostZip;
	}
	
	public long getTelephoneNumber() {
		return telephoneNumber;
	}
	
	public long getFaxNumber() {
		return faxNumber;
	}
	
	public CountryEnum getCountry() {
		return country;
	}

	

	public long getVersion() {
		return version;
	}

	public void setHospitalCode(String hospitalCode) {
		this.id.setHospitalCode(hospitalCode);
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	
	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}
	
	public void setAddressTown(String addressTown) {
		this.addressTown = addressTown;
	}
	
	public void setAddressProvince(String addressProvince) {
		this.addressProvince = addressProvince;
	}
	
	public void setAddressPostZip(String addressPostZip) {
		this.addressPostZip = addressPostZip;
	}
	
	public void setTelephoneNumber(long telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	
	public void setFaxNumber(long faxNumber) {
		this.faxNumber = faxNumber;
	}
	
	public void setCountry(CountryEnum country) {
		this.country = country;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
