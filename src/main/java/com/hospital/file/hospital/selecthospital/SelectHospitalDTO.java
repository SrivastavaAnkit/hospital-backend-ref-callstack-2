package com.hospital.file.hospital.selecthospital;

import org.springframework.beans.BeanUtils;

import com.hospital.common.state.SelectRecordDTO;
import com.hospital.file.hospital.Hospital;




/**
 * Dto for file 'Hospital' (TSACREP) and function 'Select Hospital' (TSAHSRR).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectHospitalDTO extends SelectRecordDTO<SelectHospitalGDO> {
	private static final long serialVersionUID = 7461537532523070859L;

	private long version = 0;
	private String hospitalCode = "";
    private SelectHospitalGDO gdo;
    private int page = 0;
    private int size = 10;
    private String sortData = "";
	public SelectHospitalDTO() {
	}

    public SelectHospitalDTO(long version) {
        this.version = version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public long getVersion() {
        return version;
    }

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}


    public SelectHospitalGDO getGdo() {
		return gdo;
	 }

	 public void setGdo(SelectHospitalGDO gdo) {
		this.gdo = gdo;
	 }
     /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param hospital Hospital Entity bean
     */
    public void setDtoFields(Hospital hospital) {
   this.version = hospital.getVersion();
        BeanUtils.copyProperties(hospital, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param hospital Hospital Entity bean
     */
    public void setEntityFields(Hospital hospital) {
   hospital.setVersion(this.version);
        BeanUtils.copyProperties(this, hospital);
    }

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getSortData() {
		return sortData;
	}

	public void setSortData(String sortData) {
		this.sortData = sortData;
	}
    
    
}
