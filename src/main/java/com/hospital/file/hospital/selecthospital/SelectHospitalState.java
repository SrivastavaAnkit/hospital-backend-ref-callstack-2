package com.hospital.file.hospital.selecthospital;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.file.hospital.Hospital;
import com.hospital.model.ReloadSubfileEnum;


/**
 * State for file 'Hospital' (TSACREP) and function 'Select Hospital' (TSAHSRR).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectHospitalState extends SelectHospitalDTO {
    private static final long serialVersionUID = -4440597635121198772L;


    // System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    public SelectHospitalState() {
    }

    public SelectHospitalState(Hospital hospital) {
        setDtoFields(hospital);
    }

   public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
       _sysReloadSubfile = reloadSubfile;
   }

   public ReloadSubfileEnum get_SysReloadSubfile() {
       return _sysReloadSubfile;
   }

}
