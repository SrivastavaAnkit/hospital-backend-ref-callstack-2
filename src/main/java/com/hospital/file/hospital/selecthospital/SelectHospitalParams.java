package com.hospital.file.hospital.selecthospital;

import java.io.Serializable;


/**
 * Params for resource: SelectHospital (TSAHSRR).
 *
 * @author X2EGenerator
 */
public class SelectHospitalParams implements Serializable {
    private static final long serialVersionUID = 3679338428686026874L;

	private String hospitalCode = "";

	public String getHospitalCode() {
		return hospitalCode;
	}
	
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
}

