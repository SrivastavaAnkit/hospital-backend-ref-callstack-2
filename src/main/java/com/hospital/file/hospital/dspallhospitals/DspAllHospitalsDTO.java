package com.hospital.file.hospital.dspallhospitals;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.hospital.common.state.BaseDTO;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.file.hospital.Hospital;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.ReloadSubfileEnum;

/**
 * Dto for file 'Hospital' (TSACREP) and function 'DSP All Hospitals' (TSA9DFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class DspAllHospitalsDTO extends BaseDTO {
	private static final long serialVersionUID = -2477866120664953679L;

    private RestResponsePage<DspAllHospitalsGDO> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";
	private CountryEnum country = null;
	private String countryName = "";
	private String hospitalCode = "";
	private String hospitalName = "";
	private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");
	private List<DspAllHospitalsGDO> mySelections;


	private DspAllHospitalsGDO gdo;

    public DspAllHospitalsDTO() {

    }
    public void setPageDto(RestResponsePage<DspAllHospitalsGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<DspAllHospitalsGDO> getPageDto() {
		return pageDto;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setCountry(CountryEnum country) {
		this.country = country;
    }

    public CountryEnum getCountry() {
    	return country;
    }

	public void setCountryName(String countryName) {
		this.countryName = countryName;
    }

    public String getCountryName() {
    	return countryName;
    }

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
    }

    public String getHospitalCode() {
    	return hospitalCode;
    }

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
    	return hospitalName;
    }

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
    }

    public ReturnCodeEnum getReturnCode() {
    	return returnCode;
    }

	public void setGdo(DspAllHospitalsGDO gdo) {
		this.gdo = gdo;
	}

	public DspAllHospitalsGDO getGdo() {
		return gdo;
	}
	public List<DspAllHospitalsGDO> getMySelections() {
		return mySelections;
	}
	public void setMySelections(List<DspAllHospitalsGDO> mySelections) {
		this.mySelections = mySelections;
	}

}