package com.hospital.file.hospital.dspallhospitals;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hospital.config.LocalDateDeserializer;
import com.hospital.config.LocalDateSerializer;
import com.hospital.config.LocalTimeDeserializer;
import com.hospital.config.LocalTimeSerializer;
import com.hospital.file.hospital.Hospital;
import com.hospital.model.CountryEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;

/**
 * Gdo for file 'Hospital' (TSACREP) and function 'DSP All Hospitals' (TSA9DFR).
 */
public class DspAllHospitalsGDO implements Serializable {
	private static final long serialVersionUID = -7986388149616621058L;

    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private CountryEnum country = null;
	private String countryName = "";
	private String hospitalCode = "";
	private String hospitalName = "";
	private long numberOfWards = 0L;
	private long number5 = 0L;
	private String addressStreet = "";
	private String addressTown = "";
	private String addressProvince = "";
	private String addressPostZip = "";
	private long telephoneNumber = 0L;
	private long faxNumber = 0L;
	private String addedUser = "";
	@JsonDeserialize(using= LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate addedDate = null;
	@JsonDeserialize(using= LocalTimeDeserializer.class)
	@JsonSerialize(using = LocalTimeSerializer.class)
	private LocalTime addedTime = null;
	private String changedUser = "";
	@JsonDeserialize(using= LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate changedDate = null;
	@JsonDeserialize(using= LocalTimeDeserializer.class)
	@JsonSerialize(using = LocalTimeSerializer.class)
	private LocalTime changedTime = null;

	public DspAllHospitalsGDO() {

	}

   	public DspAllHospitalsGDO(CountryEnum country, String hospitalCode, String hospitalName, String addressStreet, String addressTown, String addressProvince, String addressPostZip, long telephoneNumber, long faxNumber/*, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime*/) {
		this.country = country;
		this.hospitalCode = hospitalCode;
		this.hospitalName = hospitalName;
		this.addressStreet = addressStreet;
		this.addressTown = addressTown;
		this.addressProvince = addressProvince;
		this.addressPostZip = addressPostZip;
		this.telephoneNumber = telephoneNumber;
		this.faxNumber = faxNumber;
//		this.addedUser = addedUser;
//		this.addedDate = addedDate;
//		this.addedTime = addedTime;
//		this.changedUser = changedUser;
//		this.changedDate = changedDate;
//		this.changedTime = changedTime;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setCountry(CountryEnum country) {
    	this.country = country;
    }

	public CountryEnum getCountry() {
		return country;
	}

	public void setCountryName(String countryName) {
    	this.countryName = countryName;
    }

	public String getCountryName() {
		return countryName;
	}

	public void setHospitalCode(String hospitalCode) {
    	this.hospitalCode = hospitalCode;
    }

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalName(String hospitalName) {
    	this.hospitalName = hospitalName;
    }

	public String getHospitalName() {
		return hospitalName;
	}

	public void setNumberOfWards(long numberOfWards) {
    	this.numberOfWards = numberOfWards;
    }

	public long getNumberOfWards() {
		return numberOfWards;
	}

	public void setNumber5(long number5) {
    	this.number5 = number5;
    }

	public long getNumber5() {
		return number5;
	}

	public void setAddressStreet(String addressStreet) {
    	this.addressStreet = addressStreet;
    }

	public String getAddressStreet() {
		return addressStreet;
	}

	public void setAddressTown(String addressTown) {
    	this.addressTown = addressTown;
    }

	public String getAddressTown() {
		return addressTown;
	}

	public void setAddressProvince(String addressProvince) {
    	this.addressProvince = addressProvince;
    }

	public String getAddressProvince() {
		return addressProvince;
	}

	public void setAddressPostZip(String addressPostZip) {
    	this.addressPostZip = addressPostZip;
    }

	public String getAddressPostZip() {
		return addressPostZip;
	}

	public void setTelephoneNumber(long telephoneNumber) {
    	this.telephoneNumber = telephoneNumber;
    }

	public long getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setFaxNumber(long faxNumber) {
    	this.faxNumber = faxNumber;
    }

	public long getFaxNumber() {
		return faxNumber;
	}

	public void setAddedUser(String addedUser) {
    	this.addedUser = addedUser;
    }

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
    	this.addedDate = addedDate;
    }

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
    	this.addedTime = addedTime;
    }

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
    	this.changedUser = changedUser;
    }

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
    	this.changedDate = changedDate;
    }

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
    	this.changedTime = changedTime;
    }

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}