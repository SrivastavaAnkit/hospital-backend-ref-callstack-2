package com.hospital.file.hospital.dspallhospitals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
 
import com.hospital.support.JobContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.hospital.file.logfile.LogFile;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.file.hospital.HospitalRepository;
import com.hospital.file.doctor.dspfdoctorsforhospital.DspfDoctorsForHospitalService;
import com.hospital.file.hospital.eredithospital.ErEditHospitalService;
import com.hospital.file.hospital.eredithospitalchange.ErEditHospitalChangeService;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.patient.dsppatientsperhospital.DspPatientsPerHospitalService;
import com.hospital.file.patient.rtvpatientperhospital.RtvPatientPerHospitalService;
import com.hospital.file.ward.dspwardsperhospital2.DspWardsPerHospital2Service;
import com.hospital.file.ward.rtvnoofwardshospital.RtvNoOfWardsHospitalService;
import com.hospital.file.doctor.dspfdoctorsforhospital.DspfDoctorsForHospitalDTO;
import com.hospital.file.hospital.eredithospital.ErEditHospitalDTO;
import com.hospital.file.hospital.eredithospitalchange.ErEditHospitalChangeDTO;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.file.patient.dsppatientsperhospital.DspPatientsPerHospitalDTO;
import com.hospital.file.patient.rtvpatientperhospital.RtvPatientPerHospitalDTO;
import com.hospital.file.ward.dspwardsperhospital.DspWardsPerHospitalParams;
import com.hospital.file.ward.dspwardsperhospital2.DspWardsPerHospital2DTO;
import com.hospital.file.ward.rtvnoofwardshospital.RtvNoOfWardsHospitalDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.file.doctor.dspfdoctorsforhospital.DspfDoctorsForHospitalParams;
import com.hospital.file.hospital.eredithospital.ErEditHospitalParams;
import com.hospital.file.hospital.eredithospitalchange.ErEditHospitalChangeParams;
import com.hospital.file.patient.dsppatientsperhospital.DspPatientsPerHospitalParams;
import com.hospital.file.ward.dspwardsperhospital2.DspWardsPerHospital2Params;

import com.hospital.common.exception.ServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

	
/**
 * Service implementation for resource: DspAllHospitals (TSA9DFR).
 *
 * @author X2EGenerator
 */
@Service
public class DspAllHospitalsService extends AbstractService<DspAllHospitalsService, DspAllHospitalsState> {
    
	@Autowired
	private JobContext job;

    @Autowired
    private HospitalRepository hospitalRepository;
    
    @Autowired
    private CreateLogFileService createLogFileService;
    
    @Autowired
    private RtvNoOfWardsHospitalService rtvNoOfWardsHospitalService;
    
    @Autowired
    private RtvPatientPerHospitalService rtvPatientPerHospitalService;
    

    @Autowired
    private MessageSource messageSource;
    
	public static final String SCREEN_CTL = "dspAllHospitals";
    public static final String SCREEN_RCD = "DspAllHospitals.rcd";

    private final Step execute = define("execute", DspAllHospitalsParams.class, this::execute);
    private final Step response = define("response", DspAllHospitalsDTO.class, this::processResponse);
	private final Step serviceDspWardsPerHospital2 = define("serviceDspWardsPerHospital2",DspWardsPerHospital2Params.class, this::processServiceDspWardsPerHospital2);
	private final Step serviceDspPatientsPerHospital = define("serviceDspPatientsPerHospital",DspPatientsPerHospitalParams.class, this::processServiceDspPatientsPerHospital);
	private final Step serviceErEditHospitalChange = define("serviceErEditHospitalChange",ErEditHospitalChangeParams.class, this::processServiceErEditHospitalChange);
	private final Step serviceDspfDoctorsForHospital = define("serviceDspfDoctorsForHospital",DspfDoctorsForHospitalParams.class, this::processServiceDspfDoctorsForHospital);
	private final Step serviceErEditHospital = define("serviceErEditHospital",ErEditHospitalParams.class, this::processServiceErEditHospital);
	//private final Step serviceCvtvar = define("serviceCvtvar",CvtvarParams.class, this::processServiceCvtvar);
	//private final Step serviceRtvNoOfWardsHospital = define("serviceRtvNoOfWardsHospital",RtvNoOfWardsHospitalParams.class, this::processServiceRtvNoOfWardsHospital);
	//private final Step serviceRtvPatientPerHospital = define("serviceRtvPatientPerHospital",RtvPatientPerHospitalParams.class, this::processServiceRtvPatientPerHospital);
	//private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
	//private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
	//private final Step serviceQuit = define("serviceQuit",QuitParams.class, this::processServiceQuit);
	//private final Step serviceWrksplf = define("serviceWrksplf",WrksplfParams.class, this::processServiceWrksplf);
	//private final Step serviceCreateLogFile = define("serviceCreateLogFile",CreateLogFileParams.class, this::processServiceCreateLogFile);
	//private final Step serviceConcat = define("serviceConcat",ConcatParams.class, this::processServiceConcat);
	
    
    @Autowired
    public DspAllHospitalsService() {
        super(DspAllHospitalsService.class, DspAllHospitalsState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * DSPFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(DspAllHospitalsState state, DspAllHospitalsParams params) {
    	StepResult result = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        result = usrInitializeProgram(state);

        result = mainLoop(state, params);

        return result;
    }

    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult mainLoop(DspAllHospitalsState state, DspAllHospitalsParams params) {
        StepResult result = NO_ACTION;

        result = loadFirstSubfilePage(state, params);

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        result = conductScreenConversation(state);

        return result;
    }

    /**
     * SCREEN  initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult loadFirstSubfilePage(DspAllHospitalsState state, DspAllHospitalsParams params) {
    	StepResult result = NO_ACTION;

    	result = usrInitializeSubfileControl(state, params);

		dbfReadFirstDataRecord(state);
		//TODO: commented below code
//		if (state.getPageDto() != null && state.getPageDto().getSize() > 0) {
//		    result = loadNextSubfilePage(state);
//		}

        return result;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     * @return
     */
    private StepResult loadNextSubfilePage(DspAllHospitalsState state) {
    	StepResult result = NO_ACTION;

    	List<DspAllHospitalsGDO> list = state.getPageDto().getContent();
        for (DspAllHospitalsGDO gdo : list) {
            gdo.set_SysRecordSelected(RecordSelectedEnum._STA_YES);
            result = usrInitializeSubfileRecordFromDBFRecord(state, gdo);
//             TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
            if (gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
//                TODO:writeSubfileRecord(state);  // synon built-in function
            }
        }

        return result;
    }

    /**
     * SCREEN  initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductScreenConversation(DspAllHospitalsState state) {
        StepResult result = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            DspAllHospitalsDTO model = new DspAllHospitalsDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_CTL, model).thenCall(response);
        }

        return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processResponse(DspAllHospitalsState state, DspAllHospitalsDTO model) {
    	StepResult result = NO_ACTION;

        // update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            result = closedown(state);
        } else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
        } else if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            //TODO:processHelpRequest(state);//synon built-in function
        }
        else if (CmdKeyEnum.isNextPage(state.get_SysCmdKey())) {
            dbfReadNextPageRecord(state);
            result = loadNextSubfilePage(state);
        } else {
            result = processScreen(state);
        }

        return result;
    }

    /**
     * SCREEN process screen.
     * @param state - Service state class.
     * @return
     */
    private StepResult processScreen(DspAllHospitalsState state) {
    	StepResult result = NO_ACTION;

        result = usrProcessSubfilePreConfirm(state);
        if (result != null) {
        	return result;
        }

        if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
        	return closedown(state);
        } else {
        	if(!state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
        		return closedown(state);
	        } else {
//	        	if(!state.get_SysProgramConfirm().getCode().equals(ProgramConfirmEnum._STA_YES.getCode())) {
//	        		return closedown(state);
//		        } else {
//		        	result = usrProcessCommandKeys(state);
//		        }

	        	result = usrProcessSubfileControlPostConfirm(state);
	        	for (DspAllHospitalsGDO gdo : state.getPageDto().getContent()) {
	                if(gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
	                	result = usrProcessSubfileRecordPostConfirm(state, gdo);
//	                  TODO:writeSubfileRecord(state);   // synon built-in function
	                }
	            }
	        	result = usrFinalProcessingPostConfirm(state);
	        	result = usrProcessCommandKeys(state);
	        }
        }

        result = conductScreenConversation(state);

        return result;
    }

    /**
     * SCREEN Process subfile Pre Confirm.
     * @param state - Service state class.
     * @return
     */
    private StepResult usrProcessSubfilePreConfirm(DspAllHospitalsState state) {
    	StepResult result = NO_ACTION;

    	result = usrSubfileControlFunctionFields(state);
    	result = usrProcessSubfileControlPreConfirm(state);
    	//if(state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
    		for (DspAllHospitalsGDO gdo : state.getPageDto().getContent()) {
                //if(gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
                	result = usrSubfileRecordFunctionFields(state, gdo);
                    result = usrProcessSubfileRecordPreConfirm(state, gdo);
//                  TODO:writeSubfileRecord(state);   // synon built-in function
                //}
            }
    	//}
    	if (result != null) {
    		return result;
    	}
    		
        result = usrFinalProcessingPreConfirm(state);

        return result;
    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(DspAllHospitalsState state) {
        StepResult result = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        result = usrExitProgramProcessing(state);

        return result;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(DspAllHospitalsState state) {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadNextPageRecord(DspAllHospitalsState state)
	{
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    private void dbfReadDataRecord(DspAllHospitalsState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = new PageRequest(state.getPage(), state.getSize());
        }
        else {
            pageable = new PageRequest(state.getPage(), state.getSize(), new Sort(sortOrders));
        }

        RestResponsePage<DspAllHospitalsGDO> pageDto = hospitalRepository.dspAllHospitals(state.getCountry(), state.getHospitalName(), pageable);
        state.setPageDto(pageDto);
    }
    
	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(DspAllHospitalsState dto) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			//switchSUB 20 SUB    
			//switchBLK 1000223 BLK ACT
			// DEBUG genFunctionCall 1000224 ACT PGM.*Scan limit = CND.High Value
			dto.set_SysScanLimit(9999999);
			//switchBLK 1000253 BLK TXT
			// 
			//switchBLK 1000254 BLK ACT
			// DEBUG genFunctionCall 1000255 ACT LCL.Log Function Type = CON.DSPFIL
			dto.setLclLogFunctionType("DSPFIL");
			//switchBLK 1000258 BLK ACT
			// DEBUG genFunctionCall 1000259 ACT LCL.Log User Point = CON.Initialize program
			dto.setLclLogUserPoint("Initialize program");
			//switchBLK 1000268 BLK ACT
			// DEBUG genFunctionCall 1000269 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Control (Generated:182)
	 */
    private StepResult usrInitializeSubfileControl(DspAllHospitalsState dto, DspAllHospitalsParams params) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			//switchSUB 182 SUB    
			//switchBLK 1000001 BLK ACT
			// DEBUG genFunctionCall 1000002 ACT CTL.Country Name = CVTVAR(CTL.Country)
			if (dto.getCountry() != null) {
				dto.setCountryName(dto.getCountry().getDescription());
			}
			//switchBLK 1000277 BLK TXT
			// 
			//switchBLK 1000278 BLK ACT
			// DEBUG genFunctionCall 1000279 ACT LCL.Log Function Type = CON.DSPFIL
			dto.setLclLogFunctionType("DSPFIL");
			//switchBLK 1000282 BLK ACT
			// DEBUG genFunctionCall 1000283 ACT LCL.Log User Point = CON.Initialize subfile
			dto.setLclLogUserPoint("Initialize subfile");
			//switchBLK 1000286 BLK ACT
			// DEBUG genFunctionCall 1000287 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.control,CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "control"));
			//switchBLK 1000292 BLK ACT
			// DEBUG genFunctionCall 1000293 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrInitializeSubfileRecordFromDBFRecord(DspAllHospitalsState dto, DspAllHospitalsGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			RtvNoOfWardsHospitalDTO rtvNoOfWardsHospitalDTO;
			RtvPatientPerHospitalDTO rtvPatientPerHospitalDTO;
			//switchSUB 41 SUB    
			//switchBLK 1000007 BLK ACT
			// DEBUG genFunctionCall 1000008 ACT RCD.Country Name = CVTVAR(RCD.Country)
			gdo.setCountryName(gdo.getCountry().getDescription());
			//switchBLK 1000013 BLK ACT
			// DEBUG genFunctionCall 1000014 ACT RTV No of wards/Hospital - Ward  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvNoOfWardsHospitalDTO = new RtvNoOfWardsHospitalDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvNoOfWardsHospitalDTO.setHospitalCode(gdo.getHospitalCode());
			// DEBUG genFunctionCall Service call
			rtvNoOfWardsHospitalService.execute(rtvNoOfWardsHospitalDTO);
			// DEBUG genFunctionCall Parameters OUT
			gdo.setNumberOfWards(rtvNoOfWardsHospitalDTO.getNumberOfWards());
			gdo.setNumber5(rtvNoOfWardsHospitalDTO.getCount());
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000057 BLK ACT
			// DEBUG genFunctionCall 1000058 ACT RTV Patient per Hospital - Patient  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvPatientPerHospitalDTO = new RtvPatientPerHospitalDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvPatientPerHospitalDTO.setHospitalCode(gdo.getHospitalCode());
			// DEBUG genFunctionCall Service call
			rtvPatientPerHospitalService.execute(rtvPatientPerHospitalDTO);
			// DEBUG genFunctionCall Parameters OUT
			gdo.setNumber5(rtvPatientPerHospitalDTO.getCount());
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000325 BLK TXT
			// 
			//switchBLK 1000326 BLK ACT
			// DEBUG genFunctionCall 1000327 ACT LCL.Log Function Type = CON.DSPFIL
			dto.setLclLogFunctionType("DSPFIL");
			//switchBLK 1000330 BLK ACT
			// DEBUG genFunctionCall 1000331 ACT LCL.Log User Point = CON.Initialize subfile record
			dto.setLclLogUserPoint("Initialize subfile record");
			//switchBLK 1000334 BLK ACT
			// DEBUG genFunctionCall 1000335 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.from DBF record,CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "from DBF record"));
			//switchBLK 1000340 BLK ACT
			// DEBUG genFunctionCall 1000341 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:175)
	 */
    private StepResult usrSubfileControlFunctionFields(DspAllHospitalsState dto) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			//switchSUB 175 SUB    
			//switchBLK 1000229 BLK ACT
			// DEBUG genFunctionCall 1000230 ACT CTL.Country Name = CVTVAR(CTL.Country)
			if (dto.getCountry() != null) {
				dto.setCountryName(dto.getCountry().getDescription());
			}
			//switchBLK 1000351 BLK TXT
			// 
			//switchBLK 1000352 BLK ACT
			// DEBUG genFunctionCall 1000353 ACT LCL.Log Function Type = CON.DSPFIL
			dto.setLclLogFunctionType("DSPFIL");
			//switchBLK 1000356 BLK ACT
			// DEBUG genFunctionCall 1000357 ACT LCL.Log User Point = CON.CALC: Subfile control
			dto.setLclLogUserPoint("CALC: Subfile control");
			//switchBLK 1000360 BLK ACT
			// DEBUG genFunctionCall 1000361 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.function fields,CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "function fields"));
			//switchBLK 1000366 BLK ACT
			// DEBUG genFunctionCall 1000367 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Control (Pre-Confirm) (Generated:72)
	 */
    private StepResult usrProcessSubfileControlPreConfirm(DspAllHospitalsState dto) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			//switchSUB 72 SUB    
			//switchBLK 1000160 BLK CAS
			//switchSUB 1000160 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// CTL.*CMD key is *Cancel
				//switchBLK 1000208 BLK ACT
				// DEBUG genFunctionCall 1000209 ACT PAR.*Return code = CND.E
				dto.setLclReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000163 BLK ACT
				// DEBUG genFunctionCall 1000164 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
			//switchBLK 1000387 BLK TXT
			// 
			//switchBLK 1000388 BLK ACT
			// DEBUG genFunctionCall 1000389 ACT LCL.Log Function Type = CON.DSPFIL
			dto.setLclLogFunctionType("DSPFIL");
			//switchBLK 1000392 BLK ACT
			// DEBUG genFunctionCall 1000393 ACT LCL.Log User Point = CON.Process subfile control
			dto.setLclLogUserPoint("Process subfile control");
			//switchBLK 1000396 BLK ACT
			// DEBUG genFunctionCall 1000397 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.(Pre-confirm),CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "(Pre-confirm)"));
			//switchBLK 1000402 BLK ACT
			// DEBUG genFunctionCall 1000403 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:170)
	 */
    private StepResult usrSubfileRecordFunctionFields(DspAllHospitalsState dto, DspAllHospitalsGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			RtvNoOfWardsHospitalDTO rtvNoOfWardsHospitalDTO;
			RtvPatientPerHospitalDTO rtvPatientPerHospitalDTO;
			//switchSUB 170 SUB    
			//switchBLK 1000233 BLK ACT
			// DEBUG genFunctionCall 1000234 ACT RCD.Country Name = CVTVAR(RCD.Country)
			gdo.setCountryName(gdo.getCountry().getDescription());
			//switchBLK 1000237 BLK ACT
			// DEBUG genFunctionCall 1000238 ACT RTV No of wards/Hospital - Ward  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvNoOfWardsHospitalDTO = new RtvNoOfWardsHospitalDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvNoOfWardsHospitalDTO.setHospitalCode(gdo.getHospitalCode());
			// DEBUG genFunctionCall Service call
			rtvNoOfWardsHospitalService.execute(rtvNoOfWardsHospitalDTO);
			// DEBUG genFunctionCall Parameters OUT
			gdo.setNumberOfWards(rtvNoOfWardsHospitalDTO.getNumberOfWards());
			gdo.setNumber5(rtvNoOfWardsHospitalDTO.getCount());
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000242 BLK ACT
			// DEBUG genFunctionCall 1000243 ACT RTV Patient per Hospital - Patient  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvPatientPerHospitalDTO = new RtvPatientPerHospitalDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvPatientPerHospitalDTO.setHospitalCode(gdo.getHospitalCode());
			// DEBUG genFunctionCall Service call
			rtvPatientPerHospitalService.execute(rtvPatientPerHospitalDTO);
			// DEBUG genFunctionCall Parameters OUT
			gdo.setNumber5(rtvPatientPerHospitalDTO.getCount());
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000413 BLK TXT
			// 
			//switchBLK 1000414 BLK ACT
			// DEBUG genFunctionCall 1000415 ACT LCL.Log Function Type = CON.DSPFIL
			dto.setLclLogFunctionType("DSPFIL");
			//switchBLK 1000418 BLK ACT
			// DEBUG genFunctionCall 1000419 ACT LCL.Log User Point = CON.Subfile record function
			dto.setLclLogUserPoint("Subfile record function");
			//switchBLK 1000422 BLK ACT
			// DEBUG genFunctionCall 1000423 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.fields,CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "fields"));
			//switchBLK 1000428 BLK ACT
			// DEBUG genFunctionCall 1000429 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			//createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Record (Pre-Confirm) (Generated:101)
	 */
    private StepResult usrProcessSubfileRecordPreConfirm(DspAllHospitalsState dto, DspAllHospitalsGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	BeanUtils.copyProperties(gdo, dto);
        	CreateLogFileDTO createLogFileDTO;
			//switchSUB 101 SUB    
			//switchBLK 1000030 BLK CAS
			//switchSUB 1000030 BLK CAS
			if (gdo.get_SysSelected().equals("Display Patients")) {
				// RCD.*SFLSEL is Display Patients
				//switchBLK 1000033 BLK ACT
				// DEBUG genFunctionCall 1000034 ACT DSP Wards per Hospital 2 - Ward  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				DspWardsPerHospital2Params dspWardsPerHospital2Params = new DspWardsPerHospital2Params();
				BeanUtils.copyProperties(dto, dspWardsPerHospital2Params);
				result = StepResult.callService(DspWardsPerHospital2Service.class, dspWardsPerHospital2Params).thenCall(serviceDspWardsPerHospital2);
				}
				//switchBLK 1000121 BLK CAS
				//switchSUB 1000121 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// PAR.*Return code is E
					//switchBLK 1000124 BLK ACT
					// DEBUG genFunctionCall 1000125 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000183 BLK ACT
					// DEBUG genFunctionCall 1000184 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000128 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000130 BLK ACT
					// DEBUG genFunctionCall 1000131 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
			//switchBLK 1000185 BLK TXT
			// 
			//switchBLK 1000019 BLK CAS
			//switchSUB 1000019 BLK CAS
			if (gdo.get_SysSelected().equals("Medication")) {
				// RCD.*SFLSEL is Medication
				//switchBLK 1000022 BLK ACT
				// DEBUG genFunctionCall 1000023 ACT DSP Patients per Hospital - Patient  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				DspPatientsPerHospitalParams dspPatientsPerHospitalParams = new DspPatientsPerHospitalParams();
				BeanUtils.copyProperties(dto, dspPatientsPerHospitalParams);
				result = StepResult.callService(DspPatientsPerHospitalService.class, dspPatientsPerHospitalParams).thenCall(serviceDspPatientsPerHospital);
				}
				//switchBLK 1000134 BLK CAS
				//switchSUB 1000134 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// PAR.*Return code is E
					//switchBLK 1000137 BLK ACT
					// DEBUG genFunctionCall 1000138 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000188 BLK ACT
					// DEBUG genFunctionCall 1000189 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000141 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000143 BLK ACT
					// DEBUG genFunctionCall 1000144 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
			//switchBLK 1000186 BLK TXT
			// 
			//switchBLK 1000039 BLK CAS
			//switchSUB 1000039 BLK CAS
			if (gdo.get_SysSelected().equals("Change record")) {
				// RCD.*SFLSEL is Change record
				//switchBLK 1000042 BLK ACT
				// DEBUG genFunctionCall 1000043 ACT ER Edit Hospital Change - Hospital  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				ErEditHospitalChangeParams erEditHospitalChangeParams = new ErEditHospitalChangeParams();
				BeanUtils.copyProperties(dto, erEditHospitalChangeParams);
				result = StepResult.callService(ErEditHospitalChangeService.class, erEditHospitalChangeParams).thenCall(serviceErEditHospitalChange);
				}
				//switchBLK 1000147 BLK CAS
				//switchSUB 1000147 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// PAR.*Return code is E
					//switchBLK 1000150 BLK ACT
					// DEBUG genFunctionCall 1000151 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000206 BLK ACT
					// DEBUG genFunctionCall 1000207 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000154 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000156 BLK ACT
					// DEBUG genFunctionCall 1000157 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
			//switchBLK 1000187 BLK TXT
			// 
			//switchBLK 1000047 BLK CAS
			//switchSUB 1000047 BLK CAS
			if (gdo.get_SysSelected().equals("Doctors")) {
				// RCD.*SFLSEL is Doctors
				//switchBLK 1000050 BLK ACT
				// DEBUG genFunctionCall 1000051 ACT DSPF Doctors for Hospital - Doctor  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				DspfDoctorsForHospitalParams dspfDoctorsForHospitalParams = new DspfDoctorsForHospitalParams();
				BeanUtils.copyProperties(dto, dspfDoctorsForHospitalParams);
				result = StepResult.callService(DspfDoctorsForHospitalService.class, dspfDoctorsForHospitalParams).thenCall(serviceDspfDoctorsForHospital);
				}
				//switchBLK 1000083 BLK CAS
				//switchSUB 1000083 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// PAR.*Return code is E
					//switchBLK 1000086 BLK ACT
					// DEBUG genFunctionCall 1000087 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000190 BLK ACT
					// DEBUG genFunctionCall 1000191 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000092 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000094 BLK ACT
					// DEBUG genFunctionCall 1000095 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
			//switchBLK 1000439 BLK TXT
			// 
			//switchBLK 1000440 BLK ACT
			// DEBUG genFunctionCall 1000441 ACT LCL.Log Function Type = CON.DSPFIL
			dto.setLclLogFunctionType("DSPFIL");
			//switchBLK 1000444 BLK ACT
			// DEBUG genFunctionCall 1000445 ACT LCL.Log User Point = CON.Process subfile record
			dto.setLclLogUserPoint("Process subfile record");
			//switchBLK 1000448 BLK ACT
			// DEBUG genFunctionCall 1000449 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.(Pre-confirm),CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "(Pre-confirm)"));
			//switchBLK 1000454 BLK ACT
			// DEBUG genFunctionCall 1000455 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Final processing (Pre-confirm) (Generated:222)
	 */
    private StepResult usrFinalProcessingPreConfirm(DspAllHospitalsState dto) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			//switchSUB 222 SUB    
			//switchBLK 1000250 BLK CAS
			//switchSUB 1000250 BLK CAS
			if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("16")) {
				// CTL.*CMD key is CF16
				//switchBLK 1000248 BLK ACT
				// DEBUG genFunctionCall 1000249 ACT Execute 'WRKSPLF'
				// TODO: Unsupported Function Type 'Execute Message' (message surrogate = 1103266)
			}
			//switchBLK 1000465 BLK TXT
			// 
			//switchBLK 1000466 BLK ACT
			// DEBUG genFunctionCall 1000467 ACT LCL.Log Function Type = CON.DSPFIL
			dto.setLclLogFunctionType("DSPFIL");
			//switchBLK 1000470 BLK ACT
			// DEBUG genFunctionCall 1000471 ACT LCL.Log User Point = CON.Final processing
			dto.setLclLogUserPoint("Final processing");
			//switchBLK 1000474 BLK ACT
			// DEBUG genFunctionCall 1000475 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.(Pre-confirm),CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "(Pre-confirm)"));
			//switchBLK 1000480 BLK ACT
			// DEBUG genFunctionCall 1000481 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process subfile control (Post-confirm) (Generated:225)
	 */
    private StepResult usrProcessSubfileControlPostConfirm(DspAllHospitalsState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 225 SUB    
			// Unprocessed SUB 225 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Record (Post-Confirm Pass) (Generated:209)
	 */
    private StepResult usrProcessSubfileRecordPostConfirm(DspAllHospitalsState dto, DspAllHospitalsGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 209 SUB    
			// Unprocessed SUB 209 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Final processing (Post-confirm) (Generated:228)
	 */
    private StepResult usrFinalProcessingPostConfirm(DspAllHospitalsState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 228 SUB    
			// Unprocessed SUB 228 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Generated:140)
	 */
    private StepResult usrProcessCommandKeys(DspAllHospitalsState dto) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			//switchSUB 140 SUB    
			//switchBLK 1000065 BLK CAS
			//switchSUB 1000065 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("09"))) {
				// CTL.*CMD key is *Add
				//switchBLK 1000068 BLK ACT
				// DEBUG genFunctionCall 1000069 ACT ER Edit Hospital - Hospital  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				ErEditHospitalParams erEditHospitalParams = new ErEditHospitalParams();
				BeanUtils.copyProperties(dto, erEditHospitalParams);
				result = StepResult.callService(ErEditHospitalService.class, erEditHospitalParams).thenCall(serviceErEditHospital);
				}
				//switchBLK 1000173 BLK CAS
				//switchSUB 1000173 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000073 BLK ACT
					// DEBUG genFunctionCall 1000074 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000215 BLK ACT
					// DEBUG genFunctionCall 1000216 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000176 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000178 BLK ACT
					// DEBUG genFunctionCall 1000179 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
			//switchBLK 1000491 BLK TXT
			// 
			//switchBLK 1000492 BLK ACT
			// DEBUG genFunctionCall 1000493 ACT LCL.Log Function Type = CON.DSPFIL
			dto.setLclLogFunctionType("DSPFIL");
			//switchBLK 1000496 BLK ACT
			// DEBUG genFunctionCall 1000497 ACT LCL.Log User Point = CON.Process command keys
			dto.setLclLogUserPoint("Process command keys");
			//switchBLK 1000506 BLK ACT
			// DEBUG genFunctionCall 1000507 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000181 BLK TXT
			//
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(DspAllHospitalsState dto) {
        StepResult result = NO_ACTION;
        
        try {
        	CreateLogFileDTO createLogFileDTO;
			//switchSUB 132 SUB    
			//switchBLK 1000166 BLK CAS
			//switchSUB 1000166 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// CTL.*CMD key is *Exit
				//switchBLK 1000217 BLK ACT
				// DEBUG genFunctionCall 1000218 ACT PAR.*Return code = CND.*Normal
				dto.setLclReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000169 BLK ACT
				// DEBUG genFunctionCall 1000170 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
			//switchBLK 1000513 BLK TXT
			// 
			//switchBLK 1000514 BLK ACT
			// DEBUG genFunctionCall 1000515 ACT LCL.Log Function Type = CON.DSPFIL
			dto.setLclLogFunctionType("DSPFIL");
			//switchBLK 1000518 BLK ACT
			// DEBUG genFunctionCall 1000519 ACT LCL.Log User Point = CON.Exit program processing
			dto.setLclLogUserPoint("Exit program processing");
			//switchBLK 1000528 BLK ACT
			// DEBUG genFunctionCall 1000529 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */

    /**
     * DspWardsPerHospital2Service returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspWardsPerHospital2(DspAllHospitalsState state, DspWardsPerHospital2Params serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }

    /**
     * DspPatientsPerHospitalService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspPatientsPerHospital(DspAllHospitalsState state, DspPatientsPerHospitalParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }

    /**
     * ErEditHospitalChangeService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceErEditHospitalChange(DspAllHospitalsState state, ErEditHospitalChangeParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }

    /**
     * DspfDoctorsForHospitalService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspfDoctorsForHospital(DspAllHospitalsState state, DspfDoctorsForHospitalParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }

    /**
     * ErEditHospitalService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceErEditHospital(DspAllHospitalsState state, ErEditHospitalParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }
//
//    /**
//     * CvtvarService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCvtvar(DspAllHospitalsState state, CvtvarParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvNoOfWardsHospitalService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvNoOfWardsHospital(DspAllHospitalsState state, RtvNoOfWardsHospitalParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvPatientPerHospitalService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvPatientPerHospital(DspAllHospitalsState state, RtvPatientPerHospitalParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(DspAllHospitalsState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(DspAllHospitalsState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * QuitService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceQuit(DspAllHospitalsState state, QuitParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * WrksplfService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceWrksplf(DspAllHospitalsState state, WrksplfParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * CreateLogFileService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateLogFile(DspAllHospitalsState state, CreateLogFileParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ConcatService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceConcat(DspAllHospitalsState state, ConcatParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//


}
