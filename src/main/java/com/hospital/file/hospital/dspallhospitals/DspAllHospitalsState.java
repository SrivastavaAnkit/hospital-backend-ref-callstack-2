package com.hospital.file.hospital.dspallhospitals;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.ReloadSubfileEnum;

/**
 * State for file 'Hospital' (TSACREP) and function 'DSP All Hospitals' (TSA9DFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class DspAllHospitalsState extends DspAllHospitalsDTO {
	private static final long serialVersionUID = -277754399956644916L;

	// Local fields
	private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");
	private String lclLogFunctionType = "";
	private String lclLogUserPoint = "";

	// System fields
	private long _sysScanLimit = 0L;
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;

    public DspAllHospitalsState() {

    }

	public void setLclLogFunctionType(String logFunctionType) {
    	this.lclLogFunctionType = logFunctionType;
    }

    public String getLclLogFunctionType() {
    	return lclLogFunctionType;
    }
	public void setLclLogUserPoint(String logUserPoint) {
    	this.lclLogUserPoint = logUserPoint;
    }

    public String getLclLogUserPoint() {
    	return lclLogUserPoint;
    }
	public void setLclReturnCode(ReturnCodeEnum returnCode) {
    	this.lclReturnCode = returnCode;
    }

    public ReturnCodeEnum getLclReturnCode() {
    	return lclReturnCode;
    }

	public void set_SysScanLimit(long scanLimit) {
    	_sysScanLimit = scanLimit;
    }

    public long get_SysScanLimit() {
    	return _sysScanLimit;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }
    }