package com.hospital.file.hospital.createhospital;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.hospital.Hospital;
import com.hospital.file.hospital.HospitalId;
import com.hospital.file.hospital.HospitalRepository;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class CreateHospitalService  extends AbstractService<CreateHospitalService, CreateHospitalDTO>
{
    private final Step execute = define("execute", CreateHospitalDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private HospitalRepository hospitalRepository;
	

    @Autowired
    public CreateHospitalService() {
        super(CreateHospitalService.class, CreateHospitalDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(CreateHospitalDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(CreateHospitalDTO dto, CreateHospitalDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		Hospital hospital = new Hospital();
		hospital.setHospitalCode(dto.getHospitalCode());
		hospital.setHospitalName(dto.getHospitalName());
		hospital.setAddressStreet(dto.getAddressStreet());
		hospital.setAddressTown(dto.getAddressTown());
		hospital.setAddressProvince(dto.getAddressProvince());
		hospital.setAddressPostZip(dto.getAddressPostZip());
		hospital.setTelephoneNumber(dto.getTelephoneNumber());
		hospital.setFaxNumber(dto.getFaxNumber());
		hospital.setCountry(dto.getCountry());
		/*hospital.setAddedUser(dto.getAddedUser());
		hospital.setAddedDate(dto.getAddedDate());
		hospital.setAddedTime(dto.getAddedTime());
		hospital.setChangedUser(dto.getChangedUser());
		hospital.setChangedDate(dto.getChangedDate());
		hospital.setChangedTime(dto.getChangedTime());*/

		processingBeforeDataUpdate(dto, hospital);

		Hospital hospital2 = hospitalRepository.findById(hospital.getId()).orElse(null);
		if (hospital2 != null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0003"));
			processingIfDataRecordAlreadyExists(dto, hospital2);
		}
		else {
            try {
				hospitalRepository.save(hospital);
                dto.setReturnCode(ReturnCodeEnum.fromCode(""));
                processingAfterDataUpdate(dto, hospital);
            } catch (Exception e) {
                dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
                processingIfDataUpdateError(dto, hospital);
            }
        }

        return result;
	}

    private StepResult processingBeforeDataUpdate(CreateHospitalDTO dto, Hospital hospital) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT DB1.Added User = JOB.*USER
		/*hospital.setAddedUser(job.getUser());
		//switchBLK 1000013 BLK ACT
		//functionCall 1000014 ACT DB1.Added Date = JOB.*Job date
		hospital.setAddedDate(LocalDate.now());
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT DB1.Added Time = JOB.*Job time
		hospital.setAddedTime(LocalTime.now());*/
       return NO_ACTION;
    }

    private StepResult processingIfDataRecordAlreadyExists(CreateHospitalDTO dto, Hospital hospital) throws ServiceException {
		/**
		 * USER: Processing if Data Record Already Exists (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000019 BLK ACT
		//functionCall 1000020 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
    }

    private StepResult processingAfterDataUpdate(CreateHospitalDTO dto, Hospital hospital) throws ServiceException {
		/**
		 * USER: Processing after Data Update (Generated:11)
		 */
		//switchSUB 11 SUB    
		//switchBLK 1000035 BLK ACT
		//functionCall 1000036 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
    }

    private StepResult processingIfDataUpdateError(CreateHospitalDTO dto, Hospital hospital) throws ServiceException {
		/**
		 * Processing if Data Update Error (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000027 BLK ACT
		//functionCall 1000028 ACT PAR.USR Return Code = CND.Error
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("E"));
       return NO_ACTION;
    }
}
