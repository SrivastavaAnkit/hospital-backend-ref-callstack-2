package com.hospital.file.hospital;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;

import com.hospital.model.CountryEnum;
import com.hospital.model.CountryEnum;

import com.hospital.file.hospital.Hospital;
import com.hospital.file.hospital.selecthospital.SelectHospitalGDO;
import com.hospital.file.hospital.dspfdisplayhospitals.DspfDisplayHospitalsGDO;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsGDO;
import com.hospital.file.hospital.pmtpromptdetaildisplay.PmtPromptDetailDisplayDTO;
import com.hospital.file.hospital.pmthospitalspercountry.PmtHospitalsPerCountryDTO;
import com.hospital.file.hospital.pmttestbatchfunction.PmtTestBatchFunctionDTO;

/**
 * Custom Spring Data JPA repository implementation for model: Hospital (TSACREP).
 *
 * @author X2EGenerator
 */
@Repository
public class HospitalRepositoryImpl implements HospitalRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see com.hospital.file.hospital.HospitalService#deleteHospital(Hospital)
	 */
	@Override
	public void deleteHospital(
		String hospitalCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " hospital.id.hospitalCode = :hospitalCode";
			isParamSet = true;
		}

		String sqlString = "DELETE FROM Hospital hospital";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			query.setParameter("hospitalCode", hospitalCode);
		}

		em.createNativeQuery("SET foreign_key_checks = 0").executeUpdate();
		query.executeUpdate();
		em.createNativeQuery("SET foreign_key_checks = 1").executeUpdate();
	}

	@Override
	public RestResponsePage<SelectHospitalGDO> selectHospital(
		String hospitalCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " (hospital.id.hospitalCode >= :hospitalCode OR hospital.id.hospitalCode like CONCAT('%', :hospitalCode, '%'))";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.hospital.selecthospital.SelectHospitalGDO(hospital.id.hospitalCode, hospital.hospitalName, hospital.addressStreet, hospital.addressTown, hospital.addressProvince, hospital.addressPostZip, hospital.telephoneNumber, hospital.faxNumber, hospital.country) from Hospital hospital";
		String countQueryString = "SELECT COUNT(hospital.id.hospitalCode) FROM Hospital hospital";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Hospital hospital = new Hospital();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(hospital.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"hospital.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"hospital." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"hospital.id.hospitalCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<SelectHospitalGDO> content = query.getResultList();
		RestResponsePage<SelectHospitalGDO> pageDto = new RestResponsePage<SelectHospitalGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.hospital.HospitalService#rtvHospitalDetail(String)
	 */
	@Override
	public Hospital rtvHospitalDetail(
		String hospitalCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " hospital.id.hospitalCode = :hospitalCode";
			isParamSet = true;
		}

		String sqlString = "SELECT hospital FROM Hospital hospital";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			query.setParameter("hospitalCode", hospitalCode);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		Hospital dto = (Hospital)query.getSingleResult();

		return dto;
	}

	@Override
	public RestResponsePage<PmtPromptDetailDisplayDTO> pmtPromptDetailDisplay(
		Pageable pageable) {
		String sqlString = "SELECT new com.hospital.file.hospital.pmtpromptdetaildisplay.PmtPromptDetailDisplayDTO(hospital.id.hospitalCode, hospital.hospitalName, hospital.addressStreet, hospital.addressTown, hospital.addressProvince, hospital.addressPostZip, hospital.telephoneNumber, hospital.faxNumber, hospital.country, hospital.addedUser, hospital.addedDate, hospital.addedTime, hospital.changedUser, hospital.changedDate, hospital.changedTime) from Hospital hospital";
		String countQueryString = "SELECT COUNT(hospital.id.hospitalCode) FROM Hospital hospital";

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<PmtPromptDetailDisplayDTO> content = query.getResultList();
		RestResponsePage<PmtPromptDetailDisplayDTO> pageDto = new RestResponsePage<PmtPromptDetailDisplayDTO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<PmtHospitalsPerCountryDTO> pmtHospitalsPerCountry(
		Pageable pageable) {
		String sqlString = "SELECT new com.hospital.file.hospital.pmthospitalspercountry.PmtHospitalsPerCountryDTO(hospital.id.hospitalCode, hospital.hospitalName, hospital.addressStreet, hospital.addressTown, hospital.addressProvince, hospital.addressPostZip, hospital.telephoneNumber, hospital.faxNumber, hospital.country, hospital.addedUser, hospital.addedDate, hospital.addedTime, hospital.changedUser, hospital.changedDate, hospital.changedTime) from Hospital hospital";
		String countQueryString = "SELECT COUNT(hospital.id.hospitalCode) FROM Hospital hospital";

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<PmtHospitalsPerCountryDTO> content = query.getResultList();
		RestResponsePage<PmtHospitalsPerCountryDTO> pageDto = new RestResponsePage<PmtHospitalsPerCountryDTO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<DspfDisplayHospitalsGDO> dspfDisplayHospitals(
		CountryEnum country, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (country != null) {
			whereClause += " hospital.country = :country";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.hospital.dspfdisplayhospitals.DspfDisplayHospitalsGDO(hospital.country, hospital.id.hospitalCode, hospital.hospitalName, hospital.addressStreet, hospital.addressTown, hospital.addressProvince, hospital.addressPostZip, hospital.telephoneNumber, hospital.faxNumber, hospital.addedUser, hospital.addedDate, hospital.addedTime, hospital.changedUser, hospital.changedDate, hospital.changedTime) from Hospital hospital";
		String countQueryString = "SELECT COUNT(hospital.id.hospitalCode) FROM Hospital hospital";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Hospital hospital = new Hospital();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(hospital.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"hospital.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"hospital." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"hospital.country"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"hospital.id.hospitalCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (country != null) {
			countQuery.setParameter("country", country);
			query.setParameter("country", country);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<DspfDisplayHospitalsGDO> content = query.getResultList();
		RestResponsePage<DspfDisplayHospitalsGDO> pageDto = new RestResponsePage<DspfDisplayHospitalsGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<DspAllHospitalsGDO> dspAllHospitals(
		CountryEnum country, String hospitalName, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (country != null) {
			whereClause += " hospital.country = :country";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(hospitalName)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " hospital.hospitalName like CONCAT('%', :hospitalName, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.hospital.dspallhospitals.DspAllHospitalsGDO(hospital.country, hospital.id.hospitalCode, hospital.hospitalName, hospital.addressStreet, hospital.addressTown, hospital.addressProvince, hospital.addressPostZip, hospital.telephoneNumber, hospital.faxNumber) from Hospital hospital";
		//String sqlString = "SELECT new com.hospital.file.hospital.dspallhospitals.DspAllHospitalsGDO(hospital.country, hospital.id.hospitalCode, hospital.hospitalName, hospital.addressStreet, hospital.addressTown, hospital.addressProvince, hospital.addressPostZip, hospital.telephoneNumber, hospital.faxNumber, hospital.addedUser, hospital.addedDate, hospital.addedTime, hospital.changedUser, hospital.changedDate, hospital.changedTime) from Hospital hospital";
		String countQueryString = "SELECT COUNT(hospital.id.hospitalCode) FROM Hospital hospital";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Hospital hospital = new Hospital();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(hospital.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"hospital.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"hospital." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"hospital.country"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"hospital.id.hospitalCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (country != null) {
			countQuery.setParameter("country", country);
			query.setParameter("country", country);
		}

		if (StringUtils.isNotEmpty(hospitalName)) {
			countQuery.setParameter("hospitalName", hospitalName);
			query.setParameter("hospitalName", hospitalName);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<DspAllHospitalsGDO> content = query.getResultList();
		RestResponsePage<DspAllHospitalsGDO> pageDto = new RestResponsePage<DspAllHospitalsGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<PmtTestBatchFunctionDTO> pmtTestBatchFunction(
		Pageable pageable) {
		String sqlString = "SELECT new com.hospital.file.hospital.pmttestbatchfunction.PmtTestBatchFunctionDTO(hospital.id.hospitalCode, hospital.hospitalName, hospital.addressStreet, hospital.addressTown, hospital.addressProvince, hospital.addressPostZip, hospital.telephoneNumber, hospital.faxNumber, hospital.country, hospital.addedUser, hospital.addedDate, hospital.addedTime, hospital.changedUser, hospital.changedDate, hospital.changedTime) from Hospital hospital";
		String countQueryString = "SELECT COUNT(hospital.id.hospitalCode) FROM Hospital hospital";

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<PmtTestBatchFunctionDTO> content = query.getResultList();
		RestResponsePage<PmtTestBatchFunctionDTO> pageDto = new RestResponsePage<PmtTestBatchFunctionDTO>(
				content, pageable, count.longValue());

		return pageDto;
	}

}
