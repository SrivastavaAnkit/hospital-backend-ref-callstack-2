package com.hospital.file.hospital.deletehospital;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.hospital.Hospital;
import com.hospital.file.hospital.HospitalId;
import com.hospital.file.hospital.HospitalRepository;
import com.hospital.file.doctor.rtvhospitalexist.RtvHospitalExistService;
import com.hospital.file.doctor.rtvhospitalexist.RtvHospitalExistDTO;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class DeleteHospitalService  extends AbstractService<DeleteHospitalService, DeleteHospitalDTO>
{
    private final Step execute = define("execute", DeleteHospitalDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private HospitalRepository hospitalRepository;
	
	@Autowired
	private RtvHospitalExistService rtvHospitalExistService;
	

    @Autowired
    public DeleteHospitalService() {
        super(DeleteHospitalService.class, DeleteHospitalDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(DeleteHospitalDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(DeleteHospitalDTO dto, DeleteHospitalDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataUpdate(dto);
		HospitalId hospitalId = new HospitalId();
		hospitalId.setHospitalCode(dto.getHospitalCode());
		try {
			hospitalRepository.deleteById(hospitalId);
			hospitalRepository.flush();
			dto.setReturnCode(ReturnCodeEnum.fromCode(""));
			processingAfterDataUpdate(dto);
		} catch (Exception e) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
		}

       return result;
	}

	private StepResult processingBeforeDataUpdate(DeleteHospitalDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Generated:9)
		 */
		RtvHospitalExistDTO rtvHospitalExistDTO;
		//switchSUB 9 SUB    
		//switchBLK 1000003 BLK ACT
		//functionCall 1000004 ACT RTV Hospital exist? - Doctor  *
		rtvHospitalExistDTO = new RtvHospitalExistDTO();
		rtvHospitalExistDTO.setHospitalCode(dto.getHospitalCode());
		rtvHospitalExistService.execute(rtvHospitalExistDTO);
		dto.setLclUsrReturnCode(rtvHospitalExistDTO.getUsrReturnCode());
		//switchBLK 1000007 BLK CAS
		//switchSUB 1000007 BLK CAS
		if (dto.getLclUsrReturnCode() == UsrReturnCodeEnum.fromCode("F")) {
			// LCL.USR Return Code is Record found
			//switchBLK 1000016 BLK ACT
			//functionCall 1000017 ACT Send error message - 'Cant delete hospital'
			// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1103225)
			//switchBLK 1000018 BLK ACT
			//functionCall 1000019 ACT <-- *QUIT
			// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
		}
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(DeleteHospitalDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:17)
		 */
		//switchSUB 17 SUB    
		//switchBLK 1000010 BLK ACT
		//functionCall 1000011 ACT PGM.*Return code = CND.*Normal
		dto.setReturnCode(ReturnCodeEnum.fromCode(""));
       return NO_ACTION;
	}
}
