package com.hospital.file.hospital.eredithospitalchange;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Params for resource: ErEditHospitalChange (TSBCE1R).
 *
 * @author X2EGenerator
 */
public class ErEditHospitalChangeParams implements Serializable{
    private String hospitalCode = "";

    public String getHospitalCode() {
		return hospitalCode;
	}
	
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
}
