package com.hospital.file.doctor.editdoctorhospital;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


import com.hospital.common.exception.ServiceException;

import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorService;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailService;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorDTO;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;


/**
 * Spring Validator for file 'Doctor' (TSAFREP) and function 'Edit Doctor Hospital' (TSBPEFR).
 *
 * @author X2EGenerator
 */
@Component
public class EditDoctorHospitalValidator implements Validator {
	
	
	@Autowired
	private RtvForSupervisorService rtvForSupervisorService;
	
	@Autowired
	private RtvHospitalDetailService rtvHospitalDetailService;
	

	@Override
	public boolean supports(Class<?> clazz) {
		return EditDoctorHospitalDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors e) {
		if (!(object instanceof EditDoctorHospitalDTO)) {
			e.reject("Not a valid EditDoctorHospitalDTO");
		} else {
			EditDoctorHospitalDTO dto = (EditDoctorHospitalDTO)object;
			EditDoctorHospitalGDO gdo = dto.getGdo();
			EditDoctorHospitalParams params = new EditDoctorHospitalParams();
			BeanUtils.copyProperties(dto, params);
		}
	}
}
