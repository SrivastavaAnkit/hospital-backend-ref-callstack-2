package com.hospital.file.doctor.editdoctorhospital;

import com.hospital.common.exception.ServiceException;

/**
 * Service interface for resource: EditDoctorHospital (TSBPEFR).
 *
 * @author X2EGenerator
 */
public interface EditDoctorHospitalService {

	/**
	 * Initialize Program.
	 *
	 * @param dto EditDoctorHospitalDTO initialized by the controller
	 * @param params function parameters
	 * @throws ServiceException if a service exception occurs
	 */
	void initialize(EditDoctorHospitalDTO dto, EditDoctorHospitalParams params) throws ServiceException;

	/**
	 * Initialize Subfile Header.
	 *
	 * @param dto EditDoctorHospitalDTO initialized by the controller
	 * @param params function parameters
	 * @throws ServiceException if a service exception occurs
	 */
	void initializeSubfileHeader(EditDoctorHospitalDTO dto, EditDoctorHospitalParams params) throws ServiceException;

	/**
	 * Initialize Subfile Record (Existing Record).
	 *
	 * @param dto EditDoctorHospitalDTO initialized by the controller
	 * @param gdo EditDoctorHospitalGDO filled by the controller
	 * @throws ServiceException if a service exception occurs
	 */
	void initializeExistingRecord(EditDoctorHospitalDTO dto, EditDoctorHospitalGDO gdo) throws ServiceException;

}
