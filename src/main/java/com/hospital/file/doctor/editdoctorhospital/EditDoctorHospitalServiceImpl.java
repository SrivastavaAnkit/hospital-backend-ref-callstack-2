package com.hospital.file.doctor.editdoctorhospital;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.hospital.file.doctor.Doctor;

import com.hospital.file.doctor.DoctorRepository;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorService;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailService;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorDTO;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;

import com.hospital.common.exception.ServiceException;


/**
 * Service implementation for resource: EditDoctorHospital (TSBPEFR).
 *
 * @author X2EGenerator
 */
@Service
public class EditDoctorHospitalServiceImpl implements EditDoctorHospitalService {

	@Autowired
	private DoctorRepository doctorRepository;
	
	@Autowired
	private RtvForSupervisorService rtvForSupervisorService;
	
	@Autowired
	private RtvHospitalDetailService rtvHospitalDetailService;
	

	@Autowired
	private MessageSource messageSource;

	/**
	 * Initialize Program.
	 *
	 * @param dto EditDoctorHospitalDTO initialized by the controller
	 * @param params function parameters
	 * @throws ServiceException if a service exception occurs
	 */
	@Override
	public void initialize(EditDoctorHospitalDTO dto,	EditDoctorHospitalParams params) throws ServiceException {

		/**
		 * USER: Initialize Program (Empty:15)
		 */
		//switchSUB 15 SUB    
		// Unprocessed SUB 15 -
	}

	/**
	 * Initialize Subfile Header.
	 *
	 * @param dto EditDoctorHospitalDTO initialized by the controller
	 * @param params function parameters
	 * @throws ServiceException if a service exception occurs
	 */
	@Override
	public void initializeSubfileHeader(EditDoctorHospitalDTO dto, EditDoctorHospitalParams params) throws ServiceException {

		/**
		 * USER: Initialize Subfile Header (Empty:1488)
		 */
		//switchSUB 1488 SUB    
		// Unprocessed SUB 1488 -
		
	}

	/**
	 * Initialize Subfile Record (Existing Record).
	 *
	 * @param dto EditDoctorHospitalDTO initialized by the controller
	 * @param gdo EditDoctorHospitalGDO filled by the controller
	 * @throws ServiceException if a service exception occurs
	 */
	@Override
	public void initializeExistingRecord(EditDoctorHospitalDTO dto, EditDoctorHospitalGDO gdo) throws ServiceException {

		/**
		 * USER: Initialize Subfile Record (Existing Record) (Generated:1023)
		 */
		RtvForSupervisorDTO rtvForSupervisorDTO;
		RtvHospitalDetailDTO rtvHospitalDetailDTO;
		//switchSUB 1023 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT RTV For Supervisor - Doctor  *
		rtvForSupervisorDTO = new RtvForSupervisorDTO();
		rtvForSupervisorDTO.setDoctorCode(gdo.getSupervisingDoctorDoctor());
		rtvForSupervisorService.execute(rtvForSupervisorDTO);
		gdo.setSupervisingDoctorName(rtvForSupervisorDTO.getDoctorName());
		//switchBLK 1000032 BLK ACT
		//functionCall 1000033 ACT RTV Hospital detail - Hospital  *
		rtvHospitalDetailDTO = new RtvHospitalDetailDTO();
		rtvHospitalDetailDTO.setHospitalCode(gdo.getHospitalCode());
		rtvHospitalDetailService.execute(rtvHospitalDetailDTO);
		gdo.setHospitalName(rtvHospitalDetailDTO.getHospitalName());
		
	}
}
