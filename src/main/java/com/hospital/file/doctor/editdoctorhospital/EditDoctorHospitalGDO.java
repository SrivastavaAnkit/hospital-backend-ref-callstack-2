package com.hospital.file.doctor.editdoctorhospital;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.file.doctor.Doctor;
import com.hospital.model.SpecialityLevelEnum;

/**
 * Gdo for file 'Doctor' (TSAFREP) and function 'Edit Doctor Hospital' (TSBPEFR).
 */
public class EditDoctorHospitalGDO implements Serializable {
	private static final long serialVersionUID = 7021374529382531891L;

	private String hospitalCode = "";
	private String hospitalName = "";
	private String gdo_selected = "";
	private String doctorCode = "";
	private String doctorName = "";
	private long doctorContactNumber = 0L;
	private SpecialityLevelEnum specialityLevel = null;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;
	private String supervisingDoctorDoctor = "";
	private String supervisingDoctorName = "";

	public EditDoctorHospitalGDO() {

	}

	public EditDoctorHospitalGDO(Doctor doctor) {
		setDtoFields(doctor);
	}

	public EditDoctorHospitalGDO(String hospitalCode, String doctorCode, String doctorName, long doctorContactNumber, SpecialityLevelEnum specialityLevel, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime, String supervisingDoctorDoctor) {
		this.hospitalCode = hospitalCode;
		this.doctorCode = doctorCode;
		this.doctorName = doctorName;
		this.doctorContactNumber = doctorContactNumber;
		this.specialityLevel = specialityLevel;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
		this.supervisingDoctorDoctor = supervisingDoctorDoctor;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void set_Selected(String gdo_selected) {
		this.gdo_selected = gdo_selected;
	}

	public String get_Selected() {
		return gdo_selected;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorContactNumber(long doctorContactNumber) {
		this.doctorContactNumber = doctorContactNumber;
	}

	public long getDoctorContactNumber() {
		return doctorContactNumber;
	}

	public void setSpecialityLevel(SpecialityLevelEnum specialityLevel) {
		this.specialityLevel = specialityLevel;
	}

	public SpecialityLevelEnum getSpecialityLevel() {
		return specialityLevel;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void setSupervisingDoctorDoctor(String supervisingDoctorDoctor) {
		this.supervisingDoctorDoctor = supervisingDoctorDoctor;
	}

	public String getSupervisingDoctorDoctor() {
		return supervisingDoctorDoctor;
	}

	public void setSupervisingDoctorName(String supervisingDoctorName) {
		this.supervisingDoctorName = supervisingDoctorName;
	}

	public String getSupervisingDoctorName() {
		return supervisingDoctorName;
	}

	/**
	 * Copies the fields of the Entity bean into the GDO bean.
	 *
	 * @param doctor Doctor Entity bean
	 */
	public void setDtoFields(Doctor doctor) {
		BeanUtils.copyProperties(doctor, this);
	}

	/**
	 * Copies the fields of the GDO bean into the Entity bean.
	 *
	 * @param doctor Doctor Entity bean
	 */
	public void setEntityFields(Doctor doctor) {
		BeanUtils.copyProperties(this, doctor);
	}
}
