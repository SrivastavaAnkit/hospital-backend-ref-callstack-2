package com.hospital.file.doctor.editdoctorhospital;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;

import com.hospital.file.doctor.Doctor;
import com.hospital.file.doctor.DoctorId;

import com.hospital.model.ProgramModeEnum;

import com.hospital.file.doctor.DoctorRepository;

import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorService;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailService;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorDTO;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;

import com.hospital.common.exception.ServiceException;

import com.hospital.file.doctor.changedoctor.ChangeDoctorService;
import com.hospital.file.doctor.changedoctor.ChangeDoctorDTO;

import com.hospital.file.doctor.createdoctor.CreateDoctorService;
import com.hospital.file.doctor.createdoctor.CreateDoctorDTO;

import com.hospital.file.doctor.deletedoctor.DeleteDoctorService;
import com.hospital.file.doctor.deletedoctor.DeleteDoctorDTO;

/**
 * Controller/EDTFIL for resource: EditDoctorHospital (TSBPEFR).
 *
 * @author X2EGenerator
 */
@RestController()
public class EditDoctorHospitalController {

	@Autowired
	private DoctorRepository doctorRepository;


	@Autowired
	private ChangeDoctorService changeDoctorService;


	@Autowired
	private CreateDoctorService createDoctorService;


	@Autowired
	private DeleteDoctorService deleteDoctorService;


	@Autowired
	private EditDoctorHospitalService editDoctorHospitalService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private EditDoctorHospitalValidator editDoctorHospitalValidator;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(editDoctorHospitalValidator);
	}

	@Transactional
	@RequestMapping(value="/Doctor/EditDoctorHospital/initialize", method=RequestMethod.POST, produces="application/json;charset=utf-8")
	public EditDoctorHospitalDTO initDoctor(@RequestBody EditDoctorHospitalDTO dto) throws ServiceException {
		EditDoctorHospitalParams params = new EditDoctorHospitalParams();
		BeanUtils.copyProperties(dto, params);
		editDoctorHospitalService.initialize(dto, params);
		editDoctorHospitalService.initializeSubfileHeader(dto, params);
	
		return dto;
	}
	
	@Transactional
	@RequestMapping(value={"/Doctor/EditDoctorHospital/loadSubfilePage"}, method=RequestMethod.POST, produces="application/json;charset=utf-8")
	public EditDoctorHospitalDTO selectEditDoctorHospital(@RequestBody EditDoctorHospitalDTO dto,
		@RequestParam(value="page", required=false, defaultValue="0") int page,
		@RequestParam(value="size", required=false, defaultValue="10") int size,
		@RequestParam(value="sortdata", required=false, defaultValue="") String sortData,
	 	HttpServletRequest request) throws ServiceException {
	
		EditDoctorHospitalParams params = new EditDoctorHospitalParams();
		BeanUtils.copyProperties(dto, params);
	
		List<Order> sortOrders = new ArrayList<Order>();
		Pageable pageable;
	
		try {
			Map<String, String> sortDataMap =
				new ObjectMapper().readValue(sortData, LinkedHashMap.class);
	
			for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
				if (entry.getValue() == null) {
					continue;
				}
	
				sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
			}
		} 
		catch (IOException ioe) { }
	
		if (CollectionUtils.isEmpty(sortOrders)) {
			pageable = new PageRequest(page, size);
		} 
		else {
			pageable = new PageRequest(page, size, new Sort(sortOrders));
		}
	
		Page<EditDoctorHospitalGDO> pageDto = doctorRepository.editDoctorHospital(params.getHospitalCode(), dto.getDoctorCode(), pageable);
		for (EditDoctorHospitalGDO gdo : pageDto.getContent()) {
			editDoctorHospitalService.initializeExistingRecord(dto, gdo);
		}
		dto.setPageDto(pageDto);
	
		return dto;
	}
	
	/*@Transactional
	@RequestMapping(value="/Doctor/EditDoctorHospital/save", method=RequestMethod.PUT)
	public EditDoctorHospitalDTO saveDoctor(@RequestBody @Valid EditDoctorHospitalDTO dto) throws ServiceException {
	
		EditDoctorHospitalGDO editDoctorHospitalGdo = dto.getGdo();
		CreateDoctorDTO createDoctorDTO = new CreateDoctorDTO();
		createDoctorDTO.setDoctorCode(editDoctorHospitalGdo.getDoctorCode());
		createDoctorDTO.setHospitalCode(editDoctorHospitalGdo.getHospitalCode());
		createDoctorDTO.setSupervisingDoctorDoctor(editDoctorHospitalGdo.getSupervisingDoctorDoctor());
		createDoctorDTO.setDoctorName(editDoctorHospitalGdo.getDoctorName());
		createDoctorDTO.setDoctorContactNumber(editDoctorHospitalGdo.getDoctorContactNumber());
		createDoctorDTO.setSpecialityLevel(editDoctorHospitalGdo.getSpecialityLevel());
		createDoctorService.execute(createDoctorDTO);
		editDoctorHospitalGdo.setUsrReturnCode(createDoctorDTO.getUsrReturnCode());
	
		return dto;
	}
	
	@Transactional
	@RequestMapping(value="/Doctor/EditDoctorHospital/update", method=RequestMethod.PUT)
	public EditDoctorHospitalDTO updateDoctor(@RequestBody @Valid EditDoctorHospitalDTO dto) throws ServiceException {
			
		DoctorId doctorId = new DoctorId(dto.getGdo().getDoctorCode());
		if(doctorRepository.findOne(doctorId) != null) {
			EditDoctorHospitalGDO editDoctorHospitalGdo = dto.getGdo();
			editDoctorHospitalService.initializeExistingRecord(dto, editDoctorHospitalGdo);
	
			ChangeDoctorDTO changeDoctorDTO = new ChangeDoctorDTO();
			changeDoctorDTO.setDoctorCode(editDoctorHospitalGdo.getDoctorCode());
			changeDoctorDTO.setHospitalCode(editDoctorHospitalGdo.getHospitalCode());
			changeDoctorDTO.setSupervisingDoctorDoctor(editDoctorHospitalGdo.getSupervisingDoctorDoctor());
			changeDoctorDTO.setDoctorName(editDoctorHospitalGdo.getDoctorName());
			changeDoctorDTO.setDoctorContactNumber(editDoctorHospitalGdo.getDoctorContactNumber());
			changeDoctorDTO.setSpecialityLevel(editDoctorHospitalGdo.getSpecialityLevel());
			changeDoctorService.execute(changeDoctorDTO);
			editDoctorHospitalGdo.setUsrReturnCode(changeDoctorDTO.getUsrReturnCode());
		
			return dto;
		}
		else {
			throw new ServiceException("doctor.nf");
		}
	}
	
	@Transactional
	@RequestMapping(value="/Doctor/EditDoctorHospital/delete", method=RequestMethod.POST)
	public void deleteDoctor(@RequestBody @Valid EditDoctorHospitalDTO dto) throws ServiceException {
	
		EditDoctorHospitalGDO editDoctorHospitalGdo = dto.getGdo();
		DeleteDoctorDTO deleteDoctorDTO = new DeleteDoctorDTO();
		deleteDoctorDTO.setDoctorCode(editDoctorHospitalGdo.getDoctorCode());
		deleteDoctorService.execute(deleteDoctorDTO);
	}
	*/
	
}
