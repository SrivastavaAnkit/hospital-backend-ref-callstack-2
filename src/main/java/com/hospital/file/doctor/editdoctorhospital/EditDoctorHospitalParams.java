package com.hospital.file.doctor.editdoctorhospital;

/**
 * Params for resource: EditDoctorHospital (TSBPEFR).
 *
 * @author X2EGenerator
 */
public class EditDoctorHospitalParams {
	private String hospitalCode = "";

	public String getHospitalCode() {
		return hospitalCode;
	}
	
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
}
