package com.hospital.file.doctor.editdoctorhospital;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;

import com.hospital.file.doctor.Doctor;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.common.state.BaseDTO;

/**
 * Dto for file 'Doctor' (TSAFREP) and function 'Edit Doctor Hospital' (TSBPEFR).
 */
public class EditDoctorHospitalDTO extends BaseDTO {
	private static final long serialVersionUID = -2311752093277011204L;

	private String doctorCode = "";
	private String hospitalCode = "";
	private String hospitalName = "";
	private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");
	private Page<EditDoctorHospitalGDO> pageDto;
	private EditDoctorHospitalGDO gdo;

	public EditDoctorHospitalDTO() {

	}

	public EditDoctorHospitalDTO(Doctor doctor) {
		setDtoFields(doctor);
	}

	public EditDoctorHospitalDTO(String hospitalCode, String hospitalName) {
		this.hospitalCode = hospitalCode;
		this.hospitalName = hospitalName;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setPageDto(Page<EditDoctorHospitalGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public Page<EditDoctorHospitalGDO> getPageDto() {
		return pageDto;
	}

	public void setGdo(EditDoctorHospitalGDO gdo) {
		this.gdo = gdo;
	}

	public EditDoctorHospitalGDO getGdo() {
		return gdo;
	}

	/**
	 * Copies the fields of the Entity bean into the DTO bean.
	 *
	 * @param doctor Doctor Entity bean
	 */
	public void setDtoFields(Doctor doctor) {
		BeanUtils.copyProperties(doctor, this);
	}

	/**
	 * Copies the fields of the DTO bean into the Entity bean.
	 *
	 * @param doctor Doctor Entity bean
	 */
	public void setEntityFields(Doctor doctor) {
		BeanUtils.copyProperties(this, doctor);
	}
}
