package com.hospital.file.doctor.dspdoctordetail;

import java.io.Serializable;


/**
 * Params for resource: DspDoctorDetail (TSAUD1R).
 *
 * @author X2EGenerator DSPRCDJavaControllerGenerator.kt
 */
public class DspDoctorDetailParams implements Serializable {
    private static final long serialVersionUID = -5356460572578856017L;

	private String doctorCode = "";

	public String getDoctorCode() {
		return doctorCode;
	}
	
	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}
}

