package com.hospital.file.doctor.dspdoctordetail;
 
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.hospital.file.logfile.LogFile;
import com.hospital.file.doctor.Doctor;
import com.hospital.file.doctor.DoctorId;
import com.hospital.file.doctor.DoctorRepository;

import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorService;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorDTO;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;
import com.hospital.support.JobContext;
import com.hospital.model.CmdKeyEnum;

/**
 * Service implementation for resource: DspDoctorDetail (TSAUD1R).
 *
 * @author X2EGenerator
 */
@Service
public class DspDoctorDetailService extends AbstractService<DspDoctorDetailService, DspDoctorDetailState> {
    

	@Autowired
	private JobContext job;

	@Autowired
	private DoctorRepository doctorRepository;

    @Autowired
	private MessageSource messageSource;

    
    @Autowired
    private CreateLogFileService createLogFileService;
    
    @Autowired
    private RtvForSupervisorService rtvForSupervisorService;
    
	//private final Step serviceRtvForSupervisor = define("serviceRtvForSupervisor",RtvForSupervisorParams.class, this::processServiceRtvForSupervisor);
	//private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
	//private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
	//private final Step serviceDoctorNf = define("serviceDoctorNf",DoctorNfParams.class, this::processServiceDoctorNf);
	//private final Step serviceCreateLogFile = define("serviceCreateLogFile",CreateLogFileParams.class, this::processServiceCreateLogFile);
	//private final Step serviceConcat = define("serviceConcat",ConcatParams.class, this::processServiceConcat);
	
    
	public static final String SCREEN_KEY = "dspDoctorDetail";
    public static final String SCREEN_DTL = "DspDoctorDetail.detail";

    private final Step execute = define("execute", DspDoctorDetailParams.class, this::execute);
    private final Step keyScreenResponse = define("keyScreen", DspDoctorDetailState.class, this::processResponseToKeyScreen);
    private final Step detailScreenResponse = define("detailScreen", DspDoctorDetailState.class, this::processResponseToDetailScreen);
    
    @Autowired
	public DspDoctorDetailService() {
        super(DspDoctorDetailService.class, DspDoctorDetailState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * DSPRCD controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(DspDoctorDetailState state, DspDoctorDetailParams params) {
    	StepResult result = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        result = usrInitializeProgram(state);

        result = conductKeyScreenConversation(state);

        return result;
    }

    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductKeyScreenConversation(DspDoctorDetailState state) {
    	StepResult result = NO_ACTION;

    	result = usrLoadKeyScreen(state);
    	result = displayKeyScreenConversation(state);

        return result;
    }

    /**
     * SCREEN_KEY display processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult displayKeyScreenConversation(DspDoctorDetailState state) {
        StepResult result = NO_ACTION;

     	if (state.getConductKeyScreenConversation()) {
            DspDoctorDetailDTO model = new DspDoctorDetailDTO();
            BeanUtils.copyProperties(state, model);
    		result = callScreen(SCREEN_KEY, model).thenCall(keyScreenResponse);
    	}

    	return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processResponseToKeyScreen(DspDoctorDetailState state, DspDoctorDetailState model) {
    	StepResult result = NO_ACTION;

        // Update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            result = closedown(state);
        } else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
        	result = conductKeyScreenConversation(state);
        } else if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
        	result = displayKeyScreenConversation(state);
        } else {
            result = validateKeyScreen(state);
            if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
            	result = closedown(state);
            } else {
            	result = conductDetailScreenConversation(state);
            }
        }

        return result;
    }

    /**
     * SCREEN_KEY validate key screen.
     * @param state - Service state class.
     * @return
     */
    private StepResult validateKeyScreen(DspDoctorDetailState state) {
    	StepResult result = NO_ACTION;

        result = usrValidateKeyScreen(state);

        DoctorId doctorId = new DoctorId(state.getDoctorCode());
        Doctor doctor = doctorRepository.findById(doctorId).orElse(null);
        BeanUtils.copyProperties(doctor, state);

        result = usrLoadDetailScreenFromDbfRecord(state);

        return result;
    }

    /**
     * SCREEN_DETAIL display processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductDetailScreenConversation(DspDoctorDetailState state) {
        StepResult result = NO_ACTION;

		if (state.getConductDetailScreenConversation()) {
            DspDoctorDetailDTO model = new DspDoctorDetailDTO();
            BeanUtils.copyProperties(state, model);
    		result = callScreen(SCREEN_DTL, model).thenCall(detailScreenResponse);
		}

		return result;
	}

    /**
     * SCREEN_DETAIL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processResponseToDetailScreen(DspDoctorDetailState state, DspDoctorDetailState model) {
    	StepResult result = NO_ACTION;

    	// update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model,state);

        if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
             result = closedown(state);
        } else if (CmdKeyEnum.isKeyScreen(state.get_SysCmdKey())) {
            result = usrProcessKeyScreenRequest(state);
        } else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
        	result = conductDetailScreenConversation(state);
        } else if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
        	result = conductKeyScreenConversation(state);
        } else {
            result = processDetailScreen(state);
        }

        return result;
    }

    /**
     * SCREEN_DETAIL  process detail screen and return to key screen
     * @param state - Service state class.
     * @return
     */
    private StepResult processDetailScreen(DspDoctorDetailState state) {
    	StepResult result = NO_ACTION;

        checkInputField();
        if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
        	return closedown(state);
        } else {
        	result = usrDetailScreenFunctionFields(state);
            result = usrValidateDetailScreen(state);
            if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
            	return closedown(state);
            } else {
            	result = usrPerformConfirmedAction(state);
            	if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
                	return closedown(state);
                } else {
                	result = usrProcessCommandKeys(state);
                }
            }
        }

        result = conductKeyScreenConversation(state);

        return result;
    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(DspDoctorDetailState state) {
        StepResult result = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        result = usrExitProgramProcessing(state);

        return result;
    }

    private StepResult checkInputField() {
        return NO_ACTION;
    } 
    

    /**
	 * ==================================== Synon user point(s) ==========================================
     */

	/**
	 * USER: Initialize Program (Empty:1)
	 */
    private StepResult usrInitializeProgram(DspDoctorDetailState dto) {
        StepResult result = NO_ACTION;

        try {
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Load Key Screen (Generated:440)
	 */
    private StepResult usrLoadKeyScreen(DspDoctorDetailState dto) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			//switchSUB 440 SUB    
			//switchBLK 1000074 BLK ACT
			// DEBUG genFunctionCall 1000075 ACT PAR.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
			//switchBLK 1000140 BLK TXT
			// 
			//switchBLK 1000141 BLK ACT
			// DEBUG genFunctionCall 1000142 ACT LCL.Log Function Type = CON.DSPRCD
			dto.setLclLogFunctionType("DSPRCD");
			//switchBLK 1000145 BLK ACT
			// DEBUG genFunctionCall 1000146 ACT LCL.Log User Point = CON.Load key screen
			dto.setLclLogUserPoint("Load key screen");
			//switchBLK 1000155 BLK ACT
			// DEBUG genFunctionCall 1000156 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Key Screen (Generated:415)
	 */
    private StepResult usrValidateKeyScreen(DspDoctorDetailState dto) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			//switchSUB 415 SUB    
			//switchBLK 1000080 BLK CAS
			//switchSUB 1000080 BLK CAS
			if (dto.getDoctorCode() != null && dto.getDoctorCode().isEmpty()) {
				// KEY.Doctor Code is Blank
				//switchBLK 1000085 BLK ACT
				// DEBUG genFunctionCall 1000086 ACT Send error message - 'Doctor                 NF'
				//e.rejectValue("", "doctor.nf");
			}
			//switchBLK 1000164 BLK TXT
			// 
			//switchBLK 1000165 BLK ACT
			// DEBUG genFunctionCall 1000166 ACT LCL.Log Function Type = CON.CRTOBJ
			dto.setLclLogFunctionType("CRTOBJ");
			//switchBLK 1000169 BLK ACT
			// DEBUG genFunctionCall 1000170 ACT LCL.Log User Point = CON.Validate key screen
			dto.setLclLogUserPoint("Validate key screen");
			//switchBLK 1000179 BLK ACT
			// DEBUG genFunctionCall 1000180 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Load Detail Screen from DBF Record (Generated:100)
	 */
    private StepResult usrLoadDetailScreenFromDbfRecord(DspDoctorDetailState dto) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			RtvForSupervisorDTO rtvForSupervisorDTO;
			//switchSUB 100 SUB    
			//switchBLK 1000001 BLK ACT
			// DEBUG genFunctionCall 1000002 ACT RTV For Supervisor - Doctor  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvForSupervisorDTO = new RtvForSupervisorDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvForSupervisorDTO.setDoctorCode(dto.getDoctorCode());
			// DEBUG genFunctionCall Service call
			rtvForSupervisorService.execute(rtvForSupervisorDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setSupervisingDoctorName(rtvForSupervisorDTO.getDoctorName());
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000186 BLK TXT
			// 
			//switchBLK 1000187 BLK ACT
			// DEBUG genFunctionCall 1000188 ACT LCL.Log Function Type = CON.CRTOBJ
			dto.setLclLogFunctionType("CRTOBJ");
			//switchBLK 1000191 BLK ACT
			// DEBUG genFunctionCall 1000192 ACT LCL.Log User Point = CON.Load detail screen
			dto.setLclLogUserPoint("Load detail screen");
			//switchBLK 1000195 BLK ACT
			// DEBUG genFunctionCall 1000196 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.from DBF record,CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "from DBF record"));
			//switchBLK 1000201 BLK ACT
			// DEBUG genFunctionCall 1000202 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Key Screen Request (Generated:466)
	 */
    private StepResult usrProcessKeyScreenRequest(DspDoctorDetailState dto) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			//switchSUB 466 SUB    
			//switchBLK 1000090 BLK ACT
			// DEBUG genFunctionCall 1000091 ACT LCL.Doctor Code = CON.*BLANK
			dto.setLclDoctorCode("");
			//switchBLK 1000212 BLK TXT
			// 
			//switchBLK 1000213 BLK ACT
			// DEBUG genFunctionCall 1000214 ACT LCL.Log Function Type = CON.CRTOBJ
			dto.setLclLogFunctionType("CRTOBJ");
			//switchBLK 1000217 BLK ACT
			// DEBUG genFunctionCall 1000218 ACT LCL.Log User Point = CON.Process key screen
			dto.setLclLogUserPoint("Process key screen");
			//switchBLK 1000221 BLK ACT
			// DEBUG genFunctionCall 1000222 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.request,CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "request"));
			//switchBLK 1000227 BLK ACT
			// DEBUG genFunctionCall 1000228 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Detail Screen Function Fields (Generated:425)
	 */
    private StepResult usrDetailScreenFunctionFields(DspDoctorDetailState dto) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			RtvForSupervisorDTO rtvForSupervisorDTO;
			//switchSUB 425 SUB    
			//switchBLK 1000098 BLK ACT
			// DEBUG genFunctionCall 1000099 ACT RTV For Supervisor - Doctor  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvForSupervisorDTO = new RtvForSupervisorDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvForSupervisorDTO.setDoctorCode(dto.getDoctorCode());
			// DEBUG genFunctionCall Service call
			rtvForSupervisorService.execute(rtvForSupervisorDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setSupervisingDoctorName(rtvForSupervisorDTO.getDoctorName());
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000238 BLK TXT
			// 
			//switchBLK 1000239 BLK ACT
			// DEBUG genFunctionCall 1000240 ACT LCL.Log Function Type = CON.CRTOBJ
			dto.setLclLogFunctionType("CRTOBJ");
			//switchBLK 1000243 BLK ACT
			// DEBUG genFunctionCall 1000244 ACT LCL.Log User Point = CON.Detail screen function
			dto.setLclLogUserPoint("Detail screen function");
			//switchBLK 1000247 BLK ACT
			// DEBUG genFunctionCall 1000248 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.fields,CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "fields"));
			//switchBLK 1000253 BLK ACT
			// DEBUG genFunctionCall 1000254 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Detail Screen (Generated:199)
	 */
    private StepResult usrValidateDetailScreen(DspDoctorDetailState dto) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			//switchSUB 199 SUB    
			//switchBLK 1000008 BLK CAS
			//switchSUB 1000008 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				//switchBLK 1000026 BLK ACT
				// DEBUG genFunctionCall 1000027 ACT PAR.*Return code = CND.E
				dto.set_SysReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000011 BLK ACT
				// DEBUG genFunctionCall 1000012 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
			//switchBLK 1000268 BLK TXT
			// 
			//switchBLK 1000269 BLK ACT
			// DEBUG genFunctionCall 1000270 ACT LCL.Log Function Type = CON.CRTOBJ
			dto.setLclLogFunctionType("CRTOBJ");
			//switchBLK 1000273 BLK ACT
			// DEBUG genFunctionCall 1000274 ACT LCL.Log User Point = CON.Validate detail screen
			dto.setLclLogUserPoint("Validate detail screen");
			//switchBLK 1000283 BLK ACT
			// DEBUG genFunctionCall 1000284 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Perform Confirmed Action (Generated:455)
	 */
    private StepResult usrPerformConfirmedAction(DspDoctorDetailState dto) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			//switchSUB 455 SUB    
			//switchBLK 1000054 BLK CAS
			//switchSUB 1000054 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				//switchBLK 1000057 BLK ACT
				// DEBUG genFunctionCall 1000058 ACT PAR.*Return code = CND.E
				dto.set_SysReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000061 BLK ACT
				// DEBUG genFunctionCall 1000062 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}//switchSUB 1000064 SUB    
			 else {
				// *OTHERWISE
				//switchBLK 1000035 BLK ACT
				// DEBUG genFunctionCall 1000036 ACT PAR.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000041 BLK ACT
				// DEBUG genFunctionCall 1000042 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
			//switchBLK 1000290 BLK TXT
			// 
			//switchBLK 1000291 BLK ACT
			// DEBUG genFunctionCall 1000292 ACT LCL.Log Function Type = CON.CRTOBJ
			dto.setLclLogFunctionType("CRTOBJ");
			//switchBLK 1000295 BLK ACT
			// DEBUG genFunctionCall 1000296 ACT LCL.Log User Point = CON.Perform confirmed action
			dto.setLclLogUserPoint("Perform confirmed action");
			//switchBLK 1000305 BLK ACT
			// DEBUG genFunctionCall 1000306 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Generated:389)
	 */
    private StepResult usrProcessCommandKeys(DspDoctorDetailState dto) {
        StepResult result = NO_ACTION;

        try {
        	CreateLogFileDTO createLogFileDTO;
			//switchSUB 389 SUB    
			//switchBLK 1000044 BLK CAS
			//switchSUB 1000044 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				//switchBLK 1000047 BLK ACT
				// DEBUG genFunctionCall 1000048 ACT PAR.*Return code = CND.E
				dto.set_SysReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000051 BLK ACT
				// DEBUG genFunctionCall 1000052 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
			//switchBLK 1000312 BLK TXT
			// 
			//switchBLK 1000313 BLK ACT
			// DEBUG genFunctionCall 1000314 ACT LCL.Log Function Type = CON.CRTOBJ
			dto.setLclLogFunctionType("CRTOBJ");
			//switchBLK 1000317 BLK ACT
			// DEBUG genFunctionCall 1000318 ACT LCL.Log User Point = CON.Process command keys
			dto.setLclLogUserPoint("Process command keys");
			//switchBLK 1000327 BLK ACT
			// DEBUG genFunctionCall 1000328 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Generated:30)
	 */
    private StepResult usrExitProgramProcessing(DspDoctorDetailState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 30 SUB    
			//switchBLK 1000014 BLK CAS
			//switchSUB 1000014 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// KEY.*CMD key is *Exit
				//switchBLK 1000020 BLK ACT
				// DEBUG genFunctionCall 1000021 ACT PAR.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000017 BLK ACT
				// DEBUG genFunctionCall 1000018 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    } 
}
