package com.hospital.file.doctor.dspdoctordetail;


/**
 * State for file 'Doctor' (TSAFREP) and function 'DSP Doctor Detail' (TSAUD1R).
 */
public class DspDoctorDetailState extends DspDoctorDetailDTO {
	private static final long serialVersionUID = -321241524623160676L;


	// Local fields
	private String lclDoctorCode = "";
	private String lclDoctorNotes = "";
	private String lclLogFunctionType = "";
	private String lclLogUserPoint = "";

	private boolean conductKeyScreenConversation = true;
	private boolean conductDetailScreenConversation = true;
 
	public DspDoctorDetailState() {

    }
public void setLclDoctorCode(String doctorCode) {
		this.lclDoctorCode = doctorCode;
    }
 
	public String getLclDoctorCode() {
        return lclDoctorCode;
    }
public void setLclDoctorNotes(String doctorNotes) {
		this.lclDoctorNotes = doctorNotes;
    }
 
	public String getLclDoctorNotes() {
        return lclDoctorNotes;
    }
public void setLclLogFunctionType(String logFunctionType) {
		this.lclLogFunctionType = logFunctionType;
    }
 
	public String getLclLogFunctionType() {
        return lclLogFunctionType;
    }
public void setLclLogUserPoint(String logUserPoint) {
		this.lclLogUserPoint = logUserPoint;
    }
 
	public String getLclLogUserPoint() {
        return lclLogUserPoint;
    }

    public boolean getConductKeyScreenConversation() {
    	return conductKeyScreenConversation;
    }

    public void setConductKeyScreenConversation(boolean conductKeyScreenConversation) {
    	this.conductKeyScreenConversation = conductKeyScreenConversation;
    }

    public boolean getConductDetailScreenConversation() {
    	return conductDetailScreenConversation;
    }

    public void setConductDetailScreenConversation(boolean conductDetailScreenConversation) {
        this.conductDetailScreenConversation = conductDetailScreenConversation;
    }
}
