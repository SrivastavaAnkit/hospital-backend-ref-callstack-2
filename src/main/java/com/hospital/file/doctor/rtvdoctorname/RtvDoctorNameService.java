package com.hospital.file.doctor.rtvdoctorname;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.doctor.Doctor;
import com.hospital.file.doctor.DoctorId;
import com.hospital.file.doctor.DoctorRepository;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvDoctorNameService extends AbstractService<RtvDoctorNameService, RtvDoctorNameDTO>
{
    private final Step execute = define("execute", RtvDoctorNameDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private DoctorRepository doctorRepository;
	
    @Autowired
    public RtvDoctorNameService()
    {
        super(RtvDoctorNameService.class, RtvDoctorNameDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvDoctorNameDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvDoctorNameDTO dto, RtvDoctorNameDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Doctor> doctorList = doctorRepository.findAllByIdDoctorCode(dto.getDoctorCode());
		if (!doctorList.isEmpty()) {
			for (Doctor doctor : doctorList) {
				processDataRecord(dto, doctor);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvDoctorNameDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000017 BLK ACT
		//functionCall 1000018 ACT LCL.Doctor Name = CON.*BLANK
		dto.setLclDoctorName("");
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvDoctorNameDTO dto, Doctor doctor) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		//switchSUB 41 SUB    
		//switchBLK 1000044 BLK ACT
		//functionCall 1000045 ACT LCL.Doctor Name = CON.*BLANK
		dto.setLclDoctorName("");
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvDoctorNameDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		//switchSUB 52 SUB    
		//switchBLK 1000038 BLK ACT
		//functionCall 1000039 ACT PAR.Doctor Name = CON.*BLANK
		dto.setDoctorName("");
		//switchBLK 1000032 BLK ACT
		//functionCall 1000033 ACT LCL.Doctor Name = CON.*BLANK
		dto.setLclDoctorName("");
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvDoctorNameDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		//switchSUB 61 SUB    
		//switchBLK 1000051 BLK ACT
		//functionCall 1000052 ACT PAR.Doctor Name = LCL.Doctor Name
		dto.setDoctorName(dto.getLclDoctorName());
        return NO_ACTION;
    }
}
