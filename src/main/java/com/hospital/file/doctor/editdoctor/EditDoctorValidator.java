package com.hospital.file.doctor.editdoctor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.hospital.common.exception.ServiceException;

import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorService;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;


/**
 * Spring Validator for file 'Doctor' (TSAFREP) and function 'Edit Doctor' (TSAGEFR).
 *
 * @author X2EGenerator
 */
@Component
public class EditDoctorValidator implements Validator {
	
	
	@Autowired
	private RtvForSupervisorService rtvForSupervisorService;
	

	@Override
	public boolean supports(Class<?> clazz) {
		return EditDoctorDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors e) {
		if (!(object instanceof EditDoctorDTO)) {
			e.reject("Not a valid EditDoctorDTO");
		} else {
			EditDoctorDTO dto = (EditDoctorDTO)object;
			EditDoctorGDO gdo = dto.getGdo();
			EditDoctorParams params = new EditDoctorParams();
			BeanUtils.copyProperties(dto, params);
		}
	}
}
