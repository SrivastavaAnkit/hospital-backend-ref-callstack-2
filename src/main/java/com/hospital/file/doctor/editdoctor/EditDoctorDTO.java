package com.hospital.file.doctor.editdoctor;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.hospital.common.state.BaseDTO;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.file.medication.editmedication.EditMedicationGDO;
import com.hospital.model.ReturnCodeEnum;


/**
 * Dto for file 'Doctor' (TSAFREP) and function 'Edit Doctor' (TSAGEFR).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 *
 */

public class EditDoctorDTO extends BaseDTO {
	private static final long serialVersionUID = 1903882887370854365L;
	private long version = 0;
	// TODO: Added Two Fields pageDto and mySelection with getter setter
    private RestResponsePage<EditDoctorGDO> pageDto;
    private List<EditDoctorGDO> mySelections;
    private int page = 0;
    private int size = 10;
    private String sortData = "";
    private boolean confirm = false;
    
	private String doctorCode = "";
	private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");


	private EditDoctorGDO gdo;

    public EditDoctorDTO() {

    }

	public EditDoctorDTO(long version) {
		this.version = version;
	}

	public void setVersion(long version) {
		this.version = version;
    }

    public long getVersion() {
    	return version;
    }

    public void setPageDto(RestResponsePage<EditDoctorGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<EditDoctorGDO> getPageDto() {
		return pageDto;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

    public boolean isConfirm() {
		return confirm;
	}

	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
    }

    public String getDoctorCode() {
    	return doctorCode;
    }

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
    }

    public ReturnCodeEnum getReturnCode() {
    	return returnCode;
    }

	public void setGdo(EditDoctorGDO gdo) {
		this.gdo = gdo;
	}

	public EditDoctorGDO getGdo() {
		return gdo;
	}

	public List<EditDoctorGDO> getMySelections() {
		return mySelections;
	}

	public void setMySelections(List<EditDoctorGDO> mySelections) {
		this.mySelections = mySelections;
	}
	
	

}