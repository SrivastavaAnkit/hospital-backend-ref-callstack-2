package com.hospital.file.doctor.editdoctor;

import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.UsrReturnCodeEnum;

/**
 * State for file 'Doctor' (TSAFREP) and function 'Edit Doctor' (TSAGEFR).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 *
 */

public class EditDoctorState extends EditDoctorDTO {
	private static final long serialVersionUID = 4476747816530366367L;

	// Local fields
	private UsrReturnCodeEnum lclUsrReturnCode = null;

	// System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;


    public EditDoctorState() {

    }

	public void setLclUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
    	this.lclUsrReturnCode = usrReturnCode;
    }

    public UsrReturnCodeEnum getLclUsrReturnCode() {
    	return lclUsrReturnCode;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }
    }