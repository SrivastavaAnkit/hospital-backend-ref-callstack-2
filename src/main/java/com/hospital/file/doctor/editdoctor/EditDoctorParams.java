package com.hospital.file.doctor.editdoctor;

import java.io.Serializable;


/**
 * Params for resource: EditDoctor (TSAGEFR).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 */

// TODO: Add Serializable interface
public class EditDoctorParams implements Serializable {
	private static final long serialVersionUID = -2062955603332675373L;
}