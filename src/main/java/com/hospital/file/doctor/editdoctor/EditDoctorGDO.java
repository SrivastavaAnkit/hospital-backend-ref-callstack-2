package com.hospital.file.doctor.editdoctor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.file.doctor.Doctor;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;
import com.hospital.model.SpecialityLevelEnum;


/**
 * Gdo for file 'Doctor' (TSAFREP) and function 'Edit Doctor' (TSAGEFR).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 *
 */
public class EditDoctorGDO implements Serializable {
	private static final long serialVersionUID = -4412104641769487083L;

	private long version = 0;
    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private String doctorCode = "";
	private String doctorName = "";
	private long doctorContactNumber = 0L;
	private SpecialityLevelEnum specialityLevel = null;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;
	private String hospitalCode = "";
	private String supervisingDoctorDoctor = "";
	private String supervisingDoctorName = "";

	public EditDoctorGDO() {

	}

	// TODO : Only Add limited field which are required for Grid
   	public EditDoctorGDO(/*long version, */String doctorCode, String doctorName, long doctorContactNumber, SpecialityLevelEnum specialityLevel, /*String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime, */String hospitalCode, String supervisingDoctorDoctor) {
//		this.version = version;
		this.doctorCode = doctorCode;
		this.doctorName = doctorName;
		this.doctorContactNumber = doctorContactNumber;
		this.specialityLevel = specialityLevel;
//		this.addedUser = addedUser;
//		this.addedDate = addedDate;
//		this.addedTime = addedTime;
//		this.changedUser = changedUser;
//		this.changedDate = changedDate;
//		this.changedTime = changedTime;
		this.hospitalCode = hospitalCode;
		this.supervisingDoctorDoctor = supervisingDoctorDoctor;
	}


	public void setVersion(long version) {
		this.version = version;
	}

    public long getVersion() {
		return version;
    }

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setDoctorCode(String doctorCode) {
    	this.doctorCode = doctorCode;
    }

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorName(String doctorName) {
    	this.doctorName = doctorName;
    }

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorContactNumber(long doctorContactNumber) {
    	this.doctorContactNumber = doctorContactNumber;
    }

	public long getDoctorContactNumber() {
		return doctorContactNumber;
	}

	public void setSpecialityLevel(SpecialityLevelEnum specialityLevel) {
    	this.specialityLevel = specialityLevel;
    }

	public SpecialityLevelEnum getSpecialityLevel() {
		return specialityLevel;
	}

	public void setAddedUser(String addedUser) {
    	this.addedUser = addedUser;
    }

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
    	this.addedDate = addedDate;
    }

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
    	this.addedTime = addedTime;
    }

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
    	this.changedUser = changedUser;
    }

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
    	this.changedDate = changedDate;
    }

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
    	this.changedTime = changedTime;
    }

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void setHospitalCode(String hospitalCode) {
    	this.hospitalCode = hospitalCode;
    }

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setSupervisingDoctorDoctor(String supervisingDoctorDoctor) {
    	this.supervisingDoctorDoctor = supervisingDoctorDoctor;
    }

	public String getSupervisingDoctorDoctor() {
		return supervisingDoctorDoctor;
	}

	public void setSupervisingDoctorName(String supervisingDoctorName) {
    	this.supervisingDoctorName = supervisingDoctorName;
    }

	public String getSupervisingDoctorName() {
		return supervisingDoctorName;
	}


    /**
     * Copies the fields of the Entity bean into the GDO bean.
     *
     * @param doctor Doctor Entity bean
     */
    public void setDtoFields(Doctor doctor) {
this.version = doctor.getVersion();
  BeanUtils.copyProperties(doctor, this);
    }

    /**
     * Copies the fields of the GDO bean into the Entity bean.
     *
     * @param doctor Doctor Entity bean
     */
    public void setEntityFields(Doctor doctor) {
		doctor.setVersion(this.version);
      BeanUtils.copyProperties(this, doctor);
    }

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}