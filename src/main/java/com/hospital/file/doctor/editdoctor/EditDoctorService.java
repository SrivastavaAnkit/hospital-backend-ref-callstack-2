package com.hospital.file.doctor.editdoctor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.exception.ServiceException;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.file.doctor.DoctorId;
import com.hospital.file.doctor.DoctorRepository;
import com.hospital.file.doctor.changedoctor.ChangeDoctorDTO;
import com.hospital.file.doctor.changedoctor.ChangeDoctorService;
import com.hospital.file.doctor.createdoctor.CreateDoctorDTO;
import com.hospital.file.doctor.createdoctor.CreateDoctorService;
import com.hospital.file.doctor.deletedoctor.DeleteDoctorDTO;
import com.hospital.file.doctor.deletedoctor.DeleteDoctorService;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorDTO;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorService;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailService;
import com.hospital.file.medication.editmedication.EditMedicationGDO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ProgramModeEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.support.JobContext;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;



/**
 * EDTFIL controller for 'Edit Doctor' (TSAGEFR) of file 'Doctor' (TSAFREP)
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 */
@Service
public class EditDoctorService extends AbstractService<EditDoctorService, EditDoctorState> {
    
	@Autowired
	private JobContext job;

    @Autowired
    private DoctorRepository doctorRepository;

	@Autowired
	private RtvHospitalDetailService rtvHospitalDetailService;
	
	@Autowired
	private ChangeDoctorService changeDoctorService;
	
	@Autowired
	private CreateDoctorService createDoctorService;
	
	@Autowired
	private DeleteDoctorService deleteDoctorService;
	
	@Autowired
	private RtvForSupervisorService rtvForSupervisorService;
	

    @Autowired
    private EditDoctorService editDoctorService;

    @Autowired
    private MessageSource messageSource;

//    @Autowired
//    private EditDoctorValidator editDoctorValidator;


    
	public static final String SCREEN_CTL = "editDoctor";
    public static final String SCREEN_RCD = "EditDoctor.rcd";
    public static final String SCREEN_CFM = "EditDoctor.confirm";

    private final Step execute = define("execute", EditDoctorParams.class, this::execute);
    private final Step response = define("ctlScreen", EditDoctorState.class, this::processResponse);
	private final Step confirmScreenResponse = define("cfmscreen", EditDoctorDTO.class, this::processConfirmScreenResponse);
	//private final Step serviceCreateDoctor = define("serviceCreateDoctor",CreateDoctorParams.class, this::processServiceCreateDoctor);
	//private final Step serviceDeleteDoctor = define("serviceDeleteDoctor",DeleteDoctorParams.class, this::processServiceDeleteDoctor);
	//private final Step serviceChangeDoctor = define("serviceChangeDoctor",ChangeDoctorParams.class, this::processServiceChangeDoctor);
	//private final Step serviceRtvForSupervisor = define("serviceRtvForSupervisor",RtvForSupervisorParams.class, this::processServiceRtvForSupervisor);
	//private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
	//private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
	
    
    @Autowired
    public EditDoctorService() {
        super(EditDoctorService.class, EditDoctorState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * EDTFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(EditDoctorState state, EditDoctorParams params) {
		StepResult result = NO_ACTION;

		BeanUtils.copyProperties(params, state);
		usrInitializeProgram(state);

		result = mainLoop(state);

		return result;
	}

	/**
	 * SCREEN_KEY initial processing loop method.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult mainLoop(EditDoctorState state) {
		StepResult result = NO_ACTION;

		result = loadFirstSubfilePage(state);

		state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
		result = conductScreenConversation(state);

		return result;
	}

	/**
	 * SCREEN  initial processing loop method.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult loadFirstSubfilePage(EditDoctorState state) {
		StepResult result = NO_ACTION;

		result = usrInitializeSubfileHeader(state);

		dbfReadFirstDataRecord(state);
		if (state.getPageDto() != null && state.getPageDto().getSize() > 0) {
			state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
			result = loadNextSubfilePage(state);
		} else {
			state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
		}

		return result;
	}

	/**
	 * Iterate on data loaded to do stuff on each record.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult loadNextSubfilePage(EditDoctorState state) {
		StepResult result = NO_ACTION;

		for (EditDoctorGDO gdo : state.getPageDto().getContent()) {
			if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
				if (gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
//             TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
					result = usrInitializeSubfileRecordExistingRecord(state, gdo);
				}
			} else {
//             TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
				result = usrInitializeSubfileRecordNewRecord(state, gdo);
			}
			validateSubfileRecord(state, gdo);
			result = dbfUpdateDataRecord(state, gdo);
		}

		return result;
	}

	/**
	 * SCREEN  validate subfile record.
	 *
	 * @param gdo - subfile record class.
	 * @return result
	 */
	private StepResult validateSubfileRecord(EditDoctorState state, EditDoctorGDO gdo) {
		StepResult result = NO_ACTION;

		return result;
	}

	/**
	 * SCREEN  initial processing loop method.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult conductScreenConversation(EditDoctorState state) {
		StepResult result = NO_ACTION;

		if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
			EditDoctorDTO dto = new EditDoctorDTO();
			BeanUtils.copyProperties(state, dto);
			result = callScreen(SCREEN_CTL, dto).thenCall(response);
		}

		return result;
	}

	/**
	 * SCREEN_KEY returned response processing method.
	 *
	 * @param state      - Service state class.
	 * @param fromScreen - returned screen model.
	 * @return result
	 */
	private StepResult processResponse(EditDoctorState state, EditDoctorDTO fromScreen) {
		StepResult result = NO_ACTION;

		// update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
		BeanUtils.copyProperties(fromScreen, state);
		// TODO : Add changes for change mode and Delete condition
		if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
			result = closedown(state);
		} else if (CmdKeyEnum.isChangeMode(state.get_SysCmdKey())) {
			if(state.get_SysProgramMode()==ProgramModeEnum._STA_CHANGE){
				state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
				List<EditDoctorGDO> content=new ArrayList<EditDoctorGDO>();
				for(int idx=0;idx<10;idx++){
					content.add(new EditDoctorGDO());
				}
				RestResponsePage<EditDoctorGDO> pageDto = new RestResponsePage<>(content);
		        state.setPageDto(pageDto);
			return conductScreenConversation(state);
			}
			else{
				state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
				return mainLoop(state);
			}
		} else if (ProgramModeEnum._STA_DELETE.equals(state.get_SysProgramMode())) {
			result = processConfirmScreenResponse(state, fromScreen);
		} else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
			state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
		} else if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
			result = processHelpRequest(state); //synon built-in function
		} else if (CmdKeyEnum.isNextPage(state.get_SysCmdKey())) {
			dbfReadNextPageRecord(state);
			result = loadNextSubfilePage(state);
		} else {
			result = processScreen(state);
		}

		return result;
	}

	private StepResult processHelpRequest(EditDoctorState state) {
		return NO_ACTION;
	}

	/**
	 * SCREEN process screen.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult processScreen(EditDoctorState state) {
		StepResult result = NO_ACTION;

		result = validateSubfileControl(state);
		if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
			return result;
		} else {
			for (EditDoctorGDO gdo : state.getPageDto().getContent()) {
				//TODO: Commented below line code
				//if (gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
					result = usrValidateSubfileRecordFields(state, gdo);
					if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
						return result;
					}
					result = checkRelations(gdo);
					if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
						return result;
					}
					result = usrSubfileRecordFunctionFields(state, gdo);
					result = usrValidateSubfileRecordRelations(state, gdo);
					if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
						return result;
					}
					result = dbfUpdateDataRecord(state, gdo);
				//}
			}
		}

		EditDoctorDTO dto = new EditDoctorDTO();
		BeanUtils.copyProperties(state, dto);
		// TODO : Commented below line of code and added mainloop function
		//result = callScreen(SCREEN_CFM, dto).thenCall(confirmScreenResponse);
		result = mainLoop(state);
		return result;
	}

	/**
	 * SCREEN  check the relations of GDO.
	 *
	 * @param gdo -  Service subfile record class.
	 * @return result
	 */
	private StepResult checkRelations(EditDoctorGDO gdo) {
		StepResult result = NO_ACTION;

		return result;
	}

	/**
	 * SCREEN  validate subfile control.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult validateSubfileControl(EditDoctorState state) {
		StepResult result = NO_ACTION;

		state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
		validateSubfileControlField(state);
		if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
			return result;
		}
		usrSubfileControlFunctionFields(state);
		result = usrValidateSubfileControl(state);

		return result;
	}

	/**
	 * SCREEN  validate subfile control field.
	 *
	 * @param state -  Service state class.
	 * @return result
	 */
	private StepResult validateSubfileControlField(EditDoctorState state) {
		StepResult result = NO_ACTION;

		return result;
	}

	/**
	 * SCREEN_CONFIRM returned response processing method.
	 *
	 * @param state - Service state class.
	 * @param model - returned screen model.
	 * @return result
	 */
	private StepResult processConfirmScreenResponse(EditDoctorState state, EditDoctorDTO model) {
		StepResult result = NO_ACTION;

		if (state.isConfirm()) {
			for (EditDoctorGDO gdo : state.getPageDto().getContent()) {
				//if (gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
					if ("DELETE".equals(gdo.get_SysSelected().toUpperCase())) {
						result = dbfDeleteDataRecord(state, gdo);
					} else {
						if ("CHANGE".equals(gdo.get_SysSelected())) {
							result = dbfUpdateDataRecord(state, gdo);
						} else {
							result = dbfCreateDataRecord(state, gdo);
						}
					}
				//}
				result = usrExtraProcessingAfterDBFUpdate(state);
				state.setConfirm(false);
			}
		}

		if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
			return result;
		} else {
			if (state.get_SysReloadSubfile() == ReloadSubfileEnum._STA_YES) {
				// request subfile reload if necessary
			}
			if (CmdKeyEnum.isChangeMode(state.get_SysCmdKey())) {
				if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
					state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
				} else {
					state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
				}
			} else {
				result = usrProcessCommandKeys(state);
				if (result != StepResult.NO_ACTION) {
					return result;
				}
			}
		}

		result = mainLoop(state);

		return result;
	}

	/**
	 * Terminate this program
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult closedown(EditDoctorState state) {
		StepResult result = NO_ACTION;

		state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		result = usrExitProgramProcessing(state);

		return result;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(EditDoctorState state) {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadNextPageRecord(EditDoctorState state) {
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    private void dbfReadDataRecord(EditDoctorState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = new PageRequest(state.getPage(), state.getSize());
        }
        else {
            pageable = new PageRequest(state.getPage(), state.getSize(), new Sort(sortOrders));
        }

        RestResponsePage<EditDoctorGDO> pageDto = doctorRepository.editDoctor(state.getDoctorCode(), pageable);
        state.setPageDto(pageDto);
    }


    /**
     * Create record data
     * @param dto - Service state class.
     * @return
     */
    private StepResult dbfCreateDataRecord(EditDoctorState dto, EditDoctorGDO gdo) {
		StepResult result = NO_ACTION;


//        val dbfOBJ: X2EFunction = getFunctionForType(x2EFile, "CRTOBJ")
//        val dtoOBJ = dbfOBJ.asServiceDtoClassName()
//        val dtoVAR = dbfOBJ.asServiceDtoVariableName()
        try {
        	/**
			 * USER: Create Object (Generated:1387)
			 */
			CreateDoctorDTO createDoctorDTO;
			//switchBLK 1387 BLK ACT
			// DEBUG genFunctionCall 1388 ACT Create Doctor - Doctor  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createDoctorDTO = new CreateDoctorDTO();
			// DEBUG genFunctionCall Parameters IN
			createDoctorDTO.setDoctorCode(gdo.getDoctorCode());
			createDoctorDTO.setHospitalCode(gdo.getHospitalCode());
			createDoctorDTO.setSupervisingDoctorDoctor(gdo.getSupervisingDoctorDoctor());
			createDoctorDTO.setDoctorName(gdo.getDoctorName());
			createDoctorDTO.setDoctorContactNumber(gdo.getDoctorContactNumber());
			createDoctorDTO.setSpecialityLevel(gdo.getSpecialityLevel());
			// DEBUG genFunctionCall Service call
			createDoctorService.execute(createDoctorDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setLclUsrReturnCode(createDoctorDTO.getUsrReturnCode());
			// DEBUG genFunctionCall Parameters DONE
	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
	}


    /**
     * Delete record data
     * @param dto - Service state class.
     * @return
     */
    private StepResult dbfDeleteDataRecord(EditDoctorState dto, EditDoctorGDO gdo) {
        StepResult result = NO_ACTION;


//        val functions = x2EFile.functions.filter { it.functionType == "DLTOBJ" }

//        val dbfOBJ: X2EFunction = if (functions.isNotEmpty()) getFunctionForType(x2EFile, "DLTOBJ") else function
//        val dtoOBJ = dbfOBJ.asServiceDtoClassName()
//        val dtoVAR = dbfOBJ.asServiceDtoVariableName()
        try {
        	/**
			 * USER: Delete Object (Generated:1389)
			 */
			DeleteDoctorDTO deleteDoctorDTO;
			//switchBLK 1389 BLK ACT
			// DEBUG genFunctionCall 1390 ACT Delete Doctor - Doctor  *
			// DEBUG genFunctionCall ServiceDtoVariable
			deleteDoctorDTO = new DeleteDoctorDTO();
			// DEBUG genFunctionCall Parameters IN
			deleteDoctorDTO.setDoctorCode(gdo.getDoctorCode());
			// DEBUG genFunctionCall Service call
			deleteDoctorService.execute(deleteDoctorDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE
	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
	}


    /**
     * Update record data
     * @param dto - Service state class.
     * @return
     */
    private StepResult dbfUpdateDataRecord(EditDoctorState dto, EditDoctorGDO gdo) {
        StepResult result = NO_ACTION;


//        val dbfOBJ: X2EFunction = getFunctionForType(x2EFile, "CHGOBJ")
//        val dtoOBJ = dbfOBJ.asServiceDtoClassName()
//        val dtoVAR = dbfOBJ.asServiceDtoVariableName()
        try {
			DoctorId doctorId = new DoctorId(gdo.getDoctorCode());
			if (doctorRepository.existsById(doctorId)) {
			
        		/**
				 * USER: Change Object (Generated:1391)
				 */
				ChangeDoctorDTO changeDoctorDTO;
				//switchBLK 1391 BLK ACT
				// DEBUG genFunctionCall 1392 ACT Change Doctor - Doctor  *
				// DEBUG genFunctionCall ServiceDtoVariable
				changeDoctorDTO = new ChangeDoctorDTO();
				// DEBUG genFunctionCall Parameters IN
				changeDoctorDTO.setDoctorCode(gdo.getDoctorCode());
				changeDoctorDTO.setHospitalCode(gdo.getHospitalCode());
				changeDoctorDTO.setSupervisingDoctorDoctor(gdo.getSupervisingDoctorDoctor());
				changeDoctorDTO.setDoctorName(gdo.getDoctorName());
				changeDoctorDTO.setDoctorContactNumber(gdo.getDoctorContactNumber());
				changeDoctorDTO.setSpecialityLevel(gdo.getSpecialityLevel());
				// DEBUG genFunctionCall Service call
				changeDoctorService.execute(changeDoctorDTO);
				// DEBUG genFunctionCall Parameters OUT
				dto.setLclUsrReturnCode(changeDoctorDTO.getUsrReturnCode());
				// DEBUG genFunctionCall Parameters DONE
		
            }
            else {
                throw new ServiceException("diagnosis.nf");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
	}


    
	/**
	 * USER: Initialize Program (Generated:15)
	 */
    private StepResult usrInitializeProgram(EditDoctorState dto) {
        StepResult result = NO_ACTION;

        try {
        		//switchSUB 15 SUB    
				// Unprocessed SUB 15 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Header (Generated:1488)
	 */
    private StepResult usrInitializeSubfileHeader(EditDoctorState dto) {
        StepResult result = NO_ACTION;

        try {
        		//switchSUB 1488 SUB    
				// Unprocessed SUB 1488 -
        
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Record (New Record) (Generated:1027)
	 */
    private StepResult usrInitializeSubfileRecordNewRecord(EditDoctorState dto, EditDoctorGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        		//switchSUB 1027 SUB    
				// Unprocessed SUB 1027 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Record (Existing Record) (Generated:1023)
	 */
    private StepResult usrInitializeSubfileRecordExistingRecord(EditDoctorState dto, EditDoctorGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        		RtvForSupervisorDTO rtvForSupervisorDTO;
				//switchSUB 1023 SUB    
				//switchBLK 1000001 BLK ACT
				// DEBUG genFunctionCall 1000002 ACT RTV For Supervisor - Doctor  *
				// DEBUG genFunctionCall ServiceDtoVariable
				rtvForSupervisorDTO = new RtvForSupervisorDTO();
				// DEBUG genFunctionCall Parameters IN
				rtvForSupervisorDTO.setDoctorCode(gdo.getSupervisingDoctorDoctor());
				// DEBUG genFunctionCall Service call
				rtvForSupervisorService.execute(rtvForSupervisorDTO);
				// DEBUG genFunctionCall Parameters OUT
				gdo.setSupervisingDoctorName(rtvForSupervisorDTO.getDoctorName());
				// DEBUG genFunctionCall Parameters DONE

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:1463)
	 */
    private StepResult usrSubfileControlFunctionFields(EditDoctorState dto) {
        StepResult result = NO_ACTION;

        try {
        		//switchSUB 1463 SUB    
				// Unprocessed SUB 1463 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Subfile Control (Generated:37)
	 */
    private StepResult usrValidateSubfileControl(EditDoctorState dto) {
        StepResult result = NO_ACTION;

        try {
        		//switchSUB 37 SUB    
				//switchBLK 1000005 BLK CAS
				//switchSUB 1000005 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
					// CTL.*CMD key is *Cancel
					//switchBLK 1000017 BLK ACT
					// DEBUG genFunctionCall 1000018 ACT PAR.*Return code = CND.E
					dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
					//switchBLK 1000008 BLK ACT
					// DEBUG genFunctionCall 1000009 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Subfile Record Fields (Generated:1496)
	 */
    private StepResult usrValidateSubfileRecordFields(EditDoctorState dto, EditDoctorGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        		//switchSUB 1496 SUB    
				// Unprocessed SUB 1496 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:1449)
	 */
    private StepResult usrSubfileRecordFunctionFields(EditDoctorState dto, EditDoctorGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        		//switchSUB 1449 SUB    
				// Unprocessed SUB 1449 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Subfile Record Relations (Generated:1171)
	 */
    private StepResult usrValidateSubfileRecordRelations(EditDoctorState dto, EditDoctorGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        		//switchSUB 1171 SUB    
				//switchBLK 1000032 BLK CAS
				//switchSUB 1000032 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
					// CTL.*CMD key is *Cancel
					//switchBLK 1000035 BLK ACT
					// DEBUG genFunctionCall 1000036 ACT PAR.*Return code = CND.E
					dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
					//switchBLK 1000041 BLK ACT
					// DEBUG genFunctionCall 1000042 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Extra Processing After DBF Update (Generated:1442)
	 */
    private StepResult usrExtraProcessingAfterDBFUpdate(EditDoctorState dto) {
        StepResult result = NO_ACTION;

        try {
        		//switchSUB 1442 SUB    
				// Unprocessed SUB 1442 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Generated:1420)
	 */
    private StepResult usrProcessCommandKeys(EditDoctorState dto) {
        StepResult result = NO_ACTION;

        try {
        		//switchSUB 1420 SUB    
				// Unprocessed SUB 1420 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Generated:1262)
	 */
    private StepResult usrExitProgramProcessing(EditDoctorState dto) {
        StepResult result = NO_ACTION;
        
        try {
        		//switchSUB 1262 SUB    
				//switchBLK 1000011 BLK CAS
				//switchSUB 1000011 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
					// CTL.*CMD key is *Exit
					//switchBLK 1000026 BLK ACT
					// DEBUG genFunctionCall 1000027 ACT PAR.*Return code = CND.*Normal
					dto.setReturnCode(ReturnCodeEnum.fromCode(""));
					//switchBLK 1000014 BLK ACT
					// DEBUG genFunctionCall 1000015 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * CreateDoctorService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateDoctor(EditDoctorState state, CreateDoctorParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * DeleteDoctorService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceDeleteDoctor(EditDoctorState state, DeleteDoctorParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ChangeDoctorService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChangeDoctor(EditDoctorState state, ChangeDoctorParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvForSupervisorService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvForSupervisor(EditDoctorState state, RtvForSupervisorParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(EditDoctorState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(EditDoctorState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//


}
