package com.hospital.file.doctor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository interface for model: Doctor (TSAFREP).
 *
 * @author X2EGenerator
 */
@Repository
public interface DoctorRepository extends DoctorRepositoryCustom, JpaRepository<Doctor, DoctorId> {

	List<Doctor> findAllByHospitalCode(String hospitalCode);

	List<Doctor> findAllByIdDoctorCode(String doctorCode);
}
