package com.hospital.file.doctor.rtvdoctorperdiagnosis;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.doctor.Doctor;
import com.hospital.file.doctor.DoctorId;
import com.hospital.file.doctor.DoctorRepository;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvDoctorPerDiagnosisService extends AbstractService<RtvDoctorPerDiagnosisService, RtvDoctorPerDiagnosisDTO>
{
    private final Step execute = define("execute", RtvDoctorPerDiagnosisDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private DoctorRepository doctorRepository;
	
    @Autowired
    public RtvDoctorPerDiagnosisService()
    {
        super(RtvDoctorPerDiagnosisService.class, RtvDoctorPerDiagnosisDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvDoctorPerDiagnosisDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvDoctorPerDiagnosisDTO dto, RtvDoctorPerDiagnosisDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Doctor> doctorList = doctorRepository.findAllByIdDoctorCode(dto.getDoctorCode());
		if (!doctorList.isEmpty()) {
			for (Doctor doctor : doctorList) {
				processDataRecord(dto, doctor);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvDoctorPerDiagnosisDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Empty:48)
		 */
		
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvDoctorPerDiagnosisDTO dto, Doctor doctor) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Empty:41)
		 */
		
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvDoctorPerDiagnosisDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Empty:52)
		 */
		
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvDoctorPerDiagnosisDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Empty:61)
		 */
		
        return NO_ACTION;
    }
}
