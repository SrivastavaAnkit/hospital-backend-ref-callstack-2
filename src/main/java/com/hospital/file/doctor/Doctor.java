package com.hospital.file.doctor;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.hospital.file.hospital.Hospital;
import com.hospital.model.SpecialityLevelConverter;
import com.hospital.model.SpecialityLevelEnum;

@Entity
@Table(name="Doctor", schema="HospitalMgmt")
public class Doctor implements Serializable {
	private static final long serialVersionUID = -1L;

	@Version
	@Column(name="version")
	private long version;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns ({
		@JoinColumn(name="HospitalCode", referencedColumnName="HospitalCode", insertable=false, updatable=false)
	})
	private Hospital hospitalObj;

	@EmbeddedId
	private DoctorId id = new DoctorId();

	@Column(name = "DoctorContactNumber")
	private long doctorContactNumber = 0L;
	
	@Column(name = "DoctorName")
	private String doctorName = "";
	
	@Column(name = "HospitalCode")
	private String hospitalCode = "";
	
	@Column(name = "SpecialityLevel")
	@Convert(converter=SpecialityLevelConverter.class)
	private SpecialityLevelEnum specialityLevel;
	
	@Column(name = "SupervisingDoctorDoctor")
	private String supervisingDoctorDoctor = "";

	public DoctorId getId() {
		return id;
	}

	public String getDoctorCode() {
		return id.getDoctorCode();
	}

	public String getHospitalCode() {
		return hospitalCode;
	}
	
	public String getSupervisingDoctorDoctor() {
		return supervisingDoctorDoctor;
	}
	
	public String getDoctorName() {
		return doctorName;
	}
	
	public long getDoctorContactNumber() {
		return doctorContactNumber;
	}
	
	public SpecialityLevelEnum getSpecialityLevel() {
		return specialityLevel;
	}

	public String getHospitalName() {
		return hospitalObj.getHospitalName();
	}

	public long getVersion() {
		return version;
	}

	public void setDoctorCode(String doctorCode) {
		this.id.setDoctorCode(doctorCode);
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	
	public void setSupervisingDoctorDoctor(String supervisingDoctorDoctor) {
		this.supervisingDoctorDoctor = supervisingDoctorDoctor;
	}
	
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	
	public void setDoctorContactNumber(long doctorContactNumber) {
		this.doctorContactNumber = doctorContactNumber;
	}
	
	
	public void setSpecialityLevel(SpecialityLevelEnum specialityLevel) {
		this.specialityLevel = specialityLevel;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
