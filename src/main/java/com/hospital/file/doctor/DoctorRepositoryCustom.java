package com.hospital.file.doctor;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.hospital.model.CountryEnum;

import com.hospital.file.doctor.selectdoctor.SelectDoctorGDO;
import com.hospital.file.doctor.editdoctor.EditDoctorGDO;
import com.hospital.file.doctor.dspfdoctorsforhospital.DspfDoctorsForHospitalGDO;
import com.hospital.file.doctor.editdoctorhospital.EditDoctorHospitalGDO;

/**
 * Custom Spring Data JPA repository interface for model: Doctor (TSAFREP).
 *
 * @author X2EGenerator
 */
@Repository
public interface DoctorRepositoryCustom {

	/**
	 * 
	 * @param doctorCode Doctor Code
	 */
	void deleteDoctor(String doctorCode);

	/**
	 * 
	 * @param doctorCode Doctor Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of SelectDoctorGDO
	 */
	RestResponsePage<SelectDoctorGDO> selectDoctor(String doctorCode, Pageable pageable);

	/**
	 * 
	 * @param doctorCode Doctor Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of EditDoctorGDO
	 */
	RestResponsePage<EditDoctorGDO> editDoctor(String doctorCode, Pageable pageable);

	/**
	 * 
	 * @param doctorCode Doctor Code
	 * @return Doctor
	 */
	Doctor rtvForSupervisor(String doctorCode);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param country Country
	 * @param countryName Country Name
	 * @param telephoneNumber Telephone Number
	 * @param pageable a Pageable object used for pagination
	 * @return Page of DspfDoctorsForHospitalGDO
	 */
	RestResponsePage<DspfDoctorsForHospitalGDO> dspfDoctorsForHospital(String hospitalCode, CountryEnum country, String countryName, long telephoneNumber, Pageable pageable);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of Doctor
	 */
	RestResponsePage<Doctor> rtvDoctorsForPatient(String hospitalCode, Pageable pageable);

	/**
	 * 
	 * @param doctorCode Doctor Code
	 * @return Doctor
	 */
	Doctor rtvDoctorPerDiagnosis(String doctorCode);

	/**
	 * 
	 * @param doctorCode Doctor Code
	 * @return Doctor
	 */
	Doctor rtvDoctorName(String doctorCode);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param doctorCode Doctor Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of EditDoctorHospitalGDO
	 */
	RestResponsePage<EditDoctorHospitalGDO> editDoctorHospital(String hospitalCode, String doctorCode, Pageable pageable);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of Doctor
	 */
	RestResponsePage<Doctor> rtvHospitalExist(String hospitalCode, Pageable pageable);
}
