package com.hospital.file.doctor.editdoctorrcd;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.file.doctor.Doctor;
import com.hospital.model.UsrReturnCodeEnum;
import com.hospital.model.ReloadSubfileEnum;

/**
 * State for file 'Doctor' (TSAFREP) and function 'Edit Doctor RCD' (TSBOE1R).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
public class EditDoctorRcdState extends EditDoctorRcdDTO {
    private static final long serialVersionUID = 3707267750543574680L;


    // Local fields
    private UsrReturnCodeEnum lclUsrReturnCode = null;

    // System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysConductKeyScreenConversation = true;
    private boolean _sysConductDetailScreenConversation = true;
    private boolean _sysErrorFound = false;

    public EditDoctorRcdState() {
    }

    public EditDoctorRcdState(Doctor doctor) {
        setDtoFields(doctor);
    }

    public void setLclUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
        this.lclUsrReturnCode = usrReturnCode;
    }

    public UsrReturnCodeEnum getLclUsrReturnCode() {
        return lclUsrReturnCode;
    }

   public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
       _sysReloadSubfile = reloadSubfile;
   }

   public ReloadSubfileEnum get_SysReloadSubfileEnum() {
       return _sysReloadSubfile;
   }

    public boolean get_SysConductKeyScreenConversation() {
        return _sysConductKeyScreenConversation;
    }

    public void set_SysConductKeyScreenConversation(boolean conductKeyScreenConversation) {
        _sysConductKeyScreenConversation = conductKeyScreenConversation;
    }

    public boolean get_SysConductDetailScreenConversation() {
        return _sysConductDetailScreenConversation;
    }

    public void set_SysConductDetailScreenConversation(boolean conductDetailScreenConversation) {
        _sysConductDetailScreenConversation = conductDetailScreenConversation;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
