package com.hospital.file.doctor.editdoctorrcd;

import java.io.IOException;
import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;


import com.hospital.file.doctor.Doctor;
import com.hospital.file.doctor.DoctorId;
import com.hospital.file.doctor.DoctorRepository;
import com.hospital.file.doctor.changedoctor.ChangeDoctorService;
import com.hospital.file.doctor.changedoctor.ChangeDoctorDTO;
import com.hospital.file.doctor.createdoctor.CreateDoctorService;
import com.hospital.file.doctor.createdoctor.CreateDoctorDTO;
import com.hospital.file.doctor.deletedoctor.DeleteDoctorService;
import com.hospital.file.doctor.selectdoctor.SelectDoctorParams;
import com.hospital.file.doctor.deletedoctor.DeleteDoctorDTO;


import com.hospital.common.exception.ServiceException;

import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ProgramModeEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;
import com.hospital.model.UsrReturnCodeEnum;
import com.hospital.support.JobContext;
/**
 * Service controller for 'Edit Doctor RCD' (TSBOE1R) of file 'Doctor' (TSAFREP)
 *
 * @author X2EGenerator
 */
@Service
public class EditDoctorRcdService extends AbstractService<EditDoctorRcdService, EditDoctorRcdState>
{
    
    @Autowired
    private JobContext job;
    
    @Autowired
    private DoctorRepository doctorRepository;
    
    @Autowired
    private ChangeDoctorService changeDoctorService;
    
    @Autowired
    private CreateDoctorService createDoctorService;
    
    @Autowired
    private DeleteDoctorService deleteDoctorService;
    
        
    public static final String SCREEN_KEY = "editDoctorRcdEntryPanel";
    public static final String SCREEN_DTL = "editDoctorRcdPanel";
    public static final String SCREEN_CFM = "EditDoctorRcd.confirm";
    
    private final Step execute = define("execute", EditDoctorRcdParams.class, this::execute);
    private final Step keyScreenResponse = define("keyScreen", EditDoctorRcdDTO.class, this::processKeyScreenResponse);
    private final Step detailScreenResponse = define("dtlScreen", EditDoctorRcdDTO.class, this::processDetailScreenResponse);
    private final Step confirmScreenResponse = define("cfmScreen", EditDoctorRcdDTO.class, this::processConfirmScreenResponse);
    private final Step promptSelectDoctor = define("promptSelectDoctor",SelectDoctorParams.class, this::processPromptSelectDoctor);
    //private final Step serviceDeleteDoctor = define("serviceDeleteDoctor",DeleteDoctorParams.class, this::processServiceDeleteDoctor);
    //private final Step serviceCreateDoctor = define("serviceCreateDoctor",CreateDoctorParams.class, this::processServiceCreateDoctor);
    //private final Step serviceChangeDoctor = define("serviceChangeDoctor",ChangeDoctorParams.class, this::processServiceChangeDoctor);
    //private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
    //private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
    
        @Autowired
    public EditDoctorRcdService()
    {
        super(EditDoctorRcdService.class, EditDoctorRcdState.class);
    }
    
    @Override
    public Step getInitialStep()
    {
        return execute;
    }
    
    /**
     * EDTRCD controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(EditDoctorRcdState state, EditDoctorRcdParams params)
    {
        StepResult result = NO_ACTION;
    
        if(params != null) {
            BeanUtils.copyProperties(params, state);
        }
    
        state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
    
        result = conductKeyScreenConversation(state);
    
        return result;
    }
    
    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductKeyScreenConversation(EditDoctorRcdState state) 
    {
        StepResult result = NO_ACTION;
    
        result = displayKeyScreenConversation(state);
    
        return result;
    }
    
    /**
     * SCREEN_KEY display processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult displayKeyScreenConversation(EditDoctorRcdState state)
    {
        StepResult result = NO_ACTION;
    
        if (state.get_SysConductKeyScreenConversation()) {
            EditDoctorRcdDTO model = new EditDoctorRcdDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_KEY, model).thenCall(keyScreenResponse);
        }
    
        return result;
    }
    
    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processKeyScreenResponse(EditDoctorRcdState state, EditDoctorRcdDTO model)
    {
        StepResult result = NO_ACTION;
    
        BeanUtils.copyProperties(model, state);
    
        if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            result = displayKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            result = conductKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
           result = closedown(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
    
        }
        else if(CmdKeyEnum.isChangeMode(state.get_SysCmdKey())) {
            if(state.get_SysProgramMode() == ProgramModeEnum._STA_ADD) {
                state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            }
            else {
                state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
            }
            result = conductKeyScreenConversation(state);
        }
        else {
            checkKeyFields(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = displayKeyScreenConversation(state);
                return result;
            }
            dbfReadDataRecord(state);
    
            result = conductDetailScreenConversation(state);
        }
    
        return result;
    }
    
    /**
     * Check key fields set in SCREEN_KEY.
     * For now it's just if fields are null or strings are empty
     * @param state - Service state class.
     * @return
     */
    private void checkKeyFields(EditDoctorRcdState state) {
       if(state.getDoctorCode() == null || state.getDoctorCode().isEmpty())
       {
           state.set_SysErrorFound(true);
       }
    }
    
    /**
     * SCREEN_DTL display processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductDetailScreenConversation(EditDoctorRcdState state)
    {
        StepResult result = NO_ACTION;
    
        if (state.get_SysConductDetailScreenConversation()) {
            EditDoctorRcdDTO model = new EditDoctorRcdDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_DTL, model).thenCall(detailScreenResponse);
        }
    
        return result;
    }
    
    /**
     * SCREEN_DETAIL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processDetailScreenResponse(EditDoctorRcdState state, EditDoctorRcdDTO model)
    {
        StepResult result = NO_ACTION;
    
        BeanUtils.copyProperties(model, state);
    
        if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            result = conductDetailScreenConversation(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            result = conductDetailScreenConversation(state);
        }
        else if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            result = closedown(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
    
        }
        else if (CmdKeyEnum.isKeyScreen(state.get_SysCmdKey())) {
            result = conductKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isDelete(state.get_SysCmdKey())) {
            //delete action confirmed in client side
            dbfDeleteDataRecord(state);
            result = displayKeyScreenConversation(state);
        }
        else {
            checkFields(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = conductDetailScreenConversation(state);
                return result;
            }
            usrValidateDetailScreenFields(state);
            checkRelations(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = conductDetailScreenConversation(state);
                return result;
            }
            //TODO: make confirm screen
            result = processConfirmScreenResponse(state, null);//callScreen(SCREEN_CFM, state).thenCall(confirmScreenResponse);
        }
    
        return result;
    }
    /**
     * SCREEN_CONFIRM returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processConfirmScreenResponse(EditDoctorRcdState state, EditDoctorRcdDTO model)
    {
        StepResult result = NO_ACTION;
    
        if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
            dbfUpdateDataRecord(state);
        }
        else {
            dbfCreateDataRecord(state);
        }
    
        result = conductKeyScreenConversation(state);
    
        return result;
    }
    
    /**
     * Check fields set in SCREEN_DETAIL.
     * @param state - Service state class.
     * @return
     */
    private void checkFields(EditDoctorRcdState state) {
    
    }
    
    /**
     * Check relations set in SCREEN_DETAIL.
     * @param state - Service state class.
     * @return
     */
    private void checkRelations(EditDoctorRcdState state) {
    
    }
    
    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(EditDoctorRcdState state)
    {
        StepResult result = NO_ACTION;
    
        result = usrExitCommandProcessing(state);
    
        EditDoctorRcdParams params = new EditDoctorRcdParams();
        BeanUtils.copyProperties(state, params);
        result = StepResult.returnFromService(params);
    
        return result;
    }
        
    /**
     * ------------------------ Generated DBF methods ----------------------
     */
    
    private StepResult dbfReadDataRecord(EditDoctorRcdState dto)
    {
        StepResult result = NO_ACTION;
    
        DoctorId doctorId = new DoctorId(dto.getDoctorCode());
        Doctor doctor = doctorRepository.findById(doctorId).orElse(null);
    
        if (doctor == null) {
            dto.set_SysProgramMode(ProgramModeEnum._STA_ADD);
        }
        else {
            dto.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            BeanUtils.copyProperties(doctor, dto);
        }
        return result;
    }
    
    private StepResult dbfCreateDataRecord(EditDoctorRcdState dto)
    {
        StepResult result = NO_ACTION;
        try {
            /**
    		 * USER: Create Object (Generated:425)
    		 */
    		CreateDoctorDTO createDoctorDTO;
    		//switchBLK 425 BLK ACT
    		// DEBUG genFunctionCall 426 ACT Create Doctor - Doctor  *
    		// DEBUG genFunctionCall ServiceDtoVariable
    		createDoctorDTO = new CreateDoctorDTO();
    		// DEBUG genFunctionCall Parameters IN
    		createDoctorDTO.setDoctorCode(dto.getDoctorCode());
    		createDoctorDTO.setHospitalCode(dto.getHospitalCode());
    		createDoctorDTO.setSupervisingDoctorDoctor(dto.getSupervisingDoctorDoctor());
    		createDoctorDTO.setDoctorName(dto.getDoctorName());
    		createDoctorDTO.setDoctorContactNumber(dto.getDoctorContactNumber());
    		createDoctorDTO.setSpecialityLevel(dto.getSpecialityLevel());
    		// DEBUG genFunctionCall Service call
    		createDoctorService.execute(createDoctorDTO);
    		// DEBUG genFunctionCall Parameters OUT
    		dto.setLclUsrReturnCode(createDoctorDTO.getUsrReturnCode());
    		// DEBUG genFunctionCall Parameters DONE
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }
    private StepResult dbfDeleteDataRecord(EditDoctorRcdState dto)
    {
        StepResult result = NO_ACTION;
        try {
            /**
    		 * USER: Delete Object (Generated:383)
    		 */
    		DeleteDoctorDTO deleteDoctorDTO;
    		//switchBLK 383 BLK ACT
    		// DEBUG genFunctionCall 384 ACT Delete Doctor - Doctor  *
    		// DEBUG genFunctionCall ServiceDtoVariable
    		deleteDoctorDTO = new DeleteDoctorDTO();
    		// DEBUG genFunctionCall Parameters IN
    		deleteDoctorDTO.setDoctorCode(dto.getDoctorCode());
    		// DEBUG genFunctionCall Service call
    		deleteDoctorService.execute(deleteDoctorDTO);
    		// DEBUG genFunctionCall Parameters OUT
    		// DEBUG genFunctionCall Parameters DONE
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    
        return result;
    }
    
    private StepResult dbfUpdateDataRecord(EditDoctorRcdState dto)
    {
        StepResult result = NO_ACTION;
        try {
    		DoctorId doctorId = new DoctorId(dto.getDoctorCode());
    		if (doctorRepository.existsById(doctorId)) {
    
            	/**
    			 * USER: Change Object (Generated:427)
    			 */
    			ChangeDoctorDTO changeDoctorDTO;
    			//switchBLK 427 BLK ACT
    			// DEBUG genFunctionCall 428 ACT Change Doctor - Doctor  *
    			// DEBUG genFunctionCall ServiceDtoVariable
    			changeDoctorDTO = new ChangeDoctorDTO();
    			// DEBUG genFunctionCall Parameters IN
    			changeDoctorDTO.setDoctorCode(dto.getDoctorCode());
    			changeDoctorDTO.setHospitalCode(dto.getHospitalCode());
    			changeDoctorDTO.setSupervisingDoctorDoctor(dto.getSupervisingDoctorDoctor());
    			changeDoctorDTO.setDoctorName(dto.getDoctorName());
    			changeDoctorDTO.setDoctorContactNumber(dto.getDoctorContactNumber());
    			changeDoctorDTO.setSpecialityLevel(dto.getSpecialityLevel());
    			// DEBUG genFunctionCall Service call
    			changeDoctorService.execute(changeDoctorDTO);
    			// DEBUG genFunctionCall Parameters OUT
    			dto.setLclUsrReturnCode(changeDoctorDTO.getUsrReturnCode());
    			// DEBUG genFunctionCall Parameters DONE
    	        }
            else {
                throw new ServiceException("diagnosis.nf");
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }
    
    
/**
 * ------------------ Generated ActionDiagram UserPoint -----------------
 */

    private StepResult usrValidateDetailScreenFields(EditDoctorRcdState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Validate Detail Screen Fields (Generated:502)
			 */
			//switchSUB 502 SUB    
			//switchBLK 1000013 BLK CAS
			//switchSUB 1000013 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				//switchBLK 1000016 BLK ACT
				// DEBUG genFunctionCall 1000017 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000022 BLK ACT
				// DEBUG genFunctionCall 1000023 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrExitCommandProcessing(EditDoctorRcdState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Exit Program Processing (Generated:64)
			 */
			//switchSUB 64 SUB    
			//switchBLK 1000001 BLK CAS
			//switchSUB 1000001 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// KEY.*CMD key is *Exit
				//switchBLK 1000004 BLK ACT
				// DEBUG genFunctionCall 1000005 ACT PAR.*Return code = CND.*Normal
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000010 BLK ACT
				// DEBUG genFunctionCall 1000011 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }


    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * DeleteDoctorService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceDeleteDoctor(EditDoctorRcdState state, DeleteDoctorParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * CreateDoctorService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateDoctor(EditDoctorRcdState state, CreateDoctorParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ChangeDoctorService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChangeDoctor(EditDoctorRcdState state, ChangeDoctorParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(EditDoctorRcdState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(EditDoctorRcdState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//

    /**
     * SelectPatientService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processPromptSelectDoctor(EditDoctorRcdState state, SelectDoctorParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        result = displayKeyScreenConversation(state);

        return result;
    }

}
