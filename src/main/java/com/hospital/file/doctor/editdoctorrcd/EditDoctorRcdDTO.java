package com.hospital.file.doctor.editdoctorrcd;
import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.file.doctor.Doctor;
import com.hospital.model.SpecialityLevelEnum;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.common.state.BaseDTO;

/**
 * Dto for file 'Doctor' (TSAFREP) and function 'Edit Doctor RCD' (TSBOE1R).
 */
public class EditDoctorRcdDTO extends BaseDTO {
    private static final long serialVersionUID = 7659697234473404215L;

    private String doctorCode = "";
    private String sflselPromptText = "";
    private String hospitalCode = "";
    private String supervisingDoctorDoctor = "";
    private String doctorName = "";
    private long doctorContactNumber = 0L;
    private SpecialityLevelEnum specialityLevel = null;
    private String addedUser = "";
    private LocalDate addedDate = null;
    private LocalTime addedTime = null;
    private String changedUser = "";
    private LocalDate changedDate = null;
    private LocalTime changedTime = null;
    private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");

    public EditDoctorRcdDTO() {
    }

    public EditDoctorRcdDTO(Doctor doctor) {
        setDtoFields(doctor);
    }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    public String getSflselPromptText() {
        return sflselPromptText;
    }

    public void setSflselPromptText(String sflselPromptText) {
        this.sflselPromptText = sflselPromptText;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getSupervisingDoctorDoctor() {
        return supervisingDoctorDoctor;
    }

    public void setSupervisingDoctorDoctor(String supervisingDoctorDoctor) {
        this.supervisingDoctorDoctor = supervisingDoctorDoctor;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public long getDoctorContactNumber() {
        return doctorContactNumber;
    }

    public void setDoctorContactNumber(long doctorContactNumber) {
        this.doctorContactNumber = doctorContactNumber;
    }

    public SpecialityLevelEnum getSpecialityLevel() {
        return specialityLevel;
    }

    public void setSpecialityLevel(SpecialityLevelEnum specialityLevel) {
        this.specialityLevel = specialityLevel;
    }

    public String getAddedUser() {
        return addedUser;
    }

    public void setAddedUser(String addedUser) {
        this.addedUser = addedUser;
    }

    public LocalDate getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(LocalDate addedDate) {
        this.addedDate = addedDate;
    }

    public LocalTime getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(LocalTime addedTime) {
        this.addedTime = addedTime;
    }

    public String getChangedUser() {
        return changedUser;
    }

    public void setChangedUser(String changedUser) {
        this.changedUser = changedUser;
    }

    public LocalDate getChangedDate() {
        return changedDate;
    }

    public void setChangedDate(LocalDate changedDate) {
        this.changedDate = changedDate;
    }

    public LocalTime getChangedTime() {
        return changedTime;
    }

    public void setChangedTime(LocalTime changedTime) {
        this.changedTime = changedTime;
    }

    public ReturnCodeEnum getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(ReturnCodeEnum returnCode) {
        this.returnCode = returnCode;
    }

    /**
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param doctor Doctor Entity bean
     */
    public void setDtoFields(Doctor doctor) {
        BeanUtils.copyProperties(doctor, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param doctor Doctor Entity bean
     */
    public void setEntityFields(Doctor doctor) {
        BeanUtils.copyProperties(this, doctor);
    }
}
