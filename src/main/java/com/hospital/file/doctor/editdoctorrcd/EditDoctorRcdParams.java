package com.hospital.file.doctor.editdoctorrcd;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Params for resource: EditDoctorRcd (TSBOE1R).
 *
 * @author X2EGenerator
 */
public class EditDoctorRcdParams implements Serializable {
    private String doctorCode = "";

    public String getDoctorCode() {
		return doctorCode;
	}
	
	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}
}
