package com.hospital.file.doctor;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;

import com.hospital.model.CountryEnum;

import com.hospital.file.doctor.Doctor;
import com.hospital.file.doctor.selectdoctor.SelectDoctorGDO;
import com.hospital.file.doctor.editdoctor.EditDoctorGDO;
import com.hospital.file.doctor.dspfdoctorsforhospital.DspfDoctorsForHospitalGDO;
import com.hospital.file.doctor.editdoctorhospital.EditDoctorHospitalGDO;

/**
 * Custom Spring Data JPA repository implementation for model: Doctor (TSAFREP).
 *
 * @author X2EGenerator
 */
@Repository
public class DoctorRepositoryImpl implements DoctorRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see com.hospital.file.doctor.DoctorService#deleteDoctor(Doctor)
	 */
	@Override
	public void deleteDoctor(
		String doctorCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " doctor.id.doctorCode = :doctorCode";
			isParamSet = true;
		}

		String sqlString = "DELETE FROM Doctor doctor";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			query.setParameter("doctorCode", doctorCode);
		}

		em.createNativeQuery("SET foreign_key_checks = 0").executeUpdate();
		query.executeUpdate();
		em.createNativeQuery("SET foreign_key_checks = 1").executeUpdate();
	}

	@Override
	public RestResponsePage<SelectDoctorGDO> selectDoctor(
		String doctorCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " (doctor.id.doctorCode >= :doctorCode OR doctor.id.doctorCode like CONCAT('%', :doctorCode, '%'))";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.doctor.selectdoctor.SelectDoctorGDO(doctor.id.doctorCode, doctor.doctorName, doctor.doctorContactNumber, doctor.specialityLevel, doctor.hospitalCode, doctor.supervisingDoctorDoctor) from Doctor doctor";
		String countQueryString = "SELECT COUNT(doctor.id.doctorCode) FROM Doctor doctor";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Doctor doctor = new Doctor();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(doctor.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"doctor.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"doctor." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"doctor.id.doctorCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			countQuery.setParameter("doctorCode", doctorCode);
			query.setParameter("doctorCode", doctorCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<SelectDoctorGDO> content = query.getResultList();
		RestResponsePage<SelectDoctorGDO> pageDto = new RestResponsePage<SelectDoctorGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<EditDoctorGDO> editDoctor(
		String doctorCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " (doctor.id.doctorCode >= :doctorCode OR doctor.id.doctorCode like CONCAT('%', :doctorCode, '%'))";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.doctor.editdoctor.EditDoctorGDO(doctor.id.doctorCode, doctor.doctorName, doctor.doctorContactNumber, doctor.specialityLevel, doctor.hospitalCode, doctor.supervisingDoctorDoctor) from Doctor doctor";
		String countQueryString = "SELECT COUNT(doctor.id.doctorCode) FROM Doctor doctor";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Doctor doctor = new Doctor();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(doctor.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"doctor.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"doctor." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"doctor.id.doctorCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			countQuery.setParameter("doctorCode", doctorCode);
			query.setParameter("doctorCode", doctorCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<EditDoctorGDO> content = query.getResultList();
		RestResponsePage<EditDoctorGDO> pageDto = new RestResponsePage<EditDoctorGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.doctor.DoctorService#rtvForSupervisor(String)
	 */
	@Override
	public Doctor rtvForSupervisor(
		String doctorCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " doctor.id.doctorCode = :doctorCode";
			isParamSet = true;
		}

		String sqlString = "SELECT doctor FROM Doctor doctor";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			query.setParameter("doctorCode", doctorCode);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		Doctor dto = (Doctor)query.getSingleResult();

		return dto;
	}

	@Override
	public RestResponsePage<DspfDoctorsForHospitalGDO> dspfDoctorsForHospital(
		String hospitalCode, CountryEnum country, String countryName, long telephoneNumber, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " doctor.hospitalCode like CONCAT('%', :hospitalCode, '%')";
			isParamSet = true;
		}
		
		String sqlString = "SELECT new com.hospital.file.doctor.dspfdoctorsforhospital.DspfDoctorsForHospitalGDO(doctor.hospitalCode, doctor.id.doctorCode, doctor.doctorName, doctor.doctorContactNumber, doctor.specialityLevel,doctor.supervisingDoctorDoctor) from Doctor doctor";
		String countQueryString = "SELECT COUNT(doctor.id.doctorCode) FROM Doctor doctor";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Doctor doctor = new Doctor();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(doctor.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"doctor.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"doctor." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"doctor.hospitalCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<DspfDoctorsForHospitalGDO> content = query.getResultList();
		RestResponsePage<DspfDoctorsForHospitalGDO> pageDto = new RestResponsePage<DspfDoctorsForHospitalGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.doctor.DoctorService#rtvDoctorsForPatient(String)
	 */
	@Override
	public RestResponsePage<Doctor> rtvDoctorsForPatient(
		String hospitalCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " doctor.hospitalCode = :hospitalCode";
			isParamSet = true;
		}

		String sqlString = "SELECT doctor FROM Doctor doctor";
		String countQueryString = "SELECT COUNT(doctor.id.doctorCode) FROM Doctor doctor";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<Doctor> content = query.getResultList();
		RestResponsePage<Doctor> pageDto = new RestResponsePage<Doctor>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.doctor.DoctorService#rtvDoctorPerDiagnosis(String)
	 */
	@Override
	public Doctor rtvDoctorPerDiagnosis(
		String doctorCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " doctor.id.doctorCode = :doctorCode";
			isParamSet = true;
		}

		String sqlString = "SELECT doctor FROM Doctor doctor";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			query.setParameter("doctorCode", doctorCode);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		Doctor dto = (Doctor)query.getSingleResult();

		return dto;
	}

	/**
	 * @see com.hospital.file.doctor.DoctorService#rtvDoctorName(String)
	 */
	@Override
	public Doctor rtvDoctorName(
		String doctorCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " doctor.id.doctorCode = :doctorCode";
			isParamSet = true;
		}

		String sqlString = "SELECT doctor FROM Doctor doctor";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			query.setParameter("doctorCode", doctorCode);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		Doctor dto = (Doctor)query.getSingleResult();

		return dto;
	}

	@Override
	public RestResponsePage<EditDoctorHospitalGDO> editDoctorHospital(
		String hospitalCode, String doctorCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " doctor.hospitalCode like CONCAT('%', :hospitalCode, '%')";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(doctorCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " (doctor.id.doctorCode >= :doctorCode OR doctor.id.doctorCode like CONCAT('%', :doctorCode, '%'))";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.doctor.editdoctorhospital.EditDoctorHospitalGDO(doctor.hospitalCode, doctor.id.doctorCode, doctor.doctorName, doctor.doctorContactNumber, doctor.specialityLevel, doctor.addedUser, doctor.addedDate, doctor.addedTime, doctor.changedUser, doctor.changedDate, doctor.changedTime, doctor.supervisingDoctorDoctor) from Doctor doctor";
		String countQueryString = "SELECT COUNT(doctor.id.doctorCode) FROM Doctor doctor";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Doctor doctor = new Doctor();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(doctor.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"doctor.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"doctor." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"doctor.hospitalCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		if (StringUtils.isNotEmpty(doctorCode)) {
			countQuery.setParameter("doctorCode", doctorCode);
			query.setParameter("doctorCode", doctorCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<EditDoctorHospitalGDO> content = query.getResultList();
		RestResponsePage<EditDoctorHospitalGDO> pageDto = new RestResponsePage<EditDoctorHospitalGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.doctor.DoctorService#rtvHospitalExist(String)
	 */
	@Override
	public RestResponsePage<Doctor> rtvHospitalExist(
		String hospitalCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " doctor.hospitalCode = :hospitalCode";
			isParamSet = true;
		}

		String sqlString = "SELECT doctor FROM Doctor doctor";
		String countQueryString = "SELECT COUNT(doctor.id.doctorCode) FROM Doctor doctor";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<Doctor> content = query.getResultList();
		RestResponsePage<Doctor> pageDto = new RestResponsePage<Doctor>(
				content, pageable, count.longValue());

		return pageDto;
	}

}
