package com.hospital.file.doctor.dspfdoctorsforhospital;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.file.doctor.Doctor;
import com.hospital.model.SpecialityLevelEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;

/**
 * Gdo for file 'Doctor' (TSAFREP) and function 'DSPF Doctors for Hospital' (TSBFDFR).
 */
public class DspfDoctorsForHospitalGDO implements Serializable {
	private static final long serialVersionUID = -5698436827408350675L;

    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private String hospitalCode = "";
	private String doctorCode = "";
	private String doctorName = "";
	private long doctorContactNumber = 0L;
	private SpecialityLevelEnum specialityLevel = null;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;
	private String supervisingDoctorDoctor = "";

	public DspfDoctorsForHospitalGDO() {

	}
	//TODO: Fields not available in database.	
   	public DspfDoctorsForHospitalGDO(String hospitalCode, String doctorCode, String doctorName, long doctorContactNumber, SpecialityLevelEnum specialityLevel,/*, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime,*/ String supervisingDoctorDoctor) {
		this.hospitalCode = hospitalCode;
		this.doctorCode = doctorCode;
		this.doctorName = doctorName;
		this.doctorContactNumber = doctorContactNumber;
		this.specialityLevel = specialityLevel;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
		this.supervisingDoctorDoctor = supervisingDoctorDoctor;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setHospitalCode(String hospitalCode) {
    	this.hospitalCode = hospitalCode;
    }

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setDoctorCode(String doctorCode) {
    	this.doctorCode = doctorCode;
    }

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorName(String doctorName) {
    	this.doctorName = doctorName;
    }

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorContactNumber(long doctorContactNumber) {
    	this.doctorContactNumber = doctorContactNumber;
    }

	public long getDoctorContactNumber() {
		return doctorContactNumber;
	}

	public void setSpecialityLevel(SpecialityLevelEnum specialityLevel) {
    	this.specialityLevel = specialityLevel;
    }

	public SpecialityLevelEnum getSpecialityLevel() {
		return specialityLevel;
	}

	public void setAddedUser(String addedUser) {
    	this.addedUser = addedUser;
    }

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
    	this.addedDate = addedDate;
    }

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
    	this.addedTime = addedTime;
    }

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
    	this.changedUser = changedUser;
    }

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
    	this.changedDate = changedDate;
    }

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
    	this.changedTime = changedTime;
    }

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void setSupervisingDoctorDoctor(String supervisingDoctorDoctor) {
    	this.supervisingDoctorDoctor = supervisingDoctorDoctor;
    }

	public String getSupervisingDoctorDoctor() {
		return supervisingDoctorDoctor;
	}

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}