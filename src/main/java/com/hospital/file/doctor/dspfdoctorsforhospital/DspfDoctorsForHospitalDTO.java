package com.hospital.file.doctor.dspfdoctorsforhospital;
 
import java.math.BigDecimal;
  
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
 
import com.hospital.common.utils.RestResponsePage;

import com.hospital.model.GlobalContext;
import com.hospital.file.doctor.Doctor;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.common.state.BaseDTO;

/**
 * Dto for file 'Doctor' (TSAFREP) and function 'DSPF Doctors for Hospital' (TSBFDFR).
 */
@Configurable
public class DspfDoctorsForHospitalDTO extends BaseDTO {
	private static final long serialVersionUID = 4456066131164613566L;

    private RestResponsePage<DspfDoctorsForHospitalGDO> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";

	private CountryEnum country = null;
	private ReturnCodeEnum returnCode = null;
	private String countryName = "";
	private String hospitalCode = "";
	private String hospitalName = "";
	private long telephoneNumber = 0L;


	private DspfDoctorsForHospitalGDO gdo;

    public DspfDoctorsForHospitalDTO() {

    }

	public DspfDoctorsForHospitalDTO(String hospitalCode, String hospitalName) {
		this.hospitalCode = hospitalCode;
		this.hospitalName = hospitalName;
	}

    public void setPageDto(RestResponsePage<DspfDoctorsForHospitalGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<DspfDoctorsForHospitalGDO> getPageDto() {
		return pageDto;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setCountry(CountryEnum country) {
		this.country = country;
    }

    public CountryEnum getCountry() {
    	return country;
    }

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
    }

    public ReturnCodeEnum getReturnCode() {
    	return returnCode;
    }

	public void setCountryName(String countryName) {
		this.countryName = countryName;
    }

    public String getCountryName() {
    	return countryName;
    }

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
    }

    public String getHospitalCode() {
    	return hospitalCode;
    }

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
    	return hospitalName;
    }

	public void setTelephoneNumber(long telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
    }

    public long getTelephoneNumber() {
    	return telephoneNumber;
    }

	public void setGdo(DspfDoctorsForHospitalGDO gdo) {
		this.gdo = gdo;
	}

	public DspfDoctorsForHospitalGDO getGdo() {
		return gdo;
	}

}