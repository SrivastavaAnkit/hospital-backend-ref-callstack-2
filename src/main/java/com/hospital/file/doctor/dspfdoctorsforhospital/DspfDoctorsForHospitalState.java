package com.hospital.file.doctor.dspfdoctorsforhospital;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.ReloadSubfileEnum;

/**
 * State for file 'Doctor' (TSAFREP) and function 'DSPF Doctors for Hospital' (TSBFDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

@Configurable
public class DspfDoctorsForHospitalState extends DspfDoctorsForHospitalDTO {
	private static final long serialVersionUID = -93549703593440060L;


	//@Autowired
	private GlobalContext globalCtx=new GlobalContext();

	// Local fields
	private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");

	// System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;


    public DspfDoctorsForHospitalState() {

    }

	public void setWfAddressPostZip(String addressPostZip) {
		globalCtx.setString("addressPostZip", addressPostZip);
	}

	public String getWfAddressPostZip() {
		return globalCtx.getString("addressPostZip");
	}

	public void setWfAddressProvince(String addressProvince) {
		globalCtx.setString("addressProvince", addressProvince);
	}

	public String getWfAddressProvince() {
		return globalCtx.getString("addressProvince");
	}

	public void setWfAddressStreet(String addressStreet) {
		globalCtx.setString("addressStreet", addressStreet);
	}

	public String getWfAddressStreet() {
		return globalCtx.getString("addressStreet");
	}

	public void setWfAddressTown(String addressTown) {
		globalCtx.setString("addressTown", addressTown);
	}

	public String getWfAddressTown() {
		return globalCtx.getString("addressTown");
	}

	public void setWfFaxNumber(long faxNumber) {
		globalCtx.setLong("faxNumber", faxNumber);
	}

	public long getWfFaxNumber() {
		return globalCtx.getLong("faxNumber");
	}

	public void setLclReturnCode(ReturnCodeEnum returnCode) {
    	this.lclReturnCode = returnCode;
    }

    public ReturnCodeEnum getLclReturnCode() {
    	return lclReturnCode;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }
    }