package com.hospital.file.doctor.rtvforsupervisor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.doctor.Doctor;
import com.hospital.file.doctor.DoctorId;
import com.hospital.file.doctor.DoctorRepository;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvForSupervisorService extends AbstractService<RtvForSupervisorService, RtvForSupervisorDTO>
{
    private final Step execute = define("execute", RtvForSupervisorDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private DoctorRepository doctorRepository;
	
    @Autowired
    public RtvForSupervisorService()
    {
        super(RtvForSupervisorService.class, RtvForSupervisorDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvForSupervisorDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvForSupervisorDTO dto, RtvForSupervisorDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Doctor> doctorList = doctorRepository.findAllByIdDoctorCode(dto.getDoctorCode());
		if (!doctorList.isEmpty()) {
			for (Doctor doctor : doctorList) {
				processDataRecord(dto, doctor);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvForSupervisorDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Empty:48)
		 */
		//switchSUB 48 SUB    
		// Unprocessed SUB 48 -
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvForSupervisorDTO dto, Doctor doctor) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		//switchSUB 41 SUB    
		//switchBLK 1000008 BLK ACT
		//functionCall 1000009 ACT PAR = DB1 By name
		dto.setDoctorName(doctor.getDoctorName());
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvForSupervisorDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		//switchSUB 52 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT PAR = CON By name
		dto.setDoctorName("");
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvForSupervisorDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Empty:61)
		 */
		//switchSUB 61 SUB    
		// Unprocessed SUB 61 -
        return NO_ACTION;
    }
}
