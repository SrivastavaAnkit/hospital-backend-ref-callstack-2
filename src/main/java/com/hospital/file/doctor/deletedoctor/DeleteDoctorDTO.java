package com.hospital.file.doctor.deletedoctor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class DeleteDoctorDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private ReturnCodeEnum returnCode;
	private String doctorCode;
	private String nextScreen;
	private UsrReturnCodeEnum lclUsrReturnCode;

	public String getDoctorCode() {
		return doctorCode;
	}

	public UsrReturnCodeEnum getLclUsrReturnCode() {
		return lclUsrReturnCode;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public void setLclUsrReturnCode(UsrReturnCodeEnum lclUsrReturnCode) {
		this.lclUsrReturnCode = lclUsrReturnCode;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
