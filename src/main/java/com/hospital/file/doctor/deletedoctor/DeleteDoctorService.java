package com.hospital.file.doctor.deletedoctor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.doctor.Doctor;
import com.hospital.file.doctor.DoctorId;
import com.hospital.file.doctor.DoctorRepository;
import com.hospital.file.diagnosis.rtvdoctorexist.RtvDoctorExistService;
import com.hospital.file.diagnosis.rtvdoctorexist.RtvDoctorExistDTO;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class DeleteDoctorService  extends AbstractService<DeleteDoctorService, DeleteDoctorDTO>
{
    private final Step execute = define("execute", DeleteDoctorDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private DoctorRepository doctorRepository;
	
	@Autowired
	private RtvDoctorExistService rtvDoctorExistService;
	

    @Autowired
    public DeleteDoctorService() {
        super(DeleteDoctorService.class, DeleteDoctorDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(DeleteDoctorDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(DeleteDoctorDTO dto, DeleteDoctorDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataUpdate(dto);
		DoctorId doctorId = new DoctorId();
		doctorId.setDoctorCode(dto.getDoctorCode());
		try {
			doctorRepository.deleteById(doctorId);
			doctorRepository.flush();
			dto.setReturnCode(ReturnCodeEnum.fromCode(""));
			processingAfterDataUpdate(dto);
		} catch (Exception e) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
		}

       return result;
	}

	private StepResult processingBeforeDataUpdate(DeleteDoctorDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Generated:9)
		 */
		RtvDoctorExistDTO rtvDoctorExistDTO;
		//switchSUB 9 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT RTV Doctor Exist? - Diagnosis  *
		rtvDoctorExistDTO = new RtvDoctorExistDTO();
		rtvDoctorExistDTO.setDoctorCode(dto.getDoctorCode());
		rtvDoctorExistService.execute(rtvDoctorExistDTO);
		dto.setLclUsrReturnCode(rtvDoctorExistDTO.getUsrReturnCode());
		//switchBLK 1000005 BLK CAS
		//switchSUB 1000005 BLK CAS
		if (dto.getLclUsrReturnCode() == UsrReturnCodeEnum.fromCode("F")) {
			// LCL.USR Return Code is Record found
			//switchBLK 1000008 BLK ACT
			//functionCall 1000009 ACT Send error message - 'Can't delete Doctor'
			// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1103237)
			//switchBLK 1000010 BLK ACT
			//functionCall 1000011 ACT <-- *QUIT
			// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
		}
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(DeleteDoctorDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:17)
		 */
		//switchSUB 17 SUB    
		//switchBLK 1000012 BLK ACT
		//functionCall 1000013 ACT PGM.*Return code = CND.*Normal
		dto.setReturnCode(ReturnCodeEnum.fromCode(""));
       return NO_ACTION;
	}
}
