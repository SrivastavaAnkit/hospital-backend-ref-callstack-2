package com.hospital.file.doctor.rtvdoctorsforpatient;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.doctor.Doctor;
import com.hospital.file.doctor.DoctorId;
import com.hospital.file.doctor.DoctorRepository;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvDoctorsForPatientService extends AbstractService<RtvDoctorsForPatientService, RtvDoctorsForPatientDTO>
{
    private final Step execute = define("execute", RtvDoctorsForPatientDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private DoctorRepository doctorRepository;
	
    @Autowired
    public RtvDoctorsForPatientService()
    {
        super(RtvDoctorsForPatientService.class, RtvDoctorsForPatientDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvDoctorsForPatientDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvDoctorsForPatientDTO dto, RtvDoctorsForPatientDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Doctor> doctorList = doctorRepository.findAllByHospitalCode(dto.getHospitalCode());
		if (!doctorList.isEmpty()) {
			for (Doctor doctor : doctorList) {
				processDataRecord(dto, doctor);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvDoctorsForPatientDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Empty:48)
		 */
		//switchSUB 48 SUB    
		// Unprocessed SUB 48 -
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvDoctorsForPatientDTO dto, Doctor doctor) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		//switchSUB 41 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT PAR = DB1 By name
		dto.setDoctorCode(doctor.getDoctorCode());
		//switchBLK 1000008 BLK ACT
		//functionCall 1000009 ACT <-- *QUIT
		// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvDoctorsForPatientDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		//switchSUB 52 SUB    
		//switchBLK 1000010 BLK ACT
		//functionCall 1000011 ACT PGM.*Return code = CND.*Record does not exist
		dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0005"));
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvDoctorsForPatientDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Empty:61)
		 */
		//switchSUB 61 SUB    
		// Unprocessed SUB 61 -
        return NO_ACTION;
    }
}
