package com.hospital.file.doctor.selectdoctor;

import java.io.Serializable;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;

import com.hospital.common.state.BaseDTO;
import com.hospital.common.state.SelectRecordDTO;
import com.hospital.file.doctor.Doctor;
import com.hospital.model.RecordSelectedEnum;


/**
 * Dto for file 'Doctor' (TSAFREP) and function 'Select Doctor' (TSAFSRR).
 */
public class SelectDoctorDTO extends SelectRecordDTO<SelectDoctorGDO> {
	private static final long serialVersionUID = 8338559758202784841L;

	private String doctorCode = "";

	public SelectDoctorDTO() {
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param doctor Doctor Entity bean
     */
    public void setDtoFields(Doctor doctor) {
        BeanUtils.copyProperties(doctor, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param doctor Doctor Entity bean
     */
    public void setEntityFields(Doctor doctor) {
        BeanUtils.copyProperties(this, doctor);
    }
}