package com.hospital.file.doctor.selectdoctor;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.file.doctor.Doctor;
import com.hospital.model.ReloadSubfileEnum;

/**
 * State for file 'Doctor' (TSAFREP) and function 'Select Doctor' (TSAFSRR).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectDoctorState extends SelectDoctorDTO {
    private static final long serialVersionUID = 3042023384409194791L;


    // System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    public SelectDoctorState() {
    }

    public SelectDoctorState(Doctor doctor) {
        setDtoFields(doctor);
    }

   public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
       _sysReloadSubfile = reloadSubfile;
   }

   public ReloadSubfileEnum get_SysReloadSubfile() {
       return _sysReloadSubfile;
   }

}
