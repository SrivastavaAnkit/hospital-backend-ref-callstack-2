package com.hospital.file.doctor.selectdoctor;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

import java.io.IOException;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;

import com.hospital.file.doctor.DoctorRepository;

import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorService;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorDTO;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;
import com.hospital.support.JobContext;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.RecordSelectedEnum;
import com.hospital.model.ReloadSubfileEnum;


/**
 * Service for file 'Doctor' (TSAFREP) and function 'Select Doctor' (TSAFSRR).
 */
@Service
public class SelectDoctorService extends AbstractService<SelectDoctorService,SelectDoctorState> {
    
	@Autowired
	private JobContext job;

	@Autowired
	private DoctorRepository doctorRepository;
        
    @Autowired
    private RtvForSupervisorService rtvForSupervisorService;
    

    
    public static final String SCREEN_KEY = "selectDoctor";

	private final Step execute = define("execute", SelectDoctorParams.class, this::execute);
	private final Step response = define("response", SelectDoctorDTO.class, this::processResponse);
	//private final Step serviceRtvForSupervisor = define("serviceRtvForSupervisor",RtvForSupervisorParams.class, this::processServiceRtvForSupervisor);
	
    

	@Autowired
	public SelectDoctorService()
	{
		super(SelectDoctorService.class, SelectDoctorState.class);
	}

	@Override
	public Step getInitialStep()
	{
		return execute;
	}

    /**
     * SelectDoctor service starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
	private StepResult execute(SelectDoctorState state, SelectDoctorParams params)
	{
        StepResult result = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
		usrInitializeProgram(state);
		result =  mainLoop(state);

        return result;
	}

    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
	private StepResult mainLoop(SelectDoctorState state)
	{
        StepResult result = NO_ACTION;

		dbfReadFirstDataRecord(state);
		if (state.getPageDto() != null && state.getPageDto().getSize() > 0)
		{
			loadNextSubfilePage(state);
		}
		state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
		result = conductScreenConversation(state);

        return result;
	}

    /**
     * SCREEN_KEY display processing loop method.
     * @param state - Service state class.
     * @return
     */
	private StepResult conductScreenConversation(SelectDoctorState state)
	{
		StepResult result = NO_ACTION;

		if(state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)){
            SelectDoctorDTO dto = new SelectDoctorDTO();
            BeanUtils.copyProperties(state, dto);
			result = callScreen(SCREEN_KEY, dto).thenCall(response);
		}

		return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param dto - returned screen model.
     * @return
     */
	private StepResult processResponse(SelectDoctorState state, SelectDoctorDTO dto)
	{
        StepResult result = NO_ACTION;

        
        
        //restore pageDTO
        //temporary fix because client has to remove pageDTO
        dto.setPageDto(state.getPageDto());
		BeanUtils.copyProperties(dto, state);
		if(CmdKeyEnum.isExit(state.get_SysCmdKey()))
		{
			result = closedown(state);
            return result;
		}
		else if(CmdKeyEnum.isReset(state.get_SysCmdKey())) {
			//TODO: processResetRequest(state);//synon built-in function
		}
		else if(CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
			//TODO:processHelpRequest(state);//synon built-in function
		}
		else if(CmdKeyEnum.isNextPage(state.get_SysCmdKey())) {
			dbfReadNextDataRecord(state);
			loadNextSubfilePage(state);
		}
		else {
			usrProcessSubfileControl(state);
			//TODO:readFirstChangedSubfileRecord(state);//synon built-in function
//			while (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {//TODO:while(Changed subfile record found)
//                for (SelectDoctorGDO gdo : ((Page<SelectDoctorGDO>) state.getPageDto()).getContent())
//		        {
                    if(state.getDoctorCode() != null && !state.getDoctorCode().equals(""))
                    {
                        SelectDoctorGDO gdo = null;
                        for(SelectDoctorGDO obj: state.getPageDto().getContent()) {
                            if(obj.getDoctorCode().equals(state.getDoctorCode())) {
                                gdo = obj;
                                break;
                            }
                        }
                        usrProcessSelectedLine(state, gdo);
                        SelectDoctorParams params = new SelectDoctorParams();
                        BeanUtils.copyProperties(state, params);
                        result = StepResult.returnFromService(params);
                        return result;
                    }
//                    usrProcessChangedSubfileRecord(state, gdo);
//                    usrScreenFunctionFields(state, gdo);
//                    //TODO:updateSubfileRecord(state, gdo);//synon built-in function
//                    //TODO:readNextChangedSubfileRecord(state);//synon built-in function
//                }
			if(!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) //TODO:if(positioning field values have changed)
			{
				state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
			}
			usrProcessCommandKeys(state);
        }
        result = conductScreenConversation(state);

        return result;
	}

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     * @return
     */
	private void loadNextSubfilePage(SelectDoctorState state)
	{
		for (SelectDoctorGDO gdo : ((Page<SelectDoctorGDO>) state.getPageDto()).getContent())
		{
            state.setRecordSelect(RecordSelectedEnum._STA_YES);
			//TODO:moveDbfRecordFieldsToSubfileRecord(state);//synon built-in function
			usrScreenFunctionFields(state, gdo);
			usrLoadSubfileRecordFromDbfRecord(state, gdo);
            if(state.getRecordSelect().getCode().equals(RecordSelectedEnum._STA_YES.getCode()))
            {
                //TODO:writeSubfileRecord(state);//synon built-in function
            }
		}
	}

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
	private StepResult closedown(SelectDoctorState state)
	{
        StepResult result = NO_ACTION;

		usrExitProgramProcessing(state);

        SelectDoctorParams params = new SelectDoctorParams();
        BeanUtils.copyProperties(state, params);
        result = StepResult.returnFromService(params);

		return result;
	}
    
    /**
     * ------------------------- Generated DBF method ---------------------------
     */

    /**
     * Read data of the first page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadFirstDataRecord(SelectDoctorState state)
	{
		state.setPage(0);
		dbfReadDataRecord(state);
	}

    /**
     * Read data of the next page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadNextDataRecord(SelectDoctorState state)
	{
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    /**
     * Read data of the actual page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadDataRecord(SelectDoctorState state)
	{
		List<Order> sortOrders = new ArrayList<Order>();
		Pageable pageable;

		try
		{
			@SuppressWarnings("unchecked")
			Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

			for (Map.Entry<String, String> entry : sortDataMap.entrySet())
			{
 				if (entry.getValue() == null)
				{
  					continue;
				}

				sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
			}
		}
		catch (IOException ioe)
		{
		}

		if (CollectionUtils.isEmpty(sortOrders))
		{
			pageable = PageRequest.of(state.getPage(), state.getSize());
		}
		else
		{
			pageable = PageRequest.of(state.getPage(), state.getSize(), Sort.by(sortOrders));
		}

		RestResponsePage<SelectDoctorGDO> pageDto = doctorRepository.selectDoctor(null, pageable);
		state.setPageDto(pageDto);
	}
    
    /**
     * ---------------------- Programmatic user-point --------------------------
     */

	/**
	 * USER: Initialize Program (Generated:20)
	 */
	private StepResult usrInitializeProgram(SelectDoctorState dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 20 SUB    
			// Unprocessed SUB 20 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Process Subfile Control (Generated:72)
	 */
	private StepResult usrProcessSubfileControl(SelectDoctorState dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 72 SUB    
			// Unprocessed SUB 72 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Process Selected Line (Generated:107)
	 */
	private StepResult usrProcessSelectedLine(SelectDoctorState dto, SelectDoctorGDO gdo)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 107 SUB    
			// Unprocessed SUB 107 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Process Changed Subfile Record (Generated:101)
	 */
	private StepResult usrProcessChangedSubfileRecord(SelectDoctorState dto, SelectDoctorGDO gdo)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 101 SUB    
			// Unprocessed SUB 101 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * CALC: Screen Function Fields (Generated:165)
	 */
	private StepResult usrScreenFunctionFields(SelectDoctorState dto, SelectDoctorGDO gdo)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 165 SUB    
			// Unprocessed SUB 165 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Process Command Keys (Generated:143)
	 */
	private StepResult usrProcessCommandKeys(SelectDoctorState dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 143 SUB    
			// Unprocessed SUB 143 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Load Subfile Record from DBF Record (Generated:41)
	 */
	private StepResult usrLoadSubfileRecordFromDbfRecord(SelectDoctorState dto, SelectDoctorGDO gdo)
	{
        StepResult result = NO_ACTION;

        try
        {
		    RtvForSupervisorDTO rtvForSupervisorDTO;
			//switchSUB 41 SUB    
			//switchBLK 1000001 BLK ACT
			// DEBUG genFunctionCall 1000002 ACT RTV For Supervisor - Doctor  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvForSupervisorDTO = new RtvForSupervisorDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvForSupervisorDTO.setDoctorCode(gdo.getSupervisingDoctorDoctor());
			// DEBUG genFunctionCall Service call
			rtvForSupervisorService.execute(rtvForSupervisorDTO);
			// DEBUG genFunctionCall Parameters OUT
			gdo.setSupervisingDoctorName(rtvForSupervisorDTO.getDoctorName());
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
	private StepResult usrExitProgramProcessing(SelectDoctorState dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 132 SUB    
			// Unprocessed SUB 132 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * RtvForSupervisorService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvForSupervisor(SelectDoctorState state, RtvForSupervisorParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//


}
