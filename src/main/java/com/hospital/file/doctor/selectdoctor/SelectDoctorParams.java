package com.hospital.file.doctor.selectdoctor;

import java.io.Serializable;


/**
 * Params for resource: SelectDoctor (TSAFSRR).
 *
 * @author X2EGenerator
 */
public class SelectDoctorParams implements Serializable {
    private static final long serialVersionUID = -7512016137987865697L;

	private String doctorCode = "";

	public String getDoctorCode() {
		return doctorCode;
	}
	
	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}
}

