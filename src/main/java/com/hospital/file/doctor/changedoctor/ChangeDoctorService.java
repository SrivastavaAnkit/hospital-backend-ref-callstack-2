package com.hospital.file.doctor.changedoctor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.doctor.Doctor;
import com.hospital.file.doctor.DoctorId;
import com.hospital.file.doctor.DoctorRepository;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class ChangeDoctorService extends AbstractService<ChangeDoctorService, ChangeDoctorDTO>
{
    private final Step execute = define("execute", ChangeDoctorDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private DoctorRepository doctorRepository;
	

    @Autowired
    public ChangeDoctorService() {
        super(ChangeDoctorService.class, ChangeDoctorDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(ChangeDoctorDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(ChangeDoctorDTO dto, ChangeDoctorDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);
		DoctorId doctorId = new DoctorId();
		doctorId.setDoctorCode(dto.getDoctorCode());
		Doctor doctor = doctorRepository.findById(doctorId).get();
		if (doctor == null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0005"));
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, doctor);
			doctor.setDoctorCode(dto.getDoctorCode());
			doctor.setHospitalCode(dto.getHospitalCode());
			doctor.setSupervisingDoctorDoctor(dto.getSupervisingDoctorDoctor());
			doctor.setDoctorName(dto.getDoctorName());
			doctor.setDoctorContactNumber(dto.getDoctorContactNumber());
			doctor.setSpecialityLevel(dto.getSpecialityLevel());
			/*doctor.setAddedUser(dto.getAddedUser());
			doctor.setAddedDate(dto.getAddedDate());
			doctor.setAddedTime(dto.getAddedTime());
			doctor.setChangedUser(dto.getChangedUser());
			doctor.setChangedDate(dto.getChangedDate());
			doctor.setChangedTime(dto.getChangedTime());*/
			processingBeforeDataUpdate(dto, doctor);
			try {
				doctorRepository.saveAndFlush(doctor);
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
			}
		}

       return result;
	}

	private StepResult processingBeforeDataRead(ChangeDoctorDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000032 BLK CAS
		//switchSUB 1000032 BLK CAS
		if (dto.getHospitalCode().equals("PUBLIC")) {
			// PAR.Hospital Code is Equal Public
			//switchBLK 1000035 BLK ACT
			//functionCall 1000036 ACT LCL.Doctor Notes = CON.Public Hospital
			dto.setLclDoctorNotes("Public Hospital");
		}
       return NO_ACTION;
	}

	private StepResult processingIfDataRecordNotFound(ChangeDoctorDTO dto) throws ServiceException {
		/**
		 * USER: Processing if Data Record Not Found (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000020 BLK ACT
		//functionCall 1000021 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
	}

	private StepResult processingAfterDataRead(ChangeDoctorDTO dto, Doctor doctor) throws ServiceException {
		/**
		 * USER: Processing After Data Read (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000041 BLK CAS
		//switchSUB 1000041 BLK CAS
		if (dto.getHospitalCode().equals("PUBLIC")) {
			// PAR.Hospital Code is Equal Public
			//switchBLK 1000044 BLK ACT
			//functionCall 1000045 ACT DB1.Speciality Level = CND.General Doctor
			doctor.setSpecialityLevel(SpecialityLevelEnum.fromCode("1"));
		}
       return NO_ACTION;
	}

	private StepResult processingBeforeDataUpdate(ChangeDoctorDTO dto, Doctor doctor) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Generated:10)
		 */
		//switchSUB 10 SUB    
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT DB1.Changed User = JOB.*USER
		/*doctor.setChangedUser(job.getUser());
		//switchBLK 1000014 BLK ACT
		//functionCall 1000015 ACT DB1.Changed Date = JOB.*Job date
		doctor.setChangedDate(LocalDate.now());
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT DB1.Changed Time = JOB.*Job time
		doctor.setChangedTime(LocalTime.now());*/
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(ChangeDoctorDTO dto) throws ServiceException {
		/**
		 * USER: Processing After Data Update (Generated:23)
		 */
		//switchSUB 23 SUB    
		//switchBLK 1000026 BLK ACT
		//functionCall 1000027 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
	}
}
