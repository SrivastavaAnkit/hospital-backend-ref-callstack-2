package com.hospital.file.doctor.changedoctor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;
import com.hospital.model.UsrReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ChangeDoctorDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private LocalDate addedDate;
	private LocalDate changedDate;
	private LocalTime addedTime;
	private LocalTime changedTime;
	private ReturnCodeEnum returnCode;
	private SpecialityLevelEnum specialityLevel;
	private String addedUser;
	private String changedUser;
	private String doctorCode;
	private String doctorName;
	private String hospitalCode;
	private String supervisingDoctorDoctor;
	private UsrReturnCodeEnum usrReturnCode;
	private long doctorContactNumber;
	private String nextScreen;
	private String lclDoctorNotes;

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public long getDoctorContactNumber() {
		return doctorContactNumber;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public SpecialityLevelEnum getSpecialityLevel() {
		return specialityLevel;
	}

	public String getSupervisingDoctorDoctor() {
		return supervisingDoctorDoctor;
	}

	public UsrReturnCodeEnum getUsrReturnCode() {
		return usrReturnCode;
	}

	public String getLclDoctorNotes() {
		return lclDoctorNotes;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public void setDoctorContactNumber(long doctorContactNumber) {
		this.doctorContactNumber = doctorContactNumber;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public void setSpecialityLevel(SpecialityLevelEnum specialityLevel) {
		this.specialityLevel = specialityLevel;
	}

	public void setSupervisingDoctorDoctor(String supervisingDoctorDoctor) {
		this.supervisingDoctorDoctor = supervisingDoctorDoctor;
	}

	public void setUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
		this.usrReturnCode = usrReturnCode;
	}

	public void setLclDoctorNotes(String lclDoctorNotes) {
		this.lclDoctorNotes = lclDoctorNotes;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
