package com.hospital.file.doctor.createdoctor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.doctor.Doctor;
import com.hospital.file.doctor.DoctorId;
import com.hospital.file.doctor.DoctorRepository;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class CreateDoctorService  extends AbstractService<CreateDoctorService, CreateDoctorDTO>
{
    private final Step execute = define("execute", CreateDoctorDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private DoctorRepository doctorRepository;
	

    @Autowired
    public CreateDoctorService() {
        super(CreateDoctorService.class, CreateDoctorDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(CreateDoctorDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(CreateDoctorDTO dto, CreateDoctorDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		Doctor doctor = new Doctor();
		doctor.setDoctorCode(dto.getDoctorCode());
		doctor.setHospitalCode(dto.getHospitalCode());
		doctor.setSupervisingDoctorDoctor(dto.getSupervisingDoctorDoctor());
		doctor.setDoctorName(dto.getDoctorName());
		doctor.setDoctorContactNumber(dto.getDoctorContactNumber());
		doctor.setSpecialityLevel(dto.getSpecialityLevel());
		/*doctor.setAddedUser(dto.getAddedUser());
		doctor.setAddedDate(dto.getAddedDate());
		doctor.setAddedTime(dto.getAddedTime());
		doctor.setChangedUser(dto.getChangedUser());
		doctor.setChangedDate(dto.getChangedDate());
		doctor.setChangedTime(dto.getChangedTime());*/

		processingBeforeDataUpdate(dto, doctor);

		Doctor doctor2 = doctorRepository.findById(doctor.getId()).get();
		if (doctor2 != null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0003"));
			processingIfDataRecordAlreadyExists(dto, doctor2);
		}
		else {
            try {
				doctorRepository.save(doctor);
                dto.setReturnCode(ReturnCodeEnum.fromCode(""));
                processingAfterDataUpdate(dto, doctor);
            } catch (Exception e) {
                dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
                processingIfDataUpdateError(dto, doctor);
            }
        }

        return result;
	}

    private StepResult processingBeforeDataUpdate(CreateDoctorDTO dto, Doctor doctor) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT DB1.Added User = JOB.*USER
//		doctor.setAddedUser(job.getUser());
//		//switchBLK 1000013 BLK ACT
//		//functionCall 1000014 ACT DB1.Added Date = JOB.*Job date
//		doctor.setAddedDate(LocalDate.now());
//		//switchBLK 1000001 BLK ACT
//		//functionCall 1000002 ACT DB1.Added Time = JOB.*Job time
//		doctor.setAddedTime(LocalTime.now());
       return NO_ACTION;
    }

    private StepResult processingIfDataRecordAlreadyExists(CreateDoctorDTO dto, Doctor doctor) throws ServiceException {
		/**
		 * USER: Processing if Data Record Already Exists (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000019 BLK ACT
		//functionCall 1000020 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
    }

    private StepResult processingAfterDataUpdate(CreateDoctorDTO dto, Doctor doctor) throws ServiceException {
		/**
		 * USER: Processing after Data Update (Generated:11)
		 */
		//switchSUB 11 SUB    
		//switchBLK 1000031 BLK ACT
		//functionCall 1000032 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
    }

    private StepResult processingIfDataUpdateError(CreateDoctorDTO dto, Doctor doctor) throws ServiceException {
		/**
		 * Processing if Data Update Error (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000025 BLK ACT
		//functionCall 1000026 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
    }
}
