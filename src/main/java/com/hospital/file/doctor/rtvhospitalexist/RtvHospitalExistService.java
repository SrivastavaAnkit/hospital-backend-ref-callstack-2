package com.hospital.file.doctor.rtvhospitalexist;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.doctor.Doctor;
import com.hospital.file.doctor.DoctorId;
import com.hospital.file.doctor.DoctorRepository;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.SpecialityLevelEnum;
import com.hospital.model.UsrReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvHospitalExistService extends AbstractService<RtvHospitalExistService, RtvHospitalExistDTO>
{
    private final Step execute = define("execute", RtvHospitalExistDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private DoctorRepository doctorRepository;
	
    @Autowired
    public RtvHospitalExistService()
    {
        super(RtvHospitalExistService.class, RtvHospitalExistDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvHospitalExistDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvHospitalExistDTO dto, RtvHospitalExistDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Doctor> doctorList = doctorRepository.findAllByHospitalCode(dto.getHospitalCode());
		if (!doctorList.isEmpty()) {
			for (Doctor doctor : doctorList) {
				processDataRecord(dto, doctor);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvHospitalExistDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvHospitalExistDTO dto, Doctor doctor) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		//switchSUB 41 SUB    
		//switchBLK 1000011 BLK ACT
		//functionCall 1000012 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
		//switchBLK 1000017 BLK ACT
		//functionCall 1000018 ACT <-- *QUIT
		// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvHospitalExistDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		//switchSUB 52 SUB    
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvHospitalExistDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Empty:61)
		 */
		//switchSUB 61 SUB    
		// Unprocessed SUB 61 -
        return NO_ACTION;
    }
}
