package com.hospital.file.logfile;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;
import com.hospital.file.logfile.LogFile;

/**
 * Custom Spring Data JPA repository implementation for model: Log File (TSLOGP).
 *
 * @author X2EGenerator
 */
@Repository
public class LogFileRepositoryImpl implements LogFileRepositoryCustom {
	@PersistenceContext
	EntityManager em;

}
