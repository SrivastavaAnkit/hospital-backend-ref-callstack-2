package com.hospital.file.logfile;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

/**
 * Custom Spring Data JPA repository interface for model: Log File (TSLOGP).
 *
 * @author X2EGenerator
 */
@Repository
public interface LogFileRepositoryCustom {

}
