package com.hospital.file.logfile;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository interface for model: Log File (TSLOGP).
 *
 * @author X2EGenerator
 */
@Repository
public interface LogFileRepository extends LogFileRepositoryCustom, JpaRepository<LogFile, LogFileId> {
}
