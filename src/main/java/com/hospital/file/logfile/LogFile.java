package com.hospital.file.logfile;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name="TSLOGP", schema="HOSPITLGEN")
public class LogFile implements Serializable {
	private static final long serialVersionUID = -1L;

	
	
	

	@EmbeddedId
	private LogFileId id = new LogFileId();

	@Column(name = "ALAMCD")
	private String logFunctionType = "";
	
	@Column(name = "ALA1TX")
	private String logUserPoint = "";

	public LogFileId getId() {
		return id;
	}

	public LocalTime getLogTimestamp() {
		return id.getLogTimestamp();
	}

	public String getLogFunctionType() {
		return logFunctionType;
	}
	
	public String getLogUserPoint() {
		return logUserPoint;
	}

	

	

	public void setLogTimestamp(LocalTime logTimestamp) {
		this.id.setLogTimestamp(logTimestamp);
	}

	public void setLogFunctionType(String logFunctionType) {
		this.logFunctionType = logFunctionType;
	}
	
	public void setLogUserPoint(String logUserPoint) {
		this.logUserPoint = logUserPoint;
	}

	

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
