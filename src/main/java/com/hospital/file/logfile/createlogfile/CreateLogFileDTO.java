package com.hospital.file.logfile.createlogfile;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class CreateLogFileDTO implements Serializable {

	/**
	 * 
	 */
	

	@Autowired
	private GlobalContext global;

	private LocalDateTime logTimestamp;
	private ReturnCodeEnum returnCode;
	private String logFunctionType;
	private String logUserPoint;
	private String nextScreen;

	public String getLogFunctionType() {
		return logFunctionType;
	}

	
	public LocalDateTime getLogTimestamp() {
		return logTimestamp;
	}

	public String getLogUserPoint() {
		return logUserPoint;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setLogFunctionType(String logFunctionType) {
		this.logFunctionType = logFunctionType;
	}

	public void setLogTimestamp(LocalDateTime localDateTime) {
		this.logTimestamp = localDateTime;
	}

	public void setLogUserPoint(String logUserPoint) {
		this.logUserPoint = logUserPoint;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}

	
}
