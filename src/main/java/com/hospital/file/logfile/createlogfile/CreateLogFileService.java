package com.hospital.file.logfile.createlogfile;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.support.JobContext;
import com.hospital.file.logfile.LogFile;
import com.hospital.file.logfile.LogFileId;
import com.hospital.file.logfile.LogFileRepository;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.exception.ServiceException;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class CreateLogFileService  extends AbstractService<CreateLogFileService, CreateLogFileDTO>
{
    private final Step execute = define("execute", CreateLogFileDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private LogFileRepository logFileRepository;
	

    @Autowired
    public CreateLogFileService() {
        super(CreateLogFileService.class, CreateLogFileDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(CreateLogFileDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(CreateLogFileDTO dto, CreateLogFileDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		/*LogFile logFile = new LogFile();
		logFile.setLogTimestamp(dto.getLogTimestamp().toLocalTime());
		logFile.setLogFunctionType(dto.getLogFunctionType());
		logFile.setLogUserPoint(dto.getLogUserPoint());*/
		//processingBeforeDataUpdate(dto, logFile);

		//LogFile logFile2 = logFileRepository.findById(logFile.getId()).get();
		/*if (logFile2 != null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0003"));
			processingIfDataRecordAlreadyExists(dto, logFile2);
		}
		else {
            try {
				logFileRepository.save(logFile);
                dto.setReturnCode(ReturnCodeEnum.fromCode(""));
                processingAfterDataUpdate(dto, logFile);
            } catch (Exception e) {
                dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
                processingIfDataUpdateError(dto, logFile);
            }
        }*/

        return result;
	}

    private StepResult processingBeforeDataUpdate(CreateLogFileDTO dto, LogFile logFile) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Empty:8)
		 */
		
       return NO_ACTION;
    }

    private StepResult processingIfDataRecordAlreadyExists(CreateLogFileDTO dto, LogFile logFile) throws ServiceException {
		/**
		 * USER: Processing if Data Record Already Exists (Empty:43)
		 */
		
       return NO_ACTION;
    }

    private StepResult processingAfterDataUpdate(CreateLogFileDTO dto, LogFile logFile) throws ServiceException {
		/**
		 * USER: Processing after Data Update (Empty:11)
		 */
		
       return NO_ACTION;
    }

    private StepResult processingIfDataUpdateError(CreateLogFileDTO dto, LogFile logFile) throws ServiceException {
		/**
		 * Processing if Data Update Error (Empty:48)
		 */
		
       return NO_ACTION;
    }
}
