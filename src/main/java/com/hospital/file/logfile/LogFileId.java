package com.hospital.file.logfile;

import java.io.Serializable;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.hospital.common.jpa.TimeConverter;

@Embeddable
public class LogFileId implements Serializable {
	private static final long serialVersionUID = -1L;

	@Column(name = "ALABTZ")
	@Convert(converter=  TimeConverter.class)
	private LocalTime logTimestamp = LocalTime.of(0, 0);

	public LogFileId() {
	
	}

	public LogFileId(LocalTime logTimestamp) { 	this.logTimestamp = logTimestamp;
	}

	public LocalTime getLogTimestamp() {
		return logTimestamp;
	}
	
	public void setLogTimestamp(LocalTime logTimestamp) {
		this.logTimestamp = logTimestamp;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
