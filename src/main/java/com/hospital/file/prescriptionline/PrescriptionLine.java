package com.hospital.file.prescriptionline;

import com.hospital.model.MedicationUnitEnum;
import com.hospital.file.medication.Medication;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name="PrescriptionLine", schema="HospitalMgmt")
public class PrescriptionLine implements Serializable {
	private static final long serialVersionUID = -1L;

	@Version
	@Column(name="version")
	private long version;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns ({
		@JoinColumn(name="MedicationCode", referencedColumnName="MedicationCode", insertable=false, updatable=false)
	})
	private Medication medicationObj;

	@EmbeddedId
	private PrescriptionLineId id = new PrescriptionLineId();

	@Column(name = "MedicationCode")
	private String medicationCode = "";
	
	@Column(name = "PrescriptionQuantity")
	private long prescriptionQuantity = 0;

	public PrescriptionLineId getId() {
		return id;
	}

	public String getPrescriptionCode() {
		return id.getPrescriptionCode();
	}
	
	public LocalDate getPrescriptionDate() {
		return id.getPrescriptionDate();
	}
	
	public LocalTime getPrescriptionTime() {
		return id.getPrescriptionTime();
	}
	
	public long getPrescriptionLineNumber() {
		return id.getPrescriptionLineNumber();
	}
	
	public String getDoctorCode() {
		return id.getDoctorCode();
	}

	public String getMedicationCode() {
		return medicationCode;
	}
	
	public long getPrescriptionQuantity() {
		return prescriptionQuantity;
	}

	public String getMedicationDescription() {
		return medicationObj.getMedicationDescription();
	}
	
	public MedicationUnitEnum getMedicationUnit() {
		return medicationObj.getMedicationUnit();
	}

	public long getVersion() {
		return version;
	}

	public void setPrescriptionCode(String prescriptionCode) {
		this.id.setPrescriptionCode(prescriptionCode);
	}
	
	public void setPrescriptionDate(LocalDate prescriptionDate) {
		this.id.setPrescriptionDate(prescriptionDate);
	}
	
	public void setPrescriptionTime(LocalTime prescriptionTime) {
		this.id.setPrescriptionTime(prescriptionTime);
	}
	
	public void setPrescriptionLineNumber(long prescriptionLineNumber) {
		this.id.setPrescriptionLineNumber(prescriptionLineNumber);
	}
	
	public void setDoctorCode(String doctorCode) {
		this.id.setDoctorCode(doctorCode);
	}

	public void setMedicationCode(String medicationCode) {
		this.medicationCode = medicationCode;
	}
	
	public void setPrescriptionQuantity(long prescriptionQuantity) {
		this.prescriptionQuantity = prescriptionQuantity;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
