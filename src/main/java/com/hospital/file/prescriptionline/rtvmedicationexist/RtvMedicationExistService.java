package com.hospital.file.prescriptionline.rtvmedicationexist;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.prescriptionline.PrescriptionLine;
import com.hospital.file.prescriptionline.PrescriptionLineId;
import com.hospital.file.prescriptionline.PrescriptionLineRepository;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvMedicationExistService extends AbstractService<RtvMedicationExistService, RtvMedicationExistDTO>
{
    private final Step execute = define("execute", RtvMedicationExistDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PrescriptionLineRepository prescriptionLineRepository;
	
    @Autowired
    public RtvMedicationExistService()
    {
        super(RtvMedicationExistService.class, RtvMedicationExistDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvMedicationExistDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvMedicationExistDTO dto, RtvMedicationExistDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<PrescriptionLine> prescriptionLineList = prescriptionLineRepository.findAllByMedicationCode(dto.getMedicationCode());
		if (!prescriptionLineList.isEmpty()) {
			for (PrescriptionLine prescriptionLine : prescriptionLineList) {
				processDataRecord(dto, prescriptionLine);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvMedicationExistDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvMedicationExistDTO dto, PrescriptionLine prescriptionLine) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		//switchSUB 41 SUB    
		//switchBLK 1000011 BLK ACT
		//functionCall 1000012 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvMedicationExistDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		//switchSUB 52 SUB    
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvMedicationExistDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		//switchSUB 61 SUB    
		//switchBLK 1000017 BLK ACT
		//functionCall 1000018 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
        return NO_ACTION;
    }
}
