package com.hospital.file.prescriptionline;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.hospital.file.prescriptionline.dspfilmediaction.DspfilMediactionGDO;

/**
 * Custom Spring Data JPA repository interface for model: Prescription Line (TSAICPP).
 *
 * @author X2EGenerator
 */
@Repository
public interface PrescriptionLineRepositoryCustom {

	/**
	 * 
	 * @param doctorCode Doctor Code
	 * @param prescriptionCode Prescription Code
	 * @param prescriptionDate Prescription Date
	 * @param prescriptionTime Prescription Time
	 * @param prescriptionLineNumber Prescription Line Number
	 */
	void deletePrescriptionLine(String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, long prescriptionLineNumber);

	/**
	 * 
	 * @param doctorCode Doctor Code
	 * @param prescriptionCode Prescription Code
	 * @param prescriptionDate Prescription Date
	 * @param prescriptionTime Prescription Time
	 * @param pageable a Pageable object used for pagination
	 * @return Page of DspfilMediactionGDO
	 */
	RestResponsePage<DspfilMediactionGDO> dspfilMediaction(String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, Pageable pageable);

	/**
	 * 
	 * @param medicationCode Medication Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of PrescriptionLine
	 */
	RestResponsePage<PrescriptionLine> rtvMedicationExist(String medicationCode, Pageable pageable);

	/**
	 * 
	 * @param doctorCode Doctor Code
	 * @param prescriptionCode Prescription Code
	 * @param prescriptionDate Prescription Date
	 * @param prescriptionTime Prescription Time
	 * @param pageable a Pageable object used for pagination
	 * @return Page of PrescriptionLine
	 */
	RestResponsePage<PrescriptionLine> rtvNumberOfLines(String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, Pageable pageable);
}
