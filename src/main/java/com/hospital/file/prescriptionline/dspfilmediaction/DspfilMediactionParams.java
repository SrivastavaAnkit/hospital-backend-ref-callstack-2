package com.hospital.file.prescriptionline.dspfilmediaction;

import java.time.LocalDate;
import java.time.LocalTime;

import com.hospital.config.LocalDateConverter;
import com.hospital.config.LocalTimeConverter;

	
/**
 * Params for resource: DspfilMediaction (TSA6DFR).
 *
 * @author X2EGenerator
 */
public class DspfilMediactionParams {
	private static final long serialVersionUID = 799341313020806207L;
 
	private String doctorCode = "";
	private String prescriptionCode = "";
	private LocalDate prescriptionDate = null;
	private LocalTime prescriptionTime = null;

		
	public String getDoctorCode() {
		return doctorCode;
	}
	
	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}
	
	public String getPrescriptionCode() {
		return prescriptionCode;
	}
	
	public void setPrescriptionCode(String prescriptionCode) {
		this.prescriptionCode = prescriptionCode;
	}
	
	public LocalDate getPrescriptionDate() {
		return prescriptionDate;
	}
	
	public void setPrescriptionDate(LocalDate prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
	}
	
	public void setPrescriptionDate(String prescriptionDate) {
		setPrescriptionDate(new LocalDateConverter().convert(prescriptionDate));
	}
	
	public LocalTime getPrescriptionTime() {
		return prescriptionTime;
	}
	
	public void setPrescriptionTime(LocalTime prescriptionTime) {
		this.prescriptionTime = prescriptionTime;
	}
	
	public void setPrescriptionTime(String prescriptionTime) {
		setPrescriptionTime(new LocalTimeConverter().convert(prescriptionTime));
	}
 
}