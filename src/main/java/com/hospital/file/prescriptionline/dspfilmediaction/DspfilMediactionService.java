package com.hospital.file.prescriptionline.dspfilmediaction;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
 
import com.hospital.support.JobContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.file.prescriptionline.PrescriptionLineRepository;
import com.hospital.file.medication.rtvdetail.RtvDetailService;
import com.hospital.file.medication.rtvdetail.RtvDetailDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.common.exception.ServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

	
/**
 * Service implementation for resource: DspfilMediaction (TSA6DFR).
 *
 * @author X2EGenerator
 */
@Service
public class DspfilMediactionService extends AbstractService<DspfilMediactionService, DspfilMediactionState> {
    
	@Autowired
	private JobContext job;

    @Autowired
    private PrescriptionLineRepository prescriptionLineRepository;
    
    @Autowired
    private RtvDetailService rtvDetailService;
    

    @Autowired
    private MessageSource messageSource;
    
	public static final String SCREEN_CTL = "dspfilMediaction";
    public static final String SCREEN_RCD = "DspfilMediaction.rcd";

    private final Step execute = define("execute", DspfilMediactionParams.class, this::execute);
    private final Step response = define("response", DspfilMediactionDTO.class, this::processResponse);
	//private final Step serviceRtvDetail = define("serviceRtvDetail",RtvDetailParams.class, this::processServiceRtvDetail);
	//private final Step serviceRtvcnd = define("serviceRtvcnd",RtvcndParams.class, this::processServiceRtvcnd);
	//private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
	//private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
	
    
    @Autowired
    public DspfilMediactionService() {
        super(DspfilMediactionService.class, DspfilMediactionState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * DSPFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(DspfilMediactionState state, DspfilMediactionParams params) {
    	StepResult result = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        result = usrInitializeProgram(state);

        result = mainLoop(state, params);

        return result;
    }

    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult mainLoop(DspfilMediactionState state, DspfilMediactionParams params) {
        StepResult result = NO_ACTION;

        result = loadFirstSubfilePage(state, params);

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        result = conductScreenConversation(state);

        return result;
    }

    /**
     * SCREEN  initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult loadFirstSubfilePage(DspfilMediactionState state, DspfilMediactionParams params) {
    	StepResult result = NO_ACTION;

    	result = usrInitializeSubfileControl(state, params);

		dbfReadFirstDataRecord(state);
		if (state.getPageDto() != null && state.getPageDto().getSize() > 0) {
		    result = loadNextSubfilePage(state);
		}

        return result;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     * @return
     */
    private StepResult loadNextSubfilePage(DspfilMediactionState state) {
    	StepResult result = NO_ACTION;

    	List<DspfilMediactionGDO> list = state.getPageDto().getContent();
        for (DspfilMediactionGDO gdo : list) {
            gdo.set_SysRecordSelected(RecordSelectedEnum._STA_YES);
            result = usrInitializeSubfileRecordFromDBFRecord(state, gdo);
//             TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
            if (gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
//                TODO:writeSubfileRecord(state);  // synon built-in function
            }
        }

        return result;
    }

    /**
     * SCREEN  initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductScreenConversation(DspfilMediactionState state) {
        StepResult result = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            DspfilMediactionDTO model = new DspfilMediactionDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_CTL, model).thenCall(response);
        }

        return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processResponse(DspfilMediactionState state, DspfilMediactionDTO model) {
    	StepResult result = NO_ACTION;

        // update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            result = closedown(state);
        } else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
        } else if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            //TODO:processHelpRequest(state);//synon built-in function
        }
        else if (CmdKeyEnum.isNextPage(state.get_SysCmdKey())) {
            dbfReadNextPageRecord(state);
            result = loadNextSubfilePage(state);
        } else {
            result = processScreen(state);
        }

        return result;
    }

    /**
     * SCREEN process screen.
     * @param state - Service state class.
     * @return
     */
    private StepResult processScreen(DspfilMediactionState state) {
    	StepResult result = NO_ACTION;

        result = usrProcessSubfilePreConfirm(state);
        if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
        	return closedown(state);
        } else {
        	if(!state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
        		return closedown(state);
	        } else {
//	        	if(!state.get_SysProgramConfirm().getCode().equals(ProgramConfirmEnum._STA_YES.getCode())) {
//	        		return closedown(state);
//		        } else {
//		        	result = usrProcessCommandKeys(state);
//		        }

	        	result = usrProcessSubfileControlPostConfirm(state);
	        	for (DspfilMediactionGDO gdo : state.getPageDto().getContent()) {
	                if(gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
	                	result = usrProcessSubfileRecordPostConfirm(state, gdo);
//	                  TODO:writeSubfileRecord(state);   // synon built-in function
	                }
	            }
	        	result = usrFinalProcessingPostConfirm(state);
	        	result = usrProcessCommandKeys(state);
	        }
        }

        result = conductScreenConversation(state);

        return result;
    }

    /**
     * SCREEN Process subfile Pre Confirm.
     * @param state - Service state class.
     * @return
     */
    private StepResult usrProcessSubfilePreConfirm(DspfilMediactionState state) {
    	StepResult result = NO_ACTION;

    	result = usrSubfileControlFunctionFields(state);
    	result = usrProcessSubfileControlPreConfirm(state);
    	if(state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
    		for (DspfilMediactionGDO gdo : state.getPageDto().getContent()) {
                if(gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
                	result = usrSubfileRecordFunctionFields(state, gdo);
                    result = usrProcessSubfileRecordPreConfirm(state, gdo);
//                  TODO:writeSubfileRecord(state);   // synon built-in function
                }
            }
    	}

        result = usrFinalProcessingPreConfirm(state);

        return result;
    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(DspfilMediactionState state) {
        StepResult result = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        result = usrExitProgramProcessing(state);

        return result;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(DspfilMediactionState state) {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadNextPageRecord(DspfilMediactionState state)
	{
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    private void dbfReadDataRecord(DspfilMediactionState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = PageRequest.of(state.getPage(), state.getSize());
        }
        else {
            pageable = PageRequest.of(state.getPage(), state.getSize(), Sort.by(sortOrders));
        }

        RestResponsePage<DspfilMediactionGDO> pageDto = prescriptionLineRepository.dspfilMediaction(state.getDoctorCode(), state.getPrescriptionCode(), state.getPrescriptionDate(), state.getPrescriptionTime(), pageable);
        state.setPageDto(pageDto);
    }
    
	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(DspfilMediactionState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 20 SUB    
			// Unprocessed SUB 20 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Control (Generated:182)
	 */
    private StepResult usrInitializeSubfileControl(DspfilMediactionState dto, DspfilMediactionParams params) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 182 SUB    
			// Unprocessed SUB 182 -
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrInitializeSubfileRecordFromDBFRecord(DspfilMediactionState dto, DspfilMediactionGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	RtvDetailDTO rtvDetailDTO;
			//switchSUB 41 SUB    
			//switchBLK 1000001 BLK ACT
			// DEBUG genFunctionCall 1000002 ACT RTV Detail - Medication  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvDetailDTO = new RtvDetailDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvDetailDTO.setMedicationCode(gdo.getMedicationCode());
			// DEBUG genFunctionCall Service call
			rtvDetailService.execute(rtvDetailDTO);
			// DEBUG genFunctionCall Parameters OUT
			gdo.setMedicationDescription(rtvDetailDTO.getMedicationDescription());
			gdo.setMedicationUnit(rtvDetailDTO.getMedicationUnit());
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000006 BLK ACT
			// DEBUG genFunctionCall 1000007 ACT RCD.Medication unit value = Condition name of RCD.Medication Unit
			gdo.setMedicationUnitValue(gdo.getMedicationUnit().getDescription()); // Retrieve condition
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:175)
	 */
    private StepResult usrSubfileControlFunctionFields(DspfilMediactionState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 175 SUB    
			// Unprocessed SUB 175 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Control (Pre-Confirm) (Generated:72)
	 */
    private StepResult usrProcessSubfileControlPreConfirm(DspfilMediactionState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 72 SUB    
			//switchBLK 1000012 BLK CAS
			//switchSUB 1000012 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// CTL.*CMD key is *Cancel
				//switchBLK 1000024 BLK ACT
				// DEBUG genFunctionCall 1000025 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000015 BLK ACT
				// DEBUG genFunctionCall 1000016 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:170)
	 */
    private StepResult usrSubfileRecordFunctionFields(DspfilMediactionState dto, DspfilMediactionGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 170 SUB    
			// Unprocessed SUB 170 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Record (Pre-Confirm) (Generated:101)
	 */
    private StepResult usrProcessSubfileRecordPreConfirm(DspfilMediactionState dto, DspfilMediactionGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 101 SUB    
			// Unprocessed SUB 101 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Final processing (Pre-confirm) (Generated:222)
	 */
    private StepResult usrFinalProcessingPreConfirm(DspfilMediactionState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 222 SUB    
			// Unprocessed SUB 222 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process subfile control (Post-confirm) (Generated:225)
	 */
    private StepResult usrProcessSubfileControlPostConfirm(DspfilMediactionState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 225 SUB    
			// Unprocessed SUB 225 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Record (Post-Confirm Pass) (Generated:209)
	 */
    private StepResult usrProcessSubfileRecordPostConfirm(DspfilMediactionState dto, DspfilMediactionGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 209 SUB    
			// Unprocessed SUB 209 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Final processing (Post-confirm) (Generated:228)
	 */
    private StepResult usrFinalProcessingPostConfirm(DspfilMediactionState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 228 SUB    
			// Unprocessed SUB 228 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Generated:140)
	 */
    private StepResult usrProcessCommandKeys(DspfilMediactionState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 140 SUB    
			// Unprocessed SUB 140 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(DspfilMediactionState dto) {
        StepResult result = NO_ACTION;
        
        try {
        	//switchSUB 132 SUB    
			//switchBLK 1000018 BLK CAS
			//switchSUB 1000018 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// CTL.*CMD key is *Exit
				//switchBLK 1000031 BLK ACT
				// DEBUG genFunctionCall 1000032 ACT PAR.*Return code = CND.*Normal
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000021 BLK ACT
				// DEBUG genFunctionCall 1000022 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * RtvDetailService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvDetail(DspfilMediactionState state, RtvDetailParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvcndService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvcnd(DspfilMediactionState state, RtvcndParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(DspfilMediactionState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(DspfilMediactionState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//


}
