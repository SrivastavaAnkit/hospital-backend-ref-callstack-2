package com.hospital.file.prescriptionline.dspfilmediaction;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.hospital.model.ReloadSubfileEnum;

/**
 * State for file 'Prescription Line' (TSAICPP) and function 'Dspfil Mediaction' (TSA6DFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class DspfilMediactionState extends DspfilMediactionDTO {
	private static final long serialVersionUID = -3254139752584034464L;


	// System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;


    public DspfilMediactionState() {

    }



    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }
    }