package com.hospital.file.prescriptionline.dspfilmediaction;
 
import java.math.BigDecimal;
  
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.common.utils.RestResponsePage;

import com.hospital.file.prescriptionline.PrescriptionLine;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.common.state.BaseDTO;

/**
 * Dto for file 'Prescription Line' (TSAICPP) and function 'Dspfil Mediaction' (TSA6DFR).
 */
public class DspfilMediactionDTO extends BaseDTO {
	private static final long serialVersionUID = -1065968709837829744L;

    private RestResponsePage<DspfilMediactionGDO> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";

	private String doctorCode = "";
	private String prescriptionCode = "";
	private LocalDate prescriptionDate = null;
	private LocalTime prescriptionTime = null;
	private long prescriptionLineNumber = 0L;
	private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");


	private DspfilMediactionGDO gdo;

    public DspfilMediactionDTO() {

    }

	public DspfilMediactionDTO(String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime) {
		this.doctorCode = doctorCode;
		this.prescriptionCode = prescriptionCode;
		this.prescriptionDate = prescriptionDate;
		this.prescriptionTime = prescriptionTime;
	}

    public void setPageDto(RestResponsePage<DspfilMediactionGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<DspfilMediactionGDO> getPageDto() {
		return pageDto;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
    }

    public String getDoctorCode() {
    	return doctorCode;
    }

	public void setPrescriptionCode(String prescriptionCode) {
		this.prescriptionCode = prescriptionCode;
    }

    public String getPrescriptionCode() {
    	return prescriptionCode;
    }

	public void setPrescriptionDate(LocalDate prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
    }

    public LocalDate getPrescriptionDate() {
    	return prescriptionDate;
    }

	public void setPrescriptionTime(LocalTime prescriptionTime) {
		this.prescriptionTime = prescriptionTime;
    }

    public LocalTime getPrescriptionTime() {
    	return prescriptionTime;
    }

	public void setPrescriptionLineNumber(long prescriptionLineNumber) {
		this.prescriptionLineNumber = prescriptionLineNumber;
    }

    public long getPrescriptionLineNumber() {
    	return prescriptionLineNumber;
    }

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
    }

    public ReturnCodeEnum getReturnCode() {
    	return returnCode;
    }

	public void setGdo(DspfilMediactionGDO gdo) {
		this.gdo = gdo;
	}

	public DspfilMediactionGDO getGdo() {
		return gdo;
	}

}