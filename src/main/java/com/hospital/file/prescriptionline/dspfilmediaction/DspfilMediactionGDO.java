package com.hospital.file.prescriptionline.dspfilmediaction;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.file.prescriptionline.PrescriptionLine;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;

/**
 * Gdo for file 'Prescription Line' (TSAICPP) and function 'Dspfil Mediaction' (TSA6DFR).
 */
public class DspfilMediactionGDO implements Serializable {
	private static final long serialVersionUID = -2849484105315431707L;

    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private String doctorCode = "";
	private String prescriptionCode = "";
	private LocalDate prescriptionDate = null;
	private LocalTime prescriptionTime = null;
	private long prescriptionLineNumber = 0L;
	private String medicationCode = "";
	private long prescriptionQuantity = 0L;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;
	private String medicationDescription = "";
	private MedicationUnitEnum medicationUnit = null;
	private String medicationUnitValue = "";

	public DspfilMediactionGDO() {

	}

   	public DspfilMediactionGDO(String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, long prescriptionLineNumber, String medicationCode, long prescriptionQuantity, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime) {
		this.doctorCode = doctorCode;
		this.prescriptionCode = prescriptionCode;
		this.prescriptionDate = prescriptionDate;
		this.prescriptionTime = prescriptionTime;
		this.prescriptionLineNumber = prescriptionLineNumber;
		this.medicationCode = medicationCode;
		this.prescriptionQuantity = prescriptionQuantity;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setDoctorCode(String doctorCode) {
    	this.doctorCode = doctorCode;
    }

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setPrescriptionCode(String prescriptionCode) {
    	this.prescriptionCode = prescriptionCode;
    }

	public String getPrescriptionCode() {
		return prescriptionCode;
	}

	public void setPrescriptionDate(LocalDate prescriptionDate) {
    	this.prescriptionDate = prescriptionDate;
    }

	public LocalDate getPrescriptionDate() {
		return prescriptionDate;
	}

	public void setPrescriptionTime(LocalTime prescriptionTime) {
    	this.prescriptionTime = prescriptionTime;
    }

	public LocalTime getPrescriptionTime() {
		return prescriptionTime;
	}

	public void setPrescriptionLineNumber(long prescriptionLineNumber) {
    	this.prescriptionLineNumber = prescriptionLineNumber;
    }

	public long getPrescriptionLineNumber() {
		return prescriptionLineNumber;
	}

	public void setMedicationCode(String medicationCode) {
    	this.medicationCode = medicationCode;
    }

	public String getMedicationCode() {
		return medicationCode;
	}

	public void setPrescriptionQuantity(long prescriptionQuantity) {
    	this.prescriptionQuantity = prescriptionQuantity;
    }

	public long getPrescriptionQuantity() {
		return prescriptionQuantity;
	}

	public void setAddedUser(String addedUser) {
    	this.addedUser = addedUser;
    }

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
    	this.addedDate = addedDate;
    }

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
    	this.addedTime = addedTime;
    }

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
    	this.changedUser = changedUser;
    }

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
    	this.changedDate = changedDate;
    }

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
    	this.changedTime = changedTime;
    }

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void setMedicationDescription(String medicationDescription) {
    	this.medicationDescription = medicationDescription;
    }

	public String getMedicationDescription() {
		return medicationDescription;
	}

	public void setMedicationUnit(MedicationUnitEnum medicationUnit) {
    	this.medicationUnit = medicationUnit;
    }

	public MedicationUnitEnum getMedicationUnit() {
		return medicationUnit;
	}

	public void setMedicationUnitValue(String medicationUnitValue) {
    	this.medicationUnitValue = medicationUnitValue;
    }

	public String getMedicationUnitValue() {
		return medicationUnitValue;
	}

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}