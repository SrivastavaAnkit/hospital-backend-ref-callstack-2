package com.hospital.file.prescriptionline;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;

import com.hospital.file.prescriptionline.PrescriptionLine;
import com.hospital.file.prescriptionline.dspfilmediaction.DspfilMediactionGDO;

/**
 * Custom Spring Data JPA repository implementation for model: Prescription Line (TSAICPP).
 *
 * @author X2EGenerator
 */
@Repository
public class PrescriptionLineRepositoryImpl implements PrescriptionLineRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see com.hospital.file.prescriptionline.PrescriptionLineService#deletePrescriptionLine(PrescriptionLine)
	 */
	@Override
	public void deletePrescriptionLine(
		String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, long prescriptionLineNumber) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " prescriptionLine.id.doctorCode = :doctorCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(prescriptionCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescriptionLine.id.prescriptionCode = :prescriptionCode";
			isParamSet = true;
		}

		if (prescriptionDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescriptionLine.id.prescriptionDate = :prescriptionDate";
			isParamSet = true;
		}

		if (prescriptionTime != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescriptionLine.id.prescriptionTime = :prescriptionTime";
			isParamSet = true;
		}

		if (prescriptionLineNumber > 0) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescriptionLine.id.prescriptionLineNumber = :prescriptionLineNumber";
			isParamSet = true;
		}

		String sqlString = "DELETE FROM PrescriptionLine prescriptionLine";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			query.setParameter("doctorCode", doctorCode);
		}

		if (StringUtils.isNotEmpty(prescriptionCode)) {
			query.setParameter("prescriptionCode", prescriptionCode);
		}

		if (prescriptionDate != null) {
			query.setParameter("prescriptionDate", prescriptionDate);
		}

		if (prescriptionTime != null) {
			query.setParameter("prescriptionTime", prescriptionTime);
		}

		if (prescriptionLineNumber > 0) {
			query.setParameter("prescriptionLineNumber", prescriptionLineNumber);
		}

		em.createNativeQuery("SET foreign_key_checks = 0").executeUpdate();
		query.executeUpdate();
		em.createNativeQuery("SET foreign_key_checks = 1").executeUpdate();
	}

	@Override
	public RestResponsePage<DspfilMediactionGDO> dspfilMediaction(
		String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " prescriptionLine.id.doctorCode like CONCAT('%', :doctorCode, '%')";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(prescriptionCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescriptionLine.id.prescriptionCode like CONCAT('%', :prescriptionCode, '%')";
			isParamSet = true;
		}

		if (prescriptionDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescriptionLine.id.prescriptionDate = :prescriptionDate";
			isParamSet = true;
		}

		if (prescriptionTime != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescriptionLine.id.prescriptionTime = :prescriptionTime";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.prescriptionline.dspfilmediaction.DspfilMediactionGDO(prescriptionLine.id.doctorCode, prescriptionLine.id.prescriptionCode, prescriptionLine.id.prescriptionDate, prescriptionLine.id.prescriptionTime, prescriptionLine.id.prescriptionLineNumber, prescriptionLine.medicationCode, prescriptionLine.prescriptionQuantity, prescriptionLine.addedUser, prescriptionLine.addedDate, prescriptionLine.addedTime, prescriptionLine.changedUser, prescriptionLine.changedDate, prescriptionLine.changedTime) from PrescriptionLine prescriptionLine";
		String countQueryString = "SELECT COUNT(prescriptionLine.id.doctorCode) FROM PrescriptionLine prescriptionLine";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			PrescriptionLine prescriptionLine = new PrescriptionLine();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(prescriptionLine.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"prescriptionLine.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"prescriptionLine." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescriptionLine.id.prescriptionCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescriptionLine.id.prescriptionDate"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescriptionLine.id.prescriptionTime"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescriptionLine.id.prescriptionLineNumber"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			countQuery.setParameter("doctorCode", doctorCode);
			query.setParameter("doctorCode", doctorCode);
		}

		if (StringUtils.isNotEmpty(prescriptionCode)) {
			countQuery.setParameter("prescriptionCode", prescriptionCode);
			query.setParameter("prescriptionCode", prescriptionCode);
		}

		if (prescriptionDate != null) {
			countQuery.setParameter("prescriptionDate", prescriptionDate);
			query.setParameter("prescriptionDate", prescriptionDate);
		}

		if (prescriptionTime != null) {
			countQuery.setParameter("prescriptionTime", prescriptionTime);
			query.setParameter("prescriptionTime", prescriptionTime);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<DspfilMediactionGDO> content = query.getResultList();
		RestResponsePage<DspfilMediactionGDO> pageDto = new RestResponsePage<DspfilMediactionGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.prescriptionline.PrescriptionLineService#rtvMedicationExist(String)
	 */
	@Override
	public RestResponsePage<PrescriptionLine> rtvMedicationExist(
		String medicationCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(medicationCode)) {
			whereClause += " prescriptionLine.medicationCode = :medicationCode";
			isParamSet = true;
		}

		String sqlString = "SELECT prescriptionLine FROM PrescriptionLine prescriptionLine";
		String countQueryString = "SELECT COUNT(prescriptionLine.id.doctorCode) FROM PrescriptionLine prescriptionLine";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(medicationCode)) {
			countQuery.setParameter("medicationCode", medicationCode);
			query.setParameter("medicationCode", medicationCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<PrescriptionLine> content = query.getResultList();
		RestResponsePage<PrescriptionLine> pageDto = new RestResponsePage<PrescriptionLine>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.prescriptionline.PrescriptionLineService#rtvNumberOfLines(String, String, LocalDate, LocalTime)
	 */
	@Override
	public RestResponsePage<PrescriptionLine> rtvNumberOfLines(
		String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " prescriptionLine.id.doctorCode = :doctorCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(prescriptionCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescriptionLine.id.prescriptionCode = :prescriptionCode";
			isParamSet = true;
		}

		if (prescriptionDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescriptionLine.id.prescriptionDate = :prescriptionDate";
			isParamSet = true;
		}

		if (prescriptionTime != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescriptionLine.id.prescriptionTime = :prescriptionTime";
			isParamSet = true;
		}

		String sqlString = "SELECT prescriptionLine FROM PrescriptionLine prescriptionLine";
		String countQueryString = "SELECT COUNT(prescriptionLine.id.doctorCode) FROM PrescriptionLine prescriptionLine";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			countQuery.setParameter("doctorCode", doctorCode);
			query.setParameter("doctorCode", doctorCode);
		}

		if (StringUtils.isNotEmpty(prescriptionCode)) {
			countQuery.setParameter("prescriptionCode", prescriptionCode);
			query.setParameter("prescriptionCode", prescriptionCode);
		}

		if (prescriptionDate != null) {
			countQuery.setParameter("prescriptionDate", prescriptionDate);
			query.setParameter("prescriptionDate", prescriptionDate);
		}

		if (prescriptionTime != null) {
			countQuery.setParameter("prescriptionTime", prescriptionTime);
			query.setParameter("prescriptionTime", prescriptionTime);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<PrescriptionLine> content = query.getResultList();
		RestResponsePage<PrescriptionLine> pageDto = new RestResponsePage<PrescriptionLine>(
				content, pageable, count.longValue());

		return pageDto;
	}

}
