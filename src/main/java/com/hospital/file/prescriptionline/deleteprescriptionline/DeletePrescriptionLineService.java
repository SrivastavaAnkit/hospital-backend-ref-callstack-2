package com.hospital.file.prescriptionline.deleteprescriptionline;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.prescriptionline.PrescriptionLine;
import com.hospital.file.prescriptionline.PrescriptionLineId;
import com.hospital.file.prescriptionline.PrescriptionLineRepository;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class DeletePrescriptionLineService  extends AbstractService<DeletePrescriptionLineService, DeletePrescriptionLineDTO>
{
    private final Step execute = define("execute", DeletePrescriptionLineDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PrescriptionLineRepository prescriptionLineRepository;
	

    @Autowired
    public DeletePrescriptionLineService() {
        super(DeletePrescriptionLineService.class, DeletePrescriptionLineDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(DeletePrescriptionLineDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(DeletePrescriptionLineDTO dto, DeletePrescriptionLineDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataUpdate(dto);
		PrescriptionLineId prescriptionLineId = new PrescriptionLineId();
		prescriptionLineId.setDoctorCode(dto.getDoctorCode());
		prescriptionLineId.setPrescriptionCode(dto.getPrescriptionCode());
		prescriptionLineId.setPrescriptionDate(dto.getPrescriptionDate());
		prescriptionLineId.setPrescriptionTime(dto.getPrescriptionTime());
		prescriptionLineId.setPrescriptionLineNumber(dto.getPrescriptionLineNumber());
		try {
			prescriptionLineRepository.deleteById(prescriptionLineId);
			prescriptionLineRepository.flush();
			dto.setReturnCode(ReturnCodeEnum.fromCode(""));
			processingAfterDataUpdate(dto);
		} catch (Exception e) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
		}

       return result;
	}

	private StepResult processingBeforeDataUpdate(DeletePrescriptionLineDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Empty:9)
		 */
		
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(DeletePrescriptionLineDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Empty:17)
		 */
		
       return NO_ACTION;
	}
}
