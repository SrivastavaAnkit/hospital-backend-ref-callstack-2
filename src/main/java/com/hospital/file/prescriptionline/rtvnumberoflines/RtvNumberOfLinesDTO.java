package com.hospital.file.prescriptionline.rtvnumberoflines;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvNumberOfLinesDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private LocalDate prescriptionDate;
	private LocalTime prescriptionTime;
	private ReturnCodeEnum returnCode;
	private String doctorCode;
	private String prescriptionCode;
	private long usrNbrOfLines;
	private String nextScreen;
	private long lclUsrNbrOfLines;

	public String getDoctorCode() {
		return doctorCode;
	}

	public String getPrescriptionCode() {
		return prescriptionCode;
	}

	public LocalDate getPrescriptionDate() {
		return prescriptionDate;
	}

	public LocalTime getPrescriptionTime() {
		return prescriptionTime;
	}

	public long getUsrNbrOfLines() {
		return usrNbrOfLines;
	}

	public long getLclUsrNbrOfLines() {
		return lclUsrNbrOfLines;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public void setPrescriptionCode(String prescriptionCode) {
		this.prescriptionCode = prescriptionCode;
	}

	public void setPrescriptionDate(LocalDate prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
	}

	public void setPrescriptionTime(LocalTime prescriptionTime) {
		this.prescriptionTime = prescriptionTime;
	}

	public void setUsrNbrOfLines(long usrNbrOfLines) {
		this.usrNbrOfLines = usrNbrOfLines;
	}

	public void setLclUsrNbrOfLines(long lclUsrNbrOfLines) {
		this.lclUsrNbrOfLines = lclUsrNbrOfLines;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
