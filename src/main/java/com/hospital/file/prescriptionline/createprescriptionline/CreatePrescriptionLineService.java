package com.hospital.file.prescriptionline.createprescriptionline;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.prescriptionline.PrescriptionLine;
import com.hospital.file.prescriptionline.PrescriptionLineId;
import com.hospital.file.prescriptionline.PrescriptionLineRepository;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class CreatePrescriptionLineService  extends AbstractService<CreatePrescriptionLineService, CreatePrescriptionLineDTO>
{
    private final Step execute = define("execute", CreatePrescriptionLineDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PrescriptionLineRepository prescriptionLineRepository;
	

    @Autowired
    public CreatePrescriptionLineService() {
        super(CreatePrescriptionLineService.class, CreatePrescriptionLineDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(CreatePrescriptionLineDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(CreatePrescriptionLineDTO dto, CreatePrescriptionLineDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		PrescriptionLine prescriptionLine = new PrescriptionLine();
		prescriptionLine.setDoctorCode(dto.getDoctorCode());
		prescriptionLine.setPrescriptionCode(dto.getPrescriptionCode());
		prescriptionLine.setPrescriptionDate(dto.getPrescriptionDate());
		prescriptionLine.setPrescriptionTime(dto.getPrescriptionTime());
		prescriptionLine.setPrescriptionLineNumber(dto.getPrescriptionLineNumber());
		prescriptionLine.setMedicationCode(dto.getMedicationCode());
		prescriptionLine.setPrescriptionQuantity(dto.getPrescriptionQuantity());
		/*prescriptionLine.setAddedUser(dto.getAddedUser());
		prescriptionLine.setAddedDate(dto.getAddedDate());
		prescriptionLine.setAddedTime(dto.getAddedTime());
		prescriptionLine.setChangedUser(dto.getChangedUser());
		prescriptionLine.setChangedDate(dto.getChangedDate());
		prescriptionLine.setChangedTime(dto.getChangedTime());*/

		processingBeforeDataUpdate(dto, prescriptionLine);

		PrescriptionLine prescriptionLine2 = prescriptionLineRepository.findById(prescriptionLine.getId()).get();
		if (prescriptionLine2 != null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0003"));
			processingIfDataRecordAlreadyExists(dto, prescriptionLine2);
		}
		else {
            try {
				prescriptionLineRepository.save(prescriptionLine);
                dto.setReturnCode(ReturnCodeEnum.fromCode(""));
                processingAfterDataUpdate(dto, prescriptionLine);
            } catch (Exception e) {
                dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
                processingIfDataUpdateError(dto, prescriptionLine);
            }
        }

        return result;
	}

    private StepResult processingBeforeDataUpdate(CreatePrescriptionLineDTO dto, PrescriptionLine prescriptionLine) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000017 BLK ACT
		//functionCall 1000018 ACT DB1.Added User = JOB.*USER
		/*prescriptionLine.setAddedUser(job.getUser());
		//switchBLK 1000021 BLK ACT
		//functionCall 1000022 ACT DB1.Added Date = JOB.*Job date
		prescriptionLine.setAddedDate(LocalDate.now());
		//switchBLK 1000025 BLK ACT
		//functionCall 1000026 ACT DB1.Added Time = JOB.*Job time
		prescriptionLine.setAddedTime(LocalTime.now());*/
		//switchBLK 1000029 BLK TXT
		// 
		//switchBLK 1000003 BLK ACT
		//functionCall 1000004 ACT DB1.Prescription Line Number = PAR.Prescription Line Number + CON.1
		prescriptionLine.setPrescriptionLineNumber(dto.getPrescriptionLineNumber() + 1);
       return NO_ACTION;
    }

    private StepResult processingIfDataRecordAlreadyExists(CreatePrescriptionLineDTO dto, PrescriptionLine prescriptionLine) throws ServiceException {
		/**
		 * USER: Processing if Data Record Already Exists (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000030 BLK ACT
		//functionCall 1000031 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
    }

    private StepResult processingAfterDataUpdate(CreatePrescriptionLineDTO dto, PrescriptionLine prescriptionLine) throws ServiceException {
		/**
		 * USER: Processing after Data Update (Generated:11)
		 */
		//switchSUB 11 SUB    
		//switchBLK 1000011 BLK ACT
		//functionCall 1000012 ACT PAR.Prescription Line Number = DB1.Prescription Line Number
		dto.setPrescriptionLineNumber(prescriptionLine.getPrescriptionLineNumber());
		//switchBLK 1000044 BLK ACT
		//functionCall 1000045 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
    }

    private StepResult processingIfDataUpdateError(CreatePrescriptionLineDTO dto, PrescriptionLine prescriptionLine) throws ServiceException {
		/**
		 * Processing if Data Update Error (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000036 BLK ACT
		//functionCall 1000037 ACT PAR.USR Return Code = CND.Error
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("E"));
       return NO_ACTION;
    }
}
