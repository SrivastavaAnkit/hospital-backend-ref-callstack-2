package com.hospital.file.prescriptionline.changeprescriptionline;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ChangePrescriptionLineDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private LocalDate addedDate;
	private LocalDate changedDate;
	private LocalDate prescriptionDate;
	private LocalTime addedTime;
	private LocalTime changedTime;
	private LocalTime prescriptionTime;
	private ReturnCodeEnum returnCode;
	private String addedUser;
	private String changedUser;
	private String doctorCode;
	private String medicationCode;
	private String prescriptionCode;
	private UsrReturnCodeEnum usrReturnCode;
	private long prescriptionLineNumber;
	private long prescriptionQuantity;
	private String nextScreen;
	private String lclMedicationDescription;

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public String getMedicationCode() {
		return medicationCode;
	}

	public String getPrescriptionCode() {
		return prescriptionCode;
	}

	public LocalDate getPrescriptionDate() {
		return prescriptionDate;
	}

	public long getPrescriptionLineNumber() {
		return prescriptionLineNumber;
	}

	public long getPrescriptionQuantity() {
		return prescriptionQuantity;
	}

	public LocalTime getPrescriptionTime() {
		return prescriptionTime;
	}

	public UsrReturnCodeEnum getUsrReturnCode() {
		return usrReturnCode;
	}

	public String getLclMedicationDescription() {
		return lclMedicationDescription;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public void setMedicationCode(String medicationCode) {
		this.medicationCode = medicationCode;
	}

	public void setPrescriptionCode(String prescriptionCode) {
		this.prescriptionCode = prescriptionCode;
	}

	public void setPrescriptionDate(LocalDate prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
	}

	public void setPrescriptionLineNumber(long prescriptionLineNumber) {
		this.prescriptionLineNumber = prescriptionLineNumber;
	}

	public void setPrescriptionQuantity(long prescriptionQuantity) {
		this.prescriptionQuantity = prescriptionQuantity;
	}

	public void setPrescriptionTime(LocalTime prescriptionTime) {
		this.prescriptionTime = prescriptionTime;
	}

	public void setUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
		this.usrReturnCode = usrReturnCode;
	}

	public void setLclMedicationDescription(String lclMedicationDescription) {
		this.lclMedicationDescription = lclMedicationDescription;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
