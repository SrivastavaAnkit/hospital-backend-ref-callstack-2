package com.hospital.file.prescriptionline.changeprescriptionline;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.prescriptionline.PrescriptionLine;
import com.hospital.file.prescriptionline.PrescriptionLineId;
import com.hospital.file.prescriptionline.PrescriptionLineRepository;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class ChangePrescriptionLineService extends AbstractService<ChangePrescriptionLineService, ChangePrescriptionLineDTO>
{
    private final Step execute = define("execute", ChangePrescriptionLineDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PrescriptionLineRepository prescriptionLineRepository;
	

    @Autowired
    public ChangePrescriptionLineService() {
        super(ChangePrescriptionLineService.class, ChangePrescriptionLineDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(ChangePrescriptionLineDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(ChangePrescriptionLineDTO dto, ChangePrescriptionLineDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);
		PrescriptionLineId prescriptionLineId = new PrescriptionLineId();
		prescriptionLineId.setDoctorCode(dto.getDoctorCode());
		prescriptionLineId.setPrescriptionCode(dto.getPrescriptionCode());
		prescriptionLineId.setPrescriptionDate(dto.getPrescriptionDate());
		prescriptionLineId.setPrescriptionTime(dto.getPrescriptionTime());
		prescriptionLineId.setPrescriptionLineNumber(dto.getPrescriptionLineNumber());
		PrescriptionLine prescriptionLine = prescriptionLineRepository.findById(prescriptionLineId).get();
		if (prescriptionLine == null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0005"));
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, prescriptionLine);
			prescriptionLine.setDoctorCode(dto.getDoctorCode());
			prescriptionLine.setPrescriptionCode(dto.getPrescriptionCode());
			prescriptionLine.setPrescriptionDate(dto.getPrescriptionDate());
			prescriptionLine.setPrescriptionTime(dto.getPrescriptionTime());
			prescriptionLine.setPrescriptionLineNumber(dto.getPrescriptionLineNumber());
			prescriptionLine.setMedicationCode(dto.getMedicationCode());
			prescriptionLine.setPrescriptionQuantity(dto.getPrescriptionQuantity());
			/*prescriptionLine.setAddedUser(dto.getAddedUser());
			prescriptionLine.setAddedDate(dto.getAddedDate());
			prescriptionLine.setAddedTime(dto.getAddedTime());
			prescriptionLine.setChangedUser(dto.getChangedUser());
			prescriptionLine.setChangedDate(dto.getChangedDate());
			prescriptionLine.setChangedTime(dto.getChangedTime());*/
			processingBeforeDataUpdate(dto, prescriptionLine);
			try {
				prescriptionLineRepository.saveAndFlush(prescriptionLine);
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
			}
		}

       return result;
	}

	private StepResult processingBeforeDataRead(ChangePrescriptionLineDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000023 BLK CAS
		//switchSUB 1000023 BLK CAS
		if (dto.getPrescriptionCode().equals("TWOPLD")) {
			// PAR.Prescription Code is Two pills by day
			//switchBLK 1000026 BLK ACT
			//functionCall 1000027 ACT LCL.Medication Description = CON.2 Pills by day
			dto.setLclMedicationDescription("2 Pills by day");
		}
       return NO_ACTION;
	}

	private StepResult processingIfDataRecordNotFound(ChangePrescriptionLineDTO dto) throws ServiceException {
		/**
		 * USER: Processing if Data Record Not Found (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
	}

	private StepResult processingAfterDataRead(ChangePrescriptionLineDTO dto, PrescriptionLine prescriptionLine) throws ServiceException {
		/**
		 * USER: Processing After Data Read (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000032 BLK CAS
		//switchSUB 1000032 BLK CAS
		if (prescriptionLine.getPrescriptionCode().equals("TWOPLD")) {
			// DB1.Prescription Code is Two pills by day
			//switchBLK 1000035 BLK ACT
			//functionCall 1000036 ACT LCL.Medication Description = CON.2 Pills by day
			dto.setLclMedicationDescription("2 Pills by day");
		}
       return NO_ACTION;
	}

	private StepResult processingBeforeDataUpdate(ChangePrescriptionLineDTO dto, PrescriptionLine prescriptionLine) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Generated:10)
		 */
		//switchSUB 10 SUB    
		//switchBLK 1000011 BLK ACT
		//functionCall 1000012 ACT DB1.Changed User = JOB.*USER
	/*	prescriptionLine.setChangedUser(job.getUser());
		//switchBLK 1000015 BLK ACT
		//functionCall 1000016 ACT DB1.Changed Date = JOB.*Job date
		prescriptionLine.setChangedDate(LocalDate.now());
		//switchBLK 1000019 BLK ACT
		//functionCall 1000020 ACT DB1.Changed Time = JOB.*Job time
		prescriptionLine.setChangedTime(LocalTime.now());*/
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(ChangePrescriptionLineDTO dto) throws ServiceException {
		/**
		 * USER: Processing After Data Update (Generated:23)
		 */
		//switchSUB 23 SUB    
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
	}
}
