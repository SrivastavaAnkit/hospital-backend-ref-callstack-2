package com.hospital.file.prescriptionline;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * Spring Data JPA repository interface for model: Prescription Line (TSAICPP).
 *
 * @author X2EGenerator
 */
@Repository
public interface PrescriptionLineRepository extends PrescriptionLineRepositoryCustom, JpaRepository<PrescriptionLine, PrescriptionLineId> {

	List<PrescriptionLine> findAllByIdPrescriptionCodeAndIdPrescriptionDateAndIdPrescriptionTime(String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime);

	List<PrescriptionLine> findAllByMedicationCode(String medicationCode);
}
