package com.hospital.file.patient.changepatient;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.patient.Patient;
import com.hospital.file.patient.PatientId;
import com.hospital.file.patient.PatientRepository;
import com.hospital.model.PatientGenderEnum;
import com.hospital.model.PatientStatusEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class ChangePatientService extends AbstractService<ChangePatientService, ChangePatientDTO>
{
    private final Step execute = define("execute", ChangePatientDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PatientRepository patientRepository;
	

    @Autowired
    public ChangePatientService() {
        super(ChangePatientService.class, ChangePatientDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(ChangePatientDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(ChangePatientDTO dto, ChangePatientDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);
		PatientId patientId = new PatientId();
		patientId.setPatientCode(dto.getPatientCode());
		Patient patient = patientRepository.findById(patientId).get();
		if (patient == null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0005"));
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, patient);
			patient.setPatientCode(dto.getPatientCode());
			patient.setHospitalCode(dto.getHospitalCode());
			patient.setWardCode(dto.getWardCode());
			patient.setPatientName(dto.getPatientName());
			patient.setPatientSurname(dto.getPatientSurname());
			patient.setAddressLine1(dto.getAddressLine1());
			patient.setAddressLine2(dto.getAddressLine2());
			patient.setAddressTownCity(dto.getAddressTownCity());
			patient.setAddressCountry(dto.getAddressCountry());
			patient.setAddressPostalZipCode(dto.getAddressPostalZipCode());
			patient.setPatientIdNumber(dto.getPatientIdNumber());
			patient.setPatientDateOfBirth(dto.getPatientDateOfBirth());
			patient.setPatientOccupation(dto.getPatientOccupation());
			patient.setPatientContactNumber(dto.getPatientContactNumber());
			patient.setPatientGender(dto.getPatientGender());
			patient.setPatientStatus(dto.getPatientStatus());
			/*patient.setAddedUser(dto.getAddedUser());
			patient.setAddedDate(dto.getAddedDate());
			patient.setAddedTime(dto.getAddedTime());
			patient.setChangedUser(dto.getChangedUser());
			patient.setChangedDate(dto.getChangedDate());
			patient.setChangedTime(dto.getChangedTime());*/
			processingBeforeDataUpdate(dto, patient);
			try {
				patientRepository.saveAndFlush(patient);
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
			}
		}

       return result;
	}

	private StepResult processingBeforeDataRead(ChangePatientDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000025 BLK CAS
		//switchSUB 1000025 BLK CAS
		if (dto.getHospitalCode().equals("PUBLIC")) {
			// PAR.Hospital Code is Equal Public
			//switchBLK 1000028 BLK ACT
			//functionCall 1000029 ACT LCL.Hospital Name = CON.Public
			dto.setLclHospitalName("Public");
		}
       return NO_ACTION;
	}

	private StepResult processingIfDataRecordNotFound(ChangePatientDTO dto) throws ServiceException {
		/**
		 * USER: Processing if Data Record Not Found (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
	}

	private StepResult processingAfterDataRead(ChangePatientDTO dto, Patient patient) throws ServiceException {
		/**
		 * USER: Processing After Data Read (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000034 BLK CAS
		//switchSUB 1000034 BLK CAS
		if (patient.getHospitalCode().equals("PUBLIC")) {
			// DB1.Hospital Code is Equal Public
			//switchBLK 1000037 BLK ACT
			//functionCall 1000038 ACT LCL.Hospital Name = CON.Public
			dto.setLclHospitalName("Public");
		}
       return NO_ACTION;
	}

	private StepResult processingBeforeDataUpdate(ChangePatientDTO dto, Patient patient) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Generated:10)
		 */
		//switchSUB 10 SUB    
		//switchBLK 1000013 BLK ACT
		//functionCall 1000014 ACT DB1.Changed User = JOB.*USER
		/*patient.setChangedUser(job.getUser());
		//switchBLK 1000017 BLK ACT
		//functionCall 1000018 ACT DB1.Changed Date = JOB.*Job date
		patient.setChangedDate(LocalDate.now());
		//switchBLK 1000021 BLK ACT
		//functionCall 1000022 ACT DB1.Changed Time = JOB.*Job time
		patient.setChangedTime(LocalTime.now());*/
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(ChangePatientDTO dto) throws ServiceException {
		/**
		 * USER: Processing After Data Update (Generated:23)
		 */
		//switchSUB 23 SUB    
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
	}
}
