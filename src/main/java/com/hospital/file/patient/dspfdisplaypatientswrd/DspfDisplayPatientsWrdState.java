package com.hospital.file.patient.dspfdisplaypatientswrd;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;


/**
 * State for file 'Patient' (TSAEREP) and function 'DSPF Display Patients/Wrd' (TSARDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class DspfDisplayPatientsWrdState extends DspfDisplayPatientsWrdDTO {
	private static final long serialVersionUID = -7240935450432944976L;

	// Local fields
	private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");

	// System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;


    public DspfDisplayPatientsWrdState() {

    }

	public void setLclReturnCode(ReturnCodeEnum returnCode) {
    	this.lclReturnCode = returnCode;
    }

    public ReturnCodeEnum getLclReturnCode() {
    	return lclReturnCode;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }
    }