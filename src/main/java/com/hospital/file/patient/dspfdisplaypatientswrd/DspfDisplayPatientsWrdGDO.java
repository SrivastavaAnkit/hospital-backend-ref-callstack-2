package com.hospital.file.patient.dspfdisplaypatientswrd;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.model.PatientGenderEnum;
import com.hospital.model.PatientStatusEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;



/**
 * Gdo for file 'Patient' (TSAEREP) and function 'DSPF Display Patients/Wrd' (TSARDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */
public class DspfDisplayPatientsWrdGDO implements Serializable {
	private static final long serialVersionUID = 3599696231104204745L;

	private long version = 0;
    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private String wardCode = "";
	private String patientCode = "";
	private String hospitalCode = "";
	private String patientName = "";
	private String patientSurname = "";
	private String addressLine1 = "";
	private String addressLine2 = "";
	private String addressTownCity = "";
	private String addressCountry = "";
	private String addressPostalZipCode = "";
	private long patientIdNumber = 0L;
	private LocalDate patientDateOfBirth = null;
	private String patientOccupation = "";
	private long patientContactNumber = 0L;
	private PatientGenderEnum patientGender = null;
	private PatientStatusEnum patientStatus = null;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;

	public DspfDisplayPatientsWrdGDO() {

	}
   //TODO:FIX ME
   	public DspfDisplayPatientsWrdGDO(/*long version,*/ String wardCode, String patientCode, String hospitalCode, String patientName, String patientSurname, String addressLine1, String addressLine2, String addressTownCity, String addressCountry, String addressPostalZipCode, long patientIdNumber, LocalDate patientDateOfBirth, String patientOccupation, long patientContactNumber, PatientGenderEnum patientGender, PatientStatusEnum patientStatus/*, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime*/) {
		this.version = version;
		this.wardCode = wardCode;
		this.patientCode = patientCode;
		this.hospitalCode = hospitalCode;
		this.patientName = patientName;
		this.patientSurname = patientSurname;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.addressTownCity = addressTownCity;
		this.addressCountry = addressCountry;
		this.addressPostalZipCode = addressPostalZipCode;
		this.patientIdNumber = patientIdNumber;
		this.patientDateOfBirth = patientDateOfBirth;
		this.patientOccupation = patientOccupation;
		this.patientContactNumber = patientContactNumber;
		this.patientGender = patientGender;
		this.patientStatus = patientStatus;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}


	public void setVersion(long version) {
		this.version = version;
	}

    public long getVersion() {
		return version;
    }

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setWardCode(String wardCode) {
    	this.wardCode = wardCode;
    }

	public String getWardCode() {
		return wardCode;
	}

	public void setPatientCode(String patientCode) {
    	this.patientCode = patientCode;
    }

	public String getPatientCode() {
		return patientCode;
	}

	public void setHospitalCode(String hospitalCode) {
    	this.hospitalCode = hospitalCode;
    }

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setPatientName(String patientName) {
    	this.patientName = patientName;
    }

	public String getPatientName() {
		return patientName;
	}

	public void setPatientSurname(String patientSurname) {
    	this.patientSurname = patientSurname;
    }

	public String getPatientSurname() {
		return patientSurname;
	}

	public void setAddressLine1(String addressLine1) {
    	this.addressLine1 = addressLine1;
    }

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine2(String addressLine2) {
    	this.addressLine2 = addressLine2;
    }

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressTownCity(String addressTownCity) {
    	this.addressTownCity = addressTownCity;
    }

	public String getAddressTownCity() {
		return addressTownCity;
	}

	public void setAddressCountry(String addressCountry) {
    	this.addressCountry = addressCountry;
    }

	public String getAddressCountry() {
		return addressCountry;
	}

	public void setAddressPostalZipCode(String addressPostalZipCode) {
    	this.addressPostalZipCode = addressPostalZipCode;
    }

	public String getAddressPostalZipCode() {
		return addressPostalZipCode;
	}

	public void setPatientIdNumber(long patientIdNumber) {
    	this.patientIdNumber = patientIdNumber;
    }

	public long getPatientIdNumber() {
		return patientIdNumber;
	}

	public void setPatientDateOfBirth(LocalDate patientDateOfBirth) {
    	this.patientDateOfBirth = patientDateOfBirth;
    }

	public LocalDate getPatientDateOfBirth() {
		return patientDateOfBirth;
	}

	public void setPatientOccupation(String patientOccupation) {
    	this.patientOccupation = patientOccupation;
    }

	public String getPatientOccupation() {
		return patientOccupation;
	}

	public void setPatientContactNumber(long patientContactNumber) {
    	this.patientContactNumber = patientContactNumber;
    }

	public long getPatientContactNumber() {
		return patientContactNumber;
	}

	public void setPatientGender(PatientGenderEnum patientGender) {
    	this.patientGender = patientGender;
    }

	public PatientGenderEnum getPatientGender() {
		return patientGender;
	}

	public void setPatientStatus(PatientStatusEnum patientStatus) {
    	this.patientStatus = patientStatus;
    }

	public PatientStatusEnum getPatientStatus() {
		return patientStatus;
	}

	public void setAddedUser(String addedUser) {
    	this.addedUser = addedUser;
    }

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
    	this.addedDate = addedDate;
    }

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
    	this.addedTime = addedTime;
    }

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
    	this.changedUser = changedUser;
    }

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
    	this.changedDate = changedDate;
    }

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
    	this.changedTime = changedTime;
    }

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}