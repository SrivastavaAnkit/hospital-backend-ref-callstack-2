package com.hospital.file.patient.dspfdisplaypatientswrd;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.common.state.BaseDTO;
import com.hospital.common.utils.RestResponsePage;

import com.hospital.common.utils.RestResponsePage;
import com.hospital.model.ReturnCodeEnum;


/**
 * Dto for file 'Patient' (TSAEREP) and function 'DSPF Display Patients/Wrd' (TSARDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class DspfDisplayPatientsWrdDTO extends BaseDTO {
	private static final long serialVersionUID = -9021035671380472508L;
	private long version = 0;

    private RestResponsePage<DspfDisplayPatientsWrdGDO> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";

	private String wardCode = "";
	private String wardName = "";
	private String patientCode = "";
	private String hospitalCode = "";
	private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");


	private DspfDisplayPatientsWrdGDO gdo;

    public DspfDisplayPatientsWrdDTO() {

    }

	public DspfDisplayPatientsWrdDTO(long version, String wardCode) {
		this.version = version;
		this.wardCode = wardCode;
	}

	public void setVersion(long version) {
		this.version = version;
    }

    public long getVersion() {
    	return version;
    }

    public void setPageDto(RestResponsePage<DspfDisplayPatientsWrdGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<DspfDisplayPatientsWrdGDO> getPageDto() {
		return pageDto;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
    }

    public String getWardCode() {
    	return wardCode;
    }

	public void setWardName(String wardName) {
		this.wardName = wardName;
    }

    public String getWardName() {
    	return wardName;
    }

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
    }

    public String getPatientCode() {
    	return patientCode;
    }

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
    }

    public String getHospitalCode() {
    	return hospitalCode;
    }

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
    }

    public ReturnCodeEnum getReturnCode() {
    	return returnCode;
    }

	public void setGdo(DspfDisplayPatientsWrdGDO gdo) {
		this.gdo = gdo;
	}

	public DspfDisplayPatientsWrdGDO getGdo() {
		return gdo;
	}

}