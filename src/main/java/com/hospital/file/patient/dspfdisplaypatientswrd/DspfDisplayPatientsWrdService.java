package com.hospital.file.patient.dspfdisplaypatientswrd;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.utils.RestResponsePage;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

import com.hospital.file.diagnosis.dspfil.DspfilParams;
import com.hospital.file.diagnosis.dspfil.DspfilService;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailService;
import com.hospital.file.patient.PatientRepository;
import com.hospital.file.patient.edreditpatientdetail.EdrEditPatientDetailParams;
import com.hospital.file.ward.rtvwarddetail.RtvWardDetailDTO;
import com.hospital.file.ward.rtvwarddetail.RtvWardDetailService;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.support.JobContext;




/**
 * DSPFIL Service controller for 'DSPF Display Patients/Wrd' (TSARDFR) of file 'Patient' (TSAEREP)
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
@Service
public class DspfDisplayPatientsWrdService extends AbstractService<DspfDisplayPatientsWrdService, DspfDisplayPatientsWrdState> {
    
	@Autowired
	private JobContext job;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private RtvHospitalDetailService rtvHospitalDetailService;
    
    @Autowired
    private RtvWardDetailService rtvWardDetailService;
    

    @Autowired
    private MessageSource messageSource;
    
	public static final String SCREEN_CTL = "dspfDisplayPatientsWrd";
    public static final String SCREEN_RCD = "DspfDisplayPatientsWrd.rcd";

    private final Step execute = define("execute", DspfDisplayPatientsWrdParams.class, this::execute);
    private final Step response = define("response", DspfDisplayPatientsWrdDTO.class, this::processResponse);
	private final Step serviceDspfil = define("serviceDspfil",DspfilParams.class, this::processServiceDspfil);
	private final Step serviceEdrEditPatientDetail = define("serviceEdrEditPatientDetail",EdrEditPatientDetailParams.class, this::processServiceEdrEditPatientDetail);
	//private final Step serviceRtvWardDetail = define("serviceRtvWardDetail",RtvWardDetailParams.class, this::processServiceRtvWardDetail);
	//private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
	//private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
	//private final Step serviceQuit = define("serviceQuit",QuitParams.class, this::processServiceQuit);
	
    
    @Autowired
    public DspfDisplayPatientsWrdService() {
        super(DspfDisplayPatientsWrdService.class, DspfDisplayPatientsWrdState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * DSPFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(DspfDisplayPatientsWrdState state, DspfDisplayPatientsWrdParams params) {
    	StepResult result = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        result = usrInitializeProgram(state);

        result = mainLoop(state, params);

        return result;
    }

    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult mainLoop(DspfDisplayPatientsWrdState state, DspfDisplayPatientsWrdParams params) {
        StepResult result = NO_ACTION;

        result = loadFirstSubfilePage(state, params);

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        result = conductScreenConversation(state);

        return result;
    }

    /**
     * SCREEN  initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult loadFirstSubfilePage(DspfDisplayPatientsWrdState state, DspfDisplayPatientsWrdParams params) {
    	StepResult result = NO_ACTION;

    	result = usrInitializeSubfileControl(state, params);

		dbfReadFirstDataRecord(state);
		if (state.getPageDto() != null && state.getPageDto().getSize() > 0) {
		    result = loadNextSubfilePage(state);
		}

        return result;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     * @return
     */
    private StepResult loadNextSubfilePage(DspfDisplayPatientsWrdState state) {
    	StepResult result = NO_ACTION;

    	List<DspfDisplayPatientsWrdGDO> list = state.getPageDto().getContent();
        for (DspfDisplayPatientsWrdGDO gdo : list) {
            gdo.set_SysRecordSelected(RecordSelectedEnum._STA_YES);
            result = usrInitializeSubfileRecordFromDBFRecord(state, gdo);
//             TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
            if (gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
//                TODO:writeSubfileRecord(state);  // synon built-in function
            }
        }

        return result;
    }

    /**
     * SCREEN  initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductScreenConversation(DspfDisplayPatientsWrdState state) {
        StepResult result = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            DspfDisplayPatientsWrdDTO model = new DspfDisplayPatientsWrdDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_CTL, model).thenCall(response);
        }

        return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processResponse(DspfDisplayPatientsWrdState state, DspfDisplayPatientsWrdDTO model) {
    	StepResult result = NO_ACTION;

        // update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            result = closedown(state);
        } else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
        } else if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            //TODO:processHelpRequest(state);//synon built-in function
        }
        else if (CmdKeyEnum.isNextPage(state.get_SysCmdKey())) {
            dbfReadNextPageRecord(state);
            result = loadNextSubfilePage(state);
        } else {
            result = processScreen(state);
        }

        return result;
    }

    /**
     * SCREEN process screen.
     * @param state - Service state class.
     * @return
     */
    private StepResult processScreen(DspfDisplayPatientsWrdState state) {
    	StepResult result = NO_ACTION;

        result = usrProcessSubfilePreConfirm(state);
        if(result!=NO_ACTION) {
    		return result;
    	}
        if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
        	return closedown(state);
        } else {
        	if(!state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
        		return closedown(state);
	        } else {
//	        	if(!state.get_SysProgramConfirm().getCode().equals(ProgramConfirmEnum._STA_YES.getCode())) {
//	        		return closedown(state);
//		        } else {
//		        	result = usrProcessCommandKeys(state);
//		        }

	        	result = usrProcessSubfileControlPostConfirm(state);
	        	for (DspfDisplayPatientsWrdGDO gdo : state.getPageDto().getContent()) {
	                if(gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
	                	result = usrProcessSubfileRecordPostConfirm(state, gdo);
//	                  TODO:writeSubfileRecord(state);   // synon built-in function
	                }
	            }
	        	result = usrFinalProcessingPostConfirm(state);
	        	result = usrProcessCommandKeys(state);
	        }
        }

        result = conductScreenConversation(state);

        return result;
    }

    /**
     * SCREEN Process subfile Pre Confirm.
     * @param state - Service state class.
     * @return
     */
    private StepResult usrProcessSubfilePreConfirm(DspfDisplayPatientsWrdState state) {
    	StepResult result = NO_ACTION;

    	result = usrSubfileControlFunctionFields(state);
    	result = usrProcessSubfileControlPreConfirm(state);
    	//TODO:FIX me This code where to set ?
    	//if(state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
    		for (DspfDisplayPatientsWrdGDO gdo : state.getPageDto().getContent()) {
                if(gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
                	result = usrSubfileRecordFunctionFields(state, gdo);
                    result = usrProcessSubfileRecordPreConfirm(state, gdo);
//                  TODO:writeSubfileRecord(state);   // synon built-in function
                }
            }
    	//}
    	if(result!=NO_ACTION) {
    		return result;
    	}
    	//commented
        //result = usrFinalProcessingPreConfirm(state);

        return result;
    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(DspfDisplayPatientsWrdState state) {
        StepResult result = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        result = usrExitProgramProcessing(state);

        return result;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(DspfDisplayPatientsWrdState state) {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadNextPageRecord(DspfDisplayPatientsWrdState state)
	{
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    private void dbfReadDataRecord(DspfDisplayPatientsWrdState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = new PageRequest(state.getPage(), state.getSize());
        }
        else {
            pageable = new PageRequest(state.getPage(), state.getSize(), new Sort(sortOrders));
        }

        RestResponsePage<DspfDisplayPatientsWrdGDO> pageDto = patientRepository.dspfDisplayPatientsWrd(state.getHospitalCode(), state.getWardCode(), pageable);
        state.setPageDto(pageDto);
    }
    
	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(DspfDisplayPatientsWrdState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 20 SUB    
			// Unprocessed SUB 20 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Control (Generated:182)
	 */
    private StepResult usrInitializeSubfileControl(DspfDisplayPatientsWrdState dto, DspfDisplayPatientsWrdParams params) {
        StepResult result = NO_ACTION;

        try {
        	RtvWardDetailDTO rtvWardDetailDTO;
			//switchSUB 182 SUB    
			//switchBLK 1000001 BLK ACT
			// DEBUG genFunctionCall 1000002 ACT RTV Ward detail - Ward  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvWardDetailDTO = new RtvWardDetailDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvWardDetailDTO.setHospitalCode(dto.getHospitalCode());
			rtvWardDetailDTO.setWardCode(dto.getWardCode());
			// DEBUG genFunctionCall Service call
			rtvWardDetailService.execute(rtvWardDetailDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setWardName(rtvWardDetailDTO.getWardName());
			// DEBUG genFunctionCall Parameters DONE
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrInitializeSubfileRecordFromDBFRecord(DspfDisplayPatientsWrdState dto, DspfDisplayPatientsWrdGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 41 SUB    
			// Unprocessed SUB 41 -
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:175)
	 */
    private StepResult usrSubfileControlFunctionFields(DspfDisplayPatientsWrdState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 175 SUB    
			// Unprocessed SUB 175 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Control (Pre-Confirm) (Generated:72)
	 */
    private StepResult usrProcessSubfileControlPreConfirm(DspfDisplayPatientsWrdState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 72 SUB    
			//switchBLK 1000040 BLK CAS
			//switchSUB 1000040 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// CTL.*CMD key is *Cancel
				//switchBLK 1000046 BLK ACT
				// DEBUG genFunctionCall 1000047 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000043 BLK ACT
				// DEBUG genFunctionCall 1000044 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:170)
	 */
    private StepResult usrSubfileRecordFunctionFields(DspfDisplayPatientsWrdState dto, DspfDisplayPatientsWrdGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 170 SUB    
			// Unprocessed SUB 170 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Record (Pre-Confirm) (Generated:101)
	 */
    private StepResult usrProcessSubfileRecordPreConfirm(DspfDisplayPatientsWrdState dto, DspfDisplayPatientsWrdGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 101 SUB    
			//switchBLK 1000006 BLK CAS
			//switchSUB 1000006 BLK CAS
			if (gdo.get_SysSelected().equals("Display Patients")) {
				// RCD.*SFLSEL is Display Patients
				//switchBLK 1000011 BLK ACT
				// DEBUG genFunctionCall 1000012 ACT DSPFIL - Diagnosis  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				//TODO Unused curly bracket.
				//{
				//TODO: split
				DspfilParams dspfilParams = new DspfilParams();
				BeanUtils.copyProperties(dto, dspfilParams);
				result = StepResult.callService(DspfilService.class, dspfilParams).thenCall(serviceDspfil);
				//}
				//switchBLK 1000025 BLK CAS
				//switchSUB 1000025 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000033 BLK ACT
					// DEBUG genFunctionCall 1000034 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000077 BLK ACT
					// DEBUG genFunctionCall 1000078 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000028 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000030 BLK ACT
					// DEBUG genFunctionCall 1000031 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Final processing (Pre-confirm) (Generated:222)
	 */
    private StepResult usrFinalProcessingPreConfirm(DspfDisplayPatientsWrdState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 222 SUB    
			// Unprocessed SUB 222 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process subfile control (Post-confirm) (Generated:225)
	 */
    private StepResult usrProcessSubfileControlPostConfirm(DspfDisplayPatientsWrdState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 225 SUB    
			// Unprocessed SUB 225 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Record (Post-Confirm Pass) (Generated:209)
	 */
    private StepResult usrProcessSubfileRecordPostConfirm(DspfDisplayPatientsWrdState dto, DspfDisplayPatientsWrdGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 209 SUB    
			// Unprocessed SUB 209 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Final processing (Post-confirm) (Generated:228)
	 */
    private StepResult usrFinalProcessingPostConfirm(DspfDisplayPatientsWrdState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 228 SUB    
			// Unprocessed SUB 228 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Generated:140)
	 */
    private StepResult usrProcessCommandKeys(DspfDisplayPatientsWrdState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 140 SUB    
			//switchBLK 1000014 BLK CAS
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(DspfDisplayPatientsWrdState dto) {
        StepResult result = NO_ACTION;
        
        try {
        	//switchSUB 132 SUB    
			//switchBLK 1000052 BLK CAS
			//switchSUB 1000052 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// CTL.*CMD key is *Exit
				//switchBLK 1000055 BLK ACT
				// DEBUG genFunctionCall 1000056 ACT PAR.*Return code = CND.*Normal
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000059 BLK ACT
				// DEBUG genFunctionCall 1000060 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */

    /**
     * DspfilService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspfil(DspfDisplayPatientsWrdState state, DspfilParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

	        
        //TODO: call the continuation of the program
        //result = ??;
        DspfDisplayPatientsWrdParams params=new DspfDisplayPatientsWrdParams();
        BeanUtils.copyProperties(state, params);
        result=execute(state, params);
        return result;
    }

    /**
     * EdrEditPatientDetailService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceEdrEditPatientDetail(DspfDisplayPatientsWrdState state, EdrEditPatientDetailParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }
//
//    /**
//     * RtvWardDetailService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvWardDetail(DspfDisplayPatientsWrdState state, RtvWardDetailParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(DspfDisplayPatientsWrdState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(DspfDisplayPatientsWrdState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * QuitService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceQuit(DspfDisplayPatientsWrdState state, QuitParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//


    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */

    /**
     * DspfilService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
//    private StepResult processServiceDspfil(DspfDisplayPatientsWrdState state, DspfilParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//
//    /**
//     * EdrEditPatientDetailService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceEdrEditPatientDetail(DspfDisplayPatientsWrdState state, EdrEditPatientDetailParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//
//    /**
//     * RtvWardDetailService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvWardDetail(DspfDisplayPatientsWrdState state, RtvWardDetailParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(DspfDisplayPatientsWrdState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(DspfDisplayPatientsWrdState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * QuitService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceQuit(DspfDisplayPatientsWrdState state, QuitParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//


}
