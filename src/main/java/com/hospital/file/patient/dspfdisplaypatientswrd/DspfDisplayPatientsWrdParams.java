package com.hospital.file.patient.dspfdisplaypatientswrd;

import java.io.Serializable;


/**
 * Params for resource: DspfDisplayPatientsWrd (TSARDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
public class DspfDisplayPatientsWrdParams implements Serializable {
	private static final long serialVersionUID = 1757378974999200305L;

	private String wardCode = "";
	private String hospitalCode = "";


	public String getWardCode() {
		return wardCode;
	}
	
	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}
	
	public String getHospitalCode() {
		return hospitalCode;
	}
	
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

}