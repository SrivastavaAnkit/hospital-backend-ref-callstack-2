package com.hospital.file.patient.selectpatient;

import com.hospital.file.patient.Patient;
import com.hospital.model.ReloadSubfileEnum;

/**
 * State for file 'Patient' (TSAEREP) and function 'Select Patient' (TSANSRR).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectPatientState extends SelectPatientDTO {
    private static final long serialVersionUID = 3338715761745152682L;


    // System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    public SelectPatientState() {
    }

    public SelectPatientState(Patient patient) {
        //setDtoFields(patient);
    }

   public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
       _sysReloadSubfile = reloadSubfile;
   }

   public ReloadSubfileEnum get_SysReloadSubfile() {
       return _sysReloadSubfile;
   }

}
