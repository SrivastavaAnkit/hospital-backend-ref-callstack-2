package com.hospital.file.patient.selectpatient;



import org.springframework.data.domain.Page;

import com.hospital.common.state.BaseDTO;
import com.hospital.model.RecordSelectedEnum;


/**
 * Dto for file 'Patient' (TSAEREP) and function 'Select Patient' (TSANSRR).
 */
public class SelectPatientDTO extends BaseDTO {
	private static final long serialVersionUID = 7732031629357237970L;

	private String patientCode = "";
	private Page<SelectPatientGDO> pageDto;
	private RecordSelectedEnum recordSelect;
	private SelectPatientGDO gdo;

	 private int page = 0;
	    private int size = 10;
	    private String sortData = "";
	    private boolean confirm = false;
	    
	    
	public int getPage() {
			return page;
		}


		public void setPage(int page) {
			this.page = page;
		}


		public int getSize() {
			return size;
		}


		public void setSize(int size) {
			this.size = size;
		}


		public String getSortData() {
			return sortData;
		}


		public void setSortData(String sortData) {
			this.sortData = sortData;
		}


		public boolean isConfirm() {
			return confirm;
		}


		public void setConfirm(boolean confirm) {
			this.confirm = confirm;
		}


	public SelectPatientDTO() {
	}

	
	public SelectPatientGDO getGdo() {
		return gdo;
	}


	public void setGdo(SelectPatientGDO gdo) {
		this.gdo = gdo;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public Page<SelectPatientGDO> getPageDto() {
		return pageDto;
	}

	public void setPageDto(Page<SelectPatientGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RecordSelectedEnum getRecordSelect() {
		return recordSelect;
	}

	public void setRecordSelect(RecordSelectedEnum recordSelect) {
		this.recordSelect = recordSelect;
	}



}
