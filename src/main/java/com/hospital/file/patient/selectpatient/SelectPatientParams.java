package com.hospital.file.patient.selectpatient;

import java.io.Serializable;


/**
 * Params for resource: SelectPatient (TSANSRR).
 *
 * @author X2EGenerator
 */
public class SelectPatientParams implements Serializable {
    private static final long serialVersionUID = 7255573193665432873L;

	private String patientCode = "";

	public String getPatientCode() {
		return patientCode;
	}
	
	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}
}

