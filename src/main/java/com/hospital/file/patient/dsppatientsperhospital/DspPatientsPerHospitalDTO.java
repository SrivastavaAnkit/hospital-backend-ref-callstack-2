package com.hospital.file.patient.dsppatientsperhospital;

import com.hospital.common.state.BaseDTO;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.model.CountryEnum;
import com.hospital.model.ReturnCodeEnum;


/**
 * Dto for file 'Patient' (TSAEREP) and function 'DSP Patients per Hospital' (TSBEDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class DspPatientsPerHospitalDTO extends BaseDTO {
	private static final long serialVersionUID = -552494667243369095L;
	private long version = 0;

    private RestResponsePage<DspPatientsPerHospitalGDO> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";

	private CountryEnum country = null;
	private ReturnCodeEnum returnCode = null;
	private String countryName = "";
	private String hospitalCode = "";
	private String hospitalName = "";
	private long telephoneNumber = 0L;
	private String wardCode = "";


	private DspPatientsPerHospitalGDO gdo;

    public DspPatientsPerHospitalDTO() {

    }

	public DspPatientsPerHospitalDTO(long version, String hospitalCode) {
		this.version = version;
		this.hospitalCode = hospitalCode;
	}

	public void setVersion(long version) {
		this.version = version;
    }

    public long getVersion() {
    	return version;
    }

    public void setPageDto(RestResponsePage<DspPatientsPerHospitalGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<DspPatientsPerHospitalGDO> getPageDto() {
		return pageDto;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setCountry(CountryEnum country) {
		this.country = country;
    }

    public CountryEnum getCountry() {
    	return country;
    }

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
    }

    public ReturnCodeEnum getReturnCode() {
    	return returnCode;
    }

	public void setCountryName(String countryName) {
		this.countryName = countryName;
    }

    public String getCountryName() {
    	return countryName;
    }

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
    }

    public String getHospitalCode() {
    	return hospitalCode;
    }

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
    	return hospitalName;
    }

	public void setTelephoneNumber(long telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
    }

    public long getTelephoneNumber() {
    	return telephoneNumber;
    }

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
    }

    public String getWardCode() {
    	return wardCode;
    }

	public void setGdo(DspPatientsPerHospitalGDO gdo) {
		this.gdo = gdo;
	}

	public DspPatientsPerHospitalGDO getGdo() {
		return gdo;
	}

}