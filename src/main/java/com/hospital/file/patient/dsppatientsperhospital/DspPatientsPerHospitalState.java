package com.hospital.file.patient.dsppatientsperhospital;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReloadSubfileEnum;



/**
 * State for file 'Patient' (TSAEREP) and function 'DSP Patients per Hospital' (TSBEDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

@Configurable
public class DspPatientsPerHospitalState extends DspPatientsPerHospitalDTO {
	private static final long serialVersionUID = 4879040341922694816L;


	@Autowired
	private GlobalContext globalCtx;


	// System fields
	private long _sysScanLimit = 0L;
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;


    public DspPatientsPerHospitalState() {

    }

	public void setWfAddressPostZip(String addressPostZip) {
		globalCtx.setString("addressPostZip", addressPostZip);
	}

	public String getWfAddressPostZip() {
		return globalCtx.getString("addressPostZip");
	}

	public void setWfAddressProvince(String addressProvince) {
		globalCtx.setString("addressProvince", addressProvince);
	}

	public String getWfAddressProvince() {
		return globalCtx.getString("addressProvince");
	}

	public void setWfAddressStreet(String addressStreet) {
		globalCtx.setString("addressStreet", addressStreet);
	}

	public String getWfAddressStreet() {
		return globalCtx.getString("addressStreet");
	}

	public void setWfAddressTown(String addressTown) {
		globalCtx.setString("addressTown", addressTown);
	}

	public String getWfAddressTown() {
		return globalCtx.getString("addressTown");
	}

	public void setWfFaxNumber(long faxNumber) {
		globalCtx.setLong("faxNumber", faxNumber);
	}

	public long getWfFaxNumber() {
		return globalCtx.getLong("faxNumber");
	}


	public void set_SysScanLimit(long scanLimit) {
    	_sysScanLimit = scanLimit;
    }

    public long get_SysScanLimit() {
    	return _sysScanLimit;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }
    }