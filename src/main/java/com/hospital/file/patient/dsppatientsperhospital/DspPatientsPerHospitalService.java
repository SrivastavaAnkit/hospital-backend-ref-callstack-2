package com.hospital.file.patient.dsppatientsperhospital;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
 
import com.hospital.support.JobContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.file.patient.PatientRepository;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailService;
import com.hospital.file.ward.rtvwarddetail.RtvWardDetailService;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailDTO;
import com.hospital.file.ward.rtvwarddetail.RtvWardDetailDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.CountryEnum;
import com.hospital.model.PatientGenderEnum;
import com.hospital.model.PatientStatusEnum;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.common.exception.ServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

	
/**
 * Service implementation for resource: DspPatientsPerHospital (TSBEDFR).
 *
 * @author X2EGenerator
 */
@Service
public class DspPatientsPerHospitalService extends AbstractService<DspPatientsPerHospitalService, DspPatientsPerHospitalState> {
    
	@Autowired
	private JobContext job;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private RtvHospitalDetailService rtvHospitalDetailService;
    
    @Autowired
    private RtvWardDetailService rtvWardDetailService;
    

    @Autowired
    private MessageSource messageSource;
    
	public static final String SCREEN_CTL = "dspPatientsPerHospital";
    public static final String SCREEN_RCD = "DspPatientsPerHospital.rcd";

    private final Step execute = define("execute", DspPatientsPerHospitalParams.class, this::execute);
    private final Step response = define("response", DspPatientsPerHospitalDTO.class, this::processResponse);
	//private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
	//private final Step serviceRtvHospitalDetail = define("serviceRtvHospitalDetail",RtvHospitalDetailParams.class, this::processServiceRtvHospitalDetail);
	//private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
	//private final Step serviceRtvWardDetail = define("serviceRtvWardDetail",RtvWardDetailParams.class, this::processServiceRtvWardDetail);
	
    
    @Autowired
    public DspPatientsPerHospitalService() {
        super(DspPatientsPerHospitalService.class, DspPatientsPerHospitalState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * DSPFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(DspPatientsPerHospitalState state, DspPatientsPerHospitalParams params) {
    	StepResult result = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        result = usrInitializeProgram(state);

        result = mainLoop(state, params);

        return result;
    }

    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult mainLoop(DspPatientsPerHospitalState state, DspPatientsPerHospitalParams params) {
        StepResult result = NO_ACTION;

        result = loadFirstSubfilePage(state, params);

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        result = conductScreenConversation(state);

        return result;
    }

    /**
     * SCREEN  initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult loadFirstSubfilePage(DspPatientsPerHospitalState state, DspPatientsPerHospitalParams params) {
    	StepResult result = NO_ACTION;

    	result = usrInitializeSubfileControl(state, params);

		dbfReadFirstDataRecord(state);
		if (state.getPageDto() != null && state.getPageDto().getSize() > 0) {
		    result = loadNextSubfilePage(state);
		}

        return result;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     * @return
     */
    private StepResult loadNextSubfilePage(DspPatientsPerHospitalState state) {
    	StepResult result = NO_ACTION;

    	List<DspPatientsPerHospitalGDO> list = state.getPageDto().getContent();
        for (DspPatientsPerHospitalGDO gdo : list) {
            gdo.set_SysRecordSelected(RecordSelectedEnum._STA_YES);
            result = usrInitializeSubfileRecordFromDBFRecord(state, gdo);
//             TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
            if (gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
//                TODO:writeSubfileRecord(state);  // synon built-in function
            }
        }

        return result;
    }

    /**
     * SCREEN  initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductScreenConversation(DspPatientsPerHospitalState state) {
        StepResult result = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            DspPatientsPerHospitalDTO model = new DspPatientsPerHospitalDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_CTL, model).thenCall(response);
        }

        return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processResponse(DspPatientsPerHospitalState state, DspPatientsPerHospitalDTO model) {
    	StepResult result = NO_ACTION;

        // update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            result = closedown(state);
        } else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
        } else if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            //TODO:processHelpRequest(state);//synon built-in function
        }
        else if (CmdKeyEnum.isNextPage(state.get_SysCmdKey())) {
            dbfReadNextPageRecord(state);
            result = loadNextSubfilePage(state);
        } else {
            result = processScreen(state);
        }

        return result;
    }

    /**
     * SCREEN process screen.
     * @param state - Service state class.
     * @return
     */
    private StepResult processScreen(DspPatientsPerHospitalState state) {
    	StepResult result = NO_ACTION;

        result = usrProcessSubfilePreConfirm(state);
        if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
        	return closedown(state);
        } else {
        	if(!state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
        		return closedown(state);
	        } else {
//	        	if(!state.get_SysProgramConfirm().getCode().equals(ProgramConfirmEnum._STA_YES.getCode())) {
//	        		return closedown(state);
//		        } else {
//		        	result = usrProcessCommandKeys(state);
//		        }

	        	result = usrProcessSubfileControlPostConfirm(state);
	        	for (DspPatientsPerHospitalGDO gdo : state.getPageDto().getContent()) {
	                if(gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
	                	result = usrProcessSubfileRecordPostConfirm(state, gdo);
//	                  TODO:writeSubfileRecord(state);   // synon built-in function
	                }
	            }
	        	result = usrFinalProcessingPostConfirm(state);
	        	result = usrProcessCommandKeys(state);
	        }
        }

        result = conductScreenConversation(state);

        return result;
    }

    /**
     * SCREEN Process subfile Pre Confirm.
     * @param state - Service state class.
     * @return
     */
    private StepResult usrProcessSubfilePreConfirm(DspPatientsPerHospitalState state) {
    	StepResult result = NO_ACTION;

    	result = usrSubfileControlFunctionFields(state);
    	result = usrProcessSubfileControlPreConfirm(state);
    	if(state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
    		for (DspPatientsPerHospitalGDO gdo : state.getPageDto().getContent()) {
                if(gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
                	result = usrSubfileRecordFunctionFields(state, gdo);
                    result = usrProcessSubfileRecordPreConfirm(state, gdo);
//                  TODO:writeSubfileRecord(state);   // synon built-in function
                }
            }
    	}

        result = usrFinalProcessingPreConfirm(state);

        return result;
    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(DspPatientsPerHospitalState state) {
        StepResult result = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        result = usrExitProgramProcessing(state);

        return result;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(DspPatientsPerHospitalState state) {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadNextPageRecord(DspPatientsPerHospitalState state)
	{
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    private void dbfReadDataRecord(DspPatientsPerHospitalState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = new PageRequest(state.getPage(), state.getSize());
        }
        else {
            pageable = new PageRequest(state.getPage(), state.getSize(), new Sort(sortOrders));
        }

        RestResponsePage<DspPatientsPerHospitalGDO> pageDto = patientRepository.dspPatientsPerHospital(state.getHospitalCode(), state.getCountry(), state.getCountryName(), state.getTelephoneNumber(), state.getWardCode(), pageable);
        state.setPageDto(pageDto);
    }
    
	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(DspPatientsPerHospitalState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 20 SUB    
			//switchBLK 1000076 BLK ACT
			// DEBUG genFunctionCall 1000077 ACT PGM.*Scan limit = CND.High Value
			dto.set_SysScanLimit(9999999);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Control (Generated:182)
	 */
    private StepResult usrInitializeSubfileControl(DspPatientsPerHospitalState dto, DspPatientsPerHospitalParams params) {
        StepResult result = NO_ACTION;

        try {
        	RtvHospitalDetailDTO rtvHospitalDetailDTO;
			//switchSUB 182 SUB    
			//switchBLK 1000007 BLK ACT
			// DEBUG genFunctionCall 1000008 ACT CTL.Country = PAR.Country
			dto.setCountry(dto.getCountry());
			//switchBLK 1000001 BLK ACT
			// DEBUG genFunctionCall 1000002 ACT CTL.Country Name = PAR.Country Name
			dto.setCountryName(dto.getCountryName());
			//switchBLK 1000015 BLK ACT
			// DEBUG genFunctionCall 1000016 ACT CTL.Telephone Number = PAR.Telephone Number
			dto.setTelephoneNumber(dto.getTelephoneNumber());
			//switchBLK 1000023 BLK ACT
			// DEBUG genFunctionCall 1000024 ACT RTV Hospital detail - Hospital  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvHospitalDetailDTO = new RtvHospitalDetailDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvHospitalDetailDTO.setHospitalCode(dto.getHospitalCode());
			// DEBUG genFunctionCall Service call
			rtvHospitalDetailService.execute(rtvHospitalDetailDTO);
			// DEBUG genFunctionCall Parameters OUT
//			dto.setHospitalName(rtvHospitalDetailDTO.getHospitalName());
//			dto.setWfAddressStreet(rtvHospitalDetailDTO.getAddressStreet());
//			dto.setWfAddressTown(rtvHospitalDetailDTO.getAddressTown());
//			dto.setWfAddressProvince(rtvHospitalDetailDTO.getAddressProvince());
//			dto.setWfAddressPostZip(rtvHospitalDetailDTO.getAddressPostZip());
//			dto.setTelephoneNumber(rtvHospitalDetailDTO.getTelephoneNumber());
//			dto.setWfFaxNumber(rtvHospitalDetailDTO.getFaxNumber());
//			dto.setCountry(rtvHospitalDetailDTO.getCountry());
			// DEBUG genFunctionCall Parameters DONE
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrInitializeSubfileRecordFromDBFRecord(DspPatientsPerHospitalState dto, DspPatientsPerHospitalGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	RtvWardDetailDTO rtvWardDetailDTO;
			//switchSUB 41 SUB    
			//switchBLK 1000082 BLK ACT
			// DEBUG genFunctionCall 1000083 ACT RTV Ward detail - Ward  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvWardDetailDTO = new RtvWardDetailDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvWardDetailDTO.setHospitalCode(gdo.getHospitalCode());
			rtvWardDetailDTO.setWardCode(gdo.getWardCode());
			// DEBUG genFunctionCall Service call
			rtvWardDetailService.execute(rtvWardDetailDTO);
			// DEBUG genFunctionCall Parameters OUT
			gdo.setWardName(rtvWardDetailDTO.getWardName());
			// DEBUG genFunctionCall Parameters DONE
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:175)
	 */
    private StepResult usrSubfileControlFunctionFields(DspPatientsPerHospitalState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 175 SUB    
			// Unprocessed SUB 175 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Control (Pre-Confirm) (Generated:72)
	 */
    private StepResult usrProcessSubfileControlPreConfirm(DspPatientsPerHospitalState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 72 SUB    
			//switchBLK 1000053 BLK CAS
			//switchSUB 1000053 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// CTL.*CMD key is *Cancel
				//switchBLK 1000062 BLK ACT
				// DEBUG genFunctionCall 1000063 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000056 BLK ACT
				// DEBUG genFunctionCall 1000057 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:170)
	 */
    private StepResult usrSubfileRecordFunctionFields(DspPatientsPerHospitalState dto, DspPatientsPerHospitalGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	RtvWardDetailDTO rtvWardDetailDTO;
			//switchSUB 170 SUB    
			//switchBLK 1000087 BLK ACT
			// DEBUG genFunctionCall 1000088 ACT RTV Ward detail - Ward  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvWardDetailDTO = new RtvWardDetailDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvWardDetailDTO.setHospitalCode(gdo.getHospitalCode());
			rtvWardDetailDTO.setWardCode(gdo.getWardCode());
			// DEBUG genFunctionCall Service call
			rtvWardDetailService.execute(rtvWardDetailDTO);
			// DEBUG genFunctionCall Parameters OUT
			gdo.setWardName(rtvWardDetailDTO.getWardName());
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Record (Pre-Confirm) (Generated:101)
	 */
    private StepResult usrProcessSubfileRecordPreConfirm(DspPatientsPerHospitalState dto, DspPatientsPerHospitalGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 101 SUB    
			// Unprocessed SUB 101 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Final processing (Pre-confirm) (Generated:222)
	 */
    private StepResult usrFinalProcessingPreConfirm(DspPatientsPerHospitalState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 222 SUB    
			// Unprocessed SUB 222 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process subfile control (Post-confirm) (Generated:225)
	 */
    private StepResult usrProcessSubfileControlPostConfirm(DspPatientsPerHospitalState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 225 SUB    
			// Unprocessed SUB 225 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Record (Post-Confirm Pass) (Generated:209)
	 */
    private StepResult usrProcessSubfileRecordPostConfirm(DspPatientsPerHospitalState dto, DspPatientsPerHospitalGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 209 SUB    
			// Unprocessed SUB 209 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Final processing (Post-confirm) (Generated:228)
	 */
    private StepResult usrFinalProcessingPostConfirm(DspPatientsPerHospitalState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 228 SUB    
			// Unprocessed SUB 228 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Generated:140)
	 */
    private StepResult usrProcessCommandKeys(DspPatientsPerHospitalState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 140 SUB    
			// Unprocessed SUB 140 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(DspPatientsPerHospitalState dto) {
        StepResult result = NO_ACTION;
        
        try {
        	//switchSUB 132 SUB    
			//switchBLK 1000046 BLK CAS
			//switchSUB 1000046 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// CTL.*CMD key is *Exit
				//switchBLK 1000068 BLK ACT
				// DEBUG genFunctionCall 1000069 ACT PAR.*Return code = CND.*Normal
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000049 BLK ACT
				// DEBUG genFunctionCall 1000050 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(DspPatientsPerHospitalState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvHospitalDetailService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvHospitalDetail(DspPatientsPerHospitalState state, RtvHospitalDetailParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(DspPatientsPerHospitalState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvWardDetailService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvWardDetail(DspPatientsPerHospitalState state, RtvWardDetailParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//


}
