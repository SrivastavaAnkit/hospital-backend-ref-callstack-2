package com.hospital.file.patient.createpatient;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.patient.Patient;
import com.hospital.file.patient.PatientId;
import com.hospital.file.patient.PatientRepository;
import com.hospital.model.PatientGenderEnum;
import com.hospital.model.PatientStatusEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class CreatePatientService  extends AbstractService<CreatePatientService, CreatePatientDTO>
{
    private final Step execute = define("execute", CreatePatientDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PatientRepository patientRepository;
	

    @Autowired
    public CreatePatientService() {
        super(CreatePatientService.class, CreatePatientDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(CreatePatientDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(CreatePatientDTO dto, CreatePatientDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		Patient patient = new Patient();
		patient.setPatientCode(dto.getPatientCode());
		patient.setHospitalCode(dto.getHospitalCode());
		patient.setWardCode(dto.getWardCode());
		patient.setPatientName(dto.getPatientName());
		patient.setPatientSurname(dto.getPatientSurname());
		patient.setAddressLine1(dto.getAddressLine1());
		patient.setAddressLine2(dto.getAddressLine2());
		patient.setAddressTownCity(dto.getAddressTownCity());
		patient.setAddressCountry(dto.getAddressCountry());
		patient.setAddressPostalZipCode(dto.getAddressPostalZipCode());
		patient.setPatientIdNumber(dto.getPatientIdNumber());
		patient.setPatientDateOfBirth(dto.getPatientDateOfBirth());
		patient.setPatientOccupation(dto.getPatientOccupation());
		patient.setPatientContactNumber(dto.getPatientContactNumber());
		patient.setPatientGender(dto.getPatientGender());
		patient.setPatientStatus(dto.getPatientStatus());
		/*patient.setAddedUser(dto.getAddedUser());
		patient.setAddedDate(dto.getAddedDate());
		patient.setAddedTime(dto.getAddedTime());
		patient.setChangedUser(dto.getChangedUser());
		patient.setChangedDate(dto.getChangedDate());
		patient.setChangedTime(dto.getChangedTime());*/

		processingBeforeDataUpdate(dto, patient);

		Patient patient2 = patientRepository.findById(patient.getId()).get();
		if (patient2 != null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0003"));
			processingIfDataRecordAlreadyExists(dto, patient2);
		}
		else {
            try {
				patientRepository.save(patient);
                dto.setReturnCode(ReturnCodeEnum.fromCode(""));
                processingAfterDataUpdate(dto, patient);
            } catch (Exception e) {
                dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
                processingIfDataUpdateError(dto, patient);
            }
        }

        return result;
	}

    private StepResult processingBeforeDataUpdate(CreatePatientDTO dto, Patient patient) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT DB1.Added User = JOB.*USER
		/*patient.setAddedUser(job.getUser());
		//switchBLK 1000005 BLK ACT
		//functionCall 1000006 ACT DB1.Added Date = JOB.*Job date
		patient.setAddedDate(LocalDate.now());
		//switchBLK 1000009 BLK ACT
		//functionCall 1000010 ACT DB1.Added Time = JOB.*Job time
		patient.setAddedTime(LocalTime.now());*/
       return NO_ACTION;
    }

    private StepResult processingIfDataRecordAlreadyExists(CreatePatientDTO dto, Patient patient) throws ServiceException {
		/**
		 * USER: Processing if Data Record Already Exists (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000013 BLK ACT
		//functionCall 1000014 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
    }

    private StepResult processingAfterDataUpdate(CreatePatientDTO dto, Patient patient) throws ServiceException {
		/**
		 * USER: Processing after Data Update (Generated:11)
		 */
		//switchSUB 11 SUB    
		//switchBLK 1000025 BLK ACT
		//functionCall 1000026 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
    }

    private StepResult processingIfDataUpdateError(CreatePatientDTO dto, Patient patient) throws ServiceException {
		/**
		 * Processing if Data Update Error (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000019 BLK ACT
		//functionCall 1000020 ACT PAR.USR Return Code = CND.Error
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("E"));
       return NO_ACTION;
    }
}
