package com.hospital.file.patient.createpatient;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.PatientGenderEnum;
import com.hospital.model.PatientStatusEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class CreatePatientDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private LocalDate addedDate;
	private LocalDate changedDate;
	private LocalDate patientDateOfBirth;
	private LocalTime addedTime;
	private LocalTime changedTime;
	private PatientGenderEnum patientGender;
	private PatientStatusEnum patientStatus;
	private ReturnCodeEnum returnCode;
	private String addedUser;
	private String addressCountry;
	private String addressLine1;
	private String addressLine2;
	private String addressPostalZipCode;
	private String addressTownCity;
	private String changedUser;
	private String hospitalCode;
	private String patientCode;
	private String patientName;
	private String patientOccupation;
	private String patientSurname;
	private String wardCode;
	private UsrReturnCodeEnum usrReturnCode;
	private long patientContactNumber;
	private long patientIdNumber;
	private String nextScreen;

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public String getAddressCountry() {
		return addressCountry;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public String getAddressPostalZipCode() {
		return addressPostalZipCode;
	}

	public String getAddressTownCity() {
		return addressTownCity;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public long getPatientContactNumber() {
		return patientContactNumber;
	}

	public LocalDate getPatientDateOfBirth() {
		return patientDateOfBirth;
	}

	public PatientGenderEnum getPatientGender() {
		return patientGender;
	}

	public long getPatientIdNumber() {
		return patientIdNumber;
	}

	public String getPatientName() {
		return patientName;
	}

	public String getPatientOccupation() {
		return patientOccupation;
	}

	public PatientStatusEnum getPatientStatus() {
		return patientStatus;
	}

	public String getPatientSurname() {
		return patientSurname;
	}

	public UsrReturnCodeEnum getUsrReturnCode() {
		return usrReturnCode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public void setAddressCountry(String addressCountry) {
		this.addressCountry = addressCountry;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public void setAddressPostalZipCode(String addressPostalZipCode) {
		this.addressPostalZipCode = addressPostalZipCode;
	}

	public void setAddressTownCity(String addressTownCity) {
		this.addressTownCity = addressTownCity;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public void setPatientContactNumber(long patientContactNumber) {
		this.patientContactNumber = patientContactNumber;
	}

	public void setPatientDateOfBirth(LocalDate patientDateOfBirth) {
		this.patientDateOfBirth = patientDateOfBirth;
	}

	public void setPatientGender(PatientGenderEnum patientGender) {
		this.patientGender = patientGender;
	}

	public void setPatientIdNumber(long patientIdNumber) {
		this.patientIdNumber = patientIdNumber;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public void setPatientOccupation(String patientOccupation) {
		this.patientOccupation = patientOccupation;
	}

	public void setPatientStatus(PatientStatusEnum patientStatus) {
		this.patientStatus = patientStatus;
	}

	public void setPatientSurname(String patientSurname) {
		this.patientSurname = patientSurname;
	}

	public void setUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
		this.usrReturnCode = usrReturnCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
