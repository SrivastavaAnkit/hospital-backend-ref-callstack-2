package com.hospital.file.patient;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository interface for model: Patient (TSAEREP).
 *
 * @author X2EGenerator
 */
@Repository
public interface PatientRepository extends PatientRepositoryCustom, JpaRepository<Patient, PatientId> {

	List<Patient> findAllByHospitalCode(String hospitalCode);

	List<Patient> findAllByHospitalCodeAndWardCode(String hospitalCode, String wardCode);

	List<Patient> findAllByIdPatientCode(String patientCode);

	List<Patient> findAllByWardCode(String wardCode);
}
