package com.hospital.file.patient.rtvpatientsperward;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.patient.Patient;
import com.hospital.file.patient.PatientId;
import com.hospital.file.patient.PatientRepository;
import com.hospital.model.PatientGenderEnum;
import com.hospital.model.PatientStatusEnum;
import com.hospital.model.ReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvPatientsPerWardService extends AbstractService<RtvPatientsPerWardService, RtvPatientsPerWardDTO>
{
    private final Step execute = define("execute", RtvPatientsPerWardDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PatientRepository patientRepository;
	
    @Autowired
    public RtvPatientsPerWardService()
    {
        super(RtvPatientsPerWardService.class, RtvPatientsPerWardDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvPatientsPerWardDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvPatientsPerWardDTO dto, RtvPatientsPerWardDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Patient> patientList = patientRepository.findAllByWardCode(dto.getWardCode());
		if (!patientList.isEmpty()) {
			for (Patient patient : patientList) {
				processDataRecord(dto, patient);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvPatientsPerWardDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT WRK.Count = CON.*ZERO
		dto.setWfCount(0);
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvPatientsPerWardDTO dto, Patient patient) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		//switchSUB 41 SUB    
		//switchBLK 1000008 BLK ACT
		//functionCall 1000009 ACT WRK.Count = WRK.Count + CON.1
		dto.setWfCount(dto.getWfCount() + 1);
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvPatientsPerWardDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		//switchSUB 52 SUB    
		//switchBLK 1000017 BLK ACT
		//functionCall 1000018 ACT WRK.Count = CON.*ZERO
		dto.setWfCount(0);
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvPatientsPerWardDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		//switchSUB 61 SUB    
		//switchBLK 1000013 BLK ACT
		//functionCall 1000014 ACT PAR.Count = WRK.Count
		dto.setCount(dto.getWfCount());
        return NO_ACTION;
    }
}
