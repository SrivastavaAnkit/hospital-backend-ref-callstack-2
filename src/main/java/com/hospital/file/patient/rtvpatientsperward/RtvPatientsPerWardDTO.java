package com.hospital.file.patient.rtvpatientsperward;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvPatientsPerWardDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private ReturnCodeEnum returnCode;
	private String wardCode;
	private long count;
	private String nextScreen;

	public long getCount() {
		return count;
	}

	public String getWardCode() {
		return wardCode;
	}

	public long getWfCount() {
		return count;// global.getLong("count");
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public void setWfCount(long wfCount) {
		this.count=wfCount;
		//global.setLong("count", wfCount);
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
