package com.hospital.file.patient.rtvpatientsforhospital;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.patient.Patient;
import com.hospital.file.patient.PatientId;
import com.hospital.file.patient.PatientRepository;
import com.hospital.file.diagnosis.rtvdiagnosisforpatient.RtvDiagnosisForPatientService;
import com.hospital.file.diagnosis.rtvdiagnosisforpatient.RtvDiagnosisForPatientDTO;
import com.hospital.model.PatientGenderEnum;
import com.hospital.model.PatientStatusEnum;
import com.hospital.model.PrescriptionStatusEnum;
import com.hospital.model.ReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvPatientsForHospitalService extends AbstractService<RtvPatientsForHospitalService, RtvPatientsForHospitalDTO>
{
    private final Step execute = define("execute", RtvPatientsForHospitalDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PatientRepository patientRepository;
	
	@Autowired
	private RtvDiagnosisForPatientService rtvDiagnosisForPatientService;
	
    @Autowired
    public RtvPatientsForHospitalService()
    {
        super(RtvPatientsForHospitalService.class, RtvPatientsForHospitalDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvPatientsForHospitalDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvPatientsForHospitalDTO dto, RtvPatientsForHospitalDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Patient> patientList = patientRepository.findAllByHospitalCodeAndWardCode(dto.getHospitalCode(), dto.getWardCode());
		if (!patientList.isEmpty()) {
			for (Patient patient : patientList) {
				processDataRecord(dto, patient);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvPatientsForHospitalDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Empty:48)
		 */
		//switchSUB 48 SUB    
		// Unprocessed SUB 48 -
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvPatientsForHospitalDTO dto, Patient patient) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		RtvDiagnosisForPatientDTO rtvDiagnosisForPatientDTO;
		//switchSUB 41 SUB    
		//switchBLK 1000004 BLK ACT
		//functionCall 1000005 ACT RTV Diagnosis for Patient - Diagnosis  *
		rtvDiagnosisForPatientDTO = new RtvDiagnosisForPatientDTO();
		rtvDiagnosisForPatientDTO.setPatientCode(patient.getPatientCode());
		rtvDiagnosisForPatientDTO.setHospitalCode(dto.getHospitalCode());
		rtvDiagnosisForPatientDTO.setWardCode(patient.getWardCode());
		rtvDiagnosisForPatientService.execute(rtvDiagnosisForPatientDTO);
		dto.setLclPrescriptionCost(rtvDiagnosisForPatientDTO.getPrescriptionCost());
		dto.setLclPrescriptionStatus(rtvDiagnosisForPatientDTO.getPrescriptionStatus());
		dto.setLclTotalAmount(rtvDiagnosisForPatientDTO.getTotalAmount());
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvPatientsForHospitalDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Empty:52)
		 */
		//switchSUB 52 SUB    
		// Unprocessed SUB 52 -
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvPatientsForHospitalDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Empty:61)
		 */
		//switchSUB 61 SUB    
		// Unprocessed SUB 61 -
        return NO_ACTION;
    }
}
