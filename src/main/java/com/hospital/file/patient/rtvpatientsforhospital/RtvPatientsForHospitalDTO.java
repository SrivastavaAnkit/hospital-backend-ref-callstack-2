package com.hospital.file.patient.rtvpatientsforhospital;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.PrescriptionStatusEnum;
import com.hospital.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvPatientsForHospitalDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private ReturnCodeEnum returnCode;
	private String hospitalCode;
	private String wardCode;
	private String nextScreen;
	private BigDecimal lclPrescriptionCost;
	private PrescriptionStatusEnum lclPrescriptionStatus;
	private BigDecimal lclTotalAmount;

	public String getHospitalCode() {
		return hospitalCode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public BigDecimal getLclPrescriptionCost() {
		return lclPrescriptionCost;
	}

	public PrescriptionStatusEnum getLclPrescriptionStatus() {
		return lclPrescriptionStatus;
	}

	public BigDecimal getLclTotalAmount() {
		return lclTotalAmount;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public void setLclPrescriptionCost(BigDecimal lclPrescriptionCost) {
		this.lclPrescriptionCost = lclPrescriptionCost;
	}

	public void setLclPrescriptionStatus(PrescriptionStatusEnum lclPrescriptionStatus) {
		this.lclPrescriptionStatus = lclPrescriptionStatus;
	}

	public void setLclTotalAmount(BigDecimal lclTotalAmount) {
		this.lclTotalAmount = lclTotalAmount;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
