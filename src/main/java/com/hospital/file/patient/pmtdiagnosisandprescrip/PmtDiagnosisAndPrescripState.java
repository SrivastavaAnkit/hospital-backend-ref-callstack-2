package com.hospital.file.patient.pmtdiagnosisandprescrip;

import com.hospital.file.patient.Patient;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;

/**
 * State for file 'Patient' (TSAEREP) and function 'PMT Diagnosis & Prescrip' (TSA1PVR).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class PmtDiagnosisAndPrescripState extends PmtDiagnosisAndPrescripDTO {
    private static final long serialVersionUID = -2965402250413324508L;

    // Local fields
    private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");

    // System fields
    private ReloadSubfileEnum _sysReloadSubfile = null;
    private ReturnCodeEnum _sysReturnCode = ReturnCodeEnum._STA_NORMAL;
    private boolean _sysErrorFound = false;

    public PmtDiagnosisAndPrescripState() {
    }

    public PmtDiagnosisAndPrescripState(Patient patient) {
       // setDtoFields(patient);
    }

    public void setLclReturnCode(ReturnCodeEnum returnCode) {
        this.lclReturnCode = returnCode;
    }

    public ReturnCodeEnum getLclReturnCode() {
        return lclReturnCode;
    }

   public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
       _sysReloadSubfile = reloadSubfile;
   }

   public ReloadSubfileEnum get_SysReloadSubfile() {
       return _sysReloadSubfile;
   }

   public void set_SysReturnCode(ReturnCodeEnum returnCode) {
       _sysReturnCode = returnCode;
   }

   public ReturnCodeEnum get_SysReturnCode() {
       return _sysReturnCode;
   }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
