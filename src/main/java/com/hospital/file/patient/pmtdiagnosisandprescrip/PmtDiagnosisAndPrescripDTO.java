package com.hospital.file.patient.pmtdiagnosisandprescrip;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.common.state.BaseDTO;
import com.hospital.file.patient.Patient;
import com.hospital.model.PatientGenderEnum;
import com.hospital.model.PatientStatusEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;

/**
 * Dto for file 'Patient' (TSAEREP) and function 'PMT Diagnosis &amp; Prescrip' (TSA1PVR).
 */
public class PmtDiagnosisAndPrescripDTO extends BaseDTO {
	private static final long serialVersionUID = 2826604779190108590L;

	private String patientCode = "";
	private String patientName = "";
	private String hospitalCode = "";
	private String wardCode = "";
	private String patientSurname = "";
	private String addressLine1 = "";
	private String addressLine2 = "";
	private String addressTownCity = "";
	private String addressCountry = "";
	private String addressPostalZipCode = "";
	private long patientIdNumber = 0L;
	private LocalDate patientDateOfBirth = null;
	private String patientOccupation = "";
	private long patientContactNumber = 0L;
	private PatientGenderEnum patientGender = null;
	private PatientStatusEnum patientStatus = null;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;

	// Local fields
	private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");

	private ReloadSubfileEnum reloadSubfile = null;

	public PmtDiagnosisAndPrescripDTO() {

	}

	public PmtDiagnosisAndPrescripDTO(Patient patient) {
		BeanUtils.copyProperties(patient, this);
	}

	public PmtDiagnosisAndPrescripDTO(String patientCode, String patientName, String hospitalCode, String wardCode, String patientSurname, String addressLine1, String addressLine2, String addressTownCity, String addressCountry, String addressPostalZipCode, long patientIdNumber, LocalDate patientDateOfBirth, String patientOccupation, long patientContactNumber, PatientGenderEnum patientGender, PatientStatusEnum patientStatus, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime) {
		this.patientCode = patientCode;
		this.patientName = patientName;
		this.hospitalCode = hospitalCode;
		this.wardCode = wardCode;
		this.patientSurname = patientSurname;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.addressTownCity = addressTownCity;
		this.addressCountry = addressCountry;
		this.addressPostalZipCode = addressPostalZipCode;
		this.patientIdNumber = patientIdNumber;
		this.patientDateOfBirth = patientDateOfBirth;
		this.patientOccupation = patientOccupation;
		this.patientContactNumber = patientContactNumber;
		this.patientGender = patientGender;
		this.patientStatus = patientStatus;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setPatientSurname(String patientSurname) {
		this.patientSurname = patientSurname;
	}

	public String getPatientSurname() {
		return patientSurname;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressTownCity(String addressTownCity) {
		this.addressTownCity = addressTownCity;
	}

	public String getAddressTownCity() {
		return addressTownCity;
	}

	public void setAddressCountry(String addressCountry) {
		this.addressCountry = addressCountry;
	}

	public String getAddressCountry() {
		return addressCountry;
	}

	public void setAddressPostalZipCode(String addressPostalZipCode) {
		this.addressPostalZipCode = addressPostalZipCode;
	}

	public String getAddressPostalZipCode() {
		return addressPostalZipCode;
	}

	public void setPatientIdNumber(long patientIdNumber) {
		this.patientIdNumber = patientIdNumber;
	}

	public long getPatientIdNumber() {
		return patientIdNumber;
	}

	public void setPatientDateOfBirth(LocalDate patientDateOfBirth) {
		this.patientDateOfBirth = patientDateOfBirth;
	}

	public LocalDate getPatientDateOfBirth() {
		return patientDateOfBirth;
	}

	public void setPatientOccupation(String patientOccupation) {
		this.patientOccupation = patientOccupation;
	}

	public String getPatientOccupation() {
		return patientOccupation;
	}

	public void setPatientContactNumber(long patientContactNumber) {
		this.patientContactNumber = patientContactNumber;
	}

	public long getPatientContactNumber() {
		return patientContactNumber;
	}

	public void setPatientGender(PatientGenderEnum patientGender) {
		this.patientGender = patientGender;
	}

	public PatientGenderEnum getPatientGender() {
		return patientGender;
	}

	public void setPatientStatus(PatientStatusEnum patientStatus) {
		this.patientStatus = patientStatus;
	}

	public PatientStatusEnum getPatientStatus() {
		return patientStatus;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void setLclReturnCode(ReturnCodeEnum returnCode) {
		this.lclReturnCode = returnCode;
	}

	public ReturnCodeEnum getLclReturnCode() {
		return lclReturnCode;
	}

	public void setReloadSubfile(ReloadSubfileEnum reloadSubfile) {
		this.reloadSubfile = reloadSubfile;
	}

	public ReloadSubfileEnum getReloadSubfile() {
		return reloadSubfile;
	}
}
