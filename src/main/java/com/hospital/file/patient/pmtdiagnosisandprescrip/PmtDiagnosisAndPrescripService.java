package com.hospital.file.patient.pmtdiagnosisandprescrip;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;
import static com.hospital.common.callstack.StepResult.callService;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.file.diagnosis.edtrcdeditdiagnosis.EdtrcdEditDiagnosisParams;
import com.hospital.file.diagnosis.edtrcdeditdiagnosis.EdtrcdEditDiagnosisService;
import com.hospital.file.patient.PatientRepository;
import com.hospital.file.patient.selectpatient.SelectPatientDTO;
import com.hospital.file.patient.selectpatient.SelectPatientParams;
import com.hospital.file.patient.selectpatient.SelectPatientService;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.DeferConfirmEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.support.JobContext;


/**
 * Service for file 'Patient' (TSAEREP) and function 'PMT Diagnosis & Prescrip' (TSA1PVR).
 */
@Service
public class PmtDiagnosisAndPrescripService extends AbstractService<PmtDiagnosisAndPrescripService, PmtDiagnosisAndPrescripState> {
    
    @Autowired
    private JobContext job;

    @Autowired
    private MessageSource messageSource;

	@Autowired
	private PatientRepository patientRepository;

    
    public static final String SCREEN_KEY = "pmtDiagnosisAndPrescrip";
    public static final String SCREEN_CONFIRM_PROMPT = "PmtDiagnosisAndPrescrip.key2";

    private final Step execute = define("execute",PmtDiagnosisAndPrescripParams.class, this::execute);
    private final Step response = define("response",PmtDiagnosisAndPrescripDTO.class, this::processResponse);
//    private final Step displayConfirmPrompt = define("displayConfirmPrompt",PmtDiagnosisAndPrescripDTO.class, this::processDisplayConfirmPrompt);

	private final Step promptSelectPatient = define("promptSelectPatient",SelectPatientParams.class, this::processPromptSelectPatient);
	
	private final Step serviceEdtrcdEditDiagnosis = define("serviceEdtrcdEditDiagnosis",EdtrcdEditDiagnosisParams.class, this::processServiceEdtrcdEditDiagnosis);
	//private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
	//private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
	
    	
    @Autowired
    public PmtDiagnosisAndPrescripService()
    {
        super(PmtDiagnosisAndPrescripService.class, PmtDiagnosisAndPrescripState.class);
    }

    @Override
    public Step getInitialStep()
    {
        return execute;
    }

    /**
     * PmtDiagnosisAndPrescrip service starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(PmtDiagnosisAndPrescripState state, PmtDiagnosisAndPrescripParams params)  {
        StepResult result = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        initialize(state);
        result = conductScreenConversation(state);

        return result;
    }

    /**
     * PmtDiagnosisAndPrescrip service initialization.
     * @param state   - Service state class.
     * @return
     */
    private void initialize(PmtDiagnosisAndPrescripState state) {
        usrInitializeProgram(state);
        usrScreenFunctionFields(state);
        usrLoadScreen(state);
    }

    /**
     * SCREEN_KEY display processing loop method.
     * @param state   - Service state class.
     * @return
     */
    private StepResult conductScreenConversation(PmtDiagnosisAndPrescripState state)
    {
        StepResult result = NO_ACTION;

        PmtDiagnosisAndPrescripDTO dto = new PmtDiagnosisAndPrescripDTO();
        BeanUtils.copyProperties(state, dto);
        result = callScreen(SCREEN_KEY, dto).thenCall(response);

        return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param dto - returned screen model.
     * @return
     */
    private StepResult processResponse(PmtDiagnosisAndPrescripState state, PmtDiagnosisAndPrescripDTO dto)
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(dto, state);
        if (CmdKeyEnum.isExit(state.get_SysCmdKey()))
        {
            result = closedown(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey()))
        {
            //TODO: processResetRequest(state);//synon built-in function
            result = conductScreenConversation(state);
        }
        else if (CmdKeyEnum.isHelp(state.get_SysCmdKey()))
        {
            //TODO:processHelpRequest(state);//synon built-in function
            result = conductScreenConversation(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
			switch (state.get_SysEntrySelected())
			    {
			        case "patientCode":
			            SelectPatientParams selectPatientParams = new SelectPatientParams();
			            BeanUtils.copyProperties(state, selectPatientParams);
			            result = StepResult.callService(SelectPatientService.class, selectPatientParams).thenCall(promptSelectPatient);
			            break;
			        default:
			            System.out.println("Field "+ state.get_SysEntrySelected() + " is not promptable");
			            result = conductScreenConversation(state);
			            break;
			    }
        }
        else
        {
            validateScreenInput(state);
            if (!state.get_SysErrorFound()){
                //BeanUtils.copyProperties(state, dto);
                //TODO: for now, confirm prompt is skipped
                result = usrUserDefinedAction(state);//callScreen(SCREEN_CONFIRM_PROMPT, dto).thenCall(displayConfirmPrompt);
                if(result != NO_ACTION) {
                    return result;
                }
            }
            result = conductScreenConversation(state);
        }

        return result;
    }

//    /**
//     * SCREEN_CONFIRM_PROMPT returned response processing method.
//     * @param state - Service state class.
//     * @param dto - returned screen model.
//     * @return
//     */
//    private StepResult processDisplayConfirmPrompt(PmtDiagnosisAndPrescripState state, PmtDiagnosisAndPrescripDTO dto) {
//        StepResult result = NO_ACTION;
//
//        BeanUtils.copyProperties(dto, state);
//        if (!state.getDeferConfirm().getCode().equals(DeferConfirmEnum._PROCEED_TO_CONFIRM.getCode()))
//        {
//            usrUserDefinedAction(state);
//        }
//        result = conductScreenConversation(state);
//
//        return result;
//    }

    /**
     * Validate input in SCREEN_KEY.
     * @param state - Service state class.
     * @return
     */
    private void validateScreenInput(PmtDiagnosisAndPrescripState state)
    {
        usrProcessCommandKeys(state);
        checkFields(state);
        if (!state.get_SysErrorFound())
        {
            usrValidateFields(state);
            checkRelations(state);
            if (!state.get_SysErrorFound())
            {
                usrScreenFunctionFields(state);
                usrValidateRelations(state);
            }
        }
    }

    /**
     * Check fields set in SCREEN_KEY.
     * @param state - Service state class.
     * @return
     */
    private void checkFields(PmtDiagnosisAndPrescripState state) {

    }

    /**
     * Check relations set in SCREEN_KEY.
     * @param state - Service state class.
     * @return
     */
    private void checkRelations(PmtDiagnosisAndPrescripState state) {

    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(PmtDiagnosisAndPrescripState state)
    {
        StepResult result = NO_ACTION;

        result = usrExitProgramProcessing(state);
        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);

        PmtDiagnosisAndPrescripParams params = new PmtDiagnosisAndPrescripParams();
        BeanUtils.copyProperties(state, params);
        result = StepResult.returnFromService(params);

        return result;
    }


    
/**
 * ---------------------- Programmatic user-point --------------------------
 */
	/**
	 * USER: Initialize Program (Generated:10)
	 */
    private StepResult usrInitializeProgram(PmtDiagnosisAndPrescripState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 10 SUB    
			// Unprocessed SUB 10 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Screen Function Fields (Generated:1052)
	 */
    private StepResult usrScreenFunctionFields(PmtDiagnosisAndPrescripState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1052 SUB    
			// Unprocessed SUB 1052 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Load Screen (Generated:1058)
	 */
    private StepResult usrLoadScreen(PmtDiagnosisAndPrescripState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1058 SUB    
			// Unprocessed SUB 1058 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Generated:1035)
	 */
    private StepResult usrProcessCommandKeys(PmtDiagnosisAndPrescripState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1035 SUB    
			//switchBLK 1000038 BLK CAS
			//switchSUB 1000038 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				//switchBLK 1000033 BLK ACT
				// DEBUG genFunctionCall 1000034 ACT Exit program - return code CND.E
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Fields (Generated:1065)
	 */
    private StepResult usrValidateFields(PmtDiagnosisAndPrescripState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1065 SUB    
			// Unprocessed SUB 1065 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Relations (Generated:29)
	 */
    private StepResult usrValidateRelations(PmtDiagnosisAndPrescripState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 29 SUB    
			// Unprocessed SUB 29 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: User Defined Action (Generated:41)
	 */
    private StepResult usrUserDefinedAction(PmtDiagnosisAndPrescripState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 41 SUB    
			//switchBLK 1000001 BLK CAS
			//switchSUB 1000001 BLK CAS
			if (!dto.getPatientCode().equals("")) {
				// DTL.Patient Code is Not Blank
				//switchBLK 1000004 BLK ACT
				// DEBUG genFunctionCall 1000005 ACT EDTRCD Edit Diagnosis - Diagnosis  *
				//TODO: commented nextScreem and invoked respective service.
				//dto.setNextScreen("EdtrcdEditDiagnosis");
				EdtrcdEditDiagnosisParams params = new EdtrcdEditDiagnosisParams();
		        BeanUtils.copyProperties(dto, params);
		        result = StepResult.callService(EdtrcdEditDiagnosisService.class, params).thenCall(serviceEdtrcdEditDiagnosis);
				//switchBLK 1000007 BLK CAS
				//switchSUB 1000007 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000017 BLK ACT
					// DEBUG genFunctionCall 1000018 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
				}//switchSUB 1000010 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000014 BLK ACT
					// DEBUG genFunctionCall 1000015 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Generated:1021)
	 */
    private StepResult usrExitProgramProcessing(PmtDiagnosisAndPrescripState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1021 SUB    
			//switchBLK 1000025 BLK CAS
			//switchSUB 1000025 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// DTL.*CMD key is *Exit
				//switchBLK 1000028 BLK ACT
				// DEBUG genFunctionCall 1000029 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */

    /**
     * EdtrcdEditDiagnosisService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceEdtrcdEditDiagnosis(PmtDiagnosisAndPrescripState state, EdtrcdEditDiagnosisParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }
//
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(PmtDiagnosisAndPrescripState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(PmtDiagnosisAndPrescripState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//

    /**
     * SelectPatientService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processPromptSelectPatient(PmtDiagnosisAndPrescripState state, SelectPatientParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        result = conductScreenConversation(state);

        return result;
    }


}
