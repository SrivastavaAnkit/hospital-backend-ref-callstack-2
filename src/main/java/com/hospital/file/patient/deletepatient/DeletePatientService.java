package com.hospital.file.patient.deletepatient;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.patient.Patient;
import com.hospital.file.patient.PatientId;
import com.hospital.file.patient.PatientRepository;
import com.hospital.model.PatientGenderEnum;
import com.hospital.model.PatientStatusEnum;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class DeletePatientService  extends AbstractService<DeletePatientService, DeletePatientDTO>
{
    private final Step execute = define("execute", DeletePatientDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PatientRepository patientRepository;
	

    @Autowired
    public DeletePatientService() {
        super(DeletePatientService.class, DeletePatientDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(DeletePatientDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(DeletePatientDTO dto, DeletePatientDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataUpdate(dto);
		PatientId patientId = new PatientId();
		patientId.setPatientCode(dto.getPatientCode());
		try {
			patientRepository.deleteById(patientId);
			patientRepository.flush();
			dto.setReturnCode(ReturnCodeEnum.fromCode(""));
			processingAfterDataUpdate(dto);
		} catch (Exception e) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
		}

       return result;
	}

	private StepResult processingBeforeDataUpdate(DeletePatientDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Empty:9)
		 */
		
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(DeletePatientDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Empty:17)
		 */
		
       return NO_ACTION;
	}
}
