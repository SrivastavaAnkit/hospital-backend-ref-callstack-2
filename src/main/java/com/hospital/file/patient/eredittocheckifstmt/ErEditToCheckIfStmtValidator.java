package com.hospital.file.patient.eredittocheckifstmt;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


import com.hospital.common.exception.ServiceException;

import com.hospital.model.CmdKeyEnum;
import com.hospital.model.PatientGenderEnum;
import com.hospital.model.PatientStatusEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;


/**
 * Spring Validator for file 'Patient' (TSAEREP) and function 'ER Edit to check If stmt' (TSBHE1R).
 *
 * @author X2EGenerator
 */
@Component
public class ErEditToCheckIfStmtValidator implements Validator {
	
	

	@Override
	public boolean supports(Class<?> clazz) {
		return ErEditToCheckIfStmtDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors e) {
		if (!(object instanceof ErEditToCheckIfStmtDTO)) {
			e.reject("Not a valid ErEditToCheckIfStmtDTO");
		} else {
			ErEditToCheckIfStmtDTO dto = (ErEditToCheckIfStmtDTO)object;

			/**
			 * Validate Key Fields
			 *
			 */
			if ("".equals(dto.getPatientCode())) {
				e.rejectValue("patientCode", "value.required");
			}
			ErEditToCheckIfStmtParams params = new ErEditToCheckIfStmtParams();
			BeanUtils.copyProperties(dto, params);
			try {
				/**
				 * USER: Validate Detail Screen Fields (Generated:502)
				 */
				//switchSUB 502 SUB    
				//switchBLK 1000040 BLK CAS
				//switchSUB 1000040 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
					// DTL.*CMD key is *Cancel
					//switchBLK 1000043 BLK ACT
					//functionCall 1000044 ACT PAR.*Return code = CND.E
					dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
					//switchBLK 1000049 BLK ACT
					//functionCall 1000050 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
				//switchBLK 1000052 BLK TXT
				// 
				//switchBLK 1000001 BLK CAS
				//switchSUB 1000001 BLK CAS
				if (dto.getPatientStatus() == PatientStatusEnum.fromCode("H")) {
					// DTL.Patient Status is Hospital Plan only
					//switchBLK 1000004 BLK ACT
					//functionCall 1000005 ACT DTL.Amount = CON.5000.00
					dto.setAmount(new BigDecimal("5000.00"));
				} else //switchSUB 1000010 SUB    
				if (dto.getPatientStatus() == PatientStatusEnum.fromCode("Z")) {
					// DTL.Patient Status is No Medical Aid
					//switchBLK 1000012 BLK ACT
					//functionCall 1000013 ACT DTL.Amount = CON.12000.00
					dto.setAmount(new BigDecimal("12000.00"));
				} else //switchSUB 1000018 SUB    
				if (dto.getPatientStatus() == PatientStatusEnum.fromCode("P")) {
					// DTL.Patient Status is Private Paid Upfront
					//switchBLK 1000022 BLK ACT
					//functionCall 1000023 ACT DTL.Amount = CON.8000.00
					dto.setAmount(new BigDecimal("8000.00"));
				}//switchSUB 1000020 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000030 BLK ACT
					//functionCall 1000031 ACT DTL.Amount = CON.1000.00
					dto.setAmount(new BigDecimal("1000.00"));
				}
				//switchBLK 1000036 BLK ACT
				//functionCall 1000037 ACT PGM.*Reload subfile = CND.*YES
				//dto.setReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
			} catch (Exception se) {
				se.printStackTrace();
			}
			try {
				/**
				 * USER: Validate Detail Screen Relations (Generated:268)
				 */
				//switchSUB 268 SUB    
				//switchBLK 1000066 BLK CAS
				//switchSUB 1000066 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
					// DTL.*CMD key is *Cancel
					//switchBLK 1000069 BLK ACT
					//functionCall 1000070 ACT PAR.*Return code = CND.E
					dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
					//switchBLK 1000075 BLK ACT
					//functionCall 1000076 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			} catch (Exception se) {
				se.printStackTrace();
			}
		}
	}
}
