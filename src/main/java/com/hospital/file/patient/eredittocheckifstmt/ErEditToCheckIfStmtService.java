package com.hospital.file.patient.eredittocheckifstmt;

import java.io.IOException;
import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;


import com.hospital.file.patient.Patient;
import com.hospital.file.patient.PatientId;
import com.hospital.file.patient.PatientRepository;
import com.hospital.file.patient.changepatient.ChangePatientService;
import com.hospital.file.patient.changepatient.ChangePatientDTO;
import com.hospital.file.patient.createpatient.CreatePatientService;
import com.hospital.file.patient.createpatient.CreatePatientDTO;
import com.hospital.file.patient.deletepatient.DeletePatientService;
import com.hospital.file.patient.deletepatient.DeletePatientDTO;


import com.hospital.common.exception.ServiceException;

import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ProgramModeEnum;
import com.hospital.model.PatientStatusEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.PatientGenderEnum;
import com.hospital.model.UsrReturnCodeEnum;
/**
 * Service controller for 'ER Edit to check If stmt' (TSBHE1R) of file 'Patient' (TSAEREP)
 *
 * @author X2EGenerator
 */
@Service
public class ErEditToCheckIfStmtService extends AbstractService<ErEditToCheckIfStmtService, ErEditToCheckIfStmtDTO>
{
    
    	@Autowired
    	private PatientRepository patientRepository;
    
    	@Autowired
    	private ChangePatientService changePatientService;
    
    	@Autowired
    	private CreatePatientService createPatientService;
    
    	@Autowired
    	private DeletePatientService deletePatientService;
    
        
    public static final String SCREEN_KEY = "ErEditToCheckIfStmt.key";
    public static final String SCREEN_DTL = "ErEditToCheckIfStmt.detail";
    public static final String SCREEN_CFM = "ErEditToCheckIfStmt.confirm";
    
    private final Step execute = define("execute", ErEditToCheckIfStmtParams.class, this::execute);
    private final Step keyScreenResponse = define("keyscreen", ErEditToCheckIfStmtDTO.class, this::processKeyScreenResponse);
    private final Step detailScreenResponse = define("dtlscreen", ErEditToCheckIfStmtDTO.class, this::processDetailScreenResponse);
    private final Step confirmScreenResponse = define("cfmscreen", ErEditToCheckIfStmtDTO.class, this::processConfirmScreenResponse);
    
        
    @Autowired
    public ErEditToCheckIfStmtService()
    {
        super(ErEditToCheckIfStmtService.class, ErEditToCheckIfStmtDTO.class);
    }
    
    @Override
    public Step getInitialStep()
    {
        return execute;
    }
    
    /**
     * EDTRCD controller starting point.
     * @param dto - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(ErEditToCheckIfStmtDTO dto, ErEditToCheckIfStmtParams params)
    {
        StepResult result = NO_ACTION;
    
        BeanUtils.copyProperties(params, dto);
    
        result = conductKeyScreenConversation(dto);
    
        return result;
    }
    
    /**
     * SCREEN_KEY initial processing loop method.
     * @param dto - Service state class.
     * @return
     */
    private StepResult conductKeyScreenConversation(ErEditToCheckIfStmtDTO dto) 
    {
        StepResult result = NO_ACTION;
    
        dto.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
    
        result = displayKeyScreenConversation(dto);
    
        return result;
    }
    
    /**
     * SCREEN_KEY display processing loop method.
     * @param dto - Service state class.
     * @return
     */
    private StepResult displayKeyScreenConversation(ErEditToCheckIfStmtDTO dto)
    {
        StepResult result = NO_ACTION;
    
        if (dto.getConductKeyScreenConversation()) {
            result = callScreen(SCREEN_KEY, dto).thenCall(keyScreenResponse);
        }
    
        return result;
    }
    
    /**
     * SCREEN_KEY returned response processing method.
     * @param dto - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processKeyScreenResponse(ErEditToCheckIfStmtDTO dto, ErEditToCheckIfStmtDTO model)
    {
        StepResult result = NO_ACTION;
    
        BeanUtils.copyProperties(model, dto);
    
        if (CmdKeyEnum.isHelp(dto.get_SysCmdKey())) {
            result = displayKeyScreenConversation(dto);
        }
        else if (CmdKeyEnum.isReset(dto.get_SysCmdKey())) {
            result = conductKeyScreenConversation(dto);
        }
        else if (CmdKeyEnum.isExit(dto.get_SysCmdKey())) {
            result = usrExitCommandProcessing(dto);
        }
        else {
            dbfReadDataRecord(dto);
    
            result = conductDetailScreenConversation(dto);
        }
    
        return result;
    }
    
    /**
     * SCREEN_DTL display processing loop method.
     * @param dto - Service state class.
     * @return
     */
    private StepResult conductDetailScreenConversation(ErEditToCheckIfStmtDTO dto)
    {
        StepResult result = NO_ACTION;
    
        if (dto.getConductDetailScreenConversation()) {
            result = callScreen(SCREEN_DTL, dto).thenCall(detailScreenResponse);
        }
    
        return result;
    }
    
    /**
     * SCREEN_DETAIL returned response processing method.
     * @param dto - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processDetailScreenResponse(ErEditToCheckIfStmtDTO dto, ErEditToCheckIfStmtDTO model)
    {
        StepResult result = NO_ACTION;
    
        BeanUtils.copyProperties(model, dto);
    
        if (CmdKeyEnum.isHelp(dto.get_SysCmdKey())) {
            result = conductDetailScreenConversation(dto);
        }
        else if (CmdKeyEnum.isReset(dto.get_SysCmdKey())) {
            result = conductDetailScreenConversation(dto);
        }
        else if (CmdKeyEnum.isExit(dto.get_SysCmdKey())) {
            result = usrExitCommandProcessing(dto);
        }
        else {
    //        if (CmdKeyEnum.isDelete(dto.getCmdKey())) {
    //            dto.setProgramMode(ProgramModeEnum._STA_DELETE);
    //        }
            result = callScreen(SCREEN_CFM, dto).thenCall(confirmScreenResponse);
        }
    
        return result;
    }
    
    /**
     * SCREEN_CONFIRM returned response processing method.
     * @param dto - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processConfirmScreenResponse(ErEditToCheckIfStmtDTO dto, ErEditToCheckIfStmtDTO model)
    {
        StepResult result = NO_ACTION;
    
    //    if (dto.getProgramMode() == ProgramModeEnum._STA_DELETE) {
    //        dbfDeleteDataRecord(dto);
    //    }
    //    else
    {
            usrValidateDetailScreenFields(dto);
            usrValidateDetailScreenRelations(dto);
            if (dto.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
                dbfUpdateDataRecord(dto);
            }
            else {
                dbfCreateDataRecord(dto);
            }
        }
    
        result = conductKeyScreenConversation(dto);
    
        return result;
    }
        
    /**
     * ------------------------ Generated DBF methods ----------------------
     */
    
    private StepResult dbfReadDataRecord(ErEditToCheckIfStmtDTO dto)
    {
        StepResult result = NO_ACTION;
    
        PatientId patientId = new PatientId(dto.getPatientCode());
        Patient patient = patientRepository.findById(patientId).get();
    
        if (patient == null) {
            dto.set_SysProgramMode(ProgramModeEnum._STA_ADD);
        }
        else {
            dto.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
        }
        return result;
    }
    
    private StepResult dbfCreateDataRecord(ErEditToCheckIfStmtDTO dto)
    {
        StepResult result = NO_ACTION;
        try {
            CreatePatientDTO createPatientDTO = new CreatePatientDTO();
            createPatientDTO.setPatientCode(dto.getPatientCode());
            createPatientDTO.setHospitalCode(dto.getHospitalCode());
            createPatientDTO.setWardCode(dto.getWardCode());
            createPatientDTO.setPatientName(dto.getPatientName());
            createPatientDTO.setPatientSurname(dto.getPatientSurname());
            createPatientDTO.setAddressLine1(dto.getAddressLine1());
            createPatientDTO.setAddressLine2(dto.getAddressLine2());
            createPatientDTO.setAddressTownCity(dto.getAddressTownCity());
            createPatientDTO.setAddressCountry(dto.getAddressCountry());
            createPatientDTO.setAddressPostalZipCode(dto.getAddressPostalZipCode());
            createPatientDTO.setPatientIdNumber(dto.getPatientIdNumber());
            createPatientDTO.setPatientDateOfBirth(dto.getPatientDateOfBirth());
            createPatientDTO.setPatientOccupation(dto.getPatientOccupation());
            createPatientDTO.setPatientContactNumber(dto.getPatientContactNumber());
            createPatientDTO.setPatientGender(dto.getPatientGender());
            createPatientDTO.setPatientStatus(dto.getPatientStatus());
    	    createPatientService.execute(createPatientDTO);
             dto.setUsrReturnCode(createPatientDTO.getUsrReturnCode());
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }
    private StepResult dbfDeleteDataRecord(ErEditToCheckIfStmtDTO dto)
    {
        StepResult result = NO_ACTION;
        try {
            DeletePatientDTO deletePatientDTO = new DeletePatientDTO();
            deletePatientDTO.setPatientCode(dto.getPatientCode());
    	    deletePatientService.execute(deletePatientDTO);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    
        return result;
    }
    
    private StepResult dbfUpdateDataRecord(ErEditToCheckIfStmtDTO dto)
    {
        StepResult result = NO_ACTION;
        try {
    	PatientId patientId = new PatientId(dto.getPatientCode());
    	if (patientRepository.existsById(patientId)) {
                ChangePatientDTO changePatientDTO = new ChangePatientDTO();
                changePatientDTO.setPatientCode(dto.getPatientCode());
                changePatientDTO.setHospitalCode(dto.getHospitalCode());
                changePatientDTO.setWardCode(dto.getWardCode());
                changePatientDTO.setPatientName(dto.getPatientName());
                changePatientDTO.setPatientSurname(dto.getPatientSurname());
                changePatientDTO.setAddressLine1(dto.getAddressLine1());
                changePatientDTO.setAddressLine2(dto.getAddressLine2());
                changePatientDTO.setAddressTownCity(dto.getAddressTownCity());
                changePatientDTO.setAddressCountry(dto.getAddressCountry());
                changePatientDTO.setAddressPostalZipCode(dto.getAddressPostalZipCode());
                changePatientDTO.setPatientIdNumber(dto.getPatientIdNumber());
                changePatientDTO.setPatientDateOfBirth(dto.getPatientDateOfBirth());
                changePatientDTO.setPatientOccupation(dto.getPatientOccupation());
                changePatientDTO.setPatientContactNumber(dto.getPatientContactNumber());
                changePatientDTO.setPatientGender(dto.getPatientGender());
                changePatientDTO.setPatientStatus(dto.getPatientStatus());
    	        changePatientService.execute(changePatientDTO);
                 dto.setUsrReturnCode(changePatientDTO.getUsrReturnCode());
            }
            else {
                throw new ServiceException("diagnosis.nf");
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }
    
    
/**
 * ------------------ Generated ActionDiagram UserPoint -----------------
 */

    private StepResult usrValidateDetailScreenFields(ErEditToCheckIfStmtDTO dto)
    {
        try
        {
           /**
			 * USER: Validate Detail Screen Fields (Generated:502)
			 */
			//switchSUB 502 SUB    
			//switchBLK 1000040 BLK CAS
			//switchSUB 1000040 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				//switchBLK 1000043 BLK ACT
				//functionCall 1000044 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000049 BLK ACT
				//functionCall 1000050 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
			//switchBLK 1000052 BLK TXT
			// 
			//switchBLK 1000001 BLK CAS
			//switchSUB 1000001 BLK CAS
			if (dto.getPatientStatus() == PatientStatusEnum.fromCode("H")) {
				// DTL.Patient Status is Hospital Plan only
				//switchBLK 1000004 BLK ACT
				//functionCall 1000005 ACT DTL.Amount = CON.5000.00
				dto.setAmount(new BigDecimal("5000.00"));
			} else //switchSUB 1000010 SUB    
			if (dto.getPatientStatus() == PatientStatusEnum.fromCode("Z")) {
				// DTL.Patient Status is No Medical Aid
				//switchBLK 1000012 BLK ACT
				//functionCall 1000013 ACT DTL.Amount = CON.12000.00
				dto.setAmount(new BigDecimal("12000.00"));
			} else //switchSUB 1000018 SUB    
			if (dto.getPatientStatus() == PatientStatusEnum.fromCode("P")) {
				// DTL.Patient Status is Private Paid Upfront
				//switchBLK 1000022 BLK ACT
				//functionCall 1000023 ACT DTL.Amount = CON.8000.00
				dto.setAmount(new BigDecimal("8000.00"));
			}//switchSUB 1000020 SUB    
			 else {
				// *OTHERWISE
				//switchBLK 1000030 BLK ACT
				//functionCall 1000031 ACT DTL.Amount = CON.1000.00
				dto.setAmount(new BigDecimal("1000.00"));
			}
			//switchBLK 1000036 BLK ACT
			//functionCall 1000037 ACT PGM.*Reload subfile = CND.*YES
			//dto.setReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return NO_ACTION;
    }

    private StepResult usrValidateDetailScreenRelations(ErEditToCheckIfStmtDTO dto)
    {
        try
        {
           /**
			 * USER: Validate Detail Screen Relations (Generated:268)
			 */
			//switchSUB 268 SUB    
			//switchBLK 1000066 BLK CAS
			//switchSUB 1000066 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				//switchBLK 1000069 BLK ACT
				//functionCall 1000070 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000075 BLK ACT
				//functionCall 1000076 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return NO_ACTION;
    }

    private StepResult usrExitCommandProcessing(ErEditToCheckIfStmtDTO dto)
    {
        try
        {
           /**
			 * USER: Exit Program Processing (Generated:64)
			 */
			//switchSUB 64 SUB    
			//switchBLK 1000053 BLK CAS
			//switchSUB 1000053 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// KEY.*CMD key is *Exit
				//switchBLK 1000056 BLK ACT
				//functionCall 1000057 ACT PAR.*Return code = CND.*Normal
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000062 BLK ACT
				//functionCall 1000063 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return NO_ACTION;
    }


    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
}
