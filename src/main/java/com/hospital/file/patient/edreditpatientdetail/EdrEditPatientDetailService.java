package com.hospital.file.patient.edreditpatientdetail;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.exception.ServiceException;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailDTO;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailService;
import com.hospital.file.patient.Patient;
import com.hospital.file.patient.PatientId;
import com.hospital.file.patient.PatientRepository;
import com.hospital.file.patient.changepatient.ChangePatientDTO;
import com.hospital.file.patient.changepatient.ChangePatientService;
import com.hospital.file.patient.createpatient.CreatePatientDTO;
import com.hospital.file.patient.createpatient.CreatePatientService;
import com.hospital.file.patient.deletepatient.DeletePatientDTO;
import com.hospital.file.patient.deletepatient.DeletePatientService;
import com.hospital.file.patient.selectpatient.SelectPatientDTO;
import com.hospital.file.patient.selectpatient.SelectPatientParams;
import com.hospital.file.patient.selectpatient.SelectPatientService;
import com.hospital.file.ward.rtvwarddetail.RtvWardDetailDTO;
import com.hospital.file.ward.rtvwarddetail.RtvWardDetailService;
import com.hospital.file.ward.selectward.SelectWardParams;
import com.hospital.file.ward.selectward.SelectWardService;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ProgramModeEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.support.JobContext;
/**
 * Service controller for 'EDR Edit Patient Detail' (TSAPE1R) of file 'Patient' (TSAEREP)
 *
 * @author X2EGenerator
 *///
@Service																					//TODO: …………….. added EdrEditPatientDetailState instead of EdrEditPatientDetailDTO
public class EdrEditPatientDetailService extends AbstractService<EdrEditPatientDetailService, EdrEditPatientDetailState>
{
    
    @Autowired
    private JobContext job;
    
    @Autowired
    private PatientRepository patientRepository;
    
    	@Autowired
    	private ChangePatientService changePatientService;
    
    	@Autowired
    	private CreatePatientService createPatientService;
    
    	@Autowired
    	private DeletePatientService deletePatientService;
    
    	@Autowired
    	private RtvHospitalDetailService rtvHospitalDetailService;
    
    	@Autowired
    	private RtvWardDetailService rtvWardDetailService;
    
        @Autowired
    	private MessageSource messageSource;   
        
    public static final String SCREEN_KEY = "edrEditPatientDetailEntryPanel";
    public static final String SCREEN_DTL = "edrEditPatientDetailPanel";
    public static final String SCREEN_CFM = "EdrEditPatientDetail.confirm";
    												//TODO: ……………Added EdrEditPatientDetailParams instead of EdrEditPatientDetailDTO
    private final Step execute = define("execute", EdrEditPatientDetailParams.class, this::execute);
    private final Step keyScreenResponse = define("keyScreen", EdrEditPatientDetailDTO.class, this::processKeyScreenResponse);
    private final Step detailScreenResponse = define("dtlScreen", EdrEditPatientDetailDTO.class, this::processDetailScreenResponse);
    private final Step confirmScreenResponse = define("cfmScreen", EdrEditPatientDetailDTO.class, this::processConfirmScreenResponse);
    private final Step promptSelectPatient = define("promptSelectPatient",SelectPatientParams.class, this::processPromptSelectPatient);
    private final Step promptSelectWard = define("promptSelectWard",SelectWardParams.class, this::processPromptSelectWard);
    private final Step serviceSelectPatient = define("serviceSelectPatient",SelectPatientParams.class, this::processServiceSelectPatient);
    
        @Autowired
    public EdrEditPatientDetailService()
    {
        super(EdrEditPatientDetailService.class, EdrEditPatientDetailState.class);
    }
    
    @Override
    public Step getInitialStep()
    {
        return execute;
    }
    
    /**
     * EDTRCD controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(EdrEditPatientDetailState state, EdrEditPatientDetailParams params)
    {
        StepResult result = NO_ACTION;
    
        if(params != null) {
            BeanUtils.copyProperties(params, state);
        }
    
        state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
    
        result = conductKeyScreenConversation(state);
    
        return result;
    }
    
    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductKeyScreenConversation(EdrEditPatientDetailState state) 
    {
        StepResult result = NO_ACTION;
    
        usrInitializeKeyScreen(state);
        result = displayKeyScreenConversation(state);
    
        return result;
    }
    
    /**
     * SCREEN_KEY display processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult displayKeyScreenConversation(EdrEditPatientDetailState state)
    {
        StepResult result = NO_ACTION;
    
        if (state.get_SysConductKeyScreenConversation()) {
            EdrEditPatientDetailDTO model = new EdrEditPatientDetailDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_KEY, model).thenCall(keyScreenResponse);
        }
    
        return result;
    }
    
    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processKeyScreenResponse(EdrEditPatientDetailState state, EdrEditPatientDetailDTO model)
    {
        StepResult result = NO_ACTION;
    
        BeanUtils.copyProperties(model, state);
    
        if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            result = displayKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            result = conductKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
           result = closedown(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
    		switch (state.get_SysEntrySelected())
    		    {
    		        case "patientCode":
    		            SelectPatientParams selectPatientParams = new SelectPatientParams();
    		            BeanUtils.copyProperties(state, selectPatientParams);
    		            result = StepResult.callService(SelectPatientService.class, selectPatientParams).thenCall(promptSelectPatient);
    		            break;
    		        default:
    		            System.out.println("Field "+ state.get_SysEntrySelected() + " is not promptable");
    		            result = displayKeyScreenConversation(state);
    		            break;
    		    }
        }
        else if(CmdKeyEnum.isChangeMode(state.get_SysCmdKey())) {
            if(state.get_SysProgramMode() == ProgramModeEnum._STA_ADD) {
                state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            }
            else {
                state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
            }
            result = conductKeyScreenConversation(state);
        }
        else {
            checkKeyFields(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = displayKeyScreenConversation(state);
                return result;
            }
            dbfReadDataRecord(state);
            if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
                usrInitializeExistingScreen(state);
            }
    
            result = conductDetailScreenConversation(state);
        }
    
        return result;
    }
    
    /**
     * Check key fields set in SCREEN_KEY.
     * For now it's just if fields are null or strings are empty
     * @param state - Service state class.
     * @return
     */
    private void checkKeyFields(EdrEditPatientDetailState state) {
       if(state.getPatientCode() == null || state.getPatientCode().isEmpty())
       {
           state.set_SysErrorFound(true);
           state.getMessageMap().put("patientCode", 
    		   messageSource.getMessage("value.required", null, null, null));
           state.setMessageMap(state.getMessageMap());
       }
    }
    
    /**
     * SCREEN_DTL display processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductDetailScreenConversation(EdrEditPatientDetailState state)
    {
        StepResult result = NO_ACTION;
    
        if (state.get_SysConductDetailScreenConversation()) {
            EdrEditPatientDetailDTO model = new EdrEditPatientDetailDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_DTL, model).thenCall(detailScreenResponse);
        }
    
        return result;
    }
    
    /**
     * SCREEN_DETAIL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processDetailScreenResponse(EdrEditPatientDetailState state, EdrEditPatientDetailDTO model)
    {
        StepResult result = NO_ACTION;
    
        BeanUtils.copyProperties(model, state);
    
        if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            result = conductDetailScreenConversation(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            result = conductDetailScreenConversation(state);
        }
        else if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            result = closedown(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
    		switch (state.get_SysEntrySelected())
    		    {
    		        case "wardCode":
    		            SelectWardParams selectWardParams = new SelectWardParams();
    		            BeanUtils.copyProperties(state, selectWardParams);
    		            result = StepResult.callService(SelectWardService.class, selectWardParams).thenCall(promptSelectWard);
    		            break;
    		        default:
    		            System.out.println("Field "+ state.get_SysEntrySelected() + " is not promptable");
    		            result = conductDetailScreenConversation(state);
    		            break;
    		    }
        }
        else if (CmdKeyEnum.isKeyScreen(state.get_SysCmdKey())) {
            result = conductKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isDelete(state.get_SysCmdKey())) {
            //delete action confirmed in client side
            dbfDeleteDataRecord(state);
            result = conductDetailScreenConversation(state);
        }
        else {
            checkFields(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = conductDetailScreenConversation(state);
                return result;
            }
            usrValidateDetailScreenFields(state);
            checkRelations(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = conductDetailScreenConversation(state);
                return result;
            }
            usrValidateDetailScreenRelations(state);
            //TODO: make confirm screen
            result = processConfirmScreenResponse(state, null);//callScreen(SCREEN_CFM, state).thenCall(confirmScreenResponse);
        }
    
        return result;
    }
    /**
     * SCREEN_CONFIRM returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processConfirmScreenResponse(EdrEditPatientDetailState state, EdrEditPatientDetailDTO model)
    {
        StepResult result = NO_ACTION;
    
        if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
            dbfUpdateDataRecord(state);
        }
        else {
            dbfCreateDataRecord(state);
        }
    
        result = conductKeyScreenConversation(state);
    
        return result;
    }
    
    /**
     * Check fields set in SCREEN_DETAIL.
     * @param state - Service state class.
     * @return
     */
    private void checkFields(EdrEditPatientDetailState state) {
    
    }
    
    /**
     * Check relations set in SCREEN_DETAIL.
     * @param state - Service state class.
     * @return
     */
    private void checkRelations(EdrEditPatientDetailState state) {
    
    }
    
    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(EdrEditPatientDetailState state)
    {
        StepResult result = NO_ACTION;
    
        result = usrExitCommandProcessing(state);
    
        EdrEditPatientDetailParams params = new EdrEditPatientDetailParams();
        BeanUtils.copyProperties(state, params);
        result = StepResult.returnFromService(params);
    
        return result;
    }
        
    /**
     * ------------------------ Generated DBF methods ----------------------
     */
    
    private StepResult dbfReadDataRecord(EdrEditPatientDetailState dto)
    {
        StepResult result = NO_ACTION;
    
        PatientId patientId = new PatientId(dto.getPatientCode());
        Patient patient = patientRepository.findById(patientId).orElse(null);
    
        if (patient == null) {
            dto.set_SysProgramMode(ProgramModeEnum._STA_ADD);
        }
        else {
            dto.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            //dto.setHospitalName(patient.getHospitalName());
            //TODO: commented below line as referenced entity has some issues
            //BeanUtils.copyProperties(patient, dto);
        }
        return result;
    }
    
    private StepResult dbfCreateDataRecord(EdrEditPatientDetailState dto)
    {
        StepResult result = NO_ACTION;
        try {
            /**
    		 * USER: Create Object (Generated:425)
    		 */
    		CreatePatientDTO createPatientDTO;
    		//switchBLK 425 BLK ACT
    		// DEBUG genFunctionCall 426 ACT Create Patient - Patient  *
    		// DEBUG genFunctionCall ServiceDtoVariable
    		createPatientDTO = new CreatePatientDTO();
    		// DEBUG genFunctionCall Parameters IN
    		createPatientDTO.setPatientCode(dto.getPatientCode());
    		createPatientDTO.setHospitalCode(dto.getHospitalCode());
    		createPatientDTO.setWardCode(dto.getWardCode());
    		createPatientDTO.setPatientName(dto.getPatientName());
    		createPatientDTO.setPatientSurname(dto.getPatientSurname());
    		createPatientDTO.setAddressLine1(dto.getAddressLine1());
    		createPatientDTO.setAddressLine2(dto.getAddressLine2());
    		createPatientDTO.setAddressTownCity(dto.getAddressTownCity());
    		createPatientDTO.setAddressCountry(dto.getAddressCountry());
    		createPatientDTO.setAddressPostalZipCode(dto.getAddressPostalZipCode());
    		createPatientDTO.setPatientIdNumber(dto.getPatientIdNumber());
    		createPatientDTO.setPatientDateOfBirth(dto.getPatientDateOfBirth());
    		createPatientDTO.setPatientOccupation(dto.getPatientOccupation());
    		createPatientDTO.setPatientContactNumber(dto.getPatientContactNumber());
    		createPatientDTO.setPatientGender(dto.getPatientGender());
    		createPatientDTO.setPatientStatus(dto.getPatientStatus());
    		// DEBUG genFunctionCall Service call
    		createPatientService.execute(createPatientDTO);
    		// DEBUG genFunctionCall Parameters OUT
    		dto.setLclUsrReturnCode(createPatientDTO.getUsrReturnCode());
    		// DEBUG genFunctionCall Parameters DONE
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }
    private StepResult dbfDeleteDataRecord(EdrEditPatientDetailState dto)
    {
        StepResult result = NO_ACTION;
        try {
            /**
    		 * USER: Delete Object (Generated:383)
    		 */
    		DeletePatientDTO deletePatientDTO;
    		//switchBLK 383 BLK ACT
    		// DEBUG genFunctionCall 384 ACT Delete Patient - Patient  *
    		// DEBUG genFunctionCall ServiceDtoVariable
    		deletePatientDTO = new DeletePatientDTO();
    		// DEBUG genFunctionCall Parameters IN
    		deletePatientDTO.setPatientCode(dto.getPatientCode());
    		// DEBUG genFunctionCall Service call
    		deletePatientService.execute(deletePatientDTO);
    		// DEBUG genFunctionCall Parameters OUT
    		// DEBUG genFunctionCall Parameters DONE
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    
        return result;
    }
    
    private StepResult dbfUpdateDataRecord(EdrEditPatientDetailState dto)
    {
        StepResult result = NO_ACTION;
        try {
    		PatientId patientId = new PatientId(dto.getPatientCode());
    		if (patientRepository.existsById(patientId)) {
    
            	/**
    			 * USER: Change Object (Generated:427)
    			 */
    			ChangePatientDTO changePatientDTO;
    			//switchBLK 427 BLK ACT
    			// DEBUG genFunctionCall 428 ACT Change Patient - Patient  *
    			// DEBUG genFunctionCall ServiceDtoVariable
    			changePatientDTO = new ChangePatientDTO();
    			// DEBUG genFunctionCall Parameters IN
    			changePatientDTO.setPatientCode(dto.getPatientCode());
    			changePatientDTO.setHospitalCode(dto.getHospitalCode());
    			changePatientDTO.setWardCode(dto.getWardCode());
    			changePatientDTO.setPatientName(dto.getPatientName());
    			changePatientDTO.setPatientSurname(dto.getPatientSurname());
    			changePatientDTO.setAddressLine1(dto.getAddressLine1());
    			changePatientDTO.setAddressLine2(dto.getAddressLine2());
    			changePatientDTO.setAddressTownCity(dto.getAddressTownCity());
    			changePatientDTO.setAddressCountry(dto.getAddressCountry());
    			changePatientDTO.setAddressPostalZipCode(dto.getAddressPostalZipCode());
    			changePatientDTO.setPatientIdNumber(dto.getPatientIdNumber());
    			changePatientDTO.setPatientDateOfBirth(dto.getPatientDateOfBirth());
    			changePatientDTO.setPatientOccupation(dto.getPatientOccupation());
    			changePatientDTO.setPatientContactNumber(dto.getPatientContactNumber());
    			changePatientDTO.setPatientGender(dto.getPatientGender());
    			changePatientDTO.setPatientStatus(dto.getPatientStatus());
    			// DEBUG genFunctionCall Service call
    			changePatientService.execute(changePatientDTO);
    			// DEBUG genFunctionCall Parameters OUT
    			dto.setLclUsrReturnCode(changePatientDTO.getUsrReturnCode());
    			// DEBUG genFunctionCall Parameters DONE
    	        }
            else {
                throw new ServiceException("diagnosis.nf");
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }
    
    
/**
 * ------------------ Generated ActionDiagram UserPoint -----------------
 */

    private StepResult usrInitializeKeyScreen(EdrEditPatientDetailState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Initialize Key Screen (Generated:484)
			 */
			//switchSUB 484 SUB    
			//switchBLK 1000067 BLK CAS
			//switchSUB 1000067 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("04"))) {
				// KEY.*CMD key is *Prompt
				//switchBLK 1000070 BLK ACT
				// DEBUG genFunctionCall 1000071 ACT Select Patient - Patient  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				SelectPatientParams selectPatientParams = new SelectPatientParams();
				BeanUtils.copyProperties(dto, selectPatientParams);
				result = StepResult.callService(SelectPatientService.class, selectPatientParams).thenCall(serviceSelectPatient);
				}
			}

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrInitializeExistingScreen(EdrEditPatientDetailState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * Initialize Detail Screen (Existing Record) (Generated:242)
			 */
			RtvHospitalDetailDTO rtvHospitalDetailDTO;
			RtvWardDetailDTO rtvWardDetailDTO;
			//switchSUB 242 SUB    
			//switchBLK 1000001 BLK ACT
			//functionCall 1000002 ACT RTV Hospital detail - Hospital  *
			rtvHospitalDetailDTO = new RtvHospitalDetailDTO();
			rtvHospitalDetailDTO.setHospitalCode(dto.getHospitalCode());
			rtvHospitalDetailService.execute(rtvHospitalDetailDTO);
			dto.setHospitalName(rtvHospitalDetailDTO.getHospitalName());
			dto.setWfAddressStreet(rtvHospitalDetailDTO.getAddressStreet());
			dto.setWfAddressTown(rtvHospitalDetailDTO.getAddressTown());
			dto.setWfAddressProvince(rtvHospitalDetailDTO.getAddressProvince());
			dto.setWfAddressPostZip(rtvHospitalDetailDTO.getAddressPostZip());
			dto.setWfTelephoneNumber(rtvHospitalDetailDTO.getTelephoneNumber());
			dto.setWfFaxNumber(rtvHospitalDetailDTO.getFaxNumber());
			dto.setWfCountry(rtvHospitalDetailDTO.getCountry().getCode());
			//switchBLK 1000012 BLK ACT
			//functionCall 1000013 ACT RTV Ward detail - Ward  *
			rtvWardDetailDTO = new RtvWardDetailDTO();
			rtvWardDetailDTO.setHospitalCode(dto.getHospitalCode());
			rtvWardDetailDTO.setWardCode(dto.getWardCode());
			rtvWardDetailService.execute(rtvWardDetailDTO);
			dto.setWardName(rtvWardDetailDTO.getWardName());
			//switchBLK 1000049 BLK ACT
			//functionCall 1000050 ACT DTL.Gender Value = Condition name of DTL.Patient Gender
			//dto.setGenderValue(dto.getPatientGender().getDescription()); // Retrieve condition
			//switchBLK 1000055 BLK ACT
			//functionCall 1000056 ACT DTL.Patient Status value = Condition name of DTL.Patient Status
			//dto.setPatientStatusValue(dto.getPatientStatus().getDescription()); // Retrieve condition
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrValidateDetailScreenFields(EdrEditPatientDetailState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Validate Detail Screen Fields (Generated:502)
			 */
			//switchSUB 502 SUB    
			//switchBLK 1000076 BLK CAS
			//switchSUB 1000076 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				//switchBLK 1000088 BLK ACT
				//functionCall 1000089 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000079 BLK ACT
				//functionCall 1000080 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrValidateDetailScreenRelations(EdrEditPatientDetailState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Validate Detail Screen Relations (Generated:268)
			 */
			RtvHospitalDetailDTO rtvHospitalDetailDTO;
			RtvWardDetailDTO rtvWardDetailDTO;
			//switchSUB 268 SUB    
			//switchBLK 1000101 BLK CAS
			//switchSUB 1000101 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				//switchBLK 1000104 BLK ACT
				//functionCall 1000105 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000110 BLK ACT
				//functionCall 1000111 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
			//switchBLK 1000113 BLK TXT
			// 
			//switchBLK 1000033 BLK ACT
			//functionCall 1000034 ACT RTV Hospital detail - Hospital  *
			rtvHospitalDetailDTO = new RtvHospitalDetailDTO();
			rtvHospitalDetailDTO.setHospitalCode(dto.getHospitalCode());
			rtvHospitalDetailService.execute(rtvHospitalDetailDTO);
			dto.setHospitalName(rtvHospitalDetailDTO.getHospitalName());
			dto.setWfAddressStreet(rtvHospitalDetailDTO.getAddressStreet());
			dto.setWfAddressTown(rtvHospitalDetailDTO.getAddressTown());
			dto.setWfAddressProvince(rtvHospitalDetailDTO.getAddressProvince());
			dto.setWfAddressPostZip(rtvHospitalDetailDTO.getAddressPostZip());
			dto.setWfTelephoneNumber(rtvHospitalDetailDTO.getTelephoneNumber());
			dto.setWfFaxNumber(rtvHospitalDetailDTO.getFaxNumber());
			dto.setWfCountry(rtvHospitalDetailDTO.getCountry().getCode());
			//switchBLK 1000044 BLK ACT
			//functionCall 1000045 ACT RTV Ward detail - Ward  *
			rtvWardDetailDTO = new RtvWardDetailDTO();
			rtvWardDetailDTO.setHospitalCode(dto.getHospitalCode());
			rtvWardDetailDTO.setWardCode(dto.getWardCode());
			rtvWardDetailService.execute(rtvWardDetailDTO);
			dto.setWardName(rtvWardDetailDTO.getWardName());
			//switchBLK 1000061 BLK ACT
			// DEBUG genFunctionCall 1000062 ACT PGM.*Reload subfile = CND.*YES
			dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrExitCommandProcessing(EdrEditPatientDetailState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Exit Program Processing (Generated:64)
			 */
			//switchSUB 64 SUB    
			//switchBLK 1000082 BLK CAS
			//switchSUB 1000082 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// KEY.*CMD key is *Exit
				//switchBLK 1000095 BLK ACT
				//functionCall 1000096 ACT PAR.*Return code = CND.*Normal
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				//return StepResult.returnFromService(dto);
				//switchBLK 1000085 BLK ACT
				//functionCall 1000086 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }



   private StepResult postCallSelectPatient(EdrEditPatientDetailDTO dto,SelectPatientDTO fromScreen) {
		
		BeanUtils.copyProperties(fromScreen.getGdo(), dto);
		return callScreen(SCREEN_KEY, dto).thenCall(keyScreenResponse);
	}
	


    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */

    /**
     * SelectPatientService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceSelectPatient(EdrEditPatientDetailState state, SelectPatientParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }
//
//    /**
//     * DeletePatientService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceSelectPatient(EdrEditPatientDetailDTO dto, SelectPatientParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        return result;
//    }
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ChangePatientService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChangePatient(EdrEditPatientDetailState state, ChangePatientParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvHospitalDetailService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvHospitalDetail(EdrEditPatientDetailState state, RtvHospitalDetailParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvWardDetailService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvWardDetail(EdrEditPatientDetailState state, RtvWardDetailParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvcndService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvcnd(EdrEditPatientDetailState state, RtvcndParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(EdrEditPatientDetailState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(EdrEditPatientDetailState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//

    /**
     * SelectPatientService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processPromptSelectPatient(EdrEditPatientDetailState state, SelectPatientParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        result = displayKeyScreenConversation(state);

        return result;
    }

    /**
     * SelectPatientService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processPromptSelectWard(EdrEditPatientDetailState state, SelectWardParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        result = displayKeyScreenConversation(state);

        return result;
    }

}
