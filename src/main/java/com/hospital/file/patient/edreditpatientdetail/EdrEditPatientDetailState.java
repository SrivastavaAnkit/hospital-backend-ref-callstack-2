package com.hospital.file.patient.edreditpatientdetail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.hospital.file.patient.Patient;
import com.hospital.model.GlobalContext;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.UsrReturnCodeEnum;

/**
 * State for file 'Patient' (TSAEREP) and function 'EDR Edit Patient Detail' (TSAPE1R).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
@Configurable
public class EdrEditPatientDetailState extends EdrEditPatientDetailDTO {
    private static final long serialVersionUID = -9100887554128392567L;

    @Autowired
    private GlobalContext globalCtx;


    // Local fields
    private UsrReturnCodeEnum lclUsrReturnCode = null;

    // System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysConductKeyScreenConversation = true;
    private boolean _sysConductDetailScreenConversation = true;
    private boolean _sysErrorFound = false;

    public EdrEditPatientDetailState() {
    }

    public EdrEditPatientDetailState(Patient patient) {
        setDtoFields(patient);
    }

    public void setWfAddressPostZip(String addressPostZip) {
        //globalCtx.setString("addressPostZip", addressPostZip);
    }

    public String getWfAddressPostZip() {
        return null;//globalCtx.getString("addressPostZip");
    }

    public void setWfAddressProvince(String addressProvince) {
        //globalCtx.setString("addressProvince", addressProvince);
    }

    public String getWfAddressProvince() {
        return null;//globalCtx.getString("addressProvince");
    }

    public void setWfAddressStreet(String addressStreet) {
        //globalCtx.setString("addressStreet", addressStreet);
    }

    public String getWfAddressStreet() {
        return null;//globalCtx.getString("addressStreet");
    }

    public void setWfAddressTown(String addressTown) {
        //globalCtx.setString("addressTown", addressTown);
    }

    public String getWfAddressTown() {
        return null;//globalCtx.getString("addressTown");
    }

    public void setWfCountry(String country) {
        //globalCtx.setString("country", country);
    }

    public String getWfCountry() {
        return null;//globalCtx.getString("country");
    }

    public void setWfFaxNumber(long faxNumber) {
        //globalCtx.setLong("faxNumber", faxNumber);
    }

    public long getWfFaxNumber() {
        return 0L;//globalCtx.getLong("faxNumber");
    }

    public void setWfTelephoneNumber(long telephoneNumber) {
        //globalCtx.setLong("telephoneNumber", telephoneNumber);
    }

    public long getWfTelephoneNumber() {
        return 0L;//globalCtx.getLong("telephoneNumber");
    }

    public void setLclUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
        this.lclUsrReturnCode = usrReturnCode;
    }

    public UsrReturnCodeEnum getLclUsrReturnCode() {
        return lclUsrReturnCode;
    }

   public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
       _sysReloadSubfile = reloadSubfile;
   }

   public ReloadSubfileEnum get_SysReloadSubfileEnum() {
       return _sysReloadSubfile;
   }

    public boolean get_SysConductKeyScreenConversation() {
        return _sysConductKeyScreenConversation;
    }

    public void set_SysConductKeyScreenConversation(boolean conductKeyScreenConversation) {
        _sysConductKeyScreenConversation = conductKeyScreenConversation;
    }

    public boolean get_SysConductDetailScreenConversation() {
        return _sysConductDetailScreenConversation;
    }

    public void set_SysConductDetailScreenConversation(boolean conductDetailScreenConversation) {
        _sysConductDetailScreenConversation = conductDetailScreenConversation;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
