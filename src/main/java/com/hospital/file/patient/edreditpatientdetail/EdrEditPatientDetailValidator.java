package com.hospital.file.patient.edreditpatientdetail;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailDTO;
import com.hospital.file.hospital.rtvhospitaldetail.RtvHospitalDetailService;
import com.hospital.file.ward.rtvwarddetail.RtvWardDetailDTO;
import com.hospital.file.ward.rtvwarddetail.RtvWardDetailService;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;


/**
 * Spring Validator for file 'Patient' (TSAEREP) and function 'EDR Edit Patient Detail' (TSAPE1R).
 *
 * @author X2EGenerator
 */
@Component
public class EdrEditPatientDetailValidator implements Validator {
	
	
	@Autowired
	private RtvHospitalDetailService rtvHospitalDetailService;
	
	@Autowired
	private RtvWardDetailService rtvWardDetailService;
	

	@Override
	public boolean supports(Class<?> clazz) {
		return EdrEditPatientDetailDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors e) {
		if (!(object instanceof EdrEditPatientDetailDTO)) {
			e.reject("Not a valid EdrEditPatientDetailDTO");
		} else {
			EdrEditPatientDetailDTO dto = (EdrEditPatientDetailDTO)object;

			/**
			 * Validate Key Fields
			 *
			 */
			if ("".equals(dto.getPatientCode())) {
				e.rejectValue("patientCode", "value.required");
			}
			EdrEditPatientDetailParams params = new EdrEditPatientDetailParams();
			BeanUtils.copyProperties(dto, params);
			try {
				/**
				 * USER: Validate Detail Screen Fields (Generated:502)
				 */
				//switchSUB 502 SUB    
				//switchBLK 1000076 BLK CAS
				//switchSUB 1000076 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
					// DTL.*CMD key is *Cancel
					//switchBLK 1000088 BLK ACT
					//functionCall 1000089 ACT PAR.*Return code = CND.E
					dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
					//switchBLK 1000079 BLK ACT
					//functionCall 1000080 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			} catch (Exception se) {
				se.printStackTrace();
			}
			try {
				/**
				 * USER: Validate Detail Screen Relations (Generated:268)
				 */
				RtvHospitalDetailDTO rtvHospitalDetailDTO;
				RtvWardDetailDTO rtvWardDetailDTO;
				//switchSUB 268 SUB    
				//switchBLK 1000101 BLK CAS
				//switchSUB 1000101 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
					// DTL.*CMD key is *Cancel
					//switchBLK 1000104 BLK ACT
					//functionCall 1000105 ACT PAR.*Return code = CND.E
					dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
					//switchBLK 1000110 BLK ACT
					//functionCall 1000111 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
				//switchBLK 1000113 BLK TXT
				// 
				//switchBLK 1000033 BLK ACT
				//functionCall 1000034 ACT RTV Hospital detail - Hospital  *
				rtvHospitalDetailDTO = new RtvHospitalDetailDTO();
				rtvHospitalDetailDTO.setHospitalCode(dto.getHospitalCode());
				rtvHospitalDetailService.execute(rtvHospitalDetailDTO);
				dto.setHospitalName(rtvHospitalDetailDTO.getHospitalName());
				dto.setWfAddressStreet(rtvHospitalDetailDTO.getAddressStreet());
				dto.setWfAddressTown(rtvHospitalDetailDTO.getAddressTown());
				dto.setWfAddressProvince(rtvHospitalDetailDTO.getAddressProvince());
				dto.setWfAddressPostZip(rtvHospitalDetailDTO.getAddressPostZip());
				dto.setWfTelephoneNumber(rtvHospitalDetailDTO.getTelephoneNumber());
				dto.setWfFaxNumber(rtvHospitalDetailDTO.getFaxNumber());
				dto.setWfCountry(rtvHospitalDetailDTO.getCountry().getCode());
				//switchBLK 1000044 BLK ACT
				//functionCall 1000045 ACT RTV Ward detail - Ward  *
				rtvWardDetailDTO = new RtvWardDetailDTO();
				rtvWardDetailDTO.setHospitalCode(dto.getHospitalCode());
				rtvWardDetailDTO.setWardCode(dto.getWardCode());
				rtvWardDetailService.execute(rtvWardDetailDTO);
				dto.setWardName(rtvWardDetailDTO.getWardName());
				//switchBLK 1000061 BLK ACT
				//functionCall 1000062 ACT PGM.*Reload subfile = CND.*YES
				//((Object) dto).setReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
			} catch (Exception se) {
				se.printStackTrace();
			}
		}
	}
}
