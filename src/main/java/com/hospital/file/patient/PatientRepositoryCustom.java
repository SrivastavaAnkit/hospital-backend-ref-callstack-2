package com.hospital.file.patient;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.hospital.model.CountryEnum;

import com.hospital.file.patient.selectpatient.SelectPatientGDO;
import com.hospital.file.patient.dspfdisplaypatientswrd.DspfDisplayPatientsWrdGDO;
import com.hospital.file.patient.dspfdisplaypatientwrd2.DspfDisplayPatientWrd2GDO;
import com.hospital.file.patient.dsppatientsperhospital.DspPatientsPerHospitalGDO;
import com.hospital.file.patient.pmtdiagnosisandprescrip.PmtDiagnosisAndPrescripDTO;

/**
 * Custom Spring Data JPA repository interface for model: Patient (TSAEREP).
 *
 * @author X2EGenerator
 */
@Repository
public interface PatientRepositoryCustom {

	/**
	 * 
	 * @param patientCode Patient Code
	 */
	void deletePatient(String patientCode);

	/**
	 * 
	 * @param patientCode Patient Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of SelectPatientGDO
	 */
	RestResponsePage<SelectPatientGDO> selectPatient(String patientCode, Pageable pageable);

	/**
	 * 
	 * @param wardCode Ward Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of Patient
	 */
	RestResponsePage<Patient> rtvPatientsPerWard(String wardCode, Pageable pageable);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param wardCode Ward Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of DspfDisplayPatientsWrdGDO
	 */
	RestResponsePage<DspfDisplayPatientsWrdGDO> dspfDisplayPatientsWrd(String hospitalCode, String wardCode, Pageable pageable);

	/**
	 * 
	 * @param patientCode Patient Code
	 * @return Patient
	 */
	Patient rtvPatientName(String patientCode);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param wardCode Ward Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of DspfDisplayPatientWrd2GDO
	 */
	RestResponsePage<DspfDisplayPatientWrd2GDO> dspfDisplayPatientWrd2(String hospitalCode, String wardCode, Pageable pageable);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param country Country
	 * @param countryName Country Name
	 * @param telephoneNumber Telephone Number
	 * @param wardCode Ward Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of DspPatientsPerHospitalGDO
	 */
	RestResponsePage<DspPatientsPerHospitalGDO> dspPatientsPerHospital(String hospitalCode, CountryEnum country, String countryName, long telephoneNumber, String wardCode, Pageable pageable);

	/**
	 * 
	 * @return List of Patient
	 */
	List<Patient> prtPatientsPerHospital();

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of Patient
	 */
	RestResponsePage<Patient> rtvPatientPerHospital(String hospitalCode, Pageable pageable);

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param wardCode Ward Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of Patient
	 */
	RestResponsePage<Patient> rtvPatientsForHospital(String hospitalCode, String wardCode, Pageable pageable);

	/**
	 * 
	 * @param pageable a Pageable object used for pagination
	 * @return Page of PmtDiagnosisAndPrescripDTO
	 */
	RestResponsePage<PmtDiagnosisAndPrescripDTO> pmtDiagnosisAndPrescrip(Pageable pageable);
}
