package com.hospital.file.patient.rtvpatientname;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.patient.Patient;
import com.hospital.file.patient.PatientId;
import com.hospital.file.patient.PatientRepository;
import com.hospital.model.PatientGenderEnum;
import com.hospital.model.PatientStatusEnum;
import com.hospital.model.ReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvPatientNameService extends AbstractService<RtvPatientNameService, RtvPatientNameDTO>
{
    private final Step execute = define("execute", RtvPatientNameDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PatientRepository patientRepository;
	
    @Autowired
    public RtvPatientNameService()
    {
        super(RtvPatientNameService.class, RtvPatientNameDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvPatientNameDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvPatientNameDTO dto, RtvPatientNameDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Patient> patientList = patientRepository.findAllByIdPatientCode(dto.getPatientCode());
		if (!patientList.isEmpty()) {
			for (Patient patient : patientList) {
				processDataRecord(dto, patient);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvPatientNameDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Empty:48)
		 */
		//switchSUB 48 SUB    
		// Unprocessed SUB 48 -
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvPatientNameDTO dto, Patient patient) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		//switchSUB 41 SUB    
		//switchBLK 1000008 BLK ACT
		//functionCall 1000009 ACT PAR = DB1 By name
		dto.setPatientName(patient.getPatientName()); dto.setPatientSurname(patient.getPatientSurname());
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvPatientNameDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		//switchSUB 52 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT PAR = CON By name
		dto.setPatientName(""); dto.setPatientSurname("");
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvPatientNameDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Empty:61)
		 */
		//switchSUB 61 SUB    
		// Unprocessed SUB 61 -
        return NO_ACTION;
    }
}
