package com.hospital.file.patient.rtvpatientname;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvPatientNameDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private ReturnCodeEnum returnCode;
	private String patientCode;
	private String patientName;
	private String patientSurname;
	private String nextScreen;

	public String getPatientCode() {
		return patientCode;
	}

	public String getPatientName() {
		return patientName;
	}

	public String getPatientSurname() {
		return patientSurname;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public void setPatientSurname(String patientSurname) {
		this.patientSurname = patientSurname;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
