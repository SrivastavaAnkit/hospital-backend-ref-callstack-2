package com.hospital.file.patient.edreditpatientdetailr;
import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.hospital.model.GlobalContext;
import com.hospital.file.patient.Patient;
import com.hospital.model.PatientGenderEnum;
import com.hospital.model.PatientStatusEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.CountryEnum;

import com.hospital.common.state.BaseDTO;

/**
 * Dto for file 'Patient' (TSAEREP) and function 'EDR Edit Patient Detail R' (TSBQE1R).
 */
@Configurable
public class EdrEditPatientDetailRDTO extends BaseDTO {
    private static final long serialVersionUID = -1311654192739890276L;

    @Autowired
    private GlobalContext globalCtx;

    private String patientCode = "";
    private String sflselPromptText = "";
    private String hospitalCode = "";
    private String hospitalName = "";
    private String wardCode = "";
    private String wardName = "";
    private String patientName = "";
    private String patientSurname = "";
    private String addressLine1 = "";
    private String addressLine2 = "";
    private String addressTownCity = "";
    private String addressCountry = "";
    private String addressPostalZipCode = "";
    private long patientIdNumber = 0L;
    private LocalDate patientDateOfBirth = null;
    private String patientOccupation = "";
    private long patientContactNumber = 0L;
    private PatientGenderEnum patientGender = null;
    private String genderValue = "";
    private PatientStatusEnum patientStatus = null;
    private String addedUser = "";
    private LocalDate addedDate = null;
    private LocalTime addedTime = null;
    private String changedUser = "";
    private LocalDate changedDate = null;
    private LocalTime changedTime = null;
    private String patientStatusValue = "";
    private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");

private boolean conductKeyScreenConversation = true;
private boolean conductDetailScreenConversation = true;

    public EdrEditPatientDetailRDTO() {

    }

    public EdrEditPatientDetailRDTO(Patient patient) {
        setDtoFields(patient);
    }

    public String getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public String getSflselPromptText() {
        return sflselPromptText;
    }

    public void setSflselPromptText(String sflselPromptText) {
        this.sflselPromptText = sflselPromptText;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getWardCode() {
        return wardCode;
    }

    public void setWardCode(String wardCode) {
        this.wardCode = wardCode;
    }

    public String getWardName() {
        return wardName;
    }

    public void setWardName(String wardName) {
        this.wardName = wardName;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientSurname() {
        return patientSurname;
    }

    public void setPatientSurname(String patientSurname) {
        this.patientSurname = patientSurname;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressTownCity() {
        return addressTownCity;
    }

    public void setAddressTownCity(String addressTownCity) {
        this.addressTownCity = addressTownCity;
    }

    public String getAddressCountry() {
        return addressCountry;
    }

    public void setAddressCountry(String addressCountry) {
        this.addressCountry = addressCountry;
    }

    public String getAddressPostalZipCode() {
        return addressPostalZipCode;
    }

    public void setAddressPostalZipCode(String addressPostalZipCode) {
        this.addressPostalZipCode = addressPostalZipCode;
    }

    public long getPatientIdNumber() {
        return patientIdNumber;
    }

    public void setPatientIdNumber(long patientIdNumber) {
        this.patientIdNumber = patientIdNumber;
    }

    public LocalDate getPatientDateOfBirth() {
        return patientDateOfBirth;
    }

    public void setPatientDateOfBirth(LocalDate patientDateOfBirth) {
        this.patientDateOfBirth = patientDateOfBirth;
    }

    public String getPatientOccupation() {
        return patientOccupation;
    }

    public void setPatientOccupation(String patientOccupation) {
        this.patientOccupation = patientOccupation;
    }

    public long getPatientContactNumber() {
        return patientContactNumber;
    }

    public void setPatientContactNumber(long patientContactNumber) {
        this.patientContactNumber = patientContactNumber;
    }

    public PatientGenderEnum getPatientGender() {
        return patientGender;
    }

    public void setPatientGender(PatientGenderEnum patientGender) {
        this.patientGender = patientGender;
    }

    public String getGenderValue() {
        return genderValue;
    }

    public void setGenderValue(String genderValue) {
        this.genderValue = genderValue;
    }

    public PatientStatusEnum getPatientStatus() {
        return patientStatus;
    }

    public void setPatientStatus(PatientStatusEnum patientStatus) {
        this.patientStatus = patientStatus;
    }

    public String getAddedUser() {
        return addedUser;
    }

    public void setAddedUser(String addedUser) {
        this.addedUser = addedUser;
    }

    public LocalDate getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(LocalDate addedDate) {
        this.addedDate = addedDate;
    }

    public LocalTime getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(LocalTime addedTime) {
        this.addedTime = addedTime;
    }

    public String getChangedUser() {
        return changedUser;
    }

    public void setChangedUser(String changedUser) {
        this.changedUser = changedUser;
    }

    public LocalDate getChangedDate() {
        return changedDate;
    }

    public void setChangedDate(LocalDate changedDate) {
        this.changedDate = changedDate;
    }

    public LocalTime getChangedTime() {
        return changedTime;
    }

    public void setChangedTime(LocalTime changedTime) {
        this.changedTime = changedTime;
    }

    public String getPatientStatusValue() {
        return patientStatusValue;
    }

    public void setPatientStatusValue(String patientStatusValue) {
        this.patientStatusValue = patientStatusValue;
    }

    public ReturnCodeEnum getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(ReturnCodeEnum returnCode) {
        this.returnCode = returnCode;
    }

    public void setWfAddressPostZip(String addressPostZip) {
        globalCtx.setString("addressPostZip", addressPostZip);
    }

    public String getWfAddressPostZip() {
        return globalCtx.getString("addressPostZip");
    }

    public void setWfAddressProvince(String addressProvince) {
        globalCtx.setString("addressProvince", addressProvince);
    }

    public String getWfAddressProvince() {
        return globalCtx.getString("addressProvince");
    }

    public void setWfAddressStreet(String addressStreet) {
        globalCtx.setString("addressStreet", addressStreet);
    }

    public String getWfAddressStreet() {
        return globalCtx.getString("addressStreet");
    }

    public void setWfAddressTown(String addressTown) {
        globalCtx.setString("addressTown", addressTown);
    }

    public String getWfAddressTown() {
        return globalCtx.getString("addressTown");
    }

    public void setWfCountry(String country) {
        globalCtx.setString("country", country);
    }

    public String getWfCountry() {
        return globalCtx.getString("country");
    }

    public void setWfFaxNumber(long faxNumber) {
        globalCtx.setLong("faxNumber", faxNumber);
    }

    public long getWfFaxNumber() {
        return globalCtx.getLong("faxNumber");
    }

    public void setWfTelephoneNumber(long telephoneNumber) {
        globalCtx.setLong("telephoneNumber", telephoneNumber);
    }

    public long getWfTelephoneNumber() {
        return globalCtx.getLong("telephoneNumber");
    }

    /**
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param patient Patient Entity bean
     */
    public void setDtoFields(Patient patient) {
        BeanUtils.copyProperties(patient, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param patient Patient Entity bean
     */
    public void setEntityFields(Patient patient) {
        BeanUtils.copyProperties(this, patient);
    }

    public boolean getConductKeyScreenConversation() {
        return conductKeyScreenConversation;
    }

    public void setConductKeyScreenConversation(boolean conductKeyScreenConversation) {
        this.conductKeyScreenConversation = conductKeyScreenConversation;
    }

    public boolean getConductDetailScreenConversation() {
        return conductDetailScreenConversation;
    }

    public void setConductDetailScreenConversation(boolean conductDetailScreenConversation) {
        this.conductDetailScreenConversation = conductDetailScreenConversation;
    }

}
