package com.hospital.file.patient.edreditpatientdetailr;

import java.math.BigDecimal;

/**
 * Params for resource: EdrEditPatientDetailR (TSBQE1R).
 *
 * @author X2EGenerator
 */
public class EdrEditPatientDetailRParams {
    private String patientCode = "";

    public String getPatientCode() {
		return patientCode;
	}
	
	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}
}
