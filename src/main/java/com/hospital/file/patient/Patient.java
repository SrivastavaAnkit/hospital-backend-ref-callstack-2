package com.hospital.file.patient;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.hospital.common.jpa.DateConverter;
import com.hospital.file.ward.Ward;
import com.hospital.model.PatientGenderConverter;
import com.hospital.model.PatientGenderEnum;
import com.hospital.model.PatientStatusConverter;
import com.hospital.model.PatientStatusEnum;

@Entity
@Table(name="Patient", schema="HospitalMgmt")
public class Patient implements Serializable {
	private static final long serialVersionUID = -1L;

	@Version
	@Column(name="version")
	private long version;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns ({
		@JoinColumn(name="HospitalCode", referencedColumnName="HospitalCode", insertable=false, updatable=false)
	,	@JoinColumn(name="WardCode", referencedColumnName="WardCode", insertable=false, updatable=false)
	})
	private Ward wardObj;

	@EmbeddedId
	private PatientId id = new PatientId();

	@Column(name = "HospitalCode")
	private String hospitalCode = "";
	
	@Column(name = "WardCode")
	private String wardCode = "";
	
	@Column(name = "PatientName")
	private String patientName = "";
	
	@Column(name = "PatientSurname")
	private String patientSurname = "";
	
	@Column(name = "AddressLine1")
	private String addressLine1 = "";
	
	@Column(name = "AddressLine2")
	private String addressLine2 = "";
	
	@Column(name = "AddressTownCity")
	private String addressTownCity = "";
	
	@Column(name = "AddressCountry")
	private String addressCountry = "";
	
	@Column(name = "AddressPostalZipCode")
	private String addressPostalZipCode = "";
	
	@Column(name = "PatientIdNumber")
	private long patientIdNumber = 0L;
	
	@Column(name = "PatientDateOfBirth")
	@Convert(converter=DateConverter.class)
	private LocalDate patientDateOfBirth = LocalDate.of(1801, 1, 1);
	
	@Column(name = "PatientOccupation")
	private String patientOccupation = "";
	
	@Column(name = "PatientContactNumber")
	private long patientContactNumber = 0L;
	
	@Column(name = "PatientGender")
	@Convert(converter=PatientGenderConverter.class)
	private PatientGenderEnum patientGender ;
	
	@Column(name = "PatientStatus")
	@Convert(converter=PatientStatusConverter.class)
	private PatientStatusEnum patientStatus ;

	public PatientId getId() {
		return id;
	}

	public String getPatientCode() {
		return id.getPatientCode();
	}

	public String getHospitalCode() {
		return hospitalCode;
	}
	
	public String getWardCode() {
		return wardCode;
	}
	
	public String getPatientName() {
		return patientName;
	}
	
	public String getPatientSurname() {
		return patientSurname;
	}
	
	public String getAddressLine1() {
		return addressLine1;
	}
	
	public String getAddressLine2() {
		return addressLine2;
	}
	
	public String getAddressTownCity() {
		return addressTownCity;
	}
	
	public String getAddressCountry() {
		return addressCountry;
	}
	
	public String getAddressPostalZipCode() {
		return addressPostalZipCode;
	}
	
	public long getPatientIdNumber() {
		return patientIdNumber;
	}
	
	public LocalDate getPatientDateOfBirth() {
		return patientDateOfBirth;
	}
	
	public String getPatientOccupation() {
		return patientOccupation;
	}
	
	public long getPatientContactNumber() {
		return patientContactNumber;
	}
	
	public PatientGenderEnum getPatientGender() {
		return patientGender;
	}
	
	public PatientStatusEnum getPatientStatus() {
		return patientStatus;
	}

	public String getHospitalName() {
		return wardObj.getHospitalName();
	}
	
	public String getWardName() {
		return wardObj.getWardName();
	}

	public long getVersion() {
		return version;
	}

	public void setPatientCode(String patientCode) {
		this.id.setPatientCode(patientCode);
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	
	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}
	
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
	public void setPatientSurname(String patientSurname) {
		this.patientSurname = patientSurname;
	}
	
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	
	public void setAddressTownCity(String addressTownCity) {
		this.addressTownCity = addressTownCity;
	}
	
	public void setAddressCountry(String addressCountry) {
		this.addressCountry = addressCountry;
	}
	
	public void setAddressPostalZipCode(String addressPostalZipCode) {
		this.addressPostalZipCode = addressPostalZipCode;
	}
	
	public void setPatientIdNumber(long patientIdNumber) {
		this.patientIdNumber = patientIdNumber;
	}
	
	public void setPatientDateOfBirth(LocalDate patientDateOfBirth) {
		this.patientDateOfBirth = patientDateOfBirth;
	}
	
	public void setPatientOccupation(String patientOccupation) {
		this.patientOccupation = patientOccupation;
	}
	
	public void setPatientContactNumber(long patientContactNumber) {
		this.patientContactNumber = patientContactNumber;
	}
	
	public void setPatientGender(PatientGenderEnum patientGender) {
		this.patientGender = patientGender;
	}
	
	public void setPatientStatus(PatientStatusEnum patientStatus) {
		this.patientStatus = patientStatus;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
