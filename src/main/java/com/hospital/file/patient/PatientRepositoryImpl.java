package com.hospital.file.patient;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;

import com.hospital.model.CountryEnum;

import com.hospital.file.patient.Patient;
import com.hospital.file.patient.selectpatient.SelectPatientGDO;
import com.hospital.file.patient.dspfdisplaypatientswrd.DspfDisplayPatientsWrdGDO;
import com.hospital.file.patient.dspfdisplaypatientwrd2.DspfDisplayPatientWrd2GDO;
import com.hospital.file.patient.dsppatientsperhospital.DspPatientsPerHospitalGDO;
import com.hospital.file.patient.pmtdiagnosisandprescrip.PmtDiagnosisAndPrescripDTO;

/**
 * Custom Spring Data JPA repository implementation for model: Patient (TSAEREP).
 *
 * @author X2EGenerator
 */
@Repository
public class PatientRepositoryImpl implements PatientRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see com.hospital.file.patient.PatientService#deletePatient(Patient)
	 */
	@Override
	public void deletePatient(
		String patientCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(patientCode)) {
			whereClause += " patient.id.patientCode = :patientCode";
			isParamSet = true;
		}

		String sqlString = "DELETE FROM Patient patient";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(patientCode)) {
			query.setParameter("patientCode", patientCode);
		}

		em.createNativeQuery("SET foreign_key_checks = 0").executeUpdate();
		query.executeUpdate();
		em.createNativeQuery("SET foreign_key_checks = 1").executeUpdate();
	}

	@Override
	public RestResponsePage<SelectPatientGDO> selectPatient(
		String patientCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(patientCode)) {
			whereClause += " (patient.id.patientCode >= :patientCode OR patient.id.patientCode like CONCAT('%', :patientCode, '%'))";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.patient.selectpatient.SelectPatientGDO(patient.id.patientCode, patient.hospitalCode, patient.wardCode, patient.patientName, patient.patientSurname, patient.addressLine1, patient.addressLine2, patient.addressTownCity, patient.addressCountry, patient.addressPostalZipCode, patient.patientIdNumber, patient.patientDateOfBirth, patient.patientOccupation, patient.patientContactNumber, patient.patientGender, patient.patientStatus ) from Patient patient";
		String countQueryString = "SELECT COUNT(patient.id.patientCode) FROM Patient patient";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Patient patient = new Patient();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(patient.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"patient.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"patient." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"patient.id.patientCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(patientCode)) {
			countQuery.setParameter("patientCode", patientCode);
			query.setParameter("patientCode", patientCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<SelectPatientGDO> content = query.getResultList();
		RestResponsePage<SelectPatientGDO> pageDto = new RestResponsePage<SelectPatientGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.patient.PatientService#rtvPatientsPerWard(String)
	 */
	@Override
	public RestResponsePage<Patient> rtvPatientsPerWard(
		String wardCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(wardCode)) {
			whereClause += " patient.wardCode = :wardCode";
			isParamSet = true;
		}

		String sqlString = "SELECT patient FROM Patient patient";
		String countQueryString = "SELECT COUNT(patient.id.patientCode) FROM Patient patient";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(wardCode)) {
			countQuery.setParameter("wardCode", wardCode);
			query.setParameter("wardCode", wardCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<Patient> content = query.getResultList();
		RestResponsePage<Patient> pageDto = new RestResponsePage<Patient>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<DspfDisplayPatientsWrdGDO> dspfDisplayPatientsWrd(
		String hospitalCode, String wardCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " patient.hospitalCode like CONCAT('%', :hospitalCode, '%')";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(wardCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " patient.wardCode like CONCAT('%', :wardCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.patient.dspfdisplaypatientswrd.DspfDisplayPatientsWrdGDO(patient.wardCode, patient.id.patientCode, patient.hospitalCode, patient.patientName, patient.patientSurname, patient.addressLine1, patient.addressLine2, patient.addressTownCity, patient.addressCountry, patient.addressPostalZipCode, patient.patientIdNumber, patient.patientDateOfBirth, patient.patientOccupation, patient.patientContactNumber, patient.patientGender, patient.patientStatus ) from Patient patient";
		String countQueryString = "SELECT COUNT(patient.id.patientCode) FROM Patient patient";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Patient patient = new Patient();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(patient.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"patient.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"patient." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"patient.wardCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"patient.id.patientCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		if (StringUtils.isNotEmpty(wardCode)) {
			countQuery.setParameter("wardCode", wardCode);
			query.setParameter("wardCode", wardCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<DspfDisplayPatientsWrdGDO> content = query.getResultList();
		RestResponsePage<DspfDisplayPatientsWrdGDO> pageDto = new RestResponsePage<DspfDisplayPatientsWrdGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<PmtDiagnosisAndPrescripDTO> pmtDiagnosisAndPrescrip(
		Pageable pageable) {
		String sqlString = "SELECT new com.hospital.file.patient.pmtdiagnosisandprescrip.PmtDiagnosisAndPrescripDTO(patient.id.patientCode, patient.patientName, patient.hospitalCode, patient.wardCode, patient.patientSurname, patient.addressLine1, patient.addressLine2, patient.addressTownCity, patient.addressCountry, patient.addressPostalZipCode, patient.patientIdNumber, patient.patientDateOfBirth, patient.patientOccupation, patient.patientContactNumber, patient.patientGender, patient.patientStatus ) from Patient patient";
		String countQueryString = "SELECT COUNT(patient.id.patientCode) FROM Patient patient";

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<PmtDiagnosisAndPrescripDTO> content = query.getResultList();
		RestResponsePage<PmtDiagnosisAndPrescripDTO> pageDto = new RestResponsePage<PmtDiagnosisAndPrescripDTO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.patient.PatientService#rtvPatientName(String)
	 */
	@Override
	public Patient rtvPatientName(
		String patientCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(patientCode)) {
			whereClause += " patient.id.patientCode = :patientCode";
			isParamSet = true;
		}

		String sqlString = "SELECT patient FROM Patient patient";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(patientCode)) {
			query.setParameter("patientCode", patientCode);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		Patient dto = (Patient)query.getSingleResult();

		return dto;
	}

	@Override
	public RestResponsePage<DspfDisplayPatientWrd2GDO> dspfDisplayPatientWrd2(
		String hospitalCode, String wardCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " patient.hospitalCode like CONCAT('%', :hospitalCode, '%')";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(wardCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " patient.wardCode like CONCAT('%', :wardCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.patient.dspfdisplaypatientwrd2.DspfDisplayPatientWrd2GDO(patient.wardCode, patient.id.patientCode, patient.hospitalCode, patient.patientName, patient.patientSurname, patient.addressLine1, patient.addressLine2, patient.addressTownCity, patient.addressCountry, patient.addressPostalZipCode, patient.patientIdNumber, patient.patientDateOfBirth, patient.patientOccupation, patient.patientContactNumber, patient.patientGender, patient.patientStatus ) from Patient patient";
		String countQueryString = "SELECT COUNT(patient.id.patientCode) FROM Patient patient";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Patient patient = new Patient();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(patient.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"patient.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"patient." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"patient.wardCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"patient.id.patientCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		if (StringUtils.isNotEmpty(wardCode)) {
			countQuery.setParameter("wardCode", wardCode);
			query.setParameter("wardCode", wardCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<DspfDisplayPatientWrd2GDO> content = query.getResultList();
		RestResponsePage<DspfDisplayPatientWrd2GDO> pageDto = new RestResponsePage<DspfDisplayPatientWrd2GDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<DspPatientsPerHospitalGDO> dspPatientsPerHospital(
		String hospitalCode, CountryEnum country, String countryName, long telephoneNumber, String wardCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " patient.hospitalCode like CONCAT('%', :hospitalCode, '%')";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(wardCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " (patient.wardCode >= :wardCode OR patient.wardCode like CONCAT('%', :wardCode, '%'))";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.patient.dsppatientsperhospital.DspPatientsPerHospitalGDO(patient.hospitalCode, patient.wardCode, patient.id.patientCode, patient.patientName, patient.patientSurname, patient.addressLine1, patient.addressLine2, patient.addressTownCity, patient.addressCountry, patient.addressPostalZipCode, patient.patientIdNumber, patient.patientDateOfBirth, patient.patientOccupation, patient.patientContactNumber, patient.patientGender, patient.patientStatus) from Patient patient";
		String countQueryString = "SELECT COUNT(patient.id.patientCode) FROM Patient patient";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Patient patient = new Patient();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(patient.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"patient.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"patient." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"patient.hospitalCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"patient.wardCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		if (StringUtils.isNotEmpty(wardCode)) {
			countQuery.setParameter("wardCode", wardCode);
			query.setParameter("wardCode", wardCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<DspPatientsPerHospitalGDO> content = query.getResultList();
		RestResponsePage<DspPatientsPerHospitalGDO> pageDto = new RestResponsePage<DspPatientsPerHospitalGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.patient.PatientService#prtPatientsPerHospital(Patient)
	 */
	@Override
	public List<Patient> prtPatientsPerHospital() {
		String sqlString = "SELECT patient FROM Patient patient";

		Query query = em.createQuery(sqlString);

		@SuppressWarnings("unchecked")
		List<Patient> content = query.getResultList();

		return content;
	}

	/**
	 * @see com.hospital.file.patient.PatientService#rtvPatientPerHospital(String)
	 */
	@Override
	public RestResponsePage<Patient> rtvPatientPerHospital(
		String hospitalCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " patient.hospitalCode = :hospitalCode";
			isParamSet = true;
		}

		String sqlString = "SELECT patient FROM Patient patient";
		String countQueryString = "SELECT COUNT(patient.id.patientCode) FROM Patient patient";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<Patient> content = query.getResultList();
		RestResponsePage<Patient> pageDto = new RestResponsePage<Patient>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.patient.PatientService#rtvPatientsForHospital(String, String)
	 */
	@Override
	public RestResponsePage<Patient> rtvPatientsForHospital(
		String hospitalCode, String wardCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " patient.hospitalCode = :hospitalCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(wardCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " patient.wardCode = :wardCode";
			isParamSet = true;
		}

		String sqlString = "SELECT patient FROM Patient patient";
		String countQueryString = "SELECT COUNT(patient.id.patientCode) FROM Patient patient";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			countQuery.setParameter("hospitalCode", hospitalCode);
			query.setParameter("hospitalCode", hospitalCode);
		}

		if (StringUtils.isNotEmpty(wardCode)) {
			countQuery.setParameter("wardCode", wardCode);
			query.setParameter("wardCode", wardCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<Patient> content = query.getResultList();
		RestResponsePage<Patient> pageDto = new RestResponsePage<Patient>(
				content, pageable, count.longValue());

		return pageDto;
	}

}
