package com.hospital.file.patient.prtpatientsperhospital;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.patient.Patient;
import com.hospital.file.patient.PatientId;
import com.hospital.file.patient.PatientRepository;
import com.hospital.model.PatientGenderEnum;
import com.hospital.model.PatientStatusEnum;
import com.hospital.model.ReturnCodeEnum;



@Service
public class PrtPatientsPerHospitalService {

	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PatientRepository patientRepository;
	

	public void execute(PrtPatientsPerHospitalDTO prtPatientsPerHospitalDTO) {

		// TODO: Code generation is not currently supported for Print File Functions
	}
}
