package com.hospital.file.patient.dspfdisplaypatientwrd2;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReloadSubfileEnum;


/**
 * State for file 'Patient' (TSAEREP) and function 'DSPF Display Patient/Wrd2' (TSBBDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

@Configurable
public class DspfDisplayPatientWrd2State extends DspfDisplayPatientWrd2DTO {
	private static final long serialVersionUID = -2760648673297741361L;


	//@Autowired
	private GlobalContext globalCtx=new GlobalContext();


	// System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;


    public DspfDisplayPatientWrd2State() {

    }

	public void setWfAddressPostZip(String addressPostZip) {
		globalCtx.setString("addressPostZip", addressPostZip);
	}

	public String getWfAddressPostZip() {
		return globalCtx.getString("addressPostZip");
	}

	public void setWfAddressProvince(String addressProvince) {
		globalCtx.setString("addressProvince", addressProvince);
	}

	public String getWfAddressProvince() {
		return globalCtx.getString("addressProvince");
	}

	public void setWfAddressStreet(String addressStreet) {
		globalCtx.setString("addressStreet", addressStreet);
	}

	public String getWfAddressStreet() {
		return globalCtx.getString("addressStreet");
	}

	public void setWfAddressTown(String addressTown) {
		globalCtx.setString("addressTown", addressTown);
	}

	public String getWfAddressTown() {
		return globalCtx.getString("addressTown");
	}

	public void setWfCountry(String country) {
		globalCtx.setString("country", country);
	}

	public String getWfCountry() {
		return globalCtx.getString("country");
	}

	public void setWfFaxNumber(long faxNumber) {
		globalCtx.setLong("faxNumber", faxNumber);
	}

	public long getWfFaxNumber() {
		return globalCtx.getLong("faxNumber");
	}

	public void setWfTelephoneNumber(long telephoneNumber) {
		globalCtx.setLong("telephoneNumber", telephoneNumber);
	}

	public long getWfTelephoneNumber() {
		return globalCtx.getLong("telephoneNumber");
	}



    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }
    }