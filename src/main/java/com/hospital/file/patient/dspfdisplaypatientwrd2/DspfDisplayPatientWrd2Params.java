package com.hospital.file.patient.dspfdisplaypatientwrd2;

import java.io.Serializable;


/**
 * Params for resource: DspfDisplayPatientWrd2 (TSBBDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
public class DspfDisplayPatientWrd2Params implements Serializable {
	private static final long serialVersionUID = -5364366711264360078L;

	private String hospitalCode = "";
	private String wardCode = "";


	public String getHospitalCode() {
		return hospitalCode;
	}
	
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	
	public String getWardCode() {
		return wardCode;
	}
	
	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

}