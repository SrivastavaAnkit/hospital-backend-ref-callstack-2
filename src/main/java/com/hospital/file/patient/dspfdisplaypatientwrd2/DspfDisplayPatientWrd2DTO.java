package com.hospital.file.patient.dspfdisplaypatientwrd2;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.hospital.common.state.BaseDTO;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.model.ReturnCodeEnum;


/**
 * Dto for file 'Patient' (TSAEREP) and function 'DSPF Display Patient/Wrd2' (TSBBDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class DspfDisplayPatientWrd2DTO extends BaseDTO {
	private static final long serialVersionUID = 9154142576981525786L;
	private long version = 0;

    private RestResponsePage<DspfDisplayPatientWrd2GDO> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";

	private String patientCode = "";
	private String hospitalCode = "";
	private String hospitalName = "";
	private String wardCode = "";
	private String wardName = "";
	private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");


	private DspfDisplayPatientWrd2GDO gdo;

    public DspfDisplayPatientWrd2DTO() {

    }

	public DspfDisplayPatientWrd2DTO(long version, String wardCode) {
		this.version = version;
		this.wardCode = wardCode;
	}

	public void setVersion(long version) {
		this.version = version;
    }

    public long getVersion() {
    	return version;
    }

    public void setPageDto(RestResponsePage<DspfDisplayPatientWrd2GDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<DspfDisplayPatientWrd2GDO> getPageDto() {
		return pageDto;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
    }

    public String getPatientCode() {
    	return patientCode;
    }

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
    }

    public String getHospitalCode() {
    	return hospitalCode;
    }

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
    	return hospitalName;
    }

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
    }

    public String getWardCode() {
    	return wardCode;
    }

	public void setWardName(String wardName) {
		this.wardName = wardName;
    }

    public String getWardName() {
    	return wardName;
    }

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
    }

    public ReturnCodeEnum getReturnCode() {
    	return returnCode;
    }

	public void setGdo(DspfDisplayPatientWrd2GDO gdo) {
		this.gdo = gdo;
	}

	public DspfDisplayPatientWrd2GDO getGdo() {
		return gdo;
	}

}