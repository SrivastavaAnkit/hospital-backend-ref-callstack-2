package com.hospital.file.medication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository interface for model: Medication (TSAJREP).
 *
 * @author X2EGenerator
 */
@Repository
public interface MedicationRepository extends MedicationRepositoryCustom, JpaRepository<Medication, MedicationId> {

	List<Medication> findAllByIdMedicationCode(String medicationCode);
}
