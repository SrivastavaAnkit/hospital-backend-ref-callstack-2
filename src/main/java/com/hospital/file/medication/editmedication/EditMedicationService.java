package com.hospital.file.medication.editmedication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.exception.ServiceException;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.file.medication.MedicationId;
import com.hospital.file.medication.MedicationRepository;
import com.hospital.file.medication.changemedication.ChangeMedicationDTO;
import com.hospital.file.medication.changemedication.ChangeMedicationService;
import com.hospital.file.medication.createmedication.CreateMedicationDTO;
import com.hospital.file.medication.createmedication.CreateMedicationService;
import com.hospital.file.medication.deletemedication.DeleteMedicationDTO;
import com.hospital.file.medication.deletemedication.DeleteMedicationService;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ProgramModeEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.support.JobContext;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

/**
 * EDTFIL controller for 'Edit Medication' (TSAWEFR) of file 'Medication'
 * (TSAJREP)
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 */
@Service
public class EditMedicationService extends AbstractService<EditMedicationService, EditMedicationState> {

	@Autowired
	private JobContext job;

	@Autowired
	private MedicationRepository medicationRepository;
	@Autowired
	private ChangeMedicationService changeMedicationService;

	@Autowired
	private CreateMedicationService createMedicationService;

	@Autowired
	private DeleteMedicationService deleteMedicationService;

	@Autowired
	private EditMedicationService editMedicationService;

	@Autowired
	private MessageSource messageSource;

	// @Autowired
	// private EditMedicationValidator editMedicationValidator;

	public static final String SCREEN_CTL = "editMedication";
	public static final String SCREEN_RCD = "EditMedication.rcd";
	public static final String SCREEN_CFM = "EditMedication.confirm";

	private final Step execute = define("execute", EditMedicationParams.class, this::execute);
	private final Step response = define("ctlScreen", EditMedicationState.class, this::processResponse);
	private final Step confirmScreenResponse = define("cfmscreen", EditMedicationDTO.class,
			this::processConfirmScreenResponse);
	// private final Step serviceCreateMedication =
	// define("serviceCreateMedication",CreateMedicationParams.class,
	// this::processServiceCreateMedication);
	// private final Step serviceDeleteMedication =
	// define("serviceDeleteMedication",DeleteMedicationParams.class,
	// this::processServiceDeleteMedication);
	// private final Step serviceChangeMedication =
	// define("serviceChangeMedication",ChangeMedicationParams.class,
	// this::processServiceChangeMedication);
	// private final Step serviceRtvcnd = define("serviceRtvcnd",RtvcndParams.class,
	// this::processServiceRtvcnd);
	// private final Step serviceExitProgram =
	// define("serviceExitProgram",ExitProgramParams.class,
	// this::processServiceExitProgram);

	@Autowired
	public EditMedicationService() {
		super(EditMedicationService.class, EditMedicationState.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * EDTFIL controller starting point.
	 * 
	 * @param state  - Service state class.
	 * @param params - Service input/output parameters class.
	 * @return
	 */
	private StepResult execute(EditMedicationState state, EditMedicationParams params) {
		StepResult result = NO_ACTION;

		BeanUtils.copyProperties(params, state);
		usrInitializeProgram(state);

		result = mainLoop(state);

		return result;
	}

	/**
	 * SCREEN_KEY initial processing loop method.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult mainLoop(EditMedicationState state) {
		StepResult result = NO_ACTION;

		result = loadFirstSubfilePage(state);

		state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
		result = conductScreenConversation(state);

		return result;
	}

	/**
	 * SCREEN initial processing loop method.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult loadFirstSubfilePage(EditMedicationState state) {
		StepResult result = NO_ACTION;

		result = usrInitializeSubfileHeader(state);

		dbfReadFirstDataRecord(state);
		if (state.getPageDto() != null && state.getPageDto().getSize() > 0) {
			state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
			result = loadNextSubfilePage(state);
		} else {
			state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
		}

		return result;
	}

	/**
	 * Iterate on data loaded to do stuff on each record.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult loadNextSubfilePage(EditMedicationState state) {
		StepResult result = NO_ACTION;

		for (EditMedicationGDO gdo : state.getPageDto().getContent()) {
			if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
				if (gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
					// TODO:moveDbfRecordFieldsToSubfileRecord(state); // synon built-in function
					result = usrInitializeSubfileRecordExistingRecord(state, gdo);
				}
			} else {
				// TODO:moveDbfRecordFieldsToSubfileRecord(state); // synon built-in function
				result = usrInitializeSubfileRecordNewRecord(state, gdo);
			}
			validateSubfileRecord(state, gdo);
			result = dbfUpdateDataRecord(state, gdo);
		}

		return result;
	}

	/**
	 * SCREEN validate subfile record.
	 *
	 * @param gdo - subfile record class.
	 * @return result
	 */
	private StepResult validateSubfileRecord(EditMedicationState state, EditMedicationGDO gdo) {
		StepResult result = NO_ACTION;

		return result;
	}

	/**
	 * SCREEN initial processing loop method.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult conductScreenConversation(EditMedicationState state) {
		StepResult result = NO_ACTION;

		if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
			EditMedicationDTO dto = new EditMedicationDTO();
			BeanUtils.copyProperties(state, dto);
			result = callScreen(SCREEN_CTL, dto).thenCall(response);
		}

		return result;
	}

	/**
	 * SCREEN_KEY returned response processing method.
	 *
	 * @param state      - Service state class.
	 * @param fromScreen - returned screen model.
	 * @return result
	 */
	private StepResult processResponse(EditMedicationState state, EditMedicationDTO fromScreen) {
		StepResult result = NO_ACTION;

		// update state from vm and use state (not vm) as processResponseToKeyScreen()
		// argument.
		BeanUtils.copyProperties(fromScreen, state);
		// TODO: Added condition for change mode and delete condition
		if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
			result = closedown(state);
		}
		else if (CmdKeyEnum.isChangeMode(state.get_SysCmdKey())) {
			   if(state.get_SysProgramMode()==ProgramModeEnum._STA_CHANGE){
			    state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
			    List<EditMedicationGDO> content=new ArrayList<EditMedicationGDO>();
			    for(int idx=0;idx<10;idx++){
			     content.add(new EditMedicationGDO());
			    }
			    RestResponsePage<EditMedicationGDO> pageDto = new RestResponsePage<>(content);
			          state.setPageDto(pageDto);
			   return conductScreenConversation(state);
			   }
			   else{
			    state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
			    return mainLoop(state);
			   }
	}
		else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {

			state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
		} else if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
			result = processHelpRequest(state); // synon built-in function
		} else if (CmdKeyEnum.isNextPage(state.get_SysCmdKey())) {
			dbfReadNextPageRecord(state);
			result = loadNextSubfilePage(state);
		} else {
			result = processScreen(state);
		}

		return result;
	}

	private StepResult processHelpRequest(EditMedicationState state) {
		return NO_ACTION;
	}

	/**
	 * SCREEN process screen.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult processScreen(EditMedicationState state) {
		StepResult result = NO_ACTION;

		result = validateSubfileControl(state);
		if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
			return result;
		} else {
			for (EditMedicationGDO gdo : state.getPageDto().getContent()) {
				// TODO: Commented
				// if
				// (gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode()))
				// {
				result = usrValidateSubfileRecordFields(state, gdo);
				if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
					return result;
				}
				result = checkRelations(gdo);
				if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
					return result;
				}
				result = usrSubfileRecordFunctionFields(state, gdo);
				result = usrValidateSubfileRecordRelations(state, gdo);
				if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
					return result;
				}
				// result = dbfUpdateDataRecord(state, gdo);
				// }
			}
		}

		EditMedicationDTO dto = new EditMedicationDTO();
		BeanUtils.copyProperties(state, dto);
		result = processConfirmScreenResponse(state, dto);
		// TODO: Added mainloop funnction
		mainLoop(state);
		return result;
	}

	/**
	 * SCREEN check the relations of GDO.
	 *
	 * @param gdo - Service subfile record class.
	 * @return result
	 */
	private StepResult checkRelations(EditMedicationGDO gdo) {
		StepResult result = NO_ACTION;

		return result;
	}

	/**
	 * SCREEN validate subfile control.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult validateSubfileControl(EditMedicationState state) {
		StepResult result = NO_ACTION;

		state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
		validateSubfileControlField(state);
		if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
			return result;
		}
		usrSubfileControlFunctionFields(state);
		result = usrValidateSubfileControl(state);

		return result;
	}

	/**
	 * SCREEN validate subfile control field.
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult validateSubfileControlField(EditMedicationState state) {
		StepResult result = NO_ACTION;

		return result;
	}

	/**
	 * SCREEN_CONFIRM returned response processing method.
	 *
	 * @param state - Service state class.
	 * @param model - returned screen model.
	 * @return result
	 */
	private StepResult processConfirmScreenResponse(EditMedicationState state, EditMedicationDTO model) {
		StepResult result = NO_ACTION;
		for (EditMedicationGDO gdo : state.getPageDto().getContent()) {
			// TODO: Commented below condition code
			// if
			// (gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode()))
			// {
			if ("DELETE".equals(gdo.get_SysSelected().toUpperCase())) {
				// TODO: Add condition only for delete not for create and update
				if (state.isConfirm()) {
					result = dbfDeleteDataRecord(state, gdo);
				}
			} else {
				if ("CHANGE".equals(gdo.get_SysSelected())) {
					result = dbfUpdateDataRecord(state, gdo);
				} else {
					result = dbfCreateDataRecord(state, gdo);
				}
			}
			// }
			result = usrExtraProcessingAfterDBFUpdate(state);
			state.setConfirm(false);
			// }
		}

		if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
			return result;
		} else {
			if (state.get_SysReloadSubfile() == ReloadSubfileEnum._STA_YES) {
				// request subfile reload if necessary
			}
			if (CmdKeyEnum.isChangeMode(state.get_SysCmdKey())) {
				if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
					state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
				} else {
					state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
				}
			} else {
				result = usrProcessCommandKeys(state);
				if (result != StepResult.NO_ACTION) {
					return result;
				}
			}
		}

		result = mainLoop(state);

		return result;
	}

	/**
	 * Terminate this program
	 *
	 * @param state - Service state class.
	 * @return result
	 */
	private StepResult closedown(EditMedicationState state) {
		StepResult result = NO_ACTION;

		state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		result = usrExitProgramProcessing(state);

		return result;
	}

	/* ------------------------- Generated DBF method --------------------------- */

	private void dbfReadFirstDataRecord(EditMedicationState state) {
		state.setPage(0);
		dbfReadDataRecord(state);
	}

	/**
	 * Read data of the next page
	 * 
	 * @param state - Service state class.
	 * @return
	 */
	private void dbfReadNextPageRecord(EditMedicationState state) {
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

	private void dbfReadDataRecord(EditMedicationState state) {
		List<Order> sortOrders = new ArrayList<Order>();
		Pageable pageable;

		try {
			@SuppressWarnings("unchecked")
			Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

			for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
				if (entry.getValue() == null) {
					continue;
				}

				sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
			}
		} catch (IOException ioe) {
		}

		if (CollectionUtils.isEmpty(sortOrders)) {
			pageable = new PageRequest(state.getPage(), state.getSize());
		} else {
			pageable = new PageRequest(state.getPage(), state.getSize(), new Sort(sortOrders));
		}

		RestResponsePage<EditMedicationGDO> pageDto = medicationRepository.editMedication(state.getMedicationCode(),
				pageable);
		state.setPageDto(pageDto);
	}

	/**
	 * Create record data
	 * 
	 * @param dto - Service state class.
	 * @return
	 */
	private StepResult dbfCreateDataRecord(EditMedicationState dto, EditMedicationGDO gdo) {
		StepResult result = NO_ACTION;

		// val dbfOBJ: X2EFunction = getFunctionForType(x2EFile, "CRTOBJ")
		// val dtoOBJ = dbfOBJ.asServiceDtoClassName()
		// val dtoVAR = dbfOBJ.asServiceDtoVariableName()
		try {
			/**
			 * USER: Create Object (Generated:1387)
			 */
			CreateMedicationDTO createMedicationDTO;
			// switchBLK 1387 BLK ACT
			// DEBUG genFunctionCall 1388 ACT Create Medication - Medication *
			// DEBUG genFunctionCall ServiceDtoVariable
			createMedicationDTO = new CreateMedicationDTO();
			// DEBUG genFunctionCall Parameters IN
			createMedicationDTO.setMedicationCode(gdo.getMedicationCode());
			createMedicationDTO.setMedicationDescription(gdo.getMedicationDescription());
			createMedicationDTO.setMedicationUnit(gdo.getMedicationUnit());
			// DEBUG genFunctionCall Service call
			createMedicationService.execute(createMedicationDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setLclUsrReturnCode(createMedicationDTO.getUsrReturnCode());
			// DEBUG genFunctionCall Parameters DONE

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * Delete record data
	 * 
	 * @param dto - Service state class.
	 * @return
	 */
	private StepResult dbfDeleteDataRecord(EditMedicationState dto, EditMedicationGDO gdo) {
		StepResult result = NO_ACTION;

		// val functions = x2EFile.functions.filter { it.functionType == "DLTOBJ" }

		// val dbfOBJ: X2EFunction = if (functions.isNotEmpty())
		// getFunctionForType(x2EFile, "DLTOBJ") else function
		// val dtoOBJ = dbfOBJ.asServiceDtoClassName()
		// val dtoVAR = dbfOBJ.asServiceDtoVariableName()
		try {
			/**
			 * USER: Delete Object (Generated:1389)
			 */
			DeleteMedicationDTO deleteMedicationDTO;
			// switchBLK 1389 BLK ACT
			// DEBUG genFunctionCall 1390 ACT Delete Medication - Medication *
			// DEBUG genFunctionCall ServiceDtoVariable
			deleteMedicationDTO = new DeleteMedicationDTO();
			// DEBUG genFunctionCall Parameters IN
			deleteMedicationDTO.setMedicationCode(gdo.getMedicationCode());
			// DEBUG genFunctionCall Service call
			deleteMedicationService.execute(deleteMedicationDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * Update record data
	 * 
	 * @param dto - Service state class.
	 * @return
	 */
	private StepResult dbfUpdateDataRecord(EditMedicationState dto, EditMedicationGDO gdo) {
		StepResult result = NO_ACTION;

		// val dbfOBJ: X2EFunction = getFunctionForType(x2EFile, "CHGOBJ")
		// val dtoOBJ = dbfOBJ.asServiceDtoClassName()
		// val dtoVAR = dbfOBJ.asServiceDtoVariableName()
		try {
			MedicationId medicationId = new MedicationId(gdo.getMedicationCode());
			if (medicationRepository.existsById(medicationId)) {

				/**
				 * USER: Change Object (Generated:1391)
				 */
				ChangeMedicationDTO changeMedicationDTO;
				// switchBLK 1391 BLK ACT
				// DEBUG genFunctionCall 1392 ACT Change Medication - Medication *
				// DEBUG genFunctionCall ServiceDtoVariable
				changeMedicationDTO = new ChangeMedicationDTO();
				// DEBUG genFunctionCall Parameters IN
				changeMedicationDTO.setMedicationCode(gdo.getMedicationCode());
				changeMedicationDTO.setMedicationDescription(gdo.getMedicationDescription());
				changeMedicationDTO.setMedicationUnit(gdo.getMedicationUnit());
				// DEBUG genFunctionCall Service call
				changeMedicationService.execute(changeMedicationDTO);
				// DEBUG genFunctionCall Parameters OUT
				dto.setLclUsrReturnCode(changeMedicationDTO.getUsrReturnCode());
				// DEBUG genFunctionCall Parameters DONE

			} else {
				throw new ServiceException("diagnosis.nf");
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * USER: Initialize Program (Generated:15)
	 */
	private StepResult usrInitializeProgram(EditMedicationState dto) {
		StepResult result = NO_ACTION;

		try {
			// switchSUB 15 SUB
			// Unprocessed SUB 15 -
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * USER: Initialize Subfile Header (Generated:1488)
	 */
	private StepResult usrInitializeSubfileHeader(EditMedicationState dto) {
		StepResult result = NO_ACTION;

		try {
			// switchSUB 1488 SUB
			// Unprocessed SUB 1488 -

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * USER: Initialize Subfile Record (New Record) (Generated:1027)
	 */
	private StepResult usrInitializeSubfileRecordNewRecord(EditMedicationState dto, EditMedicationGDO gdo) {
		StepResult result = NO_ACTION;

		try {
			// switchSUB 1027 SUB
			// Unprocessed SUB 1027 -
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * USER: Initialize Subfile Record (Existing Record) (Generated:1023)
	 */
	private StepResult usrInitializeSubfileRecordExistingRecord(EditMedicationState dto, EditMedicationGDO gdo) {
		StepResult result = NO_ACTION;

		try {
			// switchSUB 1023 SUB
			// switchBLK 1000001 BLK ACT
			// DEBUG genFunctionCall 1000002 ACT RCD.Medication unit value = Condition name
			// of RCD.Medication Unit
			gdo.setMedicationUnitValue(gdo.getMedicationUnit().getDescription()); // Retrieve condition

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * CALC: Subfile Control Function Fields (Generated:1463)
	 */
	private StepResult usrSubfileControlFunctionFields(EditMedicationState dto) {
		StepResult result = NO_ACTION;

		try {
			// switchSUB 1463 SUB
			// Unprocessed SUB 1463 -
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * USER: Validate Subfile Control (Generated:37)
	 */
	private StepResult usrValidateSubfileControl(EditMedicationState dto) {
		StepResult result = NO_ACTION;

		try {
			// switchSUB 37 SUB
			// switchBLK 1000007 BLK CAS
			// switchSUB 1000007 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// CTL.*CMD key is *Cancel
				// switchBLK 1000021 BLK ACT
				// DEBUG genFunctionCall 1000022 ACT Exit program - return code CND.E
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			} else // switchSUB 1000016 SUB
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// CTL.*CMD key is *Exit
				// switchBLK 1000018 BLK ACT
				// DEBUG genFunctionCall 1000019 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * USER: Validate Subfile Record Fields (Generated:1496)
	 */
	private StepResult usrValidateSubfileRecordFields(EditMedicationState dto, EditMedicationGDO gdo) {
		StepResult result = NO_ACTION;

		try {
			// switchSUB 1496 SUB
			// Unprocessed SUB 1496 -
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * CALC: Subfile Record Function Fields (Generated:1449)
	 */
	private StepResult usrSubfileRecordFunctionFields(EditMedicationState dto, EditMedicationGDO gdo) {
		StepResult result = NO_ACTION;

		try {
			// switchSUB 1449 SUB
			// Unprocessed SUB 1449 -
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * USER: Validate Subfile Record Relations (Generated:1171)
	 */
	private StepResult usrValidateSubfileRecordRelations(EditMedicationState dto, EditMedicationGDO gdo) {
		StepResult result = NO_ACTION;

		try {
			// switchSUB 1171 SUB
			// Unprocessed SUB 1171 -
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * USER: Extra Processing After DBF Update (Generated:1442)
	 */
	private StepResult usrExtraProcessingAfterDBFUpdate(EditMedicationState dto) {
		StepResult result = NO_ACTION;

		try {
			// switchSUB 1442 SUB
			// Unprocessed SUB 1442 -
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * USER: Process Command Keys (Generated:1420)
	 */
	private StepResult usrProcessCommandKeys(EditMedicationState dto) {
		StepResult result = NO_ACTION;

		try {
			// switchSUB 1420 SUB
			// Unprocessed SUB 1420 -
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * USER: Exit Program Processing (Generated:1262)
	 */
	private StepResult usrExitProgramProcessing(EditMedicationState dto) {
		StepResult result = NO_ACTION;

		try {
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// CTL.*CMD key is *Exit
				// switchBLK 1000176 BLK ACT
				// functionCall 1000177 ACT PAR.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._EXIT_REQUESTED);
				EditMedicationParams editMedicationParams = new EditMedicationParams();
				BeanUtils.copyProperties(dto, editMedicationParams);
				result = StepResult.returnFromService(editMedicationParams);
				// switchBLK 1000076 BLK ACT
				// functionCall 1000077 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return result;
	}

	/**
	 * ---------------------- Programmatic user-point: process call service
	 * --------------------------
	 */
	//
	// /**
	// * CreateMedicationService returned response processing method.
	// * @param state - Service state class.
	// * @param serviceResult - returned service model.
	// * @return
	// */
	// private StepResult processServiceCreateMedication(EditMedicationState state,
	// CreateMedicationParams serviceResult)
	// {
	// StepResult result = NO_ACTION;
	//
	// if(serviceResult != null) {
	// BeanUtils.copyProperties(serviceResult, state);
	// }
	//
	// //TODO: call the continuation of the program
	// //result = ??;
	//
	// return result;
	// }
	////
	// /**
	// * DeleteMedicationService returned response processing method.
	// * @param state - Service state class.
	// * @param serviceResult - returned service model.
	// * @return
	// */
	// private StepResult processServiceDeleteMedication(EditMedicationState state,
	// DeleteMedicationParams serviceResult)
	// {
	// StepResult result = NO_ACTION;
	//
	// if(serviceResult != null) {
	// BeanUtils.copyProperties(serviceResult, state);
	// }
	//
	// //TODO: call the continuation of the program
	// //result = ??;
	//
	// return result;
	// }
	////
	// /**
	// * ChangeMedicationService returned response processing method.
	// * @param state - Service state class.
	// * @param serviceResult - returned service model.
	// * @return
	// */
	// private StepResult processServiceChangeMedication(EditMedicationState state,
	// ChangeMedicationParams serviceResult)
	// {
	// StepResult result = NO_ACTION;
	//
	// if(serviceResult != null) {
	// BeanUtils.copyProperties(serviceResult, state);
	// }
	//
	// //TODO: call the continuation of the program
	// //result = ??;
	//
	// return result;
	// }
	////
	// /**
	// * RtvcndService returned response processing method.
	// * @param state - Service state class.
	// * @param serviceResult - returned service model.
	// * @return
	// */
	// private StepResult processServiceRtvcnd(EditMedicationState state,
	// RtvcndParams serviceResult)
	// {
	// StepResult result = NO_ACTION;
	//
	// if(serviceResult != null) {
	// BeanUtils.copyProperties(serviceResult, state);
	// }
	//
	// //TODO: call the continuation of the program
	// //result = ??;
	//
	// return result;
	// }
	////
	// /**
	// * ExitProgramService returned response processing method.
	// * @param state - Service state class.
	// * @param serviceResult - returned service model.
	// * @return
	// */
	// private StepResult processServiceExitProgram(EditMedicationState state,
	// ExitProgramParams serviceResult)
	// {
	// StepResult result = NO_ACTION;
	//
	// if(serviceResult != null) {
	// BeanUtils.copyProperties(serviceResult, state);
	// }
	//
	// //TODO: call the continuation of the program
	// //result = ??;
	//
	// return result;
	// }
	//

}
