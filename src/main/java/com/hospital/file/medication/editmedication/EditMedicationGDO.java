package com.hospital.file.medication.editmedication;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.file.medication.Medication;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;


/**
 * Gdo for file 'Medication' (TSAJREP) and function 'Edit Medication' (TSAWEFR).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 *
 */
public class EditMedicationGDO implements Serializable {
	private static final long serialVersionUID = -1631709255533968460L;

	private long version = 0;
    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private String medicationCode = "";
	private String medicationDescription = "";
	private MedicationUnitEnum medicationUnit = null;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;
	private String medicationUnitValue = "";

	public EditMedicationGDO() {

	}

	//TODO: Commented some field in EditMedicationGDO constructor
   	public EditMedicationGDO(/*long version, */String medicationCode, String medicationDescription, MedicationUnitEnum medicationUnit/*, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime*/) {
		/*this.version = version;*/
		this.medicationCode = medicationCode;
		this.medicationDescription = medicationDescription;
		this.medicationUnit = medicationUnit;
		/*this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;*/
	}


	public void setVersion(long version) {
		this.version = version;
	}

    public long getVersion() {
		return version;
    }

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setMedicationCode(String medicationCode) {
    	this.medicationCode = medicationCode;
    }

	public String getMedicationCode() {
		return medicationCode;
	}

	public void setMedicationDescription(String medicationDescription) {
    	this.medicationDescription = medicationDescription;
    }

	public String getMedicationDescription() {
		return medicationDescription;
	}

	public void setMedicationUnit(MedicationUnitEnum medicationUnit) {
    	this.medicationUnit = medicationUnit;
    }

	public MedicationUnitEnum getMedicationUnit() {
		return medicationUnit;
	}

	public void setAddedUser(String addedUser) {
    	this.addedUser = addedUser;
    }

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
    	this.addedDate = addedDate;
    }

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
    	this.addedTime = addedTime;
    }

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
    	this.changedUser = changedUser;
    }

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
    	this.changedDate = changedDate;
    }

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
    	this.changedTime = changedTime;
    }

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void setMedicationUnitValue(String medicationUnitValue) {
    	this.medicationUnitValue = medicationUnitValue;
    }

	public String getMedicationUnitValue() {
		return medicationUnitValue;
	}


    /**
     * Copies the fields of the Entity bean into the GDO bean.
     *
     * @param medication Medication Entity bean
     */
    public void setDtoFields(Medication medication) {
this.version = medication.getVersion();
  BeanUtils.copyProperties(medication, this);
    }

    /**
     * Copies the fields of the GDO bean into the Entity bean.
     *
     * @param medication Medication Entity bean
     */
    public void setEntityFields(Medication medication) {
		medication.setVersion(this.version);
      BeanUtils.copyProperties(this, medication);
    }

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}