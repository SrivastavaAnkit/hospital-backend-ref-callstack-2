package com.hospital.file.medication.editmedication;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.hospital.common.state.BaseDTO;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsGDO;
import com.hospital.model.GlobalContext;

/**
 * Dto for file 'Medication' (TSAJREP) and function 'Edit Medication' (TSAWEFR).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 *
 */

public class EditMedicationDTO extends BaseDTO {
	private static final long serialVersionUID = 2928929585024419788L;
	
	private long version = 0;

    private RestResponsePage<EditMedicationGDO> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";
    private boolean confirm = false;
    // TODO : Added mySelection field
    private List<EditMedicationGDO> mySelections;
	private String medicationCode = "";


	private EditMedicationGDO gdo;

    public EditMedicationDTO() {

    }

	public EditMedicationDTO(long version) {
		this.version = version;
	}

	public void setVersion(long version) {
		this.version = version;
    }

    public long getVersion() {
    	return version;
    }

    public void setPageDto(RestResponsePage<EditMedicationGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<EditMedicationGDO> getPageDto() {
		return pageDto;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

    public boolean isConfirm() {
		return confirm;
	}

	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}

	public void setMedicationCode(String medicationCode) {
		this.medicationCode = medicationCode;
    }

    public String getMedicationCode() {
    	return medicationCode;
    }

	public void setGdo(EditMedicationGDO gdo) {
		this.gdo = gdo;
	}

	public EditMedicationGDO getGdo() {
		return gdo;
	}

	public List<EditMedicationGDO> getMySelections() {
		return mySelections;
	}

	public void setMySelections(List<EditMedicationGDO> mySelections) {
		this.mySelections = mySelections;
	}
   
	
}