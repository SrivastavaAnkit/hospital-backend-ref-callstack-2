package com.hospital.file.medication.editmedication;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


import com.hospital.common.exception.ServiceException;

import com.hospital.model.CmdKeyEnum;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;


/**
 * Spring Validator for file 'Medication' (TSAJREP) and function 'Edit Medication' (TSAWEFR).
 *
 * @author X2EGenerator
 */
@Component
public class EditMedicationValidator implements Validator {
	
	

	@Override
	public boolean supports(Class<?> clazz) {
		return EditMedicationDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors e) {
		if (!(object instanceof EditMedicationDTO)) {
			e.reject("Not a valid EditMedicationDTO");
		} else {
			EditMedicationDTO dto = (EditMedicationDTO)object;
			EditMedicationGDO gdo = dto.getGdo();
			EditMedicationParams params = new EditMedicationParams();
			BeanUtils.copyProperties(dto, params);
		}
	}
}
