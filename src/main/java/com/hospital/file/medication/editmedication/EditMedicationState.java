package com.hospital.file.medication.editmedication;

import org.springframework.beans.factory.annotation.Autowired;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.UsrReturnCodeEnum;


/**
 * State for file 'Medication' (TSAJREP) and function 'Edit Medication' (TSAWEFR).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 *
 */

public class EditMedicationState extends EditMedicationDTO {
	
	private static final long serialVersionUID = -655783043731536719L;

	// Local fields
	private UsrReturnCodeEnum lclUsrReturnCode = null;

	// System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;


    public EditMedicationState() {

    }

	public void setLclUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
    	this.lclUsrReturnCode = usrReturnCode;
    }

    public UsrReturnCodeEnum getLclUsrReturnCode() {
    	return lclUsrReturnCode;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }
    
    }