package com.hospital.file.medication.deletemedication;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.medication.Medication;
import com.hospital.file.medication.MedicationId;
import com.hospital.file.medication.MedicationRepository;
import com.hospital.file.prescriptionline.rtvmedicationexist.RtvMedicationExistService;
import com.hospital.file.prescriptionline.rtvmedicationexist.RtvMedicationExistDTO;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class DeleteMedicationService  extends AbstractService<DeleteMedicationService, DeleteMedicationDTO>
{
    private final Step execute = define("execute", DeleteMedicationDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private MedicationRepository medicationRepository;
	
	@Autowired
	private RtvMedicationExistService rtvMedicationExistService;
	

    @Autowired
    public DeleteMedicationService() {
        super(DeleteMedicationService.class, DeleteMedicationDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(DeleteMedicationDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(DeleteMedicationDTO dto, DeleteMedicationDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataUpdate(dto);
		MedicationId medicationId = new MedicationId();
		medicationId.setMedicationCode(dto.getMedicationCode());
		try {
			medicationRepository.deleteById(medicationId);
			medicationRepository.flush();
			dto.setReturnCode(ReturnCodeEnum.fromCode(""));
			processingAfterDataUpdate(dto);
		} catch (Exception e) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
		}

       return result;
	}

	private StepResult processingBeforeDataUpdate(DeleteMedicationDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Generated:9)
		 */
		RtvMedicationExistDTO rtvMedicationExistDTO;
		//switchSUB 9 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT RTV Medication Exist? - Prescription Line  *
		rtvMedicationExistDTO = new RtvMedicationExistDTO();
		rtvMedicationExistDTO.setMedicationCode(dto.getMedicationCode());
		rtvMedicationExistService.execute(rtvMedicationExistDTO);
		dto.setLclUsrReturnCode(rtvMedicationExistDTO.getUsrReturnCode());
		//switchBLK 1000005 BLK CAS
		//switchSUB 1000005 BLK CAS
		if (dto.getLclUsrReturnCode() == UsrReturnCodeEnum.fromCode("F")) {
			// LCL.USR Return Code is Record found
			//switchBLK 1000008 BLK ACT
			//functionCall 1000009 ACT Send error message - 'Cant delete medication'
			// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1103230)
			//switchBLK 1000010 BLK ACT
			//functionCall 1000011 ACT <-- *QUIT
			// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
		}
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(DeleteMedicationDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:17)
		 */
		//switchSUB 17 SUB    
		//switchBLK 1000012 BLK ACT
		//functionCall 1000013 ACT PGM.*Return code = CND.*Normal
		dto.setReturnCode(ReturnCodeEnum.fromCode(""));
       return NO_ACTION;
	}
}
