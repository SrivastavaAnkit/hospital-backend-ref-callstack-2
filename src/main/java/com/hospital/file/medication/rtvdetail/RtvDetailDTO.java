package com.hospital.file.medication.rtvdetail;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvDetailDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private MedicationUnitEnum medicationUnit;
	private ReturnCodeEnum returnCode;
	private String medicationCode;
	private String medicationDescription;
	private String nextScreen;

	public String getMedicationCode() {
		return medicationCode;
	}

	public String getMedicationDescription() {
		return medicationDescription;
	}

	public MedicationUnitEnum getMedicationUnit() {
		return medicationUnit;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setMedicationCode(String medicationCode) {
		this.medicationCode = medicationCode;
	}

	public void setMedicationDescription(String medicationDescription) {
		this.medicationDescription = medicationDescription;
	}

	public void setMedicationUnit(MedicationUnitEnum medicationUnit) {
		this.medicationUnit = medicationUnit;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
