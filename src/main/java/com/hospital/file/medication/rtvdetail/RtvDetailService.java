package com.hospital.file.medication.rtvdetail;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.medication.Medication;
import com.hospital.file.medication.MedicationId;
import com.hospital.file.medication.MedicationRepository;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvDetailService extends AbstractService<RtvDetailService, RtvDetailDTO>
{
    private final Step execute = define("execute", RtvDetailDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private MedicationRepository medicationRepository;
	
    @Autowired
    public RtvDetailService()
    {
        super(RtvDetailService.class, RtvDetailDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvDetailDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvDetailDTO dto, RtvDetailDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Medication> medicationList = medicationRepository.findAllByIdMedicationCode(dto.getMedicationCode());
		if (!medicationList.isEmpty()) {
			for (Medication medication : medicationList) {
				processDataRecord(dto, medication);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvDetailDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Empty:48)
		 */
		//switchSUB 48 SUB    
		// Unprocessed SUB 48 -
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvDetailDTO dto, Medication medication) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		//switchSUB 41 SUB    
		//switchBLK 1000008 BLK ACT
		//functionCall 1000009 ACT PAR = DB1 By name
		dto.setMedicationDescription(medication.getMedicationDescription()); dto.setMedicationUnit(medication.getMedicationUnit());
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvDetailDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		//switchSUB 52 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT PAR = CON By name
		dto.setMedicationDescription(""); dto.setMedicationUnit(MedicationUnitEnum.fromCode(""));
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvDetailDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Empty:61)
		 */
		//switchSUB 61 SUB    
		// Unprocessed SUB 61 -
        return NO_ACTION;
    }
}
