package com.hospital.file.medication.dspfilprescribedmedicat;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.file.medication.Medication;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;

/**
 * Gdo for file 'Medication' (TSAJREP) and function 'DSPFIL Prescribed Medicat' (TSA3DFR).
 */
public class DspfilPrescribedMedicatGDO implements Serializable {
	private static final long serialVersionUID = -4382843569140840946L;

	private RecordSelectedEnum recordSelected = RecordSelectedEnum._STA_NO;
	private RecordDataChangedEnum recordDataChanged = RecordDataChangedEnum._STA_NO;
	private String gdo_selected = "";
	private String medicationCode = "";
	private String medicationDescription = "";
	private MedicationUnitEnum medicationUnit = null;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;

	public DspfilPrescribedMedicatGDO() {
 
	}
 
   	public DspfilPrescribedMedicatGDO(String medicationCode, String medicationDescription, MedicationUnitEnum medicationUnit, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime) {
		this.medicationCode = medicationCode;
		this.medicationDescription = medicationDescription;
		this.medicationUnit = medicationUnit;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}

	public void set_Selected(String gdo_selected) {
		this.gdo_selected = gdo_selected;
	}

	public String get_Selected() {
		return gdo_selected;
	}

	public void setMedicationCode(String medicationCode) {
    	this.medicationCode = medicationCode;
    }

	public String getMedicationCode() {
		return medicationCode;
	}

	public void setMedicationDescription(String medicationDescription) {
    	this.medicationDescription = medicationDescription;
    }

	public String getMedicationDescription() {
		return medicationDescription;
	}

	public void setMedicationUnit(MedicationUnitEnum medicationUnit) {
    	this.medicationUnit = medicationUnit;
    }

	public MedicationUnitEnum getMedicationUnit() {
		return medicationUnit;
	}

	public void setAddedUser(String addedUser) {
    	this.addedUser = addedUser;
    }

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
    	this.addedDate = addedDate;
    }

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
    	this.addedTime = addedTime;
    }

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
    	this.changedUser = changedUser;
    }

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
    	this.changedDate = changedDate;
    }

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
    	this.changedTime = changedTime;
    }

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void setRecordSelected(RecordSelectedEnum recordSelected) {
		this.recordSelected = recordSelected;
	}

	public RecordSelectedEnum getRecordSelected() {
		return recordSelected;
	}
 
	public void setRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		this.recordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum getRecordDataChanged() {
		return recordDataChanged;
	}

}