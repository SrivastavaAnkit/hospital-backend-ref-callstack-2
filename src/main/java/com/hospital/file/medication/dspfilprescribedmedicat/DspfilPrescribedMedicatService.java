package com.hospital.file.medication.dspfilprescribedmedicat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
 
import com.hospital.support.JobContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.stereotype.Service;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.file.medication.MedicationRepository;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.common.exception.ServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

	
/**
 * Service implementation for resource: DspfilPrescribedMedicat (TSA3DFR).
 *
 * @author X2EGenerator
 */
@Service
public class DspfilPrescribedMedicatService extends AbstractService<DspfilPrescribedMedicatService, DspfilPrescribedMedicatDTO> {

	@Autowired
	private JobContext job;


    @Autowired
    private MedicationRepository medicationRepository;
		
	public static final String SCREEN_CTL = "device.function.DSPFIL.ctl";
    public static final String SCREEN_RCD = "device.function.DSPFIL.rcd";
    
    private final Step DspfilPrescribedMedicat = define("DspfilPrescribedMedicat", DspfilPrescribedMedicatParams.class, this::DspfilPrescribedMedicat);
    private final Step response = define("ctlScreen", DspfilPrescribedMedicatDTO.class, this::processResponse);


    public DspfilPrescribedMedicatService() {
        super(DspfilPrescribedMedicatService.class, DspfilPrescribedMedicatDTO.class);
    }

    @Override
    public Step getInitialStep() {
        return DspfilPrescribedMedicat;
    }

    private StepResult DspfilPrescribedMedicat(DspfilPrescribedMedicatDTO dto, DspfilPrescribedMedicatParams params) {
    	StepResult result = NO_ACTION;
    	
    	BeanUtils.copyProperties(params, dto);
        result = usrInitializeProgram(dto);
        
        result = mainLoop(dto, params);

        return result;
    }

    /**
     * SCREEN_KEY initial processing loop method.
     * @param dto - Service state class.
     * @return
     */
    private StepResult mainLoop(DspfilPrescribedMedicatDTO dto, DspfilPrescribedMedicatParams params) {
        StepResult result = NO_ACTION;

        result = loadFirstSubfilePage(dto, params);
        
        dto.setReloadSubfile(ReloadSubfileEnum._STA_NO);
        result = conductScreenConversation(dto);

        return result;
    }
    
    /**
     * SCREEN  initial processing loop method.
     * @param dto - Service state class.
     * @return
     */
    private StepResult loadFirstSubfilePage(DspfilPrescribedMedicatDTO dto, DspfilPrescribedMedicatParams params) {
    	StepResult result = NO_ACTION;
    	
    	result = usrInitializeSubfileControl(dto, params);
    	
		dbfReadFirstDataRecord(dto);
		if (dto.getPageDto() != null && dto.getPageDto().getSize() > 0) {
		    result = loadNextSubfilePage(dto);
		}
		
        return result;
    }
    
    /**
     * Iterate on data loaded to do stuff on each record.
     * @param dto - Service state class.
     * @return
     */
    private StepResult loadNextSubfilePage(DspfilPrescribedMedicatDTO dto) {
    	StepResult result = NO_ACTION;
    	
    	List<DspfilPrescribedMedicatGDO> list = dto.getPageDto().getContent();
        for (DspfilPrescribedMedicatGDO gdo : list) {
            gdo.setRecordSelected(RecordSelectedEnum._STA_YES);
            result = usrInitializeSubfileRecordFromDBFRecord(dto, gdo);
//             TODO:moveDbfRecordFieldsToSubfileRecord(dto);  // synon built-in function
            if (gdo.getRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
//                TODO:writeSubfileRecord(dto);  // synon built-in function
            }
        }
        
        return result;
    }
    
    /**
     * SCREEN  initial processing loop method.
     * @param dto - Service state class.
     * @return
     */
    private StepResult conductScreenConversation(DspfilPrescribedMedicatDTO dto) {
        StepResult result = NO_ACTION;

        if (dto.getReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            result = callScreen(SCREEN_CTL, dto).thenCall(response);
        }

        return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param dto - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processResponse(DspfilPrescribedMedicatDTO dto, DspfilPrescribedMedicatDTO fromScreen) {
    	StepResult result = NO_ACTION;
    	
        // update dto from vm and use dto (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(fromScreen, dto);

        if (CmdKeyEnum.isExit(dto.get_SysCmdKey())) {
            result = closedown(dto);
        } else if (CmdKeyEnum.isReset(dto.get_SysCmdKey())) {
            dto.setReloadSubfile(ReloadSubfileEnum._STA_YES);
        } else if (CmdKeyEnum.isHelp(dto.get_SysCmdKey())) {
            //TODO:processHelpRequest(dto);//synon built-in function
        }
        else if (CmdKeyEnum.isNextPage(dto.get_SysCmdKey())) {
            result = loadNextSubfilePage(dto);
        } else {
            result = processScreen(dto);
        }

        return result;
    }
    
    /**
     * SCREEN process screen.
     * @param dto - Service state class.
     * @return
     */
    private StepResult processScreen(DspfilPrescribedMedicatDTO dto) {
    	StepResult result = NO_ACTION;
    	
        result = usrProcessSubfilePreConfirm(dto);
        if (!dto.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
        	return closedown(dto);
        } else {
        	if(!dto.getReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
        		return closedown(dto);
	        } else {
//	        	if(!dto.getProgramConfirm().getCode().equals(ProgramConfirmEnum._STA_YES.getCode())) {
//	        		return closedown(dto);
//		        } else {
//		        	result = usrProcessCommandKeys(dto);
//		        }
	        	
	        	result = usrProcessSubfileControlPostConfirm(dto);
	        	for (DspfilPrescribedMedicatGDO gdo : dto.getPageDto().getContent()) {
	                if(gdo.getRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
	                	result = usrProcessSubfileRecordPostConfirm(dto, gdo);
//	                  TODO:writeSubfileRecord(dto);   // synon built-in function
	                }
	            }
	        	result = usrFinalProcessingPostConfirm(dto);
	        	result = usrProcessCommandKeys(dto);
	        }
        }
        
        result = conductScreenConversation(dto);
        
        return result;
    }

    /**
     * SCREEN Process subfile Pre Confirm.
     * @param dto - Service state class.
     * @return
     */
    private StepResult usrProcessSubfilePreConfirm(DspfilPrescribedMedicatDTO dto) {
    	StepResult result = NO_ACTION;
    	
    	result = usrSubfileControlFunctionFields(dto);
    	result = usrProcessSubfileControlPreConfirm(dto);
    	if(dto.getReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
    		for (DspfilPrescribedMedicatGDO gdo : dto.getPageDto().getContent()) {
                if(gdo.getRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
                	result = usrSubfileRecordFunctionFields(dto, gdo);
                    result = usrProcessSubfileRecordPreConfirm(dto, gdo);
//                  TODO:writeSubfileRecord(dto);   // synon built-in function
                }
            }
    	}
        
        result = usrFinalProcessingPreConfirm(dto);
        
        return result;
    }
    
    /**
     * Terminate this program
     * @param dto - Service state class.
     * @return
     */
    private StepResult closedown(DspfilPrescribedMedicatDTO dto) {
        StepResult result = NO_ACTION;
        
        dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        result = usrExitProgramProcessing(dto);
        
        return result;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(DspfilPrescribedMedicatDTO dto) {
        dto.setPage(0);
        dbfReadDataRecord(dto);
    }

    private void dbfReadDataRecord(DspfilPrescribedMedicatDTO dto) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(dto.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = new PageRequest(dto.getPage(), dto.getSize());
        }
        else {
            pageable = new PageRequest(dto.getPage(), dto.getSize(), new Sort(sortOrders));
        }

        RestResponsePage<DspfilPrescribedMedicatGDO> pageDto = medicationRepository.dspfilPrescribedMedicat(dto.getMedicationCode(), pageable);
        dto.setPageDto(pageDto);
    }


	/**
	 * USER: Initialize Program (Empty:20)
	 */
    private StepResult usrInitializeProgram(DspfilPrescribedMedicatDTO dto) {
        try {
        	//switchSUB 20 SUB    
			// Unprocessed SUB 20 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return NO_ACTION;
    }
 
	/**
	 * USER: Initialize Subfile Control (Empty:182)
	 */
    private StepResult usrInitializeSubfileControl(DspfilPrescribedMedicatDTO dto, DspfilPrescribedMedicatParams params) {
        try {
        	//switchSUB 182 SUB    
			// Unprocessed SUB 182 -
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return NO_ACTION;
    }

	/**
	 * USER: Initialize Subfile Record from DBF Record (Empty:41)
	 */
    private StepResult usrInitializeSubfileRecordFromDBFRecord(DspfilPrescribedMedicatDTO dto, DspfilPrescribedMedicatGDO gdo) {
        try {
        	//switchSUB 41 SUB    
			// Unprocessed SUB 41 -
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return NO_ACTION;
    }
 
	/**
	 * CALC: Subfile Control Function Fields (Empty:175)
	 */
    private StepResult usrSubfileControlFunctionFields(DspfilPrescribedMedicatDTO dto) {
        try {
        	//switchSUB 175 SUB    
			// Unprocessed SUB 175 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return NO_ACTION;
    }

	/**
	 * USER: Process Subfile Control (Pre-Confirm) (Empty:72)
	 */
    private StepResult usrProcessSubfileControlPreConfirm(DspfilPrescribedMedicatDTO dto) {
        try {
        	//switchSUB 72 SUB    
			// Unprocessed SUB 72 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return NO_ACTION;
    }
 
	/**
	 * CALC: Subfile Record Function Fields (Empty:170)
	 */
    private StepResult usrSubfileRecordFunctionFields(DspfilPrescribedMedicatDTO dto, DspfilPrescribedMedicatGDO gdo) {
        try {
        	//switchSUB 170 SUB    
			// Unprocessed SUB 170 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return NO_ACTION;
    }

	/**
	 * USER: Process Subfile Record (Pre-Confirm) (Empty:101)
	 */
    private StepResult usrProcessSubfileRecordPreConfirm(DspfilPrescribedMedicatDTO dto, DspfilPrescribedMedicatGDO gdo) {
        try {
        	//switchSUB 101 SUB    
			// Unprocessed SUB 101 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return NO_ACTION;
    }
 
	/**
	 * USER: Final processing (Pre-confirm) (Empty:222)
	 */
    private StepResult usrFinalProcessingPreConfirm(DspfilPrescribedMedicatDTO dto) {
        try {
        	//switchSUB 222 SUB    
			// Unprocessed SUB 222 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return NO_ACTION;
    }
 
	/**
	 * USER: Process subfile control (Post-confirm) (Empty:225)
	 */
    private StepResult usrProcessSubfileControlPostConfirm(DspfilPrescribedMedicatDTO dto) {
        try {
        	//switchSUB 225 SUB    
			// Unprocessed SUB 225 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return NO_ACTION;
    }
 
	/**
	 * USER: Process Subfile Record (Post-Confirm Pass) (Empty:209)
	 */
    private StepResult usrProcessSubfileRecordPostConfirm(DspfilPrescribedMedicatDTO dto, DspfilPrescribedMedicatGDO gdo) {
        try {
        	//switchSUB 209 SUB    
			// Unprocessed SUB 209 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return NO_ACTION;
    }

	/**
	 * USER: Final processing (Post-confirm) (Empty:228)
	 */
    private StepResult usrFinalProcessingPostConfirm(DspfilPrescribedMedicatDTO dto) {
        try {
        	//switchSUB 228 SUB    
			// Unprocessed SUB 228 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return NO_ACTION;
    }
 
	/**
	 * USER: Process Command Keys (Empty:140)
	 */
    private StepResult usrProcessCommandKeys(DspfilPrescribedMedicatDTO dto) {
        try {
        	//switchSUB 140 SUB    
			// Unprocessed SUB 140 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return NO_ACTION;
    }

	/**
	 * USER: Exit Program Processing (Empty:132)
	 */
    private StepResult usrExitProgramProcessing(DspfilPrescribedMedicatDTO dto) {
        try {
        	//switchSUB 132 SUB    
			// Unprocessed SUB 132 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return NO_ACTION;
    }
  
}
