package com.hospital.file.medication.dspfilprescribedmedicat;

	
/**
 * Params for resource: DspfilPrescribedMedicat (TSA3DFR).
 *
 * @author X2EGenerator
 */
public class DspfilPrescribedMedicatParams {
	private static final long serialVersionUID = 39655844593159923L;
 
	private String medicationCode = "";

		
	public String getMedicationCode() {
		return medicationCode;
	}
	
	public void setMedicationCode(String medicationCode) {
		this.medicationCode = medicationCode;
	}
 
}