package com.hospital.file.medication.dspfilprescribedmedicat;
 
import java.math.BigDecimal;
  
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.common.utils.RestResponsePage;

import com.hospital.file.medication.Medication;
import com.hospital.common.state.BaseDTO;

/**
 * Dto for file 'Medication' (TSAJREP) and function 'DSPFIL Prescribed Medicat' (TSA3DFR).
 */
public class DspfilPrescribedMedicatDTO extends BaseDTO {
	private static final long serialVersionUID = 7437617271877746743L;
	private String selector = "";
	private RestResponsePage<DspfilPrescribedMedicatGDO> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";
 
	private String medicationCode = "";


	private DspfilPrescribedMedicatGDO gdo;

    public DspfilPrescribedMedicatDTO() {
 
    }
			
	public DspfilPrescribedMedicatDTO(String medicationCode) {
		this.medicationCode = medicationCode;
	}

	public void setSelector(String doctorCode) {
		this.selector = doctorCode;
	}

	public String getSelector() {
		return selector;
	}

	public void setPageDto(RestResponsePage<DspfilPrescribedMedicatGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<DspfilPrescribedMedicatGDO> getPageDto() {
		return pageDto;
	}
 
	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

   public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }
 
    public String getSortData() {
        return sortData;
    }
 
	public void setMedicationCode(String medicationCode) {
		this.medicationCode = medicationCode;
    }

    public String getMedicationCode() {
    	return medicationCode;
    }
 
	public void setGdo(DspfilPrescribedMedicatGDO gdo) {
		this.gdo = gdo;
	}

	public DspfilPrescribedMedicatGDO getGdo() {
		return gdo;
	}
 
}