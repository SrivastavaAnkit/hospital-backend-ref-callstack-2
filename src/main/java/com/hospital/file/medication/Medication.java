package com.hospital.file.medication;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.hospital.model.MedicationUnitConverter;
import com.hospital.model.MedicationUnitEnum;

@Entity
@Table(name="Medication", schema="HospitalMgmt")
public class Medication implements Serializable {
	private static final long serialVersionUID = -1L;

	@Version
	@Column(name="version")
	private long version;
	
	

	@EmbeddedId
	private MedicationId id = new MedicationId();

	@Column(name = "MedicationDescription")
	private String medicationDescription = "";
	
	@Column(name="medicationUnit", length=1)
	@Convert(converter=MedicationUnitConverter.class)
	private MedicationUnitEnum medicationUnit;

	public MedicationId getId() {
		return id;
	}

	public String getMedicationCode() {
		return id.getMedicationCode();
	}

	public String getMedicationDescription() {
		return medicationDescription;
	}
	
	public MedicationUnitEnum getMedicationUnit() {
		return medicationUnit;
	}

	

	public long getVersion() {
		return version;
	}

	public void setMedicationCode(String medicationCode) {
		this.id.setMedicationCode(medicationCode);
	}

	public void setMedicationDescription(String medicationDescription) {
		this.medicationDescription = medicationDescription;
	}
	
	public void setMedicationUnit(MedicationUnitEnum medicationUnit) {
		this.medicationUnit = medicationUnit;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
