package com.hospital.file.medication.changemedication;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ChangeMedicationDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private LocalDate addedDate;
	private LocalDate changedDate;
	private LocalTime addedTime;
	private LocalTime changedTime;
	private MedicationUnitEnum medicationUnit;
	private ReturnCodeEnum returnCode;
	private String addedUser;
	private String changedUser;
	private String medicationCode;
	private String medicationDescription;
	private UsrReturnCodeEnum usrReturnCode;
	private String nextScreen;
	private String lclMedicationDescription;

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public String getMedicationCode() {
		return medicationCode;
	}

	public String getMedicationDescription() {
		return medicationDescription;
	}

	public MedicationUnitEnum getMedicationUnit() {
		return medicationUnit;
	}

	public UsrReturnCodeEnum getUsrReturnCode() {
		return usrReturnCode;
	}

	public String getLclMedicationDescription() {
		return lclMedicationDescription;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public void setMedicationCode(String medicationCode) {
		this.medicationCode = medicationCode;
	}

	public void setMedicationDescription(String medicationDescription) {
		this.medicationDescription = medicationDescription;
	}

	public void setMedicationUnit(MedicationUnitEnum medicationUnit) {
		this.medicationUnit = medicationUnit;
	}

	public void setUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
		this.usrReturnCode = usrReturnCode;
	}

	public void setLclMedicationDescription(String lclMedicationDescription) {
		this.lclMedicationDescription = lclMedicationDescription;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
