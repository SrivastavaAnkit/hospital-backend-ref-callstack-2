package com.hospital.file.medication.changemedication;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.medication.Medication;
import com.hospital.file.medication.MedicationId;
import com.hospital.file.medication.MedicationRepository;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class ChangeMedicationService extends AbstractService<ChangeMedicationService, ChangeMedicationDTO>
{
    private final Step execute = define("execute", ChangeMedicationDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private MedicationRepository medicationRepository;
	

    @Autowired
    public ChangeMedicationService() {
        super(ChangeMedicationService.class, ChangeMedicationDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(ChangeMedicationDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(ChangeMedicationDTO dto, ChangeMedicationDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);
		MedicationId medicationId = new MedicationId();
		medicationId.setMedicationCode(dto.getMedicationCode());
		Medication medication = medicationRepository.findById(medicationId).get();
		if (medication == null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0005"));
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, medication);
			medication.setMedicationCode(dto.getMedicationCode());
			medication.setMedicationDescription(dto.getMedicationDescription());
			medication.setMedicationUnit(dto.getMedicationUnit());
			/*medication.setAddedUser(dto.getAddedUser());
			medication.setAddedDate(dto.getAddedDate());
			medication.setAddedTime(dto.getAddedTime());
			medication.setChangedUser(dto.getChangedUser());
			medication.setChangedDate(dto.getChangedDate());
			medication.setChangedTime(dto.getChangedTime());*/
			processingBeforeDataUpdate(dto, medication);
			try {
				medicationRepository.saveAndFlush(medication);
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
			}
		}

       return result;
	}

	private StepResult processingBeforeDataRead(ChangeMedicationDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000036 BLK CAS
		//switchSUB 1000036 BLK CAS
		if (dto.getMedicationUnit() == MedicationUnitEnum.fromCode("C")) {
			// PAR.Medication Unit is Capsules
			//switchBLK 1000039 BLK ACT
			//functionCall 1000040 ACT LCL.Medication Description = CON.Capsule
			dto.setLclMedicationDescription("Capsule");
		}
       return NO_ACTION;
	}

	private StepResult processingIfDataRecordNotFound(ChangeMedicationDTO dto) throws ServiceException {
		/**
		 * USER: Processing if Data Record Not Found (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000003 BLK ACT
		//functionCall 1000004 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
	}

	private StepResult processingAfterDataRead(ChangeMedicationDTO dto, Medication medication) throws ServiceException {
		/**
		 * USER: Processing After Data Read (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000048 BLK CAS
		//switchSUB 1000048 BLK CAS
		if (medication.getMedicationUnit() == MedicationUnitEnum.fromCode("C")) {
			// DB1.Medication Unit is Capsules
			//switchBLK 1000051 BLK ACT
			//functionCall 1000052 ACT LCL.Medication Description = CON.Capsule
			dto.setLclMedicationDescription("Capsule");
		}
       return NO_ACTION;
	}

	private StepResult processingBeforeDataUpdate(ChangeMedicationDTO dto, Medication medication) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Generated:10)
		 */
		//switchSUB 10 SUB    
		//switchBLK 1000018 BLK ACT
		//functionCall 1000019 ACT DB1.Changed User = JOB.*USER
		/*medication.setChangedUser(job.getUser());
		//switchBLK 1000022 BLK ACT
		//functionCall 1000023 ACT DB1.Changed Date = JOB.*Job date
		medication.setChangedDate(LocalDate.now());
		//switchBLK 1000026 BLK ACT
		//functionCall 1000027 ACT DB1.Changed Time = JOB.*Job time
		medication.setChangedTime(LocalTime.now());*/
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(ChangeMedicationDTO dto) throws ServiceException {
		/**
		 * USER: Processing After Data Update (Generated:23)
		 */
		//switchSUB 23 SUB    
		//switchBLK 1000030 BLK ACT
		//functionCall 1000031 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
	}
}
