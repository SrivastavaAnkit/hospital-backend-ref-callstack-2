package com.hospital.file.medication.ereditmedstestif;

import com.hospital.file.medication.Medication;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.UsrReturnCodeEnum;


/**
 * State for file 'Medication' (TSAJREP) and function 'ER Edit Meds test IF' (TSBIE1R).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
public class ErEditMedsTestIfState extends ErEditMedsTestIfDTO {
    private static final long serialVersionUID = -8852244374353328849L;


    // Local fields
    private UsrReturnCodeEnum lclUsrReturnCode = null;

    // System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysConductKeyScreenConversation = true;
    private boolean _sysConductDetailScreenConversation = true;
    private boolean _sysErrorFound = false;

    public ErEditMedsTestIfState() {
    }

    public ErEditMedsTestIfState(Medication medication) {
        setDtoFields(medication);
    }

    public void setLclUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
        this.lclUsrReturnCode = usrReturnCode;
    }

    public UsrReturnCodeEnum getLclUsrReturnCode() {
        return lclUsrReturnCode;
    }

   public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
       _sysReloadSubfile = reloadSubfile;
   }

   public ReloadSubfileEnum get_SysReloadSubfileEnum() {
       return _sysReloadSubfile;
   }

    public boolean get_SysConductKeyScreenConversation() {
        return _sysConductKeyScreenConversation;
    }

    public void set_SysConductKeyScreenConversation(boolean conductKeyScreenConversation) {
        _sysConductKeyScreenConversation = conductKeyScreenConversation;
    }

    public boolean get_SysConductDetailScreenConversation() {
        return _sysConductDetailScreenConversation;
    }

    public void set_SysConductDetailScreenConversation(boolean conductDetailScreenConversation) {
        _sysConductDetailScreenConversation = conductDetailScreenConversation;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
