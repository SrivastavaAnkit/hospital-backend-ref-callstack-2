package com.hospital.file.medication.ereditmedstestif;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.common.state.BaseDTO;
import com.hospital.file.medication.Medication;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;


/**
 * DTO for file 'Medication' (TSAJREP) and function 'ER Edit Meds test IF' (TSBIE1R).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
public class ErEditMedsTestIfDTO extends BaseDTO {
    private static final long serialVersionUID = 3972571842534721671L;

    private long version = 0;
    private String medicationCode = "";
    private String sflselPromptText = "";
    private String medicationDescription = "";
    private MedicationUnitEnum medicationUnit = null;
    private String addedUser = "";
    private LocalDate addedDate = null;
    private LocalTime addedTime = null;
    private String changedUser = "";
    private LocalDate changedDate = null;
    private LocalTime changedTime = null;
    private BigDecimal amount = BigDecimal.ZERO;
    private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");

    public ErEditMedsTestIfDTO() {
    }

    public ErEditMedsTestIfDTO(Medication medication) {
        setDtoFields(medication);
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public long getVersion() {
        return version;
    }

    public String getMedicationCode() {
        return medicationCode;
    }

    public void setMedicationCode(String medicationCode) {
        this.medicationCode = medicationCode;
    }

    public String getSflselPromptText() {
        return sflselPromptText;
    }

    public void setSflselPromptText(String sflselPromptText) {
        this.sflselPromptText = sflselPromptText;
    }

    public String getMedicationDescription() {
        return medicationDescription;
    }

    public void setMedicationDescription(String medicationDescription) {
        this.medicationDescription = medicationDescription;
    }

    public MedicationUnitEnum getMedicationUnit() {
        return medicationUnit;
    }

    public void setMedicationUnit(MedicationUnitEnum medicationUnit) {
        this.medicationUnit = medicationUnit;
    }

    public String getAddedUser() {
        return addedUser;
    }

    public void setAddedUser(String addedUser) {
        this.addedUser = addedUser;
    }

    public LocalDate getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(LocalDate addedDate) {
        this.addedDate = addedDate;
    }

    public LocalTime getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(LocalTime addedTime) {
        this.addedTime = addedTime;
    }

    public String getChangedUser() {
        return changedUser;
    }

    public void setChangedUser(String changedUser) {
        this.changedUser = changedUser;
    }

    public LocalDate getChangedDate() {
        return changedDate;
    }

    public void setChangedDate(LocalDate changedDate) {
        this.changedDate = changedDate;
    }

    public LocalTime getChangedTime() {
        return changedTime;
    }

    public void setChangedTime(LocalTime changedTime) {
        this.changedTime = changedTime;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public ReturnCodeEnum getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(ReturnCodeEnum returnCode) {
        this.returnCode = returnCode;
    }

    /**
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param medication Medication Entity bean
     */
    public void setDtoFields(Medication medication) {
        this.version = medication.getVersion();
        BeanUtils.copyProperties(medication, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param medication Medication Entity bean
     */
    public void setEntityFields(Medication medication) {
        medication.setVersion(this.version);
        BeanUtils.copyProperties(this, medication);
    }
}
