package com.hospital.file.medication.ereditmedstestif;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


import com.hospital.common.exception.ServiceException;

import com.hospital.model.CmdKeyEnum;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;


/**
 * Spring Validator for file 'Medication' (TSAJREP) and function 'ER Edit Meds test IF' (TSBIE1R).
 *
 * @author X2EGenerator
 */
@Component
public class ErEditMedsTestIfValidator implements Validator {
	
	

	@Override
	public boolean supports(Class<?> clazz) {
		return ErEditMedsTestIfDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors e) {
		if (!(object instanceof ErEditMedsTestIfDTO)) {
			e.reject("Not a valid ErEditMedsTestIfDTO");
		} else {
			ErEditMedsTestIfDTO dto = (ErEditMedsTestIfDTO)object;

			/**
			 * Validate Key Fields
			 *
			 */
			if ("".equals(dto.getMedicationCode())) {
				e.rejectValue("medicationCode", "value.required");
			}
			ErEditMedsTestIfParams params = new ErEditMedsTestIfParams();
			BeanUtils.copyProperties(dto, params);
			try {
				/**
				 * USER: Validate Detail Screen Fields (Generated:502)
				 */
				//switchSUB 502 SUB    
				//switchBLK 1000085 BLK CAS
				//switchSUB 1000085 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
					// DTL.*CMD key is *Cancel
					//switchBLK 1000089 BLK ACT
					//functionCall 1000090 ACT PAR.*Return code = CND.E
					dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
					//switchBLK 1000095 BLK ACT
					//functionCall 1000096 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
				//switchBLK 1000088 BLK TXT
				// 
				//switchBLK 1000001 BLK CAS
				//switchSUB 1000001 BLK CAS
				if (dto.getMedicationUnit() == MedicationUnitEnum.fromCode("C")) {
					// DTL.Medication Unit is Capsules
					//switchBLK 1000004 BLK ACT
					//functionCall 1000005 ACT DTL.Amount = CON.100.00
					dto.setAmount(new BigDecimal("100.00"));
					//switchBLK 1000058 BLK CAS
					//switchSUB 1000058 BLK CAS
					if (dto.getMedicationUnit() == MedicationUnitEnum.fromCode("P")) {
						// DTL.Medication Unit is Pills
						//switchBLK 1000042 BLK ACT
						//functionCall 1000043 ACT DTL.Amount = CON.110.00
						dto.setAmount(new BigDecimal("110.00"));
						//switchBLK 1000017 BLK CAS
						//switchSUB 1000017 BLK CAS
						if (dto.getMedicationUnit() == MedicationUnitEnum.fromCode("T")) {
							// DTL.Medication Unit is Tablets
							//switchBLK 1000020 BLK ACT
							//functionCall 1000021 ACT DTL.Amount = CON.120.00
							dto.setAmount(new BigDecimal("120.00"));
						}//switchSUB 1000026 SUB    
						 else {
							// *OTHERWISE
							//switchBLK 1000028 BLK ACT
							//functionCall 1000029 ACT DTL.Amount = CON.150.00
							dto.setAmount(new BigDecimal("150.00"));
						}
					}
				}
				//switchBLK 1000034 BLK ACT
				//functionCall 1000035 ACT PGM.*Reload subfile = CND.*YES
				dto.setReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
			} catch (Exception se) {
				se.printStackTrace();
			}
			try {
				/**
				 * USER: Validate Detail Screen Relations (Generated:268)
				 */
				//switchSUB 268 SUB    
				//switchBLK 1000061 BLK CAS
				//switchSUB 1000061 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
					// DTL.*CMD key is *Cancel
					//switchBLK 1000064 BLK ACT
					//functionCall 1000065 ACT PAR.*Return code = CND.E
					dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
					//switchBLK 1000070 BLK ACT
					//functionCall 1000071 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			} catch (Exception se) {
				se.printStackTrace();
			}
		}
	}
}
