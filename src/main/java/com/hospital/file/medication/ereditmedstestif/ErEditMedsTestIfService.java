package com.hospital.file.medication.ereditmedstestif;

import java.math.BigDecimal;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.exception.ServiceException;
import com.hospital.file.medication.Medication;
import com.hospital.file.medication.MedicationId;
import com.hospital.file.medication.MedicationRepository;
import com.hospital.file.medication.changemedication.ChangeMedicationDTO;
import com.hospital.file.medication.changemedication.ChangeMedicationService;
import com.hospital.file.medication.createmedication.CreateMedicationDTO;
import com.hospital.file.medication.createmedication.CreateMedicationService;
import com.hospital.file.medication.deletemedication.DeleteMedicationDTO;
import com.hospital.file.medication.deletemedication.DeleteMedicationService;
import com.hospital.file.medication.selectmedication.SelectMedicationParams;
import com.hospital.file.medication.selectmedication.SelectMedicationService;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ProgramModeEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.support.JobContext;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;


/**
 * EDTRCD Service controller for 'ER Edit Meds test IF' (TSBIE1R) of file 'Medication' (TSAJREP)
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
@Service
public class ErEditMedsTestIfService extends AbstractService<ErEditMedsTestIfService, ErEditMedsTestIfState>
{
    
    @Autowired
    private JobContext job;
    
    @Autowired
    private MedicationRepository medicationRepository;
    
    @Autowired
    private ChangeMedicationService changeMedicationService;
    
    @Autowired
    private CreateMedicationService createMedicationService;
    
    @Autowired
    private DeleteMedicationService deleteMedicationService;
    
        
    public static final String SCREEN_KEY = "erEditMedsTestIfEntryPanel";
    public static final String SCREEN_DTL = "erEditMedsTestIfPanel";
    public static final String SCREEN_CFM = "ErEditMedsTestIf.confirm";
    
    private final Step execute = define("execute", ErEditMedsTestIfParams.class, this::execute);
    private final Step keyScreenResponse = define("keyScreen", ErEditMedsTestIfDTO.class, this::processKeyScreenResponse);
    private final Step detailScreenResponse = define("dtlScreen", ErEditMedsTestIfDTO.class, this::processDetailScreenResponse);
    private final Step confirmScreenResponse = define("cfmScreen", ErEditMedsTestIfDTO.class, this::processConfirmScreenResponse);
    private final Step promptSelectMedication = define("promptSelectMedication",SelectMedicationParams.class, this::processPromptSelectMedication);
    //private final Step serviceDeleteMedication = define("serviceDeleteMedication",DeleteMedicationParams.class, this::processServiceDeleteMedication);
    //private final Step serviceCreateMedication = define("serviceCreateMedication",CreateMedicationParams.class, this::processServiceCreateMedication);
    //private final Step serviceChangeMedication = define("serviceChangeMedication",ChangeMedicationParams.class, this::processServiceChangeMedication);
    //private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
    //private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
    
        @Autowired
    public ErEditMedsTestIfService()
    {
        super(ErEditMedsTestIfService.class, ErEditMedsTestIfState.class);
    }
    
    @Override
    public Step getInitialStep()
    {
        return execute;
    }
    
    /**
     * EDTRCD controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(ErEditMedsTestIfState state, ErEditMedsTestIfParams params)
    {
        StepResult result = NO_ACTION;
    
        if(params != null) {
            BeanUtils.copyProperties(params, state);
        }
    
        state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
    
        result = conductKeyScreenConversation(state);
    
        return result;
    }
    
    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductKeyScreenConversation(ErEditMedsTestIfState state) 
    {
        StepResult result = NO_ACTION;
    
        result = displayKeyScreenConversation(state);
    
        return result;
    }
    
    /**
     * SCREEN_KEY display processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult displayKeyScreenConversation(ErEditMedsTestIfState state)
    {
        StepResult result = NO_ACTION;
    
        if (state.get_SysConductKeyScreenConversation()) {
            ErEditMedsTestIfDTO model = new ErEditMedsTestIfDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_KEY, model).thenCall(keyScreenResponse);
        }
    
        return result;
    }
    
    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processKeyScreenResponse(ErEditMedsTestIfState state, ErEditMedsTestIfDTO model)
    {
        StepResult result = NO_ACTION;
    
        BeanUtils.copyProperties(model, state);
    
        if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            result = displayKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            result = conductKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
           result = closedown(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
    		switch (state.get_SysEntrySelected())
    		    {
    		        case "medicationCode":
    		            SelectMedicationParams selectMedicationParams = new SelectMedicationParams();
    		            BeanUtils.copyProperties(state, selectMedicationParams);
    		            result = StepResult.callService(SelectMedicationService.class, selectMedicationParams).thenCall(promptSelectMedication);
    		            break;
    		        default:
    		            System.out.println("Field "+ state.get_SysEntrySelected() + " is not promptable");
    		            result = displayKeyScreenConversation(state);
    		            break;
    		    }
        }
        else if(CmdKeyEnum.isChangeMode(state.get_SysCmdKey())) {
            if(state.get_SysProgramMode() == ProgramModeEnum._STA_ADD) {
                state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            }
            else {
                state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
            }
            result = conductKeyScreenConversation(state);
        }
        else {
            checkKeyFields(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = displayKeyScreenConversation(state);
                return result;
            }
            dbfReadDataRecord(state);
    
            result = conductDetailScreenConversation(state);
        }
    
        return result;
    }
    
    /**
     * Check key fields set in SCREEN_KEY.
     * For now it's just if fields are null or strings are empty
     * @param state - Service state class.
     * @return
     */
    private void checkKeyFields(ErEditMedsTestIfState state) {
       if(state.getMedicationCode() == null || state.getMedicationCode().isEmpty())
       {
           state.set_SysErrorFound(true);
       }
    }
    
    /**
     * SCREEN_DTL display processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductDetailScreenConversation(ErEditMedsTestIfState state)
    {
        StepResult result = NO_ACTION;
    
        if (state.get_SysConductDetailScreenConversation()) {
            ErEditMedsTestIfDTO model = new ErEditMedsTestIfDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_DTL, model).thenCall(detailScreenResponse);
        }
    
        return result;
    }
    
    /**
     * SCREEN_DETAIL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processDetailScreenResponse(ErEditMedsTestIfState state, ErEditMedsTestIfDTO model)
    {
        StepResult result = NO_ACTION;
    
        BeanUtils.copyProperties(model, state);
    
        if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            result = conductDetailScreenConversation(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            result = conductDetailScreenConversation(state);
        }
        else if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            result = closedown(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
    
        }
        else if (CmdKeyEnum.isKeyScreen(state.get_SysCmdKey())) {
            result = conductKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isDelete(state.get_SysCmdKey())) {
            //delete action confirmed in client side
            dbfDeleteDataRecord(state);
            result = conductDetailScreenConversation(state);
        }
        else {
            checkFields(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = conductDetailScreenConversation(state);
                return result;
            }
            usrValidateDetailScreenFields(state);
            checkRelations(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = conductDetailScreenConversation(state);
                return result;
            }
            usrValidateDetailScreenRelations(state);
            //TODO: make confirm screen
            result = processConfirmScreenResponse(state, null);//callScreen(SCREEN_CFM, state).thenCall(confirmScreenResponse);
        }
    
        return result;
    }
    /**
     * SCREEN_CONFIRM returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processConfirmScreenResponse(ErEditMedsTestIfState state, ErEditMedsTestIfDTO model)
    {
        StepResult result = NO_ACTION;
    
        if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
            dbfUpdateDataRecord(state);
        }
        else {
            dbfCreateDataRecord(state);
        }
    
        result = conductKeyScreenConversation(state);
    
        return result;
    }
    
    /**
     * Check fields set in SCREEN_DETAIL.
     * @param state - Service state class.
     * @return
     */
    private void checkFields(ErEditMedsTestIfState state) {
    
    }
    
    /**
     * Check relations set in SCREEN_DETAIL.
     * @param state - Service state class.
     * @return
     */
    private void checkRelations(ErEditMedsTestIfState state) {
    
    }
    
    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(ErEditMedsTestIfState state)
    {
        StepResult result = NO_ACTION;
    
        result = usrExitCommandProcessing(state);
    
        ErEditMedsTestIfParams params = new ErEditMedsTestIfParams();
        BeanUtils.copyProperties(state, params);
        result = StepResult.returnFromService(params);
    
        return result;
    }
        
    /**
     * ------------------------ Generated DBF methods ----------------------
     */
    
    private StepResult dbfReadDataRecord(ErEditMedsTestIfState dto)
    {
        StepResult result = NO_ACTION;
    
        MedicationId medicationId = new MedicationId(dto.getMedicationCode());
        Medication medication = medicationRepository.findById(medicationId).orElse(null);
    
        if (medication == null) {
            dto.set_SysProgramMode(ProgramModeEnum._STA_ADD);
        }
        else {
            dto.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            BeanUtils.copyProperties(medication, dto);
        }
        return result;
    }
    
    private StepResult dbfCreateDataRecord(ErEditMedsTestIfState dto)
    {
        StepResult result = NO_ACTION;
        try {
            /**
    		 * USER: Create Object (Generated:425)
    		 */
    		CreateMedicationDTO createMedicationDTO;
    		//switchBLK 425 BLK ACT
    		// DEBUG genFunctionCall 426 ACT Create Medication - Medication  *
    		// DEBUG genFunctionCall ServiceDtoVariable
    		createMedicationDTO = new CreateMedicationDTO();
    		// DEBUG genFunctionCall Parameters IN
    		createMedicationDTO.setMedicationCode(dto.getMedicationCode());
    		createMedicationDTO.setMedicationDescription(dto.getMedicationDescription());
    		createMedicationDTO.setMedicationUnit(dto.getMedicationUnit());
    		// DEBUG genFunctionCall Service call
    		createMedicationService.execute(createMedicationDTO);
    		// DEBUG genFunctionCall Parameters OUT
    		//dto.setLclUsrReturnCode(createMedicationDTO.getUsrReturnCode());
    		// DEBUG genFunctionCall Parameters DONE
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }
    private StepResult dbfDeleteDataRecord(ErEditMedsTestIfState dto)
    {
        StepResult result = NO_ACTION;
        try {
            /**
    		 * USER: Delete Object (Generated:383)
    		 */
    		DeleteMedicationDTO deleteMedicationDTO;
    		//switchBLK 383 BLK ACT
    		// DEBUG genFunctionCall 384 ACT Delete Medication - Medication  *
    		// DEBUG genFunctionCall ServiceDtoVariable
    		deleteMedicationDTO = new DeleteMedicationDTO();
    		// DEBUG genFunctionCall Parameters IN
    		deleteMedicationDTO.setMedicationCode(dto.getMedicationCode());
    		// DEBUG genFunctionCall Service call
    		deleteMedicationService.execute(deleteMedicationDTO);
    		// DEBUG genFunctionCall Parameters OUT
    		// DEBUG genFunctionCall Parameters DONE
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    
        return result;
    }
    
    private StepResult dbfUpdateDataRecord(ErEditMedsTestIfState dto)
    {
        StepResult result = NO_ACTION;
        try {
    		MedicationId medicationId = new MedicationId(dto.getMedicationCode());
    		if (medicationRepository.existsById(medicationId)) {
    
            	/**
    			 * USER: Change Object (Generated:427)
    			 */
    			ChangeMedicationDTO changeMedicationDTO;
    			//switchBLK 427 BLK ACT
    			// DEBUG genFunctionCall 428 ACT Change Medication - Medication  *
    			// DEBUG genFunctionCall ServiceDtoVariable
    			changeMedicationDTO = new ChangeMedicationDTO();
    			// DEBUG genFunctionCall Parameters IN
    			changeMedicationDTO.setMedicationCode(dto.getMedicationCode());
    			changeMedicationDTO.setMedicationDescription(dto.getMedicationDescription());
    			changeMedicationDTO.setMedicationUnit(dto.getMedicationUnit());
    			// DEBUG genFunctionCall Service call
    			changeMedicationService.execute(changeMedicationDTO);
    			// DEBUG genFunctionCall Parameters OUT
    			//dto.setLclUsrReturnCode(changeMedicationDTO.getUsrReturnCode());
    			// DEBUG genFunctionCall Parameters DONE
    	        }
            else {
                throw new ServiceException("diagnosis.nf");
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }
    
    
/**
 * ------------------ Generated ActionDiagram UserPoint -----------------
 */

    private StepResult usrValidateDetailScreenFields(ErEditMedsTestIfState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Validate Detail Screen Fields (Generated:502)
			 */
			//switchSUB 502 SUB    
			//switchBLK 1000085 BLK CAS
			//switchSUB 1000085 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				//switchBLK 1000089 BLK ACT
				// DEBUG genFunctionCall 1000090 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000095 BLK ACT
				// DEBUG genFunctionCall 1000096 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
			//switchBLK 1000088 BLK TXT
			// 
			//switchBLK 1000001 BLK CAS
			//switchSUB 1000001 BLK CAS
			if (dto.getMedicationUnit() == MedicationUnitEnum.fromCode("C")) {
				// DTL.Medication Unit is Capsules
				//switchBLK 1000004 BLK ACT
				// DEBUG genFunctionCall 1000005 ACT DTL.Amount = CON.100.00
				dto.setAmount(new BigDecimal("100.00"));
				//switchBLK 1000058 BLK CAS
				//switchSUB 1000058 BLK CAS
				if (dto.getMedicationUnit() == MedicationUnitEnum.fromCode("P")) {
					// DTL.Medication Unit is Pills
					//switchBLK 1000042 BLK ACT
					// DEBUG genFunctionCall 1000043 ACT DTL.Amount = CON.110.00
					dto.setAmount(new BigDecimal("110.00"));
					//switchBLK 1000017 BLK CAS
					//switchSUB 1000017 BLK CAS
					if (dto.getMedicationUnit() == MedicationUnitEnum.fromCode("T")) {
						// DTL.Medication Unit is Tablets
						//switchBLK 1000020 BLK ACT
						// DEBUG genFunctionCall 1000021 ACT DTL.Amount = CON.120.00
						dto.setAmount(new BigDecimal("120.00"));
					}//switchSUB 1000026 SUB    
					 else {
						// *OTHERWISE
						//switchBLK 1000028 BLK ACT
						// DEBUG genFunctionCall 1000029 ACT DTL.Amount = CON.150.00
						dto.setAmount(new BigDecimal("150.00"));
					}
				}
			}
			//switchBLK 1000034 BLK ACT
			// DEBUG genFunctionCall 1000035 ACT PGM.*Reload subfile = CND.*YES
			//dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrValidateDetailScreenRelations(ErEditMedsTestIfState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Validate Detail Screen Relations (Generated:268)
			 */
			//switchSUB 268 SUB    
			//switchBLK 1000061 BLK CAS
			//switchSUB 1000061 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				//switchBLK 1000064 BLK ACT
				// DEBUG genFunctionCall 1000065 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000070 BLK ACT
				// DEBUG genFunctionCall 1000071 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrExitCommandProcessing(ErEditMedsTestIfState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Exit Program Processing (Generated:64)
			 */
			//switchSUB 64 SUB    
			//switchBLK 1000073 BLK CAS
			//switchSUB 1000073 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// KEY.*CMD key is *Exit
				//switchBLK 1000076 BLK ACT
				// DEBUG genFunctionCall 1000077 ACT PAR.*Return code = CND.*Normal
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000082 BLK ACT
				// DEBUG genFunctionCall 1000083 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }


    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * DeleteMedicationService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceDeleteMedication(ErEditMedsTestIfState state, DeleteMedicationParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * CreateMedicationService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateMedication(ErEditMedsTestIfState state, CreateMedicationParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ChangeMedicationService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChangeMedication(ErEditMedsTestIfState state, ChangeMedicationParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(ErEditMedsTestIfState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(ErEditMedsTestIfState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//

    /**
     * SelectPatientService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processPromptSelectMedication(ErEditMedsTestIfState state, SelectMedicationParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        result = displayKeyScreenConversation(state);

        return result;
    }

}
