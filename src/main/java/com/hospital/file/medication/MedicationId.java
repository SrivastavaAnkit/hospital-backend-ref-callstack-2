package com.hospital.file.medication;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.io.Serializable;
import javax.persistence.Column;

public class MedicationId implements Serializable {
	private static final long serialVersionUID = -1L;

	@Column(name = "MedicationCode")
	private String medicationCode = "";

	public MedicationId() {
	
	}

	public MedicationId(String medicationCode) {
		this.medicationCode = medicationCode;
	}

	public String getMedicationCode() {
		return medicationCode;
	}
	
	public void setMedicationCode(String medicationCode) {
		this.medicationCode = medicationCode;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
