package com.hospital.file.medication;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.hospital.file.medication.selectmedication.SelectMedicationGDO;
import com.hospital.file.medication.editmedication.EditMedicationGDO;
import com.hospital.file.medication.dspfilprescribedmedicat.DspfilPrescribedMedicatGDO;
import com.hospital.file.medication.pvmenucalls.PvMenuCallsDTO;

/**
 * Custom Spring Data JPA repository interface for model: Medication (TSAJREP).
 *
 * @author X2EGenerator
 */
@Repository
public interface MedicationRepositoryCustom {

	/**
	 * 
	 * @param medicationCode Medication Code
	 */
	void deleteMedication(String medicationCode);

	/**
	 * 
	 * @param medicationCode Medication Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of SelectMedicationGDO
	 */
	RestResponsePage<SelectMedicationGDO> selectMedication(String medicationCode, Pageable pageable);

	/**
	 * 
	 * @param medicationCode Medication Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of EditMedicationGDO
	 */
	RestResponsePage<EditMedicationGDO> editMedication(String medicationCode, Pageable pageable);

	/**
	 * 
	 * @param medicationCode Medication Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of DspfilPrescribedMedicatGDO
	 */
	RestResponsePage<DspfilPrescribedMedicatGDO> dspfilPrescribedMedicat(String medicationCode, Pageable pageable);

	/**
	 * 
	 * @param medicationCode Medication Code
	 * @return Medication
	 */
	Medication rtvDetail(String medicationCode);

	/**
	 * 
	 * @param pageable a Pageable object used for pagination
	 * @return Page of PvMenuCallsDTO
	 */
	RestResponsePage<PvMenuCallsDTO> pvMenuCalls(Pageable pageable);
}
