package com.hospital.file.medication;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;

import com.hospital.file.medication.Medication;
import com.hospital.file.medication.selectmedication.SelectMedicationGDO;
import com.hospital.file.medication.editmedication.EditMedicationGDO;
import com.hospital.file.medication.dspfilprescribedmedicat.DspfilPrescribedMedicatGDO;
import com.hospital.file.medication.pvmenucalls.PvMenuCallsDTO;

/**
 * Custom Spring Data JPA repository implementation for model: Medication (TSAJREP).
 *
 * @author X2EGenerator
 */
@Repository
public class MedicationRepositoryImpl implements MedicationRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see com.hospital.file.medication.MedicationService#deleteMedication(Medication)
	 */
	@Override
	public void deleteMedication(
		String medicationCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(medicationCode)) {
			whereClause += " medication.id.medicationCode = :medicationCode";
			isParamSet = true;
		}

		String sqlString = "DELETE FROM Medication medication";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(medicationCode)) {
			query.setParameter("medicationCode", medicationCode);
		}

		em.createNativeQuery("SET foreign_key_checks = 0").executeUpdate();
		query.executeUpdate();
		em.createNativeQuery("SET foreign_key_checks = 1").executeUpdate();
	}

	@Override
	public RestResponsePage<SelectMedicationGDO> selectMedication(
		String medicationCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(medicationCode)) {
			whereClause += " (medication.id.medicationCode >= :medicationCode OR medication.id.medicationCode like CONCAT('%', :medicationCode, '%'))";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.medication.selectmedication.SelectMedicationGDO(medication.id.medicationCode, medication.medicationDescription, medication.medicationUnit, medication.addedUser, medication.addedDate, medication.addedTime, medication.changedUser, medication.changedDate, medication.changedTime) from Medication medication";
		String countQueryString = "SELECT COUNT(medication.id.medicationCode) FROM Medication medication";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Medication medication = new Medication();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(medication.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"medication.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"medication." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"medication.id.medicationCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(medicationCode)) {
			countQuery.setParameter("medicationCode", medicationCode);
			query.setParameter("medicationCode", medicationCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<SelectMedicationGDO> content = query.getResultList();
		RestResponsePage<SelectMedicationGDO> pageDto = new RestResponsePage<SelectMedicationGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<EditMedicationGDO> editMedication(
		String medicationCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(medicationCode)) {
			whereClause += " (medication.id.medicationCode >= :medicationCode OR medication.id.medicationCode like CONCAT('%', :medicationCode, '%'))";
			isParamSet = true;
		}
		// TODO: Remove added date, time and user from sqlstring query
		String sqlString = "SELECT new com.hospital.file.medication.editmedication.EditMedicationGDO(medication.id.medicationCode, medication.medicationDescription, medication.medicationUnit) from Medication medication";
		String countQueryString = "SELECT COUNT(medication.id.medicationCode) FROM Medication medication";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Medication medication = new Medication();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(medication.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"medication.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"medication." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"medication.id.medicationCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(medicationCode)) {
			countQuery.setParameter("medicationCode", medicationCode);
			query.setParameter("medicationCode", medicationCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<EditMedicationGDO> content = query.getResultList();
		RestResponsePage<EditMedicationGDO> pageDto = new RestResponsePage<EditMedicationGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<PvMenuCallsDTO> pvMenuCalls(
		Pageable pageable) {
		String sqlString = "SELECT new com.hospital.file.medication.pvmenucalls.PvMenuCallsDTO(medication.id.medicationCode, medication.medicationDescription, medication.medicationUnit, medication.addedUser, medication.addedDate, medication.addedTime, medication.changedUser, medication.changedDate, medication.changedTime) from Medication medication";
		String countQueryString = "SELECT COUNT(medication.id.medicationCode) FROM Medication medication";

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<PvMenuCallsDTO> content = query.getResultList();
		RestResponsePage<PvMenuCallsDTO> pageDto = new RestResponsePage<PvMenuCallsDTO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<DspfilPrescribedMedicatGDO> dspfilPrescribedMedicat(
		String medicationCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(medicationCode)) {
			whereClause += " medication.id.medicationCode like CONCAT('%', :medicationCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.medication.dspfilprescribedmedicat.DspfilPrescribedMedicatGDO(medication.id.medicationCode, medication.medicationDescription, medication.medicationUnit, medication.addedUser, medication.addedDate, medication.addedTime, medication.changedUser, medication.changedDate, medication.changedTime) from Medication medication";
		String countQueryString = "SELECT COUNT(medication.id.medicationCode) FROM Medication medication";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Medication medication = new Medication();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(medication.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"medication.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"medication." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"medication.id.medicationCode"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(medicationCode)) {
			countQuery.setParameter("medicationCode", medicationCode);
			query.setParameter("medicationCode", medicationCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<DspfilPrescribedMedicatGDO> content = query.getResultList();
		RestResponsePage<DspfilPrescribedMedicatGDO> pageDto = new RestResponsePage<DspfilPrescribedMedicatGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.medication.MedicationService#rtvDetail(String)
	 */
	@Override
	public Medication rtvDetail(
		String medicationCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(medicationCode)) {
			whereClause += " medication.id.medicationCode = :medicationCode";
			isParamSet = true;
		}

		String sqlString = "SELECT medication FROM Medication medication";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(medicationCode)) {
			query.setParameter("medicationCode", medicationCode);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		Medication dto = (Medication)query.getSingleResult();

		return dto;
	}

}
