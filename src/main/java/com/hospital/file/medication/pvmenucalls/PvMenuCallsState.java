package com.hospital.file.medication.pvmenucalls;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.file.medication.Medication;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;
import com.hospital.model.ReturnCodeEnum;

/**
 * State for file 'Medication' (TSAJREP) and function 'PV Menu calls' (TSA2PVR).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class PvMenuCallsState extends PvMenuCallsDTO {
    private static final long serialVersionUID = 3815604178749571918L;

    // Local fields
    private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");
    private UsrReturnCodeEnum lclUsrReturnCode = null;

    // System fields
    private ReturnCodeEnum _sysReturnCode = ReturnCodeEnum._STA_NORMAL;
    private boolean _sysErrorFound = false;

    public PvMenuCallsState() {
    }

    public PvMenuCallsState(Medication medication) {
        setDtoFields(medication);
    }

    public void setLclReturnCode(ReturnCodeEnum returnCode) {
        this.lclReturnCode = returnCode;
    }

    public ReturnCodeEnum getLclReturnCode() {
        return lclReturnCode;
    }
    public void setLclUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
        this.lclUsrReturnCode = usrReturnCode;
    }

    public UsrReturnCodeEnum getLclUsrReturnCode() {
        return lclUsrReturnCode;
    }

   public void set_SysReturnCode(ReturnCodeEnum returnCode) {
       _sysReturnCode = returnCode;
   }

   public ReturnCodeEnum get_SysReturnCode() {
       return _sysReturnCode;
   }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
