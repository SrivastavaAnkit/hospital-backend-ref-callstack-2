package com.hospital.file.medication.pvmenucalls;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;


import org.springframework.beans.BeanUtils;

import com.hospital.common.state.BaseDTO;
import com.hospital.file.medication.Medication;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;


/**
 * Dto for file 'Medication' (TSAJREP) and function 'PV Menu calls' (TSA2PVR).
 */
public class PvMenuCallsDTO extends BaseDTO implements Serializable{
	private static final long serialVersionUID = 6298607724692587113L;

	private String medicationCode = "";
	private long optionSelected = 0L;
	private String medicationDescription = "";
	private MedicationUnitEnum medicationUnit = null;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;

	// Local fields
	private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");
	private UsrReturnCodeEnum lclUsrReturnCode = null;

	public PvMenuCallsDTO() {

	}

	public PvMenuCallsDTO(Medication medication) {
		BeanUtils.copyProperties(medication, this);
	}

	public PvMenuCallsDTO(String medicationCode, String medicationDescription, MedicationUnitEnum medicationUnit, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime) {
		this.medicationCode = medicationCode;
		this.medicationDescription = medicationDescription;
		this.medicationUnit = medicationUnit;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}

	public void setMedicationCode(String medicationCode) {
		this.medicationCode = medicationCode;
	}

	public String getMedicationCode() {
		return medicationCode;
	}

	public void setOptionSelected(long optionSelected) {
		this.optionSelected = optionSelected;
	}

	public long getOptionSelected() {
		return optionSelected;
	}

	public void setMedicationDescription(String medicationDescription) {
		this.medicationDescription = medicationDescription;
	}

	public String getMedicationDescription() {
		return medicationDescription;
	}

	public void setMedicationUnit(MedicationUnitEnum medicationUnit) {
		this.medicationUnit = medicationUnit;
	}

	public MedicationUnitEnum getMedicationUnit() {
		return medicationUnit;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void setLclReturnCode(ReturnCodeEnum returnCode) {
		this.lclReturnCode = returnCode;
	}

	public ReturnCodeEnum getLclReturnCode() {
		return lclReturnCode;
	}

	public void setLclUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
		this.lclUsrReturnCode = usrReturnCode;
	}

	public UsrReturnCodeEnum getLclUsrReturnCode() {
		return lclUsrReturnCode;
	}

    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param medication Medication Entity bean
     */
    public void setDtoFields(Medication medication) {
        BeanUtils.copyProperties(medication, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param medication Medication Entity bean
     */
    public void setEntityFields(Medication medication) {
        BeanUtils.copyProperties(this, medication);
    }
}
