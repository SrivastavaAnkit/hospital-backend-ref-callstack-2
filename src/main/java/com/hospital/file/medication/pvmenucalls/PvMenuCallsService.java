package com.hospital.file.medication.pvmenucalls;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;
import static com.hospital.common.callstack.StepResult.callService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.hospital.file.diagnosis.edtrcdeditdiagnosis.EdtrcdEditDiagnosisDTO;
import com.hospital.file.doctor.Doctor;
import com.hospital.file.doctor.dspdoctordetail.DspDoctorDetailParams;
import com.hospital.file.doctor.dspdoctordetail.DspDoctorDetailService;
import com.hospital.file.medication.Medication;
import com.hospital.file.patient.Patient;

import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;

import com.hospital.file.medication.MedicationRepository;

import com.hospital.file.doctor.editdoctor.EditDoctorService;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsService;
import com.hospital.file.hospital.eredithospital.ErEditHospitalService;
import com.hospital.file.hospital.pmtpromptdetaildisplay.PmtPromptDetailDisplayService;
import com.hospital.file.hospital.pmttestbatchfunction.PmtTestBatchFunctionService;

import com.hospital.file.patient.edreditpatientdetail.EdrEditPatientDetailService;
import com.hospital.file.patient.pmtdiagnosisandprescrip.PmtDiagnosisAndPrescripService;
import com.hospital.file.patient.prtpatientsperhospital.PrtPatientsPerHospitalService;
import com.hospital.file.doctor.editdoctor.EditDoctorDTO;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsDTO;
import com.hospital.file.hospital.eredithospital.ErEditHospitalDTO;
import com.hospital.file.hospital.pmtpromptdetaildisplay.PmtPromptDetailDisplayDTO;
import com.hospital.file.hospital.pmttestbatchfunction.PmtTestBatchFunctionDTO;

import com.hospital.file.patient.edreditpatientdetail.EdrEditPatientDetailDTO;
import com.hospital.file.patient.pmtdiagnosisandprescrip.PmtDiagnosisAndPrescripDTO;
import com.hospital.file.patient.prtpatientsperhospital.PrtPatientsPerHospitalDTO;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;
import com.hospital.support.JobContext;
import com.hospital.file.doctor.editdoctor.EditDoctorParams;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsParams;
import com.hospital.file.hospital.eredithospital.ErEditHospitalParams;
import com.hospital.file.hospital.pmtpromptdetaildisplay.PmtPromptDetailDisplayParams;
import com.hospital.file.hospital.pmttestbatchfunction.PmtTestBatchFunctionParams;
import com.hospital.file.medication.editmedication.EditMedicationDTO;
import com.hospital.file.medication.editmedication.EditMedicationParams;
import com.hospital.file.medication.editmedication.EditMedicationService;
import com.hospital.file.medication.editmedication.EditMedicationState;
import com.hospital.file.patient.edreditpatientdetail.EdrEditPatientDetailParams;
import com.hospital.file.patient.pmtdiagnosisandprescrip.PmtDiagnosisAndPrescripParams;

import com.hospital.model.CmdKeyEnum;
import com.hospital.model.DeferConfirmEnum;


/**
 * Service for file 'Medication' (TSAJREP) and function 'PV Menu calls' (TSA2PVR).
 */
@Service
public class PvMenuCallsService extends AbstractService<PvMenuCallsService, PvMenuCallsState> {
    
    @Autowired
    private JobContext job;

    @Autowired
    private MessageSource messageSource;

	@Autowired
	private MedicationRepository medicationRepository;

    
    public static final String SCREEN_KEY = "pvMenuCalls";
    public static final String SCREEN_CONFIRM_PROMPT = "PvMenuCalls.key2";

    private final Step execute = define("execute",PvMenuCallsParams.class, this::execute);
    private final Step response = define("response",PvMenuCallsDTO.class, this::processResponse);
//    private final Step displayConfirmPrompt = define("displayConfirmPrompt",PvMenuCallsDTO.class, this::processDisplayConfirmPrompt);

	
	private final Step serviceErEditHospital = define("serviceErEditHospital",ErEditHospitalParams.class, this::processServiceErEditHospital);
    private final Step serviceEdrEditPatientDetail = define("serviceEdrEditPatientDetail",EdrEditPatientDetailParams.class, this::processServiceEdrEditPatientDetail);
	private final Step serviceDspAllHospitals = define("serviceDspAllHospitals",DspAllHospitalsParams.class, this::processServiceDspAllHospitals);
	private final Step servicePmtDiagnosisAndPrescrip = define("servicePmtDiagnosisAndPrescrip",PmtDiagnosisAndPrescripParams.class, this::processServicePmtDiagnosisAndPrescrip);
	private final Step servicePmtPromptDetailDisplay = define("servicePmtPromptDetailDisplay",PmtPromptDetailDisplayParams.class, this::processServicePmtPromptDetailDisplay);
	//private final Step servicePmtTestBatchFunction = define("servicePmtTestBatchFunction",PmtTestBatchFunctionParams.class, this::processServicePmtTestBatchFunction);
	private final Step serviceEditMedication = define("serviceEditMedication",EditMedicationParams.class, this::processServiceEditMedication);
	private final Step serviceEditDoctor = define("serviceEditDoctor",EditDoctorParams.class, this::processServiceEditDoctor);
	//private final Step servicePrtPatientsPerHospital = define("servicePrtPatientsPerHospital",PrtPatientsPerHospitalParams.class, this::processServicePrtPatientsPerHospital);
	//private final Step servicePrintSubmitted = define("servicePrintSubmitted",PrintSubmittedParams.class, this::processServicePrintSubmitted);
	
    	
    @Autowired
    public PvMenuCallsService()
    {
        super(PvMenuCallsService.class, PvMenuCallsState.class);
    }

    @Override
    public Step getInitialStep()
    {
        return execute;
    }

    /**
     * PvMenuCalls service starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(PvMenuCallsState state, PvMenuCallsParams params)  {
        StepResult result = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        initialize(state);
        result = conductScreenConversation(state);

        return result;
    }

    /**
     * PvMenuCalls service initialization.
     * @param state   - Service state class.
     * @return
     */
    private void initialize(PvMenuCallsState state) {
        usrInitializeProgram(state);
        usrScreenFunctionFields(state);
        usrLoadScreen(state);
    }

    /**
     * SCREEN_KEY display processing loop method.
     * @param state   - Service state class.
     * @return
     */
    private StepResult conductScreenConversation(PvMenuCallsState state)
    {
        StepResult result = NO_ACTION;

        PvMenuCallsDTO dto = new PvMenuCallsDTO();
        BeanUtils.copyProperties(state, dto);
        result = callScreen(SCREEN_KEY, dto).thenCall(response);

        return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param dto - returned screen model.
     * @return
     */
    private StepResult processResponse(PvMenuCallsState state, PvMenuCallsDTO dto)
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(dto, state);
        if (CmdKeyEnum.isExit(state.get_SysCmdKey()))
        {
            result = closedown(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey()))
        {
            //TODO: processResetRequest(state);//synon built-in function
            result = conductScreenConversation(state);
        }
        else if (CmdKeyEnum.isHelp(state.get_SysCmdKey()))
        {
            //TODO:processHelpRequest(state);//synon built-in function
            result = conductScreenConversation(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
			
        }
        else
        {
            validateScreenInput(state);
            if (!state.get_SysErrorFound()){
                //BeanUtils.copyProperties(state, dto);
                //TODO: for now, confirm prompt is skipped
                result = usrUserDefinedAction(state);//callScreen(SCREEN_CONFIRM_PROMPT, dto).thenCall(displayConfirmPrompt);
                if(result != NO_ACTION) {
                    return result;
                }
            }
            result = conductScreenConversation(state);
        }

        return result;
    }

//    /**
//     * SCREEN_CONFIRM_PROMPT returned response processing method.
//     * @param state - Service state class.
//     * @param dto - returned screen model.
//     * @return
//     */
//    private StepResult processDisplayConfirmPrompt(PvMenuCallsState state, PvMenuCallsDTO dto) {
//        StepResult result = NO_ACTION;
//
//        BeanUtils.copyProperties(dto, state);
//        if (!state.getDeferConfirm().getCode().equals(DeferConfirmEnum._PROCEED_TO_CONFIRM.getCode()))
//        {
//            usrUserDefinedAction(state);
//        }
//        result = conductScreenConversation(state);
//
//        return result;
//    }

    /**
     * Validate input in SCREEN_KEY.
     * @param state - Service state class.
     * @return
     */
    private void validateScreenInput(PvMenuCallsState state)
    {
        usrProcessCommandKeys(state);
        checkFields(state);
        if (!state.get_SysErrorFound())
        {
            usrValidateFields(state);
            checkRelations(state);
            if (!state.get_SysErrorFound())
            {
                usrScreenFunctionFields(state);
                usrValidateRelations(state);
            }
        }
    }

    /**
     * Check fields set in SCREEN_KEY.
     * @param state - Service state class.
     * @return
     */
    private void checkFields(PvMenuCallsState state) {

    }

    /**
     * Check relations set in SCREEN_KEY.
     * @param state - Service state class.
     * @return
     */
    private void checkRelations(PvMenuCallsState state) {

    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(PvMenuCallsState state)
    {
        StepResult result = NO_ACTION;

        result = usrExitProgramProcessing(state);
        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);

        PvMenuCallsParams params = new PvMenuCallsParams();
        BeanUtils.copyProperties(state, params);
        result = StepResult.returnFromService(params);

        return result;
    }


    
/**
 * ---------------------- Programmatic user-point --------------------------
 */
	/**
	 * USER: Initialize Program (Generated:10)
	 */
    private StepResult usrInitializeProgram(PvMenuCallsState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 10 SUB    
			// Unprocessed SUB 10 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Screen Function Fields (Generated:1052)
	 */
    private StepResult usrScreenFunctionFields(PvMenuCallsState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1052 SUB    
			// Unprocessed SUB 1052 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Load Screen (Generated:1058)
	 */
    private StepResult usrLoadScreen(PvMenuCallsState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1058 SUB    
			// Unprocessed SUB 1058 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Generated:1035)
	 */
    private StepResult usrProcessCommandKeys(PvMenuCallsState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1035 SUB    
			// Unprocessed SUB 1035 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Fields (Generated:1065)
	 */
    private StepResult usrValidateFields(PvMenuCallsState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1065 SUB    
			// Unprocessed SUB 1065 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Validate Relations (Generated:29)
	 */
    private StepResult usrValidateRelations(PvMenuCallsState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 29 SUB    
			// Unprocessed SUB 29 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: User Defined Action (Generated:41)
	 */
    private StepResult usrUserDefinedAction(PvMenuCallsState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 41 SUB    
			//switchBLK 1000001 BLK CAS
			//switchSUB 1000001 BLK CAS
			if (dto.getOptionSelected() != 0) {
				// DTL.Option Selected is Not Zero
				//switchBLK 1000006 BLK CAS
				//switchSUB 1000006 BLK CAS
				if (dto.getOptionSelected() == 1) {
					// DTL.Option Selected is Option 1
					//switchBLK 1000009 BLK ACT
					//functionCall 1000010 ACT Edit Medication - Medication  *
					//TODO: split	
					EditMedicationParams editMedicationParams = new EditMedicationParams();
					BeanUtils.copyProperties(dto, editMedicationParams);
					result = callService(EditMedicationService.class, editMedicationParams).thenCall(serviceEditMedication);
					//switchBLK 1000052 BLK TXT
					// 
				} else //switchSUB 1000011 SUB    
				if (dto.getOptionSelected() == 2) {
					// DTL.Option Selected is Option 2
					//switchBLK 1000013 BLK ACT
					//functionCall 1000014 ACT ER Edit Hospital - Hospital  *
					//TODO: split
					ErEditHospitalParams erEditHospitalParams = new ErEditHospitalParams();
					BeanUtils.copyProperties(dto, erEditHospitalParams);
					result = StepResult.callService(ErEditHospitalService.class, erEditHospitalParams).thenCall(serviceErEditHospital);
					//switchBLK 1000053 BLK TXT
					// 
				} else //switchSUB 1000015 SUB    
				if (dto.getOptionSelected() == 3) {
					// DTL.Option Selected is Option 3
					//switchBLK 1000017 BLK ACT
					//functionCall 1000018 ACT Edit Doctor - Doctor  *
					//TODO: split
					EditDoctorParams editDoctorParams = new EditDoctorParams();
					BeanUtils.copyProperties(dto, editDoctorParams);
					result = StepResult.callService(EditDoctorService.class, editDoctorParams).thenCall(serviceEditDoctor);
					//switchBLK 1000054 BLK TXT
					// 
				} else //switchSUB 1000019 SUB    
				if (dto.getOptionSelected() == 4) {
					// DTL.Option Selected is Option 4
					//switchBLK 1000021 BLK ACT
					//functionCall 1000022 ACT EDR Edit Patient Detail - Patient  *
					//TODO: split
					EdrEditPatientDetailParams edrEditPatientDetailParams = new EdrEditPatientDetailParams();
					BeanUtils.copyProperties(dto, edrEditPatientDetailParams);
					EdrEditPatientDetailDTO edrEditPatientDetailDTO = new EdrEditPatientDetailDTO();					
					result = callService(EdrEditPatientDetailService.class, edrEditPatientDetailParams).thenCall(serviceEdrEditPatientDetail);
					// 
				} else //switchSUB 1000031 SUB    
				if (dto.getOptionSelected() == 5) {
					// DTL.Option Selected is Option 5
					//switchBLK 1000033 BLK ACT
					//functionCall 1000034 ACT PMT Diagnosis & Prescrip - Patient  *
					//TODO: split
					PmtDiagnosisAndPrescripParams pmtDiagnosisAndPrescripParams = new PmtDiagnosisAndPrescripParams();
					BeanUtils.copyProperties(dto, pmtDiagnosisAndPrescripParams);
					result = StepResult.callService(PmtDiagnosisAndPrescripService.class, pmtDiagnosisAndPrescripParams).thenCall(servicePmtDiagnosisAndPrescrip);
				} else //switchSUB 1000035 SUB    
				if (dto.getOptionSelected() == 6) {
					// DTL.Option Selected is Option 6
					//switchBLK 1000037 BLK ACT
					//functionCall 1000038 ACT PMT Prompt detail display - Hospital  *
					//TODO: split
					PmtPromptDetailDisplayParams pmtPromptDetailDisplayParams = new PmtPromptDetailDisplayParams();
					BeanUtils.copyProperties(dto, pmtPromptDetailDisplayParams);
					result = StepResult.callService(PmtPromptDetailDisplayService.class, pmtPromptDetailDisplayParams).thenCall(servicePmtPromptDetailDisplay);
				} else //switchSUB 1000023 SUB    
				if (dto.getOptionSelected() == 7) {
					// DTL.Option Selected is Option 7
					//switchBLK 1000025 BLK ACT
					//functionCall 1000026 ACT DSP All Hospitals - Hospital  *
					//TODO: split
					DspAllHospitalsParams dspAllHospitalsParams = new DspAllHospitalsParams();
					BeanUtils.copyProperties(dto, dspAllHospitalsParams);
					//TODO: corrected post call method name below
					result = StepResult.callService(DspAllHospitalsService.class, dspAllHospitalsParams).thenCall(serviceDspAllHospitals);
				} else //switchSUB 1000027 SUB    
				if (dto.getOptionSelected() == 8) {
					
					
					DspDoctorDetailParams dspDoctorDetailParams = new DspDoctorDetailParams();
					BeanUtils.copyProperties(dto, dspDoctorDetailParams);
					result = StepResult.callService(DspDoctorDetailService.class, dspDoctorDetailParams);
					
					// DTL.Option Selected is Option 8
					//switchBLK 1000039 BLK ACT
					//functionCall 1000040 ACT Send information message - 'Print Submitted'
					//e.rejectValue("", "print.submitted");
					//switchBLK 1000029 BLK ACT
					//functionCall 1000030 ACT SBMJOB: PRT Patients per Hospital - Patient  * Job submitted
					// TODO: Unsupported Function Type 'Print File' (message surrogate = 1101202)
				} else //switchSUB 1000041 SUB    
				if (dto.getOptionSelected() == 9) {
					// DTL.Option Selected is Option 9
					//switchBLK 1000050 BLK ACT
					//functionCall 1000051 ACT PMT Test Batch Function - Hospital  *
					//TODO: split
					PmtTestBatchFunctionParams pmtTestBatchFunctionParams = new PmtTestBatchFunctionParams();
					BeanUtils.copyProperties(dto, pmtTestBatchFunctionParams);
					//result = StepResult.callService(PmtTestBatchFunctionService.class, pmtTestBatchFunctionParams);//.thenCall(servicePmtTestBatchFunctionDisplay)
				}
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Generated:1021)
	 */
    private StepResult usrExitProgramProcessing(PvMenuCallsState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 1021 SUB    
			// Unprocessed SUB 1021 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */

    //TODO: Added below method
    /**
     * ErEditHospitalService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceEditMedication(PvMenuCallsState state, EditMedicationParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }
        //TODO: Added below line of code to move on menu page

        //TODO: call the continuation of the program
        result = callScreen(SCREEN_KEY, state).thenCall(response);

        return result;
    }

    /**
     * ErEditHospitalService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceErEditHospital(PvMenuCallsState state, ErEditHospitalParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }
        //TODO: Added below code to move on menu
        result = callScreen(SCREEN_KEY, state).thenCall(response);

        return result;
    }

    /**
     * EdrEditPatientDetailService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceEdrEditPatientDetail(PvMenuCallsState state, EdrEditPatientDetailParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        result = callScreen(SCREEN_KEY, state).thenCall(response);

        return result;
    }

    /**
     * DspAllHospitalsService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspAllHospitals(PvMenuCallsState state, DspAllHospitalsParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        result = callScreen(SCREEN_KEY, state).thenCall(response);

        return result;
    }

    /**
     * PmtDiagnosisAndPrescripService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServicePmtDiagnosisAndPrescrip(PvMenuCallsState state, PmtDiagnosisAndPrescripParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        result = callScreen(SCREEN_KEY, state).thenCall(response);

        return result;
    }

    /**
     * PmtPromptDetailDisplayService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServicePmtPromptDetailDisplay(PvMenuCallsState state, PmtPromptDetailDisplayParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        result = callScreen(SCREEN_KEY, state).thenCall(response);

        return result;
    }


//
//    /**
//     * EditMedicationService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceEditMedication(PvMenuCallsState state, EditMedicationParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//  //TODO: uncommented below block
    /**
     * EditDoctorService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceEditDoctor(PvMenuCallsState state, EditDoctorParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        result = callScreen(SCREEN_KEY, state).thenCall(response);

        return result;
    }
////
//    /**
//     * PrtPatientsPerHospitalService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServicePrtPatientsPerHospital(PvMenuCallsState state, PrtPatientsPerHospitalParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * PrintSubmittedService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServicePrintSubmitted(PvMenuCallsState state, PrintSubmittedParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//


}
