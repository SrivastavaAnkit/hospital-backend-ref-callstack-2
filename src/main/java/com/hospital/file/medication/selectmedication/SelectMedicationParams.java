package com.hospital.file.medication.selectmedication;

import java.io.Serializable;


/**
 * Params for resource: SelectMedication (TSAVSRR).
 *
 * @author X2EGenerator
 */
public class SelectMedicationParams implements Serializable {
    private static final long serialVersionUID = 1248653303284515746L;

	private String medicationCode = "";

	public String getMedicationCode() {
		return medicationCode;
	}
	
	public void setMedicationCode(String medicationCode) {
		this.medicationCode = medicationCode;
	}
}

