package com.hospital.file.medication.selectmedication;

import com.hospital.file.medication.Medication;
import com.hospital.model.ReloadSubfileEnum;

/**
 * State for file 'Medication' (TSAJREP) and function 'Select Medication' (TSAVSRR).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectMedicationState extends SelectMedicationDTO {
    private static final long serialVersionUID = -6549886844808642035L;


    // System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    public SelectMedicationState() {
    }

    public SelectMedicationState(Medication medication) {
        setDtoFields(medication);
    }

   public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
       _sysReloadSubfile = reloadSubfile;
   }

   public ReloadSubfileEnum get_SysReloadSubfile() {
       return _sysReloadSubfile;
   }

}
