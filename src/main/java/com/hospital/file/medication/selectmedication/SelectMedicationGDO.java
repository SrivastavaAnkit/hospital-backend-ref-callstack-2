package com.hospital.file.medication.selectmedication;

import java.io.Serializable;

import java.time.LocalDate;
import java.time.LocalTime;

import com.hospital.model.MedicationUnitEnum;


/**
 * Gdo for file 'Medication' (TSAJREP) and function 'Select Medication' (TSAVSRR).
 */
public class SelectMedicationGDO implements Serializable {
	private static final long serialVersionUID = 8325598377082570681L;

	private long version = 0;
	private String _sysSelected = "";
	private String medicationCode = "";
	private String medicationDescription = "";
	private MedicationUnitEnum medicationUnit = null;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;

	public SelectMedicationGDO() {

	}

	public SelectMedicationGDO(long version, String medicationCode, String medicationDescription, MedicationUnitEnum medicationUnit, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime) {
		this.version = version;
		this.medicationCode = medicationCode;
		this.medicationDescription = medicationDescription;
		this.medicationUnit = medicationUnit;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public long getVersion() {
		return version;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setMedicationCode(String medicationCode) {
		this.medicationCode = medicationCode;
	}

	public String getMedicationCode() {
		return medicationCode;
	}

	public void setMedicationDescription(String medicationDescription) {
		this.medicationDescription = medicationDescription;
	}

	public String getMedicationDescription() {
		return medicationDescription;
	}

	public void setMedicationUnit(MedicationUnitEnum medicationUnit) {
		this.medicationUnit = medicationUnit;
	}

	public MedicationUnitEnum getMedicationUnit() {
		return medicationUnit;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

}
