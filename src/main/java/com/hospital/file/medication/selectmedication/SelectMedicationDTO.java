package com.hospital.file.medication.selectmedication;

import org.springframework.beans.BeanUtils;

import com.hospital.common.state.SelectRecordDTO;
import com.hospital.file.medication.Medication;

/**
 * Dto for file 'Medication' (TSAJREP) and function 'Select Medication'
 * (TSAVSRR).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectMedicationDTO extends SelectRecordDTO<SelectMedicationGDO> {
	private static final long serialVersionUID = 5968790355512544064L;

	private long version = 0;
	private int page = 0;
	private int size = 10;
	private String sortData = "";
	private boolean confirm = false;
	private String medicationCode = "";
	private SelectMedicationGDO gdo;

	public SelectMedicationDTO() {
	}

	public SelectMedicationDTO(long version) {
		this.version = version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public long getVersion() {
		return version;
	}

	public void setMedicationCode(String medicationCode) {
		this.medicationCode = medicationCode;
	}

	public String getMedicationCode() {
		return medicationCode;
	}

	public SelectMedicationGDO getGdo() {
		return gdo;
	}

	public void setGdo(SelectMedicationGDO gdo) {
		this.gdo = gdo;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getSortData() {
		return sortData;
	}

	public void setSortData(String sortData) {
		this.sortData = sortData;
	}

	public boolean isConfirm() {
		return confirm;
	}

	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}

	/**
	 * \ Copies the fields of the Entity bean into the DTO bean.
	 *
	 * @param medication
	 *            Medication Entity bean
	 */
	public void setDtoFields(Medication medication) {
		this.version = medication.getVersion();
		BeanUtils.copyProperties(medication, this);
	}

	/**
	 * Copies the fields of the DTO bean into the Entity bean.
	 *
	 * @param medication
	 *            Medication Entity bean
	 */
	public void setEntityFields(Medication medication) {
		medication.setVersion(this.version);
		BeanUtils.copyProperties(this, medication);
	}
}
