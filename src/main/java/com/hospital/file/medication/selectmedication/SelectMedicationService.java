package com.hospital.file.medication.selectmedication;
    


import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.file.medication.MedicationRepository;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.RecordSelectedEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.support.JobContext;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

/**
 * SELRCD Service controller for 'Select Medication' (TSAVSRR) of file 'Medication' (TSAJREP)
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
@Service
public class SelectMedicationService extends AbstractService<SelectMedicationService, SelectMedicationState> {
    
	@Autowired
	private JobContext job;

	@Autowired
	private MedicationRepository medicationRepository;
        

    
    public static final String SCREEN_KEY = "selectMedication";

	private final Step execute = define("execute", SelectMedicationParams.class, this::execute);
	private final Step response = define("response", SelectMedicationDTO.class, this::processResponse);
	
    

	@Autowired
	public SelectMedicationService()
	{
		super(SelectMedicationService.class, SelectMedicationState.class);
	}

	@Override
	public Step getInitialStep()
	{
		return execute;
	}

    /**
     * SelectMedication service starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
	private StepResult execute(SelectMedicationState state, SelectMedicationParams params)
	{
        StepResult result = NO_ACTION;

    	    BeanUtils.copyProperties(params, state);
    
		usrInitializeProgram(state);
		result =  mainLoop(state);

        return result;
	}

    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
	private StepResult mainLoop(SelectMedicationState state)
	{
        StepResult result = NO_ACTION;

		dbfReadFirstDataRecord(state);
		if (state.getPageDto() != null && state.getPageDto().getSize() > 0)
		{
			loadNextSubfilePage(state);
		}
		state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
		result = conductScreenConversation(state);

        return result;
	}

    /**
     * SCREEN_KEY display processing loop method.
     * @param state - Service state class.
     * @return
     */
	private StepResult conductScreenConversation(SelectMedicationState state)
	{
		StepResult result = NO_ACTION;

		if(state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)){
            SelectMedicationDTO dto = new SelectMedicationDTO();
            BeanUtils.copyProperties(state, dto);
			result = callScreen(SCREEN_KEY, dto).thenCall(response);
		}

		return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param dto - returned screen model.
     * @return
     */
	private StepResult processResponse(SelectMedicationState state, SelectMedicationDTO dto)
	{
        StepResult result = NO_ACTION;
        SelectMedicationGDO gdo = new SelectMedicationGDO();

        BeanUtils.copyProperties(dto, state);
		if(state.getGdo()!=null)
		{
			BeanUtils.copyProperties(state.getGdo(), dto);
			return StepResult.returnFromService(dto);
		}
		if(CmdKeyEnum.isExit(state.get_SysCmdKey()))
		{
			result = closedown(state);
            return result;
		}
		else if(CmdKeyEnum.isReset(state.get_SysCmdKey())) {
			//TODO: processResetRequest(state);//synon built-in function
		}
		else if(CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
			//TODO:processHelpRequest(state);//synon built-in function
		}
		else if(CmdKeyEnum.isNextPage(state.get_SysCmdKey())) {
			dbfReadNextDataRecord(state);
			loadNextSubfilePage(state);
		}
		else {
			usrProcessSubfileControl(state);
			//TODO:readFirstChangedSubfileRecord(state);//synon built-in function
			while (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {//TODO:while(Changed subfile record found)
                
                    if(state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode()))//TODO:if(subfile select (Record Context) is equal to *Select request)
                    {
                        usrProcessSelectedLine(state, gdo);
                        //PAR.*ALL = RCD.*ALL // Move all fields of the selected record on the screen to the parameter
                        //*EXIT PROGRAM
                        return result;
                    }
                    usrProcessChangedSubfileRecord(state, gdo);
                    usrScreenFunctionFields(state, gdo);
                    //TODO:updateSubfileRecord(state, gdo);//synon built-in function
                    //TODO:readNextChangedSubfileRecord(state);//synon built-in function
              
			}
			if(!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) //TODO:if(positioning field values have changed)
			{
				state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
			}
			usrProcessCommandKeys(state);
        }
        result = conductScreenConversation(state);

        return result;
	}

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     * @return
     */
	private void loadNextSubfilePage(SelectMedicationState state)
	{
		for (SelectMedicationGDO gdo : ((Page<SelectMedicationGDO>) state.getPageDto()).getContent())
		{
            state.setRecordSelect(RecordSelectedEnum._STA_YES);
			//TODO:moveDbfRecordFieldsToSubfileRecord(state);//synon built-in function
			usrScreenFunctionFields(state,gdo);
			usrLoadSubfileRecordFromDbfRecord(state, gdo);
            if(state.getRecordSelect().getCode().equals(RecordSelectedEnum._STA_YES.getCode()))
            {
                //TODO:writeSubfileRecord(state);//synon built-in function
            }
		}
	}

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
	private StepResult closedown(SelectMedicationState state)
	{
        StepResult result = NO_ACTION;

		usrExitProgramProcessing(state);
		//exitProgram

		return result;
	}
    
    /**
     * ------------------------- Generated DBF method ---------------------------
     */

    /**
     * Read data of the first page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadFirstDataRecord(SelectMedicationState state)
	{
		state.setPage(0);
		dbfReadDataRecord(state);
	}

    /**
     * Read data of the next page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadNextDataRecord(SelectMedicationState state)
	{
		//state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    /**
     * Read data of the actual page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadDataRecord(SelectMedicationState state)
	{
		List<Order> sortOrders = new ArrayList<Order>();
		Pageable pageable;

		try
		{
			@SuppressWarnings("unchecked")
			Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

			for (Map.Entry<String, String> entry : sortDataMap.entrySet())
			{
 				if (entry.getValue() == null)
				{
  					continue;
				}

				sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
			}
		}
		catch (IOException ioe)
		{
		}

		if (CollectionUtils.isEmpty(sortOrders))
		{
			pageable = new PageRequest(state.getPage(), state.getSize());
		}
		else
		{
			pageable = new PageRequest(state.getPage(), state.getSize(), new Sort(sortOrders));
		}

		RestResponsePage<SelectMedicationGDO> pageDto = medicationRepository.selectMedication(state.getMedicationCode(), pageable);
		state.setPageDto(pageDto);
	}
    
    /**
     * ---------------------- Programmatic user-point --------------------------
     */

	/**
	 * USER: Initialize Program (Generated:20)
	 */
	private StepResult usrInitializeProgram(SelectMedicationState dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 20 SUB    
			// Unprocessed SUB 20 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Process Subfile Control (Generated:72)
	 */
	private StepResult usrProcessSubfileControl(SelectMedicationState dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 72 SUB    
			// Unprocessed SUB 72 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Process Selected Line (Generated:107)
	 */
	private StepResult usrProcessSelectedLine(SelectMedicationState dto, SelectMedicationGDO gdo)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 107 SUB    
			// Unprocessed SUB 107 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Process Changed Subfile Record (Generated:101)
	 */
	private StepResult usrProcessChangedSubfileRecord(SelectMedicationState dto, SelectMedicationGDO gdo)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 101 SUB    
			// Unprocessed SUB 101 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * CALC: Screen Function Fields (Generated:165)
	 */
	private StepResult usrScreenFunctionFields(SelectMedicationState dto, SelectMedicationGDO gdo)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 165 SUB    
			// Unprocessed SUB 165 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Process Command Keys (Generated:143)
	 */
	private StepResult usrProcessCommandKeys(SelectMedicationState dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 143 SUB    
			// Unprocessed SUB 143 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Load Subfile Record from DBF Record (Generated:41)
	 */
	private StepResult usrLoadSubfileRecordFromDbfRecord(SelectMedicationState dto, SelectMedicationGDO gdo)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 41 SUB    
			// Unprocessed SUB 41 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
	private StepResult usrExitProgramProcessing(SelectMedicationState dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    //switchSUB 132 SUB    
			// Unprocessed SUB 132 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//


}
