package com.hospital.file.medication.createmedication;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.medication.Medication;
import com.hospital.file.medication.MedicationId;
import com.hospital.file.medication.MedicationRepository;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class CreateMedicationService  extends AbstractService<CreateMedicationService, CreateMedicationDTO>
{
    private final Step execute = define("execute", CreateMedicationDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private MedicationRepository medicationRepository;
	

    @Autowired
    public CreateMedicationService() {
        super(CreateMedicationService.class, CreateMedicationDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(CreateMedicationDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(CreateMedicationDTO dto, CreateMedicationDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		Medication medication = new Medication();
		medication.setMedicationCode(dto.getMedicationCode());
		medication.setMedicationDescription(dto.getMedicationDescription());
		medication.setMedicationUnit(dto.getMedicationUnit());
		/*medication.setAddedUser(dto.getAddedUser());
		medication.setAddedDate(dto.getAddedDate());
		medication.setAddedTime(dto.getAddedTime());
		medication.setChangedUser(dto.getChangedUser());
		medication.setChangedDate(dto.getChangedDate());
		medication.setChangedTime(dto.getChangedTime());*/
		processingBeforeDataUpdate(dto, medication);
		// TODO: COmmented below lines bcoz we don't need find by Id queries
		/*Medication medication2 = medicationRepository.findById(medication.getId()).get();
		if (medication2 != null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0003"));
			processingIfDataRecordAlreadyExists(dto, medication2);
		}
		else {*/
            try {
				medicationRepository.save(medication);
                dto.setReturnCode(ReturnCodeEnum.fromCode(""));
                processingAfterDataUpdate(dto, medication);
            } catch (Exception e) {
                dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
                processingIfDataUpdateError(dto, medication);
            }
        //}

        return result;
	}

    private StepResult processingBeforeDataUpdate(CreateMedicationDTO dto, Medication medication) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT DB1.Added User = JOB.*USER
		/*medication.setAddedUser(job.getUser());
		//switchBLK 1000005 BLK ACT
		//functionCall 1000006 ACT DB1.Added Date = JOB.*Job date
		medication.setAddedDate(LocalDate.now());
		//switchBLK 1000009 BLK ACT
		//functionCall 1000010 ACT DB1.Added Time = JOB.*Job time
		medication.setAddedTime(LocalTime.now());*/
       return NO_ACTION;
    }

    private StepResult processingIfDataRecordAlreadyExists(CreateMedicationDTO dto, Medication medication) throws ServiceException {
		/**
		 * USER: Processing if Data Record Already Exists (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000013 BLK ACT
		//functionCall 1000014 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
    }

    private StepResult processingAfterDataUpdate(CreateMedicationDTO dto, Medication medication) throws ServiceException {
		/**
		 * USER: Processing after Data Update (Generated:11)
		 */
		//switchSUB 11 SUB    
		//switchBLK 1000025 BLK ACT
		//functionCall 1000026 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
    }

    private StepResult processingIfDataUpdateError(CreateMedicationDTO dto, Medication medication) throws ServiceException {
		/**
		 * Processing if Data Update Error (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000019 BLK ACT
		//functionCall 1000020 ACT PAR.USR Return Code = CND.Error
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("E"));
       return NO_ACTION;
    }
}
