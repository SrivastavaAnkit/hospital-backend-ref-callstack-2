package com.hospital.file.diagnosis.changediagnosis;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hospital.support.JobContext;
import com.hospital.file.diagnosis.Diagnosis;
import com.hospital.file.diagnosis.DiagnosisId;
import com.hospital.file.diagnosis.DiagnosisRepository;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.exception.ServiceException;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class ChangeDiagnosisService extends com.hospital.common.callstack.AbstractService<ChangeDiagnosisService, ChangeDiagnosisDTO>
{
    private final Step execute = define("execute", ChangeDiagnosisDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private DiagnosisRepository diagnosisRepository;
	
	@Autowired
	private CreateLogFileService createLogFileService;
	

    @Autowired
    public ChangeDiagnosisService() {
        super(ChangeDiagnosisService.class, ChangeDiagnosisDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(ChangeDiagnosisDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(ChangeDiagnosisDTO dto, ChangeDiagnosisDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);
		DiagnosisId diagnosisId = new DiagnosisId();
		diagnosisId.setPatientCode(dto.getPatientCode());
		diagnosisId.setDiagnosisDate(dto.getDiagnosisDate());
		diagnosisId.setDiagnosisTime(dto.getDiagnosisTime());
		Diagnosis diagnosis = diagnosisRepository.findById(diagnosisId).get();
		if (diagnosis == null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0005"));
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, diagnosis);
			diagnosis.setPatientCode(dto.getPatientCode());
			diagnosis.setDiagnosisDate(dto.getDiagnosisDate());
			diagnosis.setDiagnosisTime(dto.getDiagnosisTime());
			diagnosis.setDoctorCode(dto.getDoctorCode());
			diagnosis.setFindings1(dto.getFindings1());
			diagnosis.setFindings2(dto.getFindings2());
/*			diagnosis.setAddedUser(dto.getAddedUser());
			diagnosis.setAddedDate(dto.getAddedDate());
			diagnosis.setAddedTime(dto.getAddedTime());
			diagnosis.setChangedUser(dto.getChangedUser());
			diagnosis.setChangedDate(dto.getChangedDate());
			diagnosis.setChangedTime(dto.getChangedTime());*/
			processingBeforeDataUpdate(dto, diagnosis);
			try {
				diagnosisRepository.saveAndFlush(diagnosis);
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
			}
		}

       return result;
	}

	private StepResult processingBeforeDataRead(ChangeDiagnosisDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 8 SUB    
		//switchBLK 1000020 BLK CAS
		//switchSUB 1000020 BLK CAS
		if (dto.getPatientCode().equals("CHILD")) {
			// PAR.Patient Code is Children
			//switchBLK 1000023 BLK ACT
			//functionCall 1000024 ACT LCL.Findings 1 = CON.Young children
			dto.setLclFindings1("Young children");
		}
		//switchBLK 1000067 BLK TXT
		// 
		//switchBLK 1000068 BLK ACT
		//functionCall 1000069 ACT LCL.Log Function Type = CON.CHGOBJ
		dto.setLclLogFunctionType("CHGOBJ");
		//switchBLK 1000074 BLK ACT
		//functionCall 1000075 ACT LCL.Log User Point = CON.Processing before Data
		dto.setLclLogUserPoint("Processing before Data");
		//switchBLK 1000082 BLK ACT
		//functionCall 1000083 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.read,CND.*One)
		dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "read"));
		//switchBLK 1000092 BLK ACT
		//functionCall 1000093 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
       return NO_ACTION;
	}

	private StepResult processingIfDataRecordNotFound(ChangeDiagnosisDTO dto) throws ServiceException {
		/**
		 * USER: Processing if Data Record Not Found (Generated:43)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 43 SUB    
		//switchBLK 1000039 BLK ACT
		//functionCall 1000040 ACT LCL.USR Return Code = CND.Record Not found
		dto.setLclUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
		//switchBLK 1000116 BLK TXT
		// 
		//switchBLK 1000117 BLK ACT
		//functionCall 1000118 ACT LCL.Log Function Type = CON.CHGOBJ
		dto.setLclLogFunctionType("CHGOBJ");
		//switchBLK 1000121 BLK ACT
		//functionCall 1000122 ACT LCL.Log User Point = CON.Processing if Data record
		dto.setLclLogUserPoint("Processing if Data record");
		//switchBLK 1000125 BLK ACT
		//functionCall 1000126 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.not found,CND.*One)
		dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "not found"));
		//switchBLK 1000131 BLK ACT
		//functionCall 1000132 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
       return NO_ACTION;
	}

	private StepResult processingAfterDataRead(ChangeDiagnosisDTO dto, Diagnosis diagnosis) throws ServiceException {
		/**
		 * USER: Processing After Data Read (Generated:48)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 48 SUB    
		//switchBLK 1000029 BLK CAS
		//switchSUB 1000029 BLK CAS
		if (dto.getLclFindings1().equals(dto.getFindings1())) {
			// LCL.Findings 1 EQ PAR.Findings 1
			//switchBLK 1000033 BLK ACT
			//functionCall 1000034 ACT LCL.Findings 2 = CND.Young Children
			dto.setLclFindings2("YOUNG CHILDREN");
		}
		//switchBLK 1000142 BLK TXT
		// 
		//switchBLK 1000143 BLK ACT
		//functionCall 1000144 ACT LCL.Log Function Type = CON.CHGOBJ
		dto.setLclLogFunctionType("CHGOBJ");
		//switchBLK 1000147 BLK ACT
		//functionCall 1000148 ACT LCL.Log User Point = CON.Processing after Data
		dto.setLclLogUserPoint("Processing after Data");
		//switchBLK 1000151 BLK ACT
		//functionCall 1000152 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.Read,CND.*One)
		dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "Read"));
		//switchBLK 1000157 BLK ACT
		//functionCall 1000158 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
       return NO_ACTION;
	}

	private StepResult processingBeforeDataUpdate(ChangeDiagnosisDTO dto, Diagnosis diagnosis) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Generated:10)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 10 SUB    
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT DB1.Added User = JOB.*USER
		/*diagnosis.setAddedUser(job.getUser());
		//switchBLK 1000014 BLK ACT
		//functionCall 1000015 ACT DB1.Added Date = JOB.*Job date
		diagnosis.setAddedDate(LocalDate.now());
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT DB1.Added Time = JOB.*Job time
		diagnosis.setAddedTime(LocalTime.now());*/
		//switchBLK 1000187 BLK TXT
		// 
		//switchBLK 1000168 BLK ACT
		//functionCall 1000169 ACT LCL.Log Function Type = CON.CHGOBJ
		dto.setLclLogFunctionType("CHGOBJ");
		//switchBLK 1000172 BLK ACT
		//functionCall 1000173 ACT LCL.Log User Point = CON.Processing before Data
		dto.setLclLogUserPoint("Processing before Data");
		//switchBLK 1000176 BLK ACT
		//functionCall 1000177 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.update,CND.*One)
		dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "update"));
		//switchBLK 1000182 BLK ACT
		//functionCall 1000183 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(ChangeDiagnosisDTO dto) throws ServiceException {
		/**
		 * USER: Processing After Data Update (Generated:23)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 23 SUB    
		//switchBLK 1000061 BLK ACT
		//functionCall 1000062 ACT LCL.USR Return Code = CND.Record Not found
		dto.setLclUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
		//switchBLK 1000196 BLK TXT
		// 
		//switchBLK 1000197 BLK ACT
		//functionCall 1000198 ACT LCL.Log Function Type = CON.CHGOBJ
		dto.setLclLogFunctionType("CHGOBJ");
		//switchBLK 1000201 BLK ACT
		//functionCall 1000202 ACT LCL.Log User Point = CON.Processing after Data
		dto.setLclLogUserPoint("Processing after Data");
		//switchBLK 1000205 BLK ACT
		//functionCall 1000206 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.update,CND.*One)
		dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "update"));
		//switchBLK 1000211 BLK ACT
		//functionCall 1000212 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
       return NO_ACTION;
	}
}
