package com.hospital.file.diagnosis.rtvdiagnosisforpatient;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.diagnosis.Diagnosis;
import com.hospital.file.diagnosis.DiagnosisId;
import com.hospital.file.diagnosis.DiagnosisRepository;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.prescription.rtvprescriptiondetail.RtvPrescriptionDetailService;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.file.prescription.rtvprescriptiondetail.RtvPrescriptionDetailDTO;
import com.hospital.model.PrescriptionStatusEnum;
import com.hospital.model.ReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvDiagnosisForPatientService extends AbstractService<RtvDiagnosisForPatientService, RtvDiagnosisForPatientDTO>
{
    private final Step execute = define("execute", RtvDiagnosisForPatientDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private DiagnosisRepository diagnosisRepository;
	
	@Autowired
	private CreateLogFileService createLogFileService;
	
	@Autowired
	private RtvPrescriptionDetailService rtvPrescriptionDetailService;
	
    @Autowired
    public RtvDiagnosisForPatientService()
    {
        super(RtvDiagnosisForPatientService.class, RtvDiagnosisForPatientDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvDiagnosisForPatientDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvDiagnosisForPatientDTO dto, RtvDiagnosisForPatientDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Diagnosis> diagnosisList = diagnosisRepository.findAllByIdPatientCode(dto.getPatientCode());
		if (!diagnosisList.isEmpty()) {
			for (Diagnosis diagnosis : diagnosisList) {
				processDataRecord(dto, diagnosis);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvDiagnosisForPatientDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 48 SUB    
		//switchBLK 1000020 BLK ACT
		//functionCall 1000021 ACT PAR.Prescription Cost = CON.*ZERO
		dto.setPrescriptionCost(BigDecimal.ZERO);
		//switchBLK 1000034 BLK TXT
		// 
		//switchBLK 1000035 BLK ACT
		//functionCall 1000036 ACT LCL.Log Function Type = CON.RTVOBJ
		dto.setLclLogFunctionType("RTVOBJ");
		//switchBLK 1000039 BLK ACT
		//functionCall 1000040 ACT LCL.Log User Point = CON.Initialize routine
		dto.setLclLogUserPoint("Initialize routine");
		//switchBLK 1000049 BLK ACT
		//functionCall 1000050 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvDiagnosisForPatientDTO dto, Diagnosis diagnosis) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		CreateLogFileDTO createLogFileDTO;
		RtvPrescriptionDetailDTO rtvPrescriptionDetailDTO;
		//switchSUB 41 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT RTV Prescription detail - Prescription  *
		rtvPrescriptionDetailDTO = new RtvPrescriptionDetailDTO();
		rtvPrescriptionDetailDTO.setPatientCode(diagnosis.getPatientCode());
		rtvPrescriptionDetailDTO.setHospitalCode(dto.getHospitalCode());
		rtvPrescriptionDetailDTO.setWardCode(dto.getWardCode());
		rtvPrescriptionDetailDTO.setDoctorCode(diagnosis.getDoctorCode());
		rtvPrescriptionDetailService.execute(rtvPrescriptionDetailDTO);
		dto.setPrescriptionCost(rtvPrescriptionDetailDTO.getPrescriptionCost());
		dto.setPrescriptionStatus(rtvPrescriptionDetailDTO.getPrescriptionStatus());
		dto.setTotalAmount(rtvPrescriptionDetailDTO.getTotalAmount());
		//switchBLK 1000104 BLK TXT
		// 
		//switchBLK 1000105 BLK ACT
		//functionCall 1000106 ACT LCL.Log Function Type = CON.RTVOBJ
		dto.setLclLogFunctionType("RTVOBJ");
		//switchBLK 1000109 BLK ACT
		//functionCall 1000110 ACT LCL.Log User Point = CON.Process Data record
		dto.setLclLogUserPoint("Process Data record");
		//switchBLK 1000119 BLK ACT
		//functionCall 1000120 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvDiagnosisForPatientDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 52 SUB    
		//switchBLK 1000026 BLK ACT
		//functionCall 1000027 ACT PAR.Prescription Cost = CON.*ZERO
		dto.setPrescriptionCost(BigDecimal.ZERO);
		//switchBLK 1000078 BLK TXT
		// 
		//switchBLK 1000079 BLK ACT
		//functionCall 1000080 ACT LCL.Log Function Type = CON.RTVOBJ
		dto.setLclLogFunctionType("RTVOBJ");
		//switchBLK 1000083 BLK ACT
		//functionCall 1000084 ACT LCL.Log User Point = CON.Processing if Data record
		dto.setLclLogUserPoint("Processing if Data record");
		//switchBLK 1000087 BLK ACT
		//functionCall 1000088 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.not found,CND.*One)
		dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "not found"));
		//switchBLK 1000093 BLK ACT
		//functionCall 1000094 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvDiagnosisForPatientDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 61 SUB    
		//switchBLK 1000030 BLK ACT
		//functionCall 1000031 ACT PAR.Prescription Cost = CON.*ZERO
		dto.setPrescriptionCost(BigDecimal.ZERO);
		//switchBLK 1000126 BLK TXT
		// 
		//switchBLK 1000127 BLK ACT
		//functionCall 1000128 ACT LCL.Log Function Type = CON.RTVOBJ
		dto.setLclLogFunctionType("RTVOBJ");
		//switchBLK 1000131 BLK ACT
		//functionCall 1000132 ACT LCL.Log User Point = CON.Exit processing
		dto.setLclLogUserPoint("Exit processing");
		//switchBLK 1000141 BLK ACT
		//functionCall 1000142 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
        return NO_ACTION;
    }
}
