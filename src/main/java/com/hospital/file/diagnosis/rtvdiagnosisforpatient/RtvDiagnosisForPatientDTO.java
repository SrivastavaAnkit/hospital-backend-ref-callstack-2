package com.hospital.file.diagnosis.rtvdiagnosisforpatient;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.PrescriptionStatusEnum;
import com.hospital.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvDiagnosisForPatientDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private BigDecimal prescriptionCost;
	private BigDecimal totalAmount;
	private PrescriptionStatusEnum prescriptionStatus;
	private ReturnCodeEnum returnCode;
	private String hospitalCode;
	private String patientCode;
	private String wardCode;
	private String nextScreen;
	private String lclLogFunctionType;
	private String lclLogUserPoint;

	public String getHospitalCode() {
		return hospitalCode;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public BigDecimal getPrescriptionCost() {
		return prescriptionCost;
	}

	public PrescriptionStatusEnum getPrescriptionStatus() {
		return prescriptionStatus;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public String getWardCode() {
		return wardCode;
	}

	public String getLclLogFunctionType() {
		return lclLogFunctionType;
	}

	public String getLclLogUserPoint() {
		return lclLogUserPoint;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public void setPrescriptionCost(BigDecimal prescriptionCost) {
		this.prescriptionCost = prescriptionCost;
	}

	public void setPrescriptionStatus(PrescriptionStatusEnum prescriptionStatus) {
		this.prescriptionStatus = prescriptionStatus;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public void setLclLogFunctionType(String lclLogFunctionType) {
		this.lclLogFunctionType = lclLogFunctionType;
	}

	public void setLclLogUserPoint(String lclLogUserPoint) {
		this.lclLogUserPoint = lclLogUserPoint;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
