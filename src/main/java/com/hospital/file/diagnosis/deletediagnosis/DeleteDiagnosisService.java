package com.hospital.file.diagnosis.deletediagnosis;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.diagnosis.Diagnosis;
import com.hospital.file.diagnosis.DiagnosisId;
import com.hospital.file.diagnosis.DiagnosisRepository;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.prescription.rtvdiagnosisexist.RtvDiagnosisExistService;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.file.prescription.rtvdiagnosisexist.RtvDiagnosisExistDTO;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class DeleteDiagnosisService  extends AbstractService<DeleteDiagnosisService, DeleteDiagnosisDTO>
{
    private final Step execute = define("execute", DeleteDiagnosisDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private DiagnosisRepository diagnosisRepository;
	
	@Autowired
	private CreateLogFileService createLogFileService;
	
	@Autowired
	private RtvDiagnosisExistService rtvDiagnosisExistService;
	

    @Autowired
    public DeleteDiagnosisService() {
        super(DeleteDiagnosisService.class, DeleteDiagnosisDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(DeleteDiagnosisDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(DeleteDiagnosisDTO dto, DeleteDiagnosisDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataUpdate(dto);
		DiagnosisId diagnosisId = new DiagnosisId();
		diagnosisId.setPatientCode(dto.getPatientCode());
		diagnosisId.setDiagnosisDate(dto.getDiagnosisDate());
		diagnosisId.setDiagnosisTime(dto.getDiagnosisTime());
		try {
			diagnosisRepository.deleteById(diagnosisId);
			diagnosisRepository.flush();
			dto.setReturnCode(ReturnCodeEnum.fromCode(""));
			processingAfterDataUpdate(dto);
		} catch (Exception e) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
		}

       return result;
	}

	private StepResult processingBeforeDataUpdate(DeleteDiagnosisDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Generated:9)
		 */
		CreateLogFileDTO createLogFileDTO;
		RtvDiagnosisExistDTO rtvDiagnosisExistDTO;
		//switchSUB 9 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT RTV Diagnosis Exist? - Prescription  *
		rtvDiagnosisExistDTO = new RtvDiagnosisExistDTO();
		rtvDiagnosisExistDTO.setPatientCode(dto.getPatientCode());
		rtvDiagnosisExistDTO.setDiagnosisDate(dto.getDiagnosisDate());
		rtvDiagnosisExistDTO.setDiagnosisTime(dto.getDiagnosisTime());
		rtvDiagnosisExistService.execute(rtvDiagnosisExistDTO);
		dto.setLclUsrReturnCode(rtvDiagnosisExistDTO.getUsrReturnCode());
		//switchBLK 1000010 BLK CAS
		//switchSUB 1000010 BLK CAS
		if (dto.getLclUsrReturnCode() == UsrReturnCodeEnum.fromCode("F")) {
			// LCL.USR Return Code is Record found
			//switchBLK 1000013 BLK ACT
			//functionCall 1000014 ACT Send error message - 'Can't delete Diagnosis'
			// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1103100)
			//switchBLK 1000015 BLK ACT
			//functionCall 1000016 ACT <-- *QUIT
			// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
		}
		//switchBLK 1000023 BLK TXT
		// 
		//switchBLK 1000024 BLK ACT
		//functionCall 1000025 ACT LCL.Log Function Type = CON.DLTOBJ
		dto.setLclLogFunctionType("DLTOBJ");
		//switchBLK 1000028 BLK ACT
		//functionCall 1000029 ACT LCL.Log User Point = CON.Processing before Data
		dto.setLclLogUserPoint("Processing before Data");
		//switchBLK 1000032 BLK ACT
		//functionCall 1000033 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.update,CND.*One)
		dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "update"));
		//switchBLK 1000038 BLK ACT
		//functionCall 1000039 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(DeleteDiagnosisDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:17)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 17 SUB    
		//switchBLK 1000017 BLK ACT
		//functionCall 1000018 ACT PGM.*Return code = CND.*Normal
		dto.setReturnCode(ReturnCodeEnum.fromCode(""));
		//switchBLK 1000065 BLK TXT
		// 
		//switchBLK 1000066 BLK ACT
		//functionCall 1000067 ACT LCL.Log Function Type = CON.DLTOBJ
		dto.setLclLogFunctionType("DLTOBJ");
		//switchBLK 1000070 BLK ACT
		//functionCall 1000071 ACT LCL.Log User Point = CON.Processing after Data
		dto.setLclLogUserPoint("Processing after Data");
		//switchBLK 1000074 BLK ACT
		//functionCall 1000075 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.update,CND.*One)
		dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "update"));
		//switchBLK 1000080 BLK ACT
		//functionCall 1000081 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
       return NO_ACTION;
	}
}
