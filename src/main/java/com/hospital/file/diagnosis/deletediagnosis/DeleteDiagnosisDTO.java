package com.hospital.file.diagnosis.deletediagnosis;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class DeleteDiagnosisDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private LocalDate diagnosisDate;
	private LocalTime diagnosisTime;
	private ReturnCodeEnum returnCode;
	private String patientCode;
	private String nextScreen;
	private String lclLogFunctionType;
	private String lclLogUserPoint;
	private UsrReturnCodeEnum lclUsrReturnCode;

	public LocalDate getDiagnosisDate() {
		return diagnosisDate;
	}

	public LocalTime getDiagnosisTime() {
		return diagnosisTime;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public String getLclLogFunctionType() {
		return lclLogFunctionType;
	}

	public String getLclLogUserPoint() {
		return lclLogUserPoint;
	}

	public UsrReturnCodeEnum getLclUsrReturnCode() {
		return lclUsrReturnCode;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setDiagnosisDate(LocalDate diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}

	public void setDiagnosisTime(LocalTime diagnosisTime) {
		this.diagnosisTime = diagnosisTime;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public void setLclLogFunctionType(String lclLogFunctionType) {
		this.lclLogFunctionType = lclLogFunctionType;
	}

	public void setLclLogUserPoint(String lclLogUserPoint) {
		this.lclLogUserPoint = lclLogUserPoint;
	}

	public void setLclUsrReturnCode(UsrReturnCodeEnum lclUsrReturnCode) {
		this.lclUsrReturnCode = lclUsrReturnCode;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
