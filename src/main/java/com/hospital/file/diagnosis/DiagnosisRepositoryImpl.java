package com.hospital.file.diagnosis;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;

import com.hospital.file.diagnosis.Diagnosis;
import com.hospital.file.diagnosis.dspfil.DspfilGDO;
import com.hospital.file.diagnosis.selselectdiagnosis.SelSelectDiagnosisGDO;

/**
 * Custom Spring Data JPA repository implementation for model: Diagnosis (TSAGCPP).
 *
 * @author X2EGenerator
 */
@Repository
public class DiagnosisRepositoryImpl implements DiagnosisRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see com.hospital.file.diagnosis.DiagnosisService#deleteDiagnosis(Diagnosis)
	 */
	@Override
	public void deleteDiagnosis(
		String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(patientCode)) {
			whereClause += " diagnosis.id.patientCode = :patientCode";
			isParamSet = true;
		}

		if (diagnosisDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " diagnosis.id.diagnosisDate = :diagnosisDate";
			isParamSet = true;
		}

		if (diagnosisTime != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " diagnosis.id.diagnosisTime = :diagnosisTime";
			isParamSet = true;
		}

		String sqlString = "DELETE FROM Diagnosis diagnosis";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(patientCode)) {
			query.setParameter("patientCode", patientCode);
		}

		if (diagnosisDate != null) {
			query.setParameter("diagnosisDate", diagnosisDate);
		}

		if (diagnosisTime != null) {
			query.setParameter("diagnosisTime", diagnosisTime);
		}

		em.createNativeQuery("SET foreign_key_checks = 0").executeUpdate();
		query.executeUpdate();
		em.createNativeQuery("SET foreign_key_checks = 1").executeUpdate();
	}

	@Override
	public RestResponsePage<DspfilGDO> dspfil(
		String patientCode, LocalDate diagnosisDate, String doctorCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(patientCode)) {
			whereClause += " diagnosis.id.patientCode like CONCAT('%', :patientCode, '%')";
			isParamSet = true;
		}

		if (diagnosisDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " diagnosis.id.diagnosisDate = :diagnosisDate";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(doctorCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " diagnosis.doctorCode like CONCAT('%', :doctorCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.diagnosis.dspfil.DspfilGDO(diagnosis.id.patientCode, diagnosis.id.diagnosisDate, diagnosis.id.diagnosisTime, diagnosis.doctorCode, diagnosis.findings1, diagnosis.findings2) from Diagnosis diagnosis";
		String countQueryString = "SELECT COUNT(diagnosis.id.patientCode) FROM Diagnosis diagnosis";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Diagnosis diagnosis = new Diagnosis();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(diagnosis.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"diagnosis.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"diagnosis." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"diagnosis.id.patientCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"diagnosis.id.diagnosisDate"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"diagnosis.id.diagnosisTime"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(patientCode)) {
			countQuery.setParameter("patientCode", patientCode);
			query.setParameter("patientCode", patientCode);
		}

		if (diagnosisDate != null) {
			countQuery.setParameter("diagnosisDate", diagnosisDate);
			query.setParameter("diagnosisDate", diagnosisDate);
		}

		if (StringUtils.isNotEmpty(doctorCode)) {
			countQuery.setParameter("doctorCode", doctorCode);
			query.setParameter("doctorCode", doctorCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<DspfilGDO> content = query.getResultList();
		RestResponsePage<DspfilGDO> pageDto = new RestResponsePage<DspfilGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<SelSelectDiagnosisGDO> selSelectDiagnosis(
		String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(patientCode)) {
			whereClause += " diagnosis.id.patientCode like CONCAT('%', :patientCode, '%')";
			isParamSet = true;
		}

		if (diagnosisDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " diagnosis.id.diagnosisDate = :diagnosisDate";
			isParamSet = true;
		}

		if (diagnosisTime != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " diagnosis.id.diagnosisTime = :diagnosisTime";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.diagnosis.selselectdiagnosis.SelSelectDiagnosisGDO(diagnosis.id.patientCode, diagnosis.id.diagnosisDate, diagnosis.id.diagnosisTime, diagnosis.doctorCode, diagnosis.findings1, diagnosis.findings2, diagnosis.addedUser, diagnosis.addedDate, diagnosis.addedTime, diagnosis.changedUser, diagnosis.changedDate, diagnosis.changedTime) from Diagnosis diagnosis";
		String countQueryString = "SELECT COUNT(diagnosis.id.patientCode) FROM Diagnosis diagnosis";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Diagnosis diagnosis = new Diagnosis();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(diagnosis.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"diagnosis.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"diagnosis." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"diagnosis.id.patientCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"diagnosis.id.diagnosisDate"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"diagnosis.id.diagnosisTime"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(patientCode)) {
			countQuery.setParameter("patientCode", patientCode);
			query.setParameter("patientCode", patientCode);
		}

		if (diagnosisDate != null) {
			countQuery.setParameter("diagnosisDate", diagnosisDate);
			query.setParameter("diagnosisDate", diagnosisDate);
		}

		if (diagnosisTime != null) {
			countQuery.setParameter("diagnosisTime", diagnosisTime);
			query.setParameter("diagnosisTime", diagnosisTime);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<SelSelectDiagnosisGDO> content = query.getResultList();
		RestResponsePage<SelSelectDiagnosisGDO> pageDto = new RestResponsePage<SelSelectDiagnosisGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.diagnosis.DiagnosisService#rtvDiagnosisForPatient(String, String, String)
	 */
	@Override
	public RestResponsePage<Diagnosis> rtvDiagnosisForPatient(
		String patientCode, String hospitalCode, String wardCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(patientCode)) {
			whereClause += " diagnosis.id.patientCode = :patientCode";
			isParamSet = true;
		}

		String sqlString = "SELECT diagnosis FROM Diagnosis diagnosis";
		String countQueryString = "SELECT COUNT(diagnosis.id.patientCode) FROM Diagnosis diagnosis";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(patientCode)) {
			countQuery.setParameter("patientCode", patientCode);
			query.setParameter("patientCode", patientCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<Diagnosis> content = query.getResultList();
		RestResponsePage<Diagnosis> pageDto = new RestResponsePage<Diagnosis>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.diagnosis.DiagnosisService#rtvNbrDiagnosisPatient(String)
	 */
	@Override
	public RestResponsePage<Diagnosis> rtvNbrDiagnosisPatient(
		String patientCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(patientCode)) {
			whereClause += " diagnosis.id.patientCode = :patientCode";
			isParamSet = true;
		}

		String sqlString = "SELECT diagnosis FROM Diagnosis diagnosis";
		String countQueryString = "SELECT COUNT(diagnosis.id.patientCode) FROM Diagnosis diagnosis";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(patientCode)) {
			countQuery.setParameter("patientCode", patientCode);
			query.setParameter("patientCode", patientCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<Diagnosis> content = query.getResultList();
		RestResponsePage<Diagnosis> pageDto = new RestResponsePage<Diagnosis>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.diagnosis.DiagnosisService#rtvDoctorExist(String)
	 */
	@Override
	public RestResponsePage<Diagnosis> rtvDoctorExist(
		String doctorCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " diagnosis.doctorCode = :doctorCode";
			isParamSet = true;
		}

		String sqlString = "SELECT diagnosis FROM Diagnosis diagnosis";
		String countQueryString = "SELECT COUNT(diagnosis.id.patientCode) FROM Diagnosis diagnosis";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			countQuery.setParameter("doctorCode", doctorCode);
			query.setParameter("doctorCode", doctorCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<Diagnosis> content = query.getResultList();
		RestResponsePage<Diagnosis> pageDto = new RestResponsePage<Diagnosis>(
				content, pageable, count.longValue());

		return pageDto;
	}

}
