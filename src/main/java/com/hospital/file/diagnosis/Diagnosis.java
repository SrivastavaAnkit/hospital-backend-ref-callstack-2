package com.hospital.file.diagnosis;

import com.hospital.file.doctor.Doctor;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name="Diagnosis", schema="HospitalMgmt")
public class Diagnosis implements Serializable {
	private static final long serialVersionUID = -1L;

	@Version
	@Column(name="version")
	private long version;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns ({
		@JoinColumn(name="DoctorCode", referencedColumnName="DoctorCode", insertable=false, updatable=false)
	})
	private Doctor doctorObj;

	@EmbeddedId
	private DiagnosisId id = new DiagnosisId();

	@Column(name = "DoctorCode")
	private String doctorCode = "";
	
	@Column(name = "Findings1")
	private String findings1 = "";
	
	@Column(name = "Findings2")
	private String findings2 = "";

	public DiagnosisId getId() {
		return id;
	}

	public String getPatientCode() {
		return id.getPatientCode();
	}
	
	public LocalDate getDiagnosisDate() {
		return id.getDiagnosisDate();
	}
	
	public LocalTime getDiagnosisTime() {
		return id.getDiagnosisTime();
	}

	public String getDoctorCode() {
		return doctorCode;
	}
	
	public String getFindings1() {
		return findings1;
	}
	
	public String getFindings2() {
		return findings2;
	}

	public String getDoctorName() {
		return doctorObj.getDoctorName();
	}

	public long getVersion() {
		return version;
	}

	public void setPatientCode(String patientCode) {
		this.id.setPatientCode(patientCode);
	}
	
	public void setDiagnosisDate(LocalDate diagnosisDate) {
		this.id.setDiagnosisDate(diagnosisDate);
	}
	
	public void setDiagnosisTime(LocalTime diagnosisTime) {
		this.id.setDiagnosisTime(diagnosisTime);
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}
	
	public void setFindings1(String findings1) {
		this.findings1 = findings1;
	}
	
	public void setFindings2(String findings2) {
		this.findings2 = findings2;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
