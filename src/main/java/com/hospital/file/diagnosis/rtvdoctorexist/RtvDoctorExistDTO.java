package com.hospital.file.diagnosis.rtvdoctorexist;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvDoctorExistDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private ReturnCodeEnum returnCode;
	private String doctorCode;
	private UsrReturnCodeEnum usrReturnCode;
	private String nextScreen;
	private String lclLogFunctionType;
	private String lclLogUserPoint;
	private UsrReturnCodeEnum lclUsrReturnCode;

	public String getDoctorCode() {
		return doctorCode;
	}

	public UsrReturnCodeEnum getUsrReturnCode() {
		return usrReturnCode;
	}

	public String getLclLogFunctionType() {
		return lclLogFunctionType;
	}

	public String getLclLogUserPoint() {
		return lclLogUserPoint;
	}

	public UsrReturnCodeEnum getLclUsrReturnCode() {
		return lclUsrReturnCode;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public void setUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
		this.usrReturnCode = usrReturnCode;
	}

	public void setLclLogFunctionType(String lclLogFunctionType) {
		this.lclLogFunctionType = lclLogFunctionType;
	}

	public void setLclLogUserPoint(String lclLogUserPoint) {
		this.lclLogUserPoint = lclLogUserPoint;
	}

	public void setLclUsrReturnCode(UsrReturnCodeEnum lclUsrReturnCode) {
		this.lclUsrReturnCode = lclUsrReturnCode;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
