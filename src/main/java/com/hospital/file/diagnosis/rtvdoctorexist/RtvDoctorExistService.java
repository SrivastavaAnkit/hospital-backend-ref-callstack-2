package com.hospital.file.diagnosis.rtvdoctorexist;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.diagnosis.Diagnosis;
import com.hospital.file.diagnosis.DiagnosisId;
import com.hospital.file.diagnosis.DiagnosisRepository;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvDoctorExistService extends AbstractService<RtvDoctorExistService, RtvDoctorExistDTO>
{
    private final Step execute = define("execute", RtvDoctorExistDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private DiagnosisRepository diagnosisRepository;
	
	@Autowired
	private CreateLogFileService createLogFileService;
	
    @Autowired
    public RtvDoctorExistService()
    {
        super(RtvDoctorExistService.class, RtvDoctorExistDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvDoctorExistDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvDoctorExistDTO dto, RtvDoctorExistDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Diagnosis> diagnosisList = diagnosisRepository.findAllByDoctorCode(dto.getDoctorCode());
		if (!diagnosisList.isEmpty()) {
			for (Diagnosis diagnosis : diagnosisList) {
				processDataRecord(dto, diagnosis);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvDoctorExistDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 48 SUB    
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT LCL.USR Return Code = CND.Record Not found
		dto.setLclUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
		//switchBLK 1000031 BLK ACT
		//functionCall 1000032 ACT LCL.Log Function Type = CON.RTVOBJ
		dto.setLclLogFunctionType("RTVOBJ");
		//switchBLK 1000035 BLK ACT
		//functionCall 1000036 ACT LCL.Log User Point = CON.Initialize routine
		dto.setLclLogUserPoint("Initialize routine");
		//switchBLK 1000039 BLK ACT
		//functionCall 1000040 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvDoctorExistDTO dto, Diagnosis diagnosis) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 41 SUB    
		//switchBLK 1000017 BLK ACT
		//functionCall 1000018 ACT LCL.USR Return Code = CND.Record found
		dto.setLclUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
		//switchBLK 1000064 BLK TXT
		// 
		//switchBLK 1000065 BLK ACT
		//functionCall 1000066 ACT LCL.Log Function Type = CON.RTVOBJ
		dto.setLclLogFunctionType("RTVOBJ");
		//switchBLK 1000069 BLK ACT
		//functionCall 1000070 ACT LCL.Log User Point = CON.Process Data record
		dto.setLclLogUserPoint("Process Data record");
		//switchBLK 1000073 BLK ACT
		//functionCall 1000074 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
		//switchBLK 1000092 BLK TXT
		// 
		//switchBLK 1000023 BLK ACT
		//functionCall 1000024 ACT <-- *QUIT
		// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvDoctorExistDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 52 SUB    
		//switchBLK 1000013 BLK ACT
		//functionCall 1000014 ACT LCL.USR Return Code = CND.Record Not found
		dto.setLclUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
		//switchBLK 1000044 BLK TXT
		// 
		//switchBLK 1000045 BLK ACT
		//functionCall 1000046 ACT LCL.Log Function Type = CON.RTVOBJ
		dto.setLclLogFunctionType("RTVOBJ");
		//switchBLK 1000049 BLK ACT
		//functionCall 1000050 ACT LCL.Log User Point = CON.Processing if Data record
		dto.setLclLogUserPoint("Processing if Data record");
		//switchBLK 1000053 BLK ACT
		//functionCall 1000054 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.not found,CND.*One)
		dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "not found"));
		//switchBLK 1000059 BLK ACT
		//functionCall 1000060 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvDoctorExistDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 61 SUB    
		//switchBLK 1000025 BLK ACT
		//functionCall 1000026 ACT PAR.USR Return Code = LCL.USR Return Code
		dto.setUsrReturnCode(dto.getLclUsrReturnCode());
		//switchBLK 1000078 BLK TXT
		// 
		//switchBLK 1000079 BLK ACT
		//functionCall 1000080 ACT LCL.Log Function Type = CON.RTVOBJ
		dto.setLclLogFunctionType("RTVOBJ");
		//switchBLK 1000083 BLK ACT
		//functionCall 1000084 ACT LCL.Log User Point = CON.Exit processing
		dto.setLclLogUserPoint("Exit processing");
		//switchBLK 1000087 BLK ACT
		//functionCall 1000088 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
        return NO_ACTION;
    }
}
