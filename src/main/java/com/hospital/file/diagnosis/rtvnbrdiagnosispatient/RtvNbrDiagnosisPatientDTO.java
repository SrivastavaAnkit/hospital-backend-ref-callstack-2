package com.hospital.file.diagnosis.rtvnbrdiagnosispatient;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvNbrDiagnosisPatientDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private ReturnCodeEnum returnCode;
	private String patientCode;
	private long usrNbrOfDiagnosis;
	private String nextScreen;
	private long lclUsrNbrOfDiagnosis;

	public String getPatientCode() {
		return patientCode;
	}

	public long getUsrNbrOfDiagnosis() {
		return usrNbrOfDiagnosis;
	}

	public long getLclUsrNbrOfDiagnosis() {
		return lclUsrNbrOfDiagnosis;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public void setUsrNbrOfDiagnosis(long usrNbrOfDiagnosis) {
		this.usrNbrOfDiagnosis = usrNbrOfDiagnosis;
	}

	public void setLclUsrNbrOfDiagnosis(long lclUsrNbrOfDiagnosis) {
		this.lclUsrNbrOfDiagnosis = lclUsrNbrOfDiagnosis;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
