package com.hospital.file.diagnosis.rtvnbrdiagnosispatient;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.diagnosis.Diagnosis;
import com.hospital.file.diagnosis.DiagnosisId;
import com.hospital.file.diagnosis.DiagnosisRepository;
import com.hospital.model.ReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvNbrDiagnosisPatientService extends AbstractService<RtvNbrDiagnosisPatientService, RtvNbrDiagnosisPatientDTO>
{
    private final Step execute = define("execute", RtvNbrDiagnosisPatientDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private DiagnosisRepository diagnosisRepository;
	
    @Autowired
    public RtvNbrDiagnosisPatientService()
    {
        super(RtvNbrDiagnosisPatientService.class, RtvNbrDiagnosisPatientDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvNbrDiagnosisPatientDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvNbrDiagnosisPatientDTO dto, RtvNbrDiagnosisPatientDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Diagnosis> diagnosisList = diagnosisRepository.findAllByIdPatientCode(dto.getPatientCode());
		if (!diagnosisList.isEmpty()) {
			for (Diagnosis diagnosis : diagnosisList) {
				processDataRecord(dto, diagnosis);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvNbrDiagnosisPatientDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT PAR.USR Nbr of Diagnosis = CON.*ZERO
		dto.setUsrNbrOfDiagnosis(0);
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT LCL.USR Nbr of Diagnosis = CON.*ZERO
		dto.setLclUsrNbrOfDiagnosis(0);
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvNbrDiagnosisPatientDTO dto, Diagnosis diagnosis) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		//switchSUB 41 SUB    
		//switchBLK 1000013 BLK ACT
		//functionCall 1000014 ACT LCL.USR Nbr of Diagnosis = LCL.USR Nbr of Diagnosis + CON.1
		dto.setLclUsrNbrOfDiagnosis(dto.getLclUsrNbrOfDiagnosis() + 1);
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvNbrDiagnosisPatientDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		//switchSUB 52 SUB    
		//switchBLK 1000027 BLK ACT
		//functionCall 1000028 ACT PAR.USR Nbr of Diagnosis = CON.*ZERO
		dto.setUsrNbrOfDiagnosis(0);
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvNbrDiagnosisPatientDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		//switchSUB 61 SUB    
		//switchBLK 1000021 BLK ACT
		//functionCall 1000022 ACT PAR.USR Nbr of Diagnosis = LCL.USR Nbr of Diagnosis
		dto.setUsrNbrOfDiagnosis(dto.getLclUsrNbrOfDiagnosis());
        return NO_ACTION;
    }
}
