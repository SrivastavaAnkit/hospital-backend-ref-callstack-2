package com.hospital.file.diagnosis;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.hospital.file.diagnosis.dspfil.DspfilGDO;
import com.hospital.file.diagnosis.selselectdiagnosis.SelSelectDiagnosisGDO;

/**
 * Custom Spring Data JPA repository interface for model: Diagnosis (TSAGCPP).
 *
 * @author X2EGenerator
 */
@Repository
public interface DiagnosisRepositoryCustom {

	/**
	 * 
	 * @param patientCode Patient Code
	 * @param diagnosisDate Diagnosis Date
	 * @param diagnosisTime Diagnosis Time
	 */
	void deleteDiagnosis(String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime);

	/**
	 * 
	 * @param patientCode Patient Code
	 * @param diagnosisDate Diagnosis Date
	 * @param doctorCode Doctor Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of DspfilGDO
	 */
	RestResponsePage<DspfilGDO> dspfil(String patientCode, LocalDate diagnosisDate, String doctorCode, Pageable pageable);

	/**
	 * 
	 * @param patientCode Patient Code
	 * @param diagnosisDate Diagnosis Date
	 * @param diagnosisTime Diagnosis Time
	 * @param pageable a Pageable object used for pagination
	 * @return Page of SelSelectDiagnosisGDO
	 */
	RestResponsePage<SelSelectDiagnosisGDO> selSelectDiagnosis(String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime, Pageable pageable);

	/**
	 * 
	 * @param patientCode Patient Code
	 * @param hospitalCode Hospital Code
	 * @param wardCode Ward Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of Diagnosis
	 */
	RestResponsePage<Diagnosis> rtvDiagnosisForPatient(String patientCode, String hospitalCode, String wardCode, Pageable pageable);

	/**
	 * 
	 * @param patientCode Patient Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of Diagnosis
	 */
	RestResponsePage<Diagnosis> rtvNbrDiagnosisPatient(String patientCode, Pageable pageable);

	/**
	 * 
	 * @param doctorCode Doctor Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of Diagnosis
	 */
	RestResponsePage<Diagnosis> rtvDoctorExist(String doctorCode, Pageable pageable);
}
