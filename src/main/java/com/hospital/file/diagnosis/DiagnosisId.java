package com.hospital.file.diagnosis;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Convert;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.hospital.common.jpa.DateConverter;
import com.hospital.common.jpa.TimeConverter;

public class DiagnosisId implements Serializable {
	private static final long serialVersionUID = -1L;

	@Column(name = "PatientCode")
	private String patientCode = "";
	
	@Column(name = "DiagnosisDate")
	@Convert(converter=DateConverter.class)
	private LocalDate diagnosisDate = LocalDate.of(1801, 1, 1);
	
	@Column(name = "DiagnosisTime")
	@Convert(converter=  TimeConverter.class)
	private LocalTime diagnosisTime = LocalTime.of(0, 0);

	public DiagnosisId() {
	
	}

	public DiagnosisId(String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime) {
		this.patientCode = patientCode;
		this.diagnosisDate = diagnosisDate;
		this.diagnosisTime = diagnosisTime;
	}

	public String getPatientCode() {
		return patientCode;
	}
	
	public LocalDate getDiagnosisDate() {
		return diagnosisDate;
	}
	
	public LocalTime getDiagnosisTime() {
		return diagnosisTime;
	}
	
	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}
	
	public void setDiagnosisDate(LocalDate diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}
	
	public void setDiagnosisTime(LocalTime diagnosisTime) {
		this.diagnosisTime = diagnosisTime;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
