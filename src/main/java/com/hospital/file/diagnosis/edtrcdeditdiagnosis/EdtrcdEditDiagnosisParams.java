package com.hospital.file.diagnosis.edtrcdeditdiagnosis;

import java.io.Serializable;
import java.math.BigDecimal;

import com.hospital.model.ReturnCodeEnum;

/**
 * Params for resource: EdtrcdEditDiagnosis (TSAXE1R).
 *
 * @author X2EGenerator
 */
public class EdtrcdEditDiagnosisParams implements Serializable{
    private String patientCode = "";
    private ReturnCodeEnum returnCode = null;

    public String getPatientCode() {
		return patientCode;
	}
	
	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}
	
	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}
	
	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
	
	public void setReturnCode(String returnCode) {
		setReturnCode(ReturnCodeEnum.valueOf(returnCode));
	}
}
