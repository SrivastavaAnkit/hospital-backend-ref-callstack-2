package com.hospital.file.diagnosis.edtrcdeditdiagnosis;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.exception.ServiceException;
import com.hospital.file.diagnosis.Diagnosis;
import com.hospital.file.diagnosis.DiagnosisId;
import com.hospital.file.diagnosis.DiagnosisRepository;
import com.hospital.file.diagnosis.changediagnosis.ChangeDiagnosisDTO;
import com.hospital.file.diagnosis.changediagnosis.ChangeDiagnosisService;
import com.hospital.file.diagnosis.creatediagnosis.CreateDiagnosisDTO;
import com.hospital.file.diagnosis.creatediagnosis.CreateDiagnosisService;
import com.hospital.file.diagnosis.deletediagnosis.DeleteDiagnosisDTO;
import com.hospital.file.diagnosis.deletediagnosis.DeleteDiagnosisService;
import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameDTO;
import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameService;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorDTO;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorService;
import com.hospital.file.doctor.selectdoctor.SelectDoctorParams;
import com.hospital.file.doctor.selectdoctor.SelectDoctorService;
import com.hospital.file.hospital.eredithospital.ErEditHospitalState;
import com.hospital.file.hospital.selecthospital.SelectHospitalParams;
import com.hospital.file.hospital.selecthospital.SelectHospitalService;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.patient.pmtdiagnosisandprescrip.PmtDiagnosisAndPrescripDTO;
import com.hospital.file.patient.pmtdiagnosisandprescrip.PmtDiagnosisAndPrescripService;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameDTO;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameService;
import com.hospital.file.prescription.trneditprescriptions.TrnEditPrescriptionsParams;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ProgramModeEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.support.JobContext;
/**
 * Service controller for 'EDTRCD Edit Diagnosis' (TSAXE1R) of file 'Diagnosis' (TSAGCPP)
 *
 * @author X2EGenerator
 */
@Service
public class EdtrcdEditDiagnosisService extends AbstractService<EdtrcdEditDiagnosisService, EdtrcdEditDiagnosisState>
{
    
    @Autowired
    private JobContext job;
    
    @Autowired
    private DiagnosisRepository diagnosisRepository;
    
    @Autowired
    private ChangeDiagnosisService changeDiagnosisService;
    
    @Autowired
    private CreateDiagnosisService createDiagnosisService;
    
    @Autowired
    private CreateLogFileService createLogFileService;
    
    @Autowired
    private DeleteDiagnosisService deleteDiagnosisService;
    
    @Autowired
    private RtvDoctorNameService rtvDoctorNameService;
    
    @Autowired
    private RtvForSupervisorService rtvForSupervisorService;
    
    @Autowired
    private RtvPatientNameService rtvPatientNameService;
    
    @Autowired
    private MessageSource messageSource;
    
    public static final String SCREEN_KEY = "edtrcdEditDiagnosisEntryPanel";
    public static final String SCREEN_DTL = "edtrcdEditDiagnosisPanel";
    public static final String SCREEN_CFM = "EdtrcdEditDiagnosis.confirm";
    
    private final Step execute = define("execute", EdtrcdEditDiagnosisParams.class, this::execute);
    private final Step keyScreenResponse = define("keyScreen", EdtrcdEditDiagnosisDTO.class, this::processKeyScreenResponse);
    private final Step promptSelectDoctor = define("promptSelectDoctor",SelectDoctorParams.class, this::processPromptSelectDoctor);
    private final Step detailScreenResponse = define("dtlScreen", EdtrcdEditDiagnosisDTO.class, this::processDetailScreenResponse);
    private final Step confirmScreenResponse = define("cfmScreen", EdtrcdEditDiagnosisDTO.class, this::processConfirmScreenResponse);
    //private final Step serviceDeleteDiagnosis = define("serviceDeleteDiagnosis",DeleteDiagnosisParams.class, this::processServiceDeleteDiagnosis);
    //private final Step serviceCreateDiagnosis = define("serviceCreateDiagnosis",CreateDiagnosisParams.class, this::processServiceCreateDiagnosis);
    //private final Step serviceChangeDiagnosis = define("serviceChangeDiagnosis",ChangeDiagnosisParams.class, this::processServiceChangeDiagnosis);
    //private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
    //private final Step serviceRtvForSupervisor = define("serviceRtvForSupervisor",RtvForSupervisorParams.class, this::processServiceRtvForSupervisor);
    //private final Step serviceTrnEditPrescriptions = define("serviceTrnEditPrescriptions",TrnEditPrescriptionsParams.class, this::processServiceTrnEditPrescriptions);
    //private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
    //private final Step serviceQuit = define("serviceQuit",QuitParams.class, this::processServiceQuit);
    //private final Step serviceJobDateHaveChanged = define("serviceJobDateHaveChanged",JobDateHaveChangedParams.class, this::processServiceJobDateHaveChanged);
    //private final Step serviceRtvPatientName = define("serviceRtvPatientName",RtvPatientNameParams.class, this::processServiceRtvPatientName);
    //private final Step serviceRtvDoctorName = define("serviceRtvDoctorName",RtvDoctorNameParams.class, this::processServiceRtvDoctorName);
    //private final Step serviceCreateLogFile = define("serviceCreateLogFile",CreateLogFileParams.class, this::processServiceCreateLogFile);
    //private final Step serviceConcat = define("serviceConcat",ConcatParams.class, this::processServiceConcat);
    
        @Autowired
    public EdtrcdEditDiagnosisService()
    {
        super(EdtrcdEditDiagnosisService.class, EdtrcdEditDiagnosisState.class);
    }
    
    @Override
    public Step getInitialStep()
    {
        return execute;
    }
    
    /**
     * EDTRCD controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(EdtrcdEditDiagnosisState state, EdtrcdEditDiagnosisParams params)
    {
        StepResult result = NO_ACTION;
    
        if(params != null) {
            BeanUtils.copyProperties(params, state);
        }
    
        state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
        usrInitializeProgram(state);

        result = conductKeyScreenConversation(state);

        return result;
    }
    
    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductKeyScreenConversation(EdtrcdEditDiagnosisState state) 
    {
        StepResult result = NO_ACTION;
    
        usrInitializeKeyScreen(state);
        if (CmdKeyEnum.isCancel(state.get_SysCmdKey()))
        {
            result = displayKeyScreenConversation(state);
        }
        else
        {
            result = processKeyScreenResponse(state, state);
        }
    
        return result;
    }
    
    /**
     * SCREEN_KEY display processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult displayKeyScreenConversation(EdtrcdEditDiagnosisState state)
    {
        StepResult result = NO_ACTION;
    
        if (state.get_SysConductKeyScreenConversation()) {
            EdtrcdEditDiagnosisDTO model = new EdtrcdEditDiagnosisDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_KEY, model).thenCall(keyScreenResponse);
        }
    
        return result;
    }
    
    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processKeyScreenResponse(EdtrcdEditDiagnosisState state, EdtrcdEditDiagnosisDTO model)
    {
        StepResult result = NO_ACTION;
        
        BeanUtils.copyProperties(model, state);
    
        if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            result = displayKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            result = conductKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
           result = closedown(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
    
        }
        else if(CmdKeyEnum.isChangeMode(state.get_SysCmdKey())) {
            if(state.get_SysProgramMode() == ProgramModeEnum._STA_ADD) {
                state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            }
            else {
                state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
            }
            result = conductKeyScreenConversation(state);
        }
        else {
            checkKeyFields(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = displayKeyScreenConversation(state);
                return result;
            }
            usrValidateKeyScreen(state);
            dbfReadDataRecord(state);
            if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
                usrInitializeExistingScreen(state);
            }
            else {
                usrInitializeNewScreen(state);
            }
    
            result = conductDetailScreenConversation(state);
        }
    
        return result;
    }
    
    /**
     * Check key fields set in SCREEN_KEY.
     * For now it's just if fields are null or strings are empty
     * @param state - Service state class.
     * @return
     */
    private void checkKeyFields(EdtrcdEditDiagnosisState state) {
       if(state.getDiagnosisDate() == null || state.getDiagnosisTime() == null)
       {
           state.set_SysErrorFound(true);
           state.getMessageMap().put("diagnosisDate",
    		   messageSource.getMessage("value.required", null, null, null));
           state.getMessageMap().put("diagnosisTime",
    		   messageSource.getMessage("diagnosisTime", null, null, null));
    	   state.setMessageMap(state.getMessageMap());
       }
    }
    
    /**
     * SCREEN_DTL display processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductDetailScreenConversation(EdtrcdEditDiagnosisState state)
    {
        StepResult result = NO_ACTION;
    
        if (state.get_SysConductDetailScreenConversation()) {
            EdtrcdEditDiagnosisDTO model = new EdtrcdEditDiagnosisDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_DTL, model).thenCall(detailScreenResponse);
        }
    
        return result;
    }
    
    /**
     * SCREEN_DETAIL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processDetailScreenResponse(EdtrcdEditDiagnosisState state, EdtrcdEditDiagnosisDTO model)
    {
        StepResult result = NO_ACTION;
    
        BeanUtils.copyProperties(model, state);
    
        if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            result = conductDetailScreenConversation(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            result = conductDetailScreenConversation(state);
        }
        else if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            result = closedown(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
        	switch (state.get_SysEntrySelected())
		    {
		        case "doctorCode":
		            SelectDoctorParams selectDoctorParams = new SelectDoctorParams();
		            BeanUtils.copyProperties(state, selectDoctorParams);
		            result = StepResult.callService(SelectDoctorService.class, selectDoctorParams).thenCall(promptSelectDoctor);
		            break;
		        default:
		            System.out.println("Field "+ state.get_SysEntrySelected() + " is not promptable");
		            result = displayKeyScreenConversation(state);
		            break;
		    }
    } 
        else if (CmdKeyEnum.isKeyScreen(state.get_SysCmdKey())) {
            result = usrProcessKeyScreenRequest(state);
            result = conductKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isDelete(state.get_SysCmdKey())) {
            //delete action confirmed in client side
            dbfDeleteDataRecord(state);
            result = displayKeyScreenConversation(state);
        }
        else {
            checkFields(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = conductDetailScreenConversation(state);
                return result;
            }
            usrValidateDetailScreenFields(state);
            checkRelations(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                result = conductDetailScreenConversation(state);
                return result;
            }
            usrDetailScreenFunctionFields(state);
            usrValidateDetailScreenRelations(state);
            //TODO: make confirm screen
            result = processConfirmScreenResponse(state, null);//callScreen(SCREEN_CFM, state).thenCall(confirmScreenResponse);
        }
    
        return result;
    }
    /**
     * SCREEN_CONFIRM returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processConfirmScreenResponse(EdtrcdEditDiagnosisState state, EdtrcdEditDiagnosisDTO model)
    {
        StepResult result = NO_ACTION;
    
        if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
            dbfUpdateDataRecord(state);
        }
        else {
            dbfCreateDataRecord(state);
        }
    
        result = usrProcessCommandKeys(state);
        if (result != StepResult.NO_ACTION) {
            return result;
        }
        result = conductKeyScreenConversation(state);
    
        return result;
    }
    
    /**
     * Check fields set in SCREEN_DETAIL.
     * @param state - Service state class.
     * @return
     */
    private void checkFields(EdtrcdEditDiagnosisState state) {
    
    }
    
    /**
     * Check relations set in SCREEN_DETAIL.
     * @param state - Service state class.
     * @return
     */
    private void checkRelations(EdtrcdEditDiagnosisState state) {
    
    }
    
    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(EdtrcdEditDiagnosisState state)
    {
        StepResult result = NO_ACTION;
    
        result = usrExitCommandProcessing(state);
    
        EdtrcdEditDiagnosisParams params = new EdtrcdEditDiagnosisParams();
        BeanUtils.copyProperties(state, params);
        result = StepResult.returnFromService(params);
    
        return result;
    }
        
    /**
     * ------------------------ Generated DBF methods ----------------------
     */
    
    private StepResult dbfReadDataRecord(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;
    
        DiagnosisId diagnosisId = new DiagnosisId(dto.getPatientCode(), dto.getDiagnosisDate(), dto.getDiagnosisTime());
        Diagnosis diagnosis = diagnosisRepository.findById(diagnosisId).orElse(null);
    
        if (diagnosis == null) {
            dto.set_SysProgramMode(ProgramModeEnum._STA_ADD);
        }
        else {
            dto.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            BeanUtils.copyProperties(diagnosis, dto);
        }
        return result;
    }
    
    private StepResult dbfCreateDataRecord(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;
        try {
            /**
    		 * USER: Create Object (Generated:425)
    		 */
    		CreateDiagnosisDTO createDiagnosisDTO;
    		//switchBLK 425 BLK ACT
    		// DEBUG genFunctionCall 426 ACT Create Diagnosis - Diagnosis  *
    		// DEBUG genFunctionCall ServiceDtoVariable
    		createDiagnosisDTO = new CreateDiagnosisDTO();
    		// DEBUG genFunctionCall Parameters IN
    		createDiagnosisDTO.setPatientCode(dto.getPatientCode());
    		createDiagnosisDTO.setDiagnosisDate(dto.getDiagnosisDate());
    		createDiagnosisDTO.setDiagnosisTime(dto.getDiagnosisTime());
    		createDiagnosisDTO.setDoctorCode(dto.getDoctorCode());
    		createDiagnosisDTO.setFindings1(dto.getFindings1());
    		createDiagnosisDTO.setFindings2(dto.getFindings2());
    		// DEBUG genFunctionCall Service call
    		createDiagnosisService.execute(createDiagnosisDTO);
    		// DEBUG genFunctionCall Parameters OUT
    		// DEBUG genFunctionCall Parameters DONE
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }
    private StepResult dbfDeleteDataRecord(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;
        try {
            /**
    		 * USER: Delete Object (Generated:383)
    		 */
    		DeleteDiagnosisDTO deleteDiagnosisDTO;
    		//switchBLK 383 BLK ACT
    		// DEBUG genFunctionCall 384 ACT Delete Diagnosis - Diagnosis  *
    		// DEBUG genFunctionCall ServiceDtoVariable
    		deleteDiagnosisDTO = new DeleteDiagnosisDTO();
    		// DEBUG genFunctionCall Parameters IN
    		deleteDiagnosisDTO.setPatientCode(dto.getPatientCode());
    		deleteDiagnosisDTO.setDiagnosisDate(dto.getDiagnosisDate());
    		deleteDiagnosisDTO.setDiagnosisTime(dto.getDiagnosisTime());
    		// DEBUG genFunctionCall Service call
    		deleteDiagnosisService.execute(deleteDiagnosisDTO);
    		// DEBUG genFunctionCall Parameters OUT
    		// DEBUG genFunctionCall Parameters DONE
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    
        return result;
    }
    
    private StepResult dbfUpdateDataRecord(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;
        try {
    		DiagnosisId diagnosisId = new DiagnosisId(dto.getPatientCode(), dto.getDiagnosisDate(), dto.getDiagnosisTime());
    		if (diagnosisRepository.existsById(diagnosisId)) {
    
            	/**
    			 * USER: Change Object (Generated:427)
    			 */
    			ChangeDiagnosisDTO changeDiagnosisDTO;
    			//switchBLK 427 BLK ACT
    			// DEBUG genFunctionCall 428 ACT Change Diagnosis - Diagnosis  *
    			// DEBUG genFunctionCall ServiceDtoVariable
    			changeDiagnosisDTO = new ChangeDiagnosisDTO();
    			// DEBUG genFunctionCall Parameters IN
    			changeDiagnosisDTO.setPatientCode(dto.getPatientCode());
    			changeDiagnosisDTO.setDiagnosisDate(dto.getDiagnosisDate());
    			changeDiagnosisDTO.setDiagnosisTime(dto.getDiagnosisTime());
    			changeDiagnosisDTO.setDoctorCode(dto.getDoctorCode());
    			changeDiagnosisDTO.setFindings1(dto.getFindings1());
    			changeDiagnosisDTO.setFindings2(dto.getFindings2());
    			// DEBUG genFunctionCall Service call
    			changeDiagnosisService.execute(changeDiagnosisDTO);
    			// DEBUG genFunctionCall Parameters OUT
    			// DEBUG genFunctionCall Parameters DONE
    	        }
            else {
                throw new ServiceException("diagnosis.nf");
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }
    
    
/**
 * ------------------ Generated ActionDiagram UserPoint -----------------
 */

    private StepResult usrInitializeProgram(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Initialize Program (Generated:10)
			 */
			CreateLogFileDTO createLogFileDTO;
			//switchSUB 10 SUB    
			//switchBLK 1000130 BLK ACT
			// DEBUG genFunctionCall 1000131 ACT PGM.*Program mode = CND.*CHANGE
			dto.set_SysProgramMode(ProgramModeEnum.fromCode("CHG"));
			//switchBLK 1000156 BLK TXT
			// 
			//switchBLK 1000157 BLK ACT
			// DEBUG genFunctionCall 1000158 ACT LCL.Log Function Type = CON.EDTRCD
			dto.setLclLogFunctionType("EDTRCD");
			//switchBLK 1000161 BLK ACT
			// DEBUG genFunctionCall 1000162 ACT LCL.Log User Point = CON.Initialize program
			dto.setLclLogUserPoint("Initialize program");
			//switchBLK 1000171 BLK ACT
			// DEBUG genFunctionCall 1000172 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrInitializeKeyScreen(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Initialize Key Screen (Generated:484)
			 */
			CreateLogFileDTO createLogFileDTO;
			//switchSUB 484 SUB    
			//switchBLK 1000007 BLK ACT
			// DEBUG genFunctionCall 1000008 ACT KEY.Diagnosis Date = JOB.*Job date
			dto.setDiagnosisDate(LocalDate.now());
			//switchBLK 1000013 BLK ACT
			// DEBUG genFunctionCall 1000014 ACT KEY.Diagnosis Time = JOB.*Job time
			dto.setDiagnosisTime(LocalTime.now());
			//switchBLK 1000200 BLK TXT
			// 
			//switchBLK 1000201 BLK ACT
			// DEBUG genFunctionCall 1000202 ACT LCL.Log Function Type = CON.EDTRCD
			dto.setLclLogFunctionType("EDTRCD");
			//switchBLK 1000205 BLK ACT
			// DEBUG genFunctionCall 1000206 ACT LCL.Log User Point = CON.Initialize key screen
			dto.setLclLogUserPoint("Initialize key screen");
			//switchBLK 1000215 BLK ACT
			// DEBUG genFunctionCall 1000216 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrValidateKeyScreen(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Validate Key Screen (Generated:418)
			 */
			CreateLogFileDTO createLogFileDTO;
			//switchSUB 418 SUB    
			//switchBLK 1000136 BLK CAS
			//switchSUB 1000136 BLK CAS
			if (!dto.getDiagnosisDate().equals(LocalDate.now())) {
				// KEY.Diagnosis Date NE JOB.*Job date
				//switchBLK 1000140 BLK ACT
				// DEBUG genFunctionCall 1000141 ACT Send information message - 'Job Date have changed'
				//TODO: will be handled using dto
				//e.rejectValue("", "job.date.have.changed");
			}
			//switchBLK 1000222 BLK TXT
			// 
			//switchBLK 1000223 BLK ACT
			// DEBUG genFunctionCall 1000224 ACT LCL.Log Function Type = CON.EDTRCD
			dto.setLclLogFunctionType("EDTRCD");
			//switchBLK 1000227 BLK ACT
			// DEBUG genFunctionCall 1000228 ACT LCL.Log User Point = CON.Validate key screen
			dto.setLclLogUserPoint("Validate key screen");
			//switchBLK 1000237 BLK ACT
			// DEBUG genFunctionCall 1000238 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrInitializeNewScreen(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Initialize Detail Screen (New Record) (Generated:239)
			 */
			CreateLogFileDTO createLogFileDTO;
			RtvPatientNameDTO rtvPatientNameDTO;
			//switchSUB 239 SUB    
			//switchBLK 1000142 BLK ACT
			// DEBUG genFunctionCall 1000143 ACT RTV Patient Name - Patient  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvPatientNameDTO = new RtvPatientNameDTO();
			// DEBUG genFunctionCall Parameters IN
			// TODO: Unsupported Action Diagram Context: KEY
			// DEBUG genFunctionCall Service call
			rtvPatientNameService.execute(rtvPatientNameDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setPatientName(rtvPatientNameDTO.getPatientName());
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000244 BLK TXT
			// 
			//switchBLK 1000245 BLK ACT
			// DEBUG genFunctionCall 1000246 ACT LCL.Log Function Type = CON.EDTRCD
			dto.setLclLogFunctionType("EDTRCD");
			//switchBLK 1000249 BLK ACT
			// DEBUG genFunctionCall 1000250 ACT LCL.Log User Point = CON.Initialize detail screen
			dto.setLclLogUserPoint("Initialize detail screen");
			//switchBLK 1000253 BLK ACT
			// DEBUG genFunctionCall 1000254 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.(new record),CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "(new record)"));
			//switchBLK 1000259 BLK ACT
			// DEBUG genFunctionCall 1000260 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrInitializeExistingScreen(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * Initialize Detail Screen (Existing Record) (Generated:242)
			 */
			CreateLogFileDTO createLogFileDTO;
			RtvForSupervisorDTO rtvForSupervisorDTO;
			//switchSUB 242 SUB    
			//switchBLK 1000003 BLK ACT
			// DEBUG genFunctionCall 1000004 ACT RTV For Supervisor - Doctor  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvForSupervisorDTO = new RtvForSupervisorDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvForSupervisorDTO.setDoctorCode(dto.getDoctorCode());
			// DEBUG genFunctionCall Service call
			rtvForSupervisorService.execute(rtvForSupervisorDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setDoctorName(rtvForSupervisorDTO.getDoctorName());
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000290 BLK TXT
			// 
			//switchBLK 1000291 BLK ACT
			// DEBUG genFunctionCall 1000292 ACT LCL.Log Function Type = CON.EDTRCD
			dto.setLclLogFunctionType("EDTRCD");
			//switchBLK 1000295 BLK ACT
			// DEBUG genFunctionCall 1000296 ACT LCL.Log User Point = CON.Initialize detail screen
			dto.setLclLogUserPoint("Initialize detail screen");
			//switchBLK 1000299 BLK ACT
			// DEBUG genFunctionCall 1000300 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.(existing record),CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "(existing record)"));
			//switchBLK 1000305 BLK ACT
			// DEBUG genFunctionCall 1000306 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrValidateDetailScreenFields(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Validate Detail Screen Fields (Generated:502)
			 */
			CreateLogFileDTO createLogFileDTO;
			//switchSUB 502 SUB    
			//switchBLK 1000039 BLK CAS
			//switchSUB 1000039 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				//switchBLK 1000051 BLK ACT
				// DEBUG genFunctionCall 1000052 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000042 BLK ACT
				// DEBUG genFunctionCall 1000043 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
			//switchBLK 1000340 BLK TXT
			// 
			//switchBLK 1000341 BLK ACT
			// DEBUG genFunctionCall 1000342 ACT LCL.Log Function Type = CON.EDTRCD
			dto.setLclLogFunctionType("EDTRCD");
			//switchBLK 1000345 BLK ACT
			// DEBUG genFunctionCall 1000346 ACT LCL.Log User Point = CON.Validate detail screen
			dto.setLclLogUserPoint("Validate detail screen");
			//switchBLK 1000349 BLK ACT
			// DEBUG genFunctionCall 1000350 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.fields,CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "fields"));
			//switchBLK 1000355 BLK ACT
			// DEBUG genFunctionCall 1000356 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrDetailScreenFunctionFields(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * CALC: Detail Screen Function Fields (Generated:469)
			 */
			CreateLogFileDTO createLogFileDTO;
			RtvDoctorNameDTO rtvDoctorNameDTO;
			//switchSUB 469 SUB    
			//switchBLK 1000147 BLK ACT
			// DEBUG genFunctionCall 1000148 ACT RTV Doctor Name - Doctor  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvDoctorNameDTO = new RtvDoctorNameDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvDoctorNameDTO.setDoctorCode(dto.getDoctorCode());
			// DEBUG genFunctionCall Service call
			rtvDoctorNameService.execute(rtvDoctorNameDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setDoctorName(rtvDoctorNameDTO.getDoctorName());
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000386 BLK TXT
			// 
			//switchBLK 1000387 BLK ACT
			// DEBUG genFunctionCall 1000388 ACT LCL.Log Function Type = CON.EDTRCD
			dto.setLclLogFunctionType("EDTRCD");
			//switchBLK 1000391 BLK ACT
			// DEBUG genFunctionCall 1000392 ACT LCL.Log User Point = CON.Detail screen function
			dto.setLclLogUserPoint("Detail screen function");
			//switchBLK 1000395 BLK ACT
			// DEBUG genFunctionCall 1000396 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.fields,CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "fields"));
			//switchBLK 1000401 BLK ACT
			// DEBUG genFunctionCall 1000402 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrValidateDetailScreenRelations(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Validate Detail Screen Relations (Generated:268)
			 */
			CreateLogFileDTO createLogFileDTO;
			//switchSUB 268 SUB    
			//switchBLK 1000092 BLK CAS
			//switchSUB 1000092 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// DTL.*CMD key is *Cancel
				//switchBLK 1000095 BLK ACT
				// DEBUG genFunctionCall 1000096 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000101 BLK ACT
				// DEBUG genFunctionCall 1000102 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
			//switchBLK 1000104 BLK TXT
			// 
			//switchBLK 1000026 BLK ACT
			// DEBUG genFunctionCall 1000027 ACT WRK.Patient Code = DTL.Patient Code
			dto.setWfPatientCode(dto.getPatientCode());
			//switchBLK 1000030 BLK ACT
			// DEBUG genFunctionCall 1000031 ACT WRK.Doctor Code = DTL.Doctor Code
			dto.setWfDoctorCode(dto.getDoctorCode());
			//switchBLK 1000448 BLK TXT
			// 
			//switchBLK 1000449 BLK ACT
			// DEBUG genFunctionCall 1000450 ACT LCL.Log Function Type = CON.EDTRCD
			dto.setLclLogFunctionType("EDTRCD");
			//switchBLK 1000453 BLK ACT
			// DEBUG genFunctionCall 1000454 ACT LCL.Log User Point = CON.Validate detail screen
			dto.setLclLogUserPoint("Validate detail screen");
			//switchBLK 1000457 BLK ACT
			// DEBUG genFunctionCall 1000458 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.relations,CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "relations"));
			//switchBLK 1000463 BLK ACT
			// DEBUG genFunctionCall 1000464 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrProcessKeyScreenRequest(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Process Key Screen Request (Generated:499)
			 */
			CreateLogFileDTO createLogFileDTO;
			RtvPatientNameDTO rtvPatientNameDTO;
			//switchSUB 499 SUB    
			//switchBLK 1000151 BLK ACT
			// DEBUG genFunctionCall 1000152 ACT RTV Patient Name - Patient  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvPatientNameDTO = new RtvPatientNameDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvPatientNameDTO.setPatientCode(dto.getPatientCode());
			// DEBUG genFunctionCall Service call
			rtvPatientNameService.execute(rtvPatientNameDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setLclPatientName(rtvPatientNameDTO.getPatientName());
			dto.setLclPatientSurname(rtvPatientNameDTO.getPatientSurname());
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000314 BLK TXT
			// 
			//switchBLK 1000315 BLK ACT
			// DEBUG genFunctionCall 1000316 ACT LCL.Log Function Type = CON.EDTRCD
			dto.setLclLogFunctionType("EDTRCD");
			//switchBLK 1000319 BLK ACT
			// DEBUG genFunctionCall 1000320 ACT LCL.Log User Point = CON.Process key screen
			dto.setLclLogUserPoint("Process key screen");
			//switchBLK 1000323 BLK ACT
			// DEBUG genFunctionCall 1000324 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.request,CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "request"));
			//switchBLK 1000329 BLK ACT
			// DEBUG genFunctionCall 1000330 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrProcessCommandKeys(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Process Command Keys (Generated:446)
			 */
			CreateLogFileDTO createLogFileDTO;
			//switchSUB 446 SUB    
			//switchBLK 1000105 BLK CAS
			//switchSUB 1000105 BLK CAS
			if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("14")) {
				// DTL.*CMD key is CF14
				//switchBLK 1000108 BLK ACT
				// DEBUG genFunctionCall 1000109 ACT TRN Edit Prescriptions - Prescription  *
				dto.setNextScreen("TrnEditPrescriptions");
				//switchBLK 1000113 BLK CAS
				//switchSUB 1000113 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000116 BLK ACT
					// DEBUG genFunctionCall 1000117 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000120 BLK ACT
					// DEBUG genFunctionCall 1000121 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000122 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000124 BLK ACT
					// DEBUG genFunctionCall 1000125 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
			//switchBLK 1000478 BLK TXT
			// 
			//switchBLK 1000479 BLK ACT
			// DEBUG genFunctionCall 1000480 ACT LCL.Log Function Type = CON.EDTRCD
			dto.setLclLogFunctionType("EDTRCD");
			//switchBLK 1000483 BLK ACT
			// DEBUG genFunctionCall 1000484 ACT LCL.Log User Point = CON.Process command keys
			dto.setLclLogUserPoint("Process command keys");
			//switchBLK 1000493 BLK ACT
			// DEBUG genFunctionCall 1000494 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private StepResult usrExitCommandProcessing(EdtrcdEditDiagnosisState dto)
    {
        StepResult result = NO_ACTION;

        try
        {
           /**
			 * USER: Exit Program Processing (Generated:64)
			 */
			CreateLogFileDTO createLogFileDTO;
			//switchSUB 64 SUB    
			//switchBLK 1000019 BLK CAS
			//switchSUB 1000019 BLK CAS
			if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03")) {
				// KEY.*CMD key is CF03
				//switchBLK 1000045 BLK ACT
				// DEBUG genFunctionCall 1000046 ACT PAR.*Return code = CND.*Normal
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000036 BLK ACT
				// DEBUG genFunctionCall 1000037 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				//switchBLK 1000090 BLK TXT
				// 
			}//switchSUB 1000022 SUB    
			 else {
				// *OTHERWISE
				//switchBLK 1000091 BLK TXT
				// 
				//switchBLK 1000024 BLK ACT
				// DEBUG genFunctionCall 1000025 ACT TRN Edit Prescriptions - Prescription  *
				dto.setNextScreen("TrnEditPrescriptions");
				//switchBLK 1000059 BLK CAS
				//switchSUB 1000059 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000062 BLK ACT
					// DEBUG genFunctionCall 1000063 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000068 BLK ACT
					// DEBUG genFunctionCall 1000069 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000070 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000072 BLK ACT
					// DEBUG genFunctionCall 1000073 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
			//switchBLK 1000500 BLK TXT
			// 
			//switchBLK 1000501 BLK ACT
			// DEBUG genFunctionCall 1000502 ACT LCL.Log Function Type = CON.EDTRCD
			dto.setLclLogFunctionType("EDTRCD");
			//switchBLK 1000505 BLK ACT
			// DEBUG genFunctionCall 1000506 ACT LCL.Log User Point = CON.Exit program processing
			dto.setLclLogUserPoint("Exit program processing");
			//switchBLK 1000515 BLK ACT
			// DEBUG genFunctionCall 1000516 ACT Create Log File - Log File  *
			// DEBUG genFunctionCall ServiceDtoVariable
			createLogFileDTO = new CreateLogFileDTO();
			// DEBUG genFunctionCall Parameters IN
			createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			createLogFileDTO.setLogFunctionType(dto.getLclLogFunctionType());
			createLogFileDTO.setLogUserPoint(dto.getLclLogUserPoint());
			// DEBUG genFunctionCall Service call
			createLogFileService.execute(createLogFileDTO);
			// DEBUG genFunctionCall Parameters OUT
			// DEBUG genFunctionCall Parameters DONE

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }
    
    private StepResult processPromptSelectDoctor(EdtrcdEditDiagnosisState state, SelectDoctorParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        result = conductDetailScreenConversation(state);

        return result;
    }




    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * DeleteDiagnosisService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceDeleteDiagnosis(EdtrcdEditDiagnosisState state, DeleteDiagnosisParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * CreateDiagnosisService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateDiagnosis(EdtrcdEditDiagnosisState state, CreateDiagnosisParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ChangeDiagnosisService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChangeDiagnosis(EdtrcdEditDiagnosisState state, ChangeDiagnosisParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(EdtrcdEditDiagnosisState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvForSupervisorService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvForSupervisor(EdtrcdEditDiagnosisState state, RtvForSupervisorParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * TrnEditPrescriptionsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceTrnEditPrescriptions(EdtrcdEditDiagnosisState state, TrnEditPrescriptionsParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(EdtrcdEditDiagnosisState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * QuitService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceQuit(EdtrcdEditDiagnosisState state, QuitParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * JobDateHaveChangedService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceJobDateHaveChanged(EdtrcdEditDiagnosisState state, JobDateHaveChangedParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvPatientNameService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvPatientName(EdtrcdEditDiagnosisState state, RtvPatientNameParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvDoctorNameService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvDoctorName(EdtrcdEditDiagnosisState state, RtvDoctorNameParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * CreateLogFileService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateLogFile(EdtrcdEditDiagnosisState state, CreateLogFileParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * ConcatService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceConcat(EdtrcdEditDiagnosisState state, ConcatParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//
}
