package com.hospital.file.diagnosis.edtrcdeditdiagnosis;
import java.io.Serializable;
import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.hospital.model.GlobalContext;
import com.hospital.common.state.BaseDTO;
import com.hospital.file.diagnosis.Diagnosis;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.ReturnCodeEnum;


/**
 * Dto for file 'Diagnosis' (TSAGCPP) and function 'EDTRCD Edit Diagnosis' (TSAXE1R).
 */
@Configurable
public class EdtrcdEditDiagnosisDTO extends BaseDTO implements Serializable {
    private static final long serialVersionUID = 8411799118125023554L;

    @Autowired
    private GlobalContext globalCtx;

    private String patientCode = "";
    private String patientName = "";
    private LocalDate diagnosisDate = null;
    private LocalTime diagnosisTime = null;
    private String sflselPromptText = "";
    private String doctorCode = "";
    private String doctorName = "";
    private String findings1 = "";
    private String findings2 = "";
    private String addedUser = "";
    private LocalDate addedDate = null;
    private LocalTime addedTime = null;
    private String changedUser = "";
    private LocalDate changedDate = null;
    private LocalTime changedTime = null;
    private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");

    // Local fields
    private String lclLogFunctionType = "";
    private String lclLogUserPoint = "";
    private String lclPatientName = "";
    private String lclPatientSurname = "";
    private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");

private boolean conductKeyScreenConversation = true;
private boolean conductDetailScreenConversation = true;

    public EdtrcdEditDiagnosisDTO() {

    }

    public EdtrcdEditDiagnosisDTO(Diagnosis diagnosis) {
        setDtoFields(diagnosis);
    }

    public String getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public LocalDate getDiagnosisDate() {
        return diagnosisDate;
    }

    public void setDiagnosisDate(LocalDate diagnosisDate) {
        this.diagnosisDate = diagnosisDate;
    }

    public LocalTime getDiagnosisTime() {
        return diagnosisTime;
    }

    public void setDiagnosisTime(LocalTime diagnosisTime) {
        this.diagnosisTime = diagnosisTime;
    }

    public String getSflselPromptText() {
        return sflselPromptText;
    }

    public void setSflselPromptText(String sflselPromptText) {
        this.sflselPromptText = sflselPromptText;
    }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getFindings1() {
        return findings1;
    }

    public void setFindings1(String findings1) {
        this.findings1 = findings1;
    }

    public String getFindings2() {
        return findings2;
    }

    public void setFindings2(String findings2) {
        this.findings2 = findings2;
    }

    public String getAddedUser() {
        return addedUser;
    }

    public void setAddedUser(String addedUser) {
        this.addedUser = addedUser;
    }

    public LocalDate getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(LocalDate addedDate) {
        this.addedDate = addedDate;
    }

    public LocalTime getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(LocalTime addedTime) {
        this.addedTime = addedTime;
    }

    public String getChangedUser() {
        return changedUser;
    }

    public void setChangedUser(String changedUser) {
        this.changedUser = changedUser;
    }

    public LocalDate getChangedDate() {
        return changedDate;
    }

    public void setChangedDate(LocalDate changedDate) {
        this.changedDate = changedDate;
    }

    public LocalTime getChangedTime() {
        return changedTime;
    }

    public void setChangedTime(LocalTime changedTime) {
        this.changedTime = changedTime;
    }

    public ReturnCodeEnum getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(ReturnCodeEnum returnCode) {
        this.returnCode = returnCode;
    }

    public void setWfDoctorCode(String doctorCode) {
        //globalCtx.setString("doctorCode", doctorCode);
    }

    public String getWfDoctorCode() {
        return "";
    }

    public void setWfPatientCode(String patientCode) {
       // globalCtx.setString("patientCode", patientCode);
    }

    public String getWfPatientCode() {
        return "";
    }

    public void setLclLogFunctionType(String logFunctionType) {
        this.lclLogFunctionType = logFunctionType;
    }

    public String getLclLogFunctionType() {
        return lclLogFunctionType;
    }

    public void setLclLogUserPoint(String logUserPoint) {
        this.lclLogUserPoint = logUserPoint;
    }

    public String getLclLogUserPoint() {
        return lclLogUserPoint;
    }

    public void setLclPatientName(String patientName) {
        this.lclPatientName = patientName;
    }

    public String getLclPatientName() {
        return lclPatientName;
    }

    public void setLclPatientSurname(String patientSurname) {
        this.lclPatientSurname = patientSurname;
    }

    public String getLclPatientSurname() {
        return lclPatientSurname;
    }

    public void setLclReturnCode(ReturnCodeEnum returnCode) {
        this.lclReturnCode = returnCode;
    }

    public ReturnCodeEnum getLclReturnCode() {
        return lclReturnCode;
    }

    /**
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param diagnosis Diagnosis Entity bean
     */
    public void setDtoFields(Diagnosis diagnosis) {
        BeanUtils.copyProperties(diagnosis, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param diagnosis Diagnosis Entity bean
     */
    public void setEntityFields(Diagnosis diagnosis) {
        BeanUtils.copyProperties(this, diagnosis);
    }

    public boolean getConductKeyScreenConversation() {
        return conductKeyScreenConversation;
    }

    public void setConductKeyScreenConversation(boolean conductKeyScreenConversation) {
        this.conductKeyScreenConversation = conductKeyScreenConversation;
    }

    public boolean getConductDetailScreenConversation() {
        return conductDetailScreenConversation;
    }

    public void setConductDetailScreenConversation(boolean conductDetailScreenConversation) {
        this.conductDetailScreenConversation = conductDetailScreenConversation;
    }

}
