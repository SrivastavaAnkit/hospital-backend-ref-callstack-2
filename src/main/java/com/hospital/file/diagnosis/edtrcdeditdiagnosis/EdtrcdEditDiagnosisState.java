package com.hospital.file.diagnosis.edtrcdeditdiagnosis;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.hospital.file.diagnosis.Diagnosis;
import com.hospital.model.GlobalContext;
import com.hospital.model.ProgramModeEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;

/**
 * State for file 'Diagnosis' (TSAGCPP) and function 'EDTRCD Edit Diagnosis' (TSAXE1R).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
@Configurable
public class EdtrcdEditDiagnosisState extends EdtrcdEditDiagnosisDTO implements Serializable {
    private static final long serialVersionUID = -8693842971040464887L;

    private GlobalContext globalCtx = new GlobalContext();


    // Local fields
    private String lclLogFunctionType = "";
    private String lclLogUserPoint = "";
    private String lclPatientName = "";
    private String lclPatientSurname = "";
    private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");

    // System fields
    private ProgramModeEnum _sysProgramMode = null;
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysConductKeyScreenConversation = true;
    private boolean _sysConductDetailScreenConversation = true;
    private boolean _sysErrorFound = false;

    public EdtrcdEditDiagnosisState() {
    }

    public EdtrcdEditDiagnosisState(Diagnosis diagnosis) {
        setDtoFields(diagnosis);
    }

    public void setWfDoctorCode(String doctorCode) {
        globalCtx.setString("doctorCode", doctorCode);
    }

    public String getWfDoctorCode() {
        return globalCtx.getString("doctorCode");
    }

    public void setWfPatientCode(String patientCode) {
        globalCtx.setString("patientCode", patientCode);
    }

    public String getWfPatientCode() {
        return globalCtx.getString("patientCode");
    }

    public void setLclLogFunctionType(String logFunctionType) {
        this.lclLogFunctionType = logFunctionType;
    }

    public String getLclLogFunctionType() {
        return lclLogFunctionType;
    }

    public void setLclLogUserPoint(String logUserPoint) {
        this.lclLogUserPoint = logUserPoint;
    }

    public String getLclLogUserPoint() {
        return lclLogUserPoint;
    }

    public void setLclPatientName(String patientName) {
        this.lclPatientName = patientName;
    }

    public String getLclPatientName() {
        return lclPatientName;
    }

    public void setLclPatientSurname(String patientSurname) {
        this.lclPatientSurname = patientSurname;
    }

    public String getLclPatientSurname() {
        return lclPatientSurname;
    }

    public void setLclReturnCode(ReturnCodeEnum returnCode) {
        this.lclReturnCode = returnCode;
    }

    public ReturnCodeEnum getLclReturnCode() {
        return lclReturnCode;
    }

   public void set_SysProgramMode(ProgramModeEnum programMode) {
       _sysProgramMode = programMode;
   }

   public ProgramModeEnum get_SysProgramMode() {
       return _sysProgramMode;
   }

   public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
       _sysReloadSubfile = reloadSubfile;
   }

   public ReloadSubfileEnum get_SysReloadSubfileEnum() {
       return _sysReloadSubfile;
   }

    public boolean get_SysConductKeyScreenConversation() {
        return _sysConductKeyScreenConversation;
    }

    public void set_SysConductKeyScreenConversation(boolean conductKeyScreenConversation) {
        _sysConductKeyScreenConversation = conductKeyScreenConversation;
    }

    public boolean get_SysConductDetailScreenConversation() {
        return _sysConductDetailScreenConversation;
    }

    public void set_SysConductDetailScreenConversation(boolean conductDetailScreenConversation) {
        _sysConductDetailScreenConversation = conductDetailScreenConversation;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
