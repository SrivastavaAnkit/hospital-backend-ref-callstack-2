package com.hospital.file.diagnosis.edtrcdeditdiagnosis;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.hospital.common.exception.ServiceException;

import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameService;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorService;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameService;
import com.hospital.file.prescription.trneditprescriptions.TrnEditPrescriptionsService;
import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameDTO;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorDTO;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameDTO;
import com.hospital.file.prescription.trneditprescriptions.TrnEditPrescriptionsDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ProgramModeEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.file.prescription.trneditprescriptions.TrnEditPrescriptionsParams;


/**
 * Spring Validator for file 'Diagnosis' (TSAGCPP) and function 'EDTRCD Edit Diagnosis' (TSAXE1R).
 *
 * @author X2EGenerator
 */
@Component
public class EdtrcdEditDiagnosisValidator implements Validator {
	
	
	@Autowired
	private CreateLogFileService createLogFileService;
	
	@Autowired
	private RtvDoctorNameService rtvDoctorNameService;
	
	@Autowired
	private RtvForSupervisorService rtvForSupervisorService;
	
	@Autowired
	private RtvPatientNameService rtvPatientNameService;
	

	@Override
	public boolean supports(Class<?> clazz) {
		return EdtrcdEditDiagnosisDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors e) {
		if (!(object instanceof EdtrcdEditDiagnosisDTO)) {
			e.reject("Not a valid EdtrcdEditDiagnosisDTO");
		} else {
			EdtrcdEditDiagnosisDTO dto = (EdtrcdEditDiagnosisDTO)object;

			/**
			 * Validate Key Fields
			 *
			 */
			if ("".equals(dto.getPatientCode())) {
				e.rejectValue("patientCode", "value.required");
			}
			if (dto.getDiagnosisDate() == null) {
				e.rejectValue("diagnosisDate", "value.required");
			}
			if (dto.getDiagnosisTime() == null) {
				e.rejectValue("diagnosisTime", "value.required");
			}
			EdtrcdEditDiagnosisParams params = new EdtrcdEditDiagnosisParams();
			BeanUtils.copyProperties(dto, params);
			try {
				/**
				 * USER: Validate Detail Screen Fields (Generated:502)
				 */
				CreateLogFileDTO createLogFileDTO;
				//switchSUB 502 SUB    
				//switchBLK 1000039 BLK CAS
				//switchSUB 1000039 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
					// DTL.*CMD key is *Cancel
					//switchBLK 1000051 BLK ACT
					//functionCall 1000052 ACT PAR.*Return code = CND.E
					dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
					//switchBLK 1000042 BLK ACT
					//functionCall 1000043 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
				//switchBLK 1000340 BLK TXT
				// 
				//switchBLK 1000341 BLK ACT
				//functionCall 1000342 ACT LCL.Log Function Type = CON.EDTRCD
				dto.setLclLogFunctionType("EDTRCD");
				//switchBLK 1000345 BLK ACT
				//functionCall 1000346 ACT LCL.Log User Point = CON.Validate detail screen
				dto.setLclLogUserPoint("Validate detail screen");
				//switchBLK 1000349 BLK ACT
				//functionCall 1000350 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.fields,CND.*One)
				dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "fields"));
				//switchBLK 1000355 BLK ACT
				//functionCall 1000356 ACT Create Log File - Log File  *
				createLogFileDTO = new CreateLogFileDTO();
				////createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				// TODO: Unsupported Action Diagram Context: LCL
				// TODO: Unsupported Action Diagram Context: LCL
				createLogFileService.execute(createLogFileDTO);
			} catch (Exception se) {
				se.printStackTrace();
			}
			try {
				/**
				 * USER: Validate Detail Screen Relations (Generated:268)
				 */
				CreateLogFileDTO createLogFileDTO;
				//switchSUB 268 SUB    
				//switchBLK 1000092 BLK CAS
				//switchSUB 1000092 BLK CAS
				if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
					// DTL.*CMD key is *Cancel
					//switchBLK 1000095 BLK ACT
					//functionCall 1000096 ACT PAR.*Return code = CND.E
					dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
					//switchBLK 1000101 BLK ACT
					//functionCall 1000102 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
				//switchBLK 1000104 BLK TXT
				// 
				//switchBLK 1000026 BLK ACT
				//functionCall 1000027 ACT WRK.Patient Code = DTL.Patient Code
				dto.setWfPatientCode(dto.getPatientCode());
				//switchBLK 1000030 BLK ACT
				//functionCall 1000031 ACT WRK.Doctor Code = DTL.Doctor Code
				dto.setWfDoctorCode(dto.getDoctorCode());
				//switchBLK 1000448 BLK TXT
				// 
				//switchBLK 1000449 BLK ACT
				//functionCall 1000450 ACT LCL.Log Function Type = CON.EDTRCD
				dto.setLclLogFunctionType("EDTRCD");
				//switchBLK 1000453 BLK ACT
				//functionCall 1000454 ACT LCL.Log User Point = CON.Validate detail screen
				dto.setLclLogUserPoint("Validate detail screen");
				//switchBLK 1000457 BLK ACT
				//functionCall 1000458 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.relations,CND.*One)
				dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "relations"));
				//switchBLK 1000463 BLK ACT
				//functionCall 1000464 ACT Create Log File - Log File  *
				createLogFileDTO = new CreateLogFileDTO();
				////createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
				// TODO: Unsupported Action Diagram Context: LCL
				// TODO: Unsupported Action Diagram Context: LCL
				createLogFileService.execute(createLogFileDTO);
			} catch (Exception se) {
				se.printStackTrace();
			}
		}
	}
}
