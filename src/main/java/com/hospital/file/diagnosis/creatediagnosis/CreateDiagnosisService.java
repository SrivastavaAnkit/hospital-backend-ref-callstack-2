package com.hospital.file.diagnosis.creatediagnosis;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.hospital.support.JobContext;
import com.hospital.file.diagnosis.Diagnosis;
import com.hospital.file.diagnosis.DiagnosisId;
import com.hospital.file.diagnosis.DiagnosisRepository;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.prescription.createprescription.CreatePrescriptionService;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.file.prescription.createprescription.CreatePrescriptionDTO;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.exception.ServiceException;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class CreateDiagnosisService  extends AbstractService<CreateDiagnosisService, CreateDiagnosisDTO>
{
    private final Step execute = define("execute", CreateDiagnosisDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private DiagnosisRepository diagnosisRepository;
	
	@Autowired
	private CreateLogFileService createLogFileService;
	
	@Autowired
	private CreatePrescriptionService createPrescriptionService;
	

    @Autowired
    public CreateDiagnosisService() {
        super(CreateDiagnosisService.class, CreateDiagnosisDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(CreateDiagnosisDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(CreateDiagnosisDTO dto, CreateDiagnosisDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		Diagnosis diagnosis = new Diagnosis();
		diagnosis.setPatientCode(dto.getPatientCode());
		diagnosis.setDiagnosisDate(dto.getDiagnosisDate());
		diagnosis.setDiagnosisTime(dto.getDiagnosisTime());
		diagnosis.setDoctorCode(dto.getDoctorCode());
		diagnosis.setFindings1(dto.getFindings1());
		diagnosis.setFindings2(dto.getFindings2());
		/*diagnosis.setAddedUser(dto.getAddedUser());
		diagnosis.setAddedDate(dto.getAddedDate());
		diagnosis.setAddedTime(dto.getAddedTime());
		diagnosis.setChangedUser(dto.getChangedUser());
		diagnosis.setChangedDate(dto.getChangedDate());
		diagnosis.setChangedTime(dto.getChangedTime());*/

		processingBeforeDataUpdate(dto, diagnosis);

		Diagnosis diagnosis2 = diagnosisRepository.findById(diagnosis.getId()).get();
		if (diagnosis2 != null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0003"));
			processingIfDataRecordAlreadyExists(dto, diagnosis2);
		}
		else {
            try {
				diagnosisRepository.save(diagnosis);
                dto.setReturnCode(ReturnCodeEnum.fromCode(""));
                processingAfterDataUpdate(dto, diagnosis);
            } catch (Exception e) {
                dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
                processingIfDataUpdateError(dto, diagnosis);
            }
        }

        return result;
	}

    private StepResult processingBeforeDataUpdate(CreateDiagnosisDTO dto, Diagnosis diagnosis) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 8 SUB    
		//switchBLK 1000023 BLK ACT
		//functionCall 1000024 ACT DB1.Added User = JOB.*USER
		/*diagnosis.setAddedUser(job.getUser());
		//switchBLK 1000030 BLK ACT
		//functionCall 1000031 ACT DB1.Added Date = JOB.*Job date
		diagnosis.setAddedDate(LocalDate.now());
		//switchBLK 1000017 BLK ACT
		//functionCall 1000018 ACT DB1.Added Time = JOB.*Job time
		diagnosis.setAddedTime(LocalTime.now());*/
		//switchBLK 1000060 BLK TXT
		// 
		//switchBLK 1000061 BLK ACT
		//functionCall 1000062 ACT LCL.Log Function Type = CON.CRTOBJ
		dto.setLclLogFunctionType("CRTOBJ");
		//switchBLK 1000065 BLK ACT
		//functionCall 1000066 ACT LCL.Log User Point = CON.Processing before Data
		dto.setLclLogUserPoint("Processing before Data");
		//switchBLK 1000069 BLK ACT
		//functionCall 1000070 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.update,CND.*One)
		dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "update"));
		//switchBLK 1000075 BLK ACT
		//functionCall 1000076 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
       return NO_ACTION;
    }

    private StepResult processingIfDataRecordAlreadyExists(CreateDiagnosisDTO dto, Diagnosis diagnosis) throws ServiceException {
		/**
		 * USER: Processing if Data Record Already Exists (Generated:43)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 43 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT LCL.USR Return Code = CND.Record found
		dto.setLclUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
		//switchBLK 1000106 BLK TXT
		// 
		//switchBLK 1000107 BLK ACT
		//functionCall 1000108 ACT LCL.Log Function Type = CON.CRTOBJ
		dto.setLclLogFunctionType("CRTOBJ");
		//switchBLK 1000111 BLK ACT
		//functionCall 1000112 ACT LCL.Log User Point = CON.Processing if Data record
		dto.setLclLogUserPoint("Processing if Data record");
		//switchBLK 1000115 BLK ACT
		//functionCall 1000116 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.already exist,CND.*One)
		dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "already exist"));
		//switchBLK 1000121 BLK ACT
		//functionCall 1000122 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
       return NO_ACTION;
    }

    private StepResult processingAfterDataUpdate(CreateDiagnosisDTO dto, Diagnosis diagnosis) throws ServiceException {
		/**
		 * USER: Processing after Data Update (Generated:11)
		 */
		CreateLogFileDTO createLogFileDTO;
		CreatePrescriptionDTO createPrescriptionDTO;
		//switchSUB 11 SUB    
		//switchBLK 1000037 BLK ACT
		//functionCall 1000038 ACT Create Prescription - Prescription  *
		createPrescriptionDTO = new CreatePrescriptionDTO();
		createPrescriptionDTO.setDoctorCode(diagnosis.getDoctorCode());
		// TODO: Unsupported Action Diagram Context: CND
		/*createPrescriptionDTO.setPrescriptionDate(job.getJobDate());
		createPrescriptionDTO.setPrescriptionTime(job.getJobTime());*/
		createPrescriptionDTO.setPatientCode(diagnosis.getPatientCode());
		createPrescriptionDTO.setDiagnosisDate(diagnosis.getDiagnosisDate());
		createPrescriptionDTO.setDiagnosisTime(diagnosis.getDiagnosisTime());
		// TODO: Unsupported Action Diagram Context: CON
		createPrescriptionService.execute(createPrescriptionDTO);
		dto.setUsrReturnCode(createPrescriptionDTO.getUsrReturnCode());
		//switchBLK 1000158 BLK TXT
		// 
		//switchBLK 1000159 BLK ACT
		//functionCall 1000160 ACT LCL.Log Function Type = CON.CRTOBJ
		dto.setLclLogFunctionType("CRTOBJ");
		//switchBLK 1000163 BLK ACT
		//functionCall 1000164 ACT LCL.Log User Point = CON.Processing after Data
		dto.setLclLogUserPoint("Processing after Data");
		//switchBLK 1000167 BLK ACT
		//functionCall 1000168 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.update,CND.*One)
		dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "update"));
		//switchBLK 1000173 BLK ACT
		//functionCall 1000174 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
       return NO_ACTION;
    }

    private StepResult processingIfDataUpdateError(CreateDiagnosisDTO dto, Diagnosis diagnosis) throws ServiceException {
		/**
		 * Processing if Data Update Error (Generated:48)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 48 SUB    
		//switchBLK 1000013 BLK ACT
		//functionCall 1000014 ACT LCL.USR Return Code = CND.Record Not found
		dto.setLclUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
		//switchBLK 1000132 BLK TXT
		// 
		//switchBLK 1000133 BLK ACT
		//functionCall 1000134 ACT LCL.Log Function Type = CON.CRTOBJ
		dto.setLclLogFunctionType("CRTOBJ");
		//switchBLK 1000137 BLK ACT
		//functionCall 1000138 ACT LCL.Log User Point = CON.Processing if Data update
		dto.setLclLogUserPoint("Processing if Data update");
		//switchBLK 1000141 BLK ACT
		//functionCall 1000142 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.error,CND.*One)
		dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "error"));
		//switchBLK 1000147 BLK ACT
		//functionCall 1000148 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
       return NO_ACTION;
    }
}
