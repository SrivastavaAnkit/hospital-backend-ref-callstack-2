package com.hospital.file.diagnosis.creatediagnosis;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class CreateDiagnosisDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private LocalDate addedDate;
	private LocalDate changedDate;
	private LocalDate diagnosisDate;
	private LocalTime addedTime;
	private LocalTime changedTime;
	private LocalTime diagnosisTime;
	private ReturnCodeEnum returnCode;
	private String addedUser;
	private String changedUser;
	private String doctorCode;
	private String findings1;
	private String findings2;
	private String patientCode;
	private UsrReturnCodeEnum usrReturnCode;
	private String nextScreen;
	private String lclLogFunctionType;
	private String lclLogUserPoint;
	private UsrReturnCodeEnum lclUsrReturnCode;

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public LocalDate getDiagnosisDate() {
		return diagnosisDate;
	}

	public LocalTime getDiagnosisTime() {
		return diagnosisTime;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public String getFindings1() {
		return findings1;
	}

	public String getFindings2() {
		return findings2;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public UsrReturnCodeEnum getUsrReturnCode() {
		return usrReturnCode;
	}

	public String getLclLogFunctionType() {
		return lclLogFunctionType;
	}

	public String getLclLogUserPoint() {
		return lclLogUserPoint;
	}

	public UsrReturnCodeEnum getLclUsrReturnCode() {
		return lclUsrReturnCode;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public void setDiagnosisDate(LocalDate diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}

	public void setDiagnosisTime(LocalTime diagnosisTime) {
		this.diagnosisTime = diagnosisTime;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public void setFindings1(String findings1) {
		this.findings1 = findings1;
	}

	public void setFindings2(String findings2) {
		this.findings2 = findings2;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public void setUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
		this.usrReturnCode = usrReturnCode;
	}

	public void setLclLogFunctionType(String lclLogFunctionType) {
		this.lclLogFunctionType = lclLogFunctionType;
	}

	public void setLclLogUserPoint(String lclLogUserPoint) {
		this.lclLogUserPoint = lclLogUserPoint;
	}

	public void setLclUsrReturnCode(UsrReturnCodeEnum lclUsrReturnCode) {
		this.lclUsrReturnCode = lclUsrReturnCode;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
