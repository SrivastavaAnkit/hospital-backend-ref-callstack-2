package com.hospital.file.diagnosis.selselectdiagnosis;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.data.domain.Page;

import com.hospital.common.state.BaseDTO;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;



/**
 * Dto for file 'Diagnosis' (TSAGCPP) and function 'SEL Select Diagnosis' (TSA0SRR).
 */
public class SelSelectDiagnosisDTO extends BaseDTO implements Serializable {
	private static final long serialVersionUID = 8208309679663629164L;

	private String patientCode = "";
	private String patientName = "";
	private LocalDate diagnosisDate = LocalDate.of(1801, 1, 1);
	private LocalTime diagnosisTime = LocalTime.of(0, 0);
	private Page<SelSelectDiagnosisGDO> pageDto;
	private int page = 0;
    private int size = 10;
    private String sortData = "";
    private boolean confirm = false;
	// Local fields
	private String lclLogFunctionType = "";
	private String lclLogUserPoint = "";

	private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");
	private ReloadSubfileEnum reloadSubfile = ReloadSubfileEnum.fromCode("Y");
	private long scanLimit = 0;

	public SelSelectDiagnosisDTO() {
	}

	
	
    public int getPage() {
		return page;
	}



	public void setPage(int page) {
		this.page = page;
	}



	public int getSize() {
		return size;
	}



	public void setSize(int size) {
		this.size = size;
	}



	public String getSortData() {
		return sortData;
	}



	public void setSortData(String sortData) {
		this.sortData = sortData;
	}



	public boolean isConfirm() {
		return confirm;
	}



	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}



	public SelSelectDiagnosisDTO(String patientCode) {
        this.patientCode = patientCode;    }

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setDiagnosisDate(LocalDate diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}

	public LocalDate getDiagnosisDate() {
		return diagnosisDate;
	}

	public void setDiagnosisTime(LocalTime diagnosisTime) {
		this.diagnosisTime = diagnosisTime;
	}

	public LocalTime getDiagnosisTime() {
		return diagnosisTime;
	}

	public void setLclLogFunctionType(String logFunctionType) {
		this.lclLogFunctionType = logFunctionType;
	}

	public String getLclLogFunctionType() {
		return lclLogFunctionType;
	}

	public void setLclLogUserPoint(String logUserPoint) {
		this.lclLogUserPoint = logUserPoint;
	}

	public String getLclLogUserPoint() {
		return lclLogUserPoint;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setReloadSubfile(ReloadSubfileEnum reloadSubfile) {
		this.reloadSubfile = reloadSubfile;
	}

	public ReloadSubfileEnum getReloadSubfile() {
		return reloadSubfile;
	}

	public void setScanLimit(long scanLimit) {
		this.scanLimit = scanLimit;
	}

	public int getScanLimit() {
		return (int) scanLimit;
	}

	public Page<SelSelectDiagnosisGDO> getPageDto() {
		return pageDto;
	}

	public void setPageDto(Page<SelSelectDiagnosisGDO> pageDto) {
		this.pageDto = pageDto;
	}
	
	

}
