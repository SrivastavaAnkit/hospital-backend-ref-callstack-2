 package com.hospital.file.diagnosis.selselectdiagnosis;

import java.io.Serializable;

import java.time.LocalDate;
import java.time.LocalTime;


/**
 * Gdo for file 'Diagnosis' (TSAGCPP) and function 'SEL Select Diagnosis' (TSA0SRR).
 */
public class SelSelectDiagnosisGDO implements Serializable {
	private static final long serialVersionUID = 3995912766128998633L;

	private String gdo_selected = "";
	private String patientCode = "";
	private LocalDate diagnosisDate = null;
	private LocalTime diagnosisTime = null;
	private String doctorCode = "";
	private String doctorName = "";
	private String findings1 = "";
	private String findings2 = "";
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;

	public SelSelectDiagnosisGDO() {

	}

	public SelSelectDiagnosisGDO(String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime, String doctorCode, String findings1, String findings2, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime) {
		this.patientCode = patientCode;
		this.diagnosisDate = diagnosisDate;
		this.diagnosisTime = diagnosisTime;
		this.doctorCode = doctorCode;
		this.findings1 = findings1;
		this.findings2 = findings2;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}

	public void set_Selected(String gdo_selected) {
		this.gdo_selected = gdo_selected;
	}

	public String get_Selected() {
		return gdo_selected;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public void setDiagnosisDate(LocalDate diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}

	public LocalDate getDiagnosisDate() {
		return diagnosisDate;
	}

	public void setDiagnosisTime(LocalTime diagnosisTime) {
		this.diagnosisTime = diagnosisTime;
	}

	public LocalTime getDiagnosisTime() {
		return diagnosisTime;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setFindings1(String findings1) {
		this.findings1 = findings1;
	}

	public String getFindings1() {
		return findings1;
	}

	public void setFindings2(String findings2) {
		this.findings2 = findings2;
	}

	public String getFindings2() {
		return findings2;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

}
