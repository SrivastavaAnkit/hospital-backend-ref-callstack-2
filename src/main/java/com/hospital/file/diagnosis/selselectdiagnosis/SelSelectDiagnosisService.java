package com.hospital.file.diagnosis.selselectdiagnosis;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

import java.io.IOException;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.hospital.file.logfile.LogFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;

import com.hospital.file.diagnosis.DiagnosisRepository;

import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameService;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsService;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameService;
import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameDTO;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsDTO;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsParams;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.RecordSelectedEnum;
import com.hospital.model.ReloadSubfileEnum;


/**
 * Service for file 'Diagnosis' (TSAGCPP) and function 'SEL Select Diagnosis' (TSA0SRR).
 */
@Service
public class SelSelectDiagnosisService extends AbstractService<SelSelectDiagnosisService,SelSelectDiagnosisDTO> {

	@Autowired
	private DiagnosisRepository diagnosisRepository;

	
	@Autowired
	private CreateLogFileService createLogFileService;
	
	@Autowired
	private RtvDoctorNameService rtvDoctorNameService;
	
	@Autowired
	private RtvPatientNameService rtvPatientNameService;
	

	public static final String SCREEN_KEY = "com.hospital.file.diagnosis.selselectdiagnosis.key";

	private final Step execute = define("execute", SelSelectDiagnosisParams.class, this::execute);
	private final Step response = define("response", SelSelectDiagnosisDTO.class, this::processResponse);
        //private final Step serviceDspAllHospitals = define("serviceDspAllHospitals",DspAllHospitalsParams.class, this::processServiceDspAllHospitals);


	@Autowired
	public SelSelectDiagnosisService()
	{
		super(SelSelectDiagnosisService.class, SelSelectDiagnosisDTO.class);
	}

	@Override
	public Step getInitialStep()
	{
		return execute;
	}

    /**
     * SelSelectDiagnosis service starting point.
     * @param dto   - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
	private StepResult execute(SelSelectDiagnosisDTO dto, SelSelectDiagnosisParams params)
	{
        StepResult result = NO_ACTION;

		BeanUtils.copyProperties(params, dto);
		usrInitializeProgram(dto);
		result =  mainLoop(dto);

        return result;
	}

    /**
     * SCREEN_KEY initial processing loop method.
     * @param dto - Service state class.
     * @return
     */
	private StepResult mainLoop(SelSelectDiagnosisDTO dto)
	{
        StepResult result = NO_ACTION;

		dbfReadFirstDataRecord(dto);
		if (dto.getPageDto() != null && dto.getPageDto().getSize() > 0)
		{
			loadNextSubfilePage(dto);
		}
		dto.setReloadSubfile(ReloadSubfileEnum._STA_NO);
		result = conductScreenConversation(dto);

        return result;
	}

    /**
     * SCREEN_KEY display processing loop method.
     * @param dto - Service state class.
     * @return
     */
	private StepResult conductScreenConversation(SelSelectDiagnosisDTO dto)
	{
		StepResult result = NO_ACTION;

		if(dto.getReloadSubfile().equals(ReloadSubfileEnum._STA_NO)){
			result = callScreen(SCREEN_KEY, dto).thenCall(response);
		}

		return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param dto - Service state class.
     * @param model - returned screen model.
     * @return
     */
	private StepResult processResponse(SelSelectDiagnosisDTO dto, SelSelectDiagnosisDTO fromScreen)
	{
        StepResult result = NO_ACTION;

		BeanUtils.copyProperties(fromScreen, dto);
		if(CmdKeyEnum.isExit(dto.get_SysCmdKey()))
		{
			result = closedown(dto);
            return result;
		}
		else if(CmdKeyEnum.isReset(dto.get_SysCmdKey())) {
			//TODO: processResetRequest(dto);//synon built-in function
		}
		else if(CmdKeyEnum.isHelp(dto.get_SysCmdKey())) {
			//TODO:processHelpRequest(dto);//synon built-in function
		}
		else if(CmdKeyEnum.isNextPage(dto.get_SysCmdKey())) {
			dbfReadNextDataRecord(dto);
			loadNextSubfilePage(dto);
		}
		else {
			usrProcessSubfileControl(dto);
			//TODO:readFirstChangedSubfileRecord(dto);//synon built-in function
			while (!dto.getReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {//TODO:while(Changed subfile record found)
				if(dto.getReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode()))//TODO:if(subfile select (Record Context) is equal to *Select request)
				{
					usrProcessSelectedLine(dto);
					//PAR.*ALL = RCD.*ALL // Move all fields of the selected record on the screen to the parameter
					//*EXIT PROGRAM
                    return result;
				}
				usrProcessChangedSubfileRecord(dto);
				usrScreenFunctionFields(dto);
				//TODO:updateSubfileRecord(dto);//synon built-in function
				//TODO:readNextChangedSubfileRecord(dto);//synon built-in function
			}
			if(!dto.getReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) //TODO:if(positioning field values have changed)
			{
				dto.setReloadSubfile(ReloadSubfileEnum._STA_YES);
			}
			usrProcessCommandKeys(dto);
        }
        result = conductScreenConversation(dto);

        return result;
	}

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param dto - Service state class.
     * @return
     */
	private void loadNextSubfilePage(SelSelectDiagnosisDTO dto)
	{
		for (SelSelectDiagnosisGDO gdo : ((Page<SelSelectDiagnosisGDO>) dto.getPageDto()).getContent())
		{
           /* dto.setRecordSelect(RecordSelectedEnum._STA_YES);
			//TODO:moveDbfRecordFieldsToSubfileRecord(dto);//synon built-in function
			usrScreenFunctionFields(dto);
			usrLoadSubfileRecordFromDbfRecord(dto, gdo);
            if(dto.getRecordSelect().getCode().equals(RecordSelectedEnum._STA_YES.getCode()))
            {
                //TODO:writeSubfileRecord(dto);//synon built-in function
            }*/
		}
	}

    /**
     * Terminate this program
     * @param dto - Service state class.
     * @return
     */
	private StepResult closedown(SelSelectDiagnosisDTO dto)
	{
        StepResult result = NO_ACTION;

		usrExitProgramProcessing(dto);
		//exitProgram

		return result;
	}

    /**
     * ------------------------- Generated DBF method ---------------------------
     */

    /**
     * Read data of the first page
     * @param dto - Service state class.
     * @return
     */
	private void dbfReadFirstDataRecord(SelSelectDiagnosisDTO dto)
	{
		//dto.setMessage(dto);
		dbfReadDataRecord(dto);
	}

    /**
     * Read data of the next page
     * @param dto - Service state class.
     * @return
     */
	private void dbfReadNextDataRecord(SelSelectDiagnosisDTO dto)
	{
		dto.setPage(dto.getPage() + 1);
		dbfReadDataRecord(dto);
	}

    /**
     * Read data of the actual page
     * @param dto - Service state class.
     * @return
     */
	private void dbfReadDataRecord(SelSelectDiagnosisDTO dto)
	{
		List<Order> sortOrders = new ArrayList<Order>();
		Pageable pageable;

		try
		{
			@SuppressWarnings("unchecked")
			Map<String, String> sortDataMap = new ObjectMapper().readValue(dto.getSortData(), LinkedHashMap.class);

			for (Map.Entry<String, String> entry : sortDataMap.entrySet())
			{
 				if (entry.getValue() == null)
				{
  					continue;
				}

				sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
			}
		}
		catch (IOException ioe)
		{
		}

		if (CollectionUtils.isEmpty(sortOrders))
		{
			pageable = new PageRequest(dto.getPage(), dto.getSize());
		}
		else
		{
			pageable = new PageRequest(dto.getPage(), dto.getSize(), new Sort(sortOrders));
		}

		RestResponsePage<SelSelectDiagnosisGDO> pageDto = (RestResponsePage<SelSelectDiagnosisGDO>) diagnosisRepository.selSelectDiagnosis(dto.getPatientCode(),dto.getDiagnosisDate(),dto.getDiagnosisTime(), pageable);
		dto.setPageDto(pageDto);
	}

    /**
     * ---------------------- Programmatic user-point --------------------------
     */

	/**
	 * USER: Initialize Program (Generated:20)
	 */
	private StepResult usrInitializeProgram(SelSelectDiagnosisDTO dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			RtvPatientNameDTO rtvPatientNameDTO;
			//switchSUB 20 SUB    
			//switchBLK 1000001 BLK ACT
			//functionCall 1000002 ACT PGM.*Scan limit = CND.High Value
			dto.setScanLimit(9999999);
			//switchBLK 1000007 BLK ACT
			//functionCall 1000008 ACT RTV Patient Name - Patient  *
			rtvPatientNameDTO = new RtvPatientNameDTO();
			rtvPatientNameDTO.setPatientCode(dto.getPatientCode());
			rtvPatientNameService.execute(rtvPatientNameDTO);
			dto.setPatientName(rtvPatientNameDTO.getPatientName());
			//switchBLK 1000059 BLK TXT
			// 
			//switchBLK 1000060 BLK ACT
			//functionCall 1000061 ACT LCL.Log Function Type = CON.SELRCD
			dto.setLclLogFunctionType("SELRCD");
			//switchBLK 1000064 BLK ACT
			//functionCall 1000065 ACT LCL.Log User Point = CON.Initialize program
			dto.setLclLogUserPoint("Initialize program");
			//switchBLK 1000074 BLK ACT
			//functionCall 1000075 ACT Create Log File - Log File  *
			createLogFileDTO = new CreateLogFileDTO();
			////createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			// TODO: Unsupported Action Diagram Context: LCL
			// TODO: Unsupported Action Diagram Context: LCL
			createLogFileService.execute(createLogFileDTO);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Process Subfile Control (Generated:72)
	 */
	private StepResult usrProcessSubfileControl(SelSelectDiagnosisDTO dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			RtvPatientNameDTO rtvPatientNameDTO;
			//switchSUB 72 SUB    
			//switchBLK 1000022 BLK ACT
			//functionCall 1000023 ACT RTV Patient Name - Patient  *
			rtvPatientNameDTO = new RtvPatientNameDTO();
			rtvPatientNameDTO.setPatientCode(dto.getPatientCode());
			rtvPatientNameService.execute(rtvPatientNameDTO);
			dto.setPatientName(rtvPatientNameDTO.getPatientName());
			//switchBLK 1000174 BLK TXT
			// 
			//switchBLK 1000175 BLK ACT
			//functionCall 1000176 ACT LCL.Log Function Type = CON.SELRCD
			dto.setLclLogFunctionType("SELRCD");
			//switchBLK 1000179 BLK ACT
			//functionCall 1000180 ACT LCL.Log User Point = CON.Process subfile control
			dto.setLclLogUserPoint("Process subfile control");
			//switchBLK 1000189 BLK ACT
			//functionCall 1000190 ACT Create Log File - Log File  *
			createLogFileDTO = new CreateLogFileDTO();
			////createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			// TODO: Unsupported Action Diagram Context: LCL
			// TODO: Unsupported Action Diagram Context: LCL
			createLogFileService.execute(createLogFileDTO);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Process Selected Line (Generated:107)
	 */
	private StepResult usrProcessSelectedLine(SelSelectDiagnosisDTO dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
		    SelSelectDiagnosisGDO gdo=new SelSelectDiagnosisGDO();
		    BeanUtils.copyProperties(dto, gdo);
			//switchSUB 107 SUB    
			//switchBLK 1000038 BLK CAS
			//switchSUB 1000038 BLK CAS
			if ((gdo.get_Selected().equals("*Select#1")) || (gdo.get_Selected().equals("*Select#2"))) {
				// RCD.*SFLSEL is *Select
				//switchBLK 1000047 BLK ACT
				//functionCall 1000048 ACT PAR.Diagnosis Date = RCD.Diagnosis Date
				dto.setDiagnosisDate(gdo.getDiagnosisDate());
				//switchBLK 1000041 BLK ACT
				//functionCall 1000042 ACT PAR.Diagnosis Time = RCD.Diagnosis Time
				dto.setDiagnosisTime(gdo.getDiagnosisTime());
			}
			//switchBLK 1000196 BLK TXT
			// 
			//switchBLK 1000197 BLK ACT
			//functionCall 1000198 ACT LCL.Log Function Type = CON.SELRCD
			dto.setLclLogFunctionType("SELRCD");
			//switchBLK 1000201 BLK ACT
			//functionCall 1000202 ACT LCL.Log User Point = CON.Process selected line
			dto.setLclLogUserPoint("Process selected line");
			//switchBLK 1000211 BLK ACT
			//functionCall 1000212 ACT Create Log File - Log File  *
			createLogFileDTO = new CreateLogFileDTO();
			////createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			// TODO: Unsupported Action Diagram Context: LCL
			// TODO: Unsupported Action Diagram Context: LCL
			createLogFileService.execute(createLogFileDTO);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Process Changed Subfile Record (Generated:101)
	 */
	private StepResult usrProcessChangedSubfileRecord(SelSelectDiagnosisDTO dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			//switchSUB 101 SUB    
			//switchBLK 1000053 BLK ACT
			//functionCall 1000054 ACT PGM.*Reload subfile = CND.*YES
			dto.setReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
			//switchBLK 1000218 BLK TXT
			// 
			//switchBLK 1000219 BLK ACT
			//functionCall 1000220 ACT LCL.Log Function Type = CON.SELRCD
			dto.setLclLogFunctionType("SELRCD");
			//switchBLK 1000223 BLK ACT
			//functionCall 1000224 ACT LCL.Log User Point = CON.Process changed subfile
			dto.setLclLogUserPoint("Process changed subfile");
			//switchBLK 1000227 BLK ACT
			//functionCall 1000228 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.record,CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "record"));
			//switchBLK 1000233 BLK ACT
			//functionCall 1000234 ACT Create Log File - Log File  *
			createLogFileDTO = new CreateLogFileDTO();
			////createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			// TODO: Unsupported Action Diagram Context: LCL
			// TODO: Unsupported Action Diagram Context: LCL
			createLogFileService.execute(createLogFileDTO);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * CALC: Screen Function Fields (Generated:165)
	 */
	private StepResult usrScreenFunctionFields(SelSelectDiagnosisDTO dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			RtvDoctorNameDTO rtvDoctorNameDTO;
			//switchSUB 165 SUB    
			//switchBLK 1000027 BLK ACT
			//functionCall 1000028 ACT RTV Doctor Name - Doctor  *
			SelSelectDiagnosisGDO gdo=new SelSelectDiagnosisGDO();
		    BeanUtils.copyProperties(dto, gdo);
			rtvDoctorNameDTO = new RtvDoctorNameDTO();
			rtvDoctorNameDTO.setDoctorCode(gdo.getDoctorCode());
			rtvDoctorNameService.execute(rtvDoctorNameDTO);
			gdo.setDoctorName(rtvDoctorNameDTO.getDoctorName());
			//switchBLK 1000244 BLK TXT
			// 
			//switchBLK 1000245 BLK ACT
			//functionCall 1000246 ACT LCL.Log Function Type = CON.SELRCD
			dto.setLclLogFunctionType("SELRCD");
			//switchBLK 1000249 BLK ACT
			//functionCall 1000250 ACT LCL.Log User Point = CON.Screen function fields
			dto.setLclLogUserPoint("Screen function fields");
			//switchBLK 1000259 BLK ACT
			//functionCall 1000260 ACT Create Log File - Log File  *
			createLogFileDTO = new CreateLogFileDTO();
			////createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			// TODO: Unsupported Action Diagram Context: LCL
			// TODO: Unsupported Action Diagram Context: LCL
			createLogFileService.execute(createLogFileDTO);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Process Command Keys (Generated:143)
	 */
	private StepResult usrProcessCommandKeys(SelSelectDiagnosisDTO dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			//switchSUB 143 SUB    
			//switchBLK 1000033 BLK CAS
			//switchSUB 1000033 BLK CAS
			if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("15")) {
				// CTL.*CMD key is CF15
				//switchBLK 1000036 BLK ACT
				//functionCall 1000037 ACT DSP All Hospitals - Hospital  *
				//TODO: split
				DspAllHospitalsParams dspAllHospitalsParams = new DspAllHospitalsParams();
				BeanUtils.copyProperties(dto, dspAllHospitalsParams);
				//result = StepResult.callService(DspAllHospitalsService.class, dspAllHospitalsParams);//.thenCall(serviceDspAllHospitalsDisplay)
			}
			//switchBLK 1000266 BLK TXT
			// 
			//switchBLK 1000267 BLK ACT
			//functionCall 1000268 ACT LCL.Log Function Type = CON.SELRCD
			dto.setLclLogFunctionType("SELRCD");
			//switchBLK 1000271 BLK ACT
			//functionCall 1000272 ACT LCL.Log User Point = CON.Process command keys
			dto.setLclLogUserPoint("Process command keys");
			//switchBLK 1000281 BLK ACT
			//functionCall 1000282 ACT Create Log File - Log File  *
			createLogFileDTO = new CreateLogFileDTO();
			////createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			// TODO: Unsupported Action Diagram Context: LCL
			// TODO: Unsupported Action Diagram Context: LCL
			createLogFileService.execute(createLogFileDTO);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Load Subfile Record from DBF Record (Generated:41)
	 */
	private StepResult usrLoadSubfileRecordFromDbfRecord(SelSelectDiagnosisDTO dto, SelSelectDiagnosisGDO gdo)
	{
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			RtvDoctorNameDTO rtvDoctorNameDTO;
			//switchSUB 41 SUB    
			//switchBLK 1000012 BLK ACT
			//functionCall 1000013 ACT RTV Doctor Name - Doctor  *
			rtvDoctorNameDTO = new RtvDoctorNameDTO();
			rtvDoctorNameDTO.setDoctorCode(gdo.getDoctorCode());
			rtvDoctorNameService.execute(rtvDoctorNameDTO);
			gdo.setDoctorName(rtvDoctorNameDTO.getDoctorName());
			//switchBLK 1000126 BLK TXT
			// 
			//switchBLK 1000127 BLK ACT
			//functionCall 1000128 ACT LCL.Log Function Type = CON.SELRCD
			dto.setLclLogFunctionType("SELRCD");
			//switchBLK 1000131 BLK ACT
			//functionCall 1000132 ACT LCL.Log User Point = CON.Load subfile record
			dto.setLclLogUserPoint("Load subfile record");
			//switchBLK 1000135 BLK ACT
			//functionCall 1000136 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.from DBF record,CND.*One)
			dto.setLclLogUserPoint(String.format("%s%1s%s", dto.getLclLogUserPoint().trim(), "", "from DBF record"));
			//switchBLK 1000141 BLK ACT
			//functionCall 1000142 ACT Create Log File - Log File  *
			createLogFileDTO = new CreateLogFileDTO();
			////createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			// TODO: Unsupported Action Diagram Context: LCL
			// TODO: Unsupported Action Diagram Context: LCL
			createLogFileService.execute(createLogFileDTO);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
	private StepResult usrExitProgramProcessing(SelSelectDiagnosisDTO dto)
	{
        StepResult result = NO_ACTION;

        try
        {
		    CreateLogFileDTO createLogFileDTO;
			//switchSUB 132 SUB    
			//switchBLK 1000016 BLK ACT
			//functionCall 1000017 ACT PGM.*Return code = CND.*Normal
			dto.setReturnCode(ReturnCodeEnum.fromCode(""));
			//switchBLK 1000152 BLK TXT
			// 
			//switchBLK 1000153 BLK ACT
			//functionCall 1000154 ACT LCL.Log Function Type = CON.SELRCD
			dto.setLclLogFunctionType("SELRCD");
			//switchBLK 1000157 BLK ACT
			//functionCall 1000158 ACT LCL.Log User Point = CON.Exit program processing
			dto.setLclLogUserPoint("Exit program processing");
			//switchBLK 1000167 BLK ACT
			//functionCall 1000168 ACT Create Log File - Log File  *
			createLogFileDTO = new CreateLogFileDTO();
			////createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
			// TODO: Unsupported Action Diagram Context: LCL
			// TODO: Unsupported Action Diagram Context: LCL
			createLogFileService.execute(createLogFileDTO);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

		return result;
	}

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * DspAllHospitalsService returned response processing method.
//     * @param dto - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceDspAllHospitals(SelSelectDiagnosisDTO dto, DspAllHospitalsParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        return result;
//    }
//

}
