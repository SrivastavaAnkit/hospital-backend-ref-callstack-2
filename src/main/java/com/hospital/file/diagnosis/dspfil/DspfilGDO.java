package com.hospital.file.diagnosis.dspfil;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;



/**
 * Gdo for file 'Diagnosis' (TSAGCPP) and function 'DSPFIL' (TSATDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */
public class DspfilGDO implements Serializable {
	private static final long serialVersionUID = -8865767571262783419L;

	private long version = 0;
    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private String patientCode = "";
	private LocalDate diagnosisDate = null;
	private LocalTime diagnosisTime = null;
	private String doctorCode = "";
	private String doctorName = "";
	private String findings1 = "";
	private String findings2 = "";
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;

	public DspfilGDO() {

	}

   	public DspfilGDO(/*long version,*/ String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime, String doctorCode, String findings1, String findings2/*, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime*/) {
		this.version = version;
		this.patientCode = patientCode;
		this.diagnosisDate = diagnosisDate;
		this.diagnosisTime = diagnosisTime;
		this.doctorCode = doctorCode;
		this.findings1 = findings1;
		this.findings2 = findings2;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}


	public void setVersion(long version) {
		this.version = version;
	}

    public long getVersion() {
		return version;
    }

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setPatientCode(String patientCode) {
    	this.patientCode = patientCode;
    }

	public String getPatientCode() {
		return patientCode;
	}

	public void setDiagnosisDate(LocalDate diagnosisDate) {
    	this.diagnosisDate = diagnosisDate;
    }

	public LocalDate getDiagnosisDate() {
		return diagnosisDate;
	}

	public void setDiagnosisTime(LocalTime diagnosisTime) {
    	this.diagnosisTime = diagnosisTime;
    }

	public LocalTime getDiagnosisTime() {
		return diagnosisTime;
	}

	public void setDoctorCode(String doctorCode) {
    	this.doctorCode = doctorCode;
    }

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorName(String doctorName) {
    	this.doctorName = doctorName;
    }

	public String getDoctorName() {
		return doctorName;
	}

	public void setFindings1(String findings1) {
    	this.findings1 = findings1;
    }

	public String getFindings1() {
		return findings1;
	}

	public void setFindings2(String findings2) {
    	this.findings2 = findings2;
    }

	public String getFindings2() {
		return findings2;
	}

	public void setAddedUser(String addedUser) {
    	this.addedUser = addedUser;
    }

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
    	this.addedDate = addedDate;
    }

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
    	this.addedTime = addedTime;
    }

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
    	this.changedUser = changedUser;
    }

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
    	this.changedDate = changedDate;
    }

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
    	this.changedTime = changedTime;
    }

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}