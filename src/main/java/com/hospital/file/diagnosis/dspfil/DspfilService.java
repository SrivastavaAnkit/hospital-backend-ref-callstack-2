package com.hospital.file.diagnosis.dspfil;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.utils.RestResponsePage;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callService;
import static com.hospital.common.callstack.StepResult.callScreen;
import static com.hospital.common.callstack.StepResult.returnFromService;
import com.hospital.file.diagnosis.DiagnosisRepository;
import com.hospital.file.diagnosis.rtvnbrdiagnosispatient.RtvNbrDiagnosisPatientDTO;
import com.hospital.file.diagnosis.rtvnbrdiagnosispatient.RtvNbrDiagnosisPatientService;
import com.hospital.file.doctor.dspdoctordetail.DspDoctorDetailParams;
import com.hospital.file.doctor.dspdoctordetail.DspDoctorDetailService;
import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameDTO;
import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameService;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsParams;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsService;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameDTO;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameService;
import com.hospital.file.prescription.dspfilmedicationdisplay.DspfilMedicationDisplayParams;
import com.hospital.file.prescription.dspfilmedicationdisplay.DspfilMedicationDisplayService;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.support.JobContext;




/**
 * DSPFIL Service controller for 'DSPFIL' (TSATDFR) of file 'Diagnosis' (TSAGCPP)
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
@Service
public class DspfilService extends AbstractService<DspfilService, DspfilState> {
    
	@Autowired
	private JobContext job;

    @Autowired
    private DiagnosisRepository diagnosisRepository;
    
    @Autowired
    private RtvDoctorNameService rtvDoctorNameService;
    
    @Autowired
    private RtvNbrDiagnosisPatientService rtvNbrDiagnosisPatientService;
    
    @Autowired
    private RtvPatientNameService rtvPatientNameService;
    

    @Autowired
    private MessageSource messageSource;
    
	public static final String SCREEN_CTL = "dspfil";
    public static final String SCREEN_RCD = "Dspfil.rcd";

    private final Step execute = define("execute", DspfilParams.class, this::execute);
    private final Step response = define("response", DspfilDTO.class, this::processResponse);
	private final Step serviceDspDoctorDetail = define("serviceDspDoctorDetail",DspDoctorDetailParams.class, this::processServiceDspDoctorDetail);
	private final Step serviceDspfilMedicationDisplay = define("serviceDspfilMedicationDisplay",DspfilMedicationDisplayParams.class, this::processServiceDspfilMedicationDisplay);
	private final Step serviceDspAllHospitals = define("serviceDspAllHospitals",DspAllHospitalsParams.class, this::processServiceDspAllHospitals);
	//private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
	//private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
	//private final Step serviceQuit = define("serviceQuit",QuitParams.class, this::processServiceQuit);
	//private final Step serviceRtvDoctorName = define("serviceRtvDoctorName",RtvDoctorNameParams.class, this::processServiceRtvDoctorName);
	//private final Step serviceRtvPatientName = define("serviceRtvPatientName",RtvPatientNameParams.class, this::processServiceRtvPatientName);
	//private final Step serviceRtvNbrDiagnosisPatient = define("serviceRtvNbrDiagnosisPatient",RtvNbrDiagnosisPatientParams.class, this::processServiceRtvNbrDiagnosisPatient);
	//private final Step serviceAdd = define("serviceAdd",AddParams.class, this::processServiceAdd);
	
    
    @Autowired
    public DspfilService() {
        super(DspfilService.class, DspfilState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute; 
    }

    /**
     * DSPFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(DspfilState state, DspfilParams params) {
    	StepResult result = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        result = usrInitializeProgram(state);

        result = mainLoop(state, params);

        return result;
    }

    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult mainLoop(DspfilState state, DspfilParams params) {
        StepResult result = NO_ACTION;

        result = loadFirstSubfilePage(state, params);

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        result = conductScreenConversation(state);

        return result;
    }

    /**
     * SCREEN  initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult loadFirstSubfilePage(DspfilState state, DspfilParams params) {
    	StepResult result = NO_ACTION;

    	result = usrInitializeSubfileControl(state, params);

		dbfReadFirstDataRecord(state);
		if (state.getPageDto() != null && state.getPageDto().getSize() > 0) {
		    result = loadNextSubfilePage(state);
		}

        return result;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     * @return
     */
    private StepResult loadNextSubfilePage(DspfilState state) {
    	StepResult result = NO_ACTION;

    	List<DspfilGDO> list = state.getPageDto().getContent();
        for (DspfilGDO gdo : list) {
            gdo.set_SysRecordSelected(RecordSelectedEnum._STA_YES);
            result = usrInitializeSubfileRecordFromDBFRecord(state, gdo);
//             TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
            if (gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
//                TODO:writeSubfileRecord(state);  // synon built-in function
            }
        }

        return result;
    }

    /**
     * SCREEN  initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductScreenConversation(DspfilState state) {
        StepResult result = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            DspfilDTO model = new DspfilDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_CTL, model).thenCall(response);
        }

        return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processResponse(DspfilState state, DspfilDTO model) {
    	StepResult result = NO_ACTION;

        // update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            result = closedown(state);
        } else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
        } else if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            //TODO:processHelpRequest(state);//synon built-in function
        }
        else if (CmdKeyEnum.isNextPage(state.get_SysCmdKey())) {
            dbfReadNextPageRecord(state);
            result = loadNextSubfilePage(state);
        } else {
            result = processScreen(state);
        }

        return result;
    }

    /**
     * SCREEN process screen.
     * @param state - Service state class.
     * @return
     */
    private StepResult processScreen(DspfilState state) {
    	StepResult result = NO_ACTION;

        result = usrProcessSubfilePreConfirm(state);
        if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
        	return closedown(state);
        } else {
        	if(!state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
        		return closedown(state);
	        } else {
//	        	if(!state.get_SysProgramConfirm().getCode().equals(ProgramConfirmEnum._STA_YES.getCode())) {
//	        		return closedown(state);
//		        } else {
//		        	result = usrProcessCommandKeys(state);
//		        }

	        	result = usrProcessSubfileControlPostConfirm(state);
	        	for (DspfilGDO gdo : state.getPageDto().getContent()) {
	                if(gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
	                	result = usrProcessSubfileRecordPostConfirm(state, gdo);
//	                  TODO:writeSubfileRecord(state);   // synon built-in function
	                }
	            }
	        	result = usrFinalProcessingPostConfirm(state);
	        	result = usrProcessCommandKeys(state);
	        }
        }

        result = conductScreenConversation(state);

        return result;
    }

    /**
     * SCREEN Process subfile Pre Confirm.
     * @param state - Service state class.
     * @return
     */
    private StepResult usrProcessSubfilePreConfirm(DspfilState state) {
    	StepResult result = NO_ACTION;

    	result = usrSubfileControlFunctionFields(state);
    	result = usrProcessSubfileControlPreConfirm(state);
    	if(state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
    		for (DspfilGDO gdo : state.getPageDto().getContent()) {
                if(gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
                	result = usrSubfileRecordFunctionFields(state, gdo);
                    result = usrProcessSubfileRecordPreConfirm(state, gdo);
//                  TODO:writeSubfileRecord(state);   // synon built-in function
                }
            }
    	}

        result = usrFinalProcessingPreConfirm(state);

        return result;
    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(DspfilState state) {
        StepResult result = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        result = usrExitProgramProcessing(state);

        return result;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(DspfilState state) {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadNextPageRecord(DspfilState state)
	{
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    private void dbfReadDataRecord(DspfilState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = new PageRequest(state.getPage(), state.getSize());
        }
        else {
            pageable = new PageRequest(state.getPage(), state.getSize(), new Sort(sortOrders));
        }

        RestResponsePage<DspfilGDO> pageDto = diagnosisRepository.dspfil(state.getPatientCode(), state.getDiagnosisDate(), state.getDoctorCode(), pageable);
        state.setPageDto(pageDto);
    }
    
	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(DspfilState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 20 SUB    
			//switchBLK 1000073 BLK ACT
			// DEBUG genFunctionCall 1000074 ACT PGM.*Scan limit = CND.High Value
			dto.set_SysScanLimit(9999999);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Control (Generated:182)
	 */
    private StepResult usrInitializeSubfileControl(DspfilState dto, DspfilParams params) {
        StepResult result = NO_ACTION;

        try {
        	RtvNbrDiagnosisPatientDTO rtvNbrDiagnosisPatientDTO;
			RtvPatientNameDTO rtvPatientNameDTO;
			//switchSUB 182 SUB    
			//switchBLK 1000079 BLK ACT
			// DEBUG genFunctionCall 1000080 ACT CTL.Diagnosis Date = JOB.*Job date
			dto.setDiagnosisDate(LocalDate.now());
			//switchBLK 1000095 BLK ACT
			// DEBUG genFunctionCall 1000096 ACT RTV Patient Name - Patient  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvPatientNameDTO = new RtvPatientNameDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvPatientNameDTO.setPatientCode(dto.getPatientCode());
			// DEBUG genFunctionCall Service call
			rtvPatientNameService.execute(rtvPatientNameDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setPatientName(rtvPatientNameDTO.getPatientName());
			// DEBUG genFunctionCall Parameters DONE
			//switchBLK 1000100 BLK ACT
			// DEBUG genFunctionCall 1000101 ACT RTV Nbr Diagnosis/Patient - Diagnosis  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvNbrDiagnosisPatientDTO = new RtvNbrDiagnosisPatientDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvNbrDiagnosisPatientDTO.setPatientCode(dto.getPatientCode());
			// DEBUG genFunctionCall Service call
			rtvNbrDiagnosisPatientService.execute(rtvNbrDiagnosisPatientDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setUsrNbrOfDiagnosis(rtvNbrDiagnosisPatientDTO.getUsrNbrOfDiagnosis());
			// DEBUG genFunctionCall Parameters DONE
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrInitializeSubfileRecordFromDBFRecord(DspfilState dto, DspfilGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	RtvDoctorNameDTO rtvDoctorNameDTO;
			//switchSUB 41 SUB    
			//switchBLK 1000087 BLK ACT
			// DEBUG genFunctionCall 1000088 ACT RTV Doctor Name - Doctor  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvDoctorNameDTO = new RtvDoctorNameDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvDoctorNameDTO.setDoctorCode(gdo.getDoctorCode());
			// DEBUG genFunctionCall Service call
			rtvDoctorNameService.execute(rtvDoctorNameDTO);
			// DEBUG genFunctionCall Parameters OUT
			gdo.setDoctorName(rtvDoctorNameDTO.getDoctorName());
			// DEBUG genFunctionCall Parameters DONE
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:175)
	 */
    private StepResult usrSubfileControlFunctionFields(DspfilState dto) {
        StepResult result = NO_ACTION;

        try {
        	RtvNbrDiagnosisPatientDTO rtvNbrDiagnosisPatientDTO;
			//switchSUB 175 SUB    
			//switchBLK 1000104 BLK ACT
			// DEBUG genFunctionCall 1000105 ACT RTV Nbr Diagnosis/Patient - Diagnosis  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvNbrDiagnosisPatientDTO = new RtvNbrDiagnosisPatientDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvNbrDiagnosisPatientDTO.setPatientCode(dto.getPatientCode());
			// DEBUG genFunctionCall Service call
			rtvNbrDiagnosisPatientService.execute(rtvNbrDiagnosisPatientDTO);
			// DEBUG genFunctionCall Parameters OUT
			dto.setUsrNbrOfDiagnosis(rtvNbrDiagnosisPatientDTO.getUsrNbrOfDiagnosis());
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Control (Pre-Confirm) (Generated:72)
	 */
    private StepResult usrProcessSubfileControlPreConfirm(DspfilState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 72 SUB    
			//switchBLK 1000015 BLK CAS
			//switchSUB 1000015 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// CTL.*CMD key is *Cancel
				//switchBLK 1000055 BLK ACT
				// DEBUG genFunctionCall 1000056 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000018 BLK ACT
				// DEBUG genFunctionCall 1000019 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:170)
	 */
    private StepResult usrSubfileRecordFunctionFields(DspfilState dto, DspfilGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	RtvDoctorNameDTO rtvDoctorNameDTO;
			//switchSUB 170 SUB    
			//switchBLK 1000091 BLK ACT
			// DEBUG genFunctionCall 1000092 ACT RTV Doctor Name - Doctor  *
			// DEBUG genFunctionCall ServiceDtoVariable
			rtvDoctorNameDTO = new RtvDoctorNameDTO();
			// DEBUG genFunctionCall Parameters IN
			rtvDoctorNameDTO.setDoctorCode(gdo.getDoctorCode());
			// DEBUG genFunctionCall Service call
			rtvDoctorNameService.execute(rtvDoctorNameDTO);
			// DEBUG genFunctionCall Parameters OUT
			gdo.setDoctorName(rtvDoctorNameDTO.getDoctorName());
			// DEBUG genFunctionCall Parameters DONE
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Record (Pre-Confirm) (Generated:101)
	 */
    private StepResult usrProcessSubfileRecordPreConfirm(DspfilState dto, DspfilGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 101 SUB    
			//switchBLK 1000001 BLK CAS
			//switchSUB 1000001 BLK CAS
			if (gdo.get_SysSelected().equals("Display Patients")) {
				// RCD.*SFLSEL is Display Patients
				//switchBLK 1000004 BLK ACT
				// DEBUG genFunctionCall 1000005 ACT DSP Doctor Detail - Doctor  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				DspDoctorDetailParams dspDoctorDetailParams = new DspDoctorDetailParams();
				BeanUtils.copyProperties(dto, dspDoctorDetailParams);
				result = StepResult.callService(DspDoctorDetailService.class, dspDoctorDetailParams).thenCall(serviceDspDoctorDetail);
				}
				//switchBLK 1000028 BLK CAS
				//switchSUB 1000028 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000031 BLK ACT
					// DEBUG genFunctionCall 1000032 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000068 BLK ACT
					// DEBUG genFunctionCall 1000069 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000037 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000039 BLK ACT
					// DEBUG genFunctionCall 1000040 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
			//switchBLK 1000072 BLK TXT
			// 
			//switchBLK 1000007 BLK CAS
			//switchSUB 1000007 BLK CAS
			if (gdo.get_SysSelected().equals("Medication")) {
				// RCD.*SFLSEL is Medication
				//switchBLK 1000010 BLK ACT
				// DEBUG genFunctionCall 1000011 ACT DSPFIL Medication display - Prescription  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				DspfilMedicationDisplayParams dspfilMedicationDisplayParams = new DspfilMedicationDisplayParams();
				BeanUtils.copyProperties(dto, dspfilMedicationDisplayParams);
				result = StepResult.callService(DspfilMedicationDisplayService.class, dspfilMedicationDisplayParams).thenCall(serviceDspfilMedicationDisplay);
				}
				//switchBLK 1000042 BLK CAS
				//switchSUB 1000042 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000045 BLK ACT
					// DEBUG genFunctionCall 1000046 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					//switchBLK 1000070 BLK ACT
					// DEBUG genFunctionCall 1000071 ACT <-- *QUIT
					// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				}//switchSUB 1000049 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000051 BLK ACT
					// DEBUG genFunctionCall 1000052 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Final processing (Pre-confirm) (Generated:222)
	 */
    private StepResult usrFinalProcessingPreConfirm(DspfilState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 222 SUB    
			//switchBLK 1000113 BLK ACT
			// DEBUG genFunctionCall 1000114 ACT CTL.USR Nbr of Diagnosis = CTL.USR Nbr of Diagnosis + CON.1
			dto.setUsrNbrOfDiagnosis(dto.getUsrNbrOfDiagnosis() + 1);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process subfile control (Post-confirm) (Generated:225)
	 */
    private StepResult usrProcessSubfileControlPostConfirm(DspfilState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 225 SUB    
			// Unprocessed SUB 225 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Record (Post-Confirm Pass) (Generated:209)
	 */
    private StepResult usrProcessSubfileRecordPostConfirm(DspfilState dto, DspfilGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 209 SUB    
			// Unprocessed SUB 209 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Final processing (Post-confirm) (Generated:228)
	 */
    private StepResult usrFinalProcessingPostConfirm(DspfilState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 228 SUB    
			// Unprocessed SUB 228 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Generated:140)
	 */
    private StepResult usrProcessCommandKeys(DspfilState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 140 SUB    
			//switchBLK 1000108 BLK CAS
			//switchSUB 1000108 BLK CAS
			if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("15")) {
				// CTL.*CMD key is CF15
				//switchBLK 1000111 BLK ACT
				// DEBUG genFunctionCall 1000112 ACT DSP All Hospitals - Hospital  *
				// TODO: XEKDM-779 quick fix
				// TODO: XEKDM-779 for duplicate variables when function called multiple times in same userpoint
				// TODO: XEKDM-779 braces added to isolate the context of the variable within the function call
				// TODO: XEKDM-779 better solution required
				{
				//TODO: split
				DspAllHospitalsParams dspAllHospitalsParams = new DspAllHospitalsParams();
				BeanUtils.copyProperties(dto, dspAllHospitalsParams);
				result = StepResult.callService(DspAllHospitalsService.class, dspAllHospitalsParams).thenCall(serviceDspAllHospitals);
				}
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(DspfilState dto) {
        StepResult result = NO_ACTION;
        
        try {
        	//switchSUB 132 SUB    
			//switchBLK 1000021 BLK CAS
			//switchSUB 1000021 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// CTL.*CMD key is *Exit
				//switchBLK 1000062 BLK ACT
				// DEBUG genFunctionCall 1000063 ACT PAR.*Return code = CND.*Normal
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000024 BLK ACT
				// DEBUG genFunctionCall 1000025 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */

    /**
     * DspDoctorDetailService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspDoctorDetail(DspfilState state, DspDoctorDetailParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }

    /**
     * DspfilMedicationDisplayService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspfilMedicationDisplay(DspfilState state, DspfilMedicationDisplayParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }

    /**
     * DspAllHospitalsService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspAllHospitals(DspfilState state, DspAllHospitalsParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }
//
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(DspfilState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(DspfilState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * QuitService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceQuit(DspfilState state, QuitParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvDoctorNameService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvDoctorName(DspfilState state, RtvDoctorNameParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvPatientNameService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvPatientName(DspfilState state, RtvPatientNameParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * RtvNbrDiagnosisPatientService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvNbrDiagnosisPatient(DspfilState state, RtvNbrDiagnosisPatientParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * AddService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceAdd(DspfilState state, AddParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//


}
