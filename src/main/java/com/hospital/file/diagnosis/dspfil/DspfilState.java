package com.hospital.file.diagnosis.dspfil;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;



/**
 * State for file 'Diagnosis' (TSAGCPP) and function 'DSPFIL' (TSATDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class DspfilState extends DspfilDTO {
	private static final long serialVersionUID = 8407856356411655382L;

	// Local fields
	private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");

	// System fields
	private long _sysScanLimit = 0L;
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;


    public DspfilState() {

    }

	public void setLclReturnCode(ReturnCodeEnum returnCode) {
    	this.lclReturnCode = returnCode;
    }

    public ReturnCodeEnum getLclReturnCode() {
    	return lclReturnCode;
    }

	public void set_SysScanLimit(long scanLimit) {
    	_sysScanLimit = scanLimit;
    }

    public long get_SysScanLimit() {
    	return _sysScanLimit;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }
    }