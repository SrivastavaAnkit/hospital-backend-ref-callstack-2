package com.hospital.file.diagnosis.dspfil;

import java.io.Serializable;


/**
 * Params for resource: Dspfil (TSATDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
public class DspfilParams implements Serializable {
	private static final long serialVersionUID = 6609445850262160443L;

	private String patientCode = "";


	public String getPatientCode() {
		return patientCode;
	}
	
	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

}