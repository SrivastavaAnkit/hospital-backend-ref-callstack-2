package com.hospital.file.diagnosis.dspfil;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import com.hospital.common.state.BaseDTO;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.model.ReturnCodeEnum;


/**
 * Dto for file 'Diagnosis' (TSAGCPP) and function 'DSPFIL' (TSATDFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class DspfilDTO extends BaseDTO {
	private static final long serialVersionUID = -7774344070464847524L;
	private long version = 0;

    private RestResponsePage<DspfilGDO> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";

	private String patientCode = "";
	private String patientName = "";
	private long usrNbrOfDiagnosis = 0L;
	private LocalTime diagnosisTime = null;
	private LocalDate diagnosisDate = null;
	private String doctorCode = "";
	private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");


	private DspfilGDO gdo;

    public DspfilDTO() {

    }

	public DspfilDTO(long version, String patientCode) {
		this.version = version;
		this.patientCode = patientCode;
	}

	public void setVersion(long version) {
		this.version = version;
    }

    public long getVersion() {
    	return version;
    }

    public void setPageDto(RestResponsePage<DspfilGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<DspfilGDO> getPageDto() {
		return pageDto;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
    }

    public String getPatientCode() {
    	return patientCode;
    }

	public void setPatientName(String patientName) {
		this.patientName = patientName;
    }

    public String getPatientName() {
    	return patientName;
    }

	public void setUsrNbrOfDiagnosis(long usrNbrOfDiagnosis) {
		this.usrNbrOfDiagnosis = usrNbrOfDiagnosis;
    }

    public long getUsrNbrOfDiagnosis() {
    	return usrNbrOfDiagnosis;
    }

	public void setDiagnosisTime(LocalTime diagnosisTime) {
		this.diagnosisTime = diagnosisTime;
    }

    public LocalTime getDiagnosisTime() {
    	return diagnosisTime;
    }

	public void setDiagnosisDate(LocalDate diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
    }

    public LocalDate getDiagnosisDate() {
    	return diagnosisDate;
    }

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
    }

    public String getDoctorCode() {
    	return doctorCode;
    }

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
    }

    public ReturnCodeEnum getReturnCode() {
    	return returnCode;
    }

	public void setGdo(DspfilGDO gdo) {
		this.gdo = gdo;
	}

	public DspfilGDO getGdo() {
		return gdo;
	}

}