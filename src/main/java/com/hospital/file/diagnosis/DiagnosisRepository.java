package com.hospital.file.diagnosis;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository interface for model: Diagnosis (TSAGCPP).
 *
 * @author X2EGenerator
 */
@Repository
public interface DiagnosisRepository extends DiagnosisRepositoryCustom, JpaRepository<Diagnosis, DiagnosisId> {

	List<Diagnosis> findAllByDoctorCode(String doctorCode);

	List<Diagnosis> findAllByIdPatientCode(String patientCode);
}
