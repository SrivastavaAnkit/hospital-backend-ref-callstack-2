package com.hospital.file.prescription;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.hospital.file.prescription.dspfilmedicationdisplay.DspfilMedicationDisplayGDO;
import com.hospital.file.prescription.trndisplayprescrip.TrnDisplayPrescripGDO;
import com.hospital.file.prescription.selectdrprescriptions.SelectDrPrescriptionsGDO;
import com.hospital.file.prescription.trneditprescriptions.TrnEditPrescriptionsDTO;
import com.hospital.file.prescription.trneditprescriptions.TrnEditPrescriptionsGDO;

/**
 * Custom Spring Data JPA repository interface for model: Prescription (TSAHCPP).
 *
 * @author X2EGenerator
 */
@Repository
public interface PrescriptionRepositoryCustom {

	/**
	 * 
	 * @param doctorCode Doctor Code
	 * @param prescriptionCode Prescription Code
	 * @param prescriptionDate Prescription Date
	 * @param prescriptionTime Prescription Time
	 */
	void deletePrescription(String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime);

	/**
	 * 
	 * @param patientCode Patient Code
	 * @param diagnosisDate Diagnosis Date
	 * @param diagnosisTime Diagnosis Time
	 * @param pageable a Pageable object used for pagination
	 * @return Page of DspfilMedicationDisplayGDO
	 */
	RestResponsePage<DspfilMedicationDisplayGDO> dspfilMedicationDisplay(String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime, Pageable pageable);

	/**
	 * 
	 * @param patientCode Patient Code
	 * @param hospitalCode Hospital Code
	 * @param wardCode Ward Code
	 * @param doctorCode Doctor Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of Prescription
	 */
	RestResponsePage<Prescription> rtvPrescriptionDetail(String patientCode, String hospitalCode, String wardCode, String doctorCode, Pageable pageable);

	/**
	 * 
	 * @param doctorCode Doctor Code
	 * @param prescriptionCode Prescription Code
	 * @param prescriptionDate Prescription Date
	 * @param prescriptionTime Prescription Time
	 * @param pageable a Pageable object used for pagination
	 * @return Page of TrnDisplayPrescripGDO
	 */
	RestResponsePage<TrnDisplayPrescripGDO> trnDisplayPrescrip(String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, Pageable pageable);

	/**
	 * 
	 * @param doctorCode Doctor Code
	 * @param prescriptionCode Prescription Code
	 * @param prescriptionDate Prescription Date
	 * @param prescriptionTime Prescription Time
	 * @param pageable a Pageable object used for pagination
	 * @return Page of SelectDrPrescriptionsGDO
	 */
	RestResponsePage<SelectDrPrescriptionsGDO> selectDrPrescriptions(String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, Pageable pageable);

	/**
	 * 
	 * @param patientCode Patient Code
	 * @param diagnosisDate Diagnosis Date
	 * @param diagnosisTime Diagnosis Time
	 * @param pageable a Pageable object used for pagination
	 * @return Page of Prescription
	 */
	RestResponsePage<Prescription> rtvDiagnosisExist(String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime, Pageable pageable);

	/**
	 * 
	 * @param doctorCode Doctor Code
	 * @param patientCode Patient Code
	 * @return TrnEditPrescriptionsDTO
	 */
	TrnEditPrescriptionsDTO trnEditPrescriptions(String doctorCode, String patientCode);

	/**
	 * 
	 * @param doctorCode Doctor Code
	 * @param prescriptionCode Prescription Code
	 * @param prescriptionDate Prescription Date
	 * @param prescriptionTime Prescription Time
	 * @param pageable a Pageable object used for pagination
	 * @return Page of TrnEditPrescriptionsGDO
	 */
	RestResponsePage<TrnEditPrescriptionsGDO> trnEditPrescriptions(String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, Pageable pageable);
}
