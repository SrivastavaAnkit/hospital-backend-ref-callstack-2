package com.hospital.file.prescription.dspfilmedicationdisplay;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
 
import com.hospital.support.JobContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.stereotype.Service;

import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.file.prescription.PrescriptionRepository;
import com.hospital.file.prescriptionline.dspfilmediaction.DspfilMediactionService;
import com.hospital.file.prescriptionline.dspfilmediaction.DspfilMediactionDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.file.prescriptionline.dspfilmediaction.DspfilMediactionParams;

import com.hospital.common.exception.ServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.common.utils.RestResponsePage;
import com.hospital.common.callstack.AbstractService;
import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;

import static com.hospital.common.callstack.StepResult.NO_ACTION;
import static com.hospital.common.callstack.StepResult.callScreen;

	
/**
 * Service implementation for resource: DspfilMedicationDisplay (TSA5DFR).
 *
 * @author X2EGenerator
 */
@Service
public class DspfilMedicationDisplayService extends AbstractService<DspfilMedicationDisplayService, DspfilMedicationDisplayState> {
    
	@Autowired
	private JobContext job;

    @Autowired
    private PrescriptionRepository prescriptionRepository;
    
    
	public static final String SCREEN_CTL = "dspfilMedicationDisplay";
    public static final String SCREEN_RCD = "DspfilMedicationDisplay.rcd";

    private final Step execute = define("execute", DspfilMedicationDisplayParams.class, this::execute);
    private final Step response = define("response", DspfilMedicationDisplayDTO.class, this::processResponse);
	private final Step serviceDspfilMediaction = define("serviceDspfilMediaction",DspfilMediactionParams.class, this::processServiceDspfilMediaction);
	//private final Step serviceExitProgram = define("serviceExitProgram",ExitProgramParams.class, this::processServiceExitProgram);
	//private final Step serviceMove = define("serviceMove",MoveParams.class, this::processServiceMove);
	
    
    @Autowired
    public DspfilMedicationDisplayService() {
        super(DspfilMedicationDisplayService.class, DspfilMedicationDisplayState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * DSPFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return
     */
    private StepResult execute(DspfilMedicationDisplayState state, DspfilMedicationDisplayParams params) {
    	StepResult result = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        result = usrInitializeProgram(state);

        result = mainLoop(state, params);

        return result;
    }

    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult mainLoop(DspfilMedicationDisplayState state, DspfilMedicationDisplayParams params) {
        StepResult result = NO_ACTION;

        result = loadFirstSubfilePage(state, params);

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        result = conductScreenConversation(state);

        return result;
    }

    /**
     * SCREEN  initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult loadFirstSubfilePage(DspfilMedicationDisplayState state, DspfilMedicationDisplayParams params) {
    	StepResult result = NO_ACTION;

    	result = usrInitializeSubfileControl(state, params);

		dbfReadFirstDataRecord(state);
		if (state.getPageDto() != null && state.getPageDto().getSize() > 0) {
		    result = loadNextSubfilePage(state);
		}

        return result;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     * @return
     */
    private StepResult loadNextSubfilePage(DspfilMedicationDisplayState state) {
    	StepResult result = NO_ACTION;

    	List<DspfilMedicationDisplayGDO> list = state.getPageDto().getContent();
        for (DspfilMedicationDisplayGDO gdo : list) {
            gdo.set_SysRecordSelected(RecordSelectedEnum._STA_YES);
            result = usrInitializeSubfileRecordFromDBFRecord(state, gdo);
//             TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
            if (gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
//                TODO:writeSubfileRecord(state);  // synon built-in function
            }
        }

        return result;
    }

    /**
     * SCREEN  initial processing loop method.
     * @param state - Service state class.
     * @return
     */
    private StepResult conductScreenConversation(DspfilMedicationDisplayState state) {
        StepResult result = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            DspfilMedicationDisplayDTO model = new DspfilMedicationDisplayDTO();
            BeanUtils.copyProperties(state, model);
            result = callScreen(SCREEN_CTL, model).thenCall(response);
        }

        return result;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return
     */
    private StepResult processResponse(DspfilMedicationDisplayState state, DspfilMedicationDisplayDTO model) {
    	StepResult result = NO_ACTION;

        // update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            result = closedown(state);
        } else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
        } else if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            //TODO:processHelpRequest(state);//synon built-in function
        }
        else if (CmdKeyEnum.isNextPage(state.get_SysCmdKey())) {
            dbfReadNextPageRecord(state);
            result = loadNextSubfilePage(state);
        } else {
            result = processScreen(state);
        }

        return result;
    }

    /**
     * SCREEN process screen.
     * @param state - Service state class.
     * @return
     */
    private StepResult processScreen(DspfilMedicationDisplayState state) {
    	StepResult result = NO_ACTION;

        result = usrProcessSubfilePreConfirm(state);
        if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {
        	return closedown(state);
        } else {
        	if(!state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
        		return closedown(state);
	        } else {
//	        	if(!state.get_SysProgramConfirm().getCode().equals(ProgramConfirmEnum._STA_YES.getCode())) {
//	        		return closedown(state);
//		        } else {
//		        	result = usrProcessCommandKeys(state);
//		        }

	        	result = usrProcessSubfileControlPostConfirm(state);
	        	for (DspfilMedicationDisplayGDO gdo : state.getPageDto().getContent()) {
	                if(gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
	                	result = usrProcessSubfileRecordPostConfirm(state, gdo);
//	                  TODO:writeSubfileRecord(state);   // synon built-in function
	                }
	            }
	        	result = usrFinalProcessingPostConfirm(state);
	        	result = usrProcessCommandKeys(state);
	        }
        }

        result = conductScreenConversation(state);

        return result;
    }

    /**
     * SCREEN Process subfile Pre Confirm.
     * @param state - Service state class.
     * @return
     */
    private StepResult usrProcessSubfilePreConfirm(DspfilMedicationDisplayState state) {
    	StepResult result = NO_ACTION;

    	result = usrSubfileControlFunctionFields(state);
    	result = usrProcessSubfileControlPreConfirm(state);
    	if(state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
    		for (DspfilMedicationDisplayGDO gdo : state.getPageDto().getContent()) {
                if(gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
                	result = usrSubfileRecordFunctionFields(state, gdo);
                    result = usrProcessSubfileRecordPreConfirm(state, gdo);
//                  TODO:writeSubfileRecord(state);   // synon built-in function
                }
            }
    	}

        result = usrFinalProcessingPreConfirm(state);

        return result;
    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return
     */
    private StepResult closedown(DspfilMedicationDisplayState state) {
        StepResult result = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        result = usrExitProgramProcessing(state);

        return result;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(DspfilMedicationDisplayState state) {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page
     * @param state - Service state class.
     * @return
     */
	private void dbfReadNextPageRecord(DspfilMedicationDisplayState state)
	{
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    private void dbfReadDataRecord(DspfilMedicationDisplayState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = PageRequest.of(state.getPage(), state.getSize());
        }
        else {
            pageable = PageRequest.of(state.getPage(), state.getSize(), Sort.by(sortOrders));
        }

        RestResponsePage<DspfilMedicationDisplayGDO> pageDto = prescriptionRepository.dspfilMedicationDisplay(state.getPatientCode(), state.getDiagnosisDate(), state.getDiagnosisTime(), pageable);
        state.setPageDto(pageDto);
    }
    
	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(DspfilMedicationDisplayState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 20 SUB    
			// Unprocessed SUB 20 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Control (Generated:182)
	 */
    private StepResult usrInitializeSubfileControl(DspfilMedicationDisplayState dto, DspfilMedicationDisplayParams params) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 182 SUB    
			// Unprocessed SUB 182 -
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Initialize Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrInitializeSubfileRecordFromDBFRecord(DspfilMedicationDisplayState dto, DspfilMedicationDisplayGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 41 SUB    
			// Unprocessed SUB 41 -
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:175)
	 */
    private StepResult usrSubfileControlFunctionFields(DspfilMedicationDisplayState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 175 SUB    
			// Unprocessed SUB 175 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Control (Pre-Confirm) (Generated:72)
	 */
    private StepResult usrProcessSubfileControlPreConfirm(DspfilMedicationDisplayState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 72 SUB    
			//switchBLK 1000010 BLK CAS
			//switchSUB 1000010 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("12"))) {
				// CTL.*CMD key is *Cancel
				//switchBLK 1000037 BLK ACT
				// DEBUG genFunctionCall 1000038 ACT PAR.*Return code = CND.E
				dto.setReturnCode(ReturnCodeEnum.fromCode("E"));
				//switchBLK 1000013 BLK ACT
				// DEBUG genFunctionCall 1000014 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:170)
	 */
    private StepResult usrSubfileRecordFunctionFields(DspfilMedicationDisplayState dto, DspfilMedicationDisplayGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 170 SUB    
			// Unprocessed SUB 170 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Record (Pre-Confirm) (Generated:101)
	 */
    private StepResult usrProcessSubfileRecordPreConfirm(DspfilMedicationDisplayState dto, DspfilMedicationDisplayGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 101 SUB    
			//switchBLK 1000001 BLK CAS
			//switchSUB 1000001 BLK CAS
			if (gdo.get_SysSelected().equals("Display Patients")) {
				// RCD.*SFLSEL is Display Patients
				//switchBLK 1000004 BLK ACT
				// DEBUG genFunctionCall 1000005 ACT Dspfil Mediaction - Prescription Line  *
				dto.setNextScreen("DspfilMediaction");
				//switchBLK 1000023 BLK CAS
				//switchSUB 1000023 BLK CAS
				if (dto.getLclReturnCode() == ReturnCodeEnum.fromCode("E")) {
					// LCL.*Return code is E
					//switchBLK 1000026 BLK ACT
					// DEBUG genFunctionCall 1000027 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
				}//switchSUB 1000032 SUB    
				 else {
					// *OTHERWISE
					//switchBLK 1000034 BLK ACT
					// DEBUG genFunctionCall 1000035 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				}
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Final processing (Pre-confirm) (Generated:222)
	 */
    private StepResult usrFinalProcessingPreConfirm(DspfilMedicationDisplayState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 222 SUB    
			// Unprocessed SUB 222 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process subfile control (Post-confirm) (Generated:225)
	 */
    private StepResult usrProcessSubfileControlPostConfirm(DspfilMedicationDisplayState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 225 SUB    
			// Unprocessed SUB 225 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Subfile Record (Post-Confirm Pass) (Generated:209)
	 */
    private StepResult usrProcessSubfileRecordPostConfirm(DspfilMedicationDisplayState dto, DspfilMedicationDisplayGDO gdo) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 209 SUB    
			// Unprocessed SUB 209 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Final processing (Post-confirm) (Generated:228)
	 */
    private StepResult usrFinalProcessingPostConfirm(DspfilMedicationDisplayState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 228 SUB    
			// Unprocessed SUB 228 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Process Command Keys (Generated:140)
	 */
    private StepResult usrProcessCommandKeys(DspfilMedicationDisplayState dto) {
        StepResult result = NO_ACTION;

        try {
        	//switchSUB 140 SUB    
			// Unprocessed SUB 140 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(DspfilMedicationDisplayState dto) {
        StepResult result = NO_ACTION;
        
        try {
        	//switchSUB 132 SUB    
			//switchBLK 1000016 BLK CAS
			//switchSUB 1000016 BLK CAS
			if ((dto.get_SysCmdKey() == CmdKeyEnum.fromCode("03"))) {
				// CTL.*CMD key is *Exit
				//switchBLK 1000044 BLK ACT
				// DEBUG genFunctionCall 1000045 ACT PAR.*Return code = CND.*Normal
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000019 BLK ACT
				// DEBUG genFunctionCall 1000020 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */

    /**
     * DspfilMediactionService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspfilMediaction(DspfilMedicationDisplayState state, DspfilMediactionParams serviceResult)
    {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        //result = ??;

        return result;
    }
//
//    /**
//     * ExitProgramService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceExitProgram(DspfilMedicationDisplayState state, ExitProgramParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
////
//    /**
//     * MoveService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceMove(DspfilMedicationDisplayState state, MoveParams serviceResult)
//    {
//        StepResult result = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        //result = ??;
//
//        return result;
//    }
//


}
