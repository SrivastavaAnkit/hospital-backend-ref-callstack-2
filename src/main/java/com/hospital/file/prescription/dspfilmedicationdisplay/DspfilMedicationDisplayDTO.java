package com.hospital.file.prescription.dspfilmedicationdisplay;
 
import java.math.BigDecimal;
  
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.common.utils.RestResponsePage;

import com.hospital.file.prescription.Prescription;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.common.state.BaseDTO;

/**
 * Dto for file 'Prescription' (TSAHCPP) and function 'DSPFIL Medication display' (TSA5DFR).
 */
public class DspfilMedicationDisplayDTO extends BaseDTO {
	private static final long serialVersionUID = 3513448046943227177L;

    private RestResponsePage<DspfilMedicationDisplayGDO> pageDto;
    private int page = 0;
    private int size = 10;
    private String sortData = "";

	private String patientCode = "";
	private LocalDate diagnosisDate = null;
	private LocalTime diagnosisTime = null;
	private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");


	private DspfilMedicationDisplayGDO gdo;

    public DspfilMedicationDisplayDTO() {

    }

	public DspfilMedicationDisplayDTO(String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime) {
		this.patientCode = patientCode;
		this.diagnosisDate = diagnosisDate;
		this.diagnosisTime = diagnosisTime;
	}

    public void setPageDto(RestResponsePage<DspfilMedicationDisplayGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public RestResponsePage<DspfilMedicationDisplayGDO> getPageDto() {
		return pageDto;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
    }

    public String getPatientCode() {
    	return patientCode;
    }

	public void setDiagnosisDate(LocalDate diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
    }

    public LocalDate getDiagnosisDate() {
    	return diagnosisDate;
    }

	public void setDiagnosisTime(LocalTime diagnosisTime) {
		this.diagnosisTime = diagnosisTime;
    }

    public LocalTime getDiagnosisTime() {
    	return diagnosisTime;
    }

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
    }

    public ReturnCodeEnum getReturnCode() {
    	return returnCode;
    }

	public void setGdo(DspfilMedicationDisplayGDO gdo) {
		this.gdo = gdo;
	}

	public DspfilMedicationDisplayGDO getGdo() {
		return gdo;
	}

}