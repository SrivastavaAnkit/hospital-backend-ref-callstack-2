package com.hospital.file.prescription.dspfilmedicationdisplay;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import com.hospital.config.LocalDateConverter;
import com.hospital.config.LocalTimeConverter;

	
/**
 * Params for resource: DspfilMedicationDisplay (TSA5DFR).
 *
 * @author X2EGenerator
 */
public class DspfilMedicationDisplayParams  implements Serializable{
	private static final long serialVersionUID = -1582980596978135174L;
 
	private String patientCode = "";
	private LocalDate diagnosisDate = null;
	private LocalTime diagnosisTime = null;

		
	public String getPatientCode() {
		return patientCode;
	}
	
	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}
	
	public LocalDate getDiagnosisDate() {
		return diagnosisDate;
	}
	
	public void setDiagnosisDate(LocalDate diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}
	
	public void setDiagnosisDate(String diagnosisDate) {
		setDiagnosisDate(new LocalDateConverter().convert(diagnosisDate));
	}
	
	public LocalTime getDiagnosisTime() {
		return diagnosisTime;
	}
	
	public void setDiagnosisTime(LocalTime diagnosisTime) {
		this.diagnosisTime = diagnosisTime;
	}
	
	public void setDiagnosisTime(String diagnosisTime) {
		setDiagnosisTime(new LocalTimeConverter().convert(diagnosisTime));
	}
 
}