package com.hospital.file.prescription.dspfilmedicationdisplay;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.ReloadSubfileEnum;

/**
 * State for file 'Prescription' (TSAHCPP) and function 'DSPFIL Medication display' (TSA5DFR).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class DspfilMedicationDisplayState extends DspfilMedicationDisplayDTO {
	private static final long serialVersionUID = -1601182227279885194L;

	// Local fields
	private ReturnCodeEnum lclReturnCode = ReturnCodeEnum.fromCode("");

	// System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;


    public DspfilMedicationDisplayState() {

    }

	public void setLclReturnCode(ReturnCodeEnum returnCode) {
    	this.lclReturnCode = returnCode;
    }

    public ReturnCodeEnum getLclReturnCode() {
    	return lclReturnCode;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }
    }