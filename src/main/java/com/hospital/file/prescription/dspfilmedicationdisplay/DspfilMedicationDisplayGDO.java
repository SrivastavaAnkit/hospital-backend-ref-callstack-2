package com.hospital.file.prescription.dspfilmedicationdisplay;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.file.prescription.Prescription;
import com.hospital.model.RecordDataChangedEnum;
import com.hospital.model.RecordSelectedEnum;

/**
 * Gdo for file 'Prescription' (TSAHCPP) and function 'DSPFIL Medication display' (TSA5DFR).
 */
public class DspfilMedicationDisplayGDO implements Serializable {
	private static final long serialVersionUID = -2670263566040806866L;

    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private String patientCode = "";
	private LocalDate diagnosisDate = null;
	private LocalTime diagnosisTime = null;
	private String doctorCode = "";
	private String prescriptionCode = "";
	private LocalDate prescriptionDate = null;
	private LocalTime prescriptionTime = null;
	private String doctorNotes = "";
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;

	public DspfilMedicationDisplayGDO() {

	}

   	public DspfilMedicationDisplayGDO(String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime, String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, String doctorNotes, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime) {
		this.patientCode = patientCode;
		this.diagnosisDate = diagnosisDate;
		this.diagnosisTime = diagnosisTime;
		this.doctorCode = doctorCode;
		this.prescriptionCode = prescriptionCode;
		this.prescriptionDate = prescriptionDate;
		this.prescriptionTime = prescriptionTime;
		this.doctorNotes = doctorNotes;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setPatientCode(String patientCode) {
    	this.patientCode = patientCode;
    }

	public String getPatientCode() {
		return patientCode;
	}

	public void setDiagnosisDate(LocalDate diagnosisDate) {
    	this.diagnosisDate = diagnosisDate;
    }

	public LocalDate getDiagnosisDate() {
		return diagnosisDate;
	}

	public void setDiagnosisTime(LocalTime diagnosisTime) {
    	this.diagnosisTime = diagnosisTime;
    }

	public LocalTime getDiagnosisTime() {
		return diagnosisTime;
	}

	public void setDoctorCode(String doctorCode) {
    	this.doctorCode = doctorCode;
    }

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setPrescriptionCode(String prescriptionCode) {
    	this.prescriptionCode = prescriptionCode;
    }

	public String getPrescriptionCode() {
		return prescriptionCode;
	}

	public void setPrescriptionDate(LocalDate prescriptionDate) {
    	this.prescriptionDate = prescriptionDate;
    }

	public LocalDate getPrescriptionDate() {
		return prescriptionDate;
	}

	public void setPrescriptionTime(LocalTime prescriptionTime) {
    	this.prescriptionTime = prescriptionTime;
    }

	public LocalTime getPrescriptionTime() {
		return prescriptionTime;
	}

	public void setDoctorNotes(String doctorNotes) {
    	this.doctorNotes = doctorNotes;
    }

	public String getDoctorNotes() {
		return doctorNotes;
	}

	public void setAddedUser(String addedUser) {
    	this.addedUser = addedUser;
    }

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
    	this.addedDate = addedDate;
    }

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
    	this.addedTime = addedTime;
    }

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
    	this.changedUser = changedUser;
    }

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
    	this.changedDate = changedDate;
    }

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
    	this.changedTime = changedTime;
    }

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}