package com.hospital.file.prescription.rtvprescriptiondetail;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.prescription.Prescription;
import com.hospital.file.prescription.PrescriptionId;
import com.hospital.file.prescription.PrescriptionRepository;
import com.hospital.file.workfile.createworkfile.CreateWorkFileService;
import com.hospital.file.workfile.createworkfile.CreateWorkFileDTO;
import com.hospital.model.PrescriptionStatusEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvPrescriptionDetailService extends AbstractService<RtvPrescriptionDetailService, RtvPrescriptionDetailDTO>
{
    private final Step execute = define("execute", RtvPrescriptionDetailDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PrescriptionRepository prescriptionRepository;
	
	@Autowired
	private CreateWorkFileService createWorkFileService;
	
    @Autowired
    public RtvPrescriptionDetailService()
    {
        super(RtvPrescriptionDetailService.class, RtvPrescriptionDetailDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvPrescriptionDetailDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvPrescriptionDetailDTO dto, RtvPrescriptionDetailDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Prescription> prescriptionList = prescriptionRepository.findAllByPatientCode(dto.getPatientCode());
		if (!prescriptionList.isEmpty()) {
			for (Prescription prescription : prescriptionList) {
				processDataRecord(dto, prescription);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvPrescriptionDetailDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000119 BLK ACT
		//functionCall 1000120 ACT WRK.Prescription Cost = CON.*ZERO
		dto.setWfPrescriptionCost(BigDecimal.ZERO);
		//switchBLK 1000123 BLK ACT
		//functionCall 1000124 ACT WRK.Total Amount = CON.*ZERO
		dto.setWfTotalAmount(BigDecimal.ZERO);
		//switchBLK 1000128 BLK ACT
		//functionCall 1000129 ACT WRK.Prescription Status = CND.Blank
		dto.setWfPrescriptionStatus("");
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvPrescriptionDetailDTO dto, Prescription prescription) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		CreateWorkFileDTO createWorkFileDTO;
		//switchSUB 41 SUB    
		//switchBLK 1000011 BLK ACT
		//functionCall 1000012 ACT WRK.Note Prefix = SUBSTRING(DB1.Doctor Notes,CON.1,CON.1)
		dto.setWfNotePrefix(StringUtils.substring(prescription.getDoctorNotes(), (int) 0, 1));
		//switchBLK 1000008 BLK CAS
		//switchSUB 1000008 BLK CAS
		if (dto.getWfNotePrefix().equals("A")) {
			// WRK.Note Prefix is A Prefix
			//switchBLK 1000017 BLK ACT
			//functionCall 1000018 ACT WRK.Prescription Cost = CON.100
			dto.setWfPrescriptionCost(new BigDecimal("100"));
			//switchBLK 1000021 BLK ACT
			//functionCall 1000022 ACT WRK.Prescription Status = CND.Collect
			dto.setWfPrescriptionStatus("F");
			//switchBLK 1000090 BLK ACT
			//functionCall 1000091 ACT WRK.Total Amount = CON.100
			dto.setWfTotalAmount(new BigDecimal("100"));
		} else //switchSUB 1000027 SUB    
		if (dto.getWfNotePrefix().equals("B")) {
			// WRK.Note Prefix is B Prefix
			//switchBLK 1000029 BLK ACT
			//functionCall 1000030 ACT WRK.Prescription Cost = CON.200
			dto.setWfPrescriptionCost(new BigDecimal("200"));
			//switchBLK 1000033 BLK ACT
			//functionCall 1000034 ACT WRK.Prescription Status = CND.Collect
			dto.setWfPrescriptionStatus("F");
			//switchBLK 1000094 BLK ACT
			//functionCall 1000095 ACT WRK.Total Amount = CON.100
			dto.setWfTotalAmount(new BigDecimal("100"));
		} else //switchSUB 1000055 SUB    
		if (dto.getWfNotePrefix().equals("C")) {
			// WRK.Note Prefix is C Prefix
			//switchBLK 1000057 BLK ACT
			//functionCall 1000058 ACT WRK.Prescription Cost = CON.300
			dto.setWfPrescriptionCost(new BigDecimal("300"));
			//switchBLK 1000061 BLK ACT
			//functionCall 1000062 ACT WRK.Prescription Status = CND.Mail
			dto.setWfPrescriptionStatus("M");
			//switchBLK 1000098 BLK ACT
			//functionCall 1000099 ACT WRK.Total Amount = WRK.Prescription Cost + CON.20
			dto.setWfTotalAmount(dto.getWfPrescriptionCost().add(new BigDecimal(20)));
		} else //switchSUB 1000039 SUB    
		if (dto.getWfNotePrefix().equals("D")) {
			// WRK.Note Prefix is D Prefix
			//switchBLK 1000041 BLK ACT
			//functionCall 1000042 ACT WRK.Prescription Cost = CON.400
			dto.setWfPrescriptionCost(new BigDecimal("400"));
			//switchBLK 1000045 BLK ACT
			//functionCall 1000046 ACT WRK.Prescription Status = CND.Courier
			dto.setWfPrescriptionStatus("C");
			//switchBLK 1000103 BLK ACT
			//functionCall 1000104 ACT WRK.Total Amount = WRK.Prescription Cost + CON.50
			dto.setWfTotalAmount(dto.getWfPrescriptionCost().add(new BigDecimal(50)));
		} else //switchSUB 1000071 SUB    
		if (dto.getWfNotePrefix().equals("E")) {
			// WRK.Note Prefix is E Prefix
			//switchBLK 1000073 BLK ACT
			//functionCall 1000074 ACT WRK.Prescription Cost = CON.500
			dto.setWfPrescriptionCost(new BigDecimal("500"));
			//switchBLK 1000077 BLK ACT
			//functionCall 1000078 ACT WRK.Prescription Status = CND.Deliver
			dto.setWfPrescriptionStatus("D");
			//switchBLK 1000111 BLK ACT
			//functionCall 1000112 ACT WRK.Total Amount = WRK.Prescription Cost + CON.100
			dto.setWfTotalAmount(dto.getWfPrescriptionCost().add(new BigDecimal(100)));
		}
		//switchBLK 1000134 BLK ACT
		//functionCall 1000135 ACT WRK.Line Number = WRK.Line Number + CON.1
		dto.setWfLineNumber(dto.getWfLineNumber() + 1);
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT Create Work File - Work File  *
		createWorkFileDTO = new CreateWorkFileDTO();
		createWorkFileDTO.setHospitalCode(dto.getHospitalCode());
		createWorkFileDTO.setWardCode(dto.getWardCode());
		createWorkFileDTO.setPatientCode(prescription.getPatientCode());
		createWorkFileDTO.setDoctorCode(prescription.getDoctorCode());
		createWorkFileDTO.setPrescriptionCode(prescription.getPrescriptionCode());
		createWorkFileDTO.setPrescriptionCost(dto.getWfPrescriptionCost());
		createWorkFileDTO.setPrescriptionStatus(PrescriptionStatusEnum.fromCode(dto.getWfPrescriptionStatus()));
		createWorkFileDTO.setTotalAmount(dto.getWfTotalAmount());
		createWorkFileDTO.setLineNumber(dto.getWfLineNumber());
		createWorkFileService.execute(createWorkFileDTO);
		dto.setLclUsrReturnCode(createWorkFileDTO.getUsrReturnCode());
		//switchBLK 1000145 BLK ACT
		//functionCall 1000146 ACT PAR.Prescription Cost = WRK.Prescription Cost
		dto.setPrescriptionCost(dto.getWfPrescriptionCost());
		//switchBLK 1000152 BLK ACT
		//functionCall 1000153 ACT PAR.Prescription Status = WRK.Prescription Status
		dto.setWfPrescriptionStatus(dto.getWfPrescriptionStatus());
		//switchBLK 1000140 BLK ACT
		//functionCall 1000141 ACT PAR.Total Amount = WRK.Total Amount
		dto.setTotalAmount(dto.getWfTotalAmount());
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvPrescriptionDetailDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Empty:52)
		 */
		//switchSUB 52 SUB    
		// Unprocessed SUB 52 -
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvPrescriptionDetailDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Empty:61)
		 */
		//switchSUB 61 SUB    
		// Unprocessed SUB 61 -
        return NO_ACTION;
    }
}
