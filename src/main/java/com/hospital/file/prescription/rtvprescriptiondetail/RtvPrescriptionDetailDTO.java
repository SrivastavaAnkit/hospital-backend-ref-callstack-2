package com.hospital.file.prescription.rtvprescriptiondetail;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.PrescriptionStatusEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvPrescriptionDetailDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private BigDecimal prescriptionCost;
	private BigDecimal totalAmount;
	private PrescriptionStatusEnum prescriptionStatus;
	private ReturnCodeEnum returnCode;
	private String doctorCode;
	private String hospitalCode;
	private String patientCode;
	private String wardCode;
	private String nextScreen;
	private UsrReturnCodeEnum lclUsrReturnCode;

	public String getDoctorCode() {
		return doctorCode;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public BigDecimal getPrescriptionCost() {
		return prescriptionCost;
	}

	public PrescriptionStatusEnum getPrescriptionStatus() {
		return prescriptionStatus;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public String getWardCode() {
		return wardCode;
	}

	public UsrReturnCodeEnum getLclUsrReturnCode() {
		return lclUsrReturnCode;
	}

	public long getWfLineNumber() {
		return global.getLong("lineNumber");
	}

	public String getWfNotePrefix() {
		return global.getString("notePrefix");
	}

	public BigDecimal getWfPrescriptionCost() {
		return global.getBigDecimal("prescriptionCost");
	}

	public String getWfPrescriptionStatus() {
		return global.getString("prescriptionStatus");
	}

	public BigDecimal getWfTotalAmount() {
		return global.getBigDecimal("totalAmount");
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public void setPrescriptionCost(BigDecimal prescriptionCost) {
		this.prescriptionCost = prescriptionCost;
	}

	public void setPrescriptionStatus(PrescriptionStatusEnum prescriptionStatus) {
		this.prescriptionStatus = prescriptionStatus;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public void setLclUsrReturnCode(UsrReturnCodeEnum lclUsrReturnCode) {
		this.lclUsrReturnCode = lclUsrReturnCode;
	}

	public void setWfLineNumber(long wfLineNumber) {
		global.setLong("lineNumber", wfLineNumber);
	}

	public void setWfNotePrefix(String wfNotePrefix) {
		global.setString("notePrefix", wfNotePrefix);
	}

	public void setWfPrescriptionCost(BigDecimal wfPrescriptionCost) {
		global.setBigDecimal("prescriptionCost", wfPrescriptionCost);
	}

	public void setWfPrescriptionStatus(String wfPrescriptionStatus) {
		global.setString("prescriptionStatus", wfPrescriptionStatus);
	}

	public void setWfTotalAmount(BigDecimal wfTotalAmount) {
		global.setBigDecimal("totalAmount", wfTotalAmount);
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
