package com.hospital.file.prescription;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Convert;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.hospital.common.jpa.DateConverter;
import com.hospital.common.jpa.TimeConverter;

public class PrescriptionId implements Serializable {
	private static final long serialVersionUID = -1L;

	@Column(name = "PrescriptionCode")
	private String prescriptionCode = "";
	
	@Column(name = "PrescriptionDate")
	@Convert(converter=DateConverter.class)
	private LocalDate prescriptionDate = LocalDate.of(1801, 1, 1);
	
	@Column(name = "PrescriptionTime")
	@Convert(converter=  TimeConverter.class)
	private LocalTime prescriptionTime = LocalTime.of(0, 0);
	
	@Column(name = "DoctorCode")
	private String doctorCode = "";

	public PrescriptionId() {
	
	}

	public PrescriptionId(String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime) {
		this.prescriptionCode = prescriptionCode;
		this.prescriptionDate = prescriptionDate;
		this.prescriptionTime = prescriptionTime;
		this.doctorCode = doctorCode;
	}

	public String getPrescriptionCode() {
		return prescriptionCode;
	}
	
	public LocalDate getPrescriptionDate() {
		return prescriptionDate;
	}
	
	public LocalTime getPrescriptionTime() {
		return prescriptionTime;
	}
	
	public String getDoctorCode() {
		return doctorCode;
	}
	
	public void setPrescriptionCode(String prescriptionCode) {
		this.prescriptionCode = prescriptionCode;
	}
	
	public void setPrescriptionDate(LocalDate prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
	}
	
	public void setPrescriptionTime(LocalTime prescriptionTime) {
		this.prescriptionTime = prescriptionTime;
	}
	
	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
