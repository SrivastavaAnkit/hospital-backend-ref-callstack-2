package com.hospital.file.prescription.selectdrprescriptions;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;

import com.hospital.common.state.BaseDTO;
import com.hospital.common.state.SelectRecordDTO;
import com.hospital.file.prescription.Prescription;
import com.hospital.model.RecordSelectedEnum;

/**
 * Dto for file 'Prescription' (TSAHCPP) and function 'Select DR/Prescriptions' (TSBRSRR).
 */
public class SelectDrPrescriptionsDTO extends SelectRecordDTO<SelectDrPrescriptionsGDO> {
	private static final long serialVersionUID = -395299406632302771L;

	private String doctorCode = "";
	private String prescriptionCode = "";
	private LocalDate prescriptionDate = LocalDate.of(1801, 1, 1);
	private LocalTime prescriptionTime = LocalTime.of(0, 0);

	public SelectDrPrescriptionsDTO() {
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setPrescriptionCode(String prescriptionCode) {
		this.prescriptionCode = prescriptionCode;
	}

	public String getPrescriptionCode() {
		return prescriptionCode;
	}

	public void setPrescriptionDate(LocalDate prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
	}

	public LocalDate getPrescriptionDate() {
		return prescriptionDate;
	}

	public void setPrescriptionTime(LocalTime prescriptionTime) {
		this.prescriptionTime = prescriptionTime;
	}

	public LocalTime getPrescriptionTime() {
		return prescriptionTime;
	}

    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param prescription Prescription Entity bean
     */
    public void setDtoFields(Prescription prescription) {
        BeanUtils.copyProperties(prescription, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param prescription Prescription Entity bean
     */
    public void setEntityFields(Prescription prescription) {
        BeanUtils.copyProperties(this, prescription);
    }
}
