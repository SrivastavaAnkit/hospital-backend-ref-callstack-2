package com.hospital.file.prescription.selectdrprescriptions;

import java.io.Serializable;

import java.time.LocalDate;
import java.time.LocalTime;

import com.hospital.config.LocalDateConverter;
import com.hospital.config.LocalTimeConverter;

/**
 * Params for resource: SelectDrPrescriptions (TSBRSRR).
 *
 * @author X2EGenerator
 */
public class SelectDrPrescriptionsParams implements Serializable {
    private static final long serialVersionUID = -5874532940224445132L;

	private String doctorCode = "";
	private String prescriptionCode = "";
	private LocalDate prescriptionDate = null;
	private LocalTime prescriptionTime = null;

	public String getDoctorCode() {
		return doctorCode;
	}
	
	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}
	
	public String getPrescriptionCode() {
		return prescriptionCode;
	}
	
	public void setPrescriptionCode(String prescriptionCode) {
		this.prescriptionCode = prescriptionCode;
	}
	
	public LocalDate getPrescriptionDate() {
		return prescriptionDate;
	}
	
	public void setPrescriptionDate(LocalDate prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
	}
	
	public void setPrescriptionDate(String prescriptionDate) {
		setPrescriptionDate(new LocalDateConverter().convert(prescriptionDate));
	}
	
	public LocalTime getPrescriptionTime() {
		return prescriptionTime;
	}
	
	public void setPrescriptionTime(LocalTime prescriptionTime) {
		this.prescriptionTime = prescriptionTime;
	}
	
	public void setPrescriptionTime(String prescriptionTime) {
		setPrescriptionTime(new LocalTimeConverter().convert(prescriptionTime));
	}
}

