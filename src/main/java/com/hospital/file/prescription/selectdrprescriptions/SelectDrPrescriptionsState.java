package com.hospital.file.prescription.selectdrprescriptions;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.file.prescription.Prescription;
import com.hospital.model.ReloadSubfileEnum;

/**
 * State for file 'Prescription' (TSAHCPP) and function 'Select DR/Prescriptions' (TSBRSRR).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectDrPrescriptionsState extends SelectDrPrescriptionsDTO {
    private static final long serialVersionUID = -4460851572054408448L;


    // System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    public SelectDrPrescriptionsState() {
    }

    public SelectDrPrescriptionsState(Prescription prescription) {
        setDtoFields(prescription);
    }

   public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
       _sysReloadSubfile = reloadSubfile;
   }

   public ReloadSubfileEnum get_SysReloadSubfile() {
       return _sysReloadSubfile;
   }

}
