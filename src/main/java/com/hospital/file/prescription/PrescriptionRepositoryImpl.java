package com.hospital.file.prescription;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;

import com.hospital.file.prescription.Prescription;
import com.hospital.file.prescription.dspfilmedicationdisplay.DspfilMedicationDisplayGDO;
import com.hospital.file.prescription.trndisplayprescrip.TrnDisplayPrescripGDO;
import com.hospital.file.prescription.selectdrprescriptions.SelectDrPrescriptionsGDO;
import com.hospital.file.prescription.trneditprescriptions.TrnEditPrescriptionsDTO;
import com.hospital.file.prescription.trneditprescriptions.TrnEditPrescriptionsGDO;
import com.hospital.file.prescriptionline.PrescriptionLine;

/**
 * Custom Spring Data JPA repository implementation for model: Prescription (TSAHCPP).
 *
 * @author X2EGenerator
 */
@Repository
public class PrescriptionRepositoryImpl implements PrescriptionRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see com.hospital.file.prescription.PrescriptionService#deletePrescription(Prescription)
	 */
	@Override
	public void deletePrescription(
		String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " prescription.id.doctorCode = :doctorCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(prescriptionCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescription.id.prescriptionCode = :prescriptionCode";
			isParamSet = true;
		}

		if (prescriptionDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescription.id.prescriptionDate = :prescriptionDate";
			isParamSet = true;
		}

		if (prescriptionTime != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescription.id.prescriptionTime = :prescriptionTime";
			isParamSet = true;
		}

		String sqlString = "DELETE FROM Prescription prescription";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			query.setParameter("doctorCode", doctorCode);
		}

		if (StringUtils.isNotEmpty(prescriptionCode)) {
			query.setParameter("prescriptionCode", prescriptionCode);
		}

		if (prescriptionDate != null) {
			query.setParameter("prescriptionDate", prescriptionDate);
		}

		if (prescriptionTime != null) {
			query.setParameter("prescriptionTime", prescriptionTime);
		}

		em.createNativeQuery("SET foreign_key_checks = 0").executeUpdate();
		query.executeUpdate();
		em.createNativeQuery("SET foreign_key_checks = 1").executeUpdate();
	}

	@Override
	public RestResponsePage<DspfilMedicationDisplayGDO> dspfilMedicationDisplay(
		String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(patientCode)) {
			whereClause += " prescription.patientCode like CONCAT('%', :patientCode, '%')";
			isParamSet = true;
		}

		if (diagnosisDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescription.diagnosisDate = :diagnosisDate";
			isParamSet = true;
		}

		if (diagnosisTime != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescription.diagnosisTime = :diagnosisTime";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.prescription.dspfilmedicationdisplay.DspfilMedicationDisplayGDO(prescription.patientCode, prescription.diagnosisDate, prescription.diagnosisTime, prescription.id.doctorCode, prescription.id.prescriptionCode, prescription.id.prescriptionDate, prescription.id.prescriptionTime, prescription.doctorNotes, prescription.addedUser, prescription.addedDate, prescription.addedTime, prescription.changedUser, prescription.changedDate, prescription.changedTime) from Prescription prescription";
		String countQueryString = "SELECT COUNT(prescription.id.doctorCode) FROM Prescription prescription";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Prescription prescription = new Prescription();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(prescription.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"prescription.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"prescription." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescription.patientCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescription.diagnosisDate"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescription.diagnosisTime"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(patientCode)) {
			countQuery.setParameter("patientCode", patientCode);
			query.setParameter("patientCode", patientCode);
		}

		if (diagnosisDate != null) {
			countQuery.setParameter("diagnosisDate", diagnosisDate);
			query.setParameter("diagnosisDate", diagnosisDate);
		}

		if (diagnosisTime != null) {
			countQuery.setParameter("diagnosisTime", diagnosisTime);
			query.setParameter("diagnosisTime", diagnosisTime);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<DspfilMedicationDisplayGDO> content = query.getResultList();
		RestResponsePage<DspfilMedicationDisplayGDO> pageDto = new RestResponsePage<DspfilMedicationDisplayGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.prescription.PrescriptionService#rtvPrescriptionDetail(String, String, String, String)
	 */
	@Override
	public RestResponsePage<Prescription> rtvPrescriptionDetail(
		String patientCode, String hospitalCode, String wardCode, String doctorCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(patientCode)) {
			whereClause += " prescription.patientCode = :patientCode";
			isParamSet = true;
		}

		String sqlString = "SELECT prescription FROM Prescription prescription";
		String countQueryString = "SELECT COUNT(prescription.id.doctorCode) FROM Prescription prescription";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(patientCode)) {
			countQuery.setParameter("patientCode", patientCode);
			query.setParameter("patientCode", patientCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<Prescription> content = query.getResultList();
		RestResponsePage<Prescription> pageDto = new RestResponsePage<Prescription>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<TrnDisplayPrescripGDO> trnDisplayPrescrip(
		String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " prescription.id.doctorCode like CONCAT('%', :doctorCode, '%')";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(prescriptionCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescription.id.prescriptionCode like CONCAT('%', :prescriptionCode, '%')";
			isParamSet = true;
		}

		if (prescriptionDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescription.id.prescriptionDate = :prescriptionDate";
			isParamSet = true;
		}

		if (prescriptionTime != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescription.id.prescriptionTime = :prescriptionTime";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.prescription.trndisplayprescrip.TrnDisplayPrescripGDO(prescription.id.doctorCode, prescription.id.prescriptionCode, prescription.id.prescriptionDate, prescription.id.prescriptionTime, prescription.addedUser, prescription.addedDate, prescription.addedTime, prescription.changedUser, prescription.changedDate, prescription.changedTime) from Prescription prescription";
		String countQueryString = "SELECT COUNT(prescription.id.doctorCode) FROM Prescription prescription";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Prescription prescription = new Prescription();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(prescription.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"prescription.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"prescription." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescription.id.doctorCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescription.id.prescriptionCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescription.id.prescriptionDate"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescription.id.prescriptionTime"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			countQuery.setParameter("doctorCode", doctorCode);
			query.setParameter("doctorCode", doctorCode);
		}

		if (StringUtils.isNotEmpty(prescriptionCode)) {
			countQuery.setParameter("prescriptionCode", prescriptionCode);
			query.setParameter("prescriptionCode", prescriptionCode);
		}

		if (prescriptionDate != null) {
			countQuery.setParameter("prescriptionDate", prescriptionDate);
			query.setParameter("prescriptionDate", prescriptionDate);
		}

		if (prescriptionTime != null) {
			countQuery.setParameter("prescriptionTime", prescriptionTime);
			query.setParameter("prescriptionTime", prescriptionTime);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<TrnDisplayPrescripGDO> content = query.getResultList();
		RestResponsePage<TrnDisplayPrescripGDO> pageDto = new RestResponsePage<TrnDisplayPrescripGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public RestResponsePage<SelectDrPrescriptionsGDO> selectDrPrescriptions(
		String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " (prescription.id.doctorCode >= :doctorCode OR prescription.id.doctorCode like CONCAT('%', :doctorCode, '%'))";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(prescriptionCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescription.id.prescriptionCode like CONCAT('%', :prescriptionCode, '%')";
			isParamSet = true;
		}

		if (prescriptionDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescription.id.prescriptionDate = :prescriptionDate";
			isParamSet = true;
		}

		if (prescriptionTime != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescription.id.prescriptionTime = :prescriptionTime";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.prescription.selectdrprescriptions.SelectDrPrescriptionsGDO(prescription.id.doctorCode, prescription.id.prescriptionCode, prescription.id.prescriptionDate, prescription.id.prescriptionTime, prescription.patientCode, prescription.diagnosisDate, prescription.diagnosisTime, prescription.doctorNotes, prescription.addedUser, prescription.addedDate, prescription.addedTime, prescription.changedUser, prescription.changedDate, prescription.changedTime) from Prescription prescription";
		String countQueryString = "SELECT COUNT(prescription.id.doctorCode) FROM Prescription prescription";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Prescription prescription = new Prescription();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(prescription.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"prescription.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"prescription." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescription.id.prescriptionCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescription.id.prescriptionDate"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescription.id.prescriptionTime"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			countQuery.setParameter("doctorCode", doctorCode);
			query.setParameter("doctorCode", doctorCode);
		}

		if (StringUtils.isNotEmpty(prescriptionCode)) {
			countQuery.setParameter("prescriptionCode", prescriptionCode);
			query.setParameter("prescriptionCode", prescriptionCode);
		}

		if (prescriptionDate != null) {
			countQuery.setParameter("prescriptionDate", prescriptionDate);
			query.setParameter("prescriptionDate", prescriptionDate);
		}

		if (prescriptionTime != null) {
			countQuery.setParameter("prescriptionTime", prescriptionTime);
			query.setParameter("prescriptionTime", prescriptionTime);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<SelectDrPrescriptionsGDO> content = query.getResultList();
		RestResponsePage<SelectDrPrescriptionsGDO> pageDto = new RestResponsePage<SelectDrPrescriptionsGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

	/**
	 * @see com.hospital.file.prescription.PrescriptionService#rtvDiagnosisExist(String, LocalDate, LocalTime)
	 */
	@Override
	public RestResponsePage<Prescription> rtvDiagnosisExist(
		String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(patientCode)) {
			whereClause += " prescription.patientCode = :patientCode";
			isParamSet = true;
		}

		if (diagnosisDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescription.diagnosisDate = :diagnosisDate";
			isParamSet = true;
		}

		if (diagnosisTime != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescription.diagnosisTime = :diagnosisTime";
			isParamSet = true;
		}

		String sqlString = "SELECT prescription FROM Prescription prescription";
		String countQueryString = "SELECT COUNT(prescription.id.doctorCode) FROM Prescription prescription";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(patientCode)) {
			countQuery.setParameter("patientCode", patientCode);
			query.setParameter("patientCode", patientCode);
		}

		if (diagnosisDate != null) {
			countQuery.setParameter("diagnosisDate", diagnosisDate);
			query.setParameter("diagnosisDate", diagnosisDate);
		}

		if (diagnosisTime != null) {
			countQuery.setParameter("diagnosisTime", diagnosisTime);
			query.setParameter("diagnosisTime", diagnosisTime);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<Prescription> content = query.getResultList();
		RestResponsePage<Prescription> pageDto = new RestResponsePage<Prescription>(
				content, pageable, count.longValue());

		return pageDto;
	}

	@Override
	public TrnEditPrescriptionsDTO trnEditPrescriptions(
		String doctorCode, String patientCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " prescription.id.doctorCode like CONCAT('%', :doctorCode, '%')";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(patientCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescription.patientCode like CONCAT('%', :patientCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.prescription.trneditprescriptions.TrnEditPrescriptionsDTO(prescription.id.doctorCode, prescription.id.prescriptionCode, prescription.id.prescriptionDate, prescription.id.prescriptionTime, prescription.patientCode, prescription.diagnosisDate, prescription.diagnosisTime, prescription.doctorNotes, prescription.addedUser, prescription.addedDate, prescription.addedTime, prescription.changedUser, prescription.changedDate, prescription.changedTime) from Prescription prescription";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			query.setParameter("doctorCode", doctorCode);
		}

		if (StringUtils.isNotEmpty(patientCode)) {
			query.setParameter("patientCode", patientCode);
		}

		TrnEditPrescriptionsDTO dto;
		@SuppressWarnings("unchecked")
		List<TrnEditPrescriptionsDTO> content = query.getResultList();

		if (content.size() != 0) {
			dto = (TrnEditPrescriptionsDTO)content.get(0);
		} else {
			dto = new TrnEditPrescriptionsDTO();
			dto.setDoctorCode(doctorCode);
			dto.setPatientCode(patientCode);
			dto.setPrescriptionDate(LocalDate.now());
			dto.setPrescriptionTime(LocalTime.now());
			dto.setDiagnosisDate(LocalDate.now());
			dto.setDiagnosisTime(LocalTime.now());
		}

		return dto;
	}

	@Override
	public RestResponsePage<TrnEditPrescriptionsGDO> trnEditPrescriptions(
		String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(doctorCode)) {
			whereClause += " prescriptionLine.id.doctorCode like CONCAT('%', :doctorCode, '%')";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(prescriptionCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescriptionLine.id.prescriptionCode like CONCAT('%', :prescriptionCode, '%')";
			isParamSet = true;
		}

		if (prescriptionDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescriptionLine.id.prescriptionDate = :prescriptionDate";
			isParamSet = true;
		}

		if (prescriptionTime != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " prescriptionLine.id.prescriptionTime = :prescriptionTime";
			isParamSet = true;
		}

		String sqlString = "SELECT new com.hospital.file.prescription.trneditprescriptions.TrnEditPrescriptionsGDO(prescriptionLine.id.doctorCode, prescriptionLine.id.prescriptionCode, prescriptionLine.id.prescriptionDate, prescriptionLine.id.prescriptionTime, prescriptionLine.id.prescriptionLineNumber, prescriptionLine.medicationCode, prescriptionLine.prescriptionQuantity, prescriptionLine.addedUser, prescriptionLine.addedDate, prescriptionLine.addedTime, prescriptionLine.changedUser, prescriptionLine.changedDate, prescriptionLine.changedTime) from PrescriptionLine prescriptionLine";
		String countQueryString = "SELECT COUNT(prescriptionLine.id.doctorCode) FROM PrescriptionLine prescriptionLine";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			PrescriptionLine prescriptionLine = new PrescriptionLine();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(prescriptionLine.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"prescriptionLine.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"prescriptionLine." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescriptionLine.id.doctorCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescriptionLine.id.prescriptionCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescriptionLine.id.prescriptionDate"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescriptionLine.id.prescriptionTime"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"prescriptionLine.id.prescriptionLineNumber"));
			sort = new Sort(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(doctorCode)) {
			countQuery.setParameter("doctorCode", doctorCode);
			query.setParameter("doctorCode", doctorCode);
		}

		if (StringUtils.isNotEmpty(prescriptionCode)) {
			countQuery.setParameter("prescriptionCode", prescriptionCode);
			query.setParameter("prescriptionCode", prescriptionCode);
		}

		if (prescriptionDate != null) {
			countQuery.setParameter("prescriptionDate", prescriptionDate);
			query.setParameter("prescriptionDate", prescriptionDate);
		}

		if (prescriptionTime != null) {
			countQuery.setParameter("prescriptionTime", prescriptionTime);
			query.setParameter("prescriptionTime", prescriptionTime);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<TrnEditPrescriptionsGDO> content = query.getResultList();
		RestResponsePage<TrnEditPrescriptionsGDO> pageDto = new RestResponsePage<TrnEditPrescriptionsGDO>(
				content, pageable, count.longValue());

		return pageDto;
	}

}
