package com.hospital.file.prescription.changeprescription;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.prescription.Prescription;
import com.hospital.file.prescription.PrescriptionId;
import com.hospital.file.prescription.PrescriptionRepository;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class ChangePrescriptionService extends AbstractService<ChangePrescriptionService, ChangePrescriptionDTO>
{
    private final Step execute = define("execute", ChangePrescriptionDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PrescriptionRepository prescriptionRepository;
	

    @Autowired
    public ChangePrescriptionService() {
        super(ChangePrescriptionService.class, ChangePrescriptionDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(ChangePrescriptionDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(ChangePrescriptionDTO dto, ChangePrescriptionDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);
		PrescriptionId prescriptionId = new PrescriptionId();
		prescriptionId.setDoctorCode(dto.getDoctorCode());
		prescriptionId.setPrescriptionCode(dto.getPrescriptionCode());
		prescriptionId.setPrescriptionDate(dto.getPrescriptionDate());
		prescriptionId.setPrescriptionTime(dto.getPrescriptionTime());
		Prescription prescription = prescriptionRepository.findById(prescriptionId).get();
		if (prescription == null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0005"));
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, prescription);
			prescription.setDoctorCode(dto.getDoctorCode());
			prescription.setPrescriptionCode(dto.getPrescriptionCode());
			prescription.setPrescriptionDate(dto.getPrescriptionDate());
			prescription.setPrescriptionTime(dto.getPrescriptionTime());
			prescription.setPatientCode(dto.getPatientCode());
			prescription.setDiagnosisDate(dto.getDiagnosisDate());
			prescription.setDiagnosisTime(dto.getDiagnosisTime());
			prescription.setDoctorNotes(dto.getDoctorNotes());
		/*	prescription.setAddedUser(dto.getAddedUser());
			prescription.setAddedDate(dto.getAddedDate());
			prescription.setAddedTime(dto.getAddedTime());
			prescription.setChangedUser(dto.getChangedUser());
			prescription.setChangedDate(dto.getChangedDate());
			prescription.setChangedTime(dto.getChangedTime());*/
			processingBeforeDataUpdate(dto, prescription);
			try {
				prescriptionRepository.saveAndFlush(prescription);
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
			}
		}

       return result;
	}

	private StepResult processingBeforeDataRead(ChangePrescriptionDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000025 BLK CAS
		//switchSUB 1000025 BLK CAS
		if (dto.getPrescriptionCode().equals("TWOPLD")) {
			// PAR.Prescription Code is Two pills by day
			//switchBLK 1000028 BLK ACT
			//functionCall 1000029 ACT LCL.Medication Description = CON.2 Pills by day
			dto.setLclMedicationDescription("2 Pills by day");
		}
       return NO_ACTION;
	}

	private StepResult processingIfDataRecordNotFound(ChangePrescriptionDTO dto) throws ServiceException {
		/**
		 * USER: Processing if Data Record Not Found (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
	}

	private StepResult processingAfterDataRead(ChangePrescriptionDTO dto, Prescription prescription) throws ServiceException {
		/**
		 * USER: Processing After Data Read (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000034 BLK CAS
		//switchSUB 1000034 BLK CAS
		if (prescription.getPrescriptionCode().equals("TWOPLD")) {
			// DB1.Prescription Code is Two pills by day
			//switchBLK 1000037 BLK ACT
			//functionCall 1000038 ACT LCL.Medication Description = CON.2 Pills by day
			dto.setLclMedicationDescription("2 Pills by day");
		}
       return NO_ACTION;
	}

	private StepResult processingBeforeDataUpdate(ChangePrescriptionDTO dto, Prescription prescription) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Generated:10)
		 */
		//switchSUB 10 SUB    
		//switchBLK 1000013 BLK ACT
		//functionCall 1000014 ACT DB1.Changed User = JOB.*USER
		/*prescription.setChangedUser(job.getUser());
		//switchBLK 1000017 BLK ACT
		//functionCall 1000018 ACT DB1.Changed Date = JOB.*Job date
		prescription.setChangedDate(LocalDate.now());
		//switchBLK 1000021 BLK ACT
		//functionCall 1000022 ACT DB1.Changed Time = JOB.*Job time
		prescription.setChangedTime(LocalTime.now());*/
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(ChangePrescriptionDTO dto) throws ServiceException {
		/**
		 * USER: Processing After Data Update (Generated:23)
		 */
		//switchSUB 23 SUB    
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
	}
}
