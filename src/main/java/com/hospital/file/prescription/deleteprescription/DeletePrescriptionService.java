package com.hospital.file.prescription.deleteprescription;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.prescription.Prescription;
import com.hospital.file.prescription.PrescriptionId;
import com.hospital.file.prescription.PrescriptionRepository;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class DeletePrescriptionService  extends AbstractService<DeletePrescriptionService, DeletePrescriptionDTO>
{
    private final Step execute = define("execute", DeletePrescriptionDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PrescriptionRepository prescriptionRepository;
	

    @Autowired
    public DeletePrescriptionService() {
        super(DeletePrescriptionService.class, DeletePrescriptionDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(DeletePrescriptionDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(DeletePrescriptionDTO dto, DeletePrescriptionDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataUpdate(dto);
		PrescriptionId prescriptionId = new PrescriptionId();
		prescriptionId.setDoctorCode(dto.getDoctorCode());
		prescriptionId.setPrescriptionCode(dto.getPrescriptionCode());
		prescriptionId.setPrescriptionDate(dto.getPrescriptionDate());
		prescriptionId.setPrescriptionTime(dto.getPrescriptionTime());
		try {
			prescriptionRepository.deleteById(prescriptionId);
			prescriptionRepository.flush();
			dto.setReturnCode(ReturnCodeEnum.fromCode(""));
			processingAfterDataUpdate(dto);
		} catch (Exception e) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
		}

       return result;
	}

	private StepResult processingBeforeDataUpdate(DeletePrescriptionDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Empty:9)
		 */
		
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(DeletePrescriptionDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Empty:17)
		 */
		
       return NO_ACTION;
	}
}
