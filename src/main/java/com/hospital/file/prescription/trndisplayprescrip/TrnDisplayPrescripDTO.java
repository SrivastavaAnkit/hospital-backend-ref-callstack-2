package com.hospital.file.prescription.trndisplayprescrip;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;

import com.hospital.file.prescription.Prescription;
import com.hospital.model.UsrReturnCodeEnum;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.common.state.BaseDTO;

/**
 * Dto for file 'Prescription' (TSAHCPP) and function 'TRN Display Prescrip' (TSBNDTR).
 */
public class TrnDisplayPrescripDTO extends BaseDTO {
	private static final long serialVersionUID = -1866984388103485046L;

	private String doctorCode = "";
	private String doctorName = "";
	private String prescriptionCode = "";
	private LocalDate prescriptionDate = null;
	private LocalTime prescriptionTime = null;
	private LocalDate diagnosisDate = null;
	private LocalTime diagnosisTime = null;
	private String patientCode = "";
	private String patientName = "";
	private String doctorNotes = "";
	private long usrNbrOfLines = 0;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;
	private UsrReturnCodeEnum usrReturnCode = null;

	// Local fields
	private MedicationUnitEnum lclMedicationUnit = null;
	private String lclLogFunctionType = "";
	private String lclLogUserPoint = "";

	private ReturnCodeEnum returnCode = ReturnCodeEnum.fromCode("");
	private ReloadSubfileEnum reloadSubfile = null;
	private Page<TrnDisplayPrescripGDO> pageDto;

	public TrnDisplayPrescripDTO() {

	}

	public TrnDisplayPrescripDTO(String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, LocalDate diagnosisDate, LocalTime diagnosisTime, String patientCode, String doctorNotes, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime) {
		this.doctorCode = doctorCode;
		this.prescriptionCode = prescriptionCode;
		this.prescriptionDate = prescriptionDate;
		this.prescriptionTime = prescriptionTime;
		this.diagnosisDate = diagnosisDate;
		this.diagnosisTime = diagnosisTime;
		this.patientCode = patientCode;
		this.doctorNotes = doctorNotes;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setPrescriptionCode(String prescriptionCode) {
		this.prescriptionCode = prescriptionCode;
	}

	public String getPrescriptionCode() {
		return prescriptionCode;
	}

	public void setPrescriptionDate(LocalDate prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
	}

	public LocalDate getPrescriptionDate() {
		return prescriptionDate;
	}

	public void setPrescriptionTime(LocalTime prescriptionTime) {
		this.prescriptionTime = prescriptionTime;
	}

	public LocalTime getPrescriptionTime() {
		return prescriptionTime;
	}

	public void setDiagnosisDate(LocalDate diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}

	public LocalDate getDiagnosisDate() {
		return diagnosisDate;
	}

	public void setDiagnosisTime(LocalTime diagnosisTime) {
		this.diagnosisTime = diagnosisTime;
	}

	public LocalTime getDiagnosisTime() {
		return diagnosisTime;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setDoctorNotes(String doctorNotes) {
		this.doctorNotes = doctorNotes;
	}

	public String getDoctorNotes() {
		return doctorNotes;
	}

	public void setUsrNbrOfLines(long usrNbrOfLines) {
		this.usrNbrOfLines = usrNbrOfLines;
	}

	public long getUsrNbrOfLines() {
		return usrNbrOfLines;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void setUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
		this.usrReturnCode = usrReturnCode;
	}

	public UsrReturnCodeEnum getUsrReturnCode() {
		return usrReturnCode;
	}

	public void setLclMedicationUnit(MedicationUnitEnum medicationUnit) {
		this.lclMedicationUnit = medicationUnit;
	}

	public MedicationUnitEnum getLclMedicationUnit() {
		return lclMedicationUnit;
	}

	public void setLclLogFunctionType(String logFunctionType) {
		this.lclLogFunctionType = logFunctionType;
	}

	public String getLclLogFunctionType() {
		return lclLogFunctionType;
	}

	public void setLclLogUserPoint(String logUserPoint) {
		this.lclLogUserPoint = logUserPoint;
	}

	public String getLclLogUserPoint() {
		return lclLogUserPoint;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setReloadSubfile(ReloadSubfileEnum reloadSubfile) {
		this.reloadSubfile = reloadSubfile;
	}

	public ReloadSubfileEnum getReloadSubfile() {
		return reloadSubfile;
	}

	public void setPageDto(Page<TrnDisplayPrescripGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public Page<TrnDisplayPrescripGDO> getPageDto() {
		return pageDto;
	}
}
