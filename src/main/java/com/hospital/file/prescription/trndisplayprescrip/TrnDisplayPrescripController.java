package com.hospital.file.prescription.trndisplayprescrip;

import java.io.IOException;
import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.support.JobContext;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;

import com.hospital.model.ProgramModeEnum;

import com.hospital.file.prescription.PrescriptionRepository;

import com.hospital.common.exception.ServiceException;

/**
 * Controller/DSPTRN for resource: TrnDisplayPrescrip (TSBNDTR).
 *
 * @author X2EGenerator
 */
@RestController()
public class TrnDisplayPrescripController {

	@Autowired
	private JobContext job;

	@Autowired
	private PrescriptionRepository prescriptionRepository;

	@Autowired
	private TrnDisplayPrescripService trnDisplayPrescripService;

	@Autowired
	private MessageSource messageSource;

	@Transactional(readOnly=true)
	@RequestMapping(value={"/Prescription/TrnDisplayPrescrip", "/Prescription/TrnDisplayPrescrip/{doctorCode}/{prescriptionCode}/{prescriptionDate}/{prescriptionTime}"}, method=RequestMethod.GET, produces="application/json;charset=utf-8")
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public TrnDisplayPrescripDTO selectTrnDisplayPrescrip(@PathVariable Map pathVariables, 
		@RequestParam(value="page", required=false, defaultValue="0") int page,
		@RequestParam(value="size", required=false, defaultValue="10") int size,
		@RequestParam(value="sortdata", required=false, defaultValue="") String sortData,
 		HttpServletRequest request) throws ServiceException {

		TrnDisplayPrescripParams params = new TrnDisplayPrescripParams();
		if (pathVariables != null) {
			if (pathVariables.containsKey("doctorCode") &&
				!"undefined".equals((String)pathVariables.get("doctorCode")) &&
				!"null".equals((String)pathVariables.get("doctorCode"))) {
				params.setDoctorCode((String)pathVariables.get("doctorCode"));
			}

			if (pathVariables.containsKey("prescriptionCode") &&
				!"undefined".equals((String)pathVariables.get("prescriptionCode")) &&
				!"null".equals((String)pathVariables.get("prescriptionCode"))) {
				params.setPrescriptionCode((String)pathVariables.get("prescriptionCode"));
			}

			if (pathVariables.containsKey("prescriptionDate") &&
				!"undefined".equals((String)pathVariables.get("prescriptionDate")) &&
				!"null".equals((String)pathVariables.get("prescriptionDate"))) {
				params.setPrescriptionDate((String)pathVariables.get("prescriptionDate"));
			}

			if (pathVariables.containsKey("prescriptionTime") &&
				!"undefined".equals((String)pathVariables.get("prescriptionTime")) &&
				!"null".equals((String)pathVariables.get("prescriptionTime"))) {
				params.setPrescriptionTime((String)pathVariables.get("prescriptionTime"));
			}
		}

		TrnDisplayPrescripDTO dto = new TrnDisplayPrescripDTO();
		BeanUtils.copyProperties(params, dto);

		trnDisplayPrescripService.initialize(dto, params);


		List<Order> sortOrders = new ArrayList<Order>();
		Pageable pageable;

		try {
			Map<String, String> sortDataMap =
				new ObjectMapper().readValue(sortData, LinkedHashMap.class);

			for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
				if (entry.getValue() == null) {
					continue;
				}

				sortOrders.add(new Order(Direction.fromString(entry.getValue()),
						entry.getKey()));
			}
		} catch (IOException ioe) {
		}

		if (CollectionUtils.isEmpty(sortOrders)) {
			pageable = new PageRequest(page, size);
		} else {
			pageable = new PageRequest(page, size, new Sort(sortOrders));
		}

		Page<TrnDisplayPrescripGDO> pageDto = prescriptionRepository.trnDisplayPrescrip(params.getDoctorCode(), params.getPrescriptionCode(), params.getPrescriptionDate(), params.getPrescriptionTime(), pageable);
		dto.setPageDto(pageDto);

		for (TrnDisplayPrescripGDO gdo : pageDto.getContent()) {
			trnDisplayPrescripService.initializeSubfileRecords(dto, gdo, params);
		}

		return dto;
	}
}
