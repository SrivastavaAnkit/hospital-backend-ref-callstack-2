package com.hospital.file.prescription.trndisplayprescrip;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.hospital.file.prescription.Prescription;

/**
 * Gdo for file 'Prescription' (TSAHCPP) and function 'TRN Display Prescrip' (TSBNDTR).
 */
public class TrnDisplayPrescripGDO implements Serializable {
	private static final long serialVersionUID = -4592929265041780892L;

	private String gdo_selected = "";
	private String doctorCode = "";
	private String prescriptionCode = "";
	private LocalDate prescriptionDate = null;
	private LocalTime prescriptionTime = null;
	private long prescriptionLineNumber = 0;
	private String medicationCode = "";
	private String medicationDescription = "";
	private long prescriptionQuantity = 0;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;

	public TrnDisplayPrescripGDO() {

	}

	public TrnDisplayPrescripGDO(String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime) {
		this.doctorCode = doctorCode;
		this.prescriptionCode = prescriptionCode;
		this.prescriptionDate = prescriptionDate;
		this.prescriptionTime = prescriptionTime;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}

	public void set_Selected(String gdo_selected) {
		this.gdo_selected = gdo_selected;
	}

	public String get_Selected() {
		return gdo_selected;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setPrescriptionCode(String prescriptionCode) {
		this.prescriptionCode = prescriptionCode;
	}

	public String getPrescriptionCode() {
		return prescriptionCode;
	}

	public void setPrescriptionDate(LocalDate prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
	}

	public LocalDate getPrescriptionDate() {
		return prescriptionDate;
	}

	public void setPrescriptionTime(LocalTime prescriptionTime) {
		this.prescriptionTime = prescriptionTime;
	}

	public LocalTime getPrescriptionTime() {
		return prescriptionTime;
	}

	public void setPrescriptionLineNumber(long prescriptionLineNumber) {
		this.prescriptionLineNumber = prescriptionLineNumber;
	}

	public long getPrescriptionLineNumber() {
		return prescriptionLineNumber;
	}

	public void setMedicationCode(String medicationCode) {
		this.medicationCode = medicationCode;
	}

	public String getMedicationCode() {
		return medicationCode;
	}

	public void setMedicationDescription(String medicationDescription) {
		this.medicationDescription = medicationDescription;
	}

	public String getMedicationDescription() {
		return medicationDescription;
	}

	public void setPrescriptionQuantity(long prescriptionQuantity) {
		this.prescriptionQuantity = prescriptionQuantity;
	}

	public long getPrescriptionQuantity() {
		return prescriptionQuantity;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

}
