package com.hospital.file.prescription.trndisplayprescrip;

import com.hospital.common.exception.ServiceException;

/**
 * Service interface for resource: TrnDisplayPrescrip (TSBNDTR).
 *
 * @author X2EGenerator
 */
public interface TrnDisplayPrescripService {

	/**
	 * Initialize Program.
	 *
	 * @param dto TrnDisplayPrescripDTO initialized by the controller
	 * @param params function parameters
	 * @throws ServiceException if a service exception occurs
	 */
	void initialize(TrnDisplayPrescripDTO dto, TrnDisplayPrescripParams params) throws ServiceException;

	/**
	 * Initialize Subfile Record from DBF Record.
	 *
	 * @param dto TrnDisplayPrescripDTO initialized by the controller
	 * @param gdo TrnDisplayPrescripGDO filled by the controller
	 * @param params function parameters
	 * @throws ServiceException if a service exception occurs
	 */
	void initializeSubfileRecords(TrnDisplayPrescripDTO dto, TrnDisplayPrescripGDO gdo, TrnDisplayPrescripParams params) throws ServiceException;
}
