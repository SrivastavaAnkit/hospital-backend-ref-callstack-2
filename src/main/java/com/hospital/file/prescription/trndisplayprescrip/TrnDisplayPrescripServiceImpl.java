package com.hospital.file.prescription.trndisplayprescrip;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.hospital.file.logfile.LogFile;
import com.hospital.file.prescriptionline.PrescriptionLine;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.file.prescription.PrescriptionRepository;

import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameService;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsService;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.medication.rtvdetail.RtvDetailService;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameService;
import com.hospital.file.prescriptionline.deleteprescriptionline.DeletePrescriptionLineService;
import com.hospital.file.prescriptionline.rtvnumberoflines.RtvNumberOfLinesService;
import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameDTO;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsDTO;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.file.medication.rtvdetail.RtvDetailDTO;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameDTO;
import com.hospital.file.prescriptionline.deleteprescriptionline.DeletePrescriptionLineDTO;
import com.hospital.file.prescriptionline.rtvnumberoflines.RtvNumberOfLinesDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReloadSubfileEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsParams;


import com.hospital.common.exception.ServiceException;



/**
 * Service implementation for resource: TrnDisplayPrescrip (TSBNDTR).
 *
 * @author X2EGenerator
 */
@Service
public class TrnDisplayPrescripServiceImpl implements TrnDisplayPrescripService {

	@Autowired
	private PrescriptionRepository prescriptionRepository;

	
	@Autowired
	private CreateLogFileService createLogFileService;
	
	@Autowired
	private DeletePrescriptionLineService deletePrescriptionLineService;
	
	@Autowired
	private RtvDetailService rtvDetailService;
	
	@Autowired
	private RtvDoctorNameService rtvDoctorNameService;
	
	@Autowired
	private RtvNumberOfLinesService rtvNumberOfLinesService;
	
	@Autowired
	private RtvPatientNameService rtvPatientNameService;
	

	@Autowired
	private MessageSource messageSource;

	/**
	 * Initialize Program.
	 *
	 * @param dto TrnDisplayPrescripDTO initialized by the controller
	 * @param params function parameters
	 * @throws ServiceException if a service exception occurs
	 */
	public void initialize(TrnDisplayPrescripDTO dto,	TrnDisplayPrescripParams params) throws ServiceException {
		/**
		 * USER: Initialize Program (Generated:3)
		 */
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 3 SUB    
		//switchBLK 1000030 BLK ACT
		//functionCall 1000031 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
		//switchBLK 1000128 BLK TXT
		// 
		//switchBLK 1000129 BLK ACT
		//functionCall 1000130 ACT LCL.Log Function Type = CON.DSPTRN
		dto.setLclLogFunctionType("DSPTRN");
		//switchBLK 1000133 BLK ACT
		//functionCall 1000134 ACT LCL.Log User Point = CON.Initialize program
		dto.setLclLogUserPoint("Initialize program");
		//switchBLK 1000143 BLK ACT
		//functionCall 1000144 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
	}
	/**
	 * Initialize Subfile Record from DBF Record.
	 *
	 * @param dto TrnDisplayPrescripDTO initialized by the controller
	 * @param gdo TrnDisplayPrescripGDO filled by the controller
	 * @param params function parameters
	 * @throws ServiceException if a service exception occurs
	 */
	public void initializeSubfileRecords(TrnDisplayPrescripDTO dto, TrnDisplayPrescripGDO gdo, TrnDisplayPrescripParams params) throws ServiceException {
		/**
		 * USER: Initialize Subfile Record (Generated:1240)
		 */
		CreateLogFileDTO createLogFileDTO;
		RtvDetailDTO rtvDetailDTO;
		//switchSUB 1240 SUB    
		//switchBLK 1000036 BLK ACT
		//functionCall 1000037 ACT RTV Detail - Medication  *
		rtvDetailDTO = new RtvDetailDTO();
		rtvDetailDTO.setMedicationCode(gdo.getMedicationCode());
		rtvDetailService.execute(rtvDetailDTO);
		gdo.setMedicationDescription(rtvDetailDTO.getMedicationDescription());
		dto.setLclMedicationUnit(rtvDetailDTO.getMedicationUnit());
		//switchBLK 1000200 BLK TXT
		// 
		//switchBLK 1000201 BLK ACT
		//functionCall 1000202 ACT LCL.Log Function Type = CON.DSPTRN
		dto.setLclLogFunctionType("DSPTRN");
		//switchBLK 1000205 BLK ACT
		//functionCall 1000206 ACT LCL.Log User Point = CON.Initialize subfile record
		dto.setLclLogUserPoint("Initialize subfile record");
		//switchBLK 1000215 BLK ACT
		//functionCall 1000216 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
	}
}
