package com.hospital.file.prescription;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.hospital.common.jpa.DateConverter;
import com.hospital.common.jpa.TimeConverter;

@Entity
@Table(name="Prescription", schema="HospitalMgmt")
public class Prescription implements Serializable {
	private static final long serialVersionUID = -1L;

	@Version
	@Column(name="version")
	private long version;
	
	

	@EmbeddedId
	private PrescriptionId id = new PrescriptionId();

	@Column(name = "PatientCode")
	private String patientCode = "";
	
	@Column(name = "DiagnosisDate")
	@Convert(converter=DateConverter.class)

	private LocalDate diagnosisDate = LocalDate.of(1801, 1, 1);
	
	@Column(name = "DiagnosisTime")
	@Convert(converter=TimeConverter.class)
	private LocalTime diagnosisTime = LocalTime.of(0, 0);
	
	@Column(name = "DoctorNotes")
	private String doctorNotes = "";

	public PrescriptionId getId() {
		return id;
	}

	public String getPrescriptionCode() {
		return id.getPrescriptionCode();
	}
	
	public LocalDate getPrescriptionDate() {
		return id.getPrescriptionDate();
	}
	
	public LocalTime getPrescriptionTime() {
		return id.getPrescriptionTime();
	}
	
	public String getDoctorCode() {
		return id.getDoctorCode();
	}

	public String getPatientCode() {
		return patientCode;
	}
	
	public LocalDate getDiagnosisDate() {
		return diagnosisDate;
	}
	
	public LocalTime getDiagnosisTime() {
		return diagnosisTime;
	}
	
	public String getDoctorNotes() {
		return doctorNotes;
	}

	

	public long getVersion() {
		return version;
	}

	public void setPrescriptionCode(String prescriptionCode) {
		this.id.setPrescriptionCode(prescriptionCode);
	}
	
	public void setPrescriptionDate(LocalDate prescriptionDate) {
		this.id.setPrescriptionDate(prescriptionDate);
	}
	
	public void setPrescriptionTime(LocalTime prescriptionTime) {
		this.id.setPrescriptionTime(prescriptionTime);
	}
	
	public void setDoctorCode(String doctorCode) {
		this.id.setDoctorCode(doctorCode);
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}
	
	public void setDiagnosisDate(LocalDate diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}
	
	public void setDiagnosisTime(LocalTime diagnosisTime) {
		this.diagnosisTime = diagnosisTime;
	}
	
	public void setDoctorNotes(String doctorNotes) {
		this.doctorNotes = doctorNotes;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
