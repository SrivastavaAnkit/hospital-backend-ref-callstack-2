package com.hospital.file.prescription.rtvdiagnosisexist;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvDiagnosisExistDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private LocalDate diagnosisDate;
	private LocalTime diagnosisTime;
	private ReturnCodeEnum returnCode;
	private String patientCode;
	private UsrReturnCodeEnum usrReturnCode;
	private String nextScreen;

	public LocalDate getDiagnosisDate() {
		return diagnosisDate;
	}

	public LocalTime getDiagnosisTime() {
		return diagnosisTime;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public UsrReturnCodeEnum getUsrReturnCode() {
		return usrReturnCode;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setDiagnosisDate(LocalDate diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}

	public void setDiagnosisTime(LocalTime diagnosisTime) {
		this.diagnosisTime = diagnosisTime;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public void setUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
		this.usrReturnCode = usrReturnCode;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
