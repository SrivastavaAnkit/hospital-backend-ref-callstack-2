package com.hospital.file.prescription.rtvdiagnosisexist;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.prescription.Prescription;
import com.hospital.file.prescription.PrescriptionId;
import com.hospital.file.prescription.PrescriptionRepository;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;


import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;
 
@Service
public class RtvDiagnosisExistService extends AbstractService<RtvDiagnosisExistService, RtvDiagnosisExistDTO>
{
    private final Step execute = define("execute", RtvDiagnosisExistDTO.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PrescriptionRepository prescriptionRepository;
	
    @Autowired
    public RtvDiagnosisExistService()
    {
        super(RtvDiagnosisExistService.class, RtvDiagnosisExistDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}
	
	public StepResult execute(RtvDiagnosisExistDTO dto) throws ServiceException
	{
		return executeService(dto, dto);
	}

	public StepResult executeService(RtvDiagnosisExistDTO dto, RtvDiagnosisExistDTO params) throws ServiceException
    {
        StepResult result = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Prescription> prescriptionList = prescriptionRepository.findAllByPatientCodeAndDiagnosisDateAndDiagnosisTime(dto.getPatientCode(), dto.getDiagnosisDate(), dto.getDiagnosisTime());
		if (!prescriptionList.isEmpty()) {
			for (Prescription prescription : prescriptionList) {
				processDataRecord(dto, prescription);
			}
			exitProcessing(dto);
		}
		else {
			processingIfDataRecordNotFound(dto);
		}
 
        return result;
	}

    private StepResult initializeRoutine(RtvDiagnosisExistDTO dto) throws ServiceException
    {
		/**
		 * USER: Initialize Routine (Empty:48)
		 */
		//switchSUB 48 SUB    
		// Unprocessed SUB 48 -
        return NO_ACTION;
    }

    private StepResult processDataRecord(RtvDiagnosisExistDTO dto, Prescription prescription) throws ServiceException
    {
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		//switchSUB 41 SUB    
		//switchBLK 1000009 BLK ACT
		//functionCall 1000010 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
		//switchBLK 1000013 BLK ACT
		//functionCall 1000014 ACT <-- *QUIT
		// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
        return NO_ACTION;
    }

    private StepResult processingIfDataRecordNotFound(RtvDiagnosisExistDTO dto) throws ServiceException
    {
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		//switchSUB 52 SUB    
		//switchBLK 1000003 BLK ACT
		//functionCall 1000004 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
         return NO_ACTION;
   }

    private StepResult exitProcessing(RtvDiagnosisExistDTO dto) throws ServiceException
    {
		/**
		 * USER: Exit Processing (Empty:61)
		 */
		//switchSUB 61 SUB    
		// Unprocessed SUB 61 -
        return NO_ACTION;
    }
}
