package com.hospital.file.prescription.trneditprescriptions;

import com.hospital.common.exception.ServiceException;

/**
 * Service interface for resource: TrnEditPrescriptions (TSAZETR).
 *
 * @author X2EGenerator
 */
public interface TrnEditPrescriptionsService {

	/**
	 * Initialize Program.
	 *
	 * @param dto TrnEditPrescriptionsDTO initialized by the controller
	 * @param params function parameters
	 * @throws ServiceException if a service exception occurs
	 */
	void initialize(TrnEditPrescriptionsDTO dto, TrnEditPrescriptionsParams params) throws ServiceException;

	/**
	 * Initialize Screen for New Transaction.
	 *
	 * @param dto TrnEditPrescriptionsDTO initialized by the controller
	 * @param params function parameters
	 * @throws ServiceException if a service exception occurs
	 */
	void initializeScreenNewTransaction(TrnEditPrescriptionsDTO dto, TrnEditPrescriptionsParams params) throws ServiceException;

	/**
	 * Initialize Screen for Old Transaction.
	 *
	 * @param dto TrnEditPrescriptionsDTO initialized by the controller
	 * @param params function parameters
	 * @throws ServiceException if a service exception occurs
	 */
	void initializeScreenOldTransaction(TrnEditPrescriptionsDTO dto, TrnEditPrescriptionsParams params) throws ServiceException;
}
