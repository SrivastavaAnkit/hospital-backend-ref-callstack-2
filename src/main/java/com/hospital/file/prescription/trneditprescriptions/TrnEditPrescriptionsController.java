package com.hospital.file.prescription.trneditprescriptions;

import java.io.IOException;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;

import com.hospital.file.prescription.Prescription;
import com.hospital.file.prescription.PrescriptionId;

import com.hospital.model.ProgramModeEnum;

import com.hospital.file.prescription.PrescriptionRepository;
import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameService;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorService;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsService;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.medication.rtvdetail.RtvDetailService;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameService;
import com.hospital.file.prescription.changeprescription.ChangePrescriptionService;
import com.hospital.file.prescription.createprescription.CreatePrescriptionService;
import com.hospital.file.prescription.deleteprescription.DeletePrescriptionService;
import com.hospital.file.prescription.selectdrprescriptions.SelectDrPrescriptionsService;
import com.hospital.file.prescriptionline.changeprescriptionline.ChangePrescriptionLineService;
import com.hospital.file.prescriptionline.createprescriptionline.CreatePrescriptionLineService;
import com.hospital.file.prescriptionline.deleteprescriptionline.DeletePrescriptionLineService;
import com.hospital.file.prescriptionline.rtvnumberoflines.RtvNumberOfLinesService;
import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameDTO;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorDTO;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsDTO;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.file.medication.rtvdetail.RtvDetailDTO;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameDTO;
import com.hospital.file.prescription.changeprescription.ChangePrescriptionDTO;
import com.hospital.file.prescription.createprescription.CreatePrescriptionDTO;
import com.hospital.file.prescription.deleteprescription.DeletePrescriptionDTO;
import com.hospital.file.prescription.selectdrprescriptions.SelectDrPrescriptionsDTO;
import com.hospital.file.prescriptionline.changeprescriptionline.ChangePrescriptionLineDTO;
import com.hospital.file.prescriptionline.createprescriptionline.CreatePrescriptionLineDTO;
import com.hospital.file.prescriptionline.deleteprescriptionline.DeletePrescriptionLineDTO;
import com.hospital.file.prescriptionline.rtvnumberoflines.RtvNumberOfLinesDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsParams;
import com.hospital.file.prescription.selectdrprescriptions.SelectDrPrescriptionsParams;

import com.hospital.common.exception.ServiceException;

/**
 * Controller/EDTTRN for resource: TrnEditPrescriptions (TSAZETR).
 *
 * @author X2EGenerator
 */
@RestController()
public class TrnEditPrescriptionsController {

	@Autowired
	private PrescriptionRepository prescriptionRepository;


	@Autowired
	private ChangePrescriptionLineService changePrescriptionLineService;

	@Autowired
	private ChangePrescriptionService changePrescriptionService;

	@Autowired
	private CreateLogFileService createLogFileService;

	@Autowired
	private CreatePrescriptionLineService createPrescriptionLineService;

	@Autowired
	private CreatePrescriptionService createPrescriptionService;

	@Autowired
	private DeletePrescriptionLineService deletePrescriptionLineService;

	@Autowired
	private DeletePrescriptionService deletePrescriptionService;

	@Autowired
	private DspAllHospitalsService dspAllHospitalsService;

	@Autowired
	private RtvDetailService rtvDetailService;

	@Autowired
	private RtvDoctorNameService rtvDoctorNameService;

	@Autowired
	private RtvForSupervisorService rtvForSupervisorService;

	@Autowired
	private RtvNumberOfLinesService rtvNumberOfLinesService;

	@Autowired
	private RtvPatientNameService rtvPatientNameService;

	@Autowired
	private SelectDrPrescriptionsService selectDrPrescriptionsService;

	@Autowired
	private TrnEditPrescriptionsService trnEditPrescriptionsService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private TrnEditPrescriptionsValidator trnEditPrescriptionsValidator;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(trnEditPrescriptionsValidator);
	}

	/*@Transactional
	@RequestMapping(value="/Prescription/TrnEditPrescriptions/{doctorCode}/{patientCode}", method=RequestMethod.GET, produces="application/json;charset=utf-8")
	public TrnEditPrescriptionsDTO getPrescription(@PathVariable String doctorCode, @PathVariable String patientCode, String mode) throws ServiceException {

		TrnEditPrescriptionsParams params = new TrnEditPrescriptionsParams();
		params.setDoctorCode(doctorCode);
		params.setPatientCode(patientCode);

		TrnEditPrescriptionsDTO dto = prescriptionRepository.trnEditPrescriptions(params.getDoctorCode(), params.getPatientCode());
		trnEditPrescriptionsService.initialize(dto, params);

		if ("ADD".equals(mode)) {
			trnEditPrescriptionsService.initializeScreenNewTransaction(dto, params);
		}

		if ("CHANGE".equals(mode)) {
			trnEditPrescriptionsService.initializeScreenOldTransaction(dto, params);
		}

		return dto;
	}

	@Transactional
	@RequestMapping(value="/Prescription/TrnEditPrescriptions/{doctorCode}/{prescriptionCode}/{prescriptionDate}/{prescriptionTime}", method=RequestMethod.POST)
	public TrnEditPrescriptionsDTO savePrescription(@PathVariable String doctorCode, @PathVariable String prescriptionCode, @PathVariable LocalDate prescriptionDate, @PathVariable LocalTime prescriptionTime, @RequestBody @Valid TrnEditPrescriptionsDTO trnEditPrescriptionsDto) throws ServiceException {

		CreatePrescriptionDTO createPrescriptionDTO = new CreatePrescriptionDTO();
		createPrescriptionDTO.setDoctorCode(trnEditPrescriptionsDto.getDoctorCode());
		createPrescriptionDTO.setPrescriptionCode(trnEditPrescriptionsDto.getPrescriptionCode());
		createPrescriptionDTO.setPrescriptionDate(trnEditPrescriptionsDto.getPrescriptionDate());
		createPrescriptionDTO.setPrescriptionTime(trnEditPrescriptionsDto.getPrescriptionTime());
		createPrescriptionDTO.setPatientCode(trnEditPrescriptionsDto.getPatientCode());
		createPrescriptionDTO.setDiagnosisDate(trnEditPrescriptionsDto.getDiagnosisDate());
		createPrescriptionDTO.setDiagnosisTime(trnEditPrescriptionsDto.getDiagnosisTime());
		createPrescriptionDTO.setDoctorNotes(trnEditPrescriptionsDto.getDoctorNotes());
		createPrescriptionService.execute(createPrescriptionDTO);
		trnEditPrescriptionsDto.setUsrReturnCode(createPrescriptionDTO.getUsrReturnCode());

		return trnEditPrescriptionsDto;
	}

	@Transactional
	@RequestMapping(value="/Prescription/TrnEditPrescriptions/{doctorCode}/{prescriptionCode}/{prescriptionDate}/{prescriptionTime}", method=RequestMethod.PUT)
	public TrnEditPrescriptionsDTO updatePrescription(@PathVariable String doctorCode, @PathVariable String prescriptionCode, @PathVariable LocalDate prescriptionDate, @PathVariable LocalTime prescriptionTime, @RequestBody @Valid TrnEditPrescriptionsDTO trnEditPrescriptionsDto) throws ServiceException {

		PrescriptionId prescriptionId = new PrescriptionId(doctorCode, prescriptionCode, prescriptionDate, prescriptionTime);
		if(prescriptionRepository.findOne(prescriptionId) != null) {
			ChangePrescriptionDTO changePrescriptionDTO = new ChangePrescriptionDTO();
			changePrescriptionDTO.setDoctorCode(trnEditPrescriptionsDto.getDoctorCode());
			changePrescriptionDTO.setPrescriptionCode(trnEditPrescriptionsDto.getPrescriptionCode());
			changePrescriptionDTO.setPrescriptionDate(trnEditPrescriptionsDto.getPrescriptionDate());
			changePrescriptionDTO.setPrescriptionTime(trnEditPrescriptionsDto.getPrescriptionTime());
			changePrescriptionDTO.setPatientCode(trnEditPrescriptionsDto.getPatientCode());
			changePrescriptionDTO.setDiagnosisDate(trnEditPrescriptionsDto.getDiagnosisDate());
			changePrescriptionDTO.setDiagnosisTime(trnEditPrescriptionsDto.getDiagnosisTime());
			changePrescriptionDTO.setDoctorNotes(trnEditPrescriptionsDto.getDoctorNotes());
			changePrescriptionService.execute(changePrescriptionDTO);
			trnEditPrescriptionsDto.setUsrReturnCode(changePrescriptionDTO.getUsrReturnCode());

			return trnEditPrescriptionsDto;
        }
        else {
			throw new ServiceException("prescription.nf");
        }
	}

	@Transactional
	@RequestMapping(value="/Prescription/TrnEditPrescriptions/{doctorCode}/{prescriptionCode}/{prescriptionDate}/{prescriptionTime}", method=RequestMethod.DELETE)
	public void deletePrescription(@PathVariable String doctorCode, @PathVariable String prescriptionCode, @PathVariable LocalDate prescriptionDate, @PathVariable LocalTime prescriptionTime) throws ServiceException {

		PrescriptionId prescriptionId = new PrescriptionId(doctorCode, prescriptionCode, prescriptionDate, prescriptionTime);
		Prescription prescription = prescriptionRepository.findOne(prescriptionId);
		DeletePrescriptionDTO deletePrescriptionDTO = new DeletePrescriptionDTO();
		deletePrescriptionDTO.setDoctorCode(doctorCode);
		deletePrescriptionDTO.setPrescriptionCode(prescriptionCode);
		deletePrescriptionDTO.setPrescriptionDate(prescriptionDate);
		deletePrescriptionDTO.setPrescriptionTime(prescriptionTime);
		deletePrescriptionService.execute(deletePrescriptionDTO);
	}

	@Transactional(readOnly=true)
	@RequestMapping(value={"/PrescriptionLine/TrnEditPrescriptions", "/PrescriptionLine/TrnEditPrescriptions/{doctorCode}/{prescriptionCode}/{prescriptionDate}/{prescriptionTime}"}, method=RequestMethod.GET, produces="application/json;charset=utf-8")
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public TrnEditPrescriptionsDTO selectTrnEditPrescriptions(@PathVariable Map pathVariables, 
		@RequestParam(value="page", required=false, defaultValue="0") int page,
		@RequestParam(value="size", required=false, defaultValue="10") int size,
		@RequestParam(value="sortdata", required=false) String sortData,
 		HttpServletRequest request) throws ServiceException {

		TrnEditPrescriptionsParams params = new TrnEditPrescriptionsParams();
		if (pathVariables != null) {
			if (pathVariables.containsKey("doctorCode") &&
				!"undefined".equals((String)pathVariables.get("doctorCode")) &&
				!"null".equals((String)pathVariables.get("doctorCode"))) {
				params.setDoctorCode((String)pathVariables.get("doctorCode"));
			}

			if (pathVariables.containsKey("prescriptionCode") &&
				!"undefined".equals((String)pathVariables.get("prescriptionCode")) &&
				!"null".equals((String)pathVariables.get("prescriptionCode"))) {
				params.setPrescriptionCode((String)pathVariables.get("prescriptionCode"));
			}

			if (pathVariables.containsKey("prescriptionDate") &&
				!"undefined".equals((String)pathVariables.get("prescriptionDate")) &&
				!"null".equals((String)pathVariables.get("prescriptionDate"))) {
				params.setPrescriptionDate((String)pathVariables.get("prescriptionDate"));
			}

			if (pathVariables.containsKey("prescriptionTime") &&
				!"undefined".equals((String)pathVariables.get("prescriptionTime")) &&
				!"null".equals((String)pathVariables.get("prescriptionTime"))) {
				params.setPrescriptionTime((String)pathVariables.get("prescriptionTime"));
			}
		}

		TrnEditPrescriptionsDTO dto = new TrnEditPrescriptionsDTO();

		List<Order> sortOrders = new ArrayList<Order>();
		Pageable pageable;

		try {
			Map<String, String> sortDataMap =
				new ObjectMapper().readValue(sortData, LinkedHashMap.class);

			for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
				if (entry.getValue() == null) {
					continue;
				}

				sortOrders.add(new Order(Direction.fromString(entry.getValue()),
						entry.getKey()));
			}
		} catch (IOException ioe) {
		}

		if (CollectionUtils.isEmpty(sortOrders)) {
			pageable = new PageRequest(page, size);
		} else {
			pageable = new PageRequest(page, size, new Sort(sortOrders));
		}

		Page<TrnEditPrescriptionsGDO> pageDto = prescriptionRepository.trnEditPrescriptions(params.getDoctorCode(), params.getPrescriptionCode(), params.getPrescriptionDate(), params.getPrescriptionTime(), pageable);
		dto.setPageDto(pageDto);

		return dto;
	}

	@Transactional
	@RequestMapping(value="/PrescriptionLine/TrnEditPrescriptions/{doctorCode}/{prescriptionCode}/{prescriptionDate}/{prescriptionTime}", method=RequestMethod.POST)
	public TrnEditPrescriptionsDTO savePrescriptionLine(@PathVariable String doctorCode, @PathVariable String prescriptionCode, @PathVariable LocalDate prescriptionDate, @PathVariable LocalTime prescriptionTime, @RequestBody @Valid TrnEditPrescriptionsDTO trnEditPrescriptionsDto) throws ServiceException {

		TrnEditPrescriptionsGDO trnEditPrescriptionsGdo = trnEditPrescriptionsDto.getGdo();
		CreatePrescriptionLineDTO createPrescriptionLineDTO = new CreatePrescriptionLineDTO();
		createPrescriptionLineDTO.setDoctorCode(trnEditPrescriptionsGdo.getDoctorCode());
		createPrescriptionLineDTO.setPrescriptionCode(trnEditPrescriptionsGdo.getPrescriptionCode());
		createPrescriptionLineDTO.setPrescriptionDate(trnEditPrescriptionsGdo.getPrescriptionDate());
		createPrescriptionLineDTO.setPrescriptionTime(trnEditPrescriptionsGdo.getPrescriptionTime());
		createPrescriptionLineDTO.setPrescriptionLineNumber(trnEditPrescriptionsGdo.getPrescriptionLineNumber());
		createPrescriptionLineDTO.setMedicationCode(trnEditPrescriptionsGdo.getMedicationCode());
		createPrescriptionLineDTO.setPrescriptionQuantity(trnEditPrescriptionsGdo.getPrescriptionQuantity());
		createPrescriptionLineService.execute(createPrescriptionLineDTO);
		trnEditPrescriptionsGdo.setPrescriptionLineNumber(createPrescriptionLineDTO.getPrescriptionLineNumber());
		trnEditPrescriptionsGdo.setUsrReturnCode(createPrescriptionLineDTO.getUsrReturnCode());

		return trnEditPrescriptionsDto;
	}

	@Transactional
	@RequestMapping(value="/PrescriptionLine/TrnEditPrescriptions/{doctorCode}/{prescriptionCode}/{prescriptionDate}/{prescriptionTime}", method=RequestMethod.PUT)
	public TrnEditPrescriptionsDTO updatePrescriptionLine(@PathVariable String doctorCode, @PathVariable String prescriptionCode, @PathVariable LocalDate prescriptionDate, @PathVariable LocalTime prescriptionTime, @RequestBody @Valid TrnEditPrescriptionsDTO trnEditPrescriptionsDto) throws ServiceException {

		TrnEditPrescriptionsGDO trnEditPrescriptionsGdo = trnEditPrescriptionsDto.getGdo();
		ChangePrescriptionLineDTO changePrescriptionLineDTO = new ChangePrescriptionLineDTO();
		changePrescriptionLineDTO.setDoctorCode(trnEditPrescriptionsGdo.getDoctorCode());
		changePrescriptionLineDTO.setPrescriptionCode(trnEditPrescriptionsGdo.getPrescriptionCode());
		changePrescriptionLineDTO.setPrescriptionDate(trnEditPrescriptionsGdo.getPrescriptionDate());
		changePrescriptionLineDTO.setPrescriptionTime(trnEditPrescriptionsGdo.getPrescriptionTime());
		changePrescriptionLineDTO.setPrescriptionLineNumber(trnEditPrescriptionsGdo.getPrescriptionLineNumber());
		changePrescriptionLineDTO.setMedicationCode(trnEditPrescriptionsGdo.getMedicationCode());
		changePrescriptionLineDTO.setPrescriptionQuantity(trnEditPrescriptionsGdo.getPrescriptionQuantity());
		changePrescriptionLineService.execute(changePrescriptionLineDTO);
		trnEditPrescriptionsGdo.setUsrReturnCode(changePrescriptionLineDTO.getUsrReturnCode());

		return trnEditPrescriptionsDto;
	}

*/}
