package com.hospital.file.prescription.trneditprescriptions;

import java.time.LocalDate;
import java.time.LocalTime;

import com.hospital.config.LocalDateConverter;
import com.hospital.config.LocalTimeConverter;

/**
 * Params for resource: TrnEditPrescriptions (TSAZETR).
 *
 * @author X2EGenerator
 */
public class TrnEditPrescriptionsParams {
	private String doctorCode = "";
	private String patientCode = "";
	private String prescriptionCode = "";
	private LocalDate prescriptionDate = null;
	private LocalTime prescriptionTime = null;

	public String getDoctorCode() {
		return doctorCode;
	}
	
	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}
	
	public String getPatientCode() {
		return patientCode;
	}
	
	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public String getPrescriptionCode() {
		return prescriptionCode;
	}
	
	public void setPrescriptionCode(String prescriptionCode) {
		this.prescriptionCode = prescriptionCode;
	}
	
	public LocalDate getPrescriptionDate() {
		return prescriptionDate;
	}
	
	public void setPrescriptionDate(LocalDate prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
	}
	
	public void setPrescriptionDate(String prescriptionDate) {
		setPrescriptionDate(new LocalDateConverter().convert(prescriptionDate));
	}
	
	public LocalTime getPrescriptionTime() {
		return prescriptionTime;
	}
	
	public void setPrescriptionTime(LocalTime prescriptionTime) {
		this.prescriptionTime = prescriptionTime;
	}
	
	public void setPrescriptionTime(String prescriptionTime) {
		setPrescriptionTime(new LocalTimeConverter().convert(prescriptionTime));
	}
}
