package com.hospital.file.prescription.trneditprescriptions;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.data.domain.Page;

import com.hospital.model.GlobalContext;
import com.hospital.file.prescription.Prescription;

import com.hospital.common.state.BaseDTO;

/**
 * Dto for file 'Prescription' (TSAHCPP) and function 'TRN Edit Prescriptions' (TSAZETR).
 */
@Configurable
public class TrnEditPrescriptionsDTO extends BaseDTO {
	private static final long serialVersionUID = -2638110454155096372L;

	@Autowired
	private GlobalContext globalCtx;

	private String doctorCode = "";
	private String doctorName = "";
	private String prescriptionCode = "";
	private LocalDate prescriptionDate = null;
	private LocalTime prescriptionTime = null;
	private String patientCode = "";
	private String patientSurname = "";
	private LocalDate diagnosisDate = null;
	private LocalTime diagnosisTime = null;
	private String doctorNotes = "";
	private long usrNbrOfLines = 0;
	private String addedUser = "";
	private LocalDate addedDate = null;
	private LocalTime addedTime = null;
	private String changedUser = "";
	private LocalDate changedDate = null;
	private LocalTime changedTime = null;
	private long highestLineNumber = 0;
	private String rcdDoctorCode = "";
	private String rcdPrescriptionCode = "";
	private LocalDate rcdPrescriptionDate = null;
	private LocalTime rcdPrescriptionTime = null;
	private long rcdPrescriptionLineNumber = 0;
	private String rcdMedicationCode = "";
	private String rcdMedicationDescription = "";
	private long rcdPrescriptionQuantity = 0;
	private String rcdAddedUser = "";
	private LocalDate rcdAddedDate = null;
	private LocalTime rcdAddedTime = null;
	private String rcdChangedUser = "";
	private LocalDate rcdChangedDate = null;
	private LocalTime rcdChangedTime = null;
	private Page<TrnEditPrescriptionsGDO> pageDto;
	private TrnEditPrescriptionsGDO gdo;

	public TrnEditPrescriptionsDTO() {

	}

	public TrnEditPrescriptionsDTO(Prescription prescription) {
		setDtoFields(prescription);
	}

	public TrnEditPrescriptionsDTO(String doctorCode, String prescriptionCode, LocalDate prescriptionDate, LocalTime prescriptionTime, String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime, String doctorNotes, String addedUser, LocalDate addedDate, LocalTime addedTime, String changedUser, LocalDate changedDate, LocalTime changedTime) {
		this.doctorCode = doctorCode;
		this.prescriptionCode = prescriptionCode;
		this.prescriptionDate = prescriptionDate;
		this.prescriptionTime = prescriptionTime;
		this.patientCode = patientCode;
		this.diagnosisDate = diagnosisDate;
		this.diagnosisTime = diagnosisTime;
		this.doctorNotes = doctorNotes;
		this.addedUser = addedUser;
		this.addedDate = addedDate;
		this.addedTime = addedTime;
		this.changedUser = changedUser;
		this.changedDate = changedDate;
		this.changedTime = changedTime;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setPrescriptionCode(String prescriptionCode) {
		this.prescriptionCode = prescriptionCode;
	}

	public String getPrescriptionCode() {
		return prescriptionCode;
	}

	public void setPrescriptionDate(LocalDate prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
	}

	public LocalDate getPrescriptionDate() {
		return prescriptionDate;
	}

	public void setPrescriptionTime(LocalTime prescriptionTime) {
		this.prescriptionTime = prescriptionTime;
	}

	public LocalTime getPrescriptionTime() {
		return prescriptionTime;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public void setPatientSurname(String patientSurname) {
		this.patientSurname = patientSurname;
	}

	public String getPatientSurname() {
		return patientSurname;
	}

	public void setDiagnosisDate(LocalDate diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}

	public LocalDate getDiagnosisDate() {
		return diagnosisDate;
	}

	public void setDiagnosisTime(LocalTime diagnosisTime) {
		this.diagnosisTime = diagnosisTime;
	}

	public LocalTime getDiagnosisTime() {
		return diagnosisTime;
	}

	public void setDoctorNotes(String doctorNotes) {
		this.doctorNotes = doctorNotes;
	}

	public String getDoctorNotes() {
		return doctorNotes;
	}

	public void setUsrNbrOfLines(long usrNbrOfLines) {
		this.usrNbrOfLines = usrNbrOfLines;
	}

	public long getUsrNbrOfLines() {
		return usrNbrOfLines;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedDate(LocalDate addedDate) {
		this.addedDate = addedDate;
	}

	public LocalDate getAddedDate() {
		return addedDate;
	}

	public void setAddedTime(LocalTime addedTime) {
		this.addedTime = addedTime;
	}

	public LocalTime getAddedTime() {
		return addedTime;
	}

	public void setChangedUser(String changedUser) {
		this.changedUser = changedUser;
	}

	public String getChangedUser() {
		return changedUser;
	}

	public void setChangedDate(LocalDate changedDate) {
		this.changedDate = changedDate;
	}

	public LocalDate getChangedDate() {
		return changedDate;
	}

	public void setChangedTime(LocalTime changedTime) {
		this.changedTime = changedTime;
	}

	public LocalTime getChangedTime() {
		return changedTime;
	}

	public void setHighestLineNumber(long highestLineNumber) {
		this.highestLineNumber = highestLineNumber;
	}

	public long getHighestLineNumber() {
		return highestLineNumber;
	}

	public void setRcdDoctorCode(String rcdDoctorCode) {
		this.rcdDoctorCode = rcdDoctorCode;
	}

	public String getRcdDoctorCode() {
		return rcdDoctorCode;
	}

	public void setRcdPrescriptionCode(String rcdPrescriptionCode) {
		this.rcdPrescriptionCode = rcdPrescriptionCode;
	}

	public String getRcdPrescriptionCode() {
		return rcdPrescriptionCode;
	}

	public void setRcdPrescriptionDate(LocalDate rcdPrescriptionDate) {
		this.rcdPrescriptionDate = rcdPrescriptionDate;
	}

	public LocalDate getRcdPrescriptionDate() {
		return rcdPrescriptionDate;
	}

	public void setRcdPrescriptionTime(LocalTime rcdPrescriptionTime) {
		this.rcdPrescriptionTime = rcdPrescriptionTime;
	}

	public LocalTime getRcdPrescriptionTime() {
		return rcdPrescriptionTime;
	}

	public void setRcdPrescriptionLineNumber(long rcdPrescriptionLineNumber) {
		this.rcdPrescriptionLineNumber = rcdPrescriptionLineNumber;
	}

	public long getRcdPrescriptionLineNumber() {
		return rcdPrescriptionLineNumber;
	}

	public void setRcdMedicationCode(String rcdMedicationCode) {
		this.rcdMedicationCode = rcdMedicationCode;
	}

	public String getRcdMedicationCode() {
		return rcdMedicationCode;
	}

	public void setRcdMedicationDescription(String rcdMedicationDescription) {
		this.rcdMedicationDescription = rcdMedicationDescription;
	}

	public String getRcdMedicationDescription() {
		return rcdMedicationDescription;
	}

	public void setRcdPrescriptionQuantity(long rcdPrescriptionQuantity) {
		this.rcdPrescriptionQuantity = rcdPrescriptionQuantity;
	}

	public long getRcdPrescriptionQuantity() {
		return rcdPrescriptionQuantity;
	}

	public void setRcdAddedUser(String rcdAddedUser) {
		this.rcdAddedUser = rcdAddedUser;
	}

	public String getRcdAddedUser() {
		return rcdAddedUser;
	}

	public void setRcdAddedDate(LocalDate rcdAddedDate) {
		this.rcdAddedDate = rcdAddedDate;
	}

	public LocalDate getRcdAddedDate() {
		return rcdAddedDate;
	}

	public void setRcdAddedTime(LocalTime rcdAddedTime) {
		this.rcdAddedTime = rcdAddedTime;
	}

	public LocalTime getRcdAddedTime() {
		return rcdAddedTime;
	}

	public void setRcdChangedUser(String rcdChangedUser) {
		this.rcdChangedUser = rcdChangedUser;
	}

	public String getRcdChangedUser() {
		return rcdChangedUser;
	}

	public void setRcdChangedDate(LocalDate rcdChangedDate) {
		this.rcdChangedDate = rcdChangedDate;
	}

	public LocalDate getRcdChangedDate() {
		return rcdChangedDate;
	}

	public void setRcdChangedTime(LocalTime rcdChangedTime) {
		this.rcdChangedTime = rcdChangedTime;
	}

	public LocalTime getRcdChangedTime() {
		return rcdChangedTime;
	}

	public void setWfPatientName(String patientName) {
		globalCtx.setString("patientName", patientName);
	}

	public String getWfPatientName() {
		return globalCtx.getString("patientName");
	}

	public void setPageDto(Page<TrnEditPrescriptionsGDO> pageDto) {
		this.pageDto = pageDto;
	}

	public Page<TrnEditPrescriptionsGDO> getPageDto() {
		return pageDto;
	}

	public void setGdo(TrnEditPrescriptionsGDO gdo) {
		this.gdo = gdo;
	}

	public TrnEditPrescriptionsGDO getGdo() {
		return gdo;
	}

	/**
	 * Copies the fields of the Entity bean into the DTO bean.
	 *
	 * @param prescription Prescription Entity bean
	 */
	public void setDtoFields(Prescription prescription) {
		BeanUtils.copyProperties(prescription, this);
	}

	/**
	 * Copies the fields of the DTO bean into the Entity bean.
	 *
	 * @param prescription Prescription Entity bean
	 */
	public void setEntityFields(Prescription prescription) {
		BeanUtils.copyProperties(this, prescription);
	}
}
