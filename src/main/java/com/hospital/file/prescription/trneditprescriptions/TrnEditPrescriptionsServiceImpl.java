package com.hospital.file.prescription.trneditprescriptions;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.hospital.file.logfile.LogFile;
import com.hospital.file.prescription.Prescription;
import com.hospital.file.prescriptionline.PrescriptionLine;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.file.prescription.PrescriptionRepository;

import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameService;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorService;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsService;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.medication.rtvdetail.RtvDetailService;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameService;
import com.hospital.file.prescription.changeprescription.ChangePrescriptionService;
import com.hospital.file.prescription.createprescription.CreatePrescriptionService;
import com.hospital.file.prescription.deleteprescription.DeletePrescriptionService;
import com.hospital.file.prescription.selectdrprescriptions.SelectDrPrescriptionsService;
import com.hospital.file.prescriptionline.changeprescriptionline.ChangePrescriptionLineService;
import com.hospital.file.prescriptionline.createprescriptionline.CreatePrescriptionLineService;
import com.hospital.file.prescriptionline.deleteprescriptionline.DeletePrescriptionLineService;
import com.hospital.file.prescriptionline.rtvnumberoflines.RtvNumberOfLinesService;
import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameDTO;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorDTO;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsDTO;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.file.medication.rtvdetail.RtvDetailDTO;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameDTO;
import com.hospital.file.prescription.changeprescription.ChangePrescriptionDTO;
import com.hospital.file.prescription.createprescription.CreatePrescriptionDTO;
import com.hospital.file.prescription.deleteprescription.DeletePrescriptionDTO;
import com.hospital.file.prescription.selectdrprescriptions.SelectDrPrescriptionsDTO;
import com.hospital.file.prescriptionline.changeprescriptionline.ChangePrescriptionLineDTO;
import com.hospital.file.prescriptionline.createprescriptionline.CreatePrescriptionLineDTO;
import com.hospital.file.prescriptionline.deleteprescriptionline.DeletePrescriptionLineDTO;
import com.hospital.file.prescriptionline.rtvnumberoflines.RtvNumberOfLinesDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsParams;
import com.hospital.file.prescription.selectdrprescriptions.SelectDrPrescriptionsParams;


import com.hospital.common.exception.ServiceException;


/**
 * Service implementation for resource: TrnEditPrescriptions (TSAZETR).
 *
 * @author X2EGenerator
 */
@Service
public class TrnEditPrescriptionsServiceImpl implements TrnEditPrescriptionsService {

	@Autowired
	private PrescriptionRepository prescriptionRepository;

	
	@Autowired
	private ChangePrescriptionLineService changePrescriptionLineService;
	
	@Autowired
	private ChangePrescriptionService changePrescriptionService;
	
	@Autowired
	private CreateLogFileService createLogFileService;
	
	@Autowired
	private CreatePrescriptionLineService createPrescriptionLineService;
	
	@Autowired
	private CreatePrescriptionService createPrescriptionService;
	
	@Autowired
	private DeletePrescriptionLineService deletePrescriptionLineService;
	
	@Autowired
	private DeletePrescriptionService deletePrescriptionService;
	
	@Autowired
	private RtvDetailService rtvDetailService;
	
	@Autowired
	private RtvDoctorNameService rtvDoctorNameService;
	
	@Autowired
	private RtvForSupervisorService rtvForSupervisorService;
	
	@Autowired
	private RtvNumberOfLinesService rtvNumberOfLinesService;
	
	@Autowired
	private RtvPatientNameService rtvPatientNameService;
	

	@Autowired
	private MessageSource messageSource;


	@Override
	public void initialize(TrnEditPrescriptionsDTO dto, TrnEditPrescriptionsParams params) throws ServiceException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void initializeScreenNewTransaction(TrnEditPrescriptionsDTO dto, TrnEditPrescriptionsParams params)
			throws ServiceException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void initializeScreenOldTransaction(TrnEditPrescriptionsDTO dto, TrnEditPrescriptionsParams params)
			throws ServiceException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Initialize Program.
	 *
	 * @param dto TrnEditPrescriptionsDTO initialized by the controller
	 * @param params function parameters
	 * @throws ServiceException if a service exception occurs
	 */
	/*public void initialize(TrnEditPrescriptionsDTO dto,	TrnEditPrescriptionsParams params) throws ServiceException {

		*//**
		 * USER: Initialize Program (Generated:3)
		 *//*
		CreateLogFileDTO createLogFileDTO;
		//switchSUB 3 SUB    
		//switchBLK 1000169 BLK ACT
		//functionCall 1000170 ACT PAR.*Return code = CND.*Normal
		dto.setReturnCode(ReturnCodeEnum.fromCode(""));
		//switchBLK 1000240 BLK TXT
		// 
		//switchBLK 1000241 BLK ACT
		//functionCall 1000242 ACT LCL.Log Function Type = CON.EDTTRN
		dto.setLogFunctionType("EDTTRN");
		//switchBLK 1000245 BLK ACT
		//functionCall 1000246 ACT LCL.Log User Point = CON.Initialize program
		dto.setLogUserPoint("Initialize program");
		//switchBLK 1000255 BLK ACT
		//functionCall 1000256 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
	}

	*//**
	 * Initialize Screen for New Transaction.
	 *
	 * @param dto TrnEditPrescriptionsDTO initialized by the controller
	 * @param params function parameters
	 * @throws ServiceException if a service exception occurs
	 *//*
	public void initializeScreenNewTransaction(TrnEditPrescriptionsDTO dto, TrnEditPrescriptionsParams params) throws ServiceException {

		*//**
		 * USER: Initialize Screen for New Transaction (Generated:1335)
		 *//*
		CreateLogFileDTO createLogFileDTO;
		RtvForSupervisorDTO rtvForSupervisorDTO;
		RtvPatientNameDTO rtvPatientNameDTO;
		//switchSUB 1335 SUB    
		//switchBLK 1000027 BLK ACT
		//functionCall 1000028 ACT CTL.Prescription Date = JOB.*Job date
		dto.setPrescriptionDate(LocalDate.now());
		//switchBLK 1000033 BLK ACT
		//functionCall 1000034 ACT CTL.Prescription Time = JOB.*Job time
		dto.setPrescriptionTime(LocalTime.now());
		//switchBLK 1000071 BLK ACT
		//functionCall 1000072 ACT CTL.Doctor Code = PAR.Doctor Code
		dto.setDoctorCode(params.getDoctorCode());
		//switchBLK 1000077 BLK ACT
		//functionCall 1000078 ACT CTL.Patient Code = PAR.Patient Code
		dto.setPatientCode(params.getPatientCode());
		//switchBLK 1000088 BLK ACT
		//functionCall 1000089 ACT RTV For Supervisor - Doctor  *
		rtvForSupervisorDTO = new RtvForSupervisorDTO();
		rtvForSupervisorDTO.setDoctorCode(dto.getDoctorCode());
		rtvForSupervisorService.execute(rtvForSupervisorDTO);
		dto.setDoctorName(rtvForSupervisorDTO.getDoctorName());
		//switchBLK 1000092 BLK ACT
		//functionCall 1000093 ACT RTV Patient Name - Patient  *
		rtvPatientNameDTO = new RtvPatientNameDTO();
		rtvPatientNameDTO.setPatientCode(dto.getPatientCode());
		rtvPatientNameService.execute(rtvPatientNameDTO);
		dto.setWfPatientName(rtvPatientNameDTO.getPatientName());
		dto.setPatientSurname(rtvPatientNameDTO.getPatientSurname());
		//switchBLK 1000306 BLK TXT
		// 
		//switchBLK 1000307 BLK ACT
		//functionCall 1000308 ACT LCL.Log Function Type = CON.EDTTRN
		dto.setLogFunctionType("EDTTRN");
		//switchBLK 1000311 BLK ACT
		//functionCall 1000312 ACT LCL.Log User Point = CON.Initialize screen for
		dto.setLogUserPoint("Initialize screen for");
		//switchBLK 1000315 BLK ACT
		//functionCall 1000316 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.new transaction,CND.*One)
		dto.setLogUserPoint(String.format("%s%1s%s", dto.getLogUserPoint().trim(), "", "new transaction"));
		//switchBLK 1000321 BLK ACT
		//functionCall 1000322 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
	}

	*//**
	 * Initialize Screen for Old Transaction.
	 *
	 * @param dto TrnEditPrescriptionsDTO initialized by the controller
	 * @param params function parameters
	 * @throws ServiceException if a service exception occurs
	 *//*
	public void initializeScreenOldTransaction(TrnEditPrescriptionsDTO dto, TrnEditPrescriptionsParams params) throws ServiceException {

		*//**
		 * USER: Initialize Screen for Old Transaction (Generated:1353)
		 *//*
		CreateLogFileDTO createLogFileDTO;
		RtvForSupervisorDTO rtvForSupervisorDTO;
		RtvPatientNameDTO rtvPatientNameDTO;
		//switchSUB 1353 SUB    
		//switchBLK 1000023 BLK ACT
		//functionCall 1000024 ACT RTV For Supervisor - Doctor  *
		rtvForSupervisorDTO = new RtvForSupervisorDTO();
		rtvForSupervisorDTO.setDoctorCode(dto.getDoctorCode());
		rtvForSupervisorService.execute(rtvForSupervisorDTO);
		dto.setDoctorName(rtvForSupervisorDTO.getDoctorName());
		//switchBLK 1000083 BLK ACT
		//functionCall 1000084 ACT RTV Patient Name - Patient  *
		rtvPatientNameDTO = new RtvPatientNameDTO();
		rtvPatientNameDTO.setPatientCode(dto.getPatientCode());
		rtvPatientNameService.execute(rtvPatientNameDTO);
		dto.setWfPatientName(rtvPatientNameDTO.getPatientName());
		dto.setPatientSurname(rtvPatientNameDTO.getPatientSurname());
		//switchBLK 1000356 BLK TXT
		// 
		//switchBLK 1000357 BLK ACT
		//functionCall 1000358 ACT LCL.Log Function Type = CON.EDTTRN
		dto.setLogFunctionType("EDTTRN");
		//switchBLK 1000361 BLK ACT
		//functionCall 1000362 ACT LCL.Log User Point = CON.Initialize screen for
		dto.setLogUserPoint("Initialize screen for");
		//switchBLK 1000365 BLK ACT
		//functionCall 1000366 ACT LCL.Log User Point = CONCAT(LCL.Log User Point,CON.old transaction,CND.*One)
		dto.setLogUserPoint(String.format("%s%1s%s", dto.getLogUserPoint().trim(), "", "old transaction"));
		//switchBLK 1000371 BLK ACT
		//functionCall 1000372 ACT Create Log File - Log File  *
		createLogFileDTO = new CreateLogFileDTO();
		//createLogFileDTO.setLogTimestamp(job.getSystemTimestamp());
		// TODO: Unsupported Action Diagram Context: LCL
		// TODO: Unsupported Action Diagram Context: LCL
		createLogFileService.execute(createLogFileDTO);
	}
*/}
