package com.hospital.file.prescription.trneditprescriptions;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


import com.hospital.common.exception.ServiceException;

import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameService;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorService;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsService;
import com.hospital.file.logfile.createlogfile.CreateLogFileService;
import com.hospital.file.medication.rtvdetail.RtvDetailService;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameService;
import com.hospital.file.prescription.changeprescription.ChangePrescriptionService;
import com.hospital.file.prescription.createprescription.CreatePrescriptionService;
import com.hospital.file.prescription.deleteprescription.DeletePrescriptionService;
import com.hospital.file.prescription.selectdrprescriptions.SelectDrPrescriptionsService;
import com.hospital.file.prescriptionline.changeprescriptionline.ChangePrescriptionLineService;
import com.hospital.file.prescriptionline.createprescriptionline.CreatePrescriptionLineService;
import com.hospital.file.prescriptionline.deleteprescriptionline.DeletePrescriptionLineService;
import com.hospital.file.prescriptionline.rtvnumberoflines.RtvNumberOfLinesService;
import com.hospital.file.doctor.rtvdoctorname.RtvDoctorNameDTO;
import com.hospital.file.doctor.rtvforsupervisor.RtvForSupervisorDTO;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsDTO;
import com.hospital.file.logfile.createlogfile.CreateLogFileDTO;
import com.hospital.file.medication.rtvdetail.RtvDetailDTO;
import com.hospital.file.patient.rtvpatientname.RtvPatientNameDTO;
import com.hospital.file.prescription.changeprescription.ChangePrescriptionDTO;
import com.hospital.file.prescription.createprescription.CreatePrescriptionDTO;
import com.hospital.file.prescription.deleteprescription.DeletePrescriptionDTO;
import com.hospital.file.prescription.selectdrprescriptions.SelectDrPrescriptionsDTO;
import com.hospital.file.prescriptionline.changeprescriptionline.ChangePrescriptionLineDTO;
import com.hospital.file.prescriptionline.createprescriptionline.CreatePrescriptionLineDTO;
import com.hospital.file.prescriptionline.deleteprescriptionline.DeletePrescriptionLineDTO;
import com.hospital.file.prescriptionline.rtvnumberoflines.RtvNumberOfLinesDTO;
import com.hospital.model.CmdKeyEnum;
import com.hospital.model.MedicationUnitEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;
import com.hospital.file.hospital.dspallhospitals.DspAllHospitalsParams;
import com.hospital.file.prescription.selectdrprescriptions.SelectDrPrescriptionsParams;


/**
 * Spring Validator for file 'Prescription' (TSAHCPP) and function 'TRN Edit Prescriptions' (TSAZETR).
 *
 * @author X2EGenerator
 */
@Component
public class TrnEditPrescriptionsValidator implements Validator {
	
	
	@Autowired
	private ChangePrescriptionLineService changePrescriptionLineService;
	
	@Autowired
	private ChangePrescriptionService changePrescriptionService;
	
	@Autowired
	private CreateLogFileService createLogFileService;
	
	@Autowired
	private CreatePrescriptionLineService createPrescriptionLineService;
	
	@Autowired
	private CreatePrescriptionService createPrescriptionService;
	
	@Autowired
	private DeletePrescriptionLineService deletePrescriptionLineService;
	
	@Autowired
	private DeletePrescriptionService deletePrescriptionService;
	
	@Autowired
	private RtvDetailService rtvDetailService;
	
	@Autowired
	private RtvDoctorNameService rtvDoctorNameService;
	
	@Autowired
	private RtvForSupervisorService rtvForSupervisorService;
	
	@Autowired
	private RtvNumberOfLinesService rtvNumberOfLinesService;
	
	@Autowired
	private RtvPatientNameService rtvPatientNameService;
	

	@Override
	public boolean supports(Class<?> clazz) {
		return TrnEditPrescriptionsDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors e) {
		if (!(object instanceof TrnEditPrescriptionsDTO)) {
			e.reject("Not a valid TrnEditPrescriptionsDTO");
		}
	}
}
