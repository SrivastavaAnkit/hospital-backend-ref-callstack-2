package com.hospital.file.prescription.createprescription;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.prescription.Prescription;
import com.hospital.file.prescription.PrescriptionId;
import com.hospital.file.prescription.PrescriptionRepository;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class CreatePrescriptionService  extends AbstractService<CreatePrescriptionService, CreatePrescriptionDTO>
{
    private final Step execute = define("execute", CreatePrescriptionDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private PrescriptionRepository prescriptionRepository;
	

    @Autowired
    public CreatePrescriptionService() {
        super(CreatePrescriptionService.class, CreatePrescriptionDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(CreatePrescriptionDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(CreatePrescriptionDTO dto, CreatePrescriptionDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		Prescription prescription = new Prescription();
		prescription.setDoctorCode(dto.getDoctorCode());
		prescription.setPrescriptionCode(dto.getPrescriptionCode());
		prescription.setPrescriptionDate(dto.getPrescriptionDate());
		prescription.setPrescriptionTime(dto.getPrescriptionTime());
		prescription.setPatientCode(dto.getPatientCode());
		prescription.setDiagnosisDate(dto.getDiagnosisDate());
		prescription.setDiagnosisTime(dto.getDiagnosisTime());
		prescription.setDoctorNotes(dto.getDoctorNotes());
		/*prescription.setAddedUser(dto.getAddedUser());
		prescription.setAddedDate(dto.getAddedDate());
		prescription.setAddedTime(dto.getAddedTime());
		prescription.setChangedUser(dto.getChangedUser());
		prescription.setChangedDate(dto.getChangedDate());
		prescription.setChangedTime(dto.getChangedTime());
*/
		processingBeforeDataUpdate(dto, prescription);

		Prescription prescription2 = prescriptionRepository.findById(prescription.getId()).get();
		if (prescription2 != null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0003"));
			processingIfDataRecordAlreadyExists(dto, prescription2);
		}
		else {
            try {
				prescriptionRepository.save(prescription);
                dto.setReturnCode(ReturnCodeEnum.fromCode(""));
                processingAfterDataUpdate(dto, prescription);
            } catch (Exception e) {
                dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
                processingIfDataUpdateError(dto, prescription);
            }
        }

        return result;
	}

    private StepResult processingBeforeDataUpdate(CreatePrescriptionDTO dto, Prescription prescription) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT DB1.Added User = JOB.*USER
		/*prescription.setAddedUser(job.getUser());
		//switchBLK 1000005 BLK ACT
		//functionCall 1000006 ACT DB1.Added Date = JOB.*Job date
		prescription.setAddedDate(LocalDate.now());
		//switchBLK 1000009 BLK ACT
		//functionCall 1000010 ACT DB1.Added Time = JOB.*Job time
		prescription.setAddedTime(LocalTime.now());*/
       return NO_ACTION;
    }

    private StepResult processingIfDataRecordAlreadyExists(CreatePrescriptionDTO dto, Prescription prescription) throws ServiceException {
		/**
		 * USER: Processing if Data Record Already Exists (Empty:43)
		 */
		//switchSUB 43 SUB    
		// Unprocessed SUB 43 -
       return NO_ACTION;
    }

    private StepResult processingAfterDataUpdate(CreatePrescriptionDTO dto, Prescription prescription) throws ServiceException {
		/**
		 * USER: Processing after Data Update (Empty:11)
		 */
		//switchSUB 11 SUB    
		// Unprocessed SUB 11 -
       return NO_ACTION;
    }

    private StepResult processingIfDataUpdateError(CreatePrescriptionDTO dto, Prescription prescription) throws ServiceException {
		/**
		 * Processing if Data Update Error (Empty:48)
		 */
		//switchSUB 48 SUB    
		// Unprocessed SUB 48 -
       return NO_ACTION;
    }
}
