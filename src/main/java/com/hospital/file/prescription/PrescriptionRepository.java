package com.hospital.file.prescription;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * Spring Data JPA repository interface for model: Prescription (TSAHCPP).
 *
 * @author X2EGenerator
 */
@Repository
public interface PrescriptionRepository extends PrescriptionRepositoryCustom, JpaRepository<Prescription, PrescriptionId> {

	List<Prescription> findAllByPatientCode(String patientCode);

	List<Prescription> findAllByPatientCodeAndDiagnosisDateAndDiagnosisTime(String patientCode, LocalDate diagnosisDate, LocalTime diagnosisTime);
}
