package com.hospital.file.workfile.createworkfile;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.PrescriptionStatusEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class CreateWorkFileDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private BigDecimal prescriptionCost;
	private BigDecimal totalAmount;
	private PrescriptionStatusEnum prescriptionStatus;
	private ReturnCodeEnum returnCode;
	private String doctorCode;
	private String hospitalCode;
	private String patientCode;
	private String prescriptionCode;
	private String wardCode;
	private UsrReturnCodeEnum usrReturnCode;
	private long lineNumber;
	private String nextScreen;

	public String getDoctorCode() {
		return doctorCode;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public long getLineNumber() {
		return lineNumber;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public String getPrescriptionCode() {
		return prescriptionCode;
	}

	public BigDecimal getPrescriptionCost() {
		return prescriptionCost;
	}

	public PrescriptionStatusEnum getPrescriptionStatus() {
		return prescriptionStatus;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public UsrReturnCodeEnum getUsrReturnCode() {
		return usrReturnCode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public void setLineNumber(long lineNumber) {
		this.lineNumber = lineNumber;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public void setPrescriptionCode(String prescriptionCode) {
		this.prescriptionCode = prescriptionCode;
	}

	public void setPrescriptionCost(BigDecimal prescriptionCost) {
		this.prescriptionCost = prescriptionCost;
	}

	public void setPrescriptionStatus(PrescriptionStatusEnum prescriptionStatus) {
		this.prescriptionStatus = prescriptionStatus;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setUsrReturnCode(UsrReturnCodeEnum usrReturnCode) {
		this.usrReturnCode = usrReturnCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
