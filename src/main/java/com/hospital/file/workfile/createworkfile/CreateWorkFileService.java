package com.hospital.file.workfile.createworkfile;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.workfile.WorkFile;
import com.hospital.file.workfile.WorkFileId;
import com.hospital.file.workfile.WorkFileRepository;
import com.hospital.model.PrescriptionStatusEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class CreateWorkFileService  extends AbstractService<CreateWorkFileService, CreateWorkFileDTO>
{
    private final Step execute = define("execute", CreateWorkFileDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private WorkFileRepository workFileRepository;
	

    @Autowired
    public CreateWorkFileService() {
        super(CreateWorkFileService.class, CreateWorkFileDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(CreateWorkFileDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(CreateWorkFileDTO dto, CreateWorkFileDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		WorkFile workFile = new WorkFile();
		workFile.setHospitalCode(dto.getHospitalCode());
		workFile.setWardCode(dto.getWardCode());
		workFile.setPatientCode(dto.getPatientCode());
		workFile.setDoctorCode(dto.getDoctorCode());
		workFile.setPrescriptionCode(dto.getPrescriptionCode());
		workFile.setPrescriptionCost(dto.getPrescriptionCost());
		workFile.setPrescriptionStatus(dto.getPrescriptionStatus());
		workFile.setTotalAmount(dto.getTotalAmount());
		workFile.setLineNumber(dto.getLineNumber());

		processingBeforeDataUpdate(dto, workFile);

		WorkFile workFile2 = workFileRepository.findById(workFile.getId()).get();
		if (workFile2 != null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0003"));
			processingIfDataRecordAlreadyExists(dto, workFile2);
		}
		else {
            try {
				workFileRepository.save(workFile);
                dto.setReturnCode(ReturnCodeEnum.fromCode(""));
                processingAfterDataUpdate(dto, workFile);
            } catch (Exception e) {
                dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
                processingIfDataUpdateError(dto, workFile);
            }
        }

        return result;
	}

    private StepResult processingBeforeDataUpdate(CreateWorkFileDTO dto, WorkFile workFile) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Empty:8)
		 */
		//switchSUB 8 SUB    
		// Unprocessed SUB 8 -
       return NO_ACTION;
    }

    private StepResult processingIfDataRecordAlreadyExists(CreateWorkFileDTO dto, WorkFile workFile) throws ServiceException {
		/**
		 * USER: Processing if Data Record Already Exists (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000013 BLK ACT
		//functionCall 1000014 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
    }

    private StepResult processingAfterDataUpdate(CreateWorkFileDTO dto, WorkFile workFile) throws ServiceException {
		/**
		 * USER: Processing after Data Update (Generated:11)
		 */
		//switchSUB 11 SUB    
		//switchBLK 1000025 BLK ACT
		//functionCall 1000026 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
    }

    private StepResult processingIfDataUpdateError(CreateWorkFileDTO dto, WorkFile workFile) throws ServiceException {
		/**
		 * Processing if Data Update Error (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000019 BLK ACT
		//functionCall 1000020 ACT PAR.USR Return Code = CND.Error
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("E"));
       return NO_ACTION;
    }
}
