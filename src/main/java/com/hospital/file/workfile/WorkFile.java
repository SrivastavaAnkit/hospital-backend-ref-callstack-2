package com.hospital.file.workfile;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.hospital.model.PrescriptionStatusConverter;
import com.hospital.model.PrescriptionStatusEnum;

@Entity
@Table(name="WorkFile", schema="HospitalMgmt")
public class WorkFile implements Serializable {
	private static final long serialVersionUID = -1L;

	@Version
	@Column(name="version")
	private long version;
	
	

	@EmbeddedId
	private WorkFileId id = new WorkFileId();

	@Column(name = "WardCode")
	private String wardCode = "";
	
	@Column(name = "PatientCode")
	private String patientCode = "";
	
	@Column(name = "DoctorCode")
	private String doctorCode = "";
	
	@Column(name = "PrescriptionCode")
	private String prescriptionCode = "";
	
	@Column(name = "PrescriptionCost")
	private BigDecimal prescriptionCost = BigDecimal.ZERO;
	
	@Column(name = "PrescriptionStatus")
	@Convert(converter=PrescriptionStatusConverter.class)
	private PrescriptionStatusEnum prescriptionStatus;
	
	@Column(name = "TotalAmount")
	private BigDecimal totalAmount = BigDecimal.ZERO;

	public WorkFileId getId() {
		return id;
	}

	public String getHospitalCode() {
		return id.getHospitalCode();
	}
	
	public long getLineNumber() {
		return id.getLineNumber();
	}

	public String getWardCode() {
		return wardCode;
	}
	
	public String getPatientCode() {
		return patientCode;
	}
	
	public String getDoctorCode() {
		return doctorCode;
	}
	
	public String getPrescriptionCode() {
		return prescriptionCode;
	}
	
	public BigDecimal getPrescriptionCost() {
		return prescriptionCost;
	}
	
	public PrescriptionStatusEnum getPrescriptionStatus() {
		return prescriptionStatus;
	}
	
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	

	public long getVersion() {
		return version;
	}

	public void setHospitalCode(String hospitalCode) {
		this.id.setHospitalCode(hospitalCode);
	}
	
	public void setLineNumber(long lineNumber) {
		this.id.setLineNumber(lineNumber);
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}
	
	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}
	
	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}
	
	public void setPrescriptionCode(String prescriptionCode) {
		this.prescriptionCode = prescriptionCode;
	}
	
	public void setPrescriptionCost(BigDecimal prescriptionCost) {
		this.prescriptionCost = prescriptionCost;
	}
	
	public void setPrescriptionStatus(PrescriptionStatusEnum prescriptionStatus) {
		this.prescriptionStatus = prescriptionStatus;
	}
	
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
