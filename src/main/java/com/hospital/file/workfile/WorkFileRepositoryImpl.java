package com.hospital.file.workfile;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;
import com.hospital.file.workfile.WorkFile;

/**
 * Custom Spring Data JPA repository implementation for model: Work File (TSAKCPP).
 *
 * @author X2EGenerator
 */
@Repository
public class WorkFileRepositoryImpl implements WorkFileRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see com.hospital.file.workfile.WorkFileService#deleteWorkFile(WorkFile)
	 */
	@Override
	public void deleteWorkFile(
		String hospitalCode, long lineNumber) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(hospitalCode)) {
			whereClause += " workFile.id.hospitalCode = :hospitalCode";
			isParamSet = true;
		}

		if (lineNumber > 0) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " workFile.id.lineNumber = :lineNumber";
			isParamSet = true;
		}

		String sqlString = "DELETE FROM WorkFile workFile";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(hospitalCode)) {
			query.setParameter("hospitalCode", hospitalCode);
		}

		if (lineNumber > 0) {
			query.setParameter("lineNumber", lineNumber);
		}

		em.createNativeQuery("SET foreign_key_checks = 0").executeUpdate();
		query.executeUpdate();
		em.createNativeQuery("SET foreign_key_checks = 1").executeUpdate();
	}

	/**
	 * @see com.hospital.file.workfile.WorkFileService#prtfilReportPerHosp(WorkFile)
	 */
	@Override
	public List<WorkFile> prtfilReportPerHosp() {
		String sqlString = "SELECT workFile FROM WorkFile workFile";

		Query query = em.createQuery(sqlString);

		@SuppressWarnings("unchecked")
		List<WorkFile> content = query.getResultList();

		return content;
	}

}
