package com.hospital.file.workfile.prtfilreportperhosp;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.workfile.WorkFile;
import com.hospital.file.workfile.WorkFileId;
import com.hospital.file.workfile.WorkFileRepository;
import com.hospital.model.PrescriptionStatusEnum;
import com.hospital.model.ReturnCodeEnum;



@Service
public class PrtfilReportPerHospService {

	
	@Autowired
	private JobContext job;
	
	@Autowired
	private WorkFileRepository workFileRepository;
	

	public void execute(PrtfilReportPerHospDTO prtfilReportPerHospDTO) {

		// TODO: Code generation is not currently supported for Print File Functions
	}
}
