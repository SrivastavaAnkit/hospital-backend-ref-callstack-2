package com.hospital.file.workfile;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.hospital.common.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

/**
 * Custom Spring Data JPA repository interface for model: Work File (TSAKCPP).
 *
 * @author X2EGenerator
 */
@Repository
public interface WorkFileRepositoryCustom {

	/**
	 * 
	 * @param hospitalCode Hospital Code
	 * @param lineNumber Line Number
	 */
	void deleteWorkFile(String hospitalCode, long lineNumber);

	/**
	 * 
	 * @return List of WorkFile
	 */
	List<WorkFile> prtfilReportPerHosp();
}
