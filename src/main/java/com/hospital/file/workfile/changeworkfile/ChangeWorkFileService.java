package com.hospital.file.workfile.changeworkfile;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.workfile.WorkFile;
import com.hospital.file.workfile.WorkFileId;
import com.hospital.file.workfile.WorkFileRepository;
import com.hospital.model.PrescriptionStatusEnum;
import com.hospital.model.ReturnCodeEnum;
import com.hospital.model.UsrReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class ChangeWorkFileService extends AbstractService<ChangeWorkFileService, ChangeWorkFileDTO>
{
    private final Step execute = define("execute", ChangeWorkFileDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private WorkFileRepository workFileRepository;
	

    @Autowired
    public ChangeWorkFileService() {
        super(ChangeWorkFileService.class, ChangeWorkFileDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(ChangeWorkFileDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(ChangeWorkFileDTO dto, ChangeWorkFileDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);
		WorkFileId workFileId = new WorkFileId();
		workFileId.setHospitalCode(dto.getHospitalCode());
		workFileId.setLineNumber(dto.getLineNumber());
		WorkFile workFile = workFileRepository.findById(workFileId).get();
		if (workFile == null) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0005"));
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, workFile);
			workFile.setHospitalCode(dto.getHospitalCode());
			workFile.setWardCode(dto.getWardCode());
			workFile.setPatientCode(dto.getPatientCode());
			workFile.setDoctorCode(dto.getDoctorCode());
			workFile.setPrescriptionCode(dto.getPrescriptionCode());
			workFile.setLineNumber(dto.getLineNumber());
			workFile.setPrescriptionCost(dto.getPrescriptionCost());
			workFile.setPrescriptionStatus(dto.getPrescriptionStatus());
			workFile.setTotalAmount(dto.getTotalAmount());
			processingBeforeDataUpdate(dto, workFile);
			try {
				workFileRepository.saveAndFlush(workFile);
				dto.setReturnCode(ReturnCodeEnum.fromCode(""));
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
			}
		}

       return result;
	}

	private StepResult processingBeforeDataRead(ChangeWorkFileDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		//switchSUB 8 SUB    
		//switchBLK 1000025 BLK CAS
		//switchSUB 1000025 BLK CAS
		if (dto.getHospitalCode().equals("PUBLIC")) {
			// PAR.Hospital Code is Equal Public
			//switchBLK 1000028 BLK ACT
			//functionCall 1000029 ACT LCL.Medication Description = CON.2 Pills by day
			dto.setLclMedicationDescription("2 Pills by day");
		}
       return NO_ACTION;
	}

	private StepResult processingIfDataRecordNotFound(ChangeWorkFileDTO dto) throws ServiceException {
		/**
		 * USER: Processing if Data Record Not Found (Generated:43)
		 */
		//switchSUB 43 SUB    
		//switchBLK 1000001 BLK ACT
		//functionCall 1000002 ACT PAR.USR Return Code = CND.Record Not found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("N"));
       return NO_ACTION;
	}

	private StepResult processingAfterDataRead(ChangeWorkFileDTO dto, WorkFile workFile) throws ServiceException {
		/**
		 * USER: Processing After Data Read (Generated:48)
		 */
		//switchSUB 48 SUB    
		//switchBLK 1000034 BLK CAS
		//switchSUB 1000034 BLK CAS
		if (dto.getHospitalCode().equals("PUBLIC")) {
			// PAR.Hospital Code is Equal Public
			//switchBLK 1000037 BLK ACT
			//functionCall 1000038 ACT LCL.Medication Description = CON.2 Pills by day
			dto.setLclMedicationDescription("2 Pills by day");
		}
       return NO_ACTION;
	}

	private StepResult processingBeforeDataUpdate(ChangeWorkFileDTO dto, WorkFile workFile) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Generated:10)
		 */
		//switchSUB 10 SUB    
		//switchBLK 1000013 BLK ACT
		//functionCall 1000014 ACT DB1.Changed User = JOB.*USER
		//workFile.setChangedUser(job.getUser());
		//switchBLK 1000017 BLK ACT
		//functionCall 1000018 ACT DB1.Changed Date = JOB.*Job date
		//workFile.setChangedDate(LocalDate.now());
		//switchBLK 1000021 BLK ACT
		//functionCall 1000022 ACT DB1.Changed Time = JOB.*Job time
		//workFile.setChangedTime(LocalTime.now());
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(ChangeWorkFileDTO dto) throws ServiceException {
		/**
		 * USER: Processing After Data Update (Generated:23)
		 */
		//switchSUB 23 SUB    
		//switchBLK 1000007 BLK ACT
		//functionCall 1000008 ACT PAR.USR Return Code = CND.Record found
		dto.setUsrReturnCode(UsrReturnCodeEnum.fromCode("F"));
       return NO_ACTION;
	}
}
