package com.hospital.file.workfile;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.io.Serializable;
import javax.persistence.Column;

public class WorkFileId implements Serializable {
	private static final long serialVersionUID = -1L;

	@Column(name = "HospitalCode")
	private String hospitalCode = "";
	
	@Column(name = "LineNumber")
	private long lineNumber = 0;

	public WorkFileId() {
	
	}

	public WorkFileId(String hospitalCode, long lineNumber) {
		this.hospitalCode = hospitalCode;
		this.lineNumber = lineNumber;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}
	
	public long getLineNumber() {
		return lineNumber;
	}
	
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	
	public void setLineNumber(long lineNumber) {
		this.lineNumber = lineNumber;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
