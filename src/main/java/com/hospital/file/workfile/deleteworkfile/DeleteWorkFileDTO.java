package com.hospital.file.workfile.deleteworkfile;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.hospital.model.GlobalContext;
import com.hospital.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class DeleteWorkFileDTO implements Serializable {

	@Autowired
	private GlobalContext global;

	private ReturnCodeEnum returnCode;
	private String hospitalCode;
	private long lineNumber;
	private String nextScreen;

	public String getHospitalCode() {
		return hospitalCode;
	}

	public long getLineNumber() {
		return lineNumber;
	}

	public String getNextScreen() {
		return nextScreen;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public void setLineNumber(long lineNumber) {
		this.lineNumber = lineNumber;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
}
