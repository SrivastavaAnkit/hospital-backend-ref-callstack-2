package com.hospital.file.workfile.deleteworkfile;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospital.common.exception.ServiceException;
import com.hospital.support.JobContext;
import com.hospital.file.workfile.WorkFile;
import com.hospital.file.workfile.WorkFileId;
import com.hospital.file.workfile.WorkFileRepository;
import com.hospital.model.PrescriptionStatusEnum;
import com.hospital.model.ReturnCodeEnum;

import com.hospital.common.callstack.Step;
import com.hospital.common.callstack.StepResult;
import com.hospital.common.callstack.AbstractService;
import static com.hospital.common.callstack.StepResult.NO_ACTION;

@Service
public class DeleteWorkFileService  extends AbstractService<DeleteWorkFileService, DeleteWorkFileDTO>
{
    private final Step execute = define("execute", DeleteWorkFileDTO.class, (t, u) -> {
        try {
            return executeService(t, u);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return null;
    });
	
	@Autowired
	private JobContext job;
	
	@Autowired
	private WorkFileRepository workFileRepository;
	

    @Autowired
    public DeleteWorkFileService() {
        super(DeleteWorkFileService.class, DeleteWorkFileDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

	public StepResult execute(DeleteWorkFileDTO dto) throws ServiceException {
		return executeService(dto, dto);
	}

	public StepResult executeService(DeleteWorkFileDTO dto, DeleteWorkFileDTO params) throws ServiceException {
       StepResult result = NO_ACTION;

       BeanUtils.copyProperties(params, dto);

		processingBeforeDataUpdate(dto);
		WorkFileId workFileId = new WorkFileId();
		workFileId.setHospitalCode(dto.getHospitalCode());
		workFileId.setLineNumber(dto.getLineNumber());
		try {
			workFileRepository.deleteById(workFileId);
			workFileRepository.flush();
			dto.setReturnCode(ReturnCodeEnum.fromCode(""));
			processingAfterDataUpdate(dto);
		} catch (Exception e) {
			dto.setReturnCode(ReturnCodeEnum.fromCode("Y2U0004"));
		}

       return result;
	}

	private StepResult processingBeforeDataUpdate(DeleteWorkFileDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Update (Empty:9)
		 */
		
       return NO_ACTION;
	}

	private StepResult processingAfterDataUpdate(DeleteWorkFileDTO dto) throws ServiceException {
		/**
		 * USER: Processing Before Data Read (Empty:17)
		 */
		
       return NO_ACTION;
	}
}
