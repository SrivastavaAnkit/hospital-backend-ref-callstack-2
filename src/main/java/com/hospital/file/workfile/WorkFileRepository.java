package com.hospital.file.workfile;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository interface for model: Work File (TSAKCPP).
 *
 * @author X2EGenerator
 */
@Repository
public interface WorkFileRepository extends WorkFileRepositoryCustom, JpaRepository<WorkFile, WorkFileId> {
}
