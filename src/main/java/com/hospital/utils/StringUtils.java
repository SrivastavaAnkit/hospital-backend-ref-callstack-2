package com.hospital.utils;

import static org.apache.commons.lang3.StringUtils.leftPad;
import static org.apache.commons.lang3.StringUtils.rightPad;

/**
 * Utility class for <code>String</code> related operations.
 *
 * @author Amit Arya
 * @since (2016-10-25.15:35:16)
 */
public final class StringUtils {

  /**
   * Replacement for %ALL RPG literal.
   *
   * @param toRepeat
   * @param length
   * @return string
   */
  public static String all(String toRepeat, int length) {
    return repeatString(toRepeat, length);
  }

  /**
   * Assigns from source to the target field. Assigning starts with the rightmost character of
   * source field.
   *
   * @param source source field
   * @param target target field
   * @return assigned string
   */
  public static String assign(String source, String target) {
    int targetLen = target.length();
    int sourceLen = source.length();

    if (targetLen > sourceLen) {
      target = target.substring(0, target.length() - source.length()) + source;
    } else if (targetLen < sourceLen) {
      target = source.substring(source.length() - target.length());
    } else {
      target = source;
    }

    return target;
  }

  /**
   * Assigns a single String source field to several contiguous boolean array elements starting from
   * startIndex.
   *
   * @param source source field
   * @param startIndex start index of array
   * @param array target field
   * @return target array
   */
  public static boolean[] assignAll(String source, int startIndex, boolean array[]) {
    int len = length(source);

    if ((len == 0) || !source.matches("^[0,1]+")) {
      return array;
    }

    for (int i = 0; i < len; i++) {
      array[startIndex++] = (source.charAt(i) == '1');
    }

    return array;
  }

  /**
   * Assigns from source to the target field. Assigning begins with the leftmost character in source
   * field.
   *
   * @param source source field
   * @param target target field
   * @return assigned string
   */
  public static String assignLeft(String source, String target) {
    int targetLen = target.length();
    int sourceLen = source.length();

    if (targetLen > sourceLen) {
      target = source + target.substring(source.length());
    } else if (targetLen < sourceLen) {
      target = source.substring(0, target.length());
    } else {
      target = source;
    }

    return target;
  }

  public static String blanks(int length) {
    return repeatString(" ", length);
  }

  public static String hiChar(int len) {
    return hiLoChar(len, false);
  }

  private static String hiLoChar(int len, boolean isLoval) {
    StringBuilder hiStr = new StringBuilder();
    char repeat = isLoval ? Character.MIN_VALUE : Character.MAX_VALUE;

    for (int i = 0; i < len; i++) {
      hiStr.append(repeat);
    }

    return String.valueOf(hiStr);
  }

  /**
   * Returns the length of a String.
   *
   * @param strSource string whose length is to be known
   * @return length of the String
   */
  public static int length(String strSource) {
    return (strSource == null) ? 0 : strSource.length();
  }

  public static String loChar(int len) {
    return hiLoChar(len, true);
  }

  /**
   * Trims leading white spaces.
   *
   * @param strToLTrim string array to trim
   * @return array containing strings with trimmed white spaces
   */
  public static String ltrim(String strToLTrim) {
    // If strToLTrim is null then simply return strToLTrim
    int len = length(strToLTrim);

    if (len == 0) {
      return strToLTrim;
    }

    char chArrToLTrim[] = strToLTrim.toCharArray();

    int i;

    for (i = 0; i < len; i++) {
      char ch = chArrToLTrim[i];
      int chAscii = ch;

      if (!(Character.isSpaceChar(ch) || (chAscii == 0))) {
        break;
      }
    }

    if (i == 0) {
      return strToLTrim;
    }

    return strToLTrim.substring(i, len);
  }

  public static String repeatString(String strSource, int numTimes) {
    // If strSource is null then simply return strSource
    int len = length(strSource);

    if (len == 0) {
      return strSource;
    }

    if (numTimes == 0) {
      return "";
    }

    if (numTimes == 1) {
      return strSource;
    }

    StringBuffer strToReturn = new StringBuffer(numTimes * len);

    for (int i = 0; i < numTimes; ++i) {
      strToReturn.append(strSource);
    }

    return strToReturn.toString();
  }

  /**
   * Trims trailing white spaces.
   *
   * @param strToRTrim source string
   * @return string with trimmed white spaces
   */
  public static String rtrim(String strToRTrim) {
    // If strToRTrim is null then simply return strToRTrim
    int len = length(strToRTrim);

    if (len == 0) {
      return strToRTrim;
    }

    char chArrToRTrim[] = strToRTrim.toCharArray();

    int i;

    for (i = len - 1; i >= 0; i--) {
      char ch = chArrToRTrim[i];
      int chAscii = ch;

      if (!(Character.isSpaceChar(ch) || (chAscii == 0))) {
        break;
      }
    }

    if (i == (len - 1)) {
      return strToRTrim;
    }

    return strToRTrim.substring(0, i + 1);
  }

  /**
   * Converts an Object to String.
   *
   * @param obj Object
   * @return converted object in String
   */
  public static String toChar(Object obj) {
    if (obj == null) {
      return "";
    }

    /*
     * if (isUserDefOrArr(obj.getClass())) { return objectToString(obj); }
     */

    String str = String.valueOf(obj);

    if (str.startsWith("0") && obj instanceof Integer) {
      // %Char converts leading zeros in the numeric field to blanks (the
      // numeric field is zero suppressed)
      str = str.replaceFirst("^0*", "");

      if ("".equals(str)) {
        str = "0";
      }
    }

    return str;
  }

  /**
   * Converts an Object to String and pads with a specified character to match the specified size.
   *
   * @param obj Object
   * @param size Object
   * @return converted object in String
   */
  public static String toChar(Object obj, int size) {
    if (obj == null) {
      return "";
    }

    String str = String.valueOf(obj);

    if (obj instanceof Number) {
      str = leftPad(str, size, "0");
    } else {
      str = rightPad(str, size);
    }

    return str;
  }

  /**
   * Trims leading and trailing white spaces in a given source string.
   *
   * @param strToTrim source string
   * @return string with trimmed white spaces
   */
  public static String trim(String strToTrim) {
    // If the string to trim is null then simply return the strToTrim
    if (strToTrim == null) {
      return strToTrim;
    }

    return ltrim(rtrim(strToTrim));
  }

  /**
   * Trims leading and trailing white spaces in a given source string and also sets the string to
   * default value if the string is null or empty.
   *
   * @param strToTrim source string
   * @param strDef default string
   * @return string with trimmed white spaces or default value
   */
  public static String trim(String strToTrim, String strDef) {
    String strToReturn = trim(strToTrim);

    if (length(strToReturn) == 0) {
      strToReturn = strDef;
    }

    return strToReturn;
  }


}
