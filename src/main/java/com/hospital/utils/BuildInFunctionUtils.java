package com.hospital.utils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.regex.Pattern;

import static java.lang.Math.toIntExact;
import static java.time.Month.*;

/**
 * Runtime implementation of Synon build-in functions.
 * @author Zhen Qiang He
 *
 */
public class BuildInFunctionUtils implements Serializable {

    private static final long serialVersionUID = 1694986942579952600L;

    /**
     * @param dateObj
     * @param detailType
     * @param excludedDays
     * @param excludedDates
     * @param autoLoaded
     * @param reverseSelection
     * @return
     */
    public static Integer getDateDetails(Object dateObj, String detailType, String excludedDays, List<Object> excludedDates, Boolean autoLoaded,
            Boolean reverseSelection) {
        Integer result = 0;
        detailType = (detailType == null) || detailType.trim().isEmpty() ? "AD" : detailType.trim().toUpperCase();
        excludedDays = excludedDays == null ? "1111111" : (excludedDays.trim() + "1111111").substring(0, 7);
        Set<LocalDateTime> excludedDatesSet = new HashSet<LocalDateTime>();
        if ((excludedDates != null) && !excludedDates.isEmpty()) {
            for (Object obj : excludedDates) {
                LocalDateTime dt = BuildInFunctionUtils.getDateObject(obj, true);
                if ((dt != null) && !excludedDatesSet.contains(dt)) {
                    excludedDatesSet.add(dt);
                }
            }
        }
        LocalDateTime date = BuildInFunctionUtils.getDateObject(dateObj, false);
        switch (detailType) {
            case "AD":
                if ("1111111".equals(excludedDays) && (excludedDatesSet.isEmpty())) {
                    result = Math.toIntExact(ChronoUnit.DAYS.between(LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0), date));
                } else {
                    LocalDateTime dayAtStartOfYear = LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0);
                    for (LocalDateTime day = dayAtStartOfYear; day.isBefore(date); day = day.plusDays(1)) {
                        if ((excludedDays.charAt(day.getDayOfWeek().getValue() - 1) == '1') && !excludedDatesSet.contains(day)) {
                            result++;
                        }
                    }
                }
                break;
            case "DW":
                if ("1111111".equals(excludedDays) && (excludedDatesSet.isEmpty())) {
                    result = date.getDayOfWeek().getValue();
                } else {
                    for (LocalDateTime day = (LocalDateTime.of(date.getYear(), date.getMonth().getValue(), (date.getDayOfMonth() - date.getDayOfWeek().getValue()) + 1, 0, 0, 0, 0))
                            ; day.isBefore(date) || day.isEqual(date); day = day.plusDays(1)) {
                        if ((excludedDays.charAt(day.getDayOfWeek().getValue() - 1) == '1') && !excludedDatesSet.contains(day)) {
                            result++;
                        }
                    }
                }
                break;
            case "DM":
                if ("1111111".equals(excludedDays) && (excludedDatesSet.isEmpty())) {
                    result = date.getDayOfMonth();
                } else {
                    for (LocalDateTime day = (LocalDateTime.of(date.getYear(), date.getMonth(), 1, 0, 0, 0, 0)); day.isBefore(date); day = day
                            .plusDays(1)) {
                        if ((excludedDays.charAt(day.getDayOfWeek().getValue() - 1) == '1') && !excludedDatesSet.contains(day)) {
                            result++;
                        }
                    }
                }
                break;
            case "DY":
                if ("1111111".equals(excludedDays) && (excludedDatesSet.isEmpty())) {
                    result = date.getDayOfYear();
                } else {
                    for (LocalDateTime day = (LocalDateTime.of(date.getYear(), JANUARY, 1, 0, 0, 0, 0)); day.isBefore(date); day = day
                            .plusDays(1)) {
                        if ((excludedDays.charAt(day.getDayOfWeek().getValue() - 1) == '1') && !excludedDatesSet.contains(day)) {
                            result++;
                        }
                    }
                }
                break;
            case "YR":
                result = date.getYear();
                break;
            case "MO":
                result = date.getMonth().getValue();
                break;
            case "ML":
                int endOfMonth = YearMonth.from(date).atEndOfMonth().getDayOfMonth();
                if ("1111111".equals(excludedDays) && (excludedDatesSet.isEmpty())) {
                    result = endOfMonth;
                } else {

                    LocalDateTime lastDateOfMonth = (LocalDateTime.of(date.getYear(), date.getMonth(), endOfMonth, 0, 0, 0))
                            ;
                    for (LocalDateTime day = (LocalDateTime.of(date.getYear(), date.getMonth(), 1, 0, 0, 0, 0)); day
                            .isBefore(lastDateOfMonth) || day.isEqual(lastDateOfMonth); day = day.plusDays(1)) {
                        if ((excludedDays.charAt(day.getDayOfWeek().getValue() - 1) == '1') && !excludedDatesSet.contains(day)) {
                            result++;
                        }
                    }
                }
                break;
            case "YL":
                if ("1111111".equals(excludedDays) && (excludedDatesSet.isEmpty())) {
                    result = date.with(TemporalAdjusters.lastDayOfYear()).getDayOfYear(); //.getMaximumValue();
                } else {
                    LocalDateTime firstDateOfNextYear = (LocalDateTime.of(date.getYear() + 1, JANUARY, 1, 0, 0, 0, 0));
                    for (LocalDateTime day = (LocalDateTime.of(date.getYear(), JANUARY, 1, 0, 0, 0, 0)); day
                            .isBefore(firstDateOfNextYear); day = day.plusDays(1)) {
                        if ((excludedDays.charAt(day.getDayOfWeek().getValue() - 1) == '1') && !excludedDatesSet.contains(day)) {
                            result++;
                        }
                    }
                }
                break;
            case "LY":
                result = Year.of(date.getYear()).isLeap() ? 1 : 0;
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * @param dateObj
     * @param durationObj
     * @param durationType
     * @param excludedDays
     * @param excludedDates
     * @param autoLoaded
     * @param reverseSelection
     * @return
     */
    public static LocalDateTime getDateIncrement(Object dateObj, Object durationObj, String durationType, String excludedDays, List<Object> excludedDates,
            Boolean autoLoaded, Boolean reverseSelection) {
        LocalDateTime result = null;
        durationType = (durationType == null) || durationType.trim().isEmpty() ? "DY" : durationType.trim().toUpperCase();
        excludedDays = excludedDays == null ? "1111111" : (excludedDays.trim() + "1111111").substring(0, 7);
        LocalDateTime date = BuildInFunctionUtils.getDateObject(dateObj, false);
        int duration = 0;
        if (durationObj != null) {
            if (durationObj instanceof BigDecimal) {
                duration = ((BigDecimal) durationObj).intValue();
            } else if (durationObj instanceof BigInteger) {
                duration = ((BigInteger) durationObj).intValue();
            } else if (durationObj instanceof Long) {
                duration = ((Long) durationObj).intValue();
            } else if (durationObj instanceof Integer) {
                duration = (Integer) durationObj;
            } else if (durationObj instanceof Short) {
                duration = ((Short) durationObj).intValue();
            } else if (durationObj instanceof Byte) {
                duration = ((Byte) durationObj).intValue();
            } else if (durationObj instanceof String) {
                try {
                    duration = Integer.valueOf((String) durationObj);
                } catch (NumberFormatException e) {
                    duration = 0;
                }
            }
        }
        switch (durationType) {
            case "DY":
                Set<LocalDateTime> excludedDatesSet = new HashSet<LocalDateTime>();
                if ((excludedDates != null) && !excludedDates.isEmpty()) {
                    for (Object obj : excludedDates) {
                        LocalDateTime dt = BuildInFunctionUtils.getDateObject(obj, true);
                        if ((dt != null) && !excludedDatesSet.contains(dt)) {
                            excludedDatesSet.add(dt);
                        }
                    }
                }
                if ("1111111".equals(excludedDays) && (excludedDatesSet.isEmpty())) {
                    result = date.plusDays(duration);
                } else if (excludedDays.contains("1")) {
                    for (int i = 1; i <= duration;) {
                        while (true) {
                            date = date.plusDays(1);
                            if ((excludedDays.charAt(date.getDayOfWeek().getValue() - 1) == '1') && !excludedDatesSet.contains(date)) {
                                i++;
                                break;
                            }
                        }
                    }
                    result = date;
                }
                break;
            case "MO":
                result = date.plusMonths(duration);
                break;
            case "YR":
                result = date.plusYears(duration);
                break;
            case "YM":
                int years = 0;
                int months = 0;
                StringBuilder durationStr = new StringBuilder();
                durationStr.append(duration);
                while (durationStr.length() < 4) {
                    durationStr.insert((durationStr.charAt(0) == '-') || (durationStr.charAt(0) == '+') ? 1 : 0, '0');
                }
                try {
                    years = Integer.parseInt(durationStr.substring(0, durationStr.length() - 2));
                } catch (NumberFormatException e) {
                    years = 0;
                }
                try {
                    months = Integer.parseInt(durationStr.substring(durationStr.length() - 2));
                } catch (NumberFormatException e) {
                    months = 0;
                }
                months = duration < 0 ? -months : months;
                result = date.plusYears(years).plusMonths(months);
                break;
            case "YD":
                years = 0;
                months = 0;
                int days = 0;
                durationStr = new StringBuilder();
                durationStr.append(duration);
                while (durationStr.length() < 6) {
                    durationStr.insert((durationStr.charAt(0) == '-') || (durationStr.charAt(0) == '+') ? 1 : 0, '0');
                }
                try {
                    years = Integer.parseInt(durationStr.substring(0, durationStr.length() - 4));
                } catch (NumberFormatException e) {
                    years = 0;
                }
                try {
                    months = Integer.parseInt(durationStr.substring(durationStr.length() - 4, durationStr.length() - 2));
                } catch (NumberFormatException e) {
                    months = 0;
                }
                months = duration < 0 ? -months : months;
                try {
                    days = Integer.parseInt(durationStr.substring(durationStr.length() - 2));
                } catch (NumberFormatException e) {
                    days = 0;
                }
                days = duration < 0 ? -days : days;
                result = date.plusYears(years).plusMonths(months).plusDays(days);
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * @param dateObj1
     * @param dateObj2
     * @param durationType
     * @param excludedDays
     * @param excludedDates
     * @param autoLoaded
     * @param reverseSelection
     * @return
     */
    public static Integer getDuration(Object dateObj1, Object dateObj2, String durationType, String excludedDays, List<Object> excludedDates,
            Boolean autoLoaded, Boolean reverseSelection) {
        Integer result = 0;
        durationType = (durationType == null) || durationType.trim().isEmpty() ? "DY" : durationType.trim().toUpperCase();
        LocalDateTime date1 = BuildInFunctionUtils.getDateObject(dateObj1, false);
        LocalDateTime date2 = BuildInFunctionUtils.getDateObject(dateObj2, false);

        //PeriodFormatter formatterYM = (new PeriodFormatterBuilder()).minimumPrintedDigits(2).printZeroAlways().appendYears().appendMonths().toFormatter();
        //PeriodFormatter formatterYMD = (new PeriodFormatterBuilder()).minimumPrintedDigits(2).printZeroAlways().appendYears().appendMonths().appendDays()
        //        .toFormatter();
        switch (durationType) {
            case "DY":
                excludedDays = excludedDays == null ? "1111111" : (excludedDays.trim() + "1111111").substring(0, 7);
                Set<LocalDateTime> excludedDatesSet = new HashSet<LocalDateTime>();
                if ((excludedDates != null) && !excludedDates.isEmpty()) {
                    for (Object obj : excludedDates) {
                        LocalDateTime dt = BuildInFunctionUtils.getDateObject(obj, true);
                        if ((dt != null) && !excludedDatesSet.contains(dt)) {
                            excludedDatesSet.add(dt);
                        }
                    }
                }
                if ("1111111".equals(excludedDays) && (excludedDatesSet.isEmpty())) {
                    result = toIntExact(Duration.between(date2, date1).toDays());
                    //if (result > 0)
                        //result += 1;
                } else {
                    boolean reversed = date2.isAfter(date1);
                    if (!reversed) {
                        for (LocalDateTime day = date2; day.isBefore(date1); day = day.plusDays(1)) { //.withTimeAtStartOfDay()) {
                            if ((excludedDays.charAt(day.getDayOfWeek().getValue() - 1) == '1') && !excludedDatesSet.contains(day)) {
                                result++;
                            }
                        }
                    } else {
                        for (LocalDateTime day = date2; day.isAfter(date1); day = day.minusDays(1)) { //.withTimeAtStartOfDay()) {
                            if ((excludedDays.charAt(day.getDayOfWeek().getValue() - 1) == '1') && !excludedDatesSet.contains(day)) {
                                result++;
                            }
                        }

                    }
                    result = reversed ? -result : result;
                }
                break;
            case "MO":
                result = (Period.between(date2.toLocalDate(), date1.toLocalDate().plusDays(1)).getYears() * 12)
                        + Period.between(date2.toLocalDate(), date1.toLocalDate().plusDays(1)).getMonths();
                break;
            case "YR":
                result = Period.between(date2.toLocalDate(), date1.toLocalDate().plusDays(1)).getYears();
                break;
            case "YM":
                int years = Period.between(date2.toLocalDate(), date1.toLocalDate().plusDays(1)).getYears();
                int months = Period.between(date2.toLocalDate(), date1.toLocalDate().plusDays(1).minusYears(years)).getMonths();
                result = Integer.valueOf(((years < 10 && years > -10 ? "0" + Integer.toString(years) : Integer.toString(years)) +
                        (months < 10 && months > -10 ? "0" + Integer.toString(months) : Integer.toString(months))).replaceAll("-",""));
                result = (years < 0 || months < 0) ? -result : result;
                break;
            case "YD":
                years = Period.between(date2.toLocalDate(), date1.toLocalDate()).getYears();
                months = Period.between(date2.toLocalDate(), date1.toLocalDate().minusYears(years)).getMonths();
                int days = Period.between(date2.toLocalDate(), date1.toLocalDate().minusYears(years).minusMonths(months)).getDays();
                result = Integer.valueOf(((years < 10 && years > -10 ? "0" + Integer.toString(years) : Integer.toString(years)) +
                        (months < 10 && months > -10 ? "0" + Integer.toString(months) : Integer.toString(months))+
                        (days < 10 && days > -10 ? "0" + Integer.toString(days) : Integer.toString(days))).replaceAll("-",""));
                result = (years < 0 || months < 0 || days < 0) ? -result : result;
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * @param timeObj1
     * @param timeObj2
     * @param timeUnit
     * @return
     */
    public static Integer getElapsedTime(Object timeObj1, Object timeObj2, String timeUnit) {
        Integer result = 0;
        timeUnit = (timeUnit == null) || timeUnit.trim().isEmpty() ? "HM" : timeUnit.trim().toUpperCase();
        LocalDateTime time1 = BuildInFunctionUtils.getTimeObject(timeObj1, timeUnit);
        LocalDateTime time2 = BuildInFunctionUtils.getTimeObject(timeObj2, timeUnit);

        switch (timeUnit) {
            case "SE":
                result = toIntExact(Duration.between(time2, time1).getSeconds()); //secondsBetween(time2, time1).getSeconds();
                break;
            case "MN":
                result = toIntExact(Duration.between(time2, time1).toMinutes());
                break;
            case "HR":
                result = toIntExact(Duration.between(time2, time1).toHours());
                break;
            case "HM":
                Duration period = Duration.between(time2.toLocalDate().atTime(time2.getHour(),time2.getMinute(),0), time1.toLocalDate().atTime(time1.getHour(),time1.getMinute(),0));
                int hours = toIntExact(period.toHours());
                int minutes = toIntExact(period.minusHours(hours).toMinutes());
                result = Integer.valueOf((hours < 10 ? "0" + Integer.toString(hours) :  Integer.toString(hours)) +
                        (minutes < 10 ? "0" + Integer.toString(minutes) :  Integer.toString(minutes)));
                result = (hours < 0 || minutes < 0) ? -result : result;
                break;
            case "HS":
                period = Duration.between(time2.toLocalDate(), time1.toLocalDate());
                hours = toIntExact(period.toHours());
                minutes = toIntExact(period.minusHours(hours).toMinutes());
                int seconds = toIntExact(period.minusHours(hours).minusMinutes(minutes).getSeconds());
                result = Integer.valueOf((hours < 10 ? "0" + Integer.toString(hours) :  Integer.toString(hours)) +
                        (minutes < 10 ? "0" + Integer.toString(minutes) :  Integer.toString(minutes)) +
                        (seconds < 10 ? "0" + Integer.toString(seconds) :  Integer.toString(seconds)));
                result = (hours < 0 || minutes < 0) ? -result : result;
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * @param timeObj
     * @param detailType
     * @return
     *         TODO correct time detail type values
     */
    public static Integer getTimeDetails(Object timeObj, String detailType) {
        Integer result = 0;
        detailType = (detailType == null) || detailType.trim().isEmpty() ? "HM" : detailType.trim().toUpperCase();
        LocalDateTime time = BuildInFunctionUtils.getTimeObject(timeObj, detailType);
        DateTimeFormatter formatterHM = DateTimeFormatter.ofPattern("HHmm");
        DateTimeFormatter formatterHMS = DateTimeFormatter.ofPattern("HHmmss");
        switch (detailType) {
            case "SE": // *SECONDS
                result = time.getSecond();
                break;
            case "ES": // *ELAPSED SECONDS
                result = getSecondOfDay(time);
                break;
            case "MN": // *MINUTES
                result = time.getMinute();
                break;
            case "EM": // *ELAPSED MINUTES
                result = getMinuteOfDay(time);
                break;
            case "HR": // *HOURS
                result = time.getHour();
                break;
            case "HM": // *HHMM
                result = Integer.valueOf(time.format(formatterHM));
                break;
            case "HS": // *HHMMSS
                result = Integer.valueOf(time.format(formatterHMS));
                break;
            case "PM": // *PM?
                result = time.isBefore(LocalDateTime.of(1801, JANUARY, 1, 12, 0, 0, 0)) ? 0 : 1;
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * @param timeObj
     * @param elapsedTimeObj
     * @param elapsedTimeUnit
     * @return
     */
    public static LocalDateTime getTimeIncrement(Object timeObj, Object elapsedTimeObj, String elapsedTimeUnit) {
        LocalDateTime result = null;
        elapsedTimeUnit = (elapsedTimeUnit == null) || elapsedTimeUnit.trim().isEmpty() ? "HM" : elapsedTimeUnit.trim().toUpperCase();
        LocalDateTime time = BuildInFunctionUtils.getTimeObject(timeObj, elapsedTimeUnit);
        int elapsedTime = 0;
        if (elapsedTimeObj != null) {
            if (elapsedTimeObj instanceof BigDecimal) {
                elapsedTime = ((BigDecimal) elapsedTimeObj).intValue();
            } else if (elapsedTimeObj instanceof BigInteger) {
                elapsedTime = ((BigInteger) elapsedTimeObj).intValue();
            } else if (elapsedTimeObj instanceof Long) {
                elapsedTime = ((Long) elapsedTimeObj).intValue();
            } else if (elapsedTimeObj instanceof Integer) {
                elapsedTime = (Integer) elapsedTimeObj;
            } else if (elapsedTimeObj instanceof Short) {
                elapsedTime = ((Short) elapsedTimeObj).intValue();
            } else if (elapsedTimeObj instanceof Byte) {
                elapsedTime = ((Byte) elapsedTimeObj).intValue();
            } else if (elapsedTimeObj instanceof String) {
                try {
                    elapsedTime = Integer.valueOf((String) elapsedTimeObj);
                } catch (NumberFormatException e) {
                    elapsedTime = 0;
                }
            }
        }
        switch (elapsedTimeUnit) {
            case "SE": // *SECONDS
                result = time.plusSeconds(elapsedTime).withYear(1801).withMonth(JANUARY.getValue()).withDayOfMonth(1);
                break;
            case "MN": // *MINUTES
                result = time.plusMinutes(elapsedTime).withYear(1801).withMonth(JANUARY.getValue()).withDayOfMonth(1);
                break;
            case "HR": // *HOURS
                result = time.plusHours(elapsedTime).withYear(1801).withMonth(JANUARY.getValue()).withDayOfMonth(1);
                break;
            case "HM": // *HHMM
                int hours = 0;
                int minutes = 0;
                StringBuilder elapsedTimeStr = new StringBuilder();
                elapsedTimeStr.append(elapsedTime);
                while (elapsedTimeStr.length() < 4) {
                    elapsedTimeStr.insert((elapsedTimeStr.charAt(0) == '-') || (elapsedTimeStr.charAt(0) == '+') ? 1 : 0, '0');
                }
                try {
                    hours = Integer.parseInt(elapsedTimeStr.substring(0, elapsedTimeStr.length() - 2));
                } catch (NumberFormatException e) {
                    hours = 0;
                }
                try {
                    minutes = Integer.parseInt(elapsedTimeStr.substring(elapsedTimeStr.length() - 2));
                } catch (NumberFormatException e) {
                    minutes = 0;
                }
                minutes = elapsedTime < 0 ? -minutes : minutes;
                result = time.plusHours(hours).plusMinutes(minutes).withYear(1801).withMonth(JANUARY.getValue()).withDayOfMonth(1);
                break;
            case "HS": // *HHMMSS
                hours = 0;
                minutes = 0;
                int seconds = 0;
                elapsedTimeStr = new StringBuilder();
                elapsedTimeStr.append(elapsedTime);
                while (elapsedTimeStr.length() < 6) {
                    elapsedTimeStr.insert((elapsedTimeStr.charAt(0) == '-') || (elapsedTimeStr.charAt(0) == '+') ? 1 : 0, '0');
                }
                try {
                    hours = Integer.parseInt(elapsedTimeStr.substring(0, elapsedTimeStr.length() - 4));
                } catch (NumberFormatException e) {
                    hours = 0;
                }
                try {
                    minutes = Integer.parseInt(elapsedTimeStr.substring(elapsedTimeStr.length() - 4, elapsedTimeStr.length() - 2));
                } catch (NumberFormatException e) {
                    minutes = 0;
                }
                minutes = elapsedTime < 0 ? -minutes : minutes;
                try {
                    seconds = Integer.parseInt(elapsedTimeStr.substring(elapsedTimeStr.length() - 2));
                } catch (NumberFormatException e) {
                    seconds = 0;
                }
                seconds = elapsedTime < 0 ? -seconds : seconds;
                result = time.plusHours(hours).plusMinutes(minutes).plusSeconds(seconds).withYear(1801).withMonth(JANUARY.getValue())
                        .withDayOfMonth(1);
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * @param obj
     * @param allowNull
     * @return
     */
    protected static LocalDateTime getDateObject(Object obj, boolean allowNull) {
        LocalDateTime result = null;
        if (obj != null) {
            try {
                if (obj instanceof String) {
                    if (!((String) obj).trim().isEmpty())
                        result = LocalDate.parse((String) obj).atTime(LocalTime.parse("00:00"));//(LocalDateTime.parse((String) obj, DateTimeFormatter.ISO_LOCAL_DATE));
                } else if (obj instanceof Calendar) {
                    result = toLocalDateTime((Calendar) obj);
                } else if (obj instanceof Date) {
                    result = toLocalDateTime((Date) obj);
                } else if (obj instanceof LocalDateTime) {
                    result = ((LocalDateTime) obj);
                } else if (obj instanceof BigDecimal) {
                    result = (LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0)).plusDays(((BigDecimal) obj).intValue());
                } else if (obj instanceof BigInteger) {
                    result = (LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0)).plusDays(((BigInteger) obj).intValue());
                } else if (obj instanceof Long) {
                    result = (LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0)).plusDays(((Long) obj).intValue());
                } else if (obj instanceof Integer) {
                    result = (LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0)).plusDays((Integer) obj);
                } else if (obj instanceof Short) {
                    result = (LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0)).plusDays(((Short) obj).intValue());
                } else if (obj instanceof Byte) {
                    result = (LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0)).plusDays(((Byte) obj).intValue());
                }
            } catch (Exception e) {
            }
        }
        return result == null ? (!allowNull ? LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0) : null) : result
                ;
    }

    /**
     * @param obj
     * @param timeType
     * @return
     *         TODO correct time type values
     */
    protected static LocalDateTime getTimeObject(Object obj, String timeType) {
        LocalDateTime result = null;
        int elapsedTime = 0;
        if (obj != null) {
            try {
                if ((obj instanceof String) && !((String) obj).trim().isEmpty()) {
                    boolean isPM = false;
                    boolean isAM = false;
                    String timeStr = ((String) obj).trim().toUpperCase();
                    if (Pattern.matches("^([Tt])?((([01])?\\d)([:.,]?([0-5])?\\d([:.,]?([0-5])?\\d([.](\\d){0,6})?)?)?)(\\s)*[Pp][Mm]$", timeStr)) {
                        timeStr = timeStr.replaceAll("(\\s)*[Pp][Mm]$", "");
                        isPM = true;
                    } else if (Pattern.matches("^([Tt])?((([01])?\\d)([:.,]?([0-5])?\\d([:.,]?([0-5])?\\d([.](\\d){0,6})?)?)?)(\\s)*[Aa][Mm]$", timeStr)) {
                        timeStr = timeStr.replaceAll("(\\s)*[Aa][Mm]$", "");
                        isAM = true;
                    }
                    timeStr = (timeStr.startsWith("T") ? "" : "T") + timeStr;
                    if (timeStr.length() != 6) {
                        timeStr = "T0" + timeStr.substring(1);
                    }
                    String timeStrToParse = "1801-01-01" + timeStr + ":00";
                    DateTimeFormatter dtformatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
                    result = (LocalDateTime.parse(timeStrToParse, dtformatter)); //.withYear(1801).withMonth(JANUARY.getValue()).withDayOfMonth(1);
                    if (isPM && result.getHour() != 12) {
                        result = result.withHour(result.getHour() + 12);
                    } else if (isAM && result.getHour() == 12) {
                        result = result.withHour(0);
                    }
                } else if (obj instanceof Calendar) {
                    result =  toLocalDateTime((Calendar) obj).withYear(1801).withMonth(JANUARY.getValue()).withDayOfMonth(1); //(LocalDateTime.of(((Calendar) obj).get(MILLI_OF_SECOND))).withYear(1801).withMonth(JANUARY).withDayOfMonth(1);
                } else if (obj instanceof Date) {
                    result = toLocalDateTime((Date) obj).withYear(1801).withMonth(JANUARY.getValue()).withDayOfMonth(1);
                } else if (obj instanceof LocalDateTime) {
                    result = ((LocalDateTime) obj).withYear(1801).withMonth(JANUARY.getValue()).withDayOfMonth(1);
                } else if (obj instanceof BigDecimal) {
                    result = (LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0));
                    elapsedTime = ((BigDecimal) obj).intValue();
                } else if (obj instanceof BigInteger) {
                    result = (LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0));
                    elapsedTime = ((BigInteger) obj).intValue();
                } else if (obj instanceof Long) {
                    result = (LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0));
                    elapsedTime = ((Long) obj).intValue();
                } else if (obj instanceof Integer) {
                    result = (LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0));
                    elapsedTime = (Integer) obj;
                } else if (obj instanceof Short) {
                    result = (LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0));
                    elapsedTime = ((Short) obj).intValue();
                } else if (obj instanceof Byte) {
                    result = (LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0));
                    elapsedTime = ((Byte) obj).intValue();
                }
                if ((elapsedTime != 0L) && (result != null)) {
                    switch (timeType) {
                        case "MN":
                        case "EM":
                            result = result.plusMinutes(elapsedTime).withYear(1801).withMonth(JANUARY.getValue()).withDayOfMonth(1);
                            break;
                        case "HR":
                            result = result.plusHours(elapsedTime).withYear(1801).withMonth(JANUARY.getValue()).withDayOfMonth(1);
                            break;
                        case "HM":
                            int hours = 0;
                            int minutes = 0;
                            StringBuilder durationStr = new StringBuilder();
                            durationStr.append(elapsedTime);
                            while (durationStr.length() < 4) {
                                durationStr.insert((durationStr.charAt(0) == '-') || (durationStr.charAt(0) == '+') ? 1 : 0, '0');
                            }
                            try {
                                hours = Integer.parseInt(durationStr.substring(0, durationStr.length() - 2));
                            } catch (NumberFormatException e) {
                                hours = 0;
                            }
                            try {
                                minutes = Integer.parseInt(durationStr.substring(durationStr.length() - 2));
                            } catch (NumberFormatException e) {
                                minutes = 0;
                            }
                            minutes = elapsedTime < 0 ? -minutes : minutes;
                            result = result.plusHours(hours).plusMinutes(minutes).withYear(1801).withMonth(JANUARY.getValue()).withDayOfMonth(1);
                            break;
                        case "HS":
                            hours = 0;
                            minutes = 0;
                            int seconds = 0;
                            durationStr = new StringBuilder();
                            durationStr.append(elapsedTime);
                            while (durationStr.length() < 6) {
                                durationStr.insert((durationStr.charAt(0) == '-') || (durationStr.charAt(0) == '+') ? 1 : 0, '0');
                            }
                            try {
                                hours = Integer.parseInt(durationStr.substring(0, durationStr.length() - 4));
                            } catch (NumberFormatException e) {
                                hours = 0;
                            }
                            try {
                                minutes = Integer.parseInt(durationStr.substring(durationStr.length() - 4, durationStr.length() - 2));
                            } catch (NumberFormatException e) {
                                minutes = 0;
                            }
                            minutes = elapsedTime < 0 ? -minutes : minutes;
                            try {
                                seconds = Integer.parseInt(durationStr.substring(durationStr.length() - 2));
                            } catch (NumberFormatException e) {
                                seconds = 0;
                            }
                            seconds = elapsedTime < 0 ? -seconds : seconds;
                            result = result.plusHours(hours).plusMinutes(minutes).plusSeconds(seconds).withYear(1801)
                                    .withMonth(JANUARY.getValue()).withDayOfMonth(1);
                            break;
                        case "SE":
                        case "ES":
                        case "PM":
                        default:
                            result = result.plusSeconds(elapsedTime).withYear(1801).withMonth(JANUARY.getValue()).withDayOfMonth(1);
                            break;
                    }
                }
            } catch (Exception e) {
            }
        }
        return result == null ? LocalDateTime.of(1801, JANUARY, 1, 0, 0, 0, 0) : result.withYear(1801).withMonth(JANUARY.getValue())
                .withDayOfMonth(1);
    }

    private static LocalDateTime toLocalDateTime(Calendar calendar) {
        if (calendar == null) {
            return null;
        }
        TimeZone tz = calendar.getTimeZone();
        ZoneId zid = tz == null ? ZoneId.systemDefault() : tz.toZoneId();
        LocalDateTime ldt = LocalDateTime.ofInstant(calendar.toInstant(), zid);
        return ldt.withHour(0).withMinute(0).withSecond(0).withNano(0);
    }

    private static LocalDateTime toLocalDateTime(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime().withHour(0).withMinute(0).withSecond(0).withNano(0);
    }

    private static int getSecondOfDay(LocalDateTime localDateTime) {
        return (localDateTime.getHour() * 60 * 60) + (localDateTime.getMinute() * 60) + localDateTime.getSecond();
    }

    private static int getMinuteOfDay(LocalDateTime localDateTime) {
        return (localDateTime.getHour() * 60) + localDateTime.getMinute();
    }
}