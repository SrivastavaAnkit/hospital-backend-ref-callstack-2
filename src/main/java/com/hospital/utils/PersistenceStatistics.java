package com.hospital.utils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.jpa.HibernateEntityManagerFactory;

import org.hibernate.stat.Statistics;

import org.springframework.beans.factory.FactoryBean;

import org.springframework.stereotype.Component;

/**
 * Factory bean that returns object which provides persistence statistics. It is
 * also exported as MBean through JMX.
 *
 * @author Robin Rizvi
 * @since (2016-03-04.18:57:12)
 */
@Component("PersistenceStatistics")
public class PersistenceStatistics implements FactoryBean<Statistics> {

	@PersistenceContext
	EntityManager em;

	@Override
	public Statistics getObject() throws Exception {
		return ((HibernateEntityManagerFactory)em.getEntityManagerFactory()).getSessionFactory()
				.getStatistics();
	}

	@Override
	public Class<?> getObjectType() {
		return Statistics.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
}
