#### Small changes

* Remove setFieldError field as it is a duplicate of atmsgid.
* Move LastIO to com.hospital.common.dao


#### Loops are broken

Those do {} while loops does not seems to work:
```    
do {
  dto.setPgmInd84(move("11", dto.getPgmInd84(), 2));
  // WRITE SFF01
  // Next Screen Format to Display :SFC01
  return callScreen("App.im32502", dto).thenCall(postCallIm325Controllerp2);
} while (!(dto.getFunKey01()))
```       

#### Leave statement not generated

