# Russel Metals Modernization 

This project is the backend component of the migrated RMI application logic. It contains the migrated RPG applications as well as new application services to support the different end-points of the application.

### Requirements

* [Sun JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Maven 3.3.9 or more](https://maven.apache.org/) with access to [RMI artifactory repository](https://freschesolutions.jfrog.io/freschesolutions/webapp/#/home) properly configured. (See later.)
* Your favorite IDE with [google-java-format](https://github.com/google/google-java-format) plugin installed.

### Building

Building from source
```
mvn clean install
```

### Code style

This project use google standard for code format. Please 
refer to the project [google-java-format](https://github.com/google/google-java-format) for instructions to install your favorite
IDE's plugin.


### Configuring artifactory

In case you have your credentials,you must log to [RMI Artifactory](https://freschesolutions.jfrog.io) to get your credentials to complete your settings.xml. The username is the same, but the password is an encrypted password only available in your profile section of the artifactory web application.

In case you don't have your credentials, you can download this [file](https://fresche.atlassian.net/wiki/download/attachments/53379233/settings.xml?version=1&modificationDate=1511209416909&cacheVersion=1&api=v2&download=true) and save it to your .m2 folder. You must be logged to confluence for this to work. This will be removed soon and Patria is working on setting up personal credentials for every committer. 

Here is the look of your ${HOME}/.m2/settings.xml
```
<?xml version="1.0" encoding="UTF-8"?>
<settings xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd" xmlns="http://maven.apache.org/SETTINGS/1.1.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <servers>
    <server>
      <username>${ARTIFACTORY_USERNAME}</username>
      <password>${ARTIFACTORY_PASSWORD}</password>
      <id>central</id>
    </server>
    <server>
      <username>${ARTIFACTORY_USERNAME}</username>
      <password>${ARTIFACTORY_PASSWORD}</password>
      <id>snapshots</id>
    </server>
  </servers>
  <profiles>
    <profile>
      <repositories>
        <repository>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <id>central</id>
          <name>rmi-mvn-libs-release-virtual</name>
          <url>https://freschesolutions.jfrog.io/freschesolutions/rmi-mvn-libs-release-virtual</url>
        </repository>
        <repository>
          <snapshots />
          <id>snapshots</id>
          <name>rmi-mvn-libs-snapshot-virtual</name>
          <url>https://freschesolutions.jfrog.io/freschesolutions/rmi-mvn-libs-snapshot-virtual</url>
        </repository>
      </repositories>
      <pluginRepositories>
        <pluginRepository>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <id>central</id>
          <name>rmi-mvn-libs-release-virtual</name>
          <url>https://freschesolutions.jfrog.io/freschesolutions/rmi-mvn-libs-release-virtual</url>
        </pluginRepository>
        <pluginRepository>
          <snapshots />
          <id>snapshots</id>
          <name>rmi-mvn-libs-snapshot-virtual</name>
          <url>https://freschesolutions.jfrog.io/freschesolutions/rmi-mvn-libs-snapshot-virtual</url>
        </pluginRepository>
      </pluginRepositories>
      <id>artifactory</id>
    </profile>
  </profiles>
  <activeProfiles>
    <activeProfile>artifactory</activeProfile>
  </activeProfiles>
</settings>
```