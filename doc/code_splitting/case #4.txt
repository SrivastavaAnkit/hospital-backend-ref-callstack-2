
let response;
fun pgm() {

    stmt1();
    if (<predicate>) {
       stmt5();
       sr01();
       stmt11();
    } else {
       stmt9();
       sr02();
       stmt10();
    }
    stmt4();
}

fun sr01() {
    stmt6();
    do while (<predicate>) {
       response = callScreen(x)
       stmt8();
    }
}

fun sr02() {
    stmt6();
    do while (<predicate>) {
       response = callScreen(x)
       stmt8();
    }
    sr01();
}


--With post call -----------------------------------------------------------------------

fun pgm() {

    stmt1();
    if (<predicate>) {
       stmt5();
       stack.push(pgm_endIf1)
       return sr01();
    } else {
       stmt9();
       stack.push(pgm_endElse1)
       return sr02();
    }
}

fun pgm_endIf1() {
    stmt11();
    return pgm_afterIf1()
}

fun pgm_endElse1() {
    stmt10();
    return pgm_afterIf1()
}

fun pgm_afterIf1() {
    stmt4();
}

fun sr01() {
    stmt6();
    sr01_startDo1()
}

fun sr01_startDo1() {
    if (<predicate>) {
       stack.push(sr01_endDo1)
       return {callscreen, [x]}
    }
}

fun sr01_endDo1() {
    stmt8();
    return sr01_startDo1();
}


fun sr02() {
    stmt6();
    stack.push(sr02_afterDo1)
    return sr02_startDo1();
}

fun sr02_startDo1() {
    if (<predicate>) {
       stack.push(sr02_endDo1)
       return {callscreen, [x]}
    }
    return sr02_afterDo1();
}

fun sr02_endDo1() {
    stmt8();
    return sr02_startDo1();
}

fun sr02_afterDo1() {
    return sr01();
}
