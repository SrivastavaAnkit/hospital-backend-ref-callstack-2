# Callstack API

The purposes of the callstack are:
1. Allow blocking procedural legacy application code logic to be split around interactive calls so they can run within standard modern
application servers.
1. Allow to keep application state to an external store to allow load distribution and high availability.

### Concepts

#### Interactive program
An interactive program is a program that calls a screen or another interactive service in a blocking fashion.

#### Services
A service represent a converted interactive program logic. A converted service is split
in many steps so it become non-blocking. Also, program state is stored in an external DTO.

Classes:

* com.hospital.common.callstack.Service
* com.hospital.common.callstack.AbstractService

#### Step 
A step is a piece of logic belonging to a service that was between interactive step boundary. A step
always return a step result that indicate to the runtime what action
to perform next. 

Classes:

* com.hospital.common.callstack.Step
* com.hospital.common.callstack.StepResult

#### Terminal Session and the Client API

A terminal session is an interactive session with the backend system. Terminal session differ from a browser session 
in the sense that each browser tab have a different Terminal Session instance and Terminal Session lifecycle are managed
by the client using the client API. Terminal Session is identified by a Terminal Session Id (tsId).

Classes:

* com.hospital.common.callstack.clientapi.ClientApiController
* com.hospital.common.callstack.runtime.TerminalSessionInfo


#### Service under test 
A service under test is a service that is being tested in a controlled integrated environment that include everything except the UI. 

#### Runtime
The runtime is used to execute the services in a non-blocking clusterable server environment. It is split in multiple layers to ease the testing and also provide opportunities for future integrations.

##### com.hospital.common.callstack.RunningService
This serializable class encapsulate a service state with a service step scheduler. This class will execute all the scheduled steps until there is an interactive action to perform or the service has returned. 

Classes:

* com.hospital.common.callstack.runtime.RunningService
* com.hospital.common.callstack.runtime.StepMethod

##### com.hospital.common.callstack.runtime.TerminalSession
This class manage an interactive session with the user. It uses an external storage to save, lock and restore session 
state. It also provide methods to execute steps. Those methods internally manage interactive steps that involve calling 
services and returning from those services but will return actions that involve calling a screen on the client.

Classes:

* com.hospital.common.callstack.runtime.RunningService
* com.hospital.common.callstack.runtime.StepMethod


