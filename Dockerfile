FROM azul/zulu-openjdk-alpine:8
EXPOSE 8080
COPY target/rmi-backend-*.jar /opt/rmi-backend/rmi-backend.jar
CMD  ["java", "-jar", "/opt/rmi-backend/rmi-backend.jar"]
